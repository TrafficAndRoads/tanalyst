﻿Public Class frmVisualiserOptions
  Private MyProject As clsProject
  Private MyNormalRUColour As Color
  Private MyProjectedRUColour As Color
  Private MySelectedRUColour As Color
  Private MyKey1RUColour As Color
  Private MyKey2RUColour As Color
  Private MyNormalTrajectoryColour As Color
  Private MyProjectedTrajectoryColour As Color
  Private MySelectedTrajectoryColour As Color
  Private MyKey1TrajectoryColour As Color
  Private MyKey2TrajectoryColour As Color
  Private MyRoadUserLineWidth As Single
  Private MyTrajectoryLineWidth As Single

  Public Sub New(Project As clsProject)
    InitializeComponent()

    MyProject = Project
    With MyProject.VisualiserOptions
      MyNormalRUColour = .NormalRUColour
      MyProjectedRUColour = .ProjectedRUColour
      MySelectedRUColour = .SelectedRUColour
      MyKey1RUColour = .Key1RUColour
      MyKey2RUColour = .Key2RUColour

      MyNormalTrajectoryColour = .NormalTrajectoryColour
      MyProjectedTrajectoryColour = .ProjectedTrajectoryColour
      MySelectedTrajectoryColour = .SelectedTrajectoryColour
      MyKey1TrajectoryColour = .Key1TrajectoryColour
      MyKey2TrajectoryColour = .Key2TrajectoryColour

      MyRoadUserLineWidth = .LineWidthRoadUser
      MyTrajectoryLineWidth = .LineWidthTrajectory
    End With

    With cmbRULineWidth
      .Items.Clear()
      .Items.Add("0.5")
      .Items.Add("0.75")
      .Items.Add("1")
      .Items.Add("1.25")
      .Items.Add("1.5")
      .Items.Add("1.75")
      .Items.Add("2")
      .Items.Add("2.25")
      .Items.Add("2.5")

      If IsEqual(MyRoadUserLineWidth, 0.5) Then
        .SelectedIndex = 0
      ElseIf IsEqual(MyRoadUserLineWidth, 0.75) Then
        .SelectedIndex = 1
      ElseIf IsEqual(MyRoadUserLineWidth, 1) Then
        .SelectedIndex = 2
      ElseIf IsEqual(MyRoadUserLineWidth, 1.25) Then
        .SelectedIndex = 3
      ElseIf IsEqual(MyRoadUserLineWidth, 1.5) Then
        .SelectedIndex = 4
      ElseIf IsEqual(MyRoadUserLineWidth, 1.75) Then
        .SelectedIndex = 5
      ElseIf IsEqual(MyRoadUserLineWidth, 2) Then
        .SelectedIndex = 6
      ElseIf IsEqual(MyRoadUserLineWidth, 2.25) Then
        .SelectedIndex = 7
      ElseIf IsEqual(MyRoadUserLineWidth, 2.5) Then
        .SelectedIndex = 8
      Else
        .SelectedIndex = 2
      End If
    End With

    With cmbTrajectoryWidth
      .Items.Clear()
      .Items.Add("0.5")
      .Items.Add("0.75")
      .Items.Add("1")
      .Items.Add("1.25")
      .Items.Add("1.5")
      .Items.Add("1.75")
      .Items.Add("2")
      .Items.Add("2.25")
      .Items.Add("2.5")

      If IsEqual(MyTrajectoryLineWidth, 0.5) Then
        .SelectedIndex = 0
      ElseIf IsEqual(MyTrajectoryLineWidth, 0.75) Then
        .SelectedIndex = 1
      ElseIf IsEqual(MyTrajectoryLineWidth, 1) Then
        .SelectedIndex = 2
      ElseIf IsEqual(MyTrajectoryLineWidth, 1.25) Then
        .SelectedIndex = 3
      ElseIf IsEqual(MyTrajectoryLineWidth, 1.5) Then
        .SelectedIndex = 4
      ElseIf IsEqual(MyTrajectoryLineWidth, 1.75) Then
        .SelectedIndex = 5
      ElseIf IsEqual(MyTrajectoryLineWidth, 2) Then
        .SelectedIndex = 6
      ElseIf IsEqual(MyTrajectoryLineWidth, 2.25) Then
        .SelectedIndex = 7
      ElseIf IsEqual(MyTrajectoryLineWidth, 2.5) Then
        .SelectedIndex = 8
      Else
        .SelectedIndex = 2
      End If
    End With

    Call UpdateColourViews()
  End Sub



  Private Sub UpdateColourViews()

    btnNormalRUColour.ForeColor = MyNormalRUColour
    btnProjectedRUColour.ForeColor = MyProjectedRUColour
    btnSelectedRUColour.ForeColor = MySelectedRUColour
    btnKey1RUColour.ForeColor = MyKey1RUColour
    btnKey2RUColour.ForeColor = MyKey2RUColour

    btnNormalTrajectoryColour.ForeColor = MyNormalTrajectoryColour
    btnProjectedTrajectoryColour.ForeColor = MyProjectedTrajectoryColour
    btnSelectedTrajectoryColour.ForeColor = MySelectedTrajectoryColour
    btnKey1TrajectoryColour.ForeColor = MyKey1TrajectoryColour
    btnKey2TrajectoryColour.ForeColor = MyKey2TrajectoryColour
  End Sub



  Private Sub SelectColour(ByRef ColourOfWhat As Color)
    Dim _Dialog As ColorDialog

    _Dialog = New ColorDialog
    With _Dialog
      .Color = ColourOfWhat
      .ShowDialog(Me)
      ColourOfWhat = .Color
    End With

    Call UpdateColourViews()
  End Sub



  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    With MyProject.VisualiserOptions
      .NormalRUColour = MyNormalRUColour
      .ProjectedRUColour = MyProjectedRUColour
      .SelectedRUColour = MySelectedRUColour
      .Key1RUColour = MyKey1RUColour
      .LineColourSpeedRoadUserKey1 = MyKey1RUColour
      .Key2RUColour = MyKey2RUColour
      .LineColourSpeedRoadUserKey2 = MyKey2RUColour
      .NormalTrajectoryColour = MyNormalTrajectoryColour
      .ProjectedTrajectoryColour = MyProjectedTrajectoryColour
      .SelectedTrajectoryColour = MySelectedTrajectoryColour
      .Key1TrajectoryColour = MyKey1TrajectoryColour
      .Key2TrajectoryColour = MyKey2TrajectoryColour
      .LineWidthRoadUser = MyRoadUserLineWidth
      .LineWidthTrajectory = MyTrajectoryLineWidth
    End With

    Me.Hide()
  End Sub

  Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    Me.Hide()
  End Sub

  Private Sub btnNormalRU1Colour_Click(sender As Object, e As EventArgs) Handles btnNormalRUColour.Click
    Call SelectColour(MyNormalRUColour)
  End Sub

  Private Sub btnProjectedRUColour_Click(sender As Object, e As EventArgs) Handles btnProjectedRUColour.Click
    Call SelectColour(MyProjectedRUColour)
  End Sub

  Private Sub btnSelectedRUColour_Click(sender As Object, e As EventArgs) Handles btnSelectedRUColour.Click
    Call SelectColour(MySelectedRUColour)
  End Sub
  Private Sub btnKeyRU1Colour_Click(sender As Object, e As EventArgs) Handles btnKey1RUColour.Click
    Call SelectColour(MyKey1RUColour)
  End Sub

  Private Sub btnKeyRU2Colour_Click(sender As Object, e As EventArgs) Handles btnKey2RUColour.Click
    Call SelectColour(MyKey2RUColour)
  End Sub

  Private Sub btnNormalTrajectoryColour_Click(sender As Object, e As EventArgs) Handles btnNormalTrajectoryColour.Click
    Call SelectColour(MyNormalTrajectoryColour)
  End Sub

  Private Sub btnProjectedTrajectoryColour_Click(sender As Object, e As EventArgs) Handles btnProjectedTrajectoryColour.Click
    Call SelectColour(MyProjectedTrajectoryColour)
  End Sub

  Private Sub btnSelectedTrajectoryColour_Click(sender As Object, e As EventArgs) Handles btnSelectedTrajectoryColour.Click
    Call SelectColour(MySelectedTrajectoryColour)
  End Sub

  Private Sub btnKey1TrajectoryColour_Click(sender As Object, e As EventArgs) Handles btnKey1TrajectoryColour.Click
    Call SelectColour(MyKey1TrajectoryColour)
  End Sub

  Private Sub btnKey2TrajectoryColour_Click(sender As Object, e As EventArgs) Handles btnKey2TrajectoryColour.Click
    Call SelectColour(MyKey2TrajectoryColour)
  End Sub


  Private Sub cmbRULineWidth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbRULineWidth.SelectedIndexChanged
    Select Case cmbRULineWidth.SelectedIndex
      Case 0
        MyRoadUserLineWidth = 0.5
      Case 1
        MyRoadUserLineWidth = 0.75
      Case 2
        MyRoadUserLineWidth = 1
      Case 3
        MyRoadUserLineWidth = 1.25
      Case 4
        MyRoadUserLineWidth = 1.5
      Case 5
        MyRoadUserLineWidth = 1.75
      Case 6
        MyRoadUserLineWidth = 2
      Case 7
        MyRoadUserLineWidth = 2.25
      Case 8
        MyRoadUserLineWidth = 2.5
      Case Else
        MyRoadUserLineWidth = 1
    End Select
  End Sub


  Private Sub cmbTrajectoryWidth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTrajectoryWidth.SelectedIndexChanged
    Select Case cmbTrajectoryWidth.SelectedIndex
      Case 0
        MyTrajectoryLineWidth = 0.5
      Case 1
        MyTrajectoryLineWidth = 0.75
      Case 2
        MyTrajectoryLineWidth = 1
      Case 3
        MyTrajectoryLineWidth = 1.25
      Case 4
        MyTrajectoryLineWidth = 1.5
      Case 5
        MyTrajectoryLineWidth = 1.75
      Case 6
        MyTrajectoryLineWidth = 2
      Case 7
        MyTrajectoryLineWidth = 2.25
      Case 8
        MyTrajectoryLineWidth = 2.5
      Case Else
        MyTrajectoryLineWidth = 1
    End Select
  End Sub
End Class