﻿Public Class frmVideoFiles
  Private MyCurrentRecord As clsRecord
  Private MyFramePreview As usrFramePreview
  Private MySelectedVideoFile As clsVideoFile
  Private MyTrixingWithListBox As Boolean = False



  Public WriteOnly Property CurrentRecord As clsRecord
    Set(ByVal value As clsRecord)
      MyCurrentRecord = value
    End Set
  End Property

  Private Sub frmVideoFiles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim VideoFile As clsVideoFile

    lstVideoFiles.Items.Clear()
    For Each VideoFile In MyCurrentRecord.VideoFiles
      lstVideoFiles.Items.Add(VideoFile.VideoFileName)
    Next

    MyFramePreview = New usrFramePreview
    MyFramePreview.Top = 15
    MyFramePreview.Left = 215
    MyFramePreview.Visible = False
    Me.Controls.Add(MyFramePreview)

    If lstVideoFiles.Items.Count > 0 Then lstVideoFiles.SelectedIndex = 0
  End Sub

  Private Sub btnVideoFileAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    With digOpenFile
      .FileName = ""
      .Filter = "*.avi|*.avi"
      .Multiselect = False
      .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
      .ShowDialog(Me)
      If .FileName.Length > 0 Then
        MyCurrentRecord.ImportVideoFile(.FileName)
        lstVideoFiles.Items.Add(MyCurrentRecord.VideoFiles(MyCurrentRecord.VideoFiles.Count - 1).VideoFileName)
      End If
    End With


  End Sub

  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim i As Integer

    For i = 0 To lstVideoFiles.Items.Count - 1
      If MyCurrentRecord.VideoFiles(i).Position <> i Then
        MyCurrentRecord.VideoFiles(i).Position = i
        If MyCurrentRecord.VideoFiles(i).Status = VideoFileStatuses.Unchanged Then _
          MyCurrentRecord.VideoFiles(i).Status = VideoFileStatuses.Updated
      End If
    Next i
    Me.Close()

  End Sub

  Private Sub btnSyncVideoFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim f As frmSyncVideoFiles

    '!!!!!!!!!!!!!!!!!!!!
    Dim _VideoFile1 As Integer = 3
    Dim _VideoFile2 As Integer

    If lstVideoFiles.SelectedIndex <> -1 Then
      _VideoFile2 = lstVideoFiles.SelectedIndex


      f = New frmSyncVideoFiles(MyCurrentRecord.VideoFiles(_VideoFile1), MyCurrentRecord.VideoFiles(_VideoFile2))
      f.ShowDialog(Me)
      If f.SynchronisationOK Then
        MyCurrentRecord.VideoFiles(_VideoFile2).StartTime = f.StartTime
        If MyCurrentRecord.VideoFiles(_VideoFile2).Status <> VideoFileStatuses.Created Then MyCurrentRecord.VideoFiles(_VideoFile2).Status = VideoFileStatuses.Updated
      End If
      f.Dispose()
    End If

  End Sub

  Private Sub lstVideoFiles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If Not MyTrixingWithListBox Then
      If lstVideoFiles.SelectedIndex <> -1 Then
        MySelectedVideoFile = MyCurrentRecord.VideoFiles(lstVideoFiles.SelectedIndex)

        MyFramePreview.VideoFile = MySelectedVideoFile.VideoFilePath
        MyFramePreview.ShowFileInfo = True
        MyFramePreview.Visible = True
        btnVideoFileRemove.Visible = True
        btnLoadCalibration.Visible = True
        btnLoadTimeStamps.Visible = True

        chkVisible.Checked = MySelectedVideoFile.Visible
        chkVisible.Visible = True
        chkCallibrationOK.Checked = MySelectedVideoFile.CallibrationOK
        chkCallibrationOK.Visible = True
        txtVideoFileComment.Text = MySelectedVideoFile.Comment
        txtVideoFileComment.Visible = True
        lblTstart.Visible = True
        txtTstart.Visible = True
        txtTstart.Text = Format(MySelectedVideoFile.StartTime, "0.00")
        If lstVideoFiles.SelectedIndex > 0 Then btnVideoFileUp.Enabled = True Else btnVideoFileUp.Enabled = False
        If IsBetween(lstVideoFiles.SelectedIndex, 0, lstVideoFiles.Items.Count - 2) Then btnVideoFileDown.Enabled = True Else btnVideoFileDown.Enabled = False

        If lstVideoFiles.Items.Count > 1 Then
          btnSyncVideoFiles.Visible = True
          cmbSyncFile2.Items.Clear()
          For Each _VideoFile In MyCurrentRecord.VideoFiles
            If Not MySelectedVideoFile.Equals(_VideoFile) Then cmbSyncFile2.Items.Add(_VideoFile.VideoFileName)
          Next
          cmbSyncFile2.Visible = True
        Else
          btnSyncVideoFiles.Visible = False
          cmbSyncFile2.Visible = False
        End If

      Else
        MySelectedVideoFile = Nothing
        MyFramePreview.Visible = False
        btnVideoFileRemove.Visible = False
        btnLoadCalibration.Visible = False
        btnLoadTimeStamps.Visible = False
        btnSyncVideoFiles.Visible = False
        chkVisible.Visible = False
        chkCallibrationOK.Visible = False
        txtVideoFileComment.Visible = False
        btnVideoFileUp.Enabled = False
        btnVideoFileDown.Enabled = False
        cmbSyncFile2.Items.Clear()
        cmbSyncFile2.Visible = False
        lblTstart.Visible = False
        txtTstart.Visible = False
      End If
    End If

  End Sub

  Private Sub btnVideoFileRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _SelectedPosition As Integer
    Dim _VideoFileTemp As clsVideoFile

    If lstVideoFiles.SelectedIndex <> -1 Then
      If MsgBox("Do you really want to remove this video file from the record?", MsgBoxStyle.YesNo, "Remove video file") = MsgBoxResult.Yes Then
        _SelectedPosition = lstVideoFiles.SelectedIndex

        lstVideoFiles.Items.RemoveAt(_SelectedPosition)

        _VideoFileTemp = MyCurrentRecord.VideoFiles(_SelectedPosition)
        If _VideoFileTemp.Status = VideoFileStatuses.Created Then
          MyCurrentRecord.VideoFiles.RemoveAt(_SelectedPosition)
        Else
          MyCurrentRecord.VideoFiles.Remove(_VideoFileTemp)
          _VideoFileTemp.Status = VideoFileStatuses.Deleted
          MyCurrentRecord.VideoFiles.Add(_VideoFileTemp)
        End If
      End If
    End If
  End Sub

  Private Sub txtVideoFileComment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If MySelectedVideoFile IsNot Nothing Then
      If MySelectedVideoFile.Comment <> txtVideoFileComment.Text Then
        MySelectedVideoFile.Comment = txtVideoFileComment.Text
        If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then _
          MySelectedVideoFile.Status = VideoFileStatuses.Updated
      End If
    End If
  End Sub

  Private Sub chkVisible_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If MySelectedVideoFile IsNot Nothing Then
      If MySelectedVideoFile.Visible <> chkVisible.Checked Then
        MySelectedVideoFile.Visible = chkVisible.Checked
        If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then _
          MySelectedVideoFile.Status = VideoFileStatuses.Updated
      End If
    End If
  End Sub

  Private Sub btnVideoFileUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _StringTemp As String
    Dim _SelectedPosition As Integer

    If IsBetween(lstVideoFiles.SelectedIndex, 1, lstVideoFiles.Items.Count - 1) Then
      _SelectedPosition = lstVideoFiles.SelectedIndex
      MyCurrentRecord.VideoFiles.RemoveAt(_SelectedPosition)
      MyCurrentRecord.VideoFiles.Insert(_SelectedPosition - 1, MySelectedVideoFile)

      MyTrixingWithListBox = True
      _StringTemp = lstVideoFiles.SelectedItem.ToString
      lstVideoFiles.Items.RemoveAt(_SelectedPosition)
      lstVideoFiles.Items.Insert(_SelectedPosition - 1, _StringTemp)
      lstVideoFiles.SelectedIndex = _SelectedPosition - 1
      MyTrixingWithListBox = False
    End If
  End Sub

  Private Sub btnVideoFileDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _StringTemp As String
    Dim _SelectedPosition As Integer

    If IsBetween(lstVideoFiles.SelectedIndex, 0, lstVideoFiles.Items.Count - 2) Then
      _SelectedPosition = lstVideoFiles.SelectedIndex
      MyCurrentRecord.VideoFiles.RemoveAt(_SelectedPosition)
      MyCurrentRecord.VideoFiles.Insert(_SelectedPosition + 1, MySelectedVideoFile)

      MyTrixingWithListBox = True
      _StringTemp = lstVideoFiles.SelectedItem.ToString
      lstVideoFiles.Items.RemoveAt(_SelectedPosition)
      lstVideoFiles.Items.Insert(_SelectedPosition + 1, _StringTemp)
      lstVideoFiles.SelectedIndex = _SelectedPosition + 1
      MyTrixingWithListBox = False
    End If
  End Sub

  Private Sub btnLoadTimeStamps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If MySelectedVideoFile IsNot Nothing Then
      With digOpenFile
        .FileName = ""
        .Filter = "Time stamps files (*.tatst)|*.tatst"
        .Multiselect = False
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        .ShowDialog(Me)
        If .FileName.Length > 0 Then
          If MySelectedVideoFile.ImportTimeStamps(.FileName) Then
            If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then MySelectedVideoFile.Status = VideoFileStatuses.Updated
          End If
        End If
      End With
    End If
  End Sub


  Private Sub btnLoadCalibration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If MySelectedVideoFile IsNot Nothing Then
      With digOpenFile
        .FileName = ""
        .Filter = "Calibration files (*.tacal)|*.tacal"
        .Multiselect = False
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        .ShowDialog(Me)
        If .FileName.Length > 0 Then
          If MySelectedVideoFile.ImportCallibration(.FileName) Then
            If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then MySelectedVideoFile.Status = VideoFileStatuses.Updated
            chkCallibrationOK.Checked = True
          Else
            chkCallibrationOK.Checked = False
          End If
        End If
      End With
    End If
  End Sub

  Private Sub lblBackground_Click(sender As Object, e As EventArgs) Handles lblMap.Click

  End Sub
End Class