﻿Imports System.ComponentModel

Public Class frmMedia
  Private MyMediaPath As String


  Public Property MediaPath As String
    Get
      Return MyMediaPath
    End Get
    Set(value As String)
      Dim _OldBitmap, _NewBitmaps As Bitmap
      Dim _g As Graphics

      MyMediaPath = value

      If File.Exists(MediaPath) Then
        _OldBitmap = New Bitmap(MediaPath)
        _NewBitmaps = New Bitmap(CInt(_OldBitmap.Width / 2), CInt(_OldBitmap.Height / 2))
        _g = Graphics.FromImage(_NewBitmaps)

        _g.DrawImage(_OldBitmap, 0, 0, _NewBitmaps.Width, _NewBitmaps.Height)
        picMedia.Image = _NewBitmaps
        picMedia.Width = _NewBitmaps.Width
        picMedia.Height = _NewBitmaps.Height
        Me.Width = picMedia.Right + 5
        Me.Height = picMedia.Height + 30
        _OldBitmap.Dispose()
        _g.Dispose()
      Else
        picMedia.Image = Nothing
      End If
      Me.Text = Path.GetFileNameWithoutExtension(MyMediaPath)

    End Set
  End Property


  Private Sub frmMedia_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
    frmMain.MediaFormShown = False

    Me.Hide()
    e.Cancel = True
  End Sub
End Class