﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportDataRuba
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.txtVideoFileList = New System.Windows.Forms.TextBox()
    Me.btnSelectVideoFiles = New System.Windows.Forms.Button()
    Me.btnSelectCalibration = New System.Windows.Forms.Button()
    Me.txtCalibrationFile = New System.Windows.Forms.TextBox()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.btnClearVideoFiles = New System.Windows.Forms.Button()
    Me.btnClearCalibration = New System.Windows.Forms.Button()
    Me.lblCalibration = New System.Windows.Forms.Label()
    Me.lblVideoFiles = New System.Windows.Forms.Label()
    Me.grpFrameRate = New System.Windows.Forms.GroupBox()
    Me.txtFPS = New System.Windows.Forms.TextBox()
    Me.rbtSetFPSTo = New System.Windows.Forms.RadioButton()
    Me.rbtUseVideoFPS = New System.Windows.Forms.RadioButton()
    Me.lblMap = New System.Windows.Forms.Label()
    Me.btnClearMap = New System.Windows.Forms.Button()
    Me.txtMapFile = New System.Windows.Forms.TextBox()
    Me.btnSelectMap = New System.Windows.Forms.Button()
    Me.lblMediaFiles = New System.Windows.Forms.Label()
    Me.txtMediaFileList = New System.Windows.Forms.TextBox()
    Me.lblRubaFile = New System.Windows.Forms.Label()
    Me.btnClearRubaFile = New System.Windows.Forms.Button()
    Me.txtRubaOutputFile = New System.Windows.Forms.TextBox()
    Me.btnSelectRubaFile = New System.Windows.Forms.Button()
    Me.bntClearMediaFiles = New System.Windows.Forms.Button()
    Me.btnSelectMediaFiles = New System.Windows.Forms.Button()
    Me.grpFrameRate.SuspendLayout()
    Me.SuspendLayout()
    '
    'txtVideoFileList
    '
    Me.txtVideoFileList.Enabled = False
    Me.txtVideoFileList.Location = New System.Drawing.Point(3, 116)
    Me.txtVideoFileList.Multiline = True
    Me.txtVideoFileList.Name = "txtVideoFileList"
    Me.txtVideoFileList.Size = New System.Drawing.Size(276, 98)
    Me.txtVideoFileList.TabIndex = 0
    '
    'btnSelectVideoFiles
    '
    Me.btnSelectVideoFiles.Location = New System.Drawing.Point(285, 116)
    Me.btnSelectVideoFiles.Name = "btnSelectVideoFiles"
    Me.btnSelectVideoFiles.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectVideoFiles.TabIndex = 1
    Me.btnSelectVideoFiles.Text = "Select"
    Me.btnSelectVideoFiles.UseVisualStyleBackColor = True
    '
    'btnSelectCalibration
    '
    Me.btnSelectCalibration.Location = New System.Drawing.Point(751, 114)
    Me.btnSelectCalibration.Name = "btnSelectCalibration"
    Me.btnSelectCalibration.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectCalibration.TabIndex = 2
    Me.btnSelectCalibration.Text = "Select"
    Me.btnSelectCalibration.UseVisualStyleBackColor = True
    '
    'txtCalibrationFile
    '
    Me.txtCalibrationFile.Enabled = False
    Me.txtCalibrationFile.Location = New System.Drawing.Point(469, 116)
    Me.txtCalibrationFile.Multiline = True
    Me.txtCalibrationFile.Name = "txtCalibrationFile"
    Me.txtCalibrationFile.Size = New System.Drawing.Size(276, 49)
    Me.txtCalibrationFile.TabIndex = 3
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(751, 331)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'btnOK
    '
    Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnOK.Location = New System.Drawing.Point(685, 331)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 8
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnClearVideoFiles
    '
    Me.btnClearVideoFiles.Location = New System.Drawing.Point(285, 143)
    Me.btnClearVideoFiles.Name = "btnClearVideoFiles"
    Me.btnClearVideoFiles.Size = New System.Drawing.Size(60, 22)
    Me.btnClearVideoFiles.TabIndex = 9
    Me.btnClearVideoFiles.Text = "Clear"
    Me.btnClearVideoFiles.UseVisualStyleBackColor = True
    '
    'btnClearCalibration
    '
    Me.btnClearCalibration.Location = New System.Drawing.Point(751, 143)
    Me.btnClearCalibration.Name = "btnClearCalibration"
    Me.btnClearCalibration.Size = New System.Drawing.Size(60, 22)
    Me.btnClearCalibration.TabIndex = 10
    Me.btnClearCalibration.Text = "Clear"
    Me.btnClearCalibration.UseVisualStyleBackColor = True
    '
    'lblCalibration
    '
    Me.lblCalibration.AutoSize = True
    Me.lblCalibration.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblCalibration.Location = New System.Drawing.Point(466, 96)
    Me.lblCalibration.Name = "lblCalibration"
    Me.lblCalibration.Size = New System.Drawing.Size(71, 13)
    Me.lblCalibration.TabIndex = 11
    Me.lblCalibration.Text = "Calibration:"
    '
    'lblVideoFiles
    '
    Me.lblVideoFiles.AutoSize = True
    Me.lblVideoFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoFiles.Location = New System.Drawing.Point(3, 96)
    Me.lblVideoFiles.Name = "lblVideoFiles"
    Me.lblVideoFiles.Size = New System.Drawing.Size(70, 13)
    Me.lblVideoFiles.TabIndex = 12
    Me.lblVideoFiles.Text = "Video files:"
    '
    'grpFrameRate
    '
    Me.grpFrameRate.Controls.Add(Me.txtFPS)
    Me.grpFrameRate.Controls.Add(Me.rbtSetFPSTo)
    Me.grpFrameRate.Controls.Add(Me.rbtUseVideoFPS)
    Me.grpFrameRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.grpFrameRate.Location = New System.Drawing.Point(469, 6)
    Me.grpFrameRate.Name = "grpFrameRate"
    Me.grpFrameRate.Size = New System.Drawing.Size(276, 69)
    Me.grpFrameRate.TabIndex = 14
    Me.grpFrameRate.TabStop = False
    Me.grpFrameRate.Text = "Data points / s"
    '
    'txtFPS
    '
    Me.txtFPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtFPS.Location = New System.Drawing.Point(216, 30)
    Me.txtFPS.Name = "txtFPS"
    Me.txtFPS.Size = New System.Drawing.Size(51, 20)
    Me.txtFPS.TabIndex = 2
    Me.txtFPS.Text = "15"
    '
    'rbtSetFPSTo
    '
    Me.rbtSetFPSTo.AutoSize = True
    Me.rbtSetFPSTo.Checked = True
    Me.rbtSetFPSTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rbtSetFPSTo.Location = New System.Drawing.Point(141, 31)
    Me.rbtSetFPSTo.Name = "rbtSetFPSTo"
    Me.rbtSetFPSTo.Size = New System.Drawing.Size(69, 17)
    Me.rbtSetFPSTo.TabIndex = 1
    Me.rbtSetFPSTo.TabStop = True
    Me.rbtSetFPSTo.Text = "Set all to:"
    Me.rbtSetFPSTo.UseVisualStyleBackColor = True
    '
    'rbtUseVideoFPS
    '
    Me.rbtUseVideoFPS.AutoSize = True
    Me.rbtUseVideoFPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rbtUseVideoFPS.Location = New System.Drawing.Point(6, 31)
    Me.rbtUseVideoFPS.Name = "rbtUseVideoFPS"
    Me.rbtUseVideoFPS.Size = New System.Drawing.Size(90, 17)
    Me.rbtUseVideoFPS.TabIndex = 0
    Me.rbtUseVideoFPS.Text = "Use video fps"
    Me.rbtUseVideoFPS.UseVisualStyleBackColor = True
    '
    'lblMap
    '
    Me.lblMap.AutoSize = True
    Me.lblMap.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMap.Location = New System.Drawing.Point(466, 186)
    Me.lblMap.Name = "lblMap"
    Me.lblMap.Size = New System.Drawing.Size(35, 13)
    Me.lblMap.TabIndex = 18
    Me.lblMap.Text = "Map:"
    '
    'btnClearMap
    '
    Me.btnClearMap.Location = New System.Drawing.Point(751, 233)
    Me.btnClearMap.Name = "btnClearMap"
    Me.btnClearMap.Size = New System.Drawing.Size(60, 22)
    Me.btnClearMap.TabIndex = 17
    Me.btnClearMap.Text = "Clear"
    Me.btnClearMap.UseVisualStyleBackColor = True
    '
    'txtMapFile
    '
    Me.txtMapFile.Enabled = False
    Me.txtMapFile.Location = New System.Drawing.Point(469, 206)
    Me.txtMapFile.Multiline = True
    Me.txtMapFile.Name = "txtMapFile"
    Me.txtMapFile.Size = New System.Drawing.Size(276, 49)
    Me.txtMapFile.TabIndex = 16
    '
    'btnSelectMap
    '
    Me.btnSelectMap.Location = New System.Drawing.Point(751, 205)
    Me.btnSelectMap.Name = "btnSelectMap"
    Me.btnSelectMap.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectMap.TabIndex = 15
    Me.btnSelectMap.Text = "Select"
    Me.btnSelectMap.UseVisualStyleBackColor = True
    '
    'lblMediaFiles
    '
    Me.lblMediaFiles.AutoSize = True
    Me.lblMediaFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMediaFiles.Location = New System.Drawing.Point(3, 235)
    Me.lblMediaFiles.Name = "lblMediaFiles"
    Me.lblMediaFiles.Size = New System.Drawing.Size(72, 13)
    Me.lblMediaFiles.TabIndex = 20
    Me.lblMediaFiles.Text = "Media files:"
    '
    'txtMediaFileList
    '
    Me.txtMediaFileList.Enabled = False
    Me.txtMediaFileList.Location = New System.Drawing.Point(3, 255)
    Me.txtMediaFileList.Multiline = True
    Me.txtMediaFileList.Name = "txtMediaFileList"
    Me.txtMediaFileList.Size = New System.Drawing.Size(276, 98)
    Me.txtMediaFileList.TabIndex = 19
    '
    'lblRubaFile
    '
    Me.lblRubaFile.AutoSize = True
    Me.lblRubaFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblRubaFile.Location = New System.Drawing.Point(3, 6)
    Me.lblRubaFile.Name = "lblRubaFile"
    Me.lblRubaFile.Size = New System.Drawing.Size(90, 13)
    Me.lblRubaFile.TabIndex = 24
    Me.lblRubaFile.Text = "Ruba .csv file:"
    '
    'btnClearRubaFile
    '
    Me.btnClearRubaFile.Location = New System.Drawing.Point(285, 53)
    Me.btnClearRubaFile.Name = "btnClearRubaFile"
    Me.btnClearRubaFile.Size = New System.Drawing.Size(60, 22)
    Me.btnClearRubaFile.TabIndex = 23
    Me.btnClearRubaFile.Text = "Clear"
    Me.btnClearRubaFile.UseVisualStyleBackColor = True
    '
    'txtRubaOutputFile
    '
    Me.txtRubaOutputFile.Enabled = False
    Me.txtRubaOutputFile.Location = New System.Drawing.Point(3, 26)
    Me.txtRubaOutputFile.Multiline = True
    Me.txtRubaOutputFile.Name = "txtRubaOutputFile"
    Me.txtRubaOutputFile.Size = New System.Drawing.Size(276, 49)
    Me.txtRubaOutputFile.TabIndex = 22
    '
    'btnSelectRubaFile
    '
    Me.btnSelectRubaFile.Location = New System.Drawing.Point(285, 26)
    Me.btnSelectRubaFile.Name = "btnSelectRubaFile"
    Me.btnSelectRubaFile.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectRubaFile.TabIndex = 21
    Me.btnSelectRubaFile.Text = "Select"
    Me.btnSelectRubaFile.UseVisualStyleBackColor = True
    '
    'bntClearMediaFiles
    '
    Me.bntClearMediaFiles.Location = New System.Drawing.Point(285, 282)
    Me.bntClearMediaFiles.Name = "bntClearMediaFiles"
    Me.bntClearMediaFiles.Size = New System.Drawing.Size(60, 22)
    Me.bntClearMediaFiles.TabIndex = 26
    Me.bntClearMediaFiles.Text = "Clear"
    Me.bntClearMediaFiles.UseVisualStyleBackColor = True
    '
    'btnSelectMediaFiles
    '
    Me.btnSelectMediaFiles.Location = New System.Drawing.Point(285, 255)
    Me.btnSelectMediaFiles.Name = "btnSelectMediaFiles"
    Me.btnSelectMediaFiles.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectMediaFiles.TabIndex = 25
    Me.btnSelectMediaFiles.Text = "Select"
    Me.btnSelectMediaFiles.UseVisualStyleBackColor = True
    '
    'frmImportRuba
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(816, 360)
    Me.ControlBox = False
    Me.Controls.Add(Me.bntClearMediaFiles)
    Me.Controls.Add(Me.btnSelectMediaFiles)
    Me.Controls.Add(Me.lblRubaFile)
    Me.Controls.Add(Me.btnClearRubaFile)
    Me.Controls.Add(Me.txtRubaOutputFile)
    Me.Controls.Add(Me.btnSelectRubaFile)
    Me.Controls.Add(Me.lblMediaFiles)
    Me.Controls.Add(Me.txtMediaFileList)
    Me.Controls.Add(Me.lblMap)
    Me.Controls.Add(Me.btnClearMap)
    Me.Controls.Add(Me.txtMapFile)
    Me.Controls.Add(Me.btnSelectMap)
    Me.Controls.Add(Me.grpFrameRate)
    Me.Controls.Add(Me.lblVideoFiles)
    Me.Controls.Add(Me.lblCalibration)
    Me.Controls.Add(Me.btnClearCalibration)
    Me.Controls.Add(Me.btnClearVideoFiles)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.Controls.Add(Me.txtCalibrationFile)
    Me.Controls.Add(Me.btnSelectCalibration)
    Me.Controls.Add(Me.btnSelectVideoFiles)
    Me.Controls.Add(Me.txtVideoFileList)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmImportRuba"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "Import from Ruba"
    Me.grpFrameRate.ResumeLayout(False)
    Me.grpFrameRate.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txtVideoFileList As System.Windows.Forms.TextBox
  Friend WithEvents btnSelectVideoFiles As System.Windows.Forms.Button
  Friend WithEvents btnSelectCalibration As System.Windows.Forms.Button
  Friend WithEvents txtCalibrationFile As System.Windows.Forms.TextBox
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnClearVideoFiles As System.Windows.Forms.Button
  Friend WithEvents btnClearCalibration As System.Windows.Forms.Button
  Friend WithEvents lblCalibration As System.Windows.Forms.Label
  Friend WithEvents lblVideoFiles As System.Windows.Forms.Label
  Friend WithEvents grpFrameRate As System.Windows.Forms.GroupBox
  Friend WithEvents rbtSetFPSTo As System.Windows.Forms.RadioButton
  Friend WithEvents rbtUseVideoFPS As System.Windows.Forms.RadioButton
  Friend WithEvents txtFPS As System.Windows.Forms.TextBox
  Friend WithEvents lblMap As System.Windows.Forms.Label
  Friend WithEvents btnClearMap As System.Windows.Forms.Button
  Friend WithEvents txtMapFile As System.Windows.Forms.TextBox
  Friend WithEvents btnSelectMap As System.Windows.Forms.Button
  Friend WithEvents lblMediaFiles As Label
  Friend WithEvents txtMediaFileList As TextBox
  Friend WithEvents lblRubaFile As Label
  Friend WithEvents btnClearRubaFile As Button
  Friend WithEvents txtRubaOutputFile As TextBox
  Friend WithEvents btnSelectRubaFile As Button
  Friend WithEvents bntClearMediaFiles As Button
  Friend WithEvents btnSelectMediaFiles As Button
End Class
