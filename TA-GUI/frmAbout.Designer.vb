﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbout))
    Me.PictureBox2 = New System.Windows.Forms.PictureBox()
    Me.lblLTH = New System.Windows.Forms.Label()
    Me.llbTFT = New System.Windows.Forms.LinkLabel()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.lblVersion = New System.Windows.Forms.Label()
    Me.llbSupportMail = New System.Windows.Forms.LinkLabel()
    Me.lblSupport = New System.Windows.Forms.Label()
    CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'PictureBox2
    '
    Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
    Me.PictureBox2.Location = New System.Drawing.Point(12, 25)
    Me.PictureBox2.Name = "PictureBox2"
    Me.PictureBox2.Size = New System.Drawing.Size(75, 100)
    Me.PictureBox2.TabIndex = 1
    Me.PictureBox2.TabStop = False
    '
    'lblLTH
    '
    Me.lblLTH.AutoSize = True
    Me.lblLTH.Location = New System.Drawing.Point(9, 139)
    Me.lblLTH.Name = "lblLTH"
    Me.lblLTH.Size = New System.Drawing.Size(192, 39)
    Me.lblLTH.TabIndex = 3
    Me.lblLTH.Text = "Faculty of Engineering, LTH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Department of Technology and Society" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Transport and " &
    "Roads"
    Me.lblLTH.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'llbTFT
    '
    Me.llbTFT.AutoSize = True
    Me.llbTFT.Location = New System.Drawing.Point(12, 195)
    Me.llbTFT.Name = "llbTFT"
    Me.llbTFT.Size = New System.Drawing.Size(147, 13)
    Me.llbTFT.TabIndex = 4
    Me.llbTFT.TabStop = True
    Me.llbTFT.Text = "http://www.tft.lth.se/software"
    Me.llbTFT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(224, 278)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 6
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'lblVersion
    '
    Me.lblVersion.AutoSize = True
    Me.lblVersion.Location = New System.Drawing.Point(12, 221)
    Me.lblVersion.Name = "lblVersion"
    Me.lblVersion.Size = New System.Drawing.Size(81, 13)
    Me.lblVersion.TabIndex = 7
    Me.lblVersion.Text = "Version: 4.0.0.0"
    Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'llbSupportMail
    '
    Me.llbSupportMail.AutoSize = True
    Me.llbSupportMail.Location = New System.Drawing.Point(103, 243)
    Me.llbSupportMail.Name = "llbSupportMail"
    Me.llbSupportMail.Size = New System.Drawing.Size(138, 13)
    Me.llbSupportMail.TabIndex = 9
    Me.llbSupportMail.TabStop = True
    Me.llbSupportMail.Text = "aliaksei.laureshyn@tft.lth.se"
    '
    'lblSupport
    '
    Me.lblSupport.AutoSize = True
    Me.lblSupport.Location = New System.Drawing.Point(12, 243)
    Me.lblSupport.Name = "lblSupport"
    Me.lblSupport.Size = New System.Drawing.Size(85, 13)
    Me.lblSupport.TabIndex = 8
    Me.lblSupport.Text = "Support (limited):"
    '
    'frmAbout
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(291, 312)
    Me.ControlBox = False
    Me.Controls.Add(Me.llbSupportMail)
    Me.Controls.Add(Me.lblSupport)
    Me.Controls.Add(Me.lblVersion)
    Me.Controls.Add(Me.btnOK)
    Me.Controls.Add(Me.llbTFT)
    Me.Controls.Add(Me.lblLTH)
    Me.Controls.Add(Me.PictureBox2)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frmAbout"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "About"
    CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
  Friend WithEvents lblLTH As System.Windows.Forms.Label
  Friend WithEvents llbTFT As System.Windows.Forms.LinkLabel
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents lblVersion As System.Windows.Forms.Label
  Friend WithEvents llbSupportMail As System.Windows.Forms.LinkLabel
  Friend WithEvents lblSupport As System.Windows.Forms.Label
End Class
