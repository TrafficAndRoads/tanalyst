﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrFramePreview
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.picFrame = New System.Windows.Forms.PictureBox()
    Me.lblFileInfo = New System.Windows.Forms.Label()
    CType(Me.picFrame, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'picFrame
    '
    Me.picFrame.Location = New System.Drawing.Point(0, 0)
    Me.picFrame.Name = "picFrame"
    Me.picFrame.Size = New System.Drawing.Size(100, 75)
    Me.picFrame.TabIndex = 0
    Me.picFrame.TabStop = False
    '
    'lblFileInfo
    '
    Me.lblFileInfo.AutoSize = True
    Me.lblFileInfo.Location = New System.Drawing.Point(3, 78)
    Me.lblFileInfo.Name = "lblFileInfo"
    Me.lblFileInfo.Size = New System.Drawing.Size(40, 13)
    Me.lblFileInfo.TabIndex = 1
    Me.lblFileInfo.Text = "file info" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
    '
    'usrFramePreview
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.lblFileInfo)
    Me.Controls.Add(Me.picFrame)
    Me.Name = "usrFramePreview"
    Me.Size = New System.Drawing.Size(100, 108)
    CType(Me.picFrame, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents picFrame As System.Windows.Forms.PictureBox
  Friend WithEvents lblFileInfo As System.Windows.Forms.Label

End Class
