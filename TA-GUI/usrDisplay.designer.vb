﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class usrDisplay
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(usrDisplay))
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
        Me.btnNext = New System.Windows.Forms.Button()
        Me.tmrFrames = New System.Windows.Forms.Timer(Me.components)
        Me.chkPlay = New System.Windows.Forms.CheckBox()
        Me.lblCurrentFrame = New System.Windows.Forms.Label()
        Me.tltToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnJumpForward = New System.Windows.Forms.Button()
        Me.bntJumpBack = New System.Windows.Forms.Button()
        Me.trbFrames = New System.Windows.Forms.TrackBar()
        Me.lblTotalTime = New System.Windows.Forms.Label()
        Me.lblCurrentTime = New System.Windows.Forms.Label()
        Me.mnuContext = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCopyImage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSaveImage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSaveVideoSequence = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExportAsGroundTruth = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportClickedPositionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.digSave = New System.Windows.Forms.SaveFileDialog()
        Me.digOpen = New System.Windows.Forms.OpenFileDialog()
        Me.lblYpxl = New System.Windows.Forms.Label()
        Me.pnlPlay = New System.Windows.Forms.Panel()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btnSpeedFactor = New System.Windows.Forms.Button()
        Me.mnuSpeedFactor = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSpeedFactor_x1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeedFactor_x10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnlEdit = New System.Windows.Forms.Panel()
        Me.lblZpxl = New System.Windows.Forms.Label()
        Me.btnSmoothStraight = New System.Windows.Forms.Button()
        Me.btnSmooth = New System.Windows.Forms.Button()
        Me.btnCutEnd = New System.Windows.Forms.Button()
        Me.btnCutStart = New System.Windows.Forms.Button()
        Me.lblXpxl = New System.Windows.Forms.Label()
        Me.pnlNavigation = New System.Windows.Forms.Panel()
        Me.picDisplay = New System.Windows.Forms.PictureBox()
        Me.chkGraph = New System.Windows.Forms.CheckBox()
        Me.pnlSpeed = New System.Windows.Forms.Panel()
        Me.mnuSecondsBefore = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.trbFrames, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuContext.SuspendLayout()
        Me.pnlPlay.SuspendLayout()
        Me.mnuSpeedFactor.SuspendLayout()
        Me.pnlEdit.SuspendLayout()
        Me.pnlNavigation.SuspendLayout()
        CType(Me.picDisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSpeed.SuspendLayout()
        Me.mnuSecondsBefore.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPrevious
        '
        Me.btnPrevious.Location = New System.Drawing.Point(40, 3)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(40, 40)
        Me.btnPrevious.TabIndex = 2
        Me.btnPrevious.Text = "<"
        Me.tltToolTip.SetToolTip(Me.btnPrevious, "Go to previous frame")
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.ImageKey = "Stop"
        Me.btnStop.ImageList = Me.imlButtons
        Me.btnStop.Location = New System.Drawing.Point(52, 11)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(30, 25)
        Me.btnStop.TabIndex = 3
        Me.tltToolTip.SetToolTip(Me.btnStop, "Stop")
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'imlButtons
        '
        Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlButtons.TransparentColor = System.Drawing.Color.White
        Me.imlButtons.Images.SetKeyName(0, "Play")
        Me.imlButtons.Images.SetKeyName(1, "Stop")
        Me.imlButtons.Images.SetKeyName(2, "Pause")
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(145, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(40, 40)
        Me.btnNext.TabIndex = 5
        Me.btnNext.Text = ">"
        Me.tltToolTip.SetToolTip(Me.btnNext, "Go to next frame")
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'tmrFrames
        '
        '
        'chkPlay
        '
        Me.chkPlay.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkPlay.ImageKey = "Play"
        Me.chkPlay.ImageList = Me.imlButtons
        Me.chkPlay.Location = New System.Drawing.Point(3, 7)
        Me.chkPlay.Name = "chkPlay"
        Me.chkPlay.Size = New System.Drawing.Size(50, 32)
        Me.chkPlay.TabIndex = 6
        Me.chkPlay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tltToolTip.SetToolTip(Me.chkPlay, "Play")
        Me.chkPlay.UseVisualStyleBackColor = True
        '
        'lblCurrentFrame
        '
        Me.lblCurrentFrame.AutoSize = True
        Me.lblCurrentFrame.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentFrame.Location = New System.Drawing.Point(85, 11)
        Me.lblCurrentFrame.Name = "lblCurrentFrame"
        Me.lblCurrentFrame.Size = New System.Drawing.Size(54, 20)
        Me.lblCurrentFrame.TabIndex = 8
        Me.lblCurrentFrame.Text = "00000"
        Me.lblCurrentFrame.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tltToolTip.SetToolTip(Me.lblCurrentFrame, "Current frame")
        '
        'btnJumpForward
        '
        Me.btnJumpForward.Location = New System.Drawing.Point(185, 11)
        Me.btnJumpForward.Name = "btnJumpForward"
        Me.btnJumpForward.Size = New System.Drawing.Size(30, 25)
        Me.btnJumpForward.TabIndex = 13
        Me.btnJumpForward.Text = ">>"
        Me.tltToolTip.SetToolTip(Me.btnJumpForward, "Jump forward")
        Me.btnJumpForward.UseVisualStyleBackColor = True
        '
        'bntJumpBack
        '
        Me.bntJumpBack.Location = New System.Drawing.Point(10, 10)
        Me.bntJumpBack.Name = "bntJumpBack"
        Me.bntJumpBack.Size = New System.Drawing.Size(30, 25)
        Me.bntJumpBack.TabIndex = 14
        Me.bntJumpBack.Text = "<<"
        Me.tltToolTip.SetToolTip(Me.bntJumpBack, "Jump backwards")
        Me.bntJumpBack.UseVisualStyleBackColor = True
        '
        'trbFrames
        '
        Me.trbFrames.AutoSize = False
        Me.trbFrames.Location = New System.Drawing.Point(3, 248)
        Me.trbFrames.Maximum = 200
        Me.trbFrames.Name = "trbFrames"
        Me.trbFrames.Size = New System.Drawing.Size(221, 15)
        Me.trbFrames.TabIndex = 10
        Me.trbFrames.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'lblTotalTime
        '
        Me.lblTotalTime.AutoSize = True
        Me.lblTotalTime.Location = New System.Drawing.Point(181, 266)
        Me.lblTotalTime.Name = "lblTotalTime"
        Me.lblTotalTime.Size = New System.Drawing.Size(43, 13)
        Me.lblTotalTime.TabIndex = 11
        Me.lblTotalTime.Text = "0:00:00"
        '
        'lblCurrentTime
        '
        Me.lblCurrentTime.AutoSize = True
        Me.lblCurrentTime.Location = New System.Drawing.Point(0, 266)
        Me.lblCurrentTime.Name = "lblCurrentTime"
        Me.lblCurrentTime.Size = New System.Drawing.Size(43, 13)
        Me.lblCurrentTime.TabIndex = 12
        Me.lblCurrentTime.Text = "0:00:00"
        '
        'mnuContext
        '
        Me.mnuContext.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCopyImage, Me.mnuSaveImage, Me.mnuSeparator1, Me.mnuSaveVideoSequence, Me.ToolStripSeparator1, Me.mnuExportAsGroundTruth, Me.ExportClickedPositionsToolStripMenuItem})
        Me.mnuContext.Name = "mnuContext"
        Me.mnuContext.ShowImageMargin = False
        Me.mnuContext.Size = New System.Drawing.Size(175, 126)
        '
        'mnuCopyImage
        '
        Me.mnuCopyImage.Name = "mnuCopyImage"
        Me.mnuCopyImage.Size = New System.Drawing.Size(174, 22)
        Me.mnuCopyImage.Text = "Copy image"
        '
        'mnuSaveImage
        '
        Me.mnuSaveImage.Name = "mnuSaveImage"
        Me.mnuSaveImage.Size = New System.Drawing.Size(174, 22)
        Me.mnuSaveImage.Text = "Save image"
        '
        'mnuSeparator1
        '
        Me.mnuSeparator1.Name = "mnuSeparator1"
        Me.mnuSeparator1.Size = New System.Drawing.Size(171, 6)
        '
        'mnuSaveVideoSequence
        '
        Me.mnuSaveVideoSequence.Name = "mnuSaveVideoSequence"
        Me.mnuSaveVideoSequence.Size = New System.Drawing.Size(174, 22)
        Me.mnuSaveVideoSequence.Text = "Save video sequence"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(171, 6)
        '
        'mnuExportAsGroundTruth
        '
        Me.mnuExportAsGroundTruth.Name = "mnuExportAsGroundTruth"
        Me.mnuExportAsGroundTruth.Size = New System.Drawing.Size(174, 22)
        Me.mnuExportAsGroundTruth.Text = "Export as ground truth"
        '
        'ExportClickedPositionsToolStripMenuItem
        '
        Me.ExportClickedPositionsToolStripMenuItem.Name = "ExportClickedPositionsToolStripMenuItem"
        Me.ExportClickedPositionsToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ExportClickedPositionsToolStripMenuItem.Text = "Export clicked positions"
        '
        'digOpen
        '
        Me.digOpen.FileName = "OpenFileDialog1"
        '
        'lblYpxl
        '
        Me.lblYpxl.AutoSize = True
        Me.lblYpxl.Location = New System.Drawing.Point(9, 19)
        Me.lblYpxl.Name = "lblYpxl"
        Me.lblYpxl.Size = New System.Drawing.Size(58, 13)
        Me.lblYpxl.TabIndex = 16
        Me.lblYpxl.Text = "Y: 34,65 m"
        '
        'pnlPlay
        '
        Me.pnlPlay.Controls.Add(Me.chkPlay)
        Me.pnlPlay.Controls.Add(Me.btnStop)
        Me.pnlPlay.Controls.Add(Me.btnExport)
        Me.pnlPlay.Location = New System.Drawing.Point(298, 0)
        Me.pnlPlay.Name = "pnlPlay"
        Me.pnlPlay.Size = New System.Drawing.Size(190, 52)
        Me.pnlPlay.TabIndex = 19
        '
        'btnExport
        '
        Me.btnExport.ContextMenuStrip = Me.mnuSecondsBefore
        Me.btnExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.Location = New System.Drawing.Point(125, 1)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(62, 43)
        Me.btnExport.TabIndex = 23
        Me.btnExport.Text = "Export event"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnSpeedFactor
        '
        Me.btnSpeedFactor.ContextMenuStrip = Me.mnuSpeedFactor
        Me.btnSpeedFactor.ImageKey = "(none)"
        Me.btnSpeedFactor.Location = New System.Drawing.Point(3, 9)
        Me.btnSpeedFactor.Name = "btnSpeedFactor"
        Me.btnSpeedFactor.Size = New System.Drawing.Size(30, 25)
        Me.btnSpeedFactor.TabIndex = 15
        Me.btnSpeedFactor.Text = "x1"
        Me.btnSpeedFactor.UseVisualStyleBackColor = True
        '
        'mnuSpeedFactor
        '
        Me.mnuSpeedFactor.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeedFactor_x1, Me.mnuSpeedFactor_x2, Me.mnuSpeedFactor_x3, Me.mnuSpeedFactor_x4, Me.mnuSpeedFactor_x5, Me.mnuSpeedFactor_x6, Me.mnuSpeedFactor_x7, Me.mnuSpeedFactor_x8, Me.mnuSpeedFactor_x9, Me.mnuSpeedFactor_x10})
        Me.mnuSpeedFactor.Name = "mnuSpeedFactor"
        Me.mnuSpeedFactor.Size = New System.Drawing.Size(93, 224)
        '
        'mnuSpeedFactor_x1
        '
        Me.mnuSpeedFactor_x1.Name = "mnuSpeedFactor_x1"
        Me.mnuSpeedFactor_x1.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x1.Text = "x1"
        '
        'mnuSpeedFactor_x2
        '
        Me.mnuSpeedFactor_x2.Name = "mnuSpeedFactor_x2"
        Me.mnuSpeedFactor_x2.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x2.Text = "x2"
        '
        'mnuSpeedFactor_x3
        '
        Me.mnuSpeedFactor_x3.Name = "mnuSpeedFactor_x3"
        Me.mnuSpeedFactor_x3.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x3.Text = "x3"
        '
        'mnuSpeedFactor_x4
        '
        Me.mnuSpeedFactor_x4.Name = "mnuSpeedFactor_x4"
        Me.mnuSpeedFactor_x4.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x4.Text = "x4"
        '
        'mnuSpeedFactor_x5
        '
        Me.mnuSpeedFactor_x5.Name = "mnuSpeedFactor_x5"
        Me.mnuSpeedFactor_x5.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x5.Text = "x5"
        '
        'mnuSpeedFactor_x6
        '
        Me.mnuSpeedFactor_x6.Name = "mnuSpeedFactor_x6"
        Me.mnuSpeedFactor_x6.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x6.Text = "x6"
        '
        'mnuSpeedFactor_x7
        '
        Me.mnuSpeedFactor_x7.Name = "mnuSpeedFactor_x7"
        Me.mnuSpeedFactor_x7.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x7.Text = "x7"
        '
        'mnuSpeedFactor_x8
        '
        Me.mnuSpeedFactor_x8.Name = "mnuSpeedFactor_x8"
        Me.mnuSpeedFactor_x8.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x8.Text = "x8"
        '
        'mnuSpeedFactor_x9
        '
        Me.mnuSpeedFactor_x9.Name = "mnuSpeedFactor_x9"
        Me.mnuSpeedFactor_x9.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x9.Text = "x9"
        '
        'mnuSpeedFactor_x10
        '
        Me.mnuSpeedFactor_x10.Name = "mnuSpeedFactor_x10"
        Me.mnuSpeedFactor_x10.Size = New System.Drawing.Size(92, 22)
        Me.mnuSpeedFactor_x10.Text = "x10"
        '
        'pnlEdit
        '
        Me.pnlEdit.Controls.Add(Me.lblZpxl)
        Me.pnlEdit.Controls.Add(Me.btnSmoothStraight)
        Me.pnlEdit.Controls.Add(Me.btnSmooth)
        Me.pnlEdit.Controls.Add(Me.btnCutEnd)
        Me.pnlEdit.Controls.Add(Me.btnCutStart)
        Me.pnlEdit.Controls.Add(Me.lblXpxl)
        Me.pnlEdit.Controls.Add(Me.lblYpxl)
        Me.pnlEdit.Location = New System.Drawing.Point(497, 1)
        Me.pnlEdit.Name = "pnlEdit"
        Me.pnlEdit.Size = New System.Drawing.Size(230, 51)
        Me.pnlEdit.TabIndex = 20
        Me.pnlEdit.Visible = False
        '
        'lblZpxl
        '
        Me.lblZpxl.AutoSize = True
        Me.lblZpxl.Location = New System.Drawing.Point(9, 35)
        Me.lblZpxl.Name = "lblZpxl"
        Me.lblZpxl.Size = New System.Drawing.Size(55, 13)
        Me.lblZpxl.TabIndex = 23
        Me.lblZpxl.Text = "Z: -0,25 m"
        '
        'btnSmoothStraight
        '
        Me.btnSmoothStraight.Location = New System.Drawing.Point(168, 25)
        Me.btnSmoothStraight.Name = "btnSmoothStraight"
        Me.btnSmoothStraight.Size = New System.Drawing.Size(62, 21)
        Me.btnSmoothStraight.TabIndex = 22
        Me.btnSmoothStraight.Text = "Straight"
        Me.btnSmoothStraight.UseVisualStyleBackColor = True
        '
        'btnSmooth
        '
        Me.btnSmooth.Location = New System.Drawing.Point(168, 3)
        Me.btnSmooth.Name = "btnSmooth"
        Me.btnSmooth.Size = New System.Drawing.Size(62, 21)
        Me.btnSmooth.TabIndex = 21
        Me.btnSmooth.Text = "Smooth"
        Me.btnSmooth.UseVisualStyleBackColor = True
        '
        'btnCutEnd
        '
        Me.btnCutEnd.Location = New System.Drawing.Point(89, 25)
        Me.btnCutEnd.Name = "btnCutEnd"
        Me.btnCutEnd.Size = New System.Drawing.Size(62, 21)
        Me.btnCutEnd.TabIndex = 19
        Me.btnCutEnd.Text = "Cut end"
        Me.btnCutEnd.UseVisualStyleBackColor = True
        '
        'btnCutStart
        '
        Me.btnCutStart.Location = New System.Drawing.Point(89, 3)
        Me.btnCutStart.Name = "btnCutStart"
        Me.btnCutStart.Size = New System.Drawing.Size(62, 21)
        Me.btnCutStart.TabIndex = 18
        Me.btnCutStart.Text = "Cut start"
        Me.btnCutStart.UseVisualStyleBackColor = True
        '
        'lblXpxl
        '
        Me.lblXpxl.AutoSize = True
        Me.lblXpxl.Location = New System.Drawing.Point(9, 3)
        Me.lblXpxl.Name = "lblXpxl"
        Me.lblXpxl.Size = New System.Drawing.Size(64, 13)
        Me.lblXpxl.TabIndex = 15
        Me.lblXpxl.Text = "X: 125.56 m"
        '
        'pnlNavigation
        '
        Me.pnlNavigation.Controls.Add(Me.btnNext)
        Me.pnlNavigation.Controls.Add(Me.btnPrevious)
        Me.pnlNavigation.Controls.Add(Me.lblCurrentFrame)
        Me.pnlNavigation.Controls.Add(Me.bntJumpBack)
        Me.pnlNavigation.Controls.Add(Me.btnJumpForward)
        Me.pnlNavigation.Location = New System.Drawing.Point(0, 0)
        Me.pnlNavigation.Name = "pnlNavigation"
        Me.pnlNavigation.Size = New System.Drawing.Size(235, 52)
        Me.pnlNavigation.TabIndex = 21
        '
        'picDisplay
        '
        Me.picDisplay.ContextMenuStrip = Me.mnuContext
        Me.picDisplay.Location = New System.Drawing.Point(0, 58)
        Me.picDisplay.Name = "picDisplay"
        Me.picDisplay.Size = New System.Drawing.Size(38, 71)
        Me.picDisplay.TabIndex = 22
        Me.picDisplay.TabStop = False
        '
        'chkGraph
        '
        Me.chkGraph.AutoSize = True
        Me.chkGraph.Location = New System.Drawing.Point(733, 6)
        Me.chkGraph.Name = "chkGraph"
        Me.chkGraph.Size = New System.Drawing.Size(86, 17)
        Me.chkGraph.TabIndex = 24
        Me.chkGraph.Text = "show graphs"
        Me.chkGraph.UseVisualStyleBackColor = True
        '
        'pnlSpeed
        '
        Me.pnlSpeed.Controls.Add(Me.btnSpeedFactor)
        Me.pnlSpeed.Location = New System.Drawing.Point(241, 0)
        Me.pnlSpeed.Name = "pnlSpeed"
        Me.pnlSpeed.Size = New System.Drawing.Size(51, 52)
        Me.pnlSpeed.TabIndex = 25
        '
        'mnuSecondsBefore
        '
        Me.mnuSecondsBefore.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem5, Me.ToolStripMenuItem6, Me.ToolStripMenuItem7, Me.ToolStripMenuItem8, Me.ToolStripMenuItem9, Me.ToolStripMenuItem10})
        Me.mnuSecondsBefore.Name = "mnuSpeedFactor"
        Me.mnuSecondsBefore.Size = New System.Drawing.Size(92, 136)
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem5.Text = "5s"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem6.Text = "10s"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem7.Text = "15s"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem8.Text = "20s"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem9.Text = "30s"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem10.Text = "40s"
        '
        'usrDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnlSpeed)
        Me.Controls.Add(Me.chkGraph)
        Me.Controls.Add(Me.picDisplay)
        Me.Controls.Add(Me.pnlNavigation)
        Me.Controls.Add(Me.pnlEdit)
        Me.Controls.Add(Me.pnlPlay)
        Me.Controls.Add(Me.lblCurrentTime)
        Me.Controls.Add(Me.lblTotalTime)
        Me.Controls.Add(Me.trbFrames)
        Me.Name = "usrDisplay"
        Me.Size = New System.Drawing.Size(923, 312)
        CType(Me.trbFrames, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuContext.ResumeLayout(False)
        Me.pnlPlay.ResumeLayout(False)
        Me.mnuSpeedFactor.ResumeLayout(False)
        Me.pnlEdit.ResumeLayout(False)
        Me.pnlEdit.PerformLayout()
        Me.pnlNavigation.ResumeLayout(False)
        Me.pnlNavigation.PerformLayout()
        CType(Me.picDisplay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSpeed.ResumeLayout(False)
        Me.mnuSecondsBefore.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnStop As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents tmrFrames As System.Windows.Forms.Timer
    Friend WithEvents chkPlay As System.Windows.Forms.CheckBox
    Friend WithEvents imlButtons As System.Windows.Forms.ImageList
    Friend WithEvents lblCurrentFrame As System.Windows.Forms.Label
    Friend WithEvents tltToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents trbFrames As System.Windows.Forms.TrackBar
    Friend WithEvents lblTotalTime As System.Windows.Forms.Label
    Friend WithEvents lblCurrentTime As System.Windows.Forms.Label
    Friend WithEvents mnuContext As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSaveImage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents digSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuSaveVideoSequence As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnJumpForward As System.Windows.Forms.Button
    Friend WithEvents bntJumpBack As System.Windows.Forms.Button
    Friend WithEvents digOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblYpxl As System.Windows.Forms.Label
    Friend WithEvents pnlPlay As System.Windows.Forms.Panel
    Friend WithEvents pnlEdit As System.Windows.Forms.Panel
    Friend WithEvents pnlNavigation As System.Windows.Forms.Panel
    Friend WithEvents picDisplay As System.Windows.Forms.PictureBox
    Friend WithEvents lblXpxl As System.Windows.Forms.Label
    Friend WithEvents btnCutStart As System.Windows.Forms.Button
    Friend WithEvents btnCutEnd As System.Windows.Forms.Button
    Friend WithEvents btnSmooth As System.Windows.Forms.Button
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents btnSmoothStraight As System.Windows.Forms.Button
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExportAsGroundTruth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkGraph As System.Windows.Forms.CheckBox
    Friend WithEvents btnSpeedFactor As System.Windows.Forms.Button
    Friend WithEvents pnlSpeed As System.Windows.Forms.Panel
    Friend WithEvents mnuSpeedFactor As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSpeedFactor_x1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeedFactor_x10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCopyImage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblZpxl As Label
    Friend WithEvents ExportClickedPositionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuSecondsBefore As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem5 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As ToolStripMenuItem
End Class
