﻿Public Class frmBackground
  Private MyCurrentRecord As clsRecord

  Public WriteOnly Property CurrentRecord As clsRecord
    Set(ByVal value As clsRecord)
      MyCurrentRecord = value
      Call ArrangeAll()
    End Set
  End Property

  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    Me.Hide()
  End Sub


  Private Sub txtScale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If Double.TryParse(txtScale.Text, MyCurrentRecord.Map.Scale) Then
      lblScale.ForeColor = Color.Black
      txtScale.ForeColor = Color.Black
      lblpxlm.ForeColor = Color.Black
      btnOK.Enabled = True
    Else
      lblScale.ForeColor = Color.Red
      txtScale.ForeColor = Color.Red
      lblpxlm.ForeColor = Color.Red
      '  If MyCurrentRecord.Map.Length > 0 Then btnOK.Enabled = False
    End If

  End Sub

  Private Sub btnLoadBackground_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _NewFilePath As String

    With digOpenFile
      .FileName = ""
      .Multiselect = False
      .Filter = "JPEG files (*.jpg)|*.jpg"
      .CheckFileExists = True
      .Title = "Select projected map"
      '.InitialDirectory = myproject.MapDirector
      .ShowDialog()
      If .FileName.Length > 0 Then
        '  MyCurrentRecord.Map = Path.GetFileName(.FileName)
        ' _NewFilePath = clsRecord.BackgroundDirectory & MyCurrentRecord.Background
        If Not File.Exists(_NewFilePath) Then FileCopy(.FileName, _NewFilePath)
      End If
    End With
    Call ArrangeAll()
  End Sub

  Private Sub ArrangeAll()
    Const _Ratio_4_3 As Double = 1.3333333333333333
    Dim _OriginalImage As Bitmap
    Dim _AspectRatio As Double
    Dim g As Graphics
    Dim _ScaledImage As Bitmap

    ' If MyCurrentRecord.Map.Length > 0 Then
    'txtBackground.Text = MyCurrentRecord.Map

    ' If MyCurrentRecord.BackgroundBitmap IsNot Nothing Then
    '   _OriginalImage = New Bitmap(MyCurrentRecord.MapBitmap)
    _AspectRatio = _OriginalImage.Width / _OriginalImage.Height
    If _AspectRatio < _Ratio_4_3 Then
      picBackground.Height = 75
      picBackground.Width = CInt(picBackground.Height * _AspectRatio)
    Else
      picBackground.Width = 100
      picBackground.Height = CInt(picBackground.Width / _AspectRatio)
    End If
    _ScaledImage = New Bitmap(picBackground.Width, picBackground.Height)
    g = Graphics.FromImage(_ScaledImage)
    g.DrawImage(_OriginalImage, 0, 0, picBackground.Width, picBackground.Height)
    picBackground.Image = _ScaledImage
    picBackground.Visible = True
    g.Dispose()
    '  End If

    txtScale.Visible = True
    lblScale.Visible = True
    lblpxlm.Visible = True
    '  If IsValue(MyCurrentRecord.Scale) Then txtScale.Text = Format(MyCurrentRecord.Scale, "0.000")
    ' Else
    ' lblScale.Visible = False
    ' txtScale.Visible = False
    ' picBackground.Visible = False
    ' lblpxlm.Visible = False
    ' End If
  End Sub

  Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

  End Sub
End Class