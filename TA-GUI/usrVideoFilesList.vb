﻿Public Class usrVideoFilesList
  Private myVideoFiles As List(Of clsVideoFile)

  Public Property VideoFiles As List(Of clsVideoFile)
    Get
      VideoFiles = myVideoFiles
    End Get
    Set(ByVal value As List(Of clsVideoFile))
      myVideoFiles = value
    End Set
  End Property

End Class
