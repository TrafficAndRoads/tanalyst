﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clsDisplay
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(clsDisplay))
    Me.tabDisplay = New System.Windows.Forms.TabControl()
    Me.btnPrevious = New System.Windows.Forms.Button()
    Me.btnStop = New System.Windows.Forms.Button()
    Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
    Me.btnNext = New System.Windows.Forms.Button()
    Me.tmrFrames = New System.Windows.Forms.Timer(Me.components)
    Me.chkPlay = New System.Windows.Forms.CheckBox()
    Me.lblCurrentFrame = New System.Windows.Forms.Label()
    Me.btnSpeedFactor = New System.Windows.Forms.Button()
    Me.tltToolTip = New System.Windows.Forms.ToolTip(Me.components)
    Me.trbFrames = New System.Windows.Forms.TrackBar()
    Me.lblTotalTime = New System.Windows.Forms.Label()
    Me.lblCurrentTime = New System.Windows.Forms.Label()
    CType(Me.trbFrames, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'tabDisplay
    '
    Me.tabDisplay.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
    Me.tabDisplay.Location = New System.Drawing.Point(0, 45)
    Me.tabDisplay.Name = "tabDisplay"
    Me.tabDisplay.SelectedIndex = 0
    Me.tabDisplay.Size = New System.Drawing.Size(520, 345)
    Me.tabDisplay.TabIndex = 1
    '
    'btnPrevious
    '
    Me.btnPrevious.Location = New System.Drawing.Point(150, 5)
    Me.btnPrevious.Name = "btnPrevious"
    Me.btnPrevious.Size = New System.Drawing.Size(30, 32)
    Me.btnPrevious.TabIndex = 2
    Me.btnPrevious.Text = "<"
    Me.tltToolTip.SetToolTip(Me.btnPrevious, "Go to previous frame")
    Me.btnPrevious.UseVisualStyleBackColor = True
    '
    'btnStop
    '
    Me.btnStop.ImageKey = "Stop"
    Me.btnStop.ImageList = Me.imlButtons
    Me.btnStop.Location = New System.Drawing.Point(375, 10)
    Me.btnStop.Name = "btnStop"
    Me.btnStop.Size = New System.Drawing.Size(30, 25)
    Me.btnStop.TabIndex = 3
    Me.tltToolTip.SetToolTip(Me.btnStop, "Stop")
    Me.btnStop.UseVisualStyleBackColor = True
    '
    'imlButtons
    '
    Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.imlButtons.TransparentColor = System.Drawing.Color.White
    Me.imlButtons.Images.SetKeyName(0, "Play")
    Me.imlButtons.Images.SetKeyName(1, "Stop")
    Me.imlButtons.Images.SetKeyName(2, "Pause")
    '
    'btnNext
    '
    Me.btnNext.Location = New System.Drawing.Point(235, 5)
    Me.btnNext.Name = "btnNext"
    Me.btnNext.Size = New System.Drawing.Size(30, 32)
    Me.btnNext.TabIndex = 5
    Me.btnNext.Text = ">"
    Me.tltToolTip.SetToolTip(Me.btnNext, "Go to next frame")
    Me.btnNext.UseVisualStyleBackColor = True
    '
    'tmrFrames
    '
    '
    'chkPlay
    '
    Me.chkPlay.Appearance = System.Windows.Forms.Appearance.Button
    Me.chkPlay.ImageKey = "Play"
    Me.chkPlay.ImageList = Me.imlButtons
    Me.chkPlay.Location = New System.Drawing.Point(325, 5)
    Me.chkPlay.Name = "chkPlay"
    Me.chkPlay.Size = New System.Drawing.Size(50, 32)
    Me.chkPlay.TabIndex = 6
    Me.chkPlay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.tltToolTip.SetToolTip(Me.chkPlay, "Play")
    Me.chkPlay.UseVisualStyleBackColor = True
    '
    'lblCurrentFrame
    '
    Me.lblCurrentFrame.AutoSize = True
    Me.lblCurrentFrame.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblCurrentFrame.Location = New System.Drawing.Point(180, 10)
    Me.lblCurrentFrame.Name = "lblCurrentFrame"
    Me.lblCurrentFrame.Size = New System.Drawing.Size(54, 20)
    Me.lblCurrentFrame.TabIndex = 8
    Me.lblCurrentFrame.Text = "00000"
    Me.lblCurrentFrame.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.tltToolTip.SetToolTip(Me.lblCurrentFrame, "Current frame")
    '
    'btnSpeedFactor
    '
    Me.btnSpeedFactor.ImageKey = "(none)"
    Me.btnSpeedFactor.Location = New System.Drawing.Point(420, 10)
    Me.btnSpeedFactor.Name = "btnSpeedFactor"
    Me.btnSpeedFactor.Size = New System.Drawing.Size(30, 25)
    Me.btnSpeedFactor.TabIndex = 9
    Me.btnSpeedFactor.Text = "x1"
    Me.btnSpeedFactor.UseVisualStyleBackColor = True
    '
    'trbFrames
    '
    Me.trbFrames.AutoSize = False
    Me.trbFrames.Location = New System.Drawing.Point(0, 405)
    Me.trbFrames.Name = "trbFrames"
    Me.trbFrames.Size = New System.Drawing.Size(390, 15)
    Me.trbFrames.TabIndex = 10
    Me.trbFrames.TickStyle = System.Windows.Forms.TickStyle.None
    '
    'lblTotalTime
    '
    Me.lblTotalTime.AutoSize = True
    Me.lblTotalTime.Location = New System.Drawing.Point(365, 440)
    Me.lblTotalTime.Name = "lblTotalTime"
    Me.lblTotalTime.Size = New System.Drawing.Size(43, 13)
    Me.lblTotalTime.TabIndex = 11
    Me.lblTotalTime.Text = "0:00:00"
    '
    'lblCurrentTime
    '
    Me.lblCurrentTime.AutoSize = True
    Me.lblCurrentTime.Location = New System.Drawing.Point(0, 430)
    Me.lblCurrentTime.Name = "lblCurrentTime"
    Me.lblCurrentTime.Size = New System.Drawing.Size(43, 13)
    Me.lblCurrentTime.TabIndex = 12
    Me.lblCurrentTime.Text = "0:00:00"
    '
    'clsDisplay
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.lblCurrentTime)
    Me.Controls.Add(Me.lblTotalTime)
    Me.Controls.Add(Me.trbFrames)
    Me.Controls.Add(Me.btnSpeedFactor)
    Me.Controls.Add(Me.lblCurrentFrame)
    Me.Controls.Add(Me.chkPlay)
    Me.Controls.Add(Me.btnNext)
    Me.Controls.Add(Me.btnStop)
    Me.Controls.Add(Me.btnPrevious)
    Me.Controls.Add(Me.tabDisplay)
    Me.Name = "clsDisplay"
    Me.Size = New System.Drawing.Size(525, 498)
    CType(Me.trbFrames, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents tabDisplay As System.Windows.Forms.TabControl
  Friend WithEvents btnPrevious As System.Windows.Forms.Button
  Friend WithEvents btnStop As System.Windows.Forms.Button
  Friend WithEvents btnNext As System.Windows.Forms.Button
  Friend WithEvents tmrFrames As System.Windows.Forms.Timer
  Friend WithEvents chkPlay As System.Windows.Forms.CheckBox
  Friend WithEvents imlButtons As System.Windows.Forms.ImageList
  Friend WithEvents lblCurrentFrame As System.Windows.Forms.Label
  Friend WithEvents btnSpeedFactor As System.Windows.Forms.Button
  Friend WithEvents tltToolTip As System.Windows.Forms.ToolTip
  Friend WithEvents trbFrames As System.Windows.Forms.TrackBar
  Friend WithEvents lblTotalTime As System.Windows.Forms.Label
  Friend WithEvents lblCurrentTime As System.Windows.Forms.Label

End Class
