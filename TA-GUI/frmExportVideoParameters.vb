﻿Public Class frmExportVideoParameters
  Public KBS As Integer
  Public OK As Boolean
  Private Sub frmExportVideoParameters_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    cmbBPS.Items.Clear()
    cmbBPS.Items.Add("1500 bps (low)")
    cmbBPS.Items.Add("3000 bps (normal)")
    cmbBPS.Items.Add("4500 bps (high)")
    cmbBPS.SelectedIndex = 2
  End Sub

  Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
    Select Case cmbBPS.SelectedIndex
      Case 0
        KBS = 1500
      Case 1
        KBS = 3000
      Case 2
        KBS = 4500
      Case Else
        KBS = 3000
    End Select

    Me.OK = True
    Me.Hide()

  End Sub

  Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    Me.OK = False
    Me.Hide()
  End Sub
End Class