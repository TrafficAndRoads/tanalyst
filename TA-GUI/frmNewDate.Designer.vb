﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewDate
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.nudYear = New System.Windows.Forms.NumericUpDown()
    Me.lblYear = New System.Windows.Forms.Label()
    Me.lblMonth = New System.Windows.Forms.Label()
    Me.nudMonth = New System.Windows.Forms.NumericUpDown()
    Me.lblDay = New System.Windows.Forms.Label()
    Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
    Me.lblSecond = New System.Windows.Forms.Label()
    Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
    Me.lblMinute = New System.Windows.Forms.Label()
    Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
    Me.lblHour = New System.Windows.Forms.Label()
    Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
    CType(Me.nudYear, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.nudMonth, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(225, 126)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 137
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'nudYear
    '
    Me.nudYear.AllowDrop = True
    Me.nudYear.Location = New System.Drawing.Point(72, 5)
    Me.nudYear.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.nudYear.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.nudYear.Name = "nudYear"
    Me.nudYear.Size = New System.Drawing.Size(56, 20)
    Me.nudYear.TabIndex = 1
    Me.nudYear.TabStop = False
    Me.nudYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.nudYear.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'lblYear
    '
    Me.lblYear.AutoSize = True
    Me.lblYear.Location = New System.Drawing.Point(34, 7)
    Me.lblYear.Name = "lblYear"
    Me.lblYear.Size = New System.Drawing.Size(32, 13)
    Me.lblYear.TabIndex = 140
    Me.lblYear.Text = "Year:"
    '
    'lblMonth
    '
    Me.lblMonth.AutoSize = True
    Me.lblMonth.Location = New System.Drawing.Point(26, 32)
    Me.lblMonth.Name = "lblMonth"
    Me.lblMonth.Size = New System.Drawing.Size(40, 13)
    Me.lblMonth.TabIndex = 142
    Me.lblMonth.Text = "Month:"
    '
    'nudMonth
    '
    Me.nudMonth.AllowDrop = True
    Me.nudMonth.Location = New System.Drawing.Point(72, 30)
    Me.nudMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
    Me.nudMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.nudMonth.Name = "nudMonth"
    Me.nudMonth.Size = New System.Drawing.Size(56, 20)
    Me.nudMonth.TabIndex = 141
    Me.nudMonth.TabStop = False
    Me.nudMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.nudMonth.Value = New Decimal(New Integer() {12, 0, 0, 0})
    '
    'lblDay
    '
    Me.lblDay.AutoSize = True
    Me.lblDay.Location = New System.Drawing.Point(37, 57)
    Me.lblDay.Name = "lblDay"
    Me.lblDay.Size = New System.Drawing.Size(29, 13)
    Me.lblDay.TabIndex = 144
    Me.lblDay.Text = "Day:"
    '
    'NumericUpDown2
    '
    Me.NumericUpDown2.AllowDrop = True
    Me.NumericUpDown2.Location = New System.Drawing.Point(225, 80)
    Me.NumericUpDown2.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.NumericUpDown2.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.NumericUpDown2.Name = "NumericUpDown2"
    Me.NumericUpDown2.Size = New System.Drawing.Size(56, 20)
    Me.NumericUpDown2.TabIndex = 143
    Me.NumericUpDown2.TabStop = False
    Me.NumericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.NumericUpDown2.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(157, 82)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(62, 13)
    Me.Label3.TabIndex = 146
    Me.Label3.Text = "Millisecond:"
    '
    'NumericUpDown3
    '
    Me.NumericUpDown3.AllowDrop = True
    Me.NumericUpDown3.Location = New System.Drawing.Point(225, 55)
    Me.NumericUpDown3.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.NumericUpDown3.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.NumericUpDown3.Name = "NumericUpDown3"
    Me.NumericUpDown3.Size = New System.Drawing.Size(56, 20)
    Me.NumericUpDown3.TabIndex = 145
    Me.NumericUpDown3.TabStop = False
    Me.NumericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.NumericUpDown3.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'lblSecond
    '
    Me.lblSecond.AutoSize = True
    Me.lblSecond.Location = New System.Drawing.Point(172, 57)
    Me.lblSecond.Name = "lblSecond"
    Me.lblSecond.Size = New System.Drawing.Size(47, 13)
    Me.lblSecond.TabIndex = 148
    Me.lblSecond.Text = "Second:"
    '
    'NumericUpDown4
    '
    Me.NumericUpDown4.AllowDrop = True
    Me.NumericUpDown4.Location = New System.Drawing.Point(225, 30)
    Me.NumericUpDown4.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.NumericUpDown4.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.NumericUpDown4.Name = "NumericUpDown4"
    Me.NumericUpDown4.Size = New System.Drawing.Size(56, 20)
    Me.NumericUpDown4.TabIndex = 147
    Me.NumericUpDown4.TabStop = False
    Me.NumericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.NumericUpDown4.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'lblMinute
    '
    Me.lblMinute.AutoSize = True
    Me.lblMinute.Location = New System.Drawing.Point(177, 32)
    Me.lblMinute.Name = "lblMinute"
    Me.lblMinute.Size = New System.Drawing.Size(42, 13)
    Me.lblMinute.TabIndex = 150
    Me.lblMinute.Text = "Minute:"
    '
    'NumericUpDown5
    '
    Me.NumericUpDown5.AllowDrop = True
    Me.NumericUpDown5.Location = New System.Drawing.Point(225, 5)
    Me.NumericUpDown5.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.NumericUpDown5.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.NumericUpDown5.Name = "NumericUpDown5"
    Me.NumericUpDown5.Size = New System.Drawing.Size(56, 20)
    Me.NumericUpDown5.TabIndex = 149
    Me.NumericUpDown5.TabStop = False
    Me.NumericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.NumericUpDown5.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'lblHour
    '
    Me.lblHour.AutoSize = True
    Me.lblHour.Location = New System.Drawing.Point(186, 7)
    Me.lblHour.Name = "lblHour"
    Me.lblHour.Size = New System.Drawing.Size(33, 13)
    Me.lblHour.TabIndex = 152
    Me.lblHour.Text = "Hour:"
    '
    'NumericUpDown6
    '
    Me.NumericUpDown6.AllowDrop = True
    Me.NumericUpDown6.Location = New System.Drawing.Point(72, 55)
    Me.NumericUpDown6.Maximum = New Decimal(New Integer() {2150, 0, 0, 0})
    Me.NumericUpDown6.Minimum = New Decimal(New Integer() {1970, 0, 0, 0})
    Me.NumericUpDown6.Name = "NumericUpDown6"
    Me.NumericUpDown6.Size = New System.Drawing.Size(56, 20)
    Me.NumericUpDown6.TabIndex = 151
    Me.NumericUpDown6.TabStop = False
    Me.NumericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.NumericUpDown6.Value = New Decimal(New Integer() {1970, 0, 0, 0})
    '
    'frmNewDate
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(288, 152)
    Me.ControlBox = False
    Me.Controls.Add(Me.lblHour)
    Me.Controls.Add(Me.NumericUpDown6)
    Me.Controls.Add(Me.lblMinute)
    Me.Controls.Add(Me.NumericUpDown5)
    Me.Controls.Add(Me.lblSecond)
    Me.Controls.Add(Me.NumericUpDown4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.NumericUpDown3)
    Me.Controls.Add(Me.lblDay)
    Me.Controls.Add(Me.NumericUpDown2)
    Me.Controls.Add(Me.lblMonth)
    Me.Controls.Add(Me.nudMonth)
    Me.Controls.Add(Me.lblYear)
    Me.Controls.Add(Me.nudYear)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmNewDate"
    Me.Text = "frmNewDate"
    CType(Me.nudYear, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.nudMonth, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents btnOK As Button
  Friend WithEvents nudYear As NumericUpDown
  Friend WithEvents lblYear As Label
  Friend WithEvents lblMonth As Label
  Friend WithEvents nudMonth As NumericUpDown
  Friend WithEvents lblDay As Label
  Friend WithEvents NumericUpDown2 As NumericUpDown
  Friend WithEvents Label3 As Label
  Friend WithEvents NumericUpDown3 As NumericUpDown
  Friend WithEvents lblSecond As Label
  Friend WithEvents NumericUpDown4 As NumericUpDown
  Friend WithEvents lblMinute As Label
  Friend WithEvents NumericUpDown5 As NumericUpDown
  Friend WithEvents lblHour As Label
  Friend WithEvents NumericUpDown6 As NumericUpDown
End Class
