﻿Public Class usrClickCounter
  Private MyValue As Integer

  Public Sub New()
    InitializeComponent()

    MyValue = NoValue
    txtValue.Text = ""
  End Sub

  Public Property Value As Integer
    Get
      Return MyValue
    End Get
    Set(value As Integer)
      MyValue = value
      If IsValue(MyValue) Then
        txtValue.Text = MyValue.ToString
        If MyValue > 0 Then
          mnuContextMinus1.Enabled = True
        Else
          mnuContextMinus1.Enabled = False
        End If
      Else
        txtValue.Text = ""
        mnuContextMinus1.Enabled = False
      End If
    End Set
  End Property

  Private Sub usrClickCounter_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
    txtValue.Location = New Point(0, 0)
    txtValue.Height = Me.Height
    txtValue.Width = Me.Width - 20
    btnPlusOne.Location = New Point(txtValue.Width + 2, 0)
    btnPlusOne.Width = 17
    btnPlusOne.Height = Me.Height
  End Sub

  Private Sub btnPlusOne_Click(sender As Object, e As EventArgs) Handles btnPlusOne.Click

    If Not IsValue(MyValue) Then MyValue = 0

    Me.Value = MyValue + 1
  End Sub

  Private Sub ResetToNoValueToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuContextResetToEmpty.Click
    Me.Value = NoValue
  End Sub

  Private Sub ResetToZeroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuContextResetToZero.Click
    Me.Value = 0
  End Sub

  Private Sub mnuContextMinus1_Click(sender As Object, e As EventArgs) Handles mnuContextMinus1.Click
    Me.Value = MyValue - 1
  End Sub

  Private Sub mnuContextSetTo_Click(sender As Object, e As EventArgs) Handles mnuContextSetTo.Click
    Dim _String As String
    Dim _Value As Integer

    _String = InputBox("", "Set value to...")

    If Integer.TryParse(_String, _Value) Then
      If _Value >= 0 Then Me.Value = _Value
    End If


  End Sub
End Class
