﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrUpDown
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.txtValue = New System.Windows.Forms.TextBox()
    Me.btnUp = New System.Windows.Forms.Button()
    Me.btnDown = New System.Windows.Forms.Button()
    Me.lblName = New System.Windows.Forms.Label()
    Me.SuspendLayout()
    '
    'txtValue
    '
    Me.txtValue.Location = New System.Drawing.Point(53, 12)
    Me.txtValue.Name = "txtValue"
    Me.txtValue.Size = New System.Drawing.Size(40, 20)
    Me.txtValue.TabIndex = 0
    Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'btnUp
    '
    Me.btnUp.Location = New System.Drawing.Point(95, 0)
    Me.btnUp.Name = "btnUp"
    Me.btnUp.Size = New System.Drawing.Size(19, 22)
    Me.btnUp.TabIndex = 1
    Me.btnUp.Text = "+"
    Me.btnUp.UseVisualStyleBackColor = True
    '
    'btnDown
    '
    Me.btnDown.Location = New System.Drawing.Point(95, 22)
    Me.btnDown.Name = "btnDown"
    Me.btnDown.Size = New System.Drawing.Size(19, 22)
    Me.btnDown.TabIndex = 2
    Me.btnDown.Text = "-"
    Me.btnDown.UseVisualStyleBackColor = True
    '
    'lblName
    '
    Me.lblName.AutoSize = True
    Me.lblName.Location = New System.Drawing.Point(0, 15)
    Me.lblName.Name = "lblName"
    Me.lblName.Size = New System.Drawing.Size(52, 13)
    Me.lblName.TabIndex = 3
    Me.lblName.Text = "Direction:"
    Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'usrUpDown
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.lblName)
    Me.Controls.Add(Me.btnDown)
    Me.Controls.Add(Me.btnUp)
    Me.Controls.Add(Me.txtValue)
    Me.Name = "usrUpDown"
    Me.Size = New System.Drawing.Size(114, 44)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txtValue As System.Windows.Forms.TextBox
  Friend WithEvents btnUp As System.Windows.Forms.Button
  Friend WithEvents btnDown As System.Windows.Forms.Button
  Friend WithEvents lblName As System.Windows.Forms.Label

End Class
