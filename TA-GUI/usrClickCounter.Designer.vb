﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrClickCounter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.btnPlusOne = New System.Windows.Forms.Button()
    Me.txtValue = New System.Windows.Forms.TextBox()
    Me.mnuContext = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.mnuContextResetToZero = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuContextResetToEmpty = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuContextMinus1 = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
    Me.mnuContextSetTo = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuContext.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnPlusOne
    '
    Me.btnPlusOne.ContextMenuStrip = Me.mnuContext
    Me.btnPlusOne.Location = New System.Drawing.Point(92, 48)
    Me.btnPlusOne.Name = "btnPlusOne"
    Me.btnPlusOne.Size = New System.Drawing.Size(35, 33)
    Me.btnPlusOne.TabIndex = 0
    Me.btnPlusOne.UseVisualStyleBackColor = True
    '
    'txtValue
    '
    Me.txtValue.Enabled = False
    Me.txtValue.Location = New System.Drawing.Point(3, 15)
    Me.txtValue.Name = "txtValue"
    Me.txtValue.Size = New System.Drawing.Size(45, 20)
    Me.txtValue.TabIndex = 2
    '
    'mnuContext
    '
    Me.mnuContext.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuContextMinus1, Me.ToolStripSeparator1, Me.mnuContextSetTo, Me.mnuContextResetToZero, Me.mnuContextResetToEmpty})
    Me.mnuContext.Name = "mnuContext"
    Me.mnuContext.Size = New System.Drawing.Size(154, 120)
    '
    'mnuContextResetToZero
    '
    Me.mnuContextResetToZero.Name = "mnuContextResetToZero"
    Me.mnuContextResetToZero.Size = New System.Drawing.Size(153, 22)
    Me.mnuContextResetToZero.Text = "Reset to zero"
    '
    'mnuContextResetToEmpty
    '
    Me.mnuContextResetToEmpty.Name = "mnuContextResetToEmpty"
    Me.mnuContextResetToEmpty.Size = New System.Drawing.Size(153, 22)
    Me.mnuContextResetToEmpty.Text = "Reset to empty"
    '
    'mnuContextMinus1
    '
    Me.mnuContextMinus1.Name = "mnuContextMinus1"
    Me.mnuContextMinus1.Size = New System.Drawing.Size(153, 22)
    Me.mnuContextMinus1.Text = "- 1"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(150, 6)
    '
    'mnuContextSetTo
    '
    Me.mnuContextSetTo.Name = "mnuContextSetTo"
    Me.mnuContextSetTo.Size = New System.Drawing.Size(153, 22)
    Me.mnuContextSetTo.Text = "Set to ..."
    '
    'usrClickCounter
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.txtValue)
    Me.Controls.Add(Me.btnPlusOne)
    Me.Name = "usrClickCounter"
    Me.mnuContext.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnPlusOne As System.Windows.Forms.Button
  Friend WithEvents txtValue As System.Windows.Forms.TextBox
  Friend WithEvents mnuContext As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents mnuContextResetToZero As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuContextResetToEmpty As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuContextMinus1 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents mnuContextSetTo As System.Windows.Forms.ToolStripMenuItem

End Class
