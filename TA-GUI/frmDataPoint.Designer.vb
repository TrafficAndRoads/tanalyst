﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataPoint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnAdd = New System.Windows.Forms.Button()
    Me.btnDelete = New System.Windows.Forms.Button()
    Me.rbtOriginal = New System.Windows.Forms.RadioButton()
    Me.rbtSmoothed = New System.Windows.Forms.RadioButton()
    Me.btnSmoothTrajectory = New System.Windows.Forms.Button()
    Me.btnCutStart = New System.Windows.Forms.Button()
    Me.btnCutEnd = New System.Windows.Forms.Button()
    Me.chkShowGraph = New System.Windows.Forms.CheckBox()
    Me.SuspendLayout()
    '
    'btnAdd
    '
    Me.btnAdd.Location = New System.Drawing.Point(387, 10)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(60, 22)
    Me.btnAdd.TabIndex = 3
    Me.btnAdd.Text = "Add point"
    Me.btnAdd.UseVisualStyleBackColor = True
    '
    'btnDelete
    '
    Me.btnDelete.Location = New System.Drawing.Point(387, 38)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(60, 22)
    Me.btnDelete.TabIndex = 4
    Me.btnDelete.Text = "Delete"
    Me.btnDelete.UseVisualStyleBackColor = True
    '
    'rbtOriginal
    '
    Me.rbtOriginal.AutoSize = True
    Me.rbtOriginal.Location = New System.Drawing.Point(12, 12)
    Me.rbtOriginal.Name = "rbtOriginal"
    Me.rbtOriginal.Size = New System.Drawing.Size(60, 17)
    Me.rbtOriginal.TabIndex = 9
    Me.rbtOriginal.Text = "Original"
    Me.rbtOriginal.UseVisualStyleBackColor = True
    '
    'rbtSmoothed
    '
    Me.rbtSmoothed.AutoSize = True
    Me.rbtSmoothed.Location = New System.Drawing.Point(12, 40)
    Me.rbtSmoothed.Name = "rbtSmoothed"
    Me.rbtSmoothed.Size = New System.Drawing.Size(73, 17)
    Me.rbtSmoothed.TabIndex = 8
    Me.rbtSmoothed.Text = "Smoothed"
    Me.rbtSmoothed.UseVisualStyleBackColor = True
    '
    'btnSmoothTrajectory
    '
    Me.btnSmoothTrajectory.Location = New System.Drawing.Point(186, 116)
    Me.btnSmoothTrajectory.Name = "btnSmoothTrajectory"
    Me.btnSmoothTrajectory.Size = New System.Drawing.Size(60, 22)
    Me.btnSmoothTrajectory.TabIndex = 10
    Me.btnSmoothTrajectory.Text = "Smooth"
    Me.btnSmoothTrajectory.UseVisualStyleBackColor = True
    '
    'btnCutStart
    '
    Me.btnCutStart.Location = New System.Drawing.Point(453, 38)
    Me.btnCutStart.Name = "btnCutStart"
    Me.btnCutStart.Size = New System.Drawing.Size(60, 22)
    Me.btnCutStart.TabIndex = 11
    Me.btnCutStart.Text = "Cut start"
    Me.btnCutStart.UseVisualStyleBackColor = True
    '
    'btnCutEnd
    '
    Me.btnCutEnd.Location = New System.Drawing.Point(453, 66)
    Me.btnCutEnd.Name = "btnCutEnd"
    Me.btnCutEnd.Size = New System.Drawing.Size(60, 22)
    Me.btnCutEnd.TabIndex = 12
    Me.btnCutEnd.Text = "Cut end"
    Me.btnCutEnd.UseVisualStyleBackColor = True
    '
    'chkShowGraph
    '
    Me.chkShowGraph.AutoSize = True
    Me.chkShowGraph.Location = New System.Drawing.Point(12, 116)
    Me.chkShowGraph.Name = "chkShowGraph"
    Me.chkShowGraph.Size = New System.Drawing.Size(88, 17)
    Me.chkShowGraph.TabIndex = 13
    Me.chkShowGraph.Text = "Show graphs"
    Me.chkShowGraph.UseVisualStyleBackColor = True
    '
    'frmDataPoint
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(523, 390)
    Me.ControlBox = False
    Me.Controls.Add(Me.chkShowGraph)
    Me.Controls.Add(Me.btnCutEnd)
    Me.Controls.Add(Me.btnCutStart)
    Me.Controls.Add(Me.btnSmoothTrajectory)
    Me.Controls.Add(Me.rbtOriginal)
    Me.Controls.Add(Me.rbtSmoothed)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnAdd)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmDataPoint"
    Me.Text = "Trajectory point"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents btnAdd As System.Windows.Forms.Button
  Friend WithEvents btnDelete As System.Windows.Forms.Button
  Friend WithEvents rbtOriginal As System.Windows.Forms.RadioButton
  Friend WithEvents rbtSmoothed As System.Windows.Forms.RadioButton
  Friend WithEvents btnSmoothTrajectory As System.Windows.Forms.Button
  Friend WithEvents btnCutStart As System.Windows.Forms.Button
  Friend WithEvents btnCutEnd As System.Windows.Forms.Button
  Friend WithEvents chkShowGraph As System.Windows.Forms.CheckBox
End Class
