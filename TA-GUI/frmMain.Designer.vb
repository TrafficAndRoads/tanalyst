﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuProject = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectLoad = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuProjectImportData = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectImportDataRubaOutput = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectImportDataIndividualVideos = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectImportExportedEvent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectExport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectExportVideos = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroundTruthsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SingleEventToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuProjectProperties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProjectSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuProjectExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuData = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataVideoRecordings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataDetections = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDataTableDesign = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDataFilter = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecord = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordSmoothRoadUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.CurvedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StraightToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordCreateDuplicate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuRecordLoadGates = New System.Windows.Forms.ToolStripMenuItem()
        Me.Create_Gates_Strudl = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuRecordCalculatreTTC = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordCalculateTTCTrajectoryBased = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordCalculateTTCStraight = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordCalculateDistance = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordClearTTC = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRecordMultiple = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportGateDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplay = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayRoadUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayTrajectories = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplaySpeedVectors = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDisplayZoomView = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDisplayMap = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDisplayCamera12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDisplaySettings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.stsMain = New System.Windows.Forms.StatusStrip()
        Me.stsWorldCoordinates = New System.Windows.Forms.ToolStripStatusLabel()
        Me.stsImageCoordinates = New System.Windows.Forms.ToolStripStatusLabel()
        Me.stsOnePixelError = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tlbMain = New System.Windows.Forms.ToolStrip()
        Me.tlbNew = New System.Windows.Forms.ToolStripButton()
        Me.tlbOpen = New System.Windows.Forms.ToolStripButton()
        Me.digOpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.grpMainData = New System.Windows.Forms.GroupBox()
        Me.btnMedia = New System.Windows.Forms.Button()
        Me.pnlNavigationButtons = New System.Windows.Forms.Panel()
        Me.lblCurrentRecord = New System.Windows.Forms.Label()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnJumpBack = New System.Windows.Forms.Button()
        Me.btnJumpForward = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnMap = New System.Windows.Forms.Button()
        Me.lblType = New System.Windows.Forms.Label()
        Me.cmbType = New System.Windows.Forms.ComboBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnRelatedRecords = New System.Windows.Forms.Button()
        Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
        Me.btnNew = New System.Windows.Forms.Button()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.cmbStatus = New System.Windows.Forms.ComboBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.dtpTime = New System.Windows.Forms.DateTimePicker()
        Me.grpUserControls = New System.Windows.Forms.GroupBox()
        Me.grpRoadUsers = New System.Windows.Forms.GroupBox()
        Me.chkRUKey2 = New System.Windows.Forms.CheckBox()
        Me.lblRUweight = New System.Windows.Forms.Label()
        Me.chkRUKey1 = New System.Windows.Forms.CheckBox()
        Me.txtRUweight = New System.Windows.Forms.TextBox()
        Me.cmbRUType = New System.Windows.Forms.ComboBox()
        Me.btnImportRU = New System.Windows.Forms.Button()
        Me.lblRUlength = New System.Windows.Forms.Label()
        Me.txtRUlength = New System.Windows.Forms.TextBox()
        Me.lblRUheight = New System.Windows.Forms.Label()
        Me.txtRUheight = New System.Windows.Forms.TextBox()
        Me.lblWidth = New System.Windows.Forms.Label()
        Me.txtRUwidth = New System.Windows.Forms.TextBox()
        Me.lblRUtype = New System.Windows.Forms.Label()
        Me.btnDeleteRU = New System.Windows.Forms.Button()
        Me.btnNewRU = New System.Windows.Forms.Button()
        Me.btnEditRU = New System.Windows.Forms.Button()
        Me.lstRoadUsers = New System.Windows.Forms.ListBox()
        Me.mnuRoadUser = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuRoaduserCheckGates = New System.Windows.Forms.ToolStripMenuItem()
        Me.tltToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.digSave = New System.Windows.Forms.SaveFileDialog()
        Me.btnHide = New System.Windows.Forms.Button()
        Me.mnuMain.SuspendLayout()
        Me.stsMain.SuspendLayout()
        Me.tlbMain.SuspendLayout()
        Me.grpMainData.SuspendLayout()
        Me.pnlNavigationButtons.SuspendLayout()
        Me.grpRoadUsers.SuspendLayout()
        Me.mnuRoadUser.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProject, Me.mnuData, Me.mnuRecord, Me.mnuDisplay, Me.mnuHelp})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(626, 24)
        Me.mnuMain.TabIndex = 1
        Me.mnuMain.Text = "MenuStrip1"
        '
        'mnuProject
        '
        Me.mnuProject.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProjectNew, Me.mnuProjectLoad, Me.mnuProjectClose, Me.mnuProjectSeparator1, Me.mnuProjectImportData, Me.mnuProjectExport, Me.mnuProjectSeparator2, Me.mnuProjectProperties, Me.mnuProjectSeparator3, Me.mnuProjectExit})
        Me.mnuProject.Name = "mnuProject"
        Me.mnuProject.Size = New System.Drawing.Size(56, 20)
        Me.mnuProject.Text = "Project"
        '
        'mnuProjectNew
        '
        Me.mnuProjectNew.Name = "mnuProjectNew"
        Me.mnuProjectNew.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectNew.Text = "New"
        '
        'mnuProjectLoad
        '
        Me.mnuProjectLoad.Name = "mnuProjectLoad"
        Me.mnuProjectLoad.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectLoad.Text = "Load"
        '
        'mnuProjectClose
        '
        Me.mnuProjectClose.Name = "mnuProjectClose"
        Me.mnuProjectClose.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectClose.Text = "Close"
        '
        'mnuProjectSeparator1
        '
        Me.mnuProjectSeparator1.Name = "mnuProjectSeparator1"
        Me.mnuProjectSeparator1.Size = New System.Drawing.Size(124, 6)
        '
        'mnuProjectImportData
        '
        Me.mnuProjectImportData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProjectImportDataRubaOutput, Me.mnuProjectImportDataIndividualVideos, Me.mnuProjectImportExportedEvent})
        Me.mnuProjectImportData.Name = "mnuProjectImportData"
        Me.mnuProjectImportData.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectImportData.Text = "Import"
        '
        'mnuProjectImportDataRubaOutput
        '
        Me.mnuProjectImportDataRubaOutput.Name = "mnuProjectImportDataRubaOutput"
        Me.mnuProjectImportDataRubaOutput.Size = New System.Drawing.Size(163, 22)
        Me.mnuProjectImportDataRubaOutput.Text = "Ruba output"
        '
        'mnuProjectImportDataIndividualVideos
        '
        Me.mnuProjectImportDataIndividualVideos.Name = "mnuProjectImportDataIndividualVideos"
        Me.mnuProjectImportDataIndividualVideos.Size = New System.Drawing.Size(163, 22)
        Me.mnuProjectImportDataIndividualVideos.Text = "Individual videos"
        '
        'mnuProjectImportExportedEvent
        '
        Me.mnuProjectImportExportedEvent.Name = "mnuProjectImportExportedEvent"
        Me.mnuProjectImportExportedEvent.Size = New System.Drawing.Size(163, 22)
        Me.mnuProjectImportExportedEvent.Text = "Exported event"
        '
        'mnuProjectExport
        '
        Me.mnuProjectExport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProjectExportVideos, Me.GroundTruthsToolStripMenuItem, Me.SingleEventToolStripMenuItem})
        Me.mnuProjectExport.Name = "mnuProjectExport"
        Me.mnuProjectExport.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectExport.Text = "Export"
        '
        'mnuProjectExportVideos
        '
        Me.mnuProjectExportVideos.Name = "mnuProjectExportVideos"
        Me.mnuProjectExportVideos.Size = New System.Drawing.Size(148, 22)
        Me.mnuProjectExportVideos.Text = "Videos"
        '
        'GroundTruthsToolStripMenuItem
        '
        Me.GroundTruthsToolStripMenuItem.Name = "GroundTruthsToolStripMenuItem"
        Me.GroundTruthsToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.GroundTruthsToolStripMenuItem.Text = "Ground truths"
        '
        'SingleEventToolStripMenuItem
        '
        Me.SingleEventToolStripMenuItem.Name = "SingleEventToolStripMenuItem"
        Me.SingleEventToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.SingleEventToolStripMenuItem.Text = "Single event"
        '
        'mnuProjectSeparator2
        '
        Me.mnuProjectSeparator2.Name = "mnuProjectSeparator2"
        Me.mnuProjectSeparator2.Size = New System.Drawing.Size(124, 6)
        '
        'mnuProjectProperties
        '
        Me.mnuProjectProperties.Name = "mnuProjectProperties"
        Me.mnuProjectProperties.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectProperties.Text = "Properties"
        '
        'mnuProjectSeparator3
        '
        Me.mnuProjectSeparator3.Name = "mnuProjectSeparator3"
        Me.mnuProjectSeparator3.Size = New System.Drawing.Size(124, 6)
        '
        'mnuProjectExit
        '
        Me.mnuProjectExit.Name = "mnuProjectExit"
        Me.mnuProjectExit.Size = New System.Drawing.Size(127, 22)
        Me.mnuProjectExit.Text = "Exit"
        '
        'mnuData
        '
        Me.mnuData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDataVideoRecordings, Me.mnuDataDetections, Me.ToolStripSeparator1, Me.mnuDataTableDesign, Me.ToolStripSeparator3, Me.mnuDataFilter})
        Me.mnuData.Name = "mnuData"
        Me.mnuData.Size = New System.Drawing.Size(43, 20)
        Me.mnuData.Text = "Data"
        '
        'mnuDataVideoRecordings
        '
        Me.mnuDataVideoRecordings.Name = "mnuDataVideoRecordings"
        Me.mnuDataVideoRecordings.Size = New System.Drawing.Size(163, 22)
        Me.mnuDataVideoRecordings.Text = "Video recordings"
        '
        'mnuDataDetections
        '
        Me.mnuDataDetections.Name = "mnuDataDetections"
        Me.mnuDataDetections.Size = New System.Drawing.Size(163, 22)
        Me.mnuDataDetections.Text = "Detections"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(160, 6)
        '
        'mnuDataTableDesign
        '
        Me.mnuDataTableDesign.Name = "mnuDataTableDesign"
        Me.mnuDataTableDesign.Size = New System.Drawing.Size(163, 22)
        Me.mnuDataTableDesign.Text = "Table design"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(160, 6)
        '
        'mnuDataFilter
        '
        Me.mnuDataFilter.Name = "mnuDataFilter"
        Me.mnuDataFilter.Size = New System.Drawing.Size(163, 22)
        Me.mnuDataFilter.Text = "Filter"
        '
        'mnuRecord
        '
        Me.mnuRecord.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRecordSmoothRoadUsers, Me.mnuRecordCreateDuplicate, Me.mnuRecordSeparator1, Me.mnuRecordLoadGates, Me.Create_Gates_Strudl, Me.mnuRecordSeparator2, Me.mnuRecordCalculatreTTC, Me.mnuRecordCalculateDistance, Me.mnuRecordClearTTC, Me.mnuRecordSeparator3, Me.ExportToolStripMenuItem, Me.mnuRecordMultiple, Me.ExportGateDataToolStripMenuItem})
        Me.mnuRecord.Name = "mnuRecord"
        Me.mnuRecord.Size = New System.Drawing.Size(56, 20)
        Me.mnuRecord.Text = "Record"
        '
        'mnuRecordSmoothRoadUsers
        '
        Me.mnuRecordSmoothRoadUsers.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CurvedToolStripMenuItem, Me.StraightToolStripMenuItem})
        Me.mnuRecordSmoothRoadUsers.Name = "mnuRecordSmoothRoadUsers"
        Me.mnuRecordSmoothRoadUsers.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordSmoothRoadUsers.Text = "Smooth trajectories"
        '
        'CurvedToolStripMenuItem
        '
        Me.CurvedToolStripMenuItem.Name = "CurvedToolStripMenuItem"
        Me.CurvedToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.CurvedToolStripMenuItem.Text = "Curved"
        '
        'StraightToolStripMenuItem
        '
        Me.StraightToolStripMenuItem.Name = "StraightToolStripMenuItem"
        Me.StraightToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.StraightToolStripMenuItem.Text = "Straight"
        '
        'mnuRecordCreateDuplicate
        '
        Me.mnuRecordCreateDuplicate.Name = "mnuRecordCreateDuplicate"
        Me.mnuRecordCreateDuplicate.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordCreateDuplicate.Text = "Create duplicate"
        '
        'mnuRecordSeparator1
        '
        Me.mnuRecordSeparator1.Name = "mnuRecordSeparator1"
        Me.mnuRecordSeparator1.Size = New System.Drawing.Size(228, 6)
        '
        'mnuRecordLoadGates
        '
        Me.mnuRecordLoadGates.Name = "mnuRecordLoadGates"
        Me.mnuRecordLoadGates.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordLoadGates.Text = "Load gates"
        '
        'Create_Gates_Strudl
        '
        Me.Create_Gates_Strudl.Name = "Create_Gates_Strudl"
        Me.Create_Gates_Strudl.Size = New System.Drawing.Size(231, 22)
        Me.Create_Gates_Strudl.Text = "Create gates (strudl)"
        Me.Create_Gates_Strudl.Visible = False
        '
        'mnuRecordSeparator2
        '
        Me.mnuRecordSeparator2.Name = "mnuRecordSeparator2"
        Me.mnuRecordSeparator2.Size = New System.Drawing.Size(228, 6)
        '
        'mnuRecordCalculatreTTC
        '
        Me.mnuRecordCalculatreTTC.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRecordCalculateTTCTrajectoryBased, Me.mnuRecordCalculateTTCStraight})
        Me.mnuRecordCalculatreTTC.Name = "mnuRecordCalculatreTTC"
        Me.mnuRecordCalculatreTTC.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordCalculatreTTC.Text = "Calculate TTC"
        '
        'mnuRecordCalculateTTCTrajectoryBased
        '
        Me.mnuRecordCalculateTTCTrajectoryBased.Name = "mnuRecordCalculateTTCTrajectoryBased"
        Me.mnuRecordCalculateTTCTrajectoryBased.Size = New System.Drawing.Size(161, 22)
        Me.mnuRecordCalculateTTCTrajectoryBased.Text = "Trajectory-based"
        '
        'mnuRecordCalculateTTCStraight
        '
        Me.mnuRecordCalculateTTCStraight.Name = "mnuRecordCalculateTTCStraight"
        Me.mnuRecordCalculateTTCStraight.Size = New System.Drawing.Size(161, 22)
        Me.mnuRecordCalculateTTCStraight.Text = "Straight"
        '
        'mnuRecordCalculateDistance
        '
        Me.mnuRecordCalculateDistance.Name = "mnuRecordCalculateDistance"
        Me.mnuRecordCalculateDistance.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordCalculateDistance.Text = "Calculate distances"
        '
        'mnuRecordClearTTC
        '
        Me.mnuRecordClearTTC.Name = "mnuRecordClearTTC"
        Me.mnuRecordClearTTC.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordClearTTC.Text = "Clear indicators"
        '
        'mnuRecordSeparator3
        '
        Me.mnuRecordSeparator3.Name = "mnuRecordSeparator3"
        Me.mnuRecordSeparator3.Size = New System.Drawing.Size(228, 6)
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.ExportToolStripMenuItem.Text = "Export Trajectories/Detections"
        '
        'mnuRecordMultiple
        '
        Me.mnuRecordMultiple.Name = "mnuRecordMultiple"
        Me.mnuRecordMultiple.Size = New System.Drawing.Size(231, 22)
        Me.mnuRecordMultiple.Text = "Export Safety Indicators"
        '
        'ExportGateDataToolStripMenuItem
        '
        Me.ExportGateDataToolStripMenuItem.AutoToolTip = True
        Me.ExportGateDataToolStripMenuItem.Name = "ExportGateDataToolStripMenuItem"
        Me.ExportGateDataToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.ExportGateDataToolStripMenuItem.Text = "Export Gate Data"
        Me.ExportGateDataToolStripMenuItem.ToolTipText = "Exports gate data. Select output folder followed by gate file"
        '
        'mnuDisplay
        '
        Me.mnuDisplay.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDisplayRoadUsers, Me.mnuDisplayTrajectories, Me.mnuDisplaySpeedVectors, Me.ToolStripSeparator5, Me.mnuDisplayZoomView, Me.ToolStripSeparator7, Me.mnuDisplayMap, Me.mnuDisplayCamera1, Me.mnuDisplayCamera2, Me.mnuDisplayCamera3, Me.mnuDisplayCamera4, Me.mnuDisplayCamera5, Me.mnuDisplayCamera6, Me.mnuDisplayCamera7, Me.mnuDisplayCamera8, Me.mnuDisplayCamera9, Me.mnuDisplayCamera10, Me.mnuDisplayCamera11, Me.mnuDisplayCamera12, Me.ToolStripSeparator6, Me.mnuDisplaySettings})
        Me.mnuDisplay.Name = "mnuDisplay"
        Me.mnuDisplay.Size = New System.Drawing.Size(57, 20)
        Me.mnuDisplay.Text = "Display"
        '
        'mnuDisplayRoadUsers
        '
        Me.mnuDisplayRoadUsers.Name = "mnuDisplayRoadUsers"
        Me.mnuDisplayRoadUsers.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayRoadUsers.Text = "Road Users"
        '
        'mnuDisplayTrajectories
        '
        Me.mnuDisplayTrajectories.Name = "mnuDisplayTrajectories"
        Me.mnuDisplayTrajectories.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayTrajectories.Text = "Trajectories"
        '
        'mnuDisplaySpeedVectors
        '
        Me.mnuDisplaySpeedVectors.Name = "mnuDisplaySpeedVectors"
        Me.mnuDisplaySpeedVectors.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplaySpeedVectors.Text = "Speed vectors"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(144, 6)
        '
        'mnuDisplayZoomView
        '
        Me.mnuDisplayZoomView.Name = "mnuDisplayZoomView"
        Me.mnuDisplayZoomView.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayZoomView.Text = "Zoom view"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(144, 6)
        '
        'mnuDisplayMap
        '
        Me.mnuDisplayMap.Name = "mnuDisplayMap"
        Me.mnuDisplayMap.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayMap.Text = "Map"
        '
        'mnuDisplayCamera1
        '
        Me.mnuDisplayCamera1.Name = "mnuDisplayCamera1"
        Me.mnuDisplayCamera1.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera1.Text = "Camera 1"
        '
        'mnuDisplayCamera2
        '
        Me.mnuDisplayCamera2.Name = "mnuDisplayCamera2"
        Me.mnuDisplayCamera2.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera2.Text = "Camera 2"
        '
        'mnuDisplayCamera3
        '
        Me.mnuDisplayCamera3.Name = "mnuDisplayCamera3"
        Me.mnuDisplayCamera3.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera3.Text = "Camera 3"
        '
        'mnuDisplayCamera4
        '
        Me.mnuDisplayCamera4.Name = "mnuDisplayCamera4"
        Me.mnuDisplayCamera4.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera4.Text = "Camera 4"
        '
        'mnuDisplayCamera5
        '
        Me.mnuDisplayCamera5.Name = "mnuDisplayCamera5"
        Me.mnuDisplayCamera5.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera5.Text = "Camera 5"
        '
        'mnuDisplayCamera6
        '
        Me.mnuDisplayCamera6.Name = "mnuDisplayCamera6"
        Me.mnuDisplayCamera6.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera6.Text = "Camera 6"
        '
        'mnuDisplayCamera7
        '
        Me.mnuDisplayCamera7.Name = "mnuDisplayCamera7"
        Me.mnuDisplayCamera7.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera7.Text = "Camera 7"
        '
        'mnuDisplayCamera8
        '
        Me.mnuDisplayCamera8.Name = "mnuDisplayCamera8"
        Me.mnuDisplayCamera8.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera8.Text = "Camera 8"
        '
        'mnuDisplayCamera9
        '
        Me.mnuDisplayCamera9.Name = "mnuDisplayCamera9"
        Me.mnuDisplayCamera9.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera9.Text = "Camera 9"
        '
        'mnuDisplayCamera10
        '
        Me.mnuDisplayCamera10.Name = "mnuDisplayCamera10"
        Me.mnuDisplayCamera10.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera10.Text = "Camera 10"
        '
        'mnuDisplayCamera11
        '
        Me.mnuDisplayCamera11.Name = "mnuDisplayCamera11"
        Me.mnuDisplayCamera11.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera11.Text = "Camera 11"
        '
        'mnuDisplayCamera12
        '
        Me.mnuDisplayCamera12.Name = "mnuDisplayCamera12"
        Me.mnuDisplayCamera12.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplayCamera12.Text = "Camera 12"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(144, 6)
        '
        'mnuDisplaySettings
        '
        Me.mnuDisplaySettings.Name = "mnuDisplaySettings"
        Me.mnuDisplaySettings.Size = New System.Drawing.Size(147, 22)
        Me.mnuDisplaySettings.Text = "Settings"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelpAbout, Me.ToolStripMenuItem2})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "Help"
        '
        'mnuHelpAbout
        '
        Me.mnuHelpAbout.Name = "mnuHelpAbout"
        Me.mnuHelpAbout.Size = New System.Drawing.Size(107, 22)
        Me.mnuHelpAbout.Text = "About"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(107, 22)
        Me.ToolStripMenuItem2.Text = "1"
        Me.ToolStripMenuItem2.Visible = False
        '
        'stsMain
        '
        Me.stsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stsWorldCoordinates, Me.stsImageCoordinates, Me.stsOnePixelError})
        Me.stsMain.Location = New System.Drawing.Point(0, 540)
        Me.stsMain.Name = "stsMain"
        Me.stsMain.Size = New System.Drawing.Size(626, 22)
        Me.stsMain.TabIndex = 2
        Me.stsMain.Text = "StatusStrip1"
        '
        'stsWorldCoordinates
        '
        Me.stsWorldCoordinates.AutoSize = False
        Me.stsWorldCoordinates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.stsWorldCoordinates.Name = "stsWorldCoordinates"
        Me.stsWorldCoordinates.Size = New System.Drawing.Size(180, 17)
        Me.stsWorldCoordinates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'stsImageCoordinates
        '
        Me.stsImageCoordinates.AutoSize = False
        Me.stsImageCoordinates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.stsImageCoordinates.Name = "stsImageCoordinates"
        Me.stsImageCoordinates.Size = New System.Drawing.Size(120, 17)
        Me.stsImageCoordinates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'stsOnePixelError
        '
        Me.stsOnePixelError.AutoSize = False
        Me.stsOnePixelError.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.stsOnePixelError.Name = "stsOnePixelError"
        Me.stsOnePixelError.Size = New System.Drawing.Size(120, 17)
        Me.stsOnePixelError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlbMain
        '
        Me.tlbMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tlbNew, Me.tlbOpen})
        Me.tlbMain.Location = New System.Drawing.Point(0, 24)
        Me.tlbMain.Name = "tlbMain"
        Me.tlbMain.Size = New System.Drawing.Size(626, 25)
        Me.tlbMain.TabIndex = 3
        Me.tlbMain.Text = "ToolStrip1"
        '
        'tlbNew
        '
        Me.tlbNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbNew.Image = CType(resources.GetObject("tlbNew.Image"), System.Drawing.Image)
        Me.tlbNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbNew.Name = "tlbNew"
        Me.tlbNew.Size = New System.Drawing.Size(23, 22)
        Me.tlbNew.Text = "ToolStripButton1"
        Me.tlbNew.ToolTipText = "Create new project"
        '
        'tlbOpen
        '
        Me.tlbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbOpen.Image = CType(resources.GetObject("tlbOpen.Image"), System.Drawing.Image)
        Me.tlbOpen.ImageTransparentColor = System.Drawing.Color.White
        Me.tlbOpen.Name = "tlbOpen"
        Me.tlbOpen.Size = New System.Drawing.Size(23, 22)
        Me.tlbOpen.Text = "Load Project"
        Me.tlbOpen.ToolTipText = "Load project"
        '
        'digOpenFile
        '
        Me.digOpenFile.FileName = "OpenFileDialog1"
        '
        'grpMainData
        '
        Me.grpMainData.Controls.Add(Me.btnMedia)
        Me.grpMainData.Controls.Add(Me.pnlNavigationButtons)
        Me.grpMainData.Controls.Add(Me.btnMap)
        Me.grpMainData.Controls.Add(Me.lblType)
        Me.grpMainData.Controls.Add(Me.cmbType)
        Me.grpMainData.Controls.Add(Me.btnDelete)
        Me.grpMainData.Controls.Add(Me.btnRelatedRecords)
        Me.grpMainData.Controls.Add(Me.btnNew)
        Me.grpMainData.Controls.Add(Me.lblComment)
        Me.grpMainData.Controls.Add(Me.txtComment)
        Me.grpMainData.Controls.Add(Me.lblTime)
        Me.grpMainData.Controls.Add(Me.lblStatus)
        Me.grpMainData.Controls.Add(Me.cmbStatus)
        Me.grpMainData.Controls.Add(Me.lblID)
        Me.grpMainData.Controls.Add(Me.dtpTime)
        Me.grpMainData.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMainData.Location = New System.Drawing.Point(0, 52)
        Me.grpMainData.Name = "grpMainData"
        Me.grpMainData.Size = New System.Drawing.Size(360, 230)
        Me.grpMainData.TabIndex = 4
        Me.grpMainData.TabStop = False
        Me.grpMainData.Text = "Layout Mode"
        '
        'btnMedia
        '
        Me.btnMedia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedia.Location = New System.Drawing.Point(229, 30)
        Me.btnMedia.Name = "btnMedia"
        Me.btnMedia.Size = New System.Drawing.Size(60, 21)
        Me.btnMedia.TabIndex = 147
        Me.btnMedia.Text = "Media"
        Me.btnMedia.UseVisualStyleBackColor = True
        '
        'pnlNavigationButtons
        '
        Me.pnlNavigationButtons.Controls.Add(Me.lblCurrentRecord)
        Me.pnlNavigationButtons.Controls.Add(Me.btnFirst)
        Me.pnlNavigationButtons.Controls.Add(Me.btnJumpBack)
        Me.pnlNavigationButtons.Controls.Add(Me.btnJumpForward)
        Me.pnlNavigationButtons.Controls.Add(Me.btnLast)
        Me.pnlNavigationButtons.Controls.Add(Me.btnPrevious)
        Me.pnlNavigationButtons.Controls.Add(Me.btnNext)
        Me.pnlNavigationButtons.Location = New System.Drawing.Point(6, 183)
        Me.pnlNavigationButtons.Name = "pnlNavigationButtons"
        Me.pnlNavigationButtons.Size = New System.Drawing.Size(347, 39)
        Me.pnlNavigationButtons.TabIndex = 146
        '
        'lblCurrentRecord
        '
        Me.lblCurrentRecord.AllowDrop = True
        Me.lblCurrentRecord.AutoSize = True
        Me.lblCurrentRecord.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentRecord.Location = New System.Drawing.Point(96, 9)
        Me.lblCurrentRecord.Name = "lblCurrentRecord"
        Me.lblCurrentRecord.Size = New System.Drawing.Size(54, 20)
        Me.lblCurrentRecord.TabIndex = 147
        Me.lblCurrentRecord.Text = "88888"
        Me.lblCurrentRecord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnFirst
        '
        Me.btnFirst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFirst.Location = New System.Drawing.Point(6, 6)
        Me.btnFirst.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(30, 25)
        Me.btnFirst.TabIndex = 146
        Me.btnFirst.Text = "|<<"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnJumpBack
        '
        Me.btnJumpBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJumpBack.Location = New System.Drawing.Point(36, 5)
        Me.btnJumpBack.Margin = New System.Windows.Forms.Padding(0)
        Me.btnJumpBack.Name = "btnJumpBack"
        Me.btnJumpBack.Size = New System.Drawing.Size(30, 28)
        Me.btnJumpBack.TabIndex = 145
        Me.btnJumpBack.Text = "<<"
        Me.btnJumpBack.UseVisualStyleBackColor = True
        '
        'btnJumpForward
        '
        Me.btnJumpForward.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJumpForward.Location = New System.Drawing.Point(181, 6)
        Me.btnJumpForward.Margin = New System.Windows.Forms.Padding(0)
        Me.btnJumpForward.Name = "btnJumpForward"
        Me.btnJumpForward.Size = New System.Drawing.Size(30, 28)
        Me.btnJumpForward.TabIndex = 144
        Me.btnJumpForward.Text = ">>"
        Me.btnJumpForward.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLast.Location = New System.Drawing.Point(211, 8)
        Me.btnLast.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(30, 25)
        Me.btnLast.TabIndex = 143
        Me.btnLast.Text = ">>|"
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrevious.Location = New System.Drawing.Point(66, 3)
        Me.btnPrevious.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(30, 32)
        Me.btnPrevious.TabIndex = 142
        Me.btnPrevious.Text = "<"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.Location = New System.Drawing.Point(151, 3)
        Me.btnNext.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(30, 32)
        Me.btnNext.TabIndex = 141
        Me.btnNext.Text = ">"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnMap
        '
        Me.btnMap.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMap.Location = New System.Drawing.Point(295, 30)
        Me.btnMap.Name = "btnMap"
        Me.btnMap.Size = New System.Drawing.Size(60, 21)
        Me.btnMap.TabIndex = 142
        Me.btnMap.Text = "Video"
        Me.btnMap.UseVisualStyleBackColor = True
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(27, 106)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(31, 13)
        Me.lblType.TabIndex = 140
        Me.lblType.Text = "Type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(62, 106)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(144, 21)
        Me.cmbType.TabIndex = 139
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(295, 155)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(60, 22)
        Me.btnDelete.TabIndex = 138
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnRelatedRecords
        '
        Me.btnRelatedRecords.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnRelatedRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRelatedRecords.ImageKey = "RelatedDetections"
        Me.btnRelatedRecords.ImageList = Me.imlButtons
        Me.btnRelatedRecords.Location = New System.Drawing.Point(295, 65)
        Me.btnRelatedRecords.Name = "btnRelatedRecords"
        Me.btnRelatedRecords.Size = New System.Drawing.Size(60, 60)
        Me.btnRelatedRecords.TabIndex = 137
        Me.btnRelatedRecords.UseVisualStyleBackColor = True
        '
        'imlButtons
        '
        Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlButtons.TransparentColor = System.Drawing.Color.White
        Me.imlButtons.Images.SetKeyName(0, "RelatedDetections")
        Me.imlButtons.Images.SetKeyName(1, "ParentVideoRecording")
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Location = New System.Drawing.Point(295, 131)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(60, 22)
        Me.btnNew.TabIndex = 136
        Me.btnNew.Text = "Add New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'lblComment
        '
        Me.lblComment.AutoSize = True
        Me.lblComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComment.Location = New System.Drawing.Point(7, 131)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(51, 13)
        Me.lblComment.TabIndex = 135
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComment
        '
        Me.txtComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComment.Location = New System.Drawing.Point(61, 133)
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(228, 44)
        Me.txtComment.TabIndex = 134
        '
        'lblTime
        '
        Me.lblTime.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(22, 56)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(30, 13)
        Me.lblTime.TabIndex = 128
        Me.lblTime.Text = "Time"
        Me.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(17, 81)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(37, 13)
        Me.lblStatus.TabIndex = 127
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbStatus
        '
        Me.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbStatus.FormattingEnabled = True
        Me.cmbStatus.Location = New System.Drawing.Point(62, 81)
        Me.cmbStatus.Name = "cmbStatus"
        Me.cmbStatus.Size = New System.Drawing.Size(144, 21)
        Me.cmbStatus.TabIndex = 126
        '
        'lblID
        '
        Me.lblID.AllowDrop = True
        Me.lblID.AutoSize = True
        Me.lblID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblID.Location = New System.Drawing.Point(36, 38)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(57, 13)
        Me.lblID.TabIndex = 125
        Me.lblID.Text = "ID   00000"
        '
        'dtpTime
        '
        Me.dtpTime.CustomFormat = "yyyy-MM-dd   HH:mm:ss"
        Me.dtpTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTime.Location = New System.Drawing.Point(61, 57)
        Me.dtpTime.Name = "dtpTime"
        Me.dtpTime.Size = New System.Drawing.Size(144, 20)
        Me.dtpTime.TabIndex = 122
        '
        'grpUserControls
        '
        Me.grpUserControls.Location = New System.Drawing.Point(0, 476)
        Me.grpUserControls.Name = "grpUserControls"
        Me.grpUserControls.Size = New System.Drawing.Size(360, 56)
        Me.grpUserControls.TabIndex = 143
        Me.grpUserControls.TabStop = False
        Me.grpUserControls.Text = "User-defined fields"
        '
        'grpRoadUsers
        '
        Me.grpRoadUsers.Controls.Add(Me.chkRUKey2)
        Me.grpRoadUsers.Controls.Add(Me.lblRUweight)
        Me.grpRoadUsers.Controls.Add(Me.chkRUKey1)
        Me.grpRoadUsers.Controls.Add(Me.txtRUweight)
        Me.grpRoadUsers.Controls.Add(Me.cmbRUType)
        Me.grpRoadUsers.Controls.Add(Me.btnImportRU)
        Me.grpRoadUsers.Controls.Add(Me.lblRUlength)
        Me.grpRoadUsers.Controls.Add(Me.txtRUlength)
        Me.grpRoadUsers.Controls.Add(Me.lblRUheight)
        Me.grpRoadUsers.Controls.Add(Me.txtRUheight)
        Me.grpRoadUsers.Controls.Add(Me.lblWidth)
        Me.grpRoadUsers.Controls.Add(Me.txtRUwidth)
        Me.grpRoadUsers.Controls.Add(Me.lblRUtype)
        Me.grpRoadUsers.Controls.Add(Me.btnDeleteRU)
        Me.grpRoadUsers.Controls.Add(Me.btnNewRU)
        Me.grpRoadUsers.Controls.Add(Me.btnEditRU)
        Me.grpRoadUsers.Controls.Add(Me.lstRoadUsers)
        Me.grpRoadUsers.Location = New System.Drawing.Point(0, 288)
        Me.grpRoadUsers.Name = "grpRoadUsers"
        Me.grpRoadUsers.Size = New System.Drawing.Size(360, 182)
        Me.grpRoadUsers.TabIndex = 144
        Me.grpRoadUsers.TabStop = False
        Me.grpRoadUsers.Text = "Trajectories"
        '
        'chkRUKey2
        '
        Me.chkRUKey2.AutoSize = True
        Me.chkRUKey2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRUKey2.ForeColor = System.Drawing.Color.DarkGreen
        Me.chkRUKey2.Location = New System.Drawing.Point(209, 159)
        Me.chkRUKey2.Name = "chkRUKey2"
        Me.chkRUKey2.Size = New System.Drawing.Size(58, 17)
        Me.chkRUKey2.TabIndex = 17
        Me.chkRUKey2.Text = "Key 2"
        Me.chkRUKey2.UseVisualStyleBackColor = True
        '
        'lblRUweight
        '
        Me.lblRUweight.AutoSize = True
        Me.lblRUweight.Location = New System.Drawing.Point(146, 124)
        Me.lblRUweight.Name = "lblRUweight"
        Me.lblRUweight.Size = New System.Drawing.Size(59, 13)
        Me.lblRUweight.TabIndex = 19
        Me.lblRUweight.Text = "Weight, kg"
        '
        'chkRUKey1
        '
        Me.chkRUKey1.AutoSize = True
        Me.chkRUKey1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRUKey1.ForeColor = System.Drawing.Color.Red
        Me.chkRUKey1.Location = New System.Drawing.Point(149, 159)
        Me.chkRUKey1.Name = "chkRUKey1"
        Me.chkRUKey1.Size = New System.Drawing.Size(58, 17)
        Me.chkRUKey1.TabIndex = 16
        Me.chkRUKey1.Text = "Key 1"
        Me.chkRUKey1.UseVisualStyleBackColor = True
        '
        'txtRUweight
        '
        Me.txtRUweight.Location = New System.Drawing.Point(206, 121)
        Me.txtRUweight.Name = "txtRUweight"
        Me.txtRUweight.Size = New System.Drawing.Size(61, 20)
        Me.txtRUweight.TabIndex = 18
        '
        'cmbRUType
        '
        Me.cmbRUType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRUType.FormattingEnabled = True
        Me.cmbRUType.Location = New System.Drawing.Point(156, 20)
        Me.cmbRUType.Name = "cmbRUType"
        Me.cmbRUType.Size = New System.Drawing.Size(111, 21)
        Me.cmbRUType.TabIndex = 15
        '
        'btnImportRU
        '
        Me.btnImportRU.Enabled = False
        Me.btnImportRU.Location = New System.Drawing.Point(295, 47)
        Me.btnImportRU.Name = "btnImportRU"
        Me.btnImportRU.Size = New System.Drawing.Size(60, 22)
        Me.btnImportRU.TabIndex = 12
        Me.btnImportRU.Text = "Import"
        Me.btnImportRU.UseVisualStyleBackColor = True
        '
        'lblRUlength
        '
        Me.lblRUlength.AutoSize = True
        Me.lblRUlength.Location = New System.Drawing.Point(146, 48)
        Me.lblRUlength.Name = "lblRUlength"
        Me.lblRUlength.Size = New System.Drawing.Size(54, 13)
        Me.lblRUlength.TabIndex = 11
        Me.lblRUlength.Text = "Length, m"
        '
        'txtRUlength
        '
        Me.txtRUlength.Location = New System.Drawing.Point(206, 45)
        Me.txtRUlength.Name = "txtRUlength"
        Me.txtRUlength.Size = New System.Drawing.Size(61, 20)
        Me.txtRUlength.TabIndex = 10
        '
        'lblRUheight
        '
        Me.lblRUheight.AutoSize = True
        Me.lblRUheight.Location = New System.Drawing.Point(146, 98)
        Me.lblRUheight.Name = "lblRUheight"
        Me.lblRUheight.Size = New System.Drawing.Size(52, 13)
        Me.lblRUheight.TabIndex = 9
        Me.lblRUheight.Text = "Height, m"
        '
        'txtRUheight
        '
        Me.txtRUheight.Location = New System.Drawing.Point(206, 95)
        Me.txtRUheight.Name = "txtRUheight"
        Me.txtRUheight.Size = New System.Drawing.Size(61, 20)
        Me.txtRUheight.TabIndex = 8
        '
        'lblWidth
        '
        Me.lblWidth.AutoSize = True
        Me.lblWidth.Location = New System.Drawing.Point(146, 73)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(49, 13)
        Me.lblWidth.TabIndex = 7
        Me.lblWidth.Text = "Width, m"
        '
        'txtRUwidth
        '
        Me.txtRUwidth.Location = New System.Drawing.Point(206, 70)
        Me.txtRUwidth.Name = "txtRUwidth"
        Me.txtRUwidth.Size = New System.Drawing.Size(61, 20)
        Me.txtRUwidth.TabIndex = 6
        '
        'lblRUtype
        '
        Me.lblRUtype.AutoSize = True
        Me.lblRUtype.Location = New System.Drawing.Point(119, 23)
        Me.lblRUtype.Name = "lblRUtype"
        Me.lblRUtype.Size = New System.Drawing.Size(31, 13)
        Me.lblRUtype.TabIndex = 5
        Me.lblRUtype.Text = "Type"
        '
        'btnDeleteRU
        '
        Me.btnDeleteRU.Enabled = False
        Me.btnDeleteRU.Location = New System.Drawing.Point(295, 131)
        Me.btnDeleteRU.Name = "btnDeleteRU"
        Me.btnDeleteRU.Size = New System.Drawing.Size(60, 22)
        Me.btnDeleteRU.TabIndex = 3
        Me.btnDeleteRU.Text = "Delete"
        Me.btnDeleteRU.UseVisualStyleBackColor = True
        '
        'btnNewRU
        '
        Me.btnNewRU.Enabled = False
        Me.btnNewRU.Location = New System.Drawing.Point(295, 107)
        Me.btnNewRU.Name = "btnNewRU"
        Me.btnNewRU.Size = New System.Drawing.Size(60, 22)
        Me.btnNewRU.TabIndex = 2
        Me.btnNewRU.Text = "New"
        Me.btnNewRU.UseVisualStyleBackColor = True
        '
        'btnEditRU
        '
        Me.btnEditRU.Location = New System.Drawing.Point(295, 14)
        Me.btnEditRU.Name = "btnEditRU"
        Me.btnEditRU.Size = New System.Drawing.Size(60, 22)
        Me.btnEditRU.TabIndex = 1
        Me.btnEditRU.Text = "Edit"
        Me.btnEditRU.UseVisualStyleBackColor = True
        '
        'lstRoadUsers
        '
        Me.lstRoadUsers.FormattingEnabled = True
        Me.lstRoadUsers.Location = New System.Drawing.Point(6, 19)
        Me.lstRoadUsers.Name = "lstRoadUsers"
        Me.lstRoadUsers.Size = New System.Drawing.Size(99, 134)
        Me.lstRoadUsers.TabIndex = 0
        '
        'mnuRoadUser
        '
        Me.mnuRoadUser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRoaduserCheckGates})
        Me.mnuRoadUser.Name = "mnuRoadUser"
        Me.mnuRoadUser.Size = New System.Drawing.Size(139, 26)
        '
        'mnuRoaduserCheckGates
        '
        Me.mnuRoaduserCheckGates.Name = "mnuRoaduserCheckGates"
        Me.mnuRoaduserCheckGates.Size = New System.Drawing.Size(138, 22)
        Me.mnuRoaduserCheckGates.Text = "Check gates"
        '
        'btnHide
        '
        Me.btnHide.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHide.Location = New System.Drawing.Point(366, 277)
        Me.btnHide.Name = "btnHide"
        Me.btnHide.Size = New System.Drawing.Size(16, 30)
        Me.btnHide.TabIndex = 148
        Me.btnHide.Text = "<"
        Me.btnHide.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(626, 562)
        Me.Controls.Add(Me.btnHide)
        Me.Controls.Add(Me.grpRoadUsers)
        Me.Controls.Add(Me.grpUserControls)
        Me.Controls.Add(Me.grpMainData)
        Me.Controls.Add(Me.tlbMain)
        Me.Controls.Add(Me.stsMain)
        Me.Controls.Add(Me.mnuMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.stsMain.ResumeLayout(False)
        Me.stsMain.PerformLayout()
        Me.tlbMain.ResumeLayout(False)
        Me.tlbMain.PerformLayout()
        Me.grpMainData.ResumeLayout(False)
        Me.grpMainData.PerformLayout()
        Me.pnlNavigationButtons.ResumeLayout(False)
        Me.pnlNavigationButtons.PerformLayout()
        Me.grpRoadUsers.ResumeLayout(False)
        Me.grpRoadUsers.PerformLayout()
        Me.mnuRoadUser.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuProject As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectLoad As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuProjectImportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuProjectProperties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuProjectExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stsMain As System.Windows.Forms.StatusStrip
    Friend WithEvents tlbMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tlbOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents digOpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuDataVideoRecordings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDataDetections As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDataFilter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grpMainData As System.Windows.Forms.GroupBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnRelatedRecords As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cmbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents dtpTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents grpUserControls As System.Windows.Forms.GroupBox
    Friend WithEvents grpRoadUsers As System.Windows.Forms.GroupBox
    Friend WithEvents tltToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents imlButtons As System.Windows.Forms.ImageList
    Friend WithEvents lstRoadUsers As System.Windows.Forms.ListBox
    Friend WithEvents btnNewRU As System.Windows.Forms.Button
    Friend WithEvents btnEditRU As System.Windows.Forms.Button
    Friend WithEvents btnDeleteRU As System.Windows.Forms.Button
    Friend WithEvents lblRUlength As System.Windows.Forms.Label
    Friend WithEvents txtRUlength As System.Windows.Forms.TextBox
    Friend WithEvents lblRUheight As System.Windows.Forms.Label
    Friend WithEvents txtRUheight As System.Windows.Forms.TextBox
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents txtRUwidth As System.Windows.Forms.TextBox
    Friend WithEvents lblRUtype As System.Windows.Forms.Label
    Friend WithEvents btnImportRU As System.Windows.Forms.Button
    Friend WithEvents mnuProjectNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tlbNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents mnuRecord As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordSmoothRoadUsers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMap As System.Windows.Forms.Button
    Friend WithEvents digSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuDisplay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmbRUType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlNavigationButtons As System.Windows.Forms.Panel
    Friend WithEvents lblCurrentRecord As System.Windows.Forms.Label
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnJumpBack As System.Windows.Forms.Button
    Friend WithEvents btnJumpForward As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents mnuRecordCalculatreTTC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuRecordClearTTC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordCalculateTTCStraight As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordCalculateTTCTrajectoryBased As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CurvedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StraightToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkRUKey1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkRUKey2 As System.Windows.Forms.CheckBox
    Friend WithEvents mnuProjectImportDataIndividualVideos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDataTableDesign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDisplaySettings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblRUweight As System.Windows.Forms.Label
    Friend WithEvents txtRUweight As System.Windows.Forms.TextBox
    Friend WithEvents mnuRoadUser As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuRoaduserCheckGates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordLoadGates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecordSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDisplayRoadUsers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayTrajectories As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDisplayMap As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDisplaySpeedVectors As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProjectExportVideos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroundTruthsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnHide As System.Windows.Forms.Button
    Friend WithEvents mnuDisplayCamera7 As ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera8 As ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera9 As ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera10 As ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera11 As ToolStripMenuItem
    Friend WithEvents mnuDisplayCamera12 As ToolStripMenuItem
    Friend WithEvents btnMedia As Button
    Friend WithEvents mnuProjectImportDataRubaOutput As ToolStripMenuItem
    Friend WithEvents mnuRecordMultiple As ToolStripMenuItem
    Friend WithEvents mnuDisplayZoomView As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As ToolStripSeparator
    Friend WithEvents stsWorldCoordinates As ToolStripStatusLabel
    Friend WithEvents stsImageCoordinates As ToolStripStatusLabel
    Friend WithEvents stsOnePixelError As ToolStripStatusLabel
    Friend WithEvents mnuRecordSeparator1 As ToolStripSeparator
    Friend WithEvents mnuRecordCreateDuplicate As ToolStripMenuItem
    Friend WithEvents ExportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuProjectImportExportedEvent As ToolStripMenuItem
    Friend WithEvents ExportGateDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Create_Gates_Strudl As ToolStripMenuItem
    Friend WithEvents SingleEventToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuRecordCalculateDistance As ToolStripMenuItem
End Class
