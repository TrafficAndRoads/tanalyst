﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrVideoFilesList
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lstVideoFiles = New System.Windows.Forms.ListBox()
    Me.txtVideoFileComment = New System.Windows.Forms.TextBox()
    Me.btnVideoFileUp = New System.Windows.Forms.Button()
    Me.btnVideoFileDown = New System.Windows.Forms.Button()
    Me.btnVideoFileRemove = New System.Windows.Forms.Button()
    Me.btnVideoFileAdd = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'lstVideoFiles
    '
    Me.lstVideoFiles.FormattingEnabled = True
    Me.lstVideoFiles.Items.AddRange(New Object() {"Line 1", "Line 2", "Line 3"})
    Me.lstVideoFiles.Location = New System.Drawing.Point(6, 14)
    Me.lstVideoFiles.Name = "lstVideoFiles"
    Me.lstVideoFiles.Size = New System.Drawing.Size(95, 43)
    Me.lstVideoFiles.TabIndex = 115
    '
    'txtVideoFileComment
    '
    Me.txtVideoFileComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtVideoFileComment.Location = New System.Drawing.Point(3, 63)
    Me.txtVideoFileComment.Multiline = True
    Me.txtVideoFileComment.Name = "txtVideoFileComment"
    Me.txtVideoFileComment.Size = New System.Drawing.Size(189, 19)
    Me.txtVideoFileComment.TabIndex = 114
    '
    'btnVideoFileUp
    '
    Me.btnVideoFileUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileUp.ImageKey = "Up"
    Me.btnVideoFileUp.Location = New System.Drawing.Point(104, 7)
    Me.btnVideoFileUp.Name = "btnVideoFileUp"
    Me.btnVideoFileUp.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileUp.TabIndex = 113
    Me.btnVideoFileUp.UseVisualStyleBackColor = True
    '
    'btnVideoFileDown
    '
    Me.btnVideoFileDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileDown.ImageKey = "Down"
    Me.btnVideoFileDown.Location = New System.Drawing.Point(104, 32)
    Me.btnVideoFileDown.Name = "btnVideoFileDown"
    Me.btnVideoFileDown.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileDown.TabIndex = 112
    Me.btnVideoFileDown.UseVisualStyleBackColor = True
    '
    'btnVideoFileRemove
    '
    Me.btnVideoFileRemove.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileRemove.ImageKey = "Remove"
    Me.btnVideoFileRemove.Location = New System.Drawing.Point(167, 23)
    Me.btnVideoFileRemove.Name = "btnVideoFileRemove"
    Me.btnVideoFileRemove.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileRemove.TabIndex = 111
    Me.btnVideoFileRemove.UseVisualStyleBackColor = True
    '
    'btnVideoFileAdd
    '
    Me.btnVideoFileAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileAdd.ImageKey = "AddNew"
    Me.btnVideoFileAdd.Location = New System.Drawing.Point(142, 23)
    Me.btnVideoFileAdd.Name = "btnVideoFileAdd"
    Me.btnVideoFileAdd.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileAdd.TabIndex = 110
    Me.btnVideoFileAdd.UseVisualStyleBackColor = True
    '
    'usrVideoFilesList
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.lstVideoFiles)
    Me.Controls.Add(Me.txtVideoFileComment)
    Me.Controls.Add(Me.btnVideoFileUp)
    Me.Controls.Add(Me.btnVideoFileDown)
    Me.Controls.Add(Me.btnVideoFileRemove)
    Me.Controls.Add(Me.btnVideoFileAdd)
    Me.Name = "usrVideoFilesList"
    Me.Size = New System.Drawing.Size(194, 89)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents lstVideoFiles As System.Windows.Forms.ListBox
  Friend WithEvents txtVideoFileComment As System.Windows.Forms.TextBox
  Friend WithEvents btnVideoFileUp As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileDown As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileRemove As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileAdd As System.Windows.Forms.Button

End Class
