﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapVideo
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapVideo))
    Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
    Me.tabMapVideo = New System.Windows.Forms.TabControl()
    Me.tbpVideo = New System.Windows.Forms.TabPage()
    Me.lblVideoTimeDescripancyLabel = New System.Windows.Forms.Label()
    Me.lblVideoLengthLabel = New System.Windows.Forms.Label()
    Me.lblVideoLength = New System.Windows.Forms.Label()
    Me.lblVideoFrameCount = New System.Windows.Forms.Label()
    Me.lblVideoFPS = New System.Windows.Forms.Label()
    Me.lblVideoSize = New System.Windows.Forms.Label()
    Me.lblVideoFPSLabel = New System.Windows.Forms.Label()
    Me.lblVideoSizeLabel = New System.Windows.Forms.Label()
    Me.lblVideoFrameCountLabel = New System.Windows.Forms.Label()
    Me.picVideo = New System.Windows.Forms.PictureBox()
    Me.lblVideoTimeDescripancyValue = New System.Windows.Forms.Label()
    Me.btnVideoCalibration = New System.Windows.Forms.Button()
    Me.btnVideoSynchronise = New System.Windows.Forms.Button()
    Me.lstVideo = New System.Windows.Forms.ListBox()
    Me.txtVideoComment = New System.Windows.Forms.TextBox()
    Me.btnVideoUp = New System.Windows.Forms.Button()
    Me.btnVideoDown = New System.Windows.Forms.Button()
    Me.btnVideoRemove = New System.Windows.Forms.Button()
    Me.btnVideoAdd = New System.Windows.Forms.Button()
    Me.tbpMap = New System.Windows.Forms.TabPage()
    Me.lblMapdYvalue = New System.Windows.Forms.Label()
    Me.lblMapXvalue = New System.Windows.Forms.Label()
    Me.lblMapYvalue = New System.Windows.Forms.Label()
    Me.lblMapdXvalue = New System.Windows.Forms.Label()
    Me.lblMapScaleValue = New System.Windows.Forms.Label()
    Me.lblMapAlphaValue = New System.Windows.Forms.Label()
    Me.lblMapAlphaLabel = New System.Windows.Forms.Label()
    Me.lblMapXlabel = New System.Windows.Forms.Label()
    Me.lblMapYlabel = New System.Windows.Forms.Label()
    Me.lblMapdXlabel = New System.Windows.Forms.Label()
    Me.lblMapdYlabel = New System.Windows.Forms.Label()
    Me.txtMapComment = New System.Windows.Forms.TextBox()
    Me.btnMapClear = New System.Windows.Forms.Button()
    Me.btnMapLoad = New System.Windows.Forms.Button()
    Me.lblMapSizeValue = New System.Windows.Forms.Label()
    Me.lblMapSizeLabel = New System.Windows.Forms.Label()
    Me.lblMapScaleLabel = New System.Windows.Forms.Label()
    Me.picMap = New System.Windows.Forms.PictureBox()
    Me.txtMap = New System.Windows.Forms.TextBox()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.tabMapVideo.SuspendLayout()
    Me.tbpVideo.SuspendLayout()
    CType(Me.picVideo, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tbpMap.SuspendLayout()
    CType(Me.picMap, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'imlButtons
    '
    Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.imlButtons.TransparentColor = System.Drawing.Color.White
    Me.imlButtons.Images.SetKeyName(0, "Up")
    Me.imlButtons.Images.SetKeyName(1, "Down")
    Me.imlButtons.Images.SetKeyName(2, "AddNew")
    Me.imlButtons.Images.SetKeyName(3, "Remove")
    '
    'tabMapVideo
    '
    Me.tabMapVideo.Controls.Add(Me.tbpVideo)
    Me.tabMapVideo.Controls.Add(Me.tbpMap)
    Me.tabMapVideo.Location = New System.Drawing.Point(10, 10)
    Me.tabMapVideo.Name = "tabMapVideo"
    Me.tabMapVideo.SelectedIndex = 0
    Me.tabMapVideo.Size = New System.Drawing.Size(438, 261)
    Me.tabMapVideo.TabIndex = 122
    '
    'tbpVideo
    '
    Me.tbpVideo.Controls.Add(Me.lblVideoTimeDescripancyLabel)
    Me.tbpVideo.Controls.Add(Me.lblVideoLengthLabel)
    Me.tbpVideo.Controls.Add(Me.lblVideoLength)
    Me.tbpVideo.Controls.Add(Me.lblVideoFrameCount)
    Me.tbpVideo.Controls.Add(Me.lblVideoFPS)
    Me.tbpVideo.Controls.Add(Me.lblVideoSize)
    Me.tbpVideo.Controls.Add(Me.lblVideoFPSLabel)
    Me.tbpVideo.Controls.Add(Me.lblVideoSizeLabel)
    Me.tbpVideo.Controls.Add(Me.lblVideoFrameCountLabel)
    Me.tbpVideo.Controls.Add(Me.picVideo)
    Me.tbpVideo.Controls.Add(Me.lblVideoTimeDescripancyValue)
    Me.tbpVideo.Controls.Add(Me.btnVideoCalibration)
    Me.tbpVideo.Controls.Add(Me.btnVideoSynchronise)
    Me.tbpVideo.Controls.Add(Me.lstVideo)
    Me.tbpVideo.Controls.Add(Me.txtVideoComment)
    Me.tbpVideo.Controls.Add(Me.btnVideoUp)
    Me.tbpVideo.Controls.Add(Me.btnVideoDown)
    Me.tbpVideo.Controls.Add(Me.btnVideoRemove)
    Me.tbpVideo.Controls.Add(Me.btnVideoAdd)
    Me.tbpVideo.Location = New System.Drawing.Point(4, 22)
    Me.tbpVideo.Name = "tbpVideo"
    Me.tbpVideo.Padding = New System.Windows.Forms.Padding(3)
    Me.tbpVideo.Size = New System.Drawing.Size(430, 235)
    Me.tbpVideo.TabIndex = 1
    Me.tbpVideo.Text = "Video"
    Me.tbpVideo.UseVisualStyleBackColor = True
    '
    'lblVideoTimeDescripancyLabel
    '
    Me.lblVideoTimeDescripancyLabel.AutoSize = True
    Me.lblVideoTimeDescripancyLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoTimeDescripancyLabel.Location = New System.Drawing.Point(248, 138)
    Me.lblVideoTimeDescripancyLabel.Name = "lblVideoTimeDescripancyLabel"
    Me.lblVideoTimeDescripancyLabel.Size = New System.Drawing.Size(26, 13)
    Me.lblVideoTimeDescripancyLabel.TabIndex = 162
    Me.lblVideoTimeDescripancyLabel.Text = "dT:"
    Me.lblVideoTimeDescripancyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoLengthLabel
    '
    Me.lblVideoLengthLabel.AutoSize = True
    Me.lblVideoLengthLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoLengthLabel.Location = New System.Drawing.Point(224, 7)
    Me.lblVideoLengthLabel.Name = "lblVideoLengthLabel"
    Me.lblVideoLengthLabel.Size = New System.Drawing.Size(50, 13)
    Me.lblVideoLengthLabel.TabIndex = 161
    Me.lblVideoLengthLabel.Text = "Length:"
    Me.lblVideoLengthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblVideoLengthLabel.Visible = False
    '
    'lblVideoLength
    '
    Me.lblVideoLength.AutoSize = True
    Me.lblVideoLength.Location = New System.Drawing.Point(273, 7)
    Me.lblVideoLength.Name = "lblVideoLength"
    Me.lblVideoLength.Size = New System.Drawing.Size(43, 13)
    Me.lblVideoLength.TabIndex = 160
    Me.lblVideoLength.Text = "0:30:00"
    Me.lblVideoLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblVideoLength.Visible = False
    '
    'lblVideoFrameCount
    '
    Me.lblVideoFrameCount.AutoSize = True
    Me.lblVideoFrameCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoFrameCount.Location = New System.Drawing.Point(273, 47)
    Me.lblVideoFrameCount.Name = "lblVideoFrameCount"
    Me.lblVideoFrameCount.Size = New System.Drawing.Size(37, 13)
    Me.lblVideoFrameCount.TabIndex = 158
    Me.lblVideoFrameCount.Text = "38145"
    Me.lblVideoFrameCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoFPS
    '
    Me.lblVideoFPS.AutoSize = True
    Me.lblVideoFPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoFPS.Location = New System.Drawing.Point(273, 67)
    Me.lblVideoFPS.Name = "lblVideoFPS"
    Me.lblVideoFPS.Size = New System.Drawing.Size(40, 13)
    Me.lblVideoFPS.TabIndex = 157
    Me.lblVideoFPS.Text = "21,139"
    Me.lblVideoFPS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoSize
    '
    Me.lblVideoSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblVideoSize.AutoSize = True
    Me.lblVideoSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoSize.Location = New System.Drawing.Point(273, 27)
    Me.lblVideoSize.Name = "lblVideoSize"
    Me.lblVideoSize.Size = New System.Drawing.Size(48, 13)
    Me.lblVideoSize.TabIndex = 156
    Me.lblVideoSize.Text = "640x480"
    Me.lblVideoSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoFPSLabel
    '
    Me.lblVideoFPSLabel.AutoSize = True
    Me.lblVideoFPSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoFPSLabel.Location = New System.Drawing.Point(240, 67)
    Me.lblVideoFPSLabel.Name = "lblVideoFPSLabel"
    Me.lblVideoFPSLabel.Size = New System.Drawing.Size(34, 13)
    Me.lblVideoFPSLabel.TabIndex = 155
    Me.lblVideoFPSLabel.Text = "FPS:"
    Me.lblVideoFPSLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoSizeLabel
    '
    Me.lblVideoSizeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblVideoSizeLabel.AutoSize = True
    Me.lblVideoSizeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoSizeLabel.Location = New System.Drawing.Point(203, 27)
    Me.lblVideoSizeLabel.Name = "lblVideoSizeLabel"
    Me.lblVideoSizeLabel.Size = New System.Drawing.Size(71, 13)
    Me.lblVideoSizeLabel.TabIndex = 154
    Me.lblVideoSizeLabel.Text = "Frame size:"
    Me.lblVideoSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblVideoFrameCountLabel
    '
    Me.lblVideoFrameCountLabel.AutoSize = True
    Me.lblVideoFrameCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblVideoFrameCountLabel.Location = New System.Drawing.Point(193, 47)
    Me.lblVideoFrameCountLabel.Name = "lblVideoFrameCountLabel"
    Me.lblVideoFrameCountLabel.Size = New System.Drawing.Size(81, 13)
    Me.lblVideoFrameCountLabel.TabIndex = 153
    Me.lblVideoFrameCountLabel.Text = "Frame count:"
    Me.lblVideoFrameCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'picVideo
    '
    Me.picVideo.Location = New System.Drawing.Point(324, 7)
    Me.picVideo.Name = "picVideo"
    Me.picVideo.Size = New System.Drawing.Size(100, 75)
    Me.picVideo.TabIndex = 150
    Me.picVideo.TabStop = False
    '
    'lblVideoTimeDescripancyValue
    '
    Me.lblVideoTimeDescripancyValue.AutoSize = True
    Me.lblVideoTimeDescripancyValue.Location = New System.Drawing.Point(273, 138)
    Me.lblVideoTimeDescripancyValue.Name = "lblVideoTimeDescripancyValue"
    Me.lblVideoTimeDescripancyValue.Size = New System.Drawing.Size(36, 13)
    Me.lblVideoTimeDescripancyValue.TabIndex = 148
    Me.lblVideoTimeDescripancyValue.Text = "0,56 s"
    Me.lblVideoTimeDescripancyValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'btnVideoCalibration
    '
    Me.btnVideoCalibration.Location = New System.Drawing.Point(349, 164)
    Me.btnVideoCalibration.Name = "btnVideoCalibration"
    Me.btnVideoCalibration.Size = New System.Drawing.Size(75, 22)
    Me.btnVideoCalibration.TabIndex = 144
    Me.btnVideoCalibration.Text = "Calibration"
    Me.btnVideoCalibration.UseVisualStyleBackColor = True
    '
    'btnVideoSynchronise
    '
    Me.btnVideoSynchronise.Location = New System.Drawing.Point(349, 133)
    Me.btnVideoSynchronise.Name = "btnVideoSynchronise"
    Me.btnVideoSynchronise.Size = New System.Drawing.Size(75, 22)
    Me.btnVideoSynchronise.TabIndex = 142
    Me.btnVideoSynchronise.Text = "Synchronise"
    Me.btnVideoSynchronise.UseVisualStyleBackColor = True
    Me.btnVideoSynchronise.Visible = False
    '
    'lstVideo
    '
    Me.lstVideo.FormattingEnabled = True
    Me.lstVideo.Items.AddRange(New Object() {"Line 1", "Line 2", "Line 3", "Line 4", "Line 5", "Line 6"})
    Me.lstVideo.Location = New System.Drawing.Point(6, 7)
    Me.lstVideo.Name = "lstVideo"
    Me.lstVideo.Size = New System.Drawing.Size(156, 134)
    Me.lstVideo.TabIndex = 141
    '
    'txtVideoComment
    '
    Me.txtVideoComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtVideoComment.Location = New System.Drawing.Point(6, 178)
    Me.txtVideoComment.Multiline = True
    Me.txtVideoComment.Name = "txtVideoComment"
    Me.txtVideoComment.Size = New System.Drawing.Size(156, 44)
    Me.txtVideoComment.TabIndex = 140
    '
    'btnVideoUp
    '
    Me.btnVideoUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoUp.ImageKey = "Up"
    Me.btnVideoUp.ImageList = Me.imlButtons
    Me.btnVideoUp.Location = New System.Drawing.Point(166, 53)
    Me.btnVideoUp.Name = "btnVideoUp"
    Me.btnVideoUp.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoUp.TabIndex = 139
    Me.btnVideoUp.UseVisualStyleBackColor = True
    '
    'btnVideoDown
    '
    Me.btnVideoDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoDown.ImageKey = "Down"
    Me.btnVideoDown.ImageList = Me.imlButtons
    Me.btnVideoDown.Location = New System.Drawing.Point(166, 84)
    Me.btnVideoDown.Name = "btnVideoDown"
    Me.btnVideoDown.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoDown.TabIndex = 138
    Me.btnVideoDown.UseVisualStyleBackColor = True
    '
    'btnVideoRemove
    '
    Me.btnVideoRemove.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoRemove.ImageKey = "Remove"
    Me.btnVideoRemove.ImageList = Me.imlButtons
    Me.btnVideoRemove.Location = New System.Drawing.Point(102, 147)
    Me.btnVideoRemove.Name = "btnVideoRemove"
    Me.btnVideoRemove.Size = New System.Drawing.Size(60, 25)
    Me.btnVideoRemove.TabIndex = 137
    Me.btnVideoRemove.TabStop = False
    Me.btnVideoRemove.UseVisualStyleBackColor = True
    '
    'btnVideoAdd
    '
    Me.btnVideoAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoAdd.ImageKey = "AddNew"
    Me.btnVideoAdd.ImageList = Me.imlButtons
    Me.btnVideoAdd.Location = New System.Drawing.Point(6, 147)
    Me.btnVideoAdd.Name = "btnVideoAdd"
    Me.btnVideoAdd.Size = New System.Drawing.Size(60, 25)
    Me.btnVideoAdd.TabIndex = 136
    Me.btnVideoAdd.TabStop = False
    Me.btnVideoAdd.UseVisualStyleBackColor = True
    '
    'tbpMap
    '
    Me.tbpMap.Controls.Add(Me.lblMapdYvalue)
    Me.tbpMap.Controls.Add(Me.lblMapXvalue)
    Me.tbpMap.Controls.Add(Me.lblMapYvalue)
    Me.tbpMap.Controls.Add(Me.lblMapdXvalue)
    Me.tbpMap.Controls.Add(Me.lblMapScaleValue)
    Me.tbpMap.Controls.Add(Me.lblMapAlphaValue)
    Me.tbpMap.Controls.Add(Me.lblMapAlphaLabel)
    Me.tbpMap.Controls.Add(Me.lblMapXlabel)
    Me.tbpMap.Controls.Add(Me.lblMapYlabel)
    Me.tbpMap.Controls.Add(Me.lblMapdXlabel)
    Me.tbpMap.Controls.Add(Me.lblMapdYlabel)
    Me.tbpMap.Controls.Add(Me.txtMapComment)
    Me.tbpMap.Controls.Add(Me.btnMapClear)
    Me.tbpMap.Controls.Add(Me.btnMapLoad)
    Me.tbpMap.Controls.Add(Me.lblMapSizeValue)
    Me.tbpMap.Controls.Add(Me.lblMapSizeLabel)
    Me.tbpMap.Controls.Add(Me.lblMapScaleLabel)
    Me.tbpMap.Controls.Add(Me.picMap)
    Me.tbpMap.Controls.Add(Me.txtMap)
    Me.tbpMap.Location = New System.Drawing.Point(4, 22)
    Me.tbpMap.Name = "tbpMap"
    Me.tbpMap.Padding = New System.Windows.Forms.Padding(3)
    Me.tbpMap.Size = New System.Drawing.Size(430, 235)
    Me.tbpMap.TabIndex = 0
    Me.tbpMap.Text = "Map"
    Me.tbpMap.UseVisualStyleBackColor = True
    '
    'lblMapdYvalue
    '
    Me.lblMapdYvalue.AutoSize = True
    Me.lblMapdYvalue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapdYvalue.Location = New System.Drawing.Point(355, 207)
    Me.lblMapdYvalue.Name = "lblMapdYvalue"
    Me.lblMapdYvalue.Size = New System.Drawing.Size(34, 13)
    Me.lblMapdYvalue.TabIndex = 166
    Me.lblMapdYvalue.Text = "0,536"
    Me.lblMapdYvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapdYvalue.Visible = False
    '
    'lblMapXvalue
    '
    Me.lblMapXvalue.AutoSize = True
    Me.lblMapXvalue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapXvalue.Location = New System.Drawing.Point(258, 183)
    Me.lblMapXvalue.Name = "lblMapXvalue"
    Me.lblMapXvalue.Size = New System.Drawing.Size(28, 13)
    Me.lblMapXvalue.TabIndex = 165
    Me.lblMapXvalue.Text = "42,5"
    Me.lblMapXvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapXvalue.Visible = False
    '
    'lblMapYvalue
    '
    Me.lblMapYvalue.AutoSize = True
    Me.lblMapYvalue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapYvalue.Location = New System.Drawing.Point(258, 207)
    Me.lblMapYvalue.Name = "lblMapYvalue"
    Me.lblMapYvalue.Size = New System.Drawing.Size(28, 13)
    Me.lblMapYvalue.TabIndex = 164
    Me.lblMapYvalue.Text = "15,5"
    Me.lblMapYvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapYvalue.Visible = False
    '
    'lblMapdXvalue
    '
    Me.lblMapdXvalue.AutoSize = True
    Me.lblMapdXvalue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapdXvalue.Location = New System.Drawing.Point(355, 183)
    Me.lblMapdXvalue.Name = "lblMapdXvalue"
    Me.lblMapdXvalue.Size = New System.Drawing.Size(34, 13)
    Me.lblMapdXvalue.TabIndex = 163
    Me.lblMapdXvalue.Text = "0,452"
    Me.lblMapdXvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapdXvalue.Visible = False
    '
    'lblMapScaleValue
    '
    Me.lblMapScaleValue.AutoSize = True
    Me.lblMapScaleValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapScaleValue.Location = New System.Drawing.Point(258, 160)
    Me.lblMapScaleValue.Name = "lblMapScaleValue"
    Me.lblMapScaleValue.Size = New System.Drawing.Size(75, 13)
    Me.lblMapScaleValue.TabIndex = 162
    Me.lblMapScaleValue.Text = "10,5 pxl/meter"
    Me.lblMapScaleValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblMapAlphaValue
    '
    Me.lblMapAlphaValue.AutoSize = True
    Me.lblMapAlphaValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapAlphaValue.Location = New System.Drawing.Point(258, 114)
    Me.lblMapAlphaValue.Name = "lblMapAlphaValue"
    Me.lblMapAlphaValue.Size = New System.Drawing.Size(28, 13)
    Me.lblMapAlphaValue.TabIndex = 161
    Me.lblMapAlphaValue.Text = "0,45"
    Me.lblMapAlphaValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblMapAlphaLabel
    '
    Me.lblMapAlphaLabel.AutoSize = True
    Me.lblMapAlphaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapAlphaLabel.Location = New System.Drawing.Point(213, 114)
    Me.lblMapAlphaLabel.Name = "lblMapAlphaLabel"
    Me.lblMapAlphaLabel.Size = New System.Drawing.Size(43, 13)
    Me.lblMapAlphaLabel.TabIndex = 159
    Me.lblMapAlphaLabel.Text = "Alpha:"
    Me.lblMapAlphaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblMapXlabel
    '
    Me.lblMapXlabel.AutoSize = True
    Me.lblMapXlabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapXlabel.Location = New System.Drawing.Point(237, 183)
    Me.lblMapXlabel.Name = "lblMapXlabel"
    Me.lblMapXlabel.Size = New System.Drawing.Size(19, 13)
    Me.lblMapXlabel.TabIndex = 157
    Me.lblMapXlabel.Text = "X:"
    Me.lblMapXlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapXlabel.Visible = False
    '
    'lblMapYlabel
    '
    Me.lblMapYlabel.AutoSize = True
    Me.lblMapYlabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapYlabel.Location = New System.Drawing.Point(237, 207)
    Me.lblMapYlabel.Name = "lblMapYlabel"
    Me.lblMapYlabel.Size = New System.Drawing.Size(19, 13)
    Me.lblMapYlabel.TabIndex = 155
    Me.lblMapYlabel.Text = "Y:"
    Me.lblMapYlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapYlabel.Visible = False
    '
    'lblMapdXlabel
    '
    Me.lblMapdXlabel.AutoSize = True
    Me.lblMapdXlabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapdXlabel.Location = New System.Drawing.Point(323, 183)
    Me.lblMapdXlabel.Name = "lblMapdXlabel"
    Me.lblMapdXlabel.Size = New System.Drawing.Size(26, 13)
    Me.lblMapdXlabel.TabIndex = 153
    Me.lblMapdXlabel.Text = "dX:"
    Me.lblMapdXlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapdXlabel.Visible = False
    '
    'lblMapdYlabel
    '
    Me.lblMapdYlabel.AutoSize = True
    Me.lblMapdYlabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapdYlabel.Location = New System.Drawing.Point(323, 207)
    Me.lblMapdYlabel.Name = "lblMapdYlabel"
    Me.lblMapdYlabel.Size = New System.Drawing.Size(26, 13)
    Me.lblMapdYlabel.TabIndex = 151
    Me.lblMapdYlabel.Text = "dY:"
    Me.lblMapdYlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapdYlabel.Visible = False
    '
    'txtMapComment
    '
    Me.txtMapComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtMapComment.Location = New System.Drawing.Point(6, 64)
    Me.txtMapComment.Multiline = True
    Me.txtMapComment.Name = "txtMapComment"
    Me.txtMapComment.Size = New System.Drawing.Size(156, 160)
    Me.txtMapComment.TabIndex = 147
    '
    'btnMapClear
    '
    Me.btnMapClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnMapClear.ImageKey = "Remove"
    Me.btnMapClear.ImageList = Me.imlButtons
    Me.btnMapClear.Location = New System.Drawing.Point(102, 33)
    Me.btnMapClear.Name = "btnMapClear"
    Me.btnMapClear.Size = New System.Drawing.Size(60, 25)
    Me.btnMapClear.TabIndex = 146
    Me.btnMapClear.UseVisualStyleBackColor = True
    '
    'btnMapLoad
    '
    Me.btnMapLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnMapLoad.ImageKey = "AddNew"
    Me.btnMapLoad.ImageList = Me.imlButtons
    Me.btnMapLoad.Location = New System.Drawing.Point(6, 33)
    Me.btnMapLoad.Name = "btnMapLoad"
    Me.btnMapLoad.Size = New System.Drawing.Size(60, 25)
    Me.btnMapLoad.TabIndex = 145
    Me.btnMapLoad.UseVisualStyleBackColor = True
    '
    'lblMapSizeValue
    '
    Me.lblMapSizeValue.AutoSize = True
    Me.lblMapSizeValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapSizeValue.Location = New System.Drawing.Point(258, 33)
    Me.lblMapSizeValue.Name = "lblMapSizeValue"
    Me.lblMapSizeValue.Size = New System.Drawing.Size(48, 13)
    Me.lblMapSizeValue.TabIndex = 46
    Me.lblMapSizeValue.Text = "640x480"
    Me.lblMapSizeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lblMapSizeLabel
    '
    Me.lblMapSizeLabel.AutoSize = True
    Me.lblMapSizeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapSizeLabel.Location = New System.Drawing.Point(221, 33)
    Me.lblMapSizeLabel.Name = "lblMapSizeLabel"
    Me.lblMapSizeLabel.Size = New System.Drawing.Size(35, 13)
    Me.lblMapSizeLabel.TabIndex = 45
    Me.lblMapSizeLabel.Text = "Size:"
    Me.lblMapSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblMapScaleLabel
    '
    Me.lblMapScaleLabel.AutoSize = True
    Me.lblMapScaleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapScaleLabel.Location = New System.Drawing.Point(215, 160)
    Me.lblMapScaleLabel.Name = "lblMapScaleLabel"
    Me.lblMapScaleLabel.Size = New System.Drawing.Size(43, 13)
    Me.lblMapScaleLabel.TabIndex = 42
    Me.lblMapScaleLabel.Text = "Scale:"
    Me.lblMapScaleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'picMap
    '
    Me.picMap.Location = New System.Drawing.Point(318, 7)
    Me.picMap.Name = "picMap"
    Me.picMap.Size = New System.Drawing.Size(100, 75)
    Me.picMap.TabIndex = 41
    Me.picMap.TabStop = False
    '
    'txtMap
    '
    Me.txtMap.Enabled = False
    Me.txtMap.Location = New System.Drawing.Point(6, 7)
    Me.txtMap.Name = "txtMap"
    Me.txtMap.ReadOnly = True
    Me.txtMap.Size = New System.Drawing.Size(156, 20)
    Me.txtMap.TabIndex = 37
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(388, 294)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 136
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'frmMapVideo
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(453, 322)
    Me.ControlBox = False
    Me.Controls.Add(Me.btnOK)
    Me.Controls.Add(Me.tabMapVideo)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmMapVideo"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "Map / Video"
    Me.tabMapVideo.ResumeLayout(False)
    Me.tbpVideo.ResumeLayout(False)
    Me.tbpVideo.PerformLayout()
    CType(Me.picVideo, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tbpMap.ResumeLayout(False)
    Me.tbpMap.PerformLayout()
    CType(Me.picMap, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents imlButtons As System.Windows.Forms.ImageList
  Friend WithEvents tabMapVideo As System.Windows.Forms.TabControl
  Friend WithEvents tbpMap As System.Windows.Forms.TabPage
  Friend WithEvents lblMapSizeLabel As System.Windows.Forms.Label
  Friend WithEvents lblMapScaleLabel As System.Windows.Forms.Label
  Friend WithEvents picMap As System.Windows.Forms.PictureBox
  Friend WithEvents txtMap As System.Windows.Forms.TextBox
  Friend WithEvents tbpVideo As System.Windows.Forms.TabPage
  Friend WithEvents lblVideoTimeDescripancyValue As System.Windows.Forms.Label
  Friend WithEvents btnVideoCalibration As System.Windows.Forms.Button
  Friend WithEvents btnVideoSynchronise As System.Windows.Forms.Button
  Friend WithEvents lstVideo As System.Windows.Forms.ListBox
  Friend WithEvents txtVideoComment As System.Windows.Forms.TextBox
  Friend WithEvents btnVideoUp As System.Windows.Forms.Button
  Friend WithEvents btnVideoDown As System.Windows.Forms.Button
  Friend WithEvents btnVideoRemove As System.Windows.Forms.Button
  Friend WithEvents btnVideoAdd As System.Windows.Forms.Button
  Friend WithEvents picVideo As System.Windows.Forms.PictureBox
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnMapClear As System.Windows.Forms.Button
  Friend WithEvents btnMapLoad As System.Windows.Forms.Button
  Friend WithEvents lblMapSizeValue As System.Windows.Forms.Label
  Friend WithEvents lblVideoSize As System.Windows.Forms.Label
  Friend WithEvents lblVideoFPSLabel As System.Windows.Forms.Label
  Friend WithEvents lblVideoSizeLabel As System.Windows.Forms.Label
  Friend WithEvents lblVideoFrameCountLabel As System.Windows.Forms.Label
  Friend WithEvents lblVideoFrameCount As System.Windows.Forms.Label
  Friend WithEvents lblVideoFPS As System.Windows.Forms.Label
  Friend WithEvents txtMapComment As System.Windows.Forms.TextBox
  Friend WithEvents lblMapXlabel As System.Windows.Forms.Label
  Friend WithEvents lblMapYlabel As System.Windows.Forms.Label
  Friend WithEvents lblMapdXlabel As System.Windows.Forms.Label
  Friend WithEvents lblMapdYlabel As System.Windows.Forms.Label
  Friend WithEvents lblMapAlphaLabel As Label
  Friend WithEvents lblVideoLengthLabel As Label
  Friend WithEvents lblVideoLength As Label
  Friend WithEvents lblMapAlphaValue As Label
  Friend WithEvents lblMapScaleValue As Label
  Friend WithEvents lblMapdYvalue As Label
  Friend WithEvents lblMapXvalue As Label
  Friend WithEvents lblMapYvalue As Label
  Friend WithEvents lblMapdXvalue As Label
  Friend WithEvents lblVideoTimeDescripancyLabel As Label
End Class
