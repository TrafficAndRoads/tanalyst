﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrFramePicker
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.picVideo = New System.Windows.Forms.PictureBox()
    Me.pnlNavigation = New System.Windows.Forms.Panel()
    Me.lblTimeStamp = New System.Windows.Forms.Label()
    Me.lblFrame = New System.Windows.Forms.Label()
    Me.btnFirst = New System.Windows.Forms.Button()
    Me.btnJumpBack = New System.Windows.Forms.Button()
    Me.btnJumpForward = New System.Windows.Forms.Button()
    Me.btnLast = New System.Windows.Forms.Button()
    Me.btnPrevious = New System.Windows.Forms.Button()
    Me.btnNext = New System.Windows.Forms.Button()
    CType(Me.picVideo, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.pnlNavigation.SuspendLayout()
    Me.SuspendLayout()
    '
    'picVideo
    '
    Me.picVideo.Location = New System.Drawing.Point(33, 16)
    Me.picVideo.Name = "picVideo"
    Me.picVideo.Size = New System.Drawing.Size(300, 200)
    Me.picVideo.TabIndex = 8
    Me.picVideo.TabStop = False
    '
    'pnlNavigation
    '
    Me.pnlNavigation.Controls.Add(Me.lblTimeStamp)
    Me.pnlNavigation.Controls.Add(Me.lblFrame)
    Me.pnlNavigation.Controls.Add(Me.btnFirst)
    Me.pnlNavigation.Controls.Add(Me.btnJumpBack)
    Me.pnlNavigation.Controls.Add(Me.btnJumpForward)
    Me.pnlNavigation.Controls.Add(Me.btnLast)
    Me.pnlNavigation.Controls.Add(Me.btnPrevious)
    Me.pnlNavigation.Controls.Add(Me.btnNext)
    Me.pnlNavigation.Location = New System.Drawing.Point(42, 231)
    Me.pnlNavigation.Name = "pnlNavigation"
    Me.pnlNavigation.Size = New System.Drawing.Size(246, 56)
    Me.pnlNavigation.TabIndex = 142
    '
    'lblTimeStamp
    '
    Me.lblTimeStamp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblTimeStamp.Location = New System.Drawing.Point(3, 2)
    Me.lblTimeStamp.Name = "lblTimeStamp"
    Me.lblTimeStamp.Size = New System.Drawing.Size(240, 15)
    Me.lblTimeStamp.TabIndex = 149
    Me.lblTimeStamp.Text = "18993,985 sec."
    Me.lblTimeStamp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblFrame
    '
    Me.lblFrame.AllowDrop = True
    Me.lblFrame.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblFrame.Location = New System.Drawing.Point(95, 22)
    Me.lblFrame.Name = "lblFrame"
    Me.lblFrame.Size = New System.Drawing.Size(54, 20)
    Me.lblFrame.TabIndex = 148
    Me.lblFrame.Text = "333333"
    Me.lblFrame.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btnFirst
    '
    Me.btnFirst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnFirst.Location = New System.Drawing.Point(5, 20)
    Me.btnFirst.Margin = New System.Windows.Forms.Padding(0)
    Me.btnFirst.Name = "btnFirst"
    Me.btnFirst.Size = New System.Drawing.Size(30, 25)
    Me.btnFirst.TabIndex = 147
    Me.btnFirst.TabStop = False
    Me.btnFirst.Text = "|<<"
    Me.btnFirst.UseVisualStyleBackColor = True
    '
    'btnJumpBack
    '
    Me.btnJumpBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnJumpBack.Location = New System.Drawing.Point(35, 19)
    Me.btnJumpBack.Margin = New System.Windows.Forms.Padding(0)
    Me.btnJumpBack.Name = "btnJumpBack"
    Me.btnJumpBack.Size = New System.Drawing.Size(30, 28)
    Me.btnJumpBack.TabIndex = 146
    Me.btnJumpBack.TabStop = False
    Me.btnJumpBack.Text = "<<"
    Me.btnJumpBack.UseVisualStyleBackColor = True
    '
    'btnJumpForward
    '
    Me.btnJumpForward.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnJumpForward.Location = New System.Drawing.Point(180, 20)
    Me.btnJumpForward.Margin = New System.Windows.Forms.Padding(0)
    Me.btnJumpForward.Name = "btnJumpForward"
    Me.btnJumpForward.Size = New System.Drawing.Size(30, 28)
    Me.btnJumpForward.TabIndex = 145
    Me.btnJumpForward.TabStop = False
    Me.btnJumpForward.Text = ">>"
    Me.btnJumpForward.UseVisualStyleBackColor = True
    '
    'btnLast
    '
    Me.btnLast.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnLast.Location = New System.Drawing.Point(210, 22)
    Me.btnLast.Margin = New System.Windows.Forms.Padding(0)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(30, 25)
    Me.btnLast.TabIndex = 144
    Me.btnLast.TabStop = False
    Me.btnLast.Text = ">>|"
    Me.btnLast.UseVisualStyleBackColor = True
    '
    'btnPrevious
    '
    Me.btnPrevious.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnPrevious.Location = New System.Drawing.Point(65, 17)
    Me.btnPrevious.Margin = New System.Windows.Forms.Padding(0)
    Me.btnPrevious.Name = "btnPrevious"
    Me.btnPrevious.Size = New System.Drawing.Size(30, 32)
    Me.btnPrevious.TabIndex = 143
    Me.btnPrevious.TabStop = False
    Me.btnPrevious.Text = "<"
    Me.btnPrevious.UseVisualStyleBackColor = True
    '
    'btnNext
    '
    Me.btnNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnNext.Location = New System.Drawing.Point(150, 17)
    Me.btnNext.Margin = New System.Windows.Forms.Padding(0)
    Me.btnNext.Name = "btnNext"
    Me.btnNext.Size = New System.Drawing.Size(30, 32)
    Me.btnNext.TabIndex = 142
    Me.btnNext.TabStop = False
    Me.btnNext.Text = ">"
    Me.btnNext.UseVisualStyleBackColor = True
    '
    'usrFramePicker
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.Controls.Add(Me.pnlNavigation)
    Me.Controls.Add(Me.picVideo)
    Me.Name = "usrFramePicker"
    Me.Size = New System.Drawing.Size(448, 374)
    CType(Me.picVideo, System.ComponentModel.ISupportInitialize).EndInit()
    Me.pnlNavigation.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents picVideo As System.Windows.Forms.PictureBox
  Friend WithEvents pnlNavigation As System.Windows.Forms.Panel
  Friend WithEvents lblTimeStamp As System.Windows.Forms.Label
  Friend WithEvents lblFrame As System.Windows.Forms.Label
  Friend WithEvents btnFirst As System.Windows.Forms.Button
  Friend WithEvents btnJumpBack As System.Windows.Forms.Button
  Friend WithEvents btnJumpForward As System.Windows.Forms.Button
  Friend WithEvents btnLast As System.Windows.Forms.Button
  Friend WithEvents btnPrevious As System.Windows.Forms.Button
  Friend WithEvents btnNext As System.Windows.Forms.Button

End Class
