﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMultiple
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TTCmin = New System.Windows.Forms.CheckBox()
        Me.T2 = New System.Windows.Forms.CheckBox()
        Me.T2last = New System.Windows.Forms.CheckBox()
        Me.Start = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.outputfolder = New System.Windows.Forms.Button()
        Me.output_name = New System.Windows.Forms.TextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.RecordNumber = New System.Windows.Forms.TextBox()
        Me.MaxRecordNumber = New System.Windows.Forms.TextBox()
        Me.T2DeltaV4 = New System.Windows.Forms.CheckBox()
        Me.T2DeltaV0 = New System.Windows.Forms.CheckBox()
        Me.T2DeltaV6 = New System.Windows.Forms.CheckBox()
        Me.T2DeltaV8 = New System.Windows.Forms.CheckBox()
        Me.PET = New System.Windows.Forms.CheckBox()
        Me.chkMinDistance = New System.Windows.Forms.CheckBox()
        Me.btnExportSpecial = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TTCmin
        '
        Me.TTCmin.AutoSize = True
        Me.TTCmin.Location = New System.Drawing.Point(12, 78)
        Me.TTCmin.Name = "TTCmin"
        Me.TTCmin.Size = New System.Drawing.Size(63, 17)
        Me.TTCmin.TabIndex = 0
        Me.TTCmin.Text = "TTCmin"
        Me.TTCmin.UseVisualStyleBackColor = True
        '
        'T2
        '
        Me.T2.AutoSize = True
        Me.T2.Location = New System.Drawing.Point(12, 101)
        Me.T2.Name = "T2"
        Me.T2.Size = New System.Drawing.Size(58, 17)
        Me.T2.TabIndex = 1
        Me.T2.Text = "T2 min"
        Me.T2.UseVisualStyleBackColor = True
        '
        'T2last
        '
        Me.T2last.AutoSize = True
        Me.T2last.Location = New System.Drawing.Point(12, 124)
        Me.T2last.Name = "T2last"
        Me.T2last.Size = New System.Drawing.Size(58, 17)
        Me.T2last.TabIndex = 2
        Me.T2last.Text = "T2 last"
        Me.T2last.UseVisualStyleBackColor = True
        '
        'Start
        '
        Me.Start.Location = New System.Drawing.Point(69, 182)
        Me.Start.Name = "Start"
        Me.Start.Size = New System.Drawing.Size(156, 23)
        Me.Start.TabIndex = 3
        Me.Start.Text = "Start"
        Me.Start.UseVisualStyleBackColor = True
        '
        'outputfolder
        '
        Me.outputfolder.Location = New System.Drawing.Point(69, 38)
        Me.outputfolder.Name = "outputfolder"
        Me.outputfolder.Size = New System.Drawing.Size(156, 23)
        Me.outputfolder.TabIndex = 4
        Me.outputfolder.Text = "Select output folder"
        Me.outputfolder.UseVisualStyleBackColor = True
        '
        'output_name
        '
        Me.output_name.Location = New System.Drawing.Point(12, 12)
        Me.output_name.Name = "output_name"
        Me.output_name.Size = New System.Drawing.Size(267, 20)
        Me.output_name.TabIndex = 5
        Me.output_name.Text = "Output.csv"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(35, 209)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(221, 23)
        Me.ProgressBar1.TabIndex = 6
        '
        'RecordNumber
        '
        Me.RecordNumber.Location = New System.Drawing.Point(1, 211)
        Me.RecordNumber.Name = "RecordNumber"
        Me.RecordNumber.Size = New System.Drawing.Size(29, 20)
        Me.RecordNumber.TabIndex = 7
        Me.RecordNumber.Text = "Cur"
        '
        'MaxRecordNumber
        '
        Me.MaxRecordNumber.Location = New System.Drawing.Point(259, 211)
        Me.MaxRecordNumber.Name = "MaxRecordNumber"
        Me.MaxRecordNumber.Size = New System.Drawing.Size(29, 20)
        Me.MaxRecordNumber.TabIndex = 8
        Me.MaxRecordNumber.Text = "Max"
        '
        'T2DeltaV4
        '
        Me.T2DeltaV4.AutoSize = True
        Me.T2DeltaV4.Location = New System.Drawing.Point(196, 101)
        Me.T2DeltaV4.Name = "T2DeltaV4"
        Me.T2DeltaV4.Size = New System.Drawing.Size(83, 17)
        Me.T2DeltaV4.TabIndex = 9
        Me.T2DeltaV4.Text = "T2-DeltaV 4"
        Me.T2DeltaV4.UseVisualStyleBackColor = True
        '
        'T2DeltaV0
        '
        Me.T2DeltaV0.AutoSize = True
        Me.T2DeltaV0.Location = New System.Drawing.Point(196, 78)
        Me.T2DeltaV0.Name = "T2DeltaV0"
        Me.T2DeltaV0.Size = New System.Drawing.Size(83, 17)
        Me.T2DeltaV0.TabIndex = 10
        Me.T2DeltaV0.Text = "T2-DeltaV 0"
        Me.T2DeltaV0.UseVisualStyleBackColor = True
        '
        'T2DeltaV6
        '
        Me.T2DeltaV6.AutoSize = True
        Me.T2DeltaV6.Location = New System.Drawing.Point(196, 124)
        Me.T2DeltaV6.Name = "T2DeltaV6"
        Me.T2DeltaV6.Size = New System.Drawing.Size(83, 17)
        Me.T2DeltaV6.TabIndex = 11
        Me.T2DeltaV6.Text = "T2-DeltaV 6"
        Me.T2DeltaV6.UseVisualStyleBackColor = True
        '
        'T2DeltaV8
        '
        Me.T2DeltaV8.AutoSize = True
        Me.T2DeltaV8.Location = New System.Drawing.Point(196, 147)
        Me.T2DeltaV8.Name = "T2DeltaV8"
        Me.T2DeltaV8.Size = New System.Drawing.Size(83, 17)
        Me.T2DeltaV8.TabIndex = 12
        Me.T2DeltaV8.Text = "T2-DeltaV 8"
        Me.T2DeltaV8.ThreeState = True
        Me.T2DeltaV8.UseVisualStyleBackColor = True
        '
        'PET
        '
        Me.PET.AutoSize = True
        Me.PET.Location = New System.Drawing.Point(12, 147)
        Me.PET.Name = "PET"
        Me.PET.Size = New System.Drawing.Size(47, 17)
        Me.PET.TabIndex = 13
        Me.PET.Text = "PET"
        Me.PET.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.PET.UseVisualStyleBackColor = True
        '
        'chkMinDistance
        '
        Me.chkMinDistance.AutoSize = True
        Me.chkMinDistance.Location = New System.Drawing.Point(358, 78)
        Me.chkMinDistance.Name = "chkMinDistance"
        Me.chkMinDistance.Size = New System.Drawing.Size(85, 17)
        Me.chkMinDistance.TabIndex = 14
        Me.chkMinDistance.Text = "MinDistance"
        Me.chkMinDistance.UseVisualStyleBackColor = True
        '
        'btnExportSpecial
        '
        Me.btnExportSpecial.Location = New System.Drawing.Point(330, 182)
        Me.btnExportSpecial.Name = "btnExportSpecial"
        Me.btnExportSpecial.Size = New System.Drawing.Size(156, 23)
        Me.btnExportSpecial.TabIndex = 15
        Me.btnExportSpecial.Text = "Export Special"
        Me.btnExportSpecial.UseVisualStyleBackColor = True
        '
        'frmMultiple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(571, 244)
        Me.Controls.Add(Me.btnExportSpecial)
        Me.Controls.Add(Me.chkMinDistance)
        Me.Controls.Add(Me.PET)
        Me.Controls.Add(Me.T2DeltaV8)
        Me.Controls.Add(Me.T2DeltaV6)
        Me.Controls.Add(Me.T2DeltaV0)
        Me.Controls.Add(Me.T2DeltaV4)
        Me.Controls.Add(Me.MaxRecordNumber)
        Me.Controls.Add(Me.RecordNumber)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.output_name)
        Me.Controls.Add(Me.outputfolder)
        Me.Controls.Add(Me.Start)
        Me.Controls.Add(Me.T2last)
        Me.Controls.Add(Me.T2)
        Me.Controls.Add(Me.TTCmin)
        Me.Name = "frmMultiple"
        Me.Text = "Multiple"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TTCmin As CheckBox
    Friend WithEvents T2 As CheckBox
    Friend WithEvents T2last As CheckBox
    Friend WithEvents Start As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents outputfolder As Button
    Friend WithEvents output_name As TextBox
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents RecordNumber As TextBox
    Friend WithEvents MaxRecordNumber As TextBox
    Friend WithEvents T2DeltaV4 As CheckBox
    Friend WithEvents T2DeltaV0 As CheckBox
    Friend WithEvents T2DeltaV6 As CheckBox
    Friend WithEvents T2DeltaV8 As CheckBox
    Friend WithEvents PET As CheckBox
    Friend WithEvents chkMinDistance As CheckBox
    Friend WithEvents btnExportSpecial As Button
End Class
