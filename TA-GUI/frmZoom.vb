﻿Imports System.ComponentModel



Public Class frmZoom
  Public Event BeingClosed()
  Public Event ZoomedViewMouseMove(ByVal Perspective As Integer, ByVal X As Integer, ByVal Y As Integer)
  Public Event ZoomedViewMouseDoubleClick(ByVal Perspective As Integer, ByVal X As Integer, ByVal Y As Integer)
  Public Event MouseWheelMoved(e As MouseEventArgs)
  Private MyPerspectives As List(Of Integer)
  Private MyXc, MyYc As List(Of Integer)




  Public Sub SetZoomedBitmap(ByVal ZoomedBitmap As Bitmap,
                             ByVal ZoomedPerspectives As List(Of Integer),
                             ByVal Xc As List(Of Integer),
                             ByVal Yc As List(Of Integer))

    MyPerspectives = ZoomedPerspectives
    MyXc = Xc
    MyYc = Yc

    If MyPerspectives.Count > 0 Then
      picZoomedView.Width = ZoomedBitmap.Width
      picZoomedView.Visible = True
    Else
      picZoomedView.Width = 300
      picZoomedView.Visible = False
    End If
    picZoomedView.Image = ZoomedBitmap
    Me.Width = picZoomedView.Width
  End Sub



  Private Sub frmZoom_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
    e.Cancel = True
    Me.Hide()
    RaiseEvent BeingClosed()
  End Sub


  Private Sub picZoomedView_MouseMove(sender As Object, e As MouseEventArgs) Handles picZoomedView.MouseMove
    Dim _Perspective As Integer
    Dim _OriginalX, _OriginalY As Integer

    Call TransformPointToWorld(e.X, e.Y, _Perspective, _OriginalX, _OriginalY)
    RaiseEvent ZoomedViewMouseMove(_Perspective, _OriginalX, _OriginalY)
  End Sub

  Private Sub picZoomedView_DoubleClick(sender As Object, e As MouseEventArgs) Handles picZoomedView.MouseDoubleClick
    Dim _Perspective As Integer
    Dim _OriginalX, _OriginalY As Integer

    Call TransformPointToWorld(e.X, e.Y, _Perspective, _OriginalX, _OriginalY)
    RaiseEvent ZoomedViewMouseDoubleClick(_Perspective, _OriginalX, _OriginalY)
  End Sub


  Private Sub TransformPointToWorld(ByVal X As Integer,
                                    ByVal Y As Integer,
                                    ByRef Perspective As Integer,
                                    ByRef OriginalX As Integer,
                                    ByRef OriginalY As Integer)

    Perspective = MyPerspectives(X \ 300)

    OriginalX = MyXc(X \ 300) + CInt((X - 300 * (X \ 300)) / 2 - 75)
    OriginalY = MyYc(X \ 300) + CInt(Y / 2 - 75)
  End Sub

  Private Sub frmZoom_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel
    RaiseEvent MouseWheelMoved(e)
  End Sub
End Class