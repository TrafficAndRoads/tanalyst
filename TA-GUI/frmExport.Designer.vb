﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.txtFramesBefore = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.txtFramesAfter = New System.Windows.Forms.TextBox()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.Location = New System.Drawing.Point(224, 228)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 5
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(158, 228)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 4
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'txtFramesBefore
    '
    Me.txtFramesBefore.Location = New System.Drawing.Point(127, 55)
    Me.txtFramesBefore.Name = "txtFramesBefore"
    Me.txtFramesBefore.Size = New System.Drawing.Size(78, 20)
    Me.txtFramesBefore.TabIndex = 6
    Me.txtFramesBefore.Text = "30"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(47, 58)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(74, 13)
    Me.Label1.TabIndex = 7
    Me.Label1.Text = "Frames before"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(47, 84)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(65, 13)
    Me.Label2.TabIndex = 9
    Me.Label2.Text = "Frames after"
    '
    'txtFramesAfter
    '
    Me.txtFramesAfter.Location = New System.Drawing.Point(127, 81)
    Me.txtFramesAfter.Name = "txtFramesAfter"
    Me.txtFramesAfter.Size = New System.Drawing.Size(78, 20)
    Me.txtFramesAfter.TabIndex = 8
    Me.txtFramesAfter.Text = "100"
    '
    'frmExport
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(284, 262)
    Me.ControlBox = False
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.txtFramesAfter)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.txtFramesBefore)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmExport"
    Me.ShowInTaskbar = False
    Me.Text = "Export detection"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents txtFramesBefore As System.Windows.Forms.TextBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents txtFramesAfter As System.Windows.Forms.TextBox
End Class
