﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.txtFilter = New System.Windows.Forms.TextBox()
    Me.cmdClearFilter = New System.Windows.Forms.Button()
    Me.cmdType = New System.Windows.Forms.Button()
    Me.cmdStatus = New System.Windows.Forms.Button()
    Me.cmdOR = New System.Windows.Forms.Button()
    Me.cmdAND = New System.Windows.Forms.Button()
    Me.btnID = New System.Windows.Forms.Button()
    Me.btnTrue = New System.Windows.Forms.Button()
    Me.btnConflict = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(171, 261)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 0
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(237, 261)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 1
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'txtFilter
    '
    Me.txtFilter.Location = New System.Drawing.Point(2, 161)
    Me.txtFilter.Multiline = True
    Me.txtFilter.Name = "txtFilter"
    Me.txtFilter.Size = New System.Drawing.Size(295, 94)
    Me.txtFilter.TabIndex = 2
    Me.txtFilter.Text = "[cmType] = 1"
    '
    'cmdClearFilter
    '
    Me.cmdClearFilter.Location = New System.Drawing.Point(237, 133)
    Me.cmdClearFilter.Name = "cmdClearFilter"
    Me.cmdClearFilter.Size = New System.Drawing.Size(60, 22)
    Me.cmdClearFilter.TabIndex = 3
    Me.cmdClearFilter.Text = "Clear"
    Me.cmdClearFilter.UseVisualStyleBackColor = True
    '
    'cmdType
    '
    Me.cmdType.Location = New System.Drawing.Point(12, 43)
    Me.cmdType.Name = "cmdType"
    Me.cmdType.Size = New System.Drawing.Size(60, 22)
    Me.cmdType.TabIndex = 4
    Me.cmdType.Text = "Type ="
    Me.cmdType.UseVisualStyleBackColor = True
    '
    'cmdStatus
    '
    Me.cmdStatus.Location = New System.Drawing.Point(12, 73)
    Me.cmdStatus.Name = "cmdStatus"
    Me.cmdStatus.Size = New System.Drawing.Size(60, 22)
    Me.cmdStatus.TabIndex = 5
    Me.cmdStatus.Text = "Status ="
    Me.cmdStatus.UseVisualStyleBackColor = True
    '
    'cmdOR
    '
    Me.cmdOR.Location = New System.Drawing.Point(114, 59)
    Me.cmdOR.Name = "cmdOR"
    Me.cmdOR.Size = New System.Drawing.Size(60, 22)
    Me.cmdOR.TabIndex = 7
    Me.cmdOR.Text = "OR"
    Me.cmdOR.UseVisualStyleBackColor = True
    '
    'cmdAND
    '
    Me.cmdAND.Location = New System.Drawing.Point(114, 29)
    Me.cmdAND.Name = "cmdAND"
    Me.cmdAND.Size = New System.Drawing.Size(60, 22)
    Me.cmdAND.TabIndex = 6
    Me.cmdAND.Text = "AND"
    Me.cmdAND.UseVisualStyleBackColor = True
    '
    'btnID
    '
    Me.btnID.Location = New System.Drawing.Point(12, 13)
    Me.btnID.Name = "btnID"
    Me.btnID.Size = New System.Drawing.Size(60, 22)
    Me.btnID.TabIndex = 8
    Me.btnID.Text = "ID ="
    Me.btnID.UseVisualStyleBackColor = True
    '
    'btnTrue
    '
    Me.btnTrue.Location = New System.Drawing.Point(114, 89)
    Me.btnTrue.Name = "btnTrue"
    Me.btnTrue.Size = New System.Drawing.Size(60, 22)
    Me.btnTrue.TabIndex = 9
    Me.btnTrue.Text = "TRUE"
    Me.btnTrue.UseVisualStyleBackColor = True
    '
    'btnConflict
    '
    Me.btnConflict.Location = New System.Drawing.Point(12, 103)
    Me.btnConflict.Name = "btnConflict"
    Me.btnConflict.Size = New System.Drawing.Size(60, 22)
    Me.btnConflict.TabIndex = 10
    Me.btnConflict.Text = "Conflict ="
    Me.btnConflict.UseVisualStyleBackColor = True
    '
    'frmFilter
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(298, 288)
    Me.ControlBox = False
    Me.Controls.Add(Me.btnConflict)
    Me.Controls.Add(Me.btnTrue)
    Me.Controls.Add(Me.btnID)
    Me.Controls.Add(Me.cmdAND)
    Me.Controls.Add(Me.cmdOR)
    Me.Controls.Add(Me.cmdStatus)
    Me.Controls.Add(Me.cmdType)
    Me.Controls.Add(Me.cmdClearFilter)
    Me.Controls.Add(Me.txtFilter)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmFilter"
    Me.ShowInTaskbar = False
    Me.Text = "Set record filter"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents txtFilter As System.Windows.Forms.TextBox
  Friend WithEvents cmdClearFilter As System.Windows.Forms.Button
  Friend WithEvents cmdType As System.Windows.Forms.Button
  Friend WithEvents cmdStatus As System.Windows.Forms.Button
  Friend WithEvents cmdOR As System.Windows.Forms.Button
  Friend WithEvents cmdAND As System.Windows.Forms.Button
  Friend WithEvents btnID As System.Windows.Forms.Button
  Friend WithEvents btnTrue As System.Windows.Forms.Button
  Friend WithEvents btnConflict As System.Windows.Forms.Button
End Class
