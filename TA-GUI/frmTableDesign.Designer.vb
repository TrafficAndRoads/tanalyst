﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTableDesign
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTableDesign))
    Me.btnOK = New System.Windows.Forms.Button()
    Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
    Me.pnlFields = New System.Windows.Forms.Panel()
    Me.txtFieldComment = New System.Windows.Forms.TextBox()
    Me.lstFields = New System.Windows.Forms.ListBox()
    Me.btnFieldRemove = New System.Windows.Forms.Button()
    Me.btnFieldAdd = New System.Windows.Forms.Button()
    Me.btnFieldDown = New System.Windows.Forms.Button()
    Me.btnFieldUp = New System.Windows.Forms.Button()
    Me.pnlValues = New System.Windows.Forms.Panel()
    Me.txtValueComment = New System.Windows.Forms.TextBox()
    Me.lstValues = New System.Windows.Forms.ListBox()
    Me.btnValueUp = New System.Windows.Forms.Button()
    Me.btnValueRemove = New System.Windows.Forms.Button()
    Me.btnValueDown = New System.Windows.Forms.Button()
    Me.btnValueAdd = New System.Windows.Forms.Button()
    Me.btnLockType = New System.Windows.Forms.Button()
    Me.lblValues = New System.Windows.Forms.Label()
    Me.lblLabel = New System.Windows.Forms.Label()
    Me.lblDecimals = New System.Windows.Forms.Label()
    Me.lblScale = New System.Windows.Forms.Label()
    Me.lblType = New System.Windows.Forms.Label()
    Me.cmbDecimals = New System.Windows.Forms.ComboBox()
    Me.cmbScale = New System.Windows.Forms.ComboBox()
    Me.txtLabel = New System.Windows.Forms.TextBox()
    Me.cmbType = New System.Windows.Forms.ComboBox()
    Me.pnlFields.SuspendLayout()
    Me.pnlValues.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(533, 484)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 137
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'imlButtons
    '
    Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.imlButtons.TransparentColor = System.Drawing.Color.White
    Me.imlButtons.Images.SetKeyName(0, "Up")
    Me.imlButtons.Images.SetKeyName(1, "Down")
    Me.imlButtons.Images.SetKeyName(2, "AddNew")
    Me.imlButtons.Images.SetKeyName(3, "Remove")
    '
    'pnlFields
    '
    Me.pnlFields.Controls.Add(Me.txtFieldComment)
    Me.pnlFields.Controls.Add(Me.lstFields)
    Me.pnlFields.Controls.Add(Me.btnFieldRemove)
    Me.pnlFields.Controls.Add(Me.btnFieldAdd)
    Me.pnlFields.Controls.Add(Me.btnFieldDown)
    Me.pnlFields.Controls.Add(Me.btnFieldUp)
    Me.pnlFields.Location = New System.Drawing.Point(12, 12)
    Me.pnlFields.Name = "pnlFields"
    Me.pnlFields.Size = New System.Drawing.Size(201, 382)
    Me.pnlFields.TabIndex = 163
    '
    'txtFieldComment
    '
    Me.txtFieldComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtFieldComment.Location = New System.Drawing.Point(6, 300)
    Me.txtFieldComment.Multiline = True
    Me.txtFieldComment.Name = "txtFieldComment"
    Me.txtFieldComment.Size = New System.Drawing.Size(187, 70)
    Me.txtFieldComment.TabIndex = 141
    '
    'lstFields
    '
    Me.lstFields.FormattingEnabled = True
    Me.lstFields.Location = New System.Drawing.Point(6, 7)
    Me.lstFields.Name = "lstFields"
    Me.lstFields.Size = New System.Drawing.Size(156, 251)
    Me.lstFields.TabIndex = 0
    '
    'btnFieldRemove
    '
    Me.btnFieldRemove.ImageKey = "Remove"
    Me.btnFieldRemove.ImageList = Me.imlButtons
    Me.btnFieldRemove.Location = New System.Drawing.Point(102, 260)
    Me.btnFieldRemove.Name = "btnFieldRemove"
    Me.btnFieldRemove.Size = New System.Drawing.Size(60, 25)
    Me.btnFieldRemove.TabIndex = 2
    Me.btnFieldRemove.UseVisualStyleBackColor = True
    '
    'btnFieldAdd
    '
    Me.btnFieldAdd.ImageKey = "AddNew"
    Me.btnFieldAdd.ImageList = Me.imlButtons
    Me.btnFieldAdd.Location = New System.Drawing.Point(6, 260)
    Me.btnFieldAdd.Name = "btnFieldAdd"
    Me.btnFieldAdd.Size = New System.Drawing.Size(60, 25)
    Me.btnFieldAdd.TabIndex = 3
    Me.btnFieldAdd.UseVisualStyleBackColor = True
    '
    'btnFieldDown
    '
    Me.btnFieldDown.ImageKey = "Down"
    Me.btnFieldDown.ImageList = Me.imlButtons
    Me.btnFieldDown.Location = New System.Drawing.Point(168, 84)
    Me.btnFieldDown.Name = "btnFieldDown"
    Me.btnFieldDown.Size = New System.Drawing.Size(25, 25)
    Me.btnFieldDown.TabIndex = 4
    Me.btnFieldDown.UseVisualStyleBackColor = True
    '
    'btnFieldUp
    '
    Me.btnFieldUp.ImageKey = "Up"
    Me.btnFieldUp.ImageList = Me.imlButtons
    Me.btnFieldUp.Location = New System.Drawing.Point(168, 53)
    Me.btnFieldUp.Name = "btnFieldUp"
    Me.btnFieldUp.Size = New System.Drawing.Size(25, 25)
    Me.btnFieldUp.TabIndex = 5
    Me.btnFieldUp.UseVisualStyleBackColor = True
    '
    'pnlValues
    '
    Me.pnlValues.Controls.Add(Me.txtValueComment)
    Me.pnlValues.Controls.Add(Me.lstValues)
    Me.pnlValues.Controls.Add(Me.btnValueUp)
    Me.pnlValues.Controls.Add(Me.btnValueRemove)
    Me.pnlValues.Controls.Add(Me.btnValueDown)
    Me.pnlValues.Controls.Add(Me.btnValueAdd)
    Me.pnlValues.Location = New System.Drawing.Point(332, 100)
    Me.pnlValues.Name = "pnlValues"
    Me.pnlValues.Size = New System.Drawing.Size(201, 307)
    Me.pnlValues.TabIndex = 164
    '
    'txtValueComment
    '
    Me.txtValueComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtValueComment.Location = New System.Drawing.Point(7, 212)
    Me.txtValueComment.Multiline = True
    Me.txtValueComment.Name = "txtValueComment"
    Me.txtValueComment.Size = New System.Drawing.Size(186, 82)
    Me.txtValueComment.TabIndex = 141
    '
    'lstValues
    '
    Me.lstValues.FormattingEnabled = True
    Me.lstValues.Location = New System.Drawing.Point(6, 7)
    Me.lstValues.Name = "lstValues"
    Me.lstValues.Size = New System.Drawing.Size(156, 160)
    Me.lstValues.TabIndex = 20
    '
    'btnValueUp
    '
    Me.btnValueUp.ImageKey = "Up"
    Me.btnValueUp.ImageList = Me.imlButtons
    Me.btnValueUp.Location = New System.Drawing.Point(168, 37)
    Me.btnValueUp.Name = "btnValueUp"
    Me.btnValueUp.Size = New System.Drawing.Size(25, 25)
    Me.btnValueUp.TabIndex = 24
    Me.btnValueUp.UseVisualStyleBackColor = True
    '
    'btnValueRemove
    '
    Me.btnValueRemove.ImageKey = "Remove"
    Me.btnValueRemove.ImageList = Me.imlButtons
    Me.btnValueRemove.Location = New System.Drawing.Point(102, 176)
    Me.btnValueRemove.Name = "btnValueRemove"
    Me.btnValueRemove.Size = New System.Drawing.Size(60, 25)
    Me.btnValueRemove.TabIndex = 21
    Me.btnValueRemove.UseVisualStyleBackColor = True
    '
    'btnValueDown
    '
    Me.btnValueDown.ImageKey = "Down"
    Me.btnValueDown.ImageList = Me.imlButtons
    Me.btnValueDown.Location = New System.Drawing.Point(168, 68)
    Me.btnValueDown.Name = "btnValueDown"
    Me.btnValueDown.Size = New System.Drawing.Size(25, 25)
    Me.btnValueDown.TabIndex = 23
    Me.btnValueDown.UseVisualStyleBackColor = True
    '
    'btnValueAdd
    '
    Me.btnValueAdd.ImageKey = "AddNew"
    Me.btnValueAdd.ImageList = Me.imlButtons
    Me.btnValueAdd.Location = New System.Drawing.Point(6, 176)
    Me.btnValueAdd.Name = "btnValueAdd"
    Me.btnValueAdd.Size = New System.Drawing.Size(60, 25)
    Me.btnValueAdd.TabIndex = 22
    Me.btnValueAdd.UseVisualStyleBackColor = True
    '
    'btnLockType
    '
    Me.btnLockType.Location = New System.Drawing.Point(501, 13)
    Me.btnLockType.Name = "btnLockType"
    Me.btnLockType.Size = New System.Drawing.Size(25, 25)
    Me.btnLockType.TabIndex = 166
    Me.btnLockType.UseVisualStyleBackColor = True
    Me.btnLockType.Visible = False
    '
    'lblValues
    '
    Me.lblValues.AutoSize = True
    Me.lblValues.Location = New System.Drawing.Point(282, 100)
    Me.lblValues.Name = "lblValues"
    Me.lblValues.Size = New System.Drawing.Size(42, 13)
    Me.lblValues.TabIndex = 165
    Me.lblValues.Text = "Values:"
    '
    'lblLabel
    '
    Me.lblLabel.AutoSize = True
    Me.lblLabel.Location = New System.Drawing.Point(288, 44)
    Me.lblLabel.Name = "lblLabel"
    Me.lblLabel.Size = New System.Drawing.Size(36, 13)
    Me.lblLabel.TabIndex = 162
    Me.lblLabel.Text = "Label:"
    '
    'lblDecimals
    '
    Me.lblDecimals.AutoSize = True
    Me.lblDecimals.Location = New System.Drawing.Point(271, 69)
    Me.lblDecimals.Name = "lblDecimals"
    Me.lblDecimals.Size = New System.Drawing.Size(53, 13)
    Me.lblDecimals.TabIndex = 161
    Me.lblDecimals.Text = "Decimals:"
    '
    'lblScale
    '
    Me.lblScale.AutoSize = True
    Me.lblScale.Location = New System.Drawing.Point(424, 69)
    Me.lblScale.Name = "lblScale"
    Me.lblScale.Size = New System.Drawing.Size(37, 13)
    Me.lblScale.TabIndex = 160
    Me.lblScale.Text = "Scale:"
    '
    'lblType
    '
    Me.lblType.AutoSize = True
    Me.lblType.Location = New System.Drawing.Point(290, 19)
    Me.lblType.Name = "lblType"
    Me.lblType.Size = New System.Drawing.Size(34, 13)
    Me.lblType.TabIndex = 159
    Me.lblType.Text = "Type:"
    '
    'cmbDecimals
    '
    Me.cmbDecimals.FormattingEnabled = True
    Me.cmbDecimals.Location = New System.Drawing.Point(332, 66)
    Me.cmbDecimals.Name = "cmbDecimals"
    Me.cmbDecimals.Size = New System.Drawing.Size(62, 21)
    Me.cmbDecimals.TabIndex = 158
    '
    'cmbScale
    '
    Me.cmbScale.FormattingEnabled = True
    Me.cmbScale.Location = New System.Drawing.Point(464, 66)
    Me.cmbScale.Name = "cmbScale"
    Me.cmbScale.Size = New System.Drawing.Size(62, 21)
    Me.cmbScale.TabIndex = 157
    '
    'txtLabel
    '
    Me.txtLabel.Location = New System.Drawing.Point(332, 41)
    Me.txtLabel.Name = "txtLabel"
    Me.txtLabel.Size = New System.Drawing.Size(194, 20)
    Me.txtLabel.TabIndex = 156
    '
    'cmbType
    '
    Me.cmbType.FormattingEnabled = True
    Me.cmbType.Location = New System.Drawing.Point(332, 16)
    Me.cmbType.Name = "cmbType"
    Me.cmbType.Size = New System.Drawing.Size(163, 21)
    Me.cmbType.TabIndex = 155
    '
    'frmTableDesign
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(609, 517)
    Me.ControlBox = False
    Me.Controls.Add(Me.pnlFields)
    Me.Controls.Add(Me.pnlValues)
    Me.Controls.Add(Me.btnLockType)
    Me.Controls.Add(Me.lblValues)
    Me.Controls.Add(Me.lblLabel)
    Me.Controls.Add(Me.lblDecimals)
    Me.Controls.Add(Me.lblScale)
    Me.Controls.Add(Me.lblType)
    Me.Controls.Add(Me.cmbDecimals)
    Me.Controls.Add(Me.cmbScale)
    Me.Controls.Add(Me.txtLabel)
    Me.Controls.Add(Me.cmbType)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmTableDesign"
    Me.Text = "Table design"
    Me.pnlFields.ResumeLayout(False)
    Me.pnlFields.PerformLayout()
    Me.pnlValues.ResumeLayout(False)
    Me.pnlValues.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents imlButtons As System.Windows.Forms.ImageList
  Friend WithEvents pnlFields As System.Windows.Forms.Panel
  Friend WithEvents lstFields As System.Windows.Forms.ListBox
  Friend WithEvents btnFieldRemove As System.Windows.Forms.Button
  Friend WithEvents btnFieldAdd As System.Windows.Forms.Button
  Friend WithEvents btnFieldDown As System.Windows.Forms.Button
  Friend WithEvents btnFieldUp As System.Windows.Forms.Button
  Friend WithEvents pnlValues As System.Windows.Forms.Panel
  Friend WithEvents lstValues As System.Windows.Forms.ListBox
  Friend WithEvents btnValueUp As System.Windows.Forms.Button
  Friend WithEvents btnValueRemove As System.Windows.Forms.Button
  Friend WithEvents btnValueDown As System.Windows.Forms.Button
  Friend WithEvents btnValueAdd As System.Windows.Forms.Button
  Friend WithEvents btnLockType As System.Windows.Forms.Button
  Friend WithEvents lblValues As System.Windows.Forms.Label
  Friend WithEvents lblLabel As System.Windows.Forms.Label
  Friend WithEvents lblDecimals As System.Windows.Forms.Label
  Friend WithEvents lblScale As System.Windows.Forms.Label
  Friend WithEvents lblType As System.Windows.Forms.Label
  Friend WithEvents cmbDecimals As System.Windows.Forms.ComboBox
  Friend WithEvents cmbScale As System.Windows.Forms.ComboBox
  Friend WithEvents txtLabel As System.Windows.Forms.TextBox
  Friend WithEvents cmbType As System.Windows.Forms.ComboBox
  Friend WithEvents txtFieldComment As System.Windows.Forms.TextBox
  Friend WithEvents txtValueComment As System.Windows.Forms.TextBox
End Class
