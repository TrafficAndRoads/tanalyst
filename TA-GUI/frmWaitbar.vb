﻿Public Class Waitbar


    Public Sub UserForm_Initialize(total As Integer)

        ProgressBar1.Maximum = total
        ProgressBar1.Value = 0
        Me.Show()

    End Sub

    Public Sub advance()

        ProgressBar1.Value = ProgressBar1.Value + 1

    End Sub

    Public Sub done()
        Me.Close()
    End Sub

End Class