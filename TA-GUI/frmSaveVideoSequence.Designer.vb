﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSaveVideoSequence
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnSave = New System.Windows.Forms.Button()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.txtStartFrame = New System.Windows.Forms.TextBox()
    Me.txtEndFrame = New System.Windows.Forms.TextBox()
    Me.lblStartFrame = New System.Windows.Forms.Label()
    Me.lblEndFrame = New System.Windows.Forms.Label()
    Me.lblSave = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.CheckBox1 = New System.Windows.Forms.CheckBox()
    Me.SuspendLayout()
    '
    'btnSave
    '
    Me.btnSave.Location = New System.Drawing.Point(128, 141)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(60, 22)
    Me.btnSave.TabIndex = 5
    Me.btnSave.Text = "Save"
    Me.btnSave.UseVisualStyleBackColor = True
    '
    'btnCancel
    '
    Me.btnCancel.Location = New System.Drawing.Point(194, 141)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 6
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'txtStartFrame
    '
    Me.txtStartFrame.Location = New System.Drawing.Point(106, 34)
    Me.txtStartFrame.Name = "txtStartFrame"
    Me.txtStartFrame.Size = New System.Drawing.Size(49, 20)
    Me.txtStartFrame.TabIndex = 7
    Me.txtStartFrame.Text = "38888"
    Me.txtStartFrame.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtEndFrame
    '
    Me.txtEndFrame.Location = New System.Drawing.Point(184, 34)
    Me.txtEndFrame.Name = "txtEndFrame"
    Me.txtEndFrame.Size = New System.Drawing.Size(49, 20)
    Me.txtEndFrame.TabIndex = 8
    Me.txtEndFrame.Text = "38888"
    Me.txtEndFrame.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'lblStartFrame
    '
    Me.lblStartFrame.AutoSize = True
    Me.lblStartFrame.Location = New System.Drawing.Point(73, 37)
    Me.lblStartFrame.Name = "lblStartFrame"
    Me.lblStartFrame.Size = New System.Drawing.Size(27, 13)
    Me.lblStartFrame.TabIndex = 9
    Me.lblStartFrame.Text = "from"
    '
    'lblEndFrame
    '
    Me.lblEndFrame.AutoSize = True
    Me.lblEndFrame.Location = New System.Drawing.Point(162, 37)
    Me.lblEndFrame.Name = "lblEndFrame"
    Me.lblEndFrame.Size = New System.Drawing.Size(16, 13)
    Me.lblEndFrame.TabIndex = 10
    Me.lblEndFrame.Text = "to"
    '
    'lblSave
    '
    Me.lblSave.AutoSize = True
    Me.lblSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblSave.Location = New System.Drawing.Point(44, 15)
    Me.lblSave.Name = "lblSave"
    Me.lblSave.Size = New System.Drawing.Size(36, 13)
    Me.lblSave.TabIndex = 11
    Me.lblSave.Text = "Save"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(162, 106)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(16, 13)
    Me.Label2.TabIndex = 15
    Me.Label2.Text = "to"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(73, 106)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(27, 13)
    Me.Label3.TabIndex = 14
    Me.Label3.Text = "from"
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(184, 103)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(49, 20)
    Me.TextBox1.TabIndex = 13
    Me.TextBox1.Text = "38888"
    Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'TextBox2
    '
    Me.TextBox2.Location = New System.Drawing.Point(106, 103)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Size = New System.Drawing.Size(49, 20)
    Me.TextBox2.TabIndex = 12
    Me.TextBox2.Text = "38888"
    Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'CheckBox1
    '
    Me.CheckBox1.AutoSize = True
    Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.CheckBox1.Location = New System.Drawing.Point(47, 80)
    Me.CheckBox1.Name = "CheckBox1"
    Me.CheckBox1.Size = New System.Drawing.Size(65, 17)
    Me.CheckBox1.TabIndex = 17
    Me.CheckBox1.Text = "Shadow"
    Me.CheckBox1.UseVisualStyleBackColor = True
    '
    'frmSaveVideoSequence
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(259, 171)
    Me.ControlBox = False
    Me.Controls.Add(Me.CheckBox1)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.TextBox1)
    Me.Controls.Add(Me.TextBox2)
    Me.Controls.Add(Me.lblSave)
    Me.Controls.Add(Me.lblEndFrame)
    Me.Controls.Add(Me.lblStartFrame)
    Me.Controls.Add(Me.txtEndFrame)
    Me.Controls.Add(Me.txtStartFrame)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnSave)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frmSaveVideoSequence"
    Me.Text = "Save video sequence"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents txtStartFrame As System.Windows.Forms.TextBox
  Friend WithEvents txtEndFrame As System.Windows.Forms.TextBox
  Friend WithEvents lblStartFrame As System.Windows.Forms.Label
  Friend WithEvents lblEndFrame As System.Windows.Forms.Label
  Friend WithEvents lblSave As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
  Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
  Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
