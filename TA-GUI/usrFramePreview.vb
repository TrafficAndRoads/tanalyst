﻿Public Class usrFramePreview
  Private MyVideoFile As String
  Private MyMaxHeight As Integer
  Private MyMaxWidth As Integer
  Private MyPicBoxHeight As Integer
  Private MyPicBoxWidth As Integer
  Private MyImage As Bitmap
  Private MyShowFileInfo As Boolean
  Private MyFileInfo As String

  Public Sub New()
    InitializeComponent()

    MyShowFileInfo = True
    MyVideoFile = ""
    MyFileInfo = ""
    MyMaxHeight = 101
    MyMaxWidth = 100
    MyImage = New Bitmap(100, 75)

    Call ArrangeSize()
  End Sub

  Public Property MaxHeight As Integer
    Set(ByVal value As Integer)
      MyMaxHeight = value
      Call ArrangeSize()
    End Set
    Get
      MaxHeight = MyMaxHeight
    End Get
  End Property

  Public Property MaxWidth As Integer
    Set(ByVal value As Integer)
      MyMaxWidth = value
      Call ArrangeSize()
    End Set
    Get
      MaxWidth = MyMaxHeight
    End Get
  End Property

  Public Property VideoFile As String
    Set(ByVal value As String)
      '  Dim _Reader As AviReader
      '  Dim _FrameCount As Integer
      '  Dim _FrameRate As Double

      '  If File.Exists(value) And Path.GetExtension(value) = ".avi" Then
      '    MyVideoFile = value

      '    _Reader = New AviReader(MyVideoFile)
      '    MyImage = _Reader.GetFrame(1)

      '    _FrameCount = _Reader.FrameCount
      '    _FrameRate = _Reader.FrameRate

      '    _Reader.Close()

      '    MyFileInfo = _FrameCount.ToString & " frames" & ControlChars.NewLine & Format(_FrameRate, "#0.000") & " fps"
      '  Else
      '    MyImage = New Bitmap(100, 75)
      '    MyFileInfo = ""
      '  End If
      '  Call ArrangeSize()
    End Set
    Get
      '  VideoFile = MyVideoFile
    End Get
  End Property

  Public Property ShowFileInfo As Boolean
    Get
      ShowFileInfo = MyShowFileInfo
    End Get
    Set(ByVal value As Boolean)
      MyShowFileInfo = True
      lblFileInfo.Text = MyFileInfo
      Call ArrangeSize()
    End Set
  End Property

  Private Sub ArrangeSize()
    Const _Ratio_4_3 As Double = 1.3333333333333333
    Dim _AspectRatio As Double
    Dim g As Graphics
    Dim _ScaledImage As Bitmap

    _AspectRatio = MyImage.Width / MyImage.Height
    If _AspectRatio < _Ratio_4_3 Then
      MyPicBoxHeight = MyMaxHeight - 26
      MyPicBoxWidth = CInt(MyPicBoxHeight * _AspectRatio)
    Else
      MyPicBoxWidth = MyMaxWidth
      MyPicBoxHeight = CInt(MyPicBoxWidth / _AspectRatio)
    End If

    picFrame.Height = MyPicBoxHeight
    picFrame.Width = MyPicBoxWidth
    picFrame.Left = CInt((MyMaxWidth - MyPicBoxWidth) / 2)
    lblFileInfo.Top = picFrame.Bottom

    Me.Width = MyMaxWidth
    Me.Height = MyMaxHeight

    lblFileInfo.Text = MyFileInfo
    _ScaledImage = New Bitmap(MyPicBoxWidth, MyPicBoxHeight)
    g = Graphics.FromImage(_ScaledImage)
    g.DrawImage(MyImage, 0, 0, MyPicBoxWidth, MyPicBoxHeight)
    picFrame.Image = _ScaledImage
    g.Dispose()
  End Sub

End Class
