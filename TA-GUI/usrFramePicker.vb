﻿Public Class usrFramePicker
  Private MyVideoFile As clsVideoFile
  Private MyFrame As Integer
  Private MyTime As Double
  Private MyFrameStep As Integer = 25

  Private MyFrameScale As Integer = 50



  Public Sub New(ByVal VideoFile As clsVideoFile)

    InitializeComponent()

    MyVideoFile = VideoFile

    Call ArrangeSize()

    MyFrame = 0
    Call Display(MyFrame)
  End Sub

  Public Property FrameScale As Integer
    Get
      Return MyFrameScale
    End Get
    Set(value As Integer)
      MyFrameScale = value
      Call ArrangeSize()
    End Set
  End Property


  Private Sub ArrangeSize()
    picVideo.Height = CInt(MyVideoFile.FrameSize.Height * MyFrameScale / 100)
    picVideo.Width = CInt(MyVideoFile.FrameSize.Width * MyFrameScale / 100)
    picVideo.Left = 0 : picVideo.Top = 0

    pnlNavigation.Top = picVideo.Bottom
    pnlNavigation.Left = CInt((picVideo.Width - pnlNavigation.Width) / 2)

    Me.Width = picVideo.Width
    Me.Height = pnlNavigation.Bottom
  End Sub

  Public ReadOnly Property Frame As Integer
    Get
      Frame = MyFrame
    End Get
  End Property

  Public ReadOnly Property TimeStamp As Double
    Get
      Return MyVideoFile.GetTimeStamp(MyFrame)
    End Get
  End Property


  Public ReadOnly Property AbsTime As Double
    Get
      Return MyVideoFile.GetAbsTime(MyFrame)
    End Get
  End Property

  Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
    Call Display(MyFrame + 1)
  End Sub

  Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    Call Display(MyFrame - 1)
  End Sub

  Private Sub btnJumpForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJumpForward.Click
    Call Display(MyFrame + MyFrameStep)
  End Sub

  Private Sub btnJumpBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJumpBack.Click
    Call Display(MyFrame - MyFrameStep)
  End Sub

  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    Display(MyVideoFile.FrameCount - 1)
  End Sub

  Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
    Display(0)
  End Sub

  Private Sub Display(ByVal NewFrame As Integer)
    Dim _Bitmap As Bitmap
    Dim _g As Graphics

    If IsBetween(NewFrame, 0, MyVideoFile.FrameCount - 1) Then
      MyFrame = NewFrame

      _Bitmap = New Bitmap(picVideo.Width, picVideo.Height)
      _g = Graphics.FromImage(_Bitmap)
      _g.DrawImage(MyVideoFile.GetFrameBitmap(MyFrame), 0, 0, _Bitmap.Width, _Bitmap.Height)
      picVideo.Image = _Bitmap
      lblFrame.Text = MyFrame.ToString
      lblTimeStamp.Text = Format(MyVideoFile.GetTimeStampDate(NewFrame), "yyyyMMdd-HHmmss.fff")
    End If
  End Sub

  Private Sub lblFrame_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblFrame.DoubleClick
    Dim _FrameString As String
    Dim _FrameTemp As Integer

    _FrameString = InputBox("Go to frame...")
    If Integer.TryParse(_FrameString, _FrameTemp) Then
      Call Display(_FrameTemp)
    End If
  End Sub


  Private Sub usrFramePicker_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub
End Class
