﻿Public Class frmDataPoint


  Public Event DataChanged()
  Public Event TrajectoryTypeChanged(ByVal bTrajectoryType As Byte)

  Private MyRoadUser As clsRoadUser
  Private MyFrame As Integer
  Private WithEvents MyUsrX As usrUpDown
  Private WithEvents MyUsrY As usrUpDown
  Private WithEvents MyUsrDirection As usrUpDown
  Private WithEvents MyUsrV As usrUpDown
  Private WithEvents MyUsrA As usrUpDown
  Private WithEvents MyUsrJ As usrUpDown

  Private WithEvents MyChart As Chart



#Region "Properties"

  Public Property RoadUser As clsRoadUser
    Get
      RoadUser = MyRoadUser
    End Get
    Set(ByVal value As clsRoadUser)
      MyRoadUser = value
      rbtOriginal.Checked = True
      Call LoadChartData()
      Call DisplayValues()
    End Set
  End Property

  Public Property Frame As Integer
    Get
      Frame = MyFrame
    End Get
    Set(ByVal value As Integer)
      MyFrame = value
      Call DisplayValues()
    End Set
  End Property

#End Region



#Region "Private subs"



  Private Sub AssignData()
    Select Case rbtOriginal.Checked
      Case True
        MyRoadUser.DataPoint(MyFrame).Xpxl = MyUsrX.Value
        MyRoadUser.DataPoint(MyFrame).Ypxl = MyUsrY.Value
        MyRoadUser.DataPoint(MyFrame).DxDypxl = New clsDxDy(CDbl(Math.Cos((MyUsrDirection.Value) * Math.PI / 180)), CDbl(Math.Sin((MyUsrDirection.Value) * Math.PI / 180)))
      Case False
        MyRoadUser.DataPoint(MyFrame).X = MyUsrX.Value
        MyRoadUser.DataPoint(MyFrame).Y = MyUsrY.Value
        MyRoadUser.DataPoint(MyFrame).DxDy = New clsDxDy(CDbl(Math.Cos((MyUsrDirection.Value) * Math.PI / 180)), CDbl(Math.Sin((MyUsrDirection.Value) * Math.PI / 180)))
    End Select
    MyRoadUser.DataPoint(MyFrame).V = MyUsrV.Value
    MyRoadUser.DataPoint(MyFrame).A = MyUsrA.Value
    MyRoadUser.DataPoint(MyFrame).J = MyUsrJ.Value

    Call LoadChartData()
    RaiseEvent DataChanged()
  End Sub

  Private Sub CheckEnables()

    If rbtSmoothed.Checked Then
      MyUsrV.Visible = True
      MyUsrA.Visible = True
      MyUsrJ.Visible = True
    Else
      MyUsrV.Visible = False
      MyUsrA.Visible = False
      MyUsrJ.Visible = False
    End If


    If MyRoadUser IsNot Nothing Then
      rbtOriginal.Enabled = True
      rbtSmoothed.Enabled = True
      chkShowGraph.Enabled = True


      If IsBetween(MyFrame, MyRoadUser.FirstFrame, MyRoadUser.LastFrame) Then
        btnAdd.Enabled = False
        MyUsrX.Enabled = True
        MyUsrY.Enabled = True
        MyUsrDirection.Enabled = True
        MyUsrV.Enabled = True
        MyUsrA.Enabled = True
        MyUsrJ.Enabled = True
      Else
        btnAdd.Enabled = True
        MyUsrX.Enabled = False
        MyUsrY.Enabled = False
        MyUsrDirection.Enabled = False
        MyUsrV.Enabled = False
        MyUsrA.Enabled = False
        MyUsrJ.Enabled = False
      End If

      If (MyRoadUser.DataPointsCount > 1) And ((MyFrame = MyRoadUser.FirstFrame) Or (MyFrame = MyRoadUser.LastFrame)) Then
        btnDelete.Enabled = True
      Else
        btnDelete.Enabled = False
      End If

      If IsBetween(MyFrame, MyRoadUser.FirstFrame + 1, MyRoadUser.LastFrame - 1) Then
        If MyRoadUser.DataPoint(MyFrame).OriginalXYExists Then
          btnCutStart.Enabled = True
          btnCutEnd.Enabled = True
        Else
          btnCutStart.Enabled = False
          btnCutEnd.Enabled = False
        End If
      Else
        btnCutStart.Enabled = False
        btnCutEnd.Enabled = False
      End If

      btnSmoothTrajectory.Enabled = True
    Else 'MyRoadUser IsNot Nothing
      rbtOriginal.Enabled = False
      rbtSmoothed.Enabled = False
      chkShowGraph.Enabled = False
      MyUsrX.Enabled = False
      MyUsrY.Enabled = False
      MyUsrDirection.Enabled = False
      MyUsrV.Enabled = False
      MyUsrA.Enabled = False
      MyUsrJ.Enabled = False
      btnAdd.Enabled = False
      btnDelete.Enabled = False
      btnCutStart.Enabled = False
      btnCutEnd.Enabled = False
      btnSmoothTrajectory.Enabled = False
    End If 'MyRoadUser IsNot Nothing
  End Sub

  Private Sub DisplayValues()
    Dim DataPointTemp As clsDataPoint
    Dim i As Integer


    If MyRoadUser IsNot Nothing Then
      If IsBetween(MyFrame, MyRoadUser.FirstFrame, MyRoadUser.LastFrame) Then
        If rbtOriginal.Checked Then
          i = -1
          Do
            i = i + 1
            DataPointTemp = MyRoadUser.DataPoint(MyFrame - i)
          Loop Until DataPointTemp.OriginalXYExists

          MyUsrX.Value = MyRoadUser.DataPoint(MyFrame - i).Xpxl
          MyUsrY.Value = MyRoadUser.DataPoint(MyFrame - i).Ypxl
          MyUsrDirection.Value = CDbl(Math.Atan2(MyRoadUser.DataPoint(MyFrame - i).DxDypxl.Dy, MyRoadUser.DataPoint(MyFrame - i).DxDypxl.Dx) * 180 / Math.PI)
        Else 'rbtOriginal.Checked
          '          i = -1
          '         Do
          'i = i + 1
          DataPointTemp = MyRoadUser.DataPoint(MyFrame - i)
          'Loop Until DataPointTemp.SmoothedXYExists

          MyUsrX.Value = MyRoadUser.DataPoint(MyFrame - i).X
          MyUsrY.Value = MyRoadUser.DataPoint(MyFrame - i).Y
          MyUsrDirection.Value = CDbl(Math.Atan2(MyRoadUser.DataPoint(MyFrame - i).DxDy.Dy, MyRoadUser.DataPoint(MyFrame - i).DxDy.Dx) * 180 / Math.PI)
        End If 'rbtOriginal.Checked
        '        If MyRoadUser.DataPoint(MyFrame).VExists Then MyUsrV.Value = MyRoadUser.DataPoint(MyFrame).V Else MyUsrV.Value = NoValue
        '       If MyRoadUser.DataPoint(MyFrame).AExists Then MyUsrA.Value = MyRoadUser.DataPoint(MyFrame).A Else MyUsrA.Value = NoValue
        '      If MyRoadUser.DataPoint(MyFrame).JExists Then MyUsrJ.Value = MyRoadUser.DataPoint(MyFrame).J Else MyUsrJ.Value = NoValue
      Else
        MyUsrX.Value = NoValue
        MyUsrY.Value = NoValue
        MyUsrDirection.Value = NoValue
        MyUsrV.Value = NoValue
        MyUsrA.Value = NoValue
        MyUsrJ.Value = NoValue
      End If 'IsBetween(MyFrame, MyRoadUser.FirstFrame, MyRoadUser.LastFrame)
    Else 'MyRoadUser Is Nothing
      Me.Text = "No road user selected"
    End If 'MyRoadUser IsNot Nothing
    Call CheckEnables()
  End Sub

#End Region



#Region "Control subs"

  Private Sub usrX_ValueChanged() Handles MyUsrX.ValueChanged
    Call AssignData()
  End Sub

  Private Sub usrY_ValueChanged() Handles MyUsrY.ValueChanged
    Call AssignData()
  End Sub

  Private Sub usrDirection_ValueChanged() Handles MyUsrDirection.ValueChanged
    Call AssignData()
  End Sub

  Private Sub usrV_ValueChanged() Handles MyUsrV.ValueChanged
    Call AssignData()
  End Sub

  Private Sub usrA_ValueChanged() Handles MyUsrA.ValueChanged
    Call AssignData()
  End Sub

  Private Sub usrJ_ValueChanged() Handles MyUsrJ.ValueChanged
    Call AssignData()
  End Sub

  Private Sub rbtOriginal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtOriginal.CheckedChanged
    Select Case rbtOriginal.Checked
      Case True
        RaiseEvent TrajectoryTypeChanged(0)
      Case False
        RaiseEvent TrajectoryTypeChanged(1)
    End Select
    Frame = MyFrame
  End Sub

  Private Sub btnSmoothTrajectory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSmoothTrajectory.Click
    MyRoadUser.SmoothTrajectorySimple()
    Call LoadChartData()
    RaiseEvent DataChanged()
    MsgBox("OK!")
  End Sub


  Private Sub frmDataPoint_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel
    If My.Computer.Keyboard.CtrlKeyDown Then
      If MyUsrX.Enabled And IsValue(MyUsrX.Value) Then
        MyUsrX.Value = MyUsrX.Value + e.Delta / 1200 * Math.Cos(MyUsrDirection.Value * Math.PI / 180) '=0.1 m
        MyUsrY.Value = MyUsrY.Value + e.Delta / 1200 * Math.Sin(MyUsrDirection.Value * Math.PI / 180)
        Call AssignData()
      End If
    ElseIf My.Computer.Keyboard.AltKeyDown Then
      If MyUsrY.Enabled And IsValue(MyUsrY.Value) Then
        MyUsrX.Value = MyUsrX.Value + e.Delta / 1200 * Math.Sin(MyUsrDirection.Value * Math.PI / 180)
        MyUsrY.Value = MyUsrY.Value - e.Delta / 1200 * Math.Cos(MyUsrDirection.Value * Math.PI / 180) '=0.1 m
        Call AssignData()
      End If
    ElseIf My.Computer.Keyboard.ShiftKeyDown Then
      If MyUsrDirection.Enabled And IsValue(MyUsrDirection.Value) Then
        MyUsrDirection.Value = MyUsrDirection.Value + e.Delta / 60 '=1.5 degrees
        Call AssignData()
      End If
    End If

  End Sub



  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '  Dim _NewDataPoint As clsDataPoint
    ' Dim _i As Integer


    'If MyRoadUser.DataPointsCount > 0 Then
    '  _i = 0
    '  If MyFrame > MyRoadUser.LastFrame Then
    '    Do
    '      _i = _i + 1
    '      With MyRoadUser.DataPoint(MyRoadUser.LastFrame)
    '        _NewDataPoint = New clsDataPoint(.X, .Y, New clsDxDy(.Dx, .Dy))
    '      End With

    '      MyRoadUser.AddPoint(_NewDataPoint)
    '    Loop Until MyRoadUser.LastFrame = MyFrame

    '    _NewDataPoint.Xpxl = MyRoadUser.DataPoint(MyFrame - _i).Xpxl
    '    _NewDataPoint.Ypxl = MyRoadUser.DataPoint(MyFrame - _i).Ypxl
    '    _NewDataPoint.DxDypxl = New clsDxDy(MyRoadUser.DataPoint(MyFrame - _i).DxDypxl.Dx, MyRoadUser.DataPoint(MyFrame - _i).DxDypxl.Dy)
    '  End If

    '  If MyFrame < MyRoadUser.FirstFrame Then
    '    Do
    '      _i = _i + 1
    '      _NewDataPoint = New clsDataPoint
    '      _NewDataPoint.X = MyRoadUser.DataPoint(MyRoadUser.FirstFrame).X
    '      _NewDataPoint.Y = MyRoadUser.DataPoint(MyRoadUser.FirstFrame).Y
    '      _NewDataPoint.DxDy = New clsDxDy(MyRoadUser.DataPoint(MyRoadUser.FirstFrame).Dx, MyRoadUser.DataPoint(MyRoadUser.FirstFrame).Dy)
    '      MyRoadUser.AddStartPoint(_NewDataPoint)
    '    Loop Until MyRoadUser.FirstFrame = MyFrame

    '    _NewDataPoint.Xpxl = MyRoadUser.DataPoint(MyFrame + _i).Xpxl
    '    _NewDataPoint.Ypxl = MyRoadUser.DataPoint(MyFrame + _i).Ypxl
    '    _NewDataPoint.DxDypxl = New clsDxDy(MyRoadUser.DataPoint(MyFrame + _i).Dxpxl, MyRoadUser.DataPoint(MyFrame + _i).Dypxl)
    '  End If
    'Else
    '  MyRoadUser.AddPoint(New clsDataPoint(25, 25, 1, 0, NoValue, NoValue, NoValue, 25, 25, 1, 0))
    'End If

    'Call DisplayValues()
    'Call CheckEnables()
    'RaiseEvent DataChanged()
  End Sub

  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    If MyFrame = MyRoadUser.FirstFrame Then
      MyRoadUser.RemoveFirstPoint()
    ElseIf MyFrame = MyRoadUser.LastFrame Then
      MyRoadUser.RemoveLastPoint()
    End If
    RaiseEvent DataChanged()
  End Sub

  Private Sub btnCutStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCutStart.Click
    For i = 1 To MyFrame - MyRoadUser.FirstFrame
      MyRoadUser.RemoveFirstPoint()
    Next
    Call CheckEnables()
    RaiseEvent DataChanged()
  End Sub

  Private Sub btnCutEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCutEnd.Click
    For i = 1 To MyRoadUser.LastFrame - MyFrame
      MyRoadUser.RemoveLastPoint()
    Next
    Call CheckEnables()
    RaiseEvent DataChanged()
  End Sub


  Private Sub MyChart_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyChart.MouseDoubleClick
    Dim _a As HitTestResult
    Dim _HitChartArea As ChartArea
    Dim _minX, _maxX As Double


    _a = MyChart.HitTest(e.X, e.Y)
    _HitChartArea = _a.ChartArea

    _minX = MyRoadUser.MinX
    _maxX = MyRoadUser.MaxX

    If _HitChartArea IsNot Nothing Then
      _HitChartArea.AxisX.ScaleView.ZoomReset()
      _HitChartArea.AxisY.ScaleView.ZoomReset()
    End If
  End Sub

#End Region





  Public Sub New()

    InitializeComponent()

    MyUsrX = New usrUpDown
    MyUsrX.AutoSize = True
    MyUsrX.Delta = 0.1
    MyUsrX.Enabled = False
    MyUsrX.Label = "X:"
    MyUsrX.Location = New System.Drawing.Point(94, 10)
    MyUsrX.NumDecimals = 2
    MyUsrX.TabIndex = 0
    MyUsrX.Value = 0
    Me.Controls.Add(MyUsrX)


    MyUsrY = New usrUpDown
    MyUsrY.AutoSize = True
    MyUsrY.Delta = 0.1
    MyUsrY.Enabled = False
    MyUsrY.Label = "Y:"
    MyUsrY.Location = New System.Drawing.Point(179, 10)
    MyUsrY.NumDecimals = 2
    MyUsrY.TabIndex = 1
    MyUsrY.Value = 0
    Me.Controls.Add(MyUsrY)

    MyUsrDirection = New usrUpDown
    MyUsrDirection.AutoSize = True
    MyUsrDirection.Delta = 1
    MyUsrDirection.Enabled = False
    MyUsrDirection.Label = "Direction:"
    MyUsrDirection.Location = New System.Drawing.Point(264, 10)
    MyUsrDirection.NumDecimals = 0
    MyUsrDirection.TabIndex = 2
    MyUsrDirection.Value = 0
    Me.Controls.Add(MyUsrDirection)

    MyUsrV = New usrUpDown
    MyUsrV.AutoSize = True
    MyUsrV.Delta = 0.1
    MyUsrV.Enabled = False
    MyUsrV.Label = "V:"
    MyUsrV.Location = New System.Drawing.Point(94, 60)
    MyUsrV.NumDecimals = 2
    MyUsrV.TabIndex = 3
    MyUsrV.Value = 0
    Me.Controls.Add(MyUsrV)

    MyUsrA = New usrUpDown
    MyUsrA.AutoSize = True
    MyUsrA.Delta = 0.1
    MyUsrA.Enabled = False
    MyUsrA.Label = "A:"
    MyUsrA.Location = New System.Drawing.Point(179, 60)
    MyUsrA.NumDecimals = 2
    MyUsrA.TabIndex = 4
    MyUsrA.Value = 0
    Me.Controls.Add(MyUsrA)

    MyUsrJ = New usrUpDown
    MyUsrJ.AutoSize = True
    MyUsrJ.Delta = 0.1
    MyUsrJ.Enabled = False
    MyUsrJ.Label = "J:"
    MyUsrJ.Location = New System.Drawing.Point(264, 60)
    MyUsrJ.NumDecimals = 2
    MyUsrJ.TabIndex = 5
    MyUsrJ.Value = 0
    Me.Controls.Add(MyUsrJ)

    MyChart = New Chart
    MyChart.Location = New Point(3, 145)
    MyChart.Size = New Size(510, 670)

    MyChart.ChartAreas.Add(New ChartArea("XY"))
    MyChart.ChartAreas("XY").BorderColor = Color.Gray
    MyChart.ChartAreas("XY").CursorX.IsUserSelectionEnabled = True
    MyChart.ChartAreas("XY").CursorY.IsUserSelectionEnabled = True

    MyChart.ChartAreas("XY").AxisX.Title = "X, m"
    MyChart.ChartAreas("XY").AxisX.LineColor = Color.Black
    MyChart.ChartAreas("XY").AxisX.LineWidth = 1
    MyChart.ChartAreas("XY").AxisX.Interval = 5
    MyChart.ChartAreas("XY").AxisX.MajorGrid.Interval = 5
    MyChart.ChartAreas("XY").AxisX.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("XY").AxisX.MinorGrid.Enabled = True
    MyChart.ChartAreas("XY").AxisX.MinorGrid.Interval = 1
    MyChart.ChartAreas("XY").AxisX.MinorGrid.LineColor = Color.LightGray

    MyChart.ChartAreas("XY").AxisY.Title = "Y, m"
    MyChart.ChartAreas("XY").AxisY.IsReversed = True
    MyChart.ChartAreas("XY").AxisY.LineColor = Color.Black
    MyChart.ChartAreas("XY").AxisY.LineWidth = 1
    MyChart.ChartAreas("XY").AxisY.Interval = 5
    MyChart.ChartAreas("XY").AxisY.MajorGrid.Interval = 5
    MyChart.ChartAreas("XY").AxisY.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("XY").AxisY.MinorGrid.Enabled = True
    MyChart.ChartAreas("XY").AxisY.MinorGrid.Interval = 1
    MyChart.ChartAreas("XY").AxisY.MinorGrid.LineColor = Color.LightGray

    '  MyChart.ChartAreas("XY").AxisX.ScaleView.
    ' ' MyChart.ChartAreas("XY").AxisX.ScaleView.Zoomable = True
    'MyChart.ChartAreas("XY").AxisY.ScaleView.Zoomable = True

    MyChart.ChartAreas.Add(New ChartArea("VAJ"))
    MyChart.ChartAreas("VAJ").BorderColor = Color.Gray
    MyChart.ChartAreas("VAJ").CursorX.IsUserSelectionEnabled = True
    MyChart.ChartAreas("VAJ").CursorY.IsUserSelectionEnabled = True

    MyChart.ChartAreas("VAJ").AxisX.LineColor = Color.Black
    MyChart.ChartAreas("VAJ").AxisX.LineWidth = 1
    MyChart.ChartAreas("VAJ").AxisX.Title = "Frame"
    MyChart.ChartAreas("VAJ").AxisX.Interval = 10
    MyChart.ChartAreas("VAJ").AxisX.MajorGrid.Interval = 10
    MyChart.ChartAreas("VAJ").AxisX.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("VAJ").AxisX.MinorGrid.Enabled = True
    MyChart.ChartAreas("VAJ").AxisX.MinorGrid.Interval = 2
    MyChart.ChartAreas("VAJ").AxisX.MinorGrid.LineColor = Color.LightGray


    MyChart.ChartAreas("VAJ").AxisY.Crossing = 0
    MyChart.ChartAreas("VAJ").AxisY.LineColor = Color.Black
    MyChart.ChartAreas("VAJ").AxisY.LineWidth = 1
    MyChart.ChartAreas("VAJ").AxisY.Title = "V, m/s   A, m/s2   J, m/s3"
    MyChart.ChartAreas("VAJ").AxisY.Interval = 4
    MyChart.ChartAreas("VAJ").AxisY.MajorGrid.Interval = 4
    MyChart.ChartAreas("VAJ").AxisY.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("VAJ").AxisY.MinorGrid.Enabled = True
    MyChart.ChartAreas("VAJ").AxisY.MinorGrid.Interval = 1
    MyChart.ChartAreas("VAJ").AxisY.MinorGrid.LineColor = Color.LightGray







    MyChart.Series.Add("XY")
    MyChart.Series("XY").ChartArea = "XY"
    MyChart.Series("XY").ChartType = SeriesChartType.Point
    MyChart.Series("XY").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("XY").MarkerSize = 3
    MyChart.Series("XY").Color = Color.Black


    MyChart.Series.Add("XYpxl")
    MyChart.Series("XYpxl").ChartArea = "XY"
    MyChart.Series("XYpxl").ChartType = SeriesChartType.Point
    MyChart.Series("XYpxl").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("XYpxl").MarkerSize = 3
    MyChart.Series("XYpxl").Color = Color.Red


    MyChart.Series.Add("V")
    MyChart.Series("V").ChartArea = "VAJ"
    MyChart.Series("V").ChartType = SeriesChartType.Point
    MyChart.Series("V").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("V").MarkerSize = 3
    MyChart.Series("V").Color = Color.DarkGreen

    MyChart.Series.Add("A")
    MyChart.Series("A").ChartArea = "VAJ"
    MyChart.Series("A").ChartType = SeriesChartType.Point
    MyChart.Series("A").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("A").MarkerSize = 3
    MyChart.Series("A").Color = Color.Red

    MyChart.Series.Add("J")
    MyChart.Series("J").ChartArea = "VAJ"
    MyChart.Series("J").ChartType = SeriesChartType.Point
    MyChart.Series("J").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("J").MarkerSize = 3
    MyChart.Series("J").Color = Color.Brown

    Me.Controls.Add(MyChart)




  End Sub



  Private Sub LoadChartData()
    MyChart.Series("XY").Points.Clear()
    MyChart.Series("XYpxl").Points.Clear()
    MyChart.Series("V").Points.Clear()
    MyChart.Series("A").Points.Clear()
    MyChart.Series("J").Points.Clear()



    If MyRoadUser IsNot Nothing Then
      For _Frame = MyRoadUser.FirstFrame To MyRoadUser.LastFrame
        If MyRoadUser.DataPoint(_Frame).OriginalXYExists Then MyChart.Series("XYpxl").Points.AddXY(MyRoadUser.DataPoint(_Frame).Xpxl, MyRoadUser.DataPoint(_Frame).Ypxl)
        MyChart.Series("XY").Points.AddXY(MyRoadUser.DataPoint(_Frame).X, MyRoadUser.DataPoint(_Frame).Y)
        If MyRoadUser.DataPoint(_Frame).JExists Then MyChart.Series("J").Points.AddXY(_Frame, MyRoadUser.DataPoint(_Frame).J)
        If MyRoadUser.DataPoint(_Frame).AExists Then MyChart.Series("A").Points.AddXY(_Frame, MyRoadUser.DataPoint(_Frame).A)
        If MyRoadUser.DataPoint(_Frame).VExists Then MyChart.Series("V").Points.AddXY(_Frame, MyRoadUser.DataPoint(_Frame).V)
      Next


      MyChart.ChartAreas("XY").AxisX.Minimum = Int(MyRoadUser.MinX / 5) * 5
      MyChart.ChartAreas("XY").AxisX.Maximum = Int(MyRoadUser.MaxX / 5 + 1) * 5
      MyChart.ChartAreas("XY").AxisY.Minimum = Int(MyRoadUser.MinY / 5) * 5
      MyChart.ChartAreas("XY").AxisY.Maximum = Int(MyRoadUser.MaxY / 5 + 1) * 5

      MyChart.ChartAreas("VAJ").AxisX.Minimum = Int(MyRoadUser.FirstFrame / 10) * 10
      MyChart.ChartAreas("VAJ").AxisX.Maximum = Int(MyRoadUser.LastFrame / 10 + 1) * 10
      MyChart.ChartAreas("VAJ").AxisY.Minimum = -4 'Math.Min(0, Int(MyRoadUser.MinV / 2) * 2)
      MyChart.ChartAreas("VAJ").AxisY.Maximum = Int(MyRoadUser.MaxV / 4 + 1) * 4

    End If


  End Sub

  Private Sub chkShowGraph_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkShowGraph.CheckedChanged
    If chkShowGraph.Checked Then
      Me.Height = 845
      MyChart.Visible = True
    Else
      MyChart.Visible = False
      Me.Height = 166

    End If
  End Sub



  Private Sub frmDataPoint_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub
End Class