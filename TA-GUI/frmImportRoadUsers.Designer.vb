﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportRoadUsers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportRoadUsers))
    Me.btnImport = New System.Windows.Forms.Button()
    Me.txtFile = New System.Windows.Forms.TextBox()
    Me.btnOpenFile = New System.Windows.Forms.Button()
        Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
        Me.txtData = New System.Windows.Forms.TextBox()
        Me.digOpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.cmbVideoFiles = New System.Windows.Forms.ComboBox()
        Me.picFirstFrame = New System.Windows.Forms.PictureBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.chkKeepRoadUsers = New System.Windows.Forms.CheckBox()
        Me.chkConnectTrajectories = New System.Windows.Forms.CheckBox()
        CType(Me.picFirstFrame, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnImport
        '
        Me.btnImport.Enabled = False
        Me.btnImport.Location = New System.Drawing.Point(404, 248)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(60, 22)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Enabled = False
        Me.txtFile.Location = New System.Drawing.Point(12, 12)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(266, 20)
        Me.txtFile.TabIndex = 1
        '
        'btnOpenFile
        '
        Me.btnOpenFile.ImageKey = "Open"
        Me.btnOpenFile.ImageList = Me.imlButtons
        Me.btnOpenFile.Location = New System.Drawing.Point(284, 7)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.Size = New System.Drawing.Size(25, 25)
        Me.btnOpenFile.TabIndex = 2
        Me.btnOpenFile.UseVisualStyleBackColor = True
        '
        'imlButtons
        '
        Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlButtons.TransparentColor = System.Drawing.Color.White
        Me.imlButtons.Images.SetKeyName(0, "Open")
        '
        'txtData
        '
        Me.txtData.Location = New System.Drawing.Point(12, 38)
        Me.txtData.Multiline = True
        Me.txtData.Name = "txtData"
        Me.txtData.ReadOnly = True
        Me.txtData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtData.Size = New System.Drawing.Size(297, 153)
        Me.txtData.TabIndex = 3
        Me.txtData.WordWrap = False
        '
        'digOpenFile
        '
        Me.digOpenFile.FileName = "OpenFileDialog1"
        '
        'cmbVideoFiles
        '
        Me.cmbVideoFiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVideoFiles.FormattingEnabled = True
        Me.cmbVideoFiles.Location = New System.Drawing.Point(338, 14)
        Me.cmbVideoFiles.Name = "cmbVideoFiles"
        Me.cmbVideoFiles.Size = New System.Drawing.Size(200, 21)
        Me.cmbVideoFiles.TabIndex = 4
        '
        'picFirstFrame
        '
        Me.picFirstFrame.Location = New System.Drawing.Point(338, 41)
        Me.picFirstFrame.Name = "picFirstFrame"
        Me.picFirstFrame.Size = New System.Drawing.Size(200, 150)
        Me.picFirstFrame.TabIndex = 5
        Me.picFirstFrame.TabStop = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(478, 248)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(60, 22)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkKeepRoadUsers
        '
        Me.chkKeepRoadUsers.AutoSize = True
        Me.chkKeepRoadUsers.Checked = True
        Me.chkKeepRoadUsers.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkKeepRoadUsers.Enabled = False
        Me.chkKeepRoadUsers.Location = New System.Drawing.Point(15, 197)
        Me.chkKeepRoadUsers.Name = "chkKeepRoadUsers"
        Me.chkKeepRoadUsers.Size = New System.Drawing.Size(142, 17)
        Me.chkKeepRoadUsers.TabIndex = 8
        Me.chkKeepRoadUsers.Text = "keep existing trajectories"
        Me.chkKeepRoadUsers.UseVisualStyleBackColor = True
        '
        'chkConnectTrajectories
        '
        Me.chkConnectTrajectories.AutoSize = True
        Me.chkConnectTrajectories.Checked = True
        Me.chkConnectTrajectories.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkConnectTrajectories.Enabled = False
        Me.chkConnectTrajectories.Location = New System.Drawing.Point(15, 220)
        Me.chkConnectTrajectories.Name = "chkConnectTrajectories"
        Me.chkConnectTrajectories.Size = New System.Drawing.Size(145, 17)
        Me.chkConnectTrajectories.TabIndex = 9
        Me.chkConnectTrajectories.Text = "try to connect trajectories"
        Me.chkConnectTrajectories.UseVisualStyleBackColor = True
        '
        'frmImportRoadUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 277)
        Me.Controls.Add(Me.chkConnectTrajectories)
        Me.Controls.Add(Me.chkKeepRoadUsers)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.picFirstFrame)
        Me.Controls.Add(Me.cmbVideoFiles)
        Me.Controls.Add(Me.txtData)
        Me.Controls.Add(Me.btnOpenFile)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.btnImport)
        Me.Name = "frmImportRoadUsers"
        Me.Text = "frmImportRoadUsers"
        CType(Me.picFirstFrame, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnImport As System.Windows.Forms.Button
  Friend WithEvents txtFile As System.Windows.Forms.TextBox
  Friend WithEvents btnOpenFile As System.Windows.Forms.Button
  Friend WithEvents txtData As System.Windows.Forms.TextBox
  Friend WithEvents digOpenFile As System.Windows.Forms.OpenFileDialog
  Friend WithEvents cmbVideoFiles As System.Windows.Forms.ComboBox
  Friend WithEvents picFirstFrame As System.Windows.Forms.PictureBox
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents chkKeepRoadUsers As System.Windows.Forms.CheckBox
  Friend WithEvents chkConnectTrajectories As System.Windows.Forms.CheckBox
  Friend WithEvents imlButtons As System.Windows.Forms.ImageList
End Class
