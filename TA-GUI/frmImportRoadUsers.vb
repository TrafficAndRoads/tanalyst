﻿Public Class frmImportRoadUsers
  Private MyCurrentRecord As clsRecord
  Private MyImportResults As Boolean
  Private MyImportFile As String
  Private MyVideoFile As Integer
  Private MyCmbVideoEdited As Boolean



  Public ReadOnly Property ImportResults As Boolean
    Get
      ImportResults = MyImportResults
    End Get
  End Property

  Public ReadOnly Property ImportFile As String
    Get
      ImportFile = MyImportFile
    End Get
  End Property

  Public ReadOnly Property VideoFile As Integer
    Get
      VideoFile = MyVideoFile
    End Get
  End Property

  Public ReadOnly Property KeepRoadUsers As Boolean
    Get
      KeepRoadUsers = chkKeepRoadUsers.Checked
    End Get
  End Property

  Public ReadOnly Property ConnectTrajectories As Boolean
    Get
      ConnectTrajectories = chkConnectTrajectories.Checked
    End Get
  End Property



  Public Sub New(ByVal Record As clsRecord)

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    MyCurrentRecord = Record
    MyImportFile = ""
    MyVideoFile = NoValue
    MyImportResults = False

    MyCmbVideoEdited = True
    cmbVideoFiles.Items.Clear()
    For i = 0 To MyCurrentRecord.VideoFiles.Count - 1
      cmbVideoFiles.Items.Add(MyCurrentRecord.VideoFiles(i).VideoFileName)
    Next i
    MyCmbVideoEdited = False

    If cmbVideoFiles.Items.Count > 0 Then
      cmbVideoFiles.SelectedIndex = 0
    End If

  End Sub

  Private Sub AllowImport()
    chkConnectTrajectories.Enabled = True
    chkKeepRoadUsers.Enabled = True
    btnImport.Enabled = True
  End Sub



  Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
    Dim iFile As Integer
    Dim sLine As String
    Dim sText As String
    Dim i As Integer

    With digOpenFile
      .FileName = ""
      .Multiselect = False
      .CheckFileExists = True
      .Filter = "trajectory files (*.taimp)|*.taimp"
      .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\My T-projects\Data\"
      .ShowDialog()

      If .FileName.Length > 0 Then
        txtFile.Text = Path.GetFileName(.FileName)
        MyImportFile = .FileName
        iFile = FreeFile()
        FileOpen(iFile, MyImportFile, OpenMode.Input)
        i = 0 : sText = ""
        Do Until EOF(iFile) Or i > 200
          sLine = LineInput(iFile)
          If i = 0 Then sText = sLine Else sText = sText & Environment.NewLine & sLine
          i = i + 1
        Loop
        FileClose(iFile)
        txtData.Text = sText
        If cmbVideoFiles.SelectedIndex <> -1 Then Call AllowImport()
      End If
    End With
  End Sub

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    MyVideoFile = NoValue
    MyImportFile = ""
    MyImportResults = False
    Me.Hide()
  End Sub

  Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
    MyImportResults = True
    Me.Hide()
  End Sub

  Private Sub cmbVideoFiles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVideoFiles.SelectedIndexChanged
    'Dim aReader As AviReader
    'Dim g As Graphics
    'Dim FrameOriginal As Bitmap
    'Dim FrameScaled As Bitmap

    'If Not MyCmbVideoEdited Then
    '  MyVideoFile = cmbVideoFiles.SelectedIndex
    '  aReader = New AviReader(MyCurrentRecord.VideoFiles(VideoFile).VideoFilePath)
    '  FrameOriginal = aReader.GetFrame(10)
    '  FrameScaled = New Bitmap(picFirstFrame.Width, picFirstFrame.Height)
    '  g = Graphics.FromImage(FrameScaled)
    '  g.DrawImage(FrameOriginal, New Rectangle(0, 0, picFirstFrame.Width, picFirstFrame.Height))
    '  picFirstFrame.Image = FrameScaled
    '  FrameOriginal.Dispose()
    '  aReader.Close()
    '  If MyImportFile.Length > 0 Then Call AllowImport()
    'End If
  End Sub

  Private Sub frmImportRoadUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub
End Class