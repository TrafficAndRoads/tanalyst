﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisualiserOptions
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.btnKey1RUColour = New System.Windows.Forms.Button()
    Me.btnKey2RUColour = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.cmbRULineWidth = New System.Windows.Forms.ComboBox()
    Me.btnProjectedRUColour = New System.Windows.Forms.Button()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.btnNormalRUColour = New System.Windows.Forms.Button()
    Me.btnSelectedRUColour = New System.Windows.Forms.Button()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.cmbTrajectoryWidth = New System.Windows.Forms.ComboBox()
    Me.btnKey2TrajectoryColour = New System.Windows.Forms.Button()
    Me.btnNormalTrajectoryColour = New System.Windows.Forms.Button()
    Me.btnProjectedTrajectoryColour = New System.Windows.Forms.Button()
    Me.btnSelectedTrajectoryColour = New System.Windows.Forms.Button()
    Me.btnKey1TrajectoryColour = New System.Windows.Forms.Button()
    Me.GroupBox3 = New System.Windows.Forms.GroupBox()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.GroupBox3.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(271, 272)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 0
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(337, 272)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 1
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'btnKey1RUColour
    '
    Me.btnKey1RUColour.Location = New System.Drawing.Point(93, 152)
    Me.btnKey1RUColour.Name = "btnKey1RUColour"
    Me.btnKey1RUColour.Size = New System.Drawing.Size(22, 22)
    Me.btnKey1RUColour.TabIndex = 4
    Me.btnKey1RUColour.Text = "X"
    Me.btnKey1RUColour.UseVisualStyleBackColor = True
    '
    'btnKey2RUColour
    '
    Me.btnKey2RUColour.Location = New System.Drawing.Point(93, 182)
    Me.btnKey2RUColour.Name = "btnKey2RUColour"
    Me.btnKey2RUColour.Size = New System.Drawing.Size(22, 22)
    Me.btnKey2RUColour.TabIndex = 5
    Me.btnKey2RUColour.Text = "X"
    Me.btnKey2RUColour.UseVisualStyleBackColor = True
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(19, 67)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(68, 13)
    Me.Label1.TabIndex = 9
    Me.Label1.Text = "Camera view"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label2.Location = New System.Drawing.Point(19, 215)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(35, 13)
    Me.Label2.TabIndex = 18
    Me.Label2.Text = "Width"
    '
    'cmbRULineWidth
    '
    Me.cmbRULineWidth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmbRULineWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cmbRULineWidth.FormattingEnabled = True
    Me.cmbRULineWidth.Location = New System.Drawing.Point(60, 212)
    Me.cmbRULineWidth.Name = "cmbRULineWidth"
    Me.cmbRULineWidth.Size = New System.Drawing.Size(55, 21)
    Me.cmbRULineWidth.TabIndex = 17
    '
    'btnProjectedRUColour
    '
    Me.btnProjectedRUColour.Location = New System.Drawing.Point(93, 92)
    Me.btnProjectedRUColour.Name = "btnProjectedRUColour"
    Me.btnProjectedRUColour.Size = New System.Drawing.Size(22, 22)
    Me.btnProjectedRUColour.TabIndex = 17
    Me.btnProjectedRUColour.Text = "X"
    Me.btnProjectedRUColour.UseVisualStyleBackColor = True
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(19, 97)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(54, 13)
    Me.Label5.TabIndex = 16
    Me.Label5.Text = "Projection"
    '
    'btnNormalRUColour
    '
    Me.btnNormalRUColour.Location = New System.Drawing.Point(93, 62)
    Me.btnNormalRUColour.Name = "btnNormalRUColour"
    Me.btnNormalRUColour.Size = New System.Drawing.Size(22, 22)
    Me.btnNormalRUColour.TabIndex = 15
    Me.btnNormalRUColour.Text = "X"
    Me.btnNormalRUColour.UseVisualStyleBackColor = True
    '
    'btnSelectedRUColour
    '
    Me.btnSelectedRUColour.Location = New System.Drawing.Point(93, 122)
    Me.btnSelectedRUColour.Name = "btnSelectedRUColour"
    Me.btnSelectedRUColour.Size = New System.Drawing.Size(22, 22)
    Me.btnSelectedRUColour.TabIndex = 14
    Me.btnSelectedRUColour.Text = "X"
    Me.btnSelectedRUColour.UseVisualStyleBackColor = True
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label6.Location = New System.Drawing.Point(19, 187)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(34, 13)
    Me.Label6.TabIndex = 13
    Me.Label6.Text = "Key 2"
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(19, 157)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(34, 13)
    Me.Label4.TabIndex = 11
    Me.Label4.Text = "Key 1"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(19, 127)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(49, 13)
    Me.Label3.TabIndex = 10
    Me.Label3.Text = "Selected"
    '
    'cmbTrajectoryWidth
    '
    Me.cmbTrajectoryWidth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmbTrajectoryWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cmbTrajectoryWidth.FormattingEnabled = True
    Me.cmbTrajectoryWidth.Location = New System.Drawing.Point(162, 212)
    Me.cmbTrajectoryWidth.Name = "cmbTrajectoryWidth"
    Me.cmbTrajectoryWidth.Size = New System.Drawing.Size(55, 21)
    Me.cmbTrajectoryWidth.TabIndex = 18
    '
    'btnKey2TrajectoryColour
    '
    Me.btnKey2TrajectoryColour.Location = New System.Drawing.Point(195, 182)
    Me.btnKey2TrajectoryColour.Name = "btnKey2TrajectoryColour"
    Me.btnKey2TrajectoryColour.Size = New System.Drawing.Size(22, 22)
    Me.btnKey2TrajectoryColour.TabIndex = 16
    Me.btnKey2TrajectoryColour.Text = "X"
    Me.btnKey2TrajectoryColour.UseVisualStyleBackColor = True
    '
    'btnNormalTrajectoryColour
    '
    Me.btnNormalTrajectoryColour.Location = New System.Drawing.Point(195, 62)
    Me.btnNormalTrajectoryColour.Name = "btnNormalTrajectoryColour"
    Me.btnNormalTrajectoryColour.Size = New System.Drawing.Size(22, 22)
    Me.btnNormalTrajectoryColour.TabIndex = 15
    Me.btnNormalTrajectoryColour.Text = "X"
    Me.btnNormalTrajectoryColour.UseVisualStyleBackColor = True
    '
    'btnProjectedTrajectoryColour
    '
    Me.btnProjectedTrajectoryColour.Location = New System.Drawing.Point(195, 92)
    Me.btnProjectedTrajectoryColour.Name = "btnProjectedTrajectoryColour"
    Me.btnProjectedTrajectoryColour.Size = New System.Drawing.Size(22, 22)
    Me.btnProjectedTrajectoryColour.TabIndex = 14
    Me.btnProjectedTrajectoryColour.Text = "X"
    Me.btnProjectedTrajectoryColour.UseVisualStyleBackColor = True
    '
    'btnSelectedTrajectoryColour
    '
    Me.btnSelectedTrajectoryColour.Location = New System.Drawing.Point(195, 122)
    Me.btnSelectedTrajectoryColour.Name = "btnSelectedTrajectoryColour"
    Me.btnSelectedTrajectoryColour.Size = New System.Drawing.Size(22, 22)
    Me.btnSelectedTrajectoryColour.TabIndex = 4
    Me.btnSelectedTrajectoryColour.Text = "X"
    Me.btnSelectedTrajectoryColour.UseVisualStyleBackColor = True
    '
    'btnKey1TrajectoryColour
    '
    Me.btnKey1TrajectoryColour.Location = New System.Drawing.Point(195, 152)
    Me.btnKey1TrajectoryColour.Name = "btnKey1TrajectoryColour"
    Me.btnKey1TrajectoryColour.Size = New System.Drawing.Size(22, 22)
    Me.btnKey1TrajectoryColour.TabIndex = 5
    Me.btnKey1TrajectoryColour.Text = "X"
    Me.btnKey1TrajectoryColour.UseVisualStyleBackColor = True
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.cmbTrajectoryWidth)
    Me.GroupBox3.Controls.Add(Me.btnKey2TrajectoryColour)
    Me.GroupBox3.Controls.Add(Me.Label8)
    Me.GroupBox3.Controls.Add(Me.btnNormalTrajectoryColour)
    Me.GroupBox3.Controls.Add(Me.Label7)
    Me.GroupBox3.Controls.Add(Me.btnProjectedTrajectoryColour)
    Me.GroupBox3.Controls.Add(Me.Label2)
    Me.GroupBox3.Controls.Add(Me.btnSelectedTrajectoryColour)
    Me.GroupBox3.Controls.Add(Me.cmbRULineWidth)
    Me.GroupBox3.Controls.Add(Me.btnKey1TrajectoryColour)
    Me.GroupBox3.Controls.Add(Me.btnKey2RUColour)
    Me.GroupBox3.Controls.Add(Me.btnProjectedRUColour)
    Me.GroupBox3.Controls.Add(Me.btnKey1RUColour)
    Me.GroupBox3.Controls.Add(Me.Label5)
    Me.GroupBox3.Controls.Add(Me.Label1)
    Me.GroupBox3.Controls.Add(Me.btnNormalRUColour)
    Me.GroupBox3.Controls.Add(Me.Label3)
    Me.GroupBox3.Controls.Add(Me.btnSelectedRUColour)
    Me.GroupBox3.Controls.Add(Me.Label4)
    Me.GroupBox3.Controls.Add(Me.Label6)
    Me.GroupBox3.Location = New System.Drawing.Point(12, 12)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(351, 252)
    Me.GroupBox3.TabIndex = 17
    Me.GroupBox3.TabStop = False
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label7.Location = New System.Drawing.Point(44, 39)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(71, 13)
    Me.Label7.TabIndex = 19
    Me.Label7.Text = "Road users"
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label8.Location = New System.Drawing.Point(143, 39)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(74, 13)
    Me.Label8.TabIndex = 20
    Me.Label8.Text = "Trajectories"
    '
    'frmVisualiserOptions
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(410, 312)
    Me.ControlBox = False
    Me.Controls.Add(Me.GroupBox3)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmVisualiserOptions"
    Me.ShowInTaskbar = False
    Me.Text = "Set record filter"
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnKey1RUColour As System.Windows.Forms.Button
  Friend WithEvents btnKey2RUColour As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents btnNormalRUColour As System.Windows.Forms.Button
  Friend WithEvents btnSelectedRUColour As System.Windows.Forms.Button
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents btnNormalTrajectoryColour As System.Windows.Forms.Button
  Friend WithEvents btnProjectedTrajectoryColour As System.Windows.Forms.Button
  Friend WithEvents btnSelectedTrajectoryColour As System.Windows.Forms.Button
  Friend WithEvents btnKey1TrajectoryColour As System.Windows.Forms.Button
  Friend WithEvents btnProjectedRUColour As System.Windows.Forms.Button
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents btnKey2TrajectoryColour As System.Windows.Forms.Button
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents cmbRULineWidth As System.Windows.Forms.ComboBox
  Friend WithEvents cmbTrajectoryWidth As System.Windows.Forms.ComboBox
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
