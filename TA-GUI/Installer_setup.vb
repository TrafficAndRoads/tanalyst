﻿Imports System.ComponentModel
Imports System.Configuration.Install
Imports Microsoft.Win32

Public Class Installer_setup

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent

    End Sub

    Public Overrides Sub Install(ByVal stateSaver As System.Collections.IDictionary)
        MyBase.Install(stateSaver)
        'This section check Registry [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\14.0\Common\FilesPaths] - Check mso.dll file exist (office x64), and delete the registry 
        Try
            If Not EventLog.SourceExists("T-Analyst") Then
                EventLog.CreateEventSource("T-Analyst", "Application")
                Return
            End If
            Dim regPath As String = "SOFTWARE\Microsoft\Office\14.0\Common\FilesPaths"
            Dim delKey As String = "mso.dll"
            Using logEvent As New EventLog
                logEvent.Source = "T-Analyst"
                If My.Computer.Registry.LocalMachine.GetValue(regPath, delKey, Nothing) Is Nothing Then
                    logEvent.WriteEntry("Registry mso.dll not exist")
                Else
                    Dim regKey As RegistryKey = My.Computer.Registry.LocalMachine.OpenSubKey(regPath, True)
                    regKey.DeleteValue(delKey)
                    regKey.Close()
                    logEvent.WriteEntry("Registry mso.dll has been deleted")
                    'Dim key As New Text.StringBuilder()
                    'For Each item As String In regKey.GetValueNames()
                    '    key.Append(item + ",")
                    'Next
                End If
                logEvent.Close()
            End Using
        Catch ex As Exception
            Using logEvent As New EventLog
                logEvent.Source = "T-Analyst"
                logEvent.WriteEntry(ex.Message + ", " + ex.StackTrace)
                logEvent.Close()
            End Using
        End Try
    End Sub

    Public Overrides Sub Commit(ByVal savedState As System.Collections.IDictionary)
        MyBase.Commit(savedState)
    End Sub

    Public Overrides Sub Rollback(ByVal savedState As System.Collections.IDictionary)
        MyBase.Rollback(savedState)
    End Sub

    Public Overrides Sub Uninstall(ByVal savedState As System.Collections.IDictionary)
        MyBase.Uninstall(savedState)
    End Sub

End Class
