﻿Public Class frmMultiple

  Private MyProject As clsProject
  Private MyFolderLocation As String


  Public Sub New(ByVal Project As clsProject)


    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    MyProject = Project
    MyFolderLocation = vbNullString

    ProgressBar1.Minimum = 1
    ProgressBar1.Maximum = MyProject.NumberOfFilteredRecordsDT + 1
    MaxRecordNumber.Text() = (MyProject.NumberOfFilteredRecordsDT + 1).ToString
  End Sub



  Private Sub outputfolder_Click(sender As Object, e As EventArgs) Handles outputfolder.Click
    Dim fod As FolderBrowserDialog
    fod = New FolderBrowserDialog()
    If (fod.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
      MyFolderLocation = fod.SelectedPath
    End If

  End Sub


  Private Sub Start_Click(sender As Object, e As EventArgs) Handles Start.Click

    Dim i, j As Integer
    Dim FoundTTCmin, traj_length As Double
    Dim FoundT2min, FoundT2last, FoundPET As Double
    Dim FoundT2DeltaV01, FoundT2DeltaV41, FoundT2DeltaV61, FoundT2DeltaV81 As Double
    Dim FoundT2DeltaV02, FoundT2DeltaV42, FoundT2DeltaV62, FoundT2DeltaV82 As Double
    'Dim T2min_Speed1, T2min_Speed2, TTCmin_Speed1, TTCmin_Speed2, TTCmin_DeltaV0_1, TTCmin_DeltaV0_2, TTCmin_relativespeed, T2_DeltaV0_1, T2_DeltaV0_2, T2_relativespeed As Double

    Dim _MinDistance As Double
    Dim _dV1, _dV2 As Double



    Dim go As Boolean
        go = False

    If MyFolderLocation = vbNullString Then

      Dim fod As FolderBrowserDialog
      fod = New FolderBrowserDialog()

      If (fod.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
        MyFolderLocation = fod.SelectedPath
      End If

    End If


    If Dir(MyFolderLocation & "/" & output_name.Text) = output_name.Text Then


      Dim svar = MsgBox("File already exists. Overwrite the file?", MsgBoxStyle.YesNo, "File already exists")

      If svar = MsgBoxResult.Yes Then

        My.Computer.FileSystem.DeleteFile(MyFolderLocation & "/" & output_name.Text)
        go = True

      End If

    Else

      go = True

    End If





    If go Then


      Dim outFile As IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(MyFolderLocation & "/" & output_name.Text, False)
      outFile.WriteLine("ID;Date;RoadUser1;Weight1;RoadUser2;Weight2;InteractionType;TTCmin;T2min;T2last;PET;T2DeltaV0 RoadUser 1;T2DeltaV0 RoadUser 2;T2DeltaV4 RoadUser 1;T2DeltaV4 RoadUser 2;T2DeltaV6 RoadUser 1;T2DeltaV6 RoadUser 2;T2DeltaV8 RoadUser 1;T2DeltaV8 RoadUser 2;Traj_length;MinDistance;dV1MinDistance;dV2MinDistance;Comments")

      ProgressBar1.Minimum = 0
      ProgressBar1.Maximum = MyProject.NumberOfFilteredRecordsDT
      MaxRecordNumber.Text() = CStr(MyProject.NumberOfFilteredRecordsDT)
      MaxRecordNumber.Update()
      ProgressBar1.Visible = True

      For i = 0 To MyProject.NumberOfFilteredRecordsDT - 1


        ProgressBar1.Value = i + 1
        ProgressBar1.Update()
        RecordNumber.Text() = CStr(i)
        RecordNumber.Update()
        MyProject.LoadRecordDT(i)


        If MyProject.CurrentRecordDT.RoadUsers.Count > 2 Then

          outFile.WriteLine(CStr(MyProject.CurrentRecordDT.ID.ToString()) & ";" & CStr(MyProject.CurrentRecord.DateTime) & ";;;" & CStr(MyProject.CurrentRecordDT.Type) & ";;;;;;;;;;;;;;" & "Too many trajectories, only 2 trajectories per event is allowed")

        ElseIf MyProject.CurrentRecordDT.RoadUsers.Count < 2 Then

          outFile.WriteLine(CStr(MyProject.CurrentRecordDT.ID.ToString()) & ";" & CStr(MyProject.CurrentRecord.DateTime) & ";;;" & CStr(MyProject.CurrentRecordDT.Type) & ";;;;;;;;;;;;;;" & "Too few trajectories found")

        Else

          For Each RoadUser In MyProject.CurrentRecord.RoadUsers
            RoadUser.SmoothTrajectory()
          Next

          'MyProject.CurrentRecordDT.KeyRoadUser1 = MyProject.CurrentRecord.RoadUsers(0)
          'MyProject.CurrentRecordDT.KeyRoadUser2 = MyProject.CurrentRecord.RoadUsers(1)
          MyProject.CurrentRecord.SaveRoadUsers()
          MyProject.CurrentRecord.CalculateTTCindicators(True)

          traj_length = Math.Min(MyProject.CurrentRecordDT.KeyRoadUser1.TrajectoryLength, MyProject.CurrentRecordDT.KeyRoadUser2.TrajectoryLength)

          FoundT2min = 999
          FoundT2last = 999
          FoundTTCmin = 999
          FoundPET = 999
          FoundT2DeltaV01 = 999
          FoundT2DeltaV02 = 999
          FoundT2DeltaV41 = 999
          FoundT2DeltaV42 = 999
          FoundT2DeltaV61 = 999
          FoundT2DeltaV62 = 999
          FoundT2DeltaV81 = 999
          FoundT2DeltaV82 = 999

          If TTCmin.CheckState = 1 Then


            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.TTC) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.TTC < FoundTTCmin Then
                FoundTTCmin = MyProject.CurrentRecord.TimeLine(j).Indicators.TTC

                'for Attila
                'TTCmin_Speed1 = MyProject.CurrentRecord.TimeLine(j).Indicators.V1
                'TTCmin_Speed2 = MyProject.CurrentRecord.TimeLine(j).Indicators.V2
                'TTCmin_DeltaV0_1 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01
                'TTCmin_DeltaV0_2 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02
                'TTCmin_relativespeed = MyProject.CurrentRecord.TimeLine(j).Indicators.Vrel

              End If
            Next

          End If

          If T2.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.T2) Then
                If MyProject.CurrentRecord.TimeLine(j).Indicators.T2 > MyProject.CurrentRecord.TimeLine(j).Indicators.T1 And MyProject.CurrentRecord.TimeLine(j).Indicators.T2 < FoundT2min Then
                  FoundT2min = MyProject.CurrentRecord.TimeLine(j).Indicators.T2

                  'for Attila
                  'T2min_Speed1 = MyProject.CurrentRecord.TimeLine(j).Indicators.V1
                  'T2min_Speed2 = MyProject.CurrentRecord.TimeLine(j).Indicators.V2
                  'T2_DeltaV0_1 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01
                  'T2_DeltaV0_2 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02
                  'T2_relativespeed = MyProject.CurrentRecord.TimeLine(j).Indicators.Vrel

                ElseIf MyProject.CurrentRecord.TimeLine(j).Indicators.T1 > MyProject.CurrentRecord.TimeLine(j).Indicators.T2 And MyProject.CurrentRecord.TimeLine(j).Indicators.T1 < FoundT2min Then
                  FoundT2min = MyProject.CurrentRecord.TimeLine(j).Indicators.T1

                  'for Attila
                  'T2min_Speed1 = MyProject.CurrentRecord.TimeLine(j).Indicators.V1
                  'T2min_Speed2 = MyProject.CurrentRecord.TimeLine(j).Indicators.V2
                  'T2_DeltaV0_1 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01
                  'T2_DeltaV0_2 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02
                  'T2_relativespeed = MyProject.CurrentRecord.TimeLine(j).Indicators.Vrel

                End If
              End If
            Next

          End If

          If T2last.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.T2) Then
                If MyProject.CurrentRecord.TimeLine(j).Indicators.T2 > MyProject.CurrentRecord.TimeLine(j).Indicators.T1 Then
                  FoundT2last = MyProject.CurrentRecord.TimeLine(j).Indicators.T2
                Else
                  FoundT2last = MyProject.CurrentRecord.TimeLine(j).Indicators.T1
                End If
              End If
            Next

          End If

          If T2DeltaV0.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01 < FoundT2DeltaV01 Then
                FoundT2DeltaV01 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV01
              End If
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02 < FoundT2DeltaV02 Then
                FoundT2DeltaV02 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV02
              End If
            Next

          End If

          If T2DeltaV4.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV41) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV41 < FoundT2DeltaV41 Then
                FoundT2DeltaV41 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV41
              End If
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV42) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV42 < FoundT2DeltaV42 Then
                FoundT2DeltaV42 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV42
              End If
            Next

          End If

          If T2DeltaV6.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV61) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV61 < FoundT2DeltaV61 Then
                FoundT2DeltaV61 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV61
              End If
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV62) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV62 < FoundT2DeltaV62 Then
                FoundT2DeltaV62 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV62
              End If
            Next

          End If

          If T2DeltaV8.CheckState = 1 Then

            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV81) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV81 < FoundT2DeltaV81 Then
                FoundT2DeltaV81 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV81
              End If
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV82) AndAlso MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV82 < FoundT2DeltaV82 Then
                FoundT2DeltaV82 = MyProject.CurrentRecord.TimeLine(j).Indicators.DeltaV82
              End If
            Next

          End If

          If PET.CheckState = 1 Then
            For j = 0 To MyProject.CurrentRecord.FrameCount - 1
              If IsNothing(MyProject.CurrentRecord.TimeLine(j).Indicators) Then
              ElseIf IsValue(MyProject.CurrentRecord.TimeLine(j).Indicators.PET) Then

                FoundPET = MyProject.CurrentRecord.TimeLine(j).Indicators.PET

              End If
            Next
          End If






          If chkMinDistance.CheckState = CheckState.Checked Then

            MyProject.CurrentRecord.CalculateDistanceIndicators()

            _MinDistance = Double.MaxValue
            For _Frame = 0 To MyProject.CurrentRecord.FrameCount - 1
              If MyProject.CurrentRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
                If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance) Then
                  If MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance < _MinDistance Then

                    _MinDistance = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance
                    If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV1AsIs) Then _dV1 = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV1AsIs Else _dV1 = -1
                    If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV2AsIs) Then _dV2 = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV2AsIs Else _dV2 = -1
                  End If
                End If
              End If

            Next
          End If





        End If



        If FoundTTCmin = 999 Then
          FoundTTCmin = -1
        End If

        If FoundPET = 999 Then
          FoundPET = -1
        End If

        If FoundT2min = 999 Then
          FoundT2min = -1
        End If

        If FoundT2last = 999 Then
          FoundT2last = -1
        End If

        If FoundT2DeltaV01 = 999 Then
          FoundT2DeltaV01 = -1
        End If
        If FoundT2DeltaV02 = 999 Then
          FoundT2DeltaV02 = -1
        End If

        If FoundT2DeltaV41 = 999 Then
          FoundT2DeltaV41 = -1
        End If
        If FoundT2DeltaV42 = 999 Then
          FoundT2DeltaV42 = -1
        End If
        If FoundT2DeltaV61 = 999 Then
          FoundT2DeltaV61 = -1
        End If
        If FoundT2DeltaV62 = 999 Then
          FoundT2DeltaV62 = -1
        End If

        If FoundT2DeltaV81 = 999 Then
          FoundT2DeltaV81 = -1
        End If
        If FoundT2DeltaV82 = 999 Then
          FoundT2DeltaV82 = -1
        End If



        outFile.WriteLine(CStr(MyProject.CurrentRecordDT.ID.ToString()) & ";" &
                          CStr(MyProject.CurrentRecord.DateTime) & ";" &
                          MyProject.CurrentRecordDT.KeyRoadUser1.Type.Name & ";" &
                          MyProject.CurrentRecordDT.KeyRoadUser1.Weight.ToString & ";" &
                          MyProject.CurrentRecordDT.KeyRoadUser2.Type.Name & ";" &
                          MyProject.CurrentRecordDT.KeyRoadUser2.Weight.ToString & ";" &
                          MyProject.CurrentRecord.Type.ToString & ";" &
                          FoundTTCmin.ToString("0.000") & ";" &
                          FoundT2min.ToString("0.000") & ";" &
                          FoundT2last.ToString("0.000") & ";" &
                          FoundPET.ToString("0.000") & ";" &
                          FoundT2DeltaV01.ToString("0.000") & ";" &
                          FoundT2DeltaV02.ToString("0.000") & ";" &
                          FoundT2DeltaV41.ToString("0.000") & ";" &
                          FoundT2DeltaV42.ToString("0.000") & ";" &
                          FoundT2DeltaV61.ToString("0.000") & ";" &
                          FoundT2DeltaV62.ToString("0.000") & ";" &
                          FoundT2DeltaV81.ToString("0.000") & ";" &
                          FoundT2DeltaV82.ToString("0.000") & ";" &
                          traj_length.ToString("0.000") & ";" &
                          _MinDistance.ToString("0.000") & ";" &
                          _dV1.ToString("0.000") & ";" &
                          _dV1.ToString("0.000") & ";" &
                          MyProject.CurrentRecordDT.Comment)

        'outFile.WriteLine(CStr(MyProject.CurrentRecordDT.ID.ToString()) & ";" & CStr(MyProject.CurrentRecord.DateTime) & ";" & MyProject.CurrentRecordDT.KeyRoadUser1.Type.Name & ";" & MyProject.CurrentRecordDT.KeyRoadUser2.Type.Name _
        '                  & ";" & CStr(MyProject.CurrentRecord.Type) & ";" & CStr(FoundTTCmin) & ";" & CStr(FoundT2min) & ";" & CStr(FoundT2last) & ";" & CStr(FoundPET) & ";" & CStr(FoundT2DeltaV01) & ";" & CStr(FoundT2DeltaV02) & ";" & CStr(FoundT2DeltaV41) _
        '                & ";" & CStr(FoundT2DeltaV42) & ";" & CStr(FoundT2DeltaV61) & ";" & CStr(FoundT2DeltaV62) & ";" & CStr(FoundT2DeltaV81) & ";" & CStr(FoundT2DeltaV82) & ";" & CStr(traj_length) & "; ;" & CStr(T2min_Speed1) & ";" & CStr(T2min_Speed2) & ";" & CStr(TTCmin_Speed1) & ";" & CStr(TTCmin_Speed2) & "; ;" & CStr(T2_DeltaV0_1) & ";" & CStr(T2_DeltaV0_2) & ";" & CStr(TTCmin_DeltaV0_1) & ";" & CStr(TTCmin_DeltaV0_2) & "; ;" & CStr(T2_relativespeed) & ";" & CStr(TTCmin_relativespeed))


        'End If

        MyProject.CloseCurrentRecordDT()
        outFile.Flush()

      Next

      outFile.Close()
      Console.WriteLine(My.Computer.FileSystem.ReadAllText(MyFolderLocation & "/" & output_name.Text))
      Me.Close()
      MessageBox.Show("Finished!")
    End If

  End Sub

    Private Sub btnExportSpecial_Click(sender As Object, e As EventArgs) Handles btnExportSpecial.Click
    '  Dim _SaveFileDialog As SaveFileDialog
    Dim _FilePath As String
    Dim _FileIndex As Integer
    Dim _OutputLine As String
    Dim _MinDistanceFrame, _FirstPETFrame As Integer
    Dim _CurrentRecordIndex As Integer

    Dim _i, _j As Integer
    Dim _File As Integer
    Dim FoundTTCmin, traj_length As Double
    Dim FoundT2min, FoundT2last, FoundPET As Double
    Dim FoundT2DeltaV01, FoundT2DeltaV41, FoundT2DeltaV61, FoundT2DeltaV81 As Double
    Dim FoundT2DeltaV02, FoundT2DeltaV42, FoundT2DeltaV62, FoundT2DeltaV82 As Double
    'Dim T2min_Speed1, T2min_Speed2, TTCmin_Speed1, TTCmin_Speed2, TTCmin_DeltaV0_1, TTCmin_DeltaV0_2, TTCmin_relativespeed, T2_DeltaV0_1, T2_DeltaV0_2, T2_relativespeed As Double

    Dim _MinDistance, _PETDistance As Double
    Dim _dV1Dist, _dV2Dist, _dV1PET, _dV2PET As Double
    Dim _Gate1, _Gate2 As String


    _CurrentRecordIndex = MyProject.CurrentRecordIndexDT


    _FileIndex = FileSystem.FreeFile()

    With New SaveFileDialog
      '.CheckPathExists = True
      .Filter = "Text files (*.txt)|*.txt"
      .AddExtension = True
      .ShowDialog()
      If .FileName.Length > 0 Then
        FileSystem.FileOpen(_FileIndex, .FileName, OpenMode.Output)
      Else
        Exit Sub
      End If
    End With


    _OutputLine = "Record ID;Date;RU_Type;RU_Direction;RU_weight;Frame(minDistance);MinDistance;RU_Speed(minDistance);RU_Delta-V(minDistance);Frame(FirstPET);Distance(FirstPET);RU_Speed(FirstPET);RU_Delta-V(FirstPET);Comments" 'RoadUser2;Weight2;InteractionType;TTCmin;T2min;T2last;PET;T2DeltaV0 RoadUser 1;T2DeltaV0 RoadUser 2;T2DeltaV4 RoadUser 1;T2DeltaV4 RoadUser 2;T2DeltaV6 RoadUser 1;T2DeltaV6 RoadUser 2;T2DeltaV8 RoadUser 1;T2DeltaV8 RoadUser 2;Traj_length;MinDistance;dV1MinDistance;dV2MinDistance;Comments"
    FileSystem.PrintLine(_FileIndex, _OutputLine)



    'ProgressBar1.Minimum = 0
    '  ProgressBar1.Maximum = MyProject.NumberOfFilteredRecordsDT
    '  MaxRecordNumber.Text() = CStr(MyProject.NumberOfFilteredRecordsDT)
    '  MaxRecordNumber.Update()
    '  ProgressBar1.Visible = True

    For _i = 0 To MyProject.NumberOfFilteredRecordsDT - 1
      MyProject.LoadRecordDT(_i)
      MyProject.CurrentRecordDT.LoadGates("E:\Pyramid\Pyramid.Data\Import\Directions.tagat")

      ProgressBar1.Value = _i + 1
      ProgressBar1.Update()
      RecordNumber.Text() = CStr(_i + 1)
      RecordNumber.Update()




      If (MyProject.CurrentRecordDT.KeyRoadUser1 IsNot Nothing) And (MyProject.CurrentRecordDT.KeyRoadUser2 IsNot Nothing) Then
        MyProject.CurrentRecordDT.KeyRoadUser1.SmoothTrajectory()
        MyProject.CurrentRecordDT.KeyRoadUser2.SmoothTrajectory()
        MyProject.CurrentRecord.SaveRoadUsers()




        MyProject.CurrentRecord.CalculateTTCindicators()

        For _Frame = MyProject.CurrentRecord.FrameCount - 1 To 0 Step -1
          If MyProject.CurrentRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
            If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.PET) Then


              _FirstPETFrame = _Frame

            End If
          End If
        Next


        MyProject.CurrentRecord.CalculateDistanceIndicators()

        _MinDistance = Double.MaxValue

        For _Frame = 0 To MyProject.CurrentRecord.FrameCount - 1
          If MyProject.CurrentRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
            If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance) Then
              If MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance < _MinDistance Then
                _MinDistance = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.Distance
                _MinDistanceFrame = _Frame
                If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV1AsIs) Then _dV1Dist = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV1AsIs Else _dV1Dist = -1
                If IsValue(MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV2AsIs) Then _dV2Dist = MyProject.CurrentRecord.TimeLine(_Frame).Indicators.DeltaV2AsIs Else _dV2Dist = -1
              End If
            End If
          End If
        Next


        _PETDistance = MyProject.CurrentRecord.TimeLine(_FirstPETFrame).Indicators.Distance
        If IsValue(MyProject.CurrentRecord.TimeLine(_FirstPETFrame).Indicators.DeltaV1AsIs) Then _dV1PET = MyProject.CurrentRecord.TimeLine(_FirstPETFrame).Indicators.DeltaV1AsIs Else _dV1PET = -1
        If IsValue(MyProject.CurrentRecord.TimeLine(_FirstPETFrame).Indicators.DeltaV2AsIs) Then _dV2PET = MyProject.CurrentRecord.TimeLine(_FirstPETFrame).Indicators.DeltaV2AsIs Else _dV2PET = -1



        _Gate1 = ""
        _Gate2 = ""
        For Each _Gate In MyProject.CurrentRecordDT.Gates
          If Not IsNothing(MyProject.CurrentRecord.KeyRoadUser1.CrossGateAt(_Gate)) Then
            _Gate1 = _Gate1 & _Gate.Name
          End If
          If Not IsNothing(MyProject.CurrentRecord.KeyRoadUser2.CrossGateAt(_Gate)) Then
            _Gate2 = _Gate2 & _Gate.Name
          End If
        Next




        _OutputLine = MyProject.CurrentRecordDT.ID.ToString & ";" &
                      MyProject.CurrentRecordDT.DateTime.ToString & ";" &
                      MyProject.CurrentRecord.KeyRoadUser1.Type.Name & ";" &
                      _Gate1 & ";" &
                      MyProject.CurrentRecord.KeyRoadUser1.Type.Weight.ToString & ";" &
                      _MinDistanceFrame.ToString & ";" &
                      _MinDistance.ToString("0.000") & ";" &
                      MyProject.CurrentRecord.KeyRoadUser1.DataPoint(_MinDistanceFrame).V.ToString("0.000") & ";" &
                      _dV1Dist.ToString("0.000") & ";" &
                      _FirstPETFrame.ToString & ";" &
                      _PETDistance.ToString("0.000") & ";" &
                      MyProject.CurrentRecord.KeyRoadUser1.DataPoint(_FirstPETFrame).V.ToString("0.000") & ";" &
                     _dV1PET.ToString("0.000") & ";" &
                     MyProject.CurrentRecordDT.Comment
        PrintLine(_FileIndex, _OutputLine)



        _OutputLine = MyProject.CurrentRecordDT.ID.ToString & ";" &
                      MyProject.CurrentRecordDT.DateTime.ToString & ";" &
                      MyProject.CurrentRecord.KeyRoadUser2.Type.Name & ";" &
                      _Gate2 & ";" &
                      MyProject.CurrentRecord.KeyRoadUser2.Type.Weight.ToString & ";" &
                      _MinDistanceFrame.ToString & ";" &
                      _MinDistance.ToString("0.000") & ";" &
                      MyProject.CurrentRecord.KeyRoadUser2.DataPoint(_MinDistanceFrame).V.ToString("0.000") & ";" &
                      _dV2Dist.ToString("0.000") & ";" &
                      _FirstPETFrame.ToString & ";" &
                      _PETDistance.ToString("0.000") & ";" &
                      MyProject.CurrentRecord.KeyRoadUser2.DataPoint(_FirstPETFrame).V.ToString("0.000") & ";" &
                     _dV2PET.ToString("0.000") & ";" &
                     MyProject.CurrentRecordDT.Comment
        PrintLine(_FileIndex, _OutputLine)


      Else
        _OutputLine = MyProject.CurrentRecordDT.ID.ToString & ";" &
                    MyProject.CurrentRecordDT.DateTime.ToString & ";"
        PrintLine(_FileIndex, _OutputLine)
        PrintLine(_FileIndex, _OutputLine)
      End If




      MyProject.CloseCurrentRecordDT()

    Next


    FileClose(_FileIndex)
    MyProject.LoadRecordDT(_CurrentRecordIndex)

    MessageBox.Show("Finished!")
    Me.Close()

  End Sub
End Class