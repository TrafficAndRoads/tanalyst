﻿Public Class frmSyncVideoFiles
  Private MyVideoFile1 As clsVideoFile
  Private MyVideoFile2 As clsVideoFile
  Private MyFramePairs As New List(Of FramePair)
  Private MyFramePicker1 As usrFramePicker
  Private MyFramePicker2 As usrFramePicker
  Private MyClockDescripancy As Double = 0
  Private MySynchronisationOK As Boolean = False
  Private MyIsCalculating As Boolean = False

  Private Structure FramePair
    Dim Frame1 As Integer
    Dim Frame2 As Integer
    Dim Time1 As Double
    Dim Time2 As Double
    Dim ClockDescripancy As Double
  End Structure


  Public Sub New(ByVal VideoFile1 As clsVideoFile, ByVal VideoFile2 As clsVideoFile)
    Dim _MaxHeight As Integer

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    MyVideoFile1 = VideoFile1
    MyVideoFile2 = VideoFile2

    MyFramePicker1 = New usrFramePicker(MyVideoFile1)
    MyFramePicker2 = New usrFramePicker(MyVideoFile2)

    _MaxHeight = Math.Max(MyFramePicker1.Height, MyFramePicker2.Height)
    _MaxHeight = Math.Max(_MaxHeight, pnlTstart.Height)


    MyFramePicker1.Left = 5
    MyFramePicker1.Top = _MaxHeight - MyFramePicker1.Height + 5
    Me.Controls.Add(MyFramePicker1)

    MyFramePicker2.Left = MyFramePicker1.Right + 5
    MyFramePicker2.Top = _MaxHeight - MyFramePicker2.Height + 5
    Me.Controls.Add(MyFramePicker2)

    pnlTstart.Top = 5
    pnlTstart.Left = MyFramePicker2.Right + 5

    btnCancel.Top = _MaxHeight + 5
    btnOK.Top = btnCancel.Top
    btnCancel.Left = pnlTstart.Right - btnCancel.Width
    btnOK.Left = btnCancel.Left - 5 - btnOK.Width

    Me.Width = pnlTstart.Right + 25
    Me.Height = btnCancel.Bottom + 40


    MyClockDescripancy = MyVideoFile2.ClockDiscrepancy

    Call CalculateParameters()

  End Sub

  Public ReadOnly Property SynchronisationOK As Boolean
    Get
      SynchronisationOK = MySynchronisationOK
    End Get
  End Property

  Public ReadOnly Property ClockDescripancy As Double
    Get
      Return MyClockDescripancy
    End Get
  End Property

  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    MySynchronisationOK = True
    Me.Close()
  End Sub

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    MySynchronisationOK = False
    Me.Close()
  End Sub

  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    Dim NewFramePair As FramePair

    With NewFramePair
      .Frame1 = MyFramePicker1.Frame
      .Frame2 = MyFramePicker2.Frame
      .Time1 = MyFramePicker1.AbsTime
      .Time2 = MyFramePicker2.TimeStamp

      .ClockDescripancy = .Time1 - .Time2
      MyFramePairs.Add(NewFramePair)
      lstFramePairs.Items.Add(.Frame1 & "=" & .Frame2 & ", dT=" & Format(.ClockDescripancy, "0.000" & " sec"))
    End With

    Call CalculateParameters()
  End Sub

  Private Sub CalculateParameters()

    MyIsCalculating = True

    If MyFramePairs.Count = 0 Then
      lblClockDiscrepancy.Text = "Clock descrepancy = " & FormatNumber(MyClockDescripancy, 3) & " sec"
    Else
      MyClockDescripancy = 0
      For i = 0 To MyFramePairs.Count - 1
        MyClockDescripancy = MyClockDescripancy + MyFramePairs(i).ClockDescripancy
      Next i
      MyClockDescripancy = MyClockDescripancy / MyFramePairs.Count

      lblClockDiscrepancy.Text = "Clock descrepancy = " & FormatNumber(MyClockDescripancy, 3) & " sec."
    End If

    MyIsCalculating = False
  End Sub

  Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
    If lstFramePairs.SelectedIndex <> -1 Then
      MyFramePairs.RemoveAt(lstFramePairs.SelectedIndex)
      lstFramePairs.Items.RemoveAt(lstFramePairs.SelectedIndex)
      If lstFramePairs.Items.Count = 0 Then
        MyClockDescripancy = MyVideoFile1.GetAbsTime(0) - MyVideoFile2.GetAbsTime(0)
      End If
      Call CalculateParameters()
    End If
  End Sub

  Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
    MyFramePairs.Clear()
    lstFramePairs.Items.Clear()
    MyClockDescripancy = 0
    Call CalculateParameters()
  End Sub

End Class