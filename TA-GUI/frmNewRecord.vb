﻿Public Class frmNewRecord
  Public VideoFile As String
  Public StartTime As Double
  Public DataPointCount As Integer
  Public DataPointFrequency As Double
  Public AddRecord As Boolean


  Private MyParentProject As clsProject
  Private MyVideoLength As Double
  Private MyVideoFPS As Double
  Private MyVideoFrameCount As Double


  Public Sub New(ByVal ParentProject As clsProject)

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    MyParentProject = ParentProject

    Me.AddRecord = False
    Me.VideoFile = ""
    Me.StartTime = NoValue
    Me.DataPointCount = NoValue
    Me.DataPointFrequency = NoValue
    btnOK.Enabled = False
  End Sub



  Private Sub btnLoadVideoFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadVideoFile.Click
    Dim _VideoReader As clsVideoReader
    Dim _OldBitmap As Bitmap


    With New OpenFileDialog
      .FileName = ""
      .Multiselect = False
            .Filter = "Video files (*.avi, *.mp4, *.mov, *.mpg, *.mkv)|*.avi;*.mp4;*.mov;*.mpg; *.mkv|Zipped frames (*.zip)|*.zip"
            .CheckFileExists = True
      .InitialDirectory = MyParentProject.VideoDirectory
      .Title = "Select video file"
      .ShowDialog()

      If .FileName.Length > 0 Then
        Try
          _VideoReader = New clsVideoReader(.FileName)
          _OldBitmap = _VideoReader.GetFrame(0)

          MyVideoFrameCount = _VideoReader.FrameCount
          MyVideoFPS = _VideoReader.FPS
          MyVideoLength = MyVideoFrameCount / MyVideoFPS
          Me.StartTime = _VideoReader.GetTimeStamp(0)

          _VideoReader.Close()
          _VideoReader = Nothing

        Catch ex As Exception
          MsgBox("OpenCV seems to have problems reading the video file '" & .FileName & "'. Try convert the video to some other format.")
          _VideoReader = Nothing
          Exit Sub
        End Try


        Me.VideoFile = .FileName
        txtVideoFile.Text = Path.GetFileName(VideoFile)

        picFirstFrame.Image = ScaleImageToFitGivenSize(_OldBitmap, picFirstFrame.Width, picFirstFrame.Height)
        picFirstFrame.Visible = True

        lblFrameSizeValue.Text = _OldBitmap.Width.ToString & "x" & _OldBitmap.Height
        lblFrameSize.Visible = True
        lblFrameSizeValue.Visible = True
        _OldBitmap.Dispose()

        lblLengthValue.Text = SecondsToTimeDisplay(MyVideoLength)
        lblLength.Visible = True
        lblLengthValue.Visible = True

        lblFrameCountValue.Text = MyVideoFrameCount.ToString
        lblFrameCount.Visible = True
        lblFrameCountValue.Visible = True

        lblFPSValue.Text = MyVideoFPS.ToString("0.000")
        lblFPS.Visible = True
        lblFPSValue.Visible = True


        txtDateTime.Text = Format(Date.FromOADate(Me.StartTime / 3600 / 24), "dd-MM-yyyy   HH:mm:ss.fff")

        Me.DataPointFrequency = 15
        lblDataPointFrequencyValue.Text = Me.DataPointFrequency.ToString("0.000")

        Me.DataPointCount = CInt(MyVideoLength * Me.DataPointFrequency)
        lblDataPointCountValue.Text = Me.DataPointCount.ToString

        pnlRecord.Visible = True

        btnOK.Enabled = True

        If Me.StartTime = 2209161600 Then MsgBox("The internal project structure require that each video file is time-referenced. By default, a file is assigned the date '1970-01-01   00:00:00'." &
                                          " If you want to use other date, rename the video file before importing into format 'yyyymmdd-HHMMss', e.g. '20150103-120500.avi'.")
      End If
    End With
  End Sub




  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    Me.AddRecord = True
    Me.Hide()
  End Sub



  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    Me.AddRecord = False
    Me.Hide()
  End Sub



  Private Sub btnChangeFPS_Click(sender As Object, e As EventArgs) Handles btnChangeFPS.Click
    Dim _NewFPS As Double
    Dim _NewFPSstring As String
    Dim _Length As Double
    Dim _NewDataPointCount As Integer


    _NewFPSstring = InputBox("Enter the new data point frequency value:", "Change data point frequency", "15")

    If Double.TryParse(_NewFPSstring, _NewFPS) Then
      If IsBetween(_NewFPS, 1, 100) Then
        Me.DataPointFrequency = _NewFPS
        lblDataPointFrequencyValue.Text = Me.DataPointFrequency.ToString("0.000")

        Me.DataPointCount = CInt(MyVideoLength * Me.DataPointFrequency)
        lblDataPointCountValue.Text = Me.DataPointCount.ToString
      End If
    End If
  End Sub

  Private Sub frmNewRecord_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub
End Class