﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportDataIndividualVideos
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.txtImportFileList = New System.Windows.Forms.TextBox()
    Me.btnSelectFiles = New System.Windows.Forms.Button()
    Me.btnSelectCalibration = New System.Windows.Forms.Button()
    Me.txtCalibrationFile = New System.Windows.Forms.TextBox()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.btnClearFiles = New System.Windows.Forms.Button()
    Me.btnClearCalibration = New System.Windows.Forms.Button()
    Me.lblCalibration = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.grpFrameRape = New System.Windows.Forms.GroupBox()
    Me.txtFPS = New System.Windows.Forms.TextBox()
    Me.rbtSetFPSTo = New System.Windows.Forms.RadioButton()
    Me.rbtUseVideoFPS = New System.Windows.Forms.RadioButton()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.btnClearMap = New System.Windows.Forms.Button()
    Me.txtMapFile = New System.Windows.Forms.TextBox()
    Me.btnSelectMap = New System.Windows.Forms.Button()
    Me.grpFrameRape.SuspendLayout()
    Me.SuspendLayout()
    '
    'txtImportFileList
    '
    Me.txtImportFileList.Enabled = False
    Me.txtImportFileList.Location = New System.Drawing.Point(2, 35)
    Me.txtImportFileList.Multiline = True
    Me.txtImportFileList.Name = "txtImportFileList"
    Me.txtImportFileList.Size = New System.Drawing.Size(376, 212)
    Me.txtImportFileList.TabIndex = 0
    '
    'btnSelectFiles
    '
    Me.btnSelectFiles.Location = New System.Drawing.Point(384, 87)
    Me.btnSelectFiles.Name = "btnSelectFiles"
    Me.btnSelectFiles.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectFiles.TabIndex = 1
    Me.btnSelectFiles.Text = "Select"
    Me.btnSelectFiles.UseVisualStyleBackColor = True
    '
    'btnSelectCalibration
    '
    Me.btnSelectCalibration.Location = New System.Drawing.Point(751, 115)
    Me.btnSelectCalibration.Name = "btnSelectCalibration"
    Me.btnSelectCalibration.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectCalibration.TabIndex = 2
    Me.btnSelectCalibration.Text = "Select"
    Me.btnSelectCalibration.UseVisualStyleBackColor = True
    '
    'txtCalibrationFile
    '
    Me.txtCalibrationFile.Enabled = False
    Me.txtCalibrationFile.Location = New System.Drawing.Point(469, 116)
    Me.txtCalibrationFile.Multiline = True
    Me.txtCalibrationFile.Name = "txtCalibrationFile"
    Me.txtCalibrationFile.Size = New System.Drawing.Size(276, 49)
    Me.txtCalibrationFile.TabIndex = 3
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(751, 280)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'btnOK
    '
    Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnOK.Location = New System.Drawing.Point(685, 280)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 8
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnClearFiles
    '
    Me.btnClearFiles.Location = New System.Drawing.Point(384, 115)
    Me.btnClearFiles.Name = "btnClearFiles"
    Me.btnClearFiles.Size = New System.Drawing.Size(60, 22)
    Me.btnClearFiles.TabIndex = 9
    Me.btnClearFiles.Text = "Clear"
    Me.btnClearFiles.UseVisualStyleBackColor = True
    '
    'btnClearCalibration
    '
    Me.btnClearCalibration.Location = New System.Drawing.Point(751, 143)
    Me.btnClearCalibration.Name = "btnClearCalibration"
    Me.btnClearCalibration.Size = New System.Drawing.Size(60, 22)
    Me.btnClearCalibration.TabIndex = 10
    Me.btnClearCalibration.Text = "Clear"
    Me.btnClearCalibration.UseVisualStyleBackColor = True
    '
    'lblCalibration
    '
    Me.lblCalibration.AutoSize = True
    Me.lblCalibration.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblCalibration.Location = New System.Drawing.Point(466, 96)
    Me.lblCalibration.Name = "lblCalibration"
    Me.lblCalibration.Size = New System.Drawing.Size(71, 13)
    Me.lblCalibration.TabIndex = 11
    Me.lblCalibration.Text = "Calibration:"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(2, 9)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(70, 13)
    Me.Label1.TabIndex = 12
    Me.Label1.Text = "Video files:"
    '
    'grpFrameRape
    '
    Me.grpFrameRape.Controls.Add(Me.txtFPS)
    Me.grpFrameRape.Controls.Add(Me.rbtSetFPSTo)
    Me.grpFrameRape.Controls.Add(Me.rbtUseVideoFPS)
    Me.grpFrameRape.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.grpFrameRape.Location = New System.Drawing.Point(469, 12)
    Me.grpFrameRape.Name = "grpFrameRape"
    Me.grpFrameRape.Size = New System.Drawing.Size(276, 74)
    Me.grpFrameRape.TabIndex = 14
    Me.grpFrameRape.TabStop = False
    Me.grpFrameRape.Text = "Data points / s"
    '
    'txtFPS
    '
    Me.txtFPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtFPS.Location = New System.Drawing.Point(216, 30)
    Me.txtFPS.Name = "txtFPS"
    Me.txtFPS.Size = New System.Drawing.Size(51, 20)
    Me.txtFPS.TabIndex = 2
    Me.txtFPS.Text = "15"
    '
    'rbtSetFPSTo
    '
    Me.rbtSetFPSTo.AutoSize = True
    Me.rbtSetFPSTo.Checked = True
    Me.rbtSetFPSTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rbtSetFPSTo.Location = New System.Drawing.Point(141, 31)
    Me.rbtSetFPSTo.Name = "rbtSetFPSTo"
    Me.rbtSetFPSTo.Size = New System.Drawing.Size(69, 17)
    Me.rbtSetFPSTo.TabIndex = 1
    Me.rbtSetFPSTo.TabStop = True
    Me.rbtSetFPSTo.Text = "Set all to:"
    Me.rbtSetFPSTo.UseVisualStyleBackColor = True
    '
    'rbtUseVideoFPS
    '
    Me.rbtUseVideoFPS.AutoSize = True
    Me.rbtUseVideoFPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rbtUseVideoFPS.Location = New System.Drawing.Point(6, 31)
    Me.rbtUseVideoFPS.Name = "rbtUseVideoFPS"
    Me.rbtUseVideoFPS.Size = New System.Drawing.Size(90, 17)
    Me.rbtUseVideoFPS.TabIndex = 0
    Me.rbtUseVideoFPS.Text = "Use video fps"
    Me.rbtUseVideoFPS.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label2.Location = New System.Drawing.Point(466, 178)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(35, 13)
    Me.Label2.TabIndex = 18
    Me.Label2.Text = "Map:"
    '
    'btnClearMap
    '
    Me.btnClearMap.Location = New System.Drawing.Point(751, 225)
    Me.btnClearMap.Name = "btnClearMap"
    Me.btnClearMap.Size = New System.Drawing.Size(60, 22)
    Me.btnClearMap.TabIndex = 17
    Me.btnClearMap.Text = "Clear"
    Me.btnClearMap.UseVisualStyleBackColor = True
    '
    'txtMapFile
    '
    Me.txtMapFile.Enabled = False
    Me.txtMapFile.Location = New System.Drawing.Point(469, 198)
    Me.txtMapFile.Multiline = True
    Me.txtMapFile.Name = "txtMapFile"
    Me.txtMapFile.Size = New System.Drawing.Size(276, 49)
    Me.txtMapFile.TabIndex = 16
    '
    'btnSelectMap
    '
    Me.btnSelectMap.Location = New System.Drawing.Point(751, 197)
    Me.btnSelectMap.Name = "btnSelectMap"
    Me.btnSelectMap.Size = New System.Drawing.Size(60, 22)
    Me.btnSelectMap.TabIndex = 15
    Me.btnSelectMap.Text = "Select"
    Me.btnSelectMap.UseVisualStyleBackColor = True
    '
    'frmImportData
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(820, 304)
    Me.ControlBox = False
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClearMap)
    Me.Controls.Add(Me.txtMapFile)
    Me.Controls.Add(Me.btnSelectMap)
    Me.Controls.Add(Me.grpFrameRape)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.lblCalibration)
    Me.Controls.Add(Me.btnClearCalibration)
    Me.Controls.Add(Me.btnClearFiles)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.Controls.Add(Me.txtCalibrationFile)
    Me.Controls.Add(Me.btnSelectCalibration)
    Me.Controls.Add(Me.btnSelectFiles)
    Me.Controls.Add(Me.txtImportFileList)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmImportData"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "Import individual videos"
    Me.grpFrameRape.ResumeLayout(False)
    Me.grpFrameRape.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txtImportFileList As System.Windows.Forms.TextBox
  Friend WithEvents btnSelectFiles As System.Windows.Forms.Button
  Friend WithEvents btnSelectCalibration As System.Windows.Forms.Button
  Friend WithEvents txtCalibrationFile As System.Windows.Forms.TextBox
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnClearFiles As System.Windows.Forms.Button
  Friend WithEvents btnClearCalibration As System.Windows.Forms.Button
  Friend WithEvents lblCalibration As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents grpFrameRape As System.Windows.Forms.GroupBox
  Friend WithEvents rbtSetFPSTo As System.Windows.Forms.RadioButton
  Friend WithEvents rbtUseVideoFPS As System.Windows.Forms.RadioButton
  Friend WithEvents txtFPS As System.Windows.Forms.TextBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents btnClearMap As System.Windows.Forms.Button
  Friend WithEvents txtMapFile As System.Windows.Forms.TextBox
  Friend WithEvents btnSelectMap As System.Windows.Forms.Button
End Class
