﻿Public Class frmFilter

  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    Me.Hide()
  End Sub

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    txtFilter.Text = ""
    Me.Hide()
  End Sub

  Private Sub cmdClearFilter_Click(sender As Object, e As EventArgs) Handles cmdClearFilter.Click
    txtFilter.Text = ""
  End Sub

  Private Sub cmdType_Click(sender As Object, e As EventArgs) Handles cmdType.Click
    Call QuickFilter(txtFilter, "(cmType = )", 10)
  End Sub

  Private Sub cmdStatus_Click(sender As Object, e As EventArgs) Handles cmdStatus.Click
    Call QuickFilter(txtFilter, "(cmStatus = )", 12)
  End Sub

  Private Sub cmdAND_Click(sender As Object, e As EventArgs) Handles cmdAND.Click
    Call QuickFilter(txtFilter, " AND ")
  End Sub

  Private Sub cmdOR_Click(sender As Object, e As EventArgs) Handles cmdOR.Click
    Call QuickFilter(txtFilter, " OR ")
  End Sub


  Private Sub QuickFilter(ByVal TextBox As TextBox, ByVal InsertText As String, Optional ByVal SelectAt As Integer = -1)
    Dim _Position, _Length As Integer
    Dim _Filter As String


    txtFilter.Focus()
    _Position = txtFilter.SelectionStart
    _Length = txtFilter.SelectionLength

    _Filter = txtFilter.Text.Remove(_Position, _Length).Insert(_Position, InsertText)

    txtFilter.Text = _Filter
    txtFilter.SelectionLength = 0

    If SelectAt > 0 Then
      txtFilter.SelectionStart = _Position + SelectAt
    Else
      txtFilter.SelectionStart = _Position + InsertText.Length
    End If
  End Sub

  Private Sub btnID_Click(sender As Object, e As EventArgs) Handles btnID.Click
    Call QuickFilter(txtFilter, "(cmID = )", 8)
  End Sub

  Private Sub btnTrue_Click(sender As Object, e As EventArgs) Handles btnTrue.Click
    Call QuickFilter(txtFilter, " TRUE")
  End Sub

  Private Sub btnConflict_Click(sender As Object, e As EventArgs) Handles btnConflict.Click
    Call QuickFilter(txtFilter, "(uDT_Conflict = )", 16)
  End Sub
End Class