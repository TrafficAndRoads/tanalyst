﻿Public Class clsDisplay
  Private MyTimeLine As List(Of clsTimePoint)
  Private MyRoadusers As List(Of clsRoadUser)
  Private MyVideoFiles As New List(Of clsVideoFile)
  Private crReaders As New List(Of ClipReader)
  Private iMyFramesCount As Integer
  Private sMyBackground As String
  Private iMyBackground As Image
  Private sMyScale As Single = 1
  Private iMyCurrentFrame As Integer
  Private bMyPlayingNow As Boolean
  Private oWatch As New Stopwatch
  Private lngMyPreviousTick As Long
  Private iMySpeedFactor As Integer = 1

#Region "Properties"

  Public WriteOnly Property TimeLine As List(Of clsTimePoint)
    Set(ByVal value As List(Of clsTimePoint))
      MyTimeLine = value
      iMyFramesCount = MyTimeLine.Count
    End Set
  End Property

  Public WriteOnly Property RoadUsers As List(Of clsRoadUser)
    Set(ByVal value As List(Of clsRoadUser))
      MyRoadusers = value
    End Set
  End Property

  Public Property VideoFiles() As List(Of clsVideoFile)
    Get
      VideoFiles = MyVideoFiles
    End Get

    Set(ByVal value As List(Of clsVideoFile))
      Dim picBox As PictureBox
      Dim VideoFile As clsVideoFile
      Dim i As Integer
      Dim iNumFramesInRow As Integer = 1
      Dim crReader As ClipReader

      MyVideoFiles = value
      crReaders.Clear()

      For i = 0 To tabDisplay.TabPages.Count - 1
        If Not tabDisplay.TabPages(i).Name.Equals("tbpProjected") Then tabDisplay.TabPages.RemoveAt(i)
      Next i

      i = 0
      For Each VideoFile In MyVideoFiles
        i = i + 1
        crReader = New ClipReader(VideoFile.VideoFileFull)
        crReaders.Add(crReader)
        tabDisplay.TabPages.Add("tbpVideo" & i, "Video " & i)
        picBox = New PictureBox
        picBox.Name = "picVideo" & i
        picBox.Height = crReader.FrameSize.Height
        picBox.Width = crReader.FrameSize.Width
        tabDisplay.TabPages("tbpVideo" & i).Controls.Add(picBox)
      Next
      Call AdjustSize()
      Display(0)
    End Set
  End Property

  Public Property ProjectedScale() As Single
    Get
      ProjectedScale = sMyScale
    End Get
    Set(ByVal value As Single)
      sMyScale = value
    End Set
  End Property

  Public Property Background As String
    Get
      Background = sMyBackground
    End Get
    Set(ByVal value As String)
      Dim picPictureBox As PictureBox

      sMyBackground = value
      If sMyBackground.Length > 0 Then
        If File.Exists(sMyBackground) Then
          iMyBackground = New Bitmap(sMyBackground)

          If tabDisplay.TabPages.ContainsKey("tbpProjected") Then
            picPictureBox = CType(tabDisplay.TabPages("tbpProjected").Controls("PicProjected"), PictureBox)
          Else
            tabDisplay.TabPages.Insert(0, "tbpProjected", "Projected")
            picPictureBox = New PictureBox
            picPictureBox.Name = "picProjected"
            tabDisplay.TabPages(0).Controls.Add(picPictureBox)
          End If
          picPictureBox.Height = iMyBackground.Height
          picPictureBox.Width = iMyBackground.Width
          picPictureBox.Image = iMyBackground
        Else
          sMyBackground = ""
          iMyBackground = Nothing
          If tabDisplay.TabPages.ContainsKey("tbpProjected") Then tabDisplay.TabPages.RemoveByKey("tbpProjected")
        End If
      End If
      Call AdjustSize()
    End Set
  End Property

  Public Property CurrentFrame() As Integer
    Get
      CurrentFrame = iMyCurrentFrame
    End Get
    Set(ByVal value As Integer)
      iMyCurrentFrame = value
    End Set
  End Property

#End Region


#Region "Public Subs"

  Public Sub Clear()
    Dim crReader As ClipReader

    tabDisplay.TabPages.Clear()
    iMySpeedFactor = 1
    btnSpeedFactor.Text = "x1"
    For Each crReader In crReaders
      crReader.Close()
    Next
    MyVideoFiles.Clear()
    crReaders.Clear()
    oWatch.Stop()
  End Sub

#End Region



#Region "Private subs"

  Private Sub AdjustSize()
    Dim iWidth As Integer = -1
    Dim iHeight As Integer = -1
    Dim i As Integer

    With tabDisplay
      For i = 0 To .TabPages.Count - 1
        Select Case .TabPages(i).Name
          Case "tbpProjected"
            iWidth = .TabPages(i).Controls("picProjected").Width
            iHeight = .TabPages(i).Controls("picProjected").Height
          Case Else
            If .TabPages(i).Controls("picVideo" & i).Width > iWidth Then iWidth = .TabPages(i).Controls("picVideo" & i).Width
            If .TabPages(i).Controls("picVideo" & i).Height > iHeight Then iHeight = .TabPages(i).Controls("picVideo" & i).Height
        End Select
      Next i


      If (iWidth > 0) And (iHeight > 0) Then
        .Width = iWidth
        .Height = iHeight
        .Height = iHeight + (.Height - .TabPages(0).Height)

        For i = 0 To .TabPages.Count - 1
          Select Case .TabPages(i).Name
            Case "tbpProjected"
              .TabPages(i).Controls("picProjected").Left = CInt((iWidth - .TabPages(i).Controls("picProjected").Width) / 2)
              .TabPages(i).Controls("picProjected").Top = CInt((iHeight - .TabPages(i).Controls("picProjected").Height) / 2)
            Case Else
              .TabPages(i).Controls("picVideo" & i).Left = CInt((iWidth - .TabPages(i).Controls("picVideo" & i).Width) / 2)
              .TabPages(i).Controls("picVideo" & i).Top = CInt((iHeight - .TabPages(i).Controls("picVideo" & i).Height) / 2)
          End Select
        Next i

        trbFrames.Minimum = 0
        trbFrames.Maximum = iMyFramesCount - 1
        trbFrames.Top = tabDisplay.Bottom
        trbFrames.Width = iWidth
        lblCurrentTime.Top = trbFrames.Bottom
        lblTotalTime.Top = trbFrames.Bottom
        lblCurrentTime.Left = 0
        lblTotalTime.Left = trbFrames.Right - lblTotalTime.Width

        Me.Width = .Width
        Me.Height = .Height + tabDisplay.Top + trbFrames.Height + lblCurrentTime.Height
      Else
        Me.Width = 0
        Me.Height = 0
        Me.Visible = False
      End If
    End With
  End Sub

  Private Sub Display(Optional ByVal iFrame As Integer = -1)
    Dim G As System.Drawing.Graphics
    Dim iBackgroundTemp As Image
    Dim f As Frame
    Dim picBox As PictureBox
    Dim iRoadUser As Integer
    Dim i As Integer

    If iFrame >= 0 Then iMyCurrentFrame = iFrame
    Select tabDisplay.SelectedTab.Name
      Case "tbpProjected"
        picBox = CType(tabDisplay.SelectedTab.Controls("picProjected"), PictureBox)
        iBackgroundTemp = New Bitmap(iMyBackground)
        G = System.Drawing.Graphics.FromImage(iBackgroundTemp)
        For Each iRoadUser In MyTimeLine(iMyCurrentFrame).RoadUsers
          DrawRoadUserProjected(G, MyRoadusers(iRoadUser), iMyCurrentFrame)
        Next
        picBox.Image = iBackgroundTemp
        G.Dispose()
      Case Else
        i = CInt(tabDisplay.SelectedTab.Name.Remove(0, 8))
        picBox = CType(tabDisplay.SelectedTab.Controls("picVideo" & i), PictureBox)
        f = crReaders(i - 1).GetFrame(iMyCurrentFrame)
        For Each iRoadUser In MyTimeLine(iMyCurrentFrame).RoadUsers
          DrawRoadUserTransformed(f, MyVideoFiles(i - 1), MyRoadusers(iRoadUser), iMyCurrentFrame)
        Next
        picBox.Image = f
    End Select
    lblCurrentFrame.Text = iMyCurrentFrame.ToString("00000")
    btnNext.Left = lblCurrentFrame.Right

    trbFrames.Value = iMyCurrentFrame
    lblCurrentTime.Text = TimeDisplay(MyTimeLine(iMyCurrentFrame).Time)
    lblTotalTime.Text = TimeDisplay(MyTimeLine(iMyFramesCount - 1).Time)
  End Sub

  Private Sub DrawRoadUserProjected(ByVal g As Graphics, ByVal RoadUser As clsRoadUser, ByVal iFrame As Integer)
    Dim Xg As Single, Yg As Single
    Dim pxlLength As Single, pxlWidth As Single
    Dim Dx As Single, Dy As Single
    Dim pPen As Pen
    Dim P() As Point

    If intBetween(iFrame, RoadUser.FirstFrame, RoadUser.LastFrame) Then
      pPen = New Pen(Brushes.DarkGreen, 3)
      pPen.LineJoin = Drawing2D.LineJoin.Round
      Xg = RoadUser.DataPoint(iFrame).X * sMyScale
      Yg = RoadUser.DataPoint(iFrame).Y * sMyScale
      pxlLength = RoadUser.Length * sMyScale
      pxlWidth = RoadUser.Width * sMyScale
      Dx = RoadUser.DataPoint(iFrame).Dx
      Dy = RoadUser.DataPoint(iFrame).Dy
      ReDim P(4)
      P(0) = RoadUserPoint("LeftFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(1) = RoadUserPoint("RightFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(2) = RoadUserPoint("RightBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(3) = RoadUserPoint("LeftBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(4) = P(0)
      g.DrawLines(pPen, P)
      ReDim P(3)
      P(0) = RoadUserPoint("MiddleFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(1) = RoadUserPoint("MiddleMiddleRight", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(2) = RoadUserPoint("MiddleMiddleLeft", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P(3) = P(0)
      g.DrawLines(pPen, P)
      pPen.Dispose()
    End If
  End Sub

  Private Sub DrawRoadUserTransformed(ByVal f As Frame, ByVal VideoFile As clsVideoFile, ByVal RoadUser As clsRoadUser, ByVal iFrame As Integer)
    Dim Xg As Single, Yg As Single
    Dim pxlLength As Single, pxlWidth As Single, pxlHeight As Single
    Dim Dx As Single, Dy As Single
    Dim P1 As Point, P2 As Point, P3 As Point, P4 As Point
    Dim P1mid As Point, P2mid As Point, P3mid As Point
    Dim P1up As Point, P2up As Point, P3up As Point, P4up As Point

    If intBetween(iFrame, RoadUser.FirstFrame, RoadUser.LastFrame) Then
      Xg = RoadUser.DataPoint(iFrame).X * sMyScale
      Yg = RoadUser.DataPoint(iFrame).Y * sMyScale
      pxlLength = RoadUser.Length * sMyScale
      pxlWidth = RoadUser.Width * sMyScale
      pxlHeight = RoadUser.Height * sMyScale
      Dx = RoadUser.DataPoint(iFrame).Dx
      Dy = RoadUser.DataPoint(iFrame).Dy

      P1 = RoadUserPoint("LeftFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P1 = VideoFile.Transform(P1.X, P1.Y)
      P2 = RoadUserPoint("RightFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P2 = VideoFile.Transform(P2.X, P2.Y)
      P3 = RoadUserPoint("RightBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P3 = VideoFile.Transform(P3.X, P3.Y)
      P4 = RoadUserPoint("LeftBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P4 = VideoFile.Transform(P4.X, P4.Y)

      P1mid = RoadUserPoint("MiddleFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P1mid = VideoFile.Transform(P1mid.X, P1mid.Y)
      P2mid = RoadUserPoint("MiddleMiddleRight", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P2mid = VideoFile.Transform(P2mid.X, P2mid.Y)
      P3mid = RoadUserPoint("MiddleMiddleLeft", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P3mid = VideoFile.Transform(P3mid.X, P3mid.Y)

      P1up = RoadUserPoint("LeftFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P1up = VideoFile.Transform(P1up.X, P1up.Y, -pxlHeight)
      P2up = RoadUserPoint("RightFront", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P2up = VideoFile.Transform(P2up.X, P2up.Y, -pxlHeight)
      P3up = RoadUserPoint("RightBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P3up = VideoFile.Transform(P3up.X, P3up.Y, -pxlHeight)
      P4up = RoadUserPoint("LeftBack", Xg, Yg, pxlLength, pxlWidth, Dx, Dy)
      P4up = VideoFile.Transform(P4up.X, P4up.Y, -pxlHeight)

      f.DrawLine(P1, P2, Color.Red)
      f.DrawLine(P2, P3, Color.Red)
      f.DrawLine(P3, P4, Color.Red)
      f.DrawLine(P4, P1, Color.Red)
      f.DrawLine(P1mid, P2mid, Color.Red)
      f.DrawLine(P2mid, P3mid, Color.Red)
      f.DrawLine(P3mid, P1mid, Color.Red)
      f.DrawLine(P1up, P2up, Color.White)
      f.DrawLine(P2up, P3up, Color.White)
      f.DrawLine(P3up, P4up, Color.White)
      f.DrawLine(P4up, P1up, Color.White)
      f.DrawLine(P1, P1up, Color.White)
      f.DrawLine(P2, P2up, Color.White)
      f.DrawLine(P3, P3up, Color.White)
      f.DrawLine(P4, P4up, Color.White)
    End If
  End Sub

  Private Function RoadUserPoint(ByVal WhatPoint As String, ByVal Xg As Single, ByVal Yg As Single, ByVal pxlLength As Single, ByVal pxlWidth As Single, ByVal Dx As Single, ByVal Dy As Single) As Point
    Dim p As New Point
    Select Case WhatPoint
      Case "LeftFront"
        p.X = CInt(Xg + (pxlLength * Dx - pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (pxlLength * Dy + pxlWidth * Dx) / 2)

      Case "RightFront"
        p.X = CInt(Xg + (pxlLength * Dx + pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (pxlLength * Dy - pxlWidth * Dx) / 2)

      Case "RightBack"
        p.X = CInt(Xg + (-pxlLength * Dx + pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (-pxlLength * Dy - pxlWidth * Dx) / 2)

      Case "LeftBack"
        p.X = CInt(Xg + (-pxlLength * Dx - pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (-pxlLength * Dy + pxlWidth * Dx) / 2)

      Case "MiddleFront"
        p.X = CInt(Xg + pxlLength * Dx / 2)
        p.Y = CInt(Yg + pxlLength * Dy / 2)

      Case "MiddleMiddleLeft"
        p.X = CInt(Xg + (pxlLength / 2 * Dx - pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (pxlLength / 2 * Dy + pxlWidth * Dx) / 2)


      Case "MiddleMiddleRight"
        p.X = CInt(Xg + (pxlLength / 2 * Dx + pxlWidth * Dy) / 2)
        p.Y = CInt(Yg + (pxlLength / 2 * Dy - pxlWidth * Dx) / 2)


        '      Case "500m ahead"
        '       X_Point = x + 500 * Dx
        '      Y_Point = y + 500 * Dy

    End Select


    Return p
  End Function

  Private Function TimeDisplay(ByVal sTime As Single) As String
    Dim iHours As Integer
    Dim iMinutes As Integer
    Dim iSeconds As Integer
    Dim sDisplay As String

    iHours = CInt(Math.Truncate(sTime / 3600))
    sTime = sTime - iHours * 3600
    iMinutes = CInt(Math.Truncate(sTime / 60))
    sTime = sTime - iMinutes * 60
    iSeconds = CInt(sTime)

    sDisplay = iHours.ToString & ":" & iMinutes.ToString("00") & ":" & iSeconds.ToString("00")
    Return sDisplay
  End Function

#End Region



#Region "Control Subs"

  Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
    If iMyCurrentFrame < iMyFramesCount - 1 Then
      Call Display(iMyCurrentFrame + 1)
    End If
  End Sub

  Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    If iMyCurrentFrame > 0 Then
      Call Display(iMyCurrentFrame - 1)
    End If
  End Sub

  Private Sub tmrFrames_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrFrames.Tick
    Dim iStep As Integer
    Dim iTimeToLoad As Integer
    Dim iDelay As Integer
    Dim iLostFrames As Integer = 0
    Dim iFrameExposureTime As Integer
    Dim lngTickNow As Long

    tmrFrames.Enabled = False
    If iMyCurrentFrame < iMyFramesCount - 1 Then
      Call Display()
      lngTickNow = oWatch.ElapsedMilliseconds
      iTimeToLoad = CInt(lngTickNow - lngMyPreviousTick)
      iFrameExposureTime = CInt(1000 * (MyTimeLine(iMyCurrentFrame + 1).Time - MyTimeLine(iMyCurrentFrame).Time) / iMySpeedFactor)
      iDelay = iFrameExposureTime - (iTimeToLoad Mod iFrameExposureTime)
      iStep = (iTimeToLoad \ iFrameExposureTime) + 1
      iMyCurrentFrame = iMyCurrentFrame + iStep
      lngMyPreviousTick = lngTickNow + iDelay
      tmrFrames.Interval = iDelay
      tmrFrames.Enabled = True
    Else
      chkPlay.Checked = False
      chkPlay.ImageKey = "Play"
      tltToolTip.SetToolTip(chkPlay, "Play")
      trbFrames.Enabled = True
      bMyPlayingNow = False
      Call Display(0)
    End If
  End Sub

  Private Sub chkPlay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPlay.CheckedChanged
    If chkPlay.Checked Then
      oWatch.Reset()
      oWatch.Start()
      lngMyPreviousTick = 1
      tmrFrames.Interval = 1
      tmrFrames.Enabled = True
      chkPlay.ImageKey = "Pause"
      tltToolTip.SetToolTip(chkPlay, "Pause")
      trbFrames.Enabled = False
      bMyPlayingNow = True
    Else
      oWatch.Stop()
      tmrFrames.Enabled = False
      chkPlay.ImageKey = "Play"
      tltToolTip.SetToolTip(chkPlay, "Play")
      trbFrames.Enabled = True
      bMyPlayingNow = False
    End If
  End Sub

  Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
    oWatch.Stop()
    tmrFrames.Enabled = False
    chkPlay.Checked = False
    chkPlay.ImageKey = "Play"
    tltToolTip.SetToolTip(chkPlay, "Play")
    trbFrames.Enabled = True
    bMyPlayingNow = False
    Call Display(0)
  End Sub

  Private Sub trbFrames_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles trbFrames.MouseUp
    iMyCurrentFrame = trbFrames.Value
    Call Display()
  End Sub

  Private Sub trbFrames_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles trbFrames.Scroll
    tltToolTip.SetToolTip(trbFrames, TimeDisplay(MyTimeLine(trbFrames.Value).Time))
  End Sub

  Private Sub lblCurrentFrame_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCurrentFrame.DoubleClick
    Dim sFrame As String
    Dim iFrame As Integer

    If Not bMyPlayingNow Then
      sFrame = InputBox("Move to frame:", "Frame navigation")
      If Integer.TryParse(sFrame, iFrame) Then
        If intBetween(iFrame, 0, iMyFramesCount - 1) Then
          Display(iFrame)
        End If
      End If
    End If
  End Sub

  Private Sub tabDisplay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabDisplay.SelectedIndexChanged
    If tabDisplay.TabPages.Count > 0 Then Display()
  End Sub

  Private Sub btnSpeedFactor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpeedFactor.Click
    Select Case iMySpeedFactor
      Case 1
        iMySpeedFactor = 2
        btnSpeedFactor.Text = "x2"
      Case 2
        iMySpeedFactor = 4
        btnSpeedFactor.Text = "x4"
      Case 4
        iMySpeedFactor = 8
        btnSpeedFactor.Text = "x8"
      Case 8
        iMySpeedFactor = 1
        btnSpeedFactor.Text = "x1"
    End Select
  End Sub

#End Region

  Private Sub clsDisplay_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

  End Sub
End Class
