﻿Public Class frmImportDataRuba
  Public DoImport As Boolean
  Public RubaOtputFile As String
  Public VideoFileList As List(Of String)
  Public MediaFileList As List(Of String)
  Public CalibrationFile As String
  Public MapFile As String
  Public FrameRate As Double
  Public UseExistingFPS As Boolean

  Private MyParentProject As clsProject


  Public Sub New(ParentProject As clsProject)
    InitializeComponent()

    Me.DoImport = False
    Me.RubaOtputFile = ""
    Me.VideoFileList = New List(Of String)
    Me.MediaFileList = New List(Of String)
    Me.MapFile = ""
    Me.CalibrationFile = ""
    Me.CalibrationFile = ""
    Me.UseExistingFPS = False
    Me.FrameRate = 15
    rbtSetFPSTo.Checked = True
    MyParentProject = ParentProject
  End Sub



  Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
    Me.DoImport = True
    Me.Hide()
  End Sub



  Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    Me.DoImport = False
    Me.Hide()
  End Sub



  Private Sub btnSelectFiles_Click(sender As Object, e As EventArgs) Handles btnSelectVideoFiles.Click
    Dim _Dialog As OpenFileDialog
    Dim _FileList As String

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = True
      .Filter = "Video files (*.avi, mp4)|*.avi;*.mp4|Zipped frames (*.zip)|*.zip"
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.VideoDirectory
      .ShowDialog()

      If .FileNames.Length > 0 Then
        Me.VideoFileList.Clear()
        _FileList = ""
        For _i = 1 To .FileNames.Length
          _FileList = _FileList & .FileNames(_i - 1) & Environment.NewLine
          Me.VideoFileList.Add(.FileNames(_i - 1))
        Next
        txtVideoFileList.Text = _FileList
      End If
    End With
  End Sub



  Private Sub btnClearFiles_Click(sender As Object, e As EventArgs) Handles btnClearVideoFiles.Click
    Me.VideoFileList.Clear()
    txtVideoFileList.Text = ""
  End Sub



  Private Sub btnSelectCalibration_Click(sender As Object, e As EventArgs) Handles btnSelectCalibration.Click
    Dim _Dialog As OpenFileDialog

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = False
      .Filter = "Calibration files (*.tacal)|*.tacal"
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .FileName = ""
      .ShowDialog()

      If .FileName.Length > 0 Then
        Me.CalibrationFile = .FileName
        txtCalibrationFile.Text = .FileName
      End If
    End With
  End Sub



  Private Sub btnClearCalibration_Click(sender As Object, e As EventArgs) Handles btnClearCalibration.Click
    Me.CalibrationFile = ""
    txtCalibrationFile.Text = ""
  End Sub



  Private Sub rbtUseVideoFPS_CheckedChanged(sender As Object, e As EventArgs) Handles rbtUseVideoFPS.CheckedChanged
    If rbtUseVideoFPS.Checked Then
      Me.UseExistingFPS = True
      txtFPS.Enabled = False
      btnOK.Enabled = True
    End If
  End Sub



  Private Sub rbtSetFPSTo_CheckedChanged(sender As Object, e As EventArgs) Handles rbtSetFPSTo.CheckedChanged
    If rbtSetFPSTo.Checked Then
      Me.UseExistingFPS = False
      txtFPS.Enabled = True
      Call CheckFPStext()
    End If
  End Sub



  Private Sub txtFPS_TextChanged(sender As Object, e As EventArgs) Handles txtFPS.TextChanged
    Call CheckFPStext()
  End Sub



  Private Sub CheckFPStext()
    Dim _TempFPS As Double

    If Double.TryParse(txtFPS.Text, _TempFPS) Then
      txtFPS.ForeColor = Color.Black
      Me.FrameRate = _TempFPS
      btnOK.Enabled = True
    Else
      txtFPS.ForeColor = Color.Red
      btnOK.Enabled = False
    End If
  End Sub

  Private Sub btnSelectMap_Click(sender As Object, e As EventArgs) Handles btnSelectMap.Click
    Dim _Dialog As OpenFileDialog

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = False
      .Filter = "Map files (*.tamap)|*.tamap"
      .FileName = ""
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .ShowDialog()

      If .FileName.Length > 0 Then
        Me.MapFile = .FileName
        txtMapFile.Text = .FileName
      End If
    End With
  End Sub

  Private Sub btnClearMap_Click(sender As Object, e As EventArgs) Handles btnClearMap.Click
    Me.MapFile = ""
    txtMapFile.Text = ""
  End Sub

  Private Sub btnSelectRuba_Click(sender As Object, e As EventArgs) Handles btnSelectRubaFile.Click
    Dim _Dialog As OpenFileDialog

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = False
      .Filter = "Ruba output files (*.csv)|*.csv"
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .FileName = ""
      .ShowDialog()

      If .FileName.Length > 0 Then
        Me.RubaOtputFile = .FileName
        txtRubaOutputFile.Text = .FileName
      End If
    End With
  End Sub

  Private Sub btnSelectMediaFiles_Click(sender As Object, e As EventArgs) Handles btnSelectMediaFiles.Click
    Dim _Dialog As OpenFileDialog
    Dim _FileList As String

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = True
      .Filter = "Image files (*.png)|*.png"
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .ShowDialog()

      If .FileNames.Length > 0 Then
        Me.MediaFileList.Clear()
        _FileList = ""
        For _i = 1 To .FileNames.Length
          _FileList = _FileList & .FileNames(_i - 1) & Environment.NewLine
          Me.MediaFileList.Add(.FileNames(_i - 1))
        Next
        txtMediaFileList.Text = _FileList
      End If
    End With
  End Sub

  Private Sub btnClearRubaFile_Click(sender As Object, e As EventArgs) Handles btnClearRubaFile.Click
    Me.RubaOtputFile = ""
    txtRubaOutputFile.Text = ""
  End Sub

  Private Sub bntClearMediaFiles_Click(sender As Object, e As EventArgs) Handles bntClearMediaFiles.Click
    Me.MediaFileList.Clear()
    txtMediaFileList.Text = ""
  End Sub
End Class