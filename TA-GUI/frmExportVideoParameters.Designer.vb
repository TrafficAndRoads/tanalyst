﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExportVideoParameters
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.cmbBPS = New System.Windows.Forms.ComboBox()
    Me.lblQuality = New System.Windows.Forms.Label()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(142, 100)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 137
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'cmbBPS
    '
    Me.cmbBPS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmbBPS.FormattingEnabled = True
    Me.cmbBPS.Location = New System.Drawing.Point(65, 31)
    Me.cmbBPS.Name = "cmbBPS"
    Me.cmbBPS.Size = New System.Drawing.Size(165, 21)
    Me.cmbBPS.TabIndex = 138
    '
    'lblQuality
    '
    Me.lblQuality.AutoSize = True
    Me.lblQuality.Location = New System.Drawing.Point(20, 34)
    Me.lblQuality.Name = "lblQuality"
    Me.lblQuality.Size = New System.Drawing.Size(39, 13)
    Me.lblQuality.TabIndex = 139
    Me.lblQuality.Text = "Quality"
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(212, 100)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 140
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'frmExportVideoParameters
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(284, 134)
    Me.ControlBox = False
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.lblQuality)
    Me.Controls.Add(Me.cmbBPS)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmExportVideoParameters"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "Video export parameters"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents cmbBPS As System.Windows.Forms.ComboBox
  Friend WithEvents lblQuality As System.Windows.Forms.Label
  Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
