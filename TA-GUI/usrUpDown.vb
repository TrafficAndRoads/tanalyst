﻿Public Class usrUpDown
  Private sMyValue As Double = 0
  Private sMyDelta As Double = 0.1
  Private iMyNumDecimals As Integer = 2
  Private bTextChangedManually As Boolean = False
  Private sMyLabel As String = ""

  Public Event ValueChanged()



  Public Property Value As Double
    Get
      Value = sMyValue
    End Get
    Set(ByVal input As Double)
      bTextChangedManually = False
      sMyValue = input
      If isvalue(sMyValue) Then
        txtValue.Text = FormatNumber(sMyValue, iMyNumDecimals)
      Else
        txtValue.Text = "NoValue"
      End If
      bTextChangedManually = True
    End Set
  End Property

  Public Property Delta As Double
    Get
      Delta = sMyDelta
    End Get
    Set(ByVal value As Double)
      sMyDelta = value
    End Set
  End Property

  Public Property NumDecimals As Integer
    Get
      NumDecimals = iMyNumDecimals
    End Get
    Set(ByVal input As Integer)
      iMyNumDecimals = input
      value = sMyValue
    End Set
  End Property

  Public Property Label As String
    Get
      Label = sMyLabel
    End Get
    Set(ByVal value As String)
      sMyLabel = value
      lblName.Text = sMyLabel
      txtValue.Left = lblName.Right
      btnUp.Left = txtValue.Right
      btnDown.Left = txtValue.Right
      Me.Width = btnUp.Right
      Me.Height = btnDown.Bottom
    End Set
  End Property




  Private Sub txtValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged
    If bTextChangedManually Then
      Double.TryParse(txtValue.Text, sMyValue)
      RaiseEvent ValueChanged()
    End If
  End Sub

  Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
    If isvalue(sMyValue) Then
      bTextChangedManually = False
      sMyValue = sMyValue + sMyDelta
      txtValue.Text = FormatNumber(sMyValue, iMyNumDecimals)
      RaiseEvent ValueChanged()
      bTextChangedManually = True
    End If
  End Sub

  Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
    If isvalue(sMyValue) Then
      bTextChangedManually = False
      sMyValue = sMyValue - sMyDelta
      txtValue.Text = FormatNumber(sMyValue, iMyNumDecimals)
      RaiseEvent ValueChanged()
      bTextChangedManually = True
    End If
  End Sub

End Class
