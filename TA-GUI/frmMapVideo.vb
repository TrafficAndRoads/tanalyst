﻿Public Class frmMapVideo
  Private MyRecord As clsRecord
  Private MySelectedVideoFile As clsVideoFile
  Private MyIsLoading As Boolean
  Private MyToolTip As ToolTip



  Public Sub New(ParentRecord As clsRecord)
    Call InitializeComponent()

    MyToolTip = New ToolTip

    MyRecord = ParentRecord
    Call LoadRecord()

  End Sub

  Private Sub LoadRecord()
    Dim _VideoFile As clsVideoFile


    Call LoadMap()

    MyIsLoading = True
    lstVideo.Items.Clear()
    For Each _VideoFile In MyRecord.VideoFiles
      lstVideo.Items.Add(_VideoFile.VideoFileName)
    Next

    If lstVideo.Items.Count > 0 Then
      lstVideo.SelectedIndex = 0
      MySelectedVideoFile = MyRecord.VideoFiles(0)
    Else
      lstVideo.SelectedIndex = -1
      MySelectedVideoFile = Nothing
    End If

    MyIsLoading = False

    Call LoadVideoFile()

  End Sub

  Private Sub LoadMap()

    MyIsLoading = True

    With MyRecord.Map
      If .Available Then
        txtMap.Text = .Map
        txtMap.Visible = True

        lblMapSizeLabel.Visible = True
        lblMapSizeValue.Text = .Size.Width.ToString & "x" & .Size.Height.ToString
        lblMapSizeValue.Visible = True

        lblMapAlphaLabel.Visible = True
        lblMapAlphaValue.Text = Format(.Alpha, "0.00")
        lblMapAlphaValue.Visible = True

        lblMapScaleLabel.Visible = True
        lblMapScaleValue.Text = Format(.Scale, "0.000") & " pxl/meter"
        lblMapScaleValue.Visible = True

        lblMapXlabel.Visible = True
        lblMapXvalue.Text = Format(.X, "0.000")
        lblMapXvalue.Visible = True

        lblMapYlabel.Visible = True
        lblMapYvalue.Text = Format(.Y, "0.000")
        lblMapYvalue.Visible = True

        lblMapdXlabel.Visible = True
        lblMapdXvalue.Text = Format(.Dx, "0.00000")
        lblMapdXvalue.Visible = True

        lblMapdYlabel.Visible = True
        lblMapdYvalue.Text = Format(.Dy, "0.00000")
        lblMapdYvalue.Visible = True

        txtMapComment.Text = .Comment
        txtMapComment.Visible = True

        picMap.Image = ScaleImageToFitGivenSize(.Bitmap, picMap.Width, picMap.Height)
        picMap.Visible = True

        btnMapLoad.Visible = True
        btnMapClear.Visible = True

      Else
        txtMap.Text = ""
        txtMap.Visible = True

        lblMapSizeLabel.Visible = False
        lblMapSizeValue.Visible = False

        lblMapAlphaLabel.Visible = False
        lblMapAlphaValue.Visible = False

        lblMapScaleLabel.Visible = False
        lblMapScaleValue.Visible = False

        lblMapXlabel.Visible = False
        lblMapXvalue.Visible = False

        lblMapYlabel.Visible = False
        lblMapYvalue.Visible = False

        lblMapdXlabel.Visible = False
        lblMapdXvalue.Visible = False

        lblMapdYlabel.Visible = False
        lblMapdYvalue.Visible = False

        txtMapComment.Visible = False

        picMap.Visible = False

        btnMapLoad.Visible = True
        btnMapClear.Visible = False
      End If
    End With

    Call SetTooltips()

    MyIsLoading = False

  End Sub

  Private Sub LoadVideoFile()

    MyIsLoading = True
    If MySelectedVideoFile IsNot Nothing Then
      With MySelectedVideoFile
        lblVideoLengthLabel.Visible = True
        lblVideoLength.Text = SecondsToTimeDisplay(.FrameCount / .FrameRate)
        lblVideoLength.Visible = True

        lblVideoSizeLabel.Visible = True
        lblVideoSize.Text = .FrameSize.Width.ToString & "x" & .FrameSize.Height.ToString
        lblVideoSize.Visible = True

        lblVideoFrameCountLabel.Visible = True
        lblVideoFrameCount.Text = .FrameCount.ToString
        lblVideoFrameCount.Visible = True

        lblVideoFPSLabel.Visible = True
        lblVideoFPS.Text = Format(.FrameRate, "0.000")
        lblVideoFPS.Visible = True

        txtVideoComment.Text = .Comment
        txtVideoComment.Visible = True

        lblVideoTimeDescripancyLabel.Visible = True
        lblVideoTimeDescripancyValue.Text = FormatNumber(.ClockDiscrepancy, 3) & " s."
        lblVideoTimeDescripancyValue.Visible = True

        ' lblVideoTimeDescripancyRate.Text = "dT: " & FormatNumber(.ClockDiscrepancyRate, 15)
        ' lblVideoTimeDescripancyRate.Visible = True

        If lstVideo.SelectedIndex > 0 Then
          btnVideoSynchronise.Visible = True
        Else
          btnVideoSynchronise.Visible = False
        End If

        btnVideoCalibration.Visible = True

        btnVideoAdd.Visible = True
        btnVideoRemove.Visible = True

        btnVideoUp.Visible = True
        If lstVideo.SelectedIndex > 0 Then
          btnVideoUp.Enabled = True
        Else
          btnVideoUp.Enabled = False
        End If

        btnVideoDown.Visible = True
        If lstVideo.SelectedIndex < lstVideo.Items.Count - 1 Then
          btnVideoDown.Enabled = True
        Else
          btnVideoDown.Enabled = False
        End If

        If lstVideo.SelectedIndex = 0 Then
          btnVideoSynchronise.Visible = False
        ElseIf lstVideo.SelectedIndex > 0 Then
          btnVideoSynchronise.Visible = True
        Else
          btnVideoSynchronise.Visible = False
        End If


        picVideo.Image = ScaleImageToFitGivenSize(.GetFrameBitmap(0), picVideo.Width, picVideo.Height)
        picVideo.Visible = True
      End With

    Else
      lblVideoLengthLabel.Visible = False
      lblVideoLength.Visible = False

      lblVideoSizeLabel.Visible = False
      lblVideoSize.Visible = False

      lblVideoFrameCountLabel.Visible = False
      lblVideoFrameCount.Visible = False

      lblVideoFPSLabel.Visible = False
      lblVideoFPS.Visible = False

      txtVideoComment.Visible = False

      lblVideoTimeDescripancyLabel.Visible = False
      lblVideoTimeDescripancyValue.Visible = False
      'lblVideoTimeDescripancyRate.Visible = False

      btnVideoSynchronise.Visible = False
      btnVideoCalibration.Visible = False

      btnVideoAdd.Visible = True
      btnVideoRemove.Visible = False

      btnVideoUp.Visible = False
      btnVideoDown.Visible = False

      picVideo.Visible = False
    End If

    Call SetTooltips()

    MyIsLoading = False
  End Sub

  Private Sub SetTooltips()
    Dim _CalibrationParameters As String
    Dim _GroundPlanePoints As String


    MyToolTip.RemoveAll()


    If MySelectedVideoFile IsNot Nothing Then
      With MySelectedVideoFile
        If .CallibrationOK Then
          _CalibrationParameters = "dx:      " & .TSAI.dx.ToString &
             Environment.NewLine & "dy:      " & .TSAI.dy.ToString &
             Environment.NewLine & "Cx:      " & .TSAI.Cx.ToString &
             Environment.NewLine & "Cy:      " & .TSAI.Cy.ToString &
             Environment.NewLine & "Sx:      " & .TSAI.Sx.ToString &
             Environment.NewLine & "f:       " & .TSAI.f.ToString &
             Environment.NewLine & "k:       " & .TSAI.k.ToString &
             Environment.NewLine & "Tx:      " & .TSAI.Tx.ToString &
             Environment.NewLine & "Ty:      " & .TSAI.Ty.ToString &
             Environment.NewLine & "Tz:      " & .TSAI.Tz.ToString &
             Environment.NewLine & "r1:      " & .TSAI.r1.ToString &
             Environment.NewLine & "r2:      " & .TSAI.r2.ToString &
             Environment.NewLine & "r3:      " & .TSAI.r3.ToString &
             Environment.NewLine & "r4:      " & .TSAI.r4.ToString &
             Environment.NewLine & "r5:      " & .TSAI.r5.ToString &
             Environment.NewLine & "r6:      " & .TSAI.r6.ToString &
             Environment.NewLine & "r7:      " & .TSAI.r7.ToString &
             Environment.NewLine & "r8:      " & .TSAI.r8.ToString &
             Environment.NewLine & "r9:      " & .TSAI.r9.ToString
        Else
          _CalibrationParameters = "No calibration was imported yet"
        End If
        MyToolTip.SetToolTip(btnVideoCalibration, _CalibrationParameters)
      End With
    End If


    With MyRecord.Map
      If .Available Then
        _GroundPlanePoints = .GroundPlanePoints.Count.ToString & " ground plane points available"

        If .GroundPlanePoints.Count > 0 Then
          _GroundPlanePoints = _GroundPlanePoints & Environment.NewLine & Environment.NewLine & "X, m         Y, m         Z, m"
          For _i = 0 To .GroundPlanePoints.Count - 1
            _GroundPlanePoints = _GroundPlanePoints & Environment.NewLine & .GroundPlanePoints(_i).X.ToString("0.000") & "    " &
                                                              .GroundPlanePoints(_i).Y.ToString("0.000") & "    " &
                                                              .GroundPlanePoints(_i).Z.ToString("0.000")
            If _i > 10 Then
              _GroundPlanePoints = _GroundPlanePoints & Environment.NewLine & "..."
              Exit For
            End If
          Next _i
        End If
        MyToolTip.SetToolTip(lblMapAlphaValue, _GroundPlanePoints)
      End If
    End With



  End Sub
  Private Sub btnOK_Click() Handles btnOK.Click
    Dim _i As Integer

    For _i = 0 To lstVideo.Items.Count - 1
      If MyRecord.VideoFiles(_i).Position <> _i Then
        MyRecord.VideoFiles(_i).Position = _i
        If MyRecord.VideoFiles(_i).Status = VideoFileStatuses.Unchanged Then _
        MyRecord.VideoFiles(_i).Status = VideoFileStatuses.Updated
      End If
    Next _i

    MyRecord.ReloadFramesInTimeLine()
    Me.Close()

  End Sub





  Private Sub lstVideo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstVideo.SelectedIndexChanged
    If Not MyIsLoading Then
      If lstVideo.SelectedIndex <> -1 Then
        MySelectedVideoFile = MyRecord.VideoFiles(lstVideo.SelectedIndex)
      Else
        MySelectedVideoFile = Nothing
      End If
      Call LoadVideoFile()
    End If

  End Sub

  Private Sub btnVideoDown_Click(sender As Object, e As EventArgs) Handles btnVideoDown.Click
    Dim _StringTemp As String
    Dim _SelectedPosition As Integer

    If IsBetween(lstVideo.SelectedIndex, 0, lstVideo.Items.Count - 2) Then
      _SelectedPosition = lstVideo.SelectedIndex
      MyRecord.VideoFiles.RemoveAt(_SelectedPosition)
      MyRecord.VideoFiles.Insert(_SelectedPosition + 1, MySelectedVideoFile)

      MyIsLoading = True
      _StringTemp = lstVideo.SelectedItem.ToString
      lstVideo.Items.RemoveAt(_SelectedPosition)
      lstVideo.Items.Insert(_SelectedPosition + 1, _StringTemp)
      MyIsLoading = False
      lstVideo.SelectedIndex = _SelectedPosition + 1
    End If
  End Sub

  Private Sub btnVideoUp_Click(sender As Object, e As EventArgs) Handles btnVideoUp.Click
    Dim _StringTemp As String
    Dim _SelectedPosition As Integer

    If IsBetween(lstVideo.SelectedIndex, 1, lstVideo.Items.Count - 1) Then
      _SelectedPosition = lstVideo.SelectedIndex
      MyRecord.VideoFiles.RemoveAt(_SelectedPosition)
      MyRecord.VideoFiles.Insert(_SelectedPosition - 1, MySelectedVideoFile)

      MyIsLoading = True
      _StringTemp = lstVideo.SelectedItem.ToString
      lstVideo.Items.RemoveAt(_SelectedPosition)
      lstVideo.Items.Insert(_SelectedPosition - 1, _StringTemp)
      MyIsLoading = False
      lstVideo.SelectedIndex = _SelectedPosition - 1
    End If
  End Sub


  Private Sub btnVideoCalibration_Click(sender As Object, e As EventArgs) Handles btnVideoCalibration.Click
    Dim _OpenFileDialog As OpenFileDialog

    If MySelectedVideoFile IsNot Nothing Then
      MyIsLoading = True
      _OpenFileDialog = New OpenFileDialog
      With _OpenFileDialog
        .FileName = ""
        .Filter = "Calibration files (*.tacal)|*.tacal"
        .Multiselect = False
        .InitialDirectory = MyRecord.ImportDirectory
        .CheckFileExists = True
        .ShowDialog(Me)
        If .FileName.Length > 0 Then
          If MySelectedVideoFile.ImportCallibration(.FileName) Then
            If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then MySelectedVideoFile.Status = VideoFileStatuses.Updated
          End If
        End If
      End With
      Call SetTooltips()
      MyIsLoading = False
    End If
  End Sub

  Private Sub btnVideoAdd_Click(sender As Object, e As EventArgs) Handles btnVideoAdd.Click
    Dim _OpenFileDialog As OpenFileDialog

    _OpenFileDialog = New OpenFileDialog
    With _OpenFileDialog
      .FileName = ""
      .Filter = "Video files (*.avi, mp4)|*.avi;*.mp4|Zipped frames (*.zip)|*.zip"
      .Multiselect = False
      .CheckFileExists = True
      .InitialDirectory = MyRecord.VideoDirectory
      .ShowDialog(Me)
      If .FileName.Length > 0 Then
        If MyRecord.ImportVideoFile(.FileName) Then
          lstVideo.Items.Add(MyRecord.VideoFiles(MyRecord.VideoFiles.Count - 1).VideoFileName)
          lstVideo.SelectedIndex = lstVideo.Items.Count - 1
        End If
      End If
    End With
  End Sub

  Private Sub btnVideoSynchronise_Click(sender As Object, e As EventArgs) Handles btnVideoSynchronise.Click
    Dim f As frmSyncVideoFiles
    Dim _VideoFile1 As Integer = 0
    Dim _VideoFile2 As Integer

    If lstVideo.SelectedIndex = 0 Then
      For _i = 0 To lstVideo.Items.Count - 1
        If MyRecord.VideoFiles(_i).Status <> VideoFileStatuses.Created Then MyRecord.VideoFiles(_i).Status = VideoFileStatuses.Updated
      Next
      Call LoadVideoFile()

    ElseIf lstVideo.SelectedIndex > 0 Then
      _VideoFile2 = lstVideo.SelectedIndex

      f = New frmSyncVideoFiles(MyRecord.VideoFiles(_VideoFile1), MyRecord.VideoFiles(_VideoFile2))
      f.ShowDialog(Me)
      If f.SynchronisationOK Then
        MyIsLoading = True
        MyRecord.VideoFiles(_VideoFile2).ClockDiscrepancy = f.ClockDescripancy
        '  MyRecord.VideoFiles(_VideoFile2).ClockDiscrepancyRate = 1
        lblVideoTimeDescripancyValue.Text = FormatNumber(f.ClockDescripancy, 3) & " s."
        ' lblVideoTimeDescripancyRate.Text = "dT rate: " & FormatNumber(MyRecord.VideoFiles(_VideoFile2).ClockDiscrepancyRate, 15) & " s"

        If MyRecord.VideoFiles(_VideoFile2).Status <> VideoFileStatuses.Created Then MyRecord.VideoFiles(_VideoFile2).Status = VideoFileStatuses.Updated
        MyIsLoading = False
      End If
      f.Dispose()
    End If
  End Sub

  Private Sub btnVideoRemove_Click(sender As Object, e As EventArgs) Handles btnVideoRemove.Click
    Dim _SelectedPosition As Integer
    Dim _VideoFileTemp As clsVideoFile

    If lstVideo.SelectedIndex <> -1 Then
      If MsgBox("Do you really want to remove this video file from the record?", MsgBoxStyle.YesNo, "Remove video file") = MsgBoxResult.Yes Then
        _SelectedPosition = lstVideo.SelectedIndex

        lstVideo.Items.RemoveAt(_SelectedPosition)

        _VideoFileTemp = MyRecord.VideoFiles(_SelectedPosition)
        If _VideoFileTemp.Status = VideoFileStatuses.Created Then
          MyRecord.VideoFiles.RemoveAt(_SelectedPosition)
        Else
          MyRecord.VideoFiles.Remove(_VideoFileTemp)
          _VideoFileTemp.Status = VideoFileStatuses.Deleted
          MyRecord.VideoFiles.Add(_VideoFileTemp)
        End If
      End If
    End If
  End Sub

  Private Sub txtVideoComment_TextChanged(sender As Object, e As EventArgs) Handles txtVideoComment.TextChanged
    If MySelectedVideoFile IsNot Nothing Then
      If MySelectedVideoFile.Comment <> txtVideoComment.Text Then
        MySelectedVideoFile.Comment = txtVideoComment.Text
        If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then _
          MySelectedVideoFile.Status = VideoFileStatuses.Updated
      End If
    End If
  End Sub


  Private Sub txtMapComment_TextChanged(sender As Object, e As EventArgs) Handles txtMapComment.TextChanged
    If Not MyIsLoading Then
      MyRecord.Map.Comment = txtMapComment.Text
    End If
  End Sub




  Private Sub btnMapClear_Click(sender As Object, e As EventArgs) Handles btnMapClear.Click
    With MyRecord.Map
      .Available = False
      .Map = ""
      .X = NoValue
      .Y = NoValue
      .Dx = NoValue
      .Dy = NoValue
      .Size = Nothing
      .Scale = NoValue
      .Comment = ""
    End With

    Call LoadMap()
  End Sub

  Private Sub btnMapLoad_Click(sender As Object, e As EventArgs) Handles btnMapLoad.Click
    Dim _OpenFileDialog As OpenFileDialog

    _OpenFileDialog = New OpenFileDialog
    With _OpenFileDialog
      .FileName = ""
      .Filter = "Map parameter files (*.tamap)|*.tamap"
      .Multiselect = False
      .CheckFileExists = True
      .InitialDirectory = MyRecord.ImportDirectory
      .ShowDialog(Me)
      If .FileName.Length > 0 Then
        MyRecord.ImportMap(.FileName)
        Call LoadMap()
      End If
    End With
  End Sub


  Private Sub lblVideoTimeDescripancy_DoubleClick(sender As Object, e As EventArgs) Handles lblVideoTimeDescripancyValue.DoubleClick
    Dim _ClockDescrepancyString As String
    Dim _ClockDescrepancy As Double

    _ClockDescrepancyString = InputBox("Enter new value for the clock discrepancy:", "Clock discrepancy", FormatNumber(MyRecord.VideoFiles(lstVideo.SelectedIndex).ClockDiscrepancy, 3))
    If Double.TryParse(_ClockDescrepancyString, _ClockDescrepancy) Then
      MyRecord.VideoFiles(lstVideo.SelectedIndex).ClockDiscrepancy = _ClockDescrepancy
      lblVideoTimeDescripancyValue.Text = FormatNumber(MySelectedVideoFile.ClockDiscrepancy, 3) & " s."
      If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then MySelectedVideoFile.Status = VideoFileStatuses.Updated
    End If
  End Sub



  'Private Sub lblVideoTimeDescripancyRate_DoubleClick(sender As Object, e As EventArgs)
  '  Dim _ClockDescrepancyRateString As String
  '  Dim _ClockDescrepancyRate As Double

  '  _ClockDescrepancyRateString = InputBox("Enter new value for the clock discrepancy rate:", "Clock discrepancy rate", FormatNumber(MyRecord.VideoFiles(lstVideo.SelectedIndex).ClockDiscrepancyRate, 3))
  '  If Double.TryParse(_ClockDescrepancyRateString, _ClockDescrepancyRate) Then
  '    MyRecord.VideoFiles(lstVideo.SelectedIndex).ClockDiscrepancyRate = _ClockDescrepancyRate
  '    ' lblVideoTimeDescripancyRate.Text = "dT rate: " & FormatNumber(MySelectedVideoFile.ClockDiscrepancyRate, 15) & " s."
  '    If MySelectedVideoFile.Status = VideoFileStatuses.Unchanged Then MySelectedVideoFile.Status = VideoFileStatuses.Updated
  '  End If
  'End Sub


  'Private Sub btnGroundPlane_Click(sender As Object, e As EventArgs) Handles btnGroundPlane.Click
  '  Dim _MessageText As String

  '  With MyRecord.Map
  '    If .Available Then
  '      _MessageText = "X, m         Y, m         Z, m"
  '      For _i = 0 To .GroundPlanePoints.Count - 1
  '        _MessageText = _MessageText & Environment.NewLine & .GroundPlanePoints(_i).X.ToString("0.000") & "    " &
  '                                                            .GroundPlanePoints(_i).Y.ToString("0.000") & "    " &
  '                                                            .GroundPlanePoints(_i).Z.ToString("0.000")
  '        If _i > 10 Then
  '          _MessageText = _MessageText & Environment.NewLine & "..."
  '          Exit For
  '        End If
  '      Next _i
  '    Else
  '      _MessageText = "There is no ground points available."
  '    End If

  '    MsgBox(_MessageText, MsgBoxStyle.OkOnly, "Ground plane points (" & .GroundPlanePoints.Count.ToString & ")")
  '  End With
  'End Sub

End Class