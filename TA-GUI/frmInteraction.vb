﻿Public Class frmInteraction
  Dim MyInteraction As usrInteractionAnalyser


  Public Sub New(ByVal ParentRecord As clsRecord)

    InitializeComponent()

    MyInteraction = New usrInteractionAnalyser
    MyInteraction.Top = 3
    MyInteraction.Left = 3
    MyInteraction.Width = Me.Width - 20
    MyInteraction.Height = Me.Height - 40
    Me.Controls.Add(MyInteraction)

    MyInteraction.RoadUser1 = ParentRecord.RoadUsers(0)
    MyInteraction.RoadUser2 = ParentRecord.RoadUsers(1)

    MyInteraction.CalculateTTC()
    MyInteraction.DisplayResults()
  End Sub
End Class