﻿Imports System.Data.OleDb

Public Class frmTableDesign
  Private MyProject As clsProject

  Private MyUserControls As List(Of clsUserControl)
  Private MyStatuses As List(Of clsValue)
  Private MyTypes As List(Of clsValue)

  Private MyFieldIndex As Integer


  Private MyFieldsAreLoading As Boolean
  Private MyFieldIsLoading As Boolean
  Private MyValueIsLoading As Boolean





  Public Sub New(ByVal Project As clsProject)
    Dim _Row As DataRow
    Dim _Status As clsValue
    Dim _Type As clsValue



    Call InitializeComponent()

    MyProject = Project

    MyFieldsAreLoading = True

    lstFields.Items.Clear()

    Select Case MyProject.LayoutMode
      'Case ProjectLayoutModes.VideoRecordings
      '  MyStatuses = New List(Of clsValue)
      '  For Each _Row In MyProject.StatusValuesVR.Rows
      '    _Status = New clsValue
      '    _Status.ID = GetSQLValueInteger(_Row("cmID"))
      '    _Status.Name = GetSQLValueString(_Row("cmName"))
      '    _Status.Position = GetSQLValueInteger(_Row("cmPosition"))
      '    _Status.Comment = GetSQLValueString(_Row("cmComment"))
      '    _Status.Status = clsValue.RecordStatus.NoChanges
      '    MyStatuses.Add(_Status)
      '  Next

        'MyTypes = New List(Of clsValue)
        'For Each _Row In MyProject.TypeValuesVR.Rows
        '  _Type = New clsValue
        '  _Type.ID = GetSQLValueInteger(_Row("cmID"))
        '  _Type.Name = GetSQLValueString(_Row("cmName"))
        '  _Type.Position = GetSQLValueInteger(_Row("cmPosition"))
        '  _Type.Comment = GetSQLValueString(_Row("cmComment"))
        '  _Type.Status = clsValue.RecordStatus.NoChanges
        '  MyTypes.Add(_Type)
        'Next

        'MyUserControls = Project.GetUserControlsVR
        'For Each _Control In MyUserControls
        '  lstFields.Items.Add(_Control.Name)
        'Next

      Case ProjectLayoutModes.Detections
        MyStatuses = New List(Of clsValue)
        For Each _Row In MyProject.StatusValuesDT.Rows
          _Status = New clsValue
          _Status.ID = GetSQLValueInteger(_Row("cmID"))
          _Status.Name = GetSQLValueString(_Row("cmName"))
          _Status.Position = GetSQLValueInteger(_Row("cmPosition"))
          _Status.Comment = GetSQLValueString(_Row("cmComment"))
          _Status.Status = clsValue.RecordStatus.NoChanges
          MyStatuses.Add(_Status)
        Next

        MyTypes = New List(Of clsValue)
        For Each _Row In MyProject.TypeValuesDT.Rows
          _Type = New clsValue
          _Type.ID = GetSQLValueInteger(_Row("cmID"))
          _Type.Name = GetSQLValueString(_Row("cmName"))
          _Type.Position = GetSQLValueInteger(_Row("cmPosition"))
          _Type.Comment = GetSQLValueString(_Row("cmComment"))
          _Type.Status = clsValue.RecordStatus.NoChanges
          MyTypes.Add(_Type)
        Next

        MyUserControls = Project.GetUserControlsDT
        For Each _Control In MyUserControls
          lstFields.Items.Add(_Control.Name)
        Next
    End Select
    MyFieldsAreLoading = False

    MyFieldIsLoading = True
    cmbType.Items.Clear()
    cmbType.Items.Add("TextBox")
    cmbType.Items.Add("Numberbox")
    cmbType.Items.Add("CheckBox")
    cmbType.Items.Add("ComboBox")
    cmbType.Items.Add("Placeholder")

    cmbDecimals.Items.Clear()
    cmbDecimals.Items.Add("0")
    cmbDecimals.Items.Add("1")
    cmbDecimals.Items.Add("2")

    cmbScale.Items.Clear()
    cmbScale.Items.Add("100%")
    cmbScale.Items.Add("200%")

    MyFieldIndex = -3
    Call LoadField()
    MyFieldIsLoading = False

  End Sub



  Private Sub LoadField()

    Select Case MyFieldIndex
      Case -3
        btnFieldAdd.Enabled = True
        btnFieldRemove.Enabled = False
        btnFieldUp.Enabled = False
        btnFieldDown.Enabled = False
        lblType.Visible = False
        cmbType.Visible = False
        lblLabel.Visible = False
        txtLabel.Visible = False
        lblDecimals.Visible = False
        cmbDecimals.Visible = False
        lblScale.Visible = False
        cmbScale.Visible = False
        lblValues.Visible = False
        pnlValues.Visible = False

      Case -2
        btnFieldAdd.Enabled = True
        btnFieldRemove.Enabled = False
        btnFieldUp.Enabled = False
        btnFieldDown.Enabled = False
        lblType.Visible = True
        cmbType.SelectedIndex = 3
        cmbType.Visible = True
        cmbType.Enabled = False
        lblLabel.Visible = True
        txtLabel.Text = "Status"
        txtLabel.Visible = True
        txtLabel.Enabled = False
        lblDecimals.Visible = False
        cmbDecimals.Visible = False
        lblScale.Visible = False
        cmbScale.Visible = False
        lblValues.Visible = True
        pnlValues.Visible = True
        lstValues.Items.Clear()
        For Each _Status In MyStatuses
          lstValues.Items.Add(_Status.Name)
        Next
        lstValues.SelectedIndex = -1
        Call LoadValue()

      Case -1
        btnFieldAdd.Enabled = True
        btnFieldRemove.Enabled = False
        btnFieldUp.Enabled = False
        btnFieldDown.Enabled = False
        lblType.Visible = True
        cmbType.SelectedIndex = 3
        cmbType.Visible = True
        cmbType.Enabled = False
        lblLabel.Visible = True
        txtLabel.Text = "Type"
        txtLabel.Visible = True
        txtLabel.Enabled = False
        lblDecimals.Visible = False
        cmbDecimals.Visible = False
        lblScale.Visible = False
        cmbScale.Visible = False
        lblValues.Visible = True
        pnlValues.Visible = True
        lstValues.Items.Clear()
        For Each _Type In MyTypes
          lstValues.Items.Add(_Type.Name)
        Next
        lstValues.SelectedIndex = -1
        Call LoadValue()

      Case Else
        lblType.Visible = True
        cmbType.Visible = True
        'cmbType.SelectedIndex = MyUserControls(MyFieldIndex).Type - 1
        'Call LoadTypeUserControl(MyUserControls(MyFieldIndex))
    End Select



  End Sub

  Private Sub LoadTypeUserControl(ByVal UserControl As clsUserControl)
    Select Case UserControl.Type
      Case UserControlTypes.NotDefined
        lblLabel.Visible = False
        txtLabel.Visible = False
        lblDecimals.Visible = False
        cmbDecimals.Visible = False
        lblScale.Visible = False
        cmbScale.Visible = False
        cmbScale.Enabled = False
        lblValues.Visible = False
        pnlValues.Visible = False

      Case UserControlTypes.Textbox
        txtLabel.Text = UserControl.Label
        lblLabel.Visible = True
        txtLabel.Visible = True
        txtLabel.Enabled = True
        cmbDecimals.SelectedIndex = 0
        lblDecimals.Visible = True
        cmbDecimals.Visible = True
        cmbDecimals.Enabled = False
        If UserControl.Scale = 2 Then cmbScale.SelectedIndex = 1 Else cmbScale.SelectedIndex = 0
        lblScale.Visible = True
        cmbScale.Visible = True
        cmbScale.Enabled = True
        lblValues.Visible = False
        pnlValues.Visible = False

      Case UserControlTypes.Numberbox
        txtLabel.Text = UserControl.Label
        lblLabel.Visible = True
        txtLabel.Visible = True
        txtLabel.Enabled = True
        cmbDecimals.SelectedIndex = UserControl.Decimals
        lblDecimals.Visible = True
        cmbDecimals.Visible = True
        cmbDecimals.Enabled = True
        cmbScale.SelectedIndex = 0
        lblScale.Visible = True
        cmbScale.Visible = True
        cmbScale.Enabled = False
        lblValues.Visible = False
        pnlValues.Visible = False

      Case UserControlTypes.Checkbox
        txtLabel.Text = UserControl.Label
        lblLabel.Visible = True
        txtLabel.Visible = True
        txtLabel.Enabled = True
        cmbDecimals.SelectedIndex = 0
        lblDecimals.Visible = True
        cmbDecimals.Visible = True
        cmbDecimals.Enabled = False
        cmbScale.SelectedIndex = 0
        lblScale.Visible = True
        cmbScale.Visible = True
        cmbScale.Enabled = False
        lblValues.Visible = False
        pnlValues.Visible = False

      Case UserControlTypes.Combobox
        txtLabel.Text = UserControl.Label
        lblLabel.Visible = True
        txtLabel.Visible = True
        txtLabel.Enabled = True
        cmbDecimals.SelectedIndex = 0
        lblDecimals.Visible = True
        cmbDecimals.Visible = True
        cmbDecimals.Enabled = False
        If UserControl.Scale = 2 Then cmbScale.SelectedIndex = 1 Else cmbScale.SelectedIndex = 0
        lblScale.Visible = True
        cmbScale.Visible = True
        cmbScale.Enabled = True
        lblValues.Visible = True
        pnlValues.Visible = True

      Case UserControlTypes.Placeholder
        txtLabel.Text = ""
        lblLabel.Visible = True
        txtLabel.Visible = True
        txtLabel.Enabled = False
        cmbDecimals.SelectedIndex = 0
        lblDecimals.Visible = True
        cmbDecimals.Visible = True
        cmbDecimals.Enabled = False
        cmbScale.SelectedIndex = 0
        lblScale.Visible = True
        cmbScale.Visible = True
        cmbScale.Enabled = False
        lblValues.Visible = False
        pnlValues.Visible = False
    End Select
  End Sub

  Private Sub LoadValue()



    If lstValues.SelectedIndex > -1 Then
      Select Case MyFieldIndex
        Case -2
          txtValueComment.Text = MyStatuses(lstValues.SelectedIndex).Comment
        Case -1
          txtValueComment.Text = MyTypes(lstValues.SelectedIndex).Comment
        Case Else

      End Select
      txtValueComment.Visible = True
      btnValueAdd.Enabled = True
      btnValueRemove.Enabled = True
      btnValueUp.Enabled = True
      btnValueDown.Enabled = True
    Else
      txtValueComment.Visible = False
      btnValueAdd.Enabled = True
      btnValueRemove.Enabled = False
      btnValueUp.Enabled = False
      btnValueDown.Enabled = False
    End If

  End Sub
  Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

    Call SaveChanges()




    Me.Hide()
  End Sub


  Private Sub SaveChanges()
    Dim _OleDBConnection As OleDbConnection
    Dim _OleDbCommand As OleDbCommand
    Dim _i As Integer

    _OleDBConnection = MyProject.DatabaseConnection


    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        _i = 0
        For Each _Status In MyStatuses
          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = _OleDBConnection
          _OleDbCommand.CommandText = "UPDATE VideoRecordingStatus SET cmName = @Name, " &
                                                                  "cmPosition = @Position, " &
                                                                   "cmComment = @Comment " &
                                                                  "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Name", _Status.Name)
          _OleDbCommand.Parameters.AddWithValue("@Position", _i)
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Status.Comment)
          _OleDbCommand.Parameters.AddWithValue("@ID", _Status.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _i = _i + 1
        Next

        _i = 0
        For Each _Type In MyTypes
          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = _OleDBConnection
          _OleDbCommand.CommandText = "UPDATE VideoRecordingType SET cmName = @Name, " &
                                                                "cmPosition = @Position, " &
                                                                 "cmComment = @Comment " &
                                                                "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Name", _Type.Name)
          _OleDbCommand.Parameters.AddWithValue("@Position", _i)
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Type.Comment)
          _OleDbCommand.Parameters.AddWithValue("@ID", _Type.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _i = _i + 1
        Next



      Case ProjectLayoutModes.Detections
        _i = 0
        For Each _Status In MyStatuses
          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = _OleDBConnection
          _OleDbCommand.CommandText = "UPDATE DetectionStatus SET cmName = @Name, " &
                                                             "cmPosition = @Position, " &
                                                              "cmComment = @Comment " &
                                                             "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Name", _Status.Name)
          _OleDbCommand.Parameters.AddWithValue("@Position", _i)
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Status.Comment)
          _OleDbCommand.Parameters.AddWithValue("@ID", _Status.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _i = _i + 1
        Next

        _i = 0
        For Each _Type In MyTypes
          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = _OleDBConnection
          _OleDbCommand.CommandText = "UPDATE DetectionType SET cmName = @Name, " &
                                                           "cmPosition = @Position, " &
                                                            "cmComment = @Comment " &
                                                           "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Name", _Type.Name)
          _OleDbCommand.Parameters.AddWithValue("@Position", _i)
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Type.Comment)
          _OleDbCommand.Parameters.AddWithValue("@ID", _Type.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _i = _i + 1
        Next


    End Select
  End Sub




  Private Sub lstFields_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstFields.SelectedIndexChanged
    MyFieldIsLoading = True
    MyFieldIndex = lstFields.SelectedIndex - 2
    Call LoadField()
    MyFieldIsLoading = False
  End Sub

  Private Sub btnLockType_Click(sender As Object, e As EventArgs)

    btnLockType.Visible = False
  End Sub

  Private Sub lstValues_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstValues.SelectedIndexChanged
    MyValueIsLoading = True
    Call LoadValue()
    MyValueIsLoading = False
  End Sub





  Private Sub txtValueComment_TextChanged(sender As Object, e As EventArgs) Handles txtValueComment.TextChanged

    If Not (MyFieldsAreLoading) And (Not MyFieldIsLoading) And (Not MyValueIsLoading) Then
      Select Case MyFieldIndex
        Case -3

        Case -2
          MyStatuses(lstValues.SelectedIndex).Comment = txtValueComment.Text
        Case -1
          MyTypes(lstValues.SelectedIndex).Comment = txtValueComment.Text
        Case Else

      End Select
    End If
  End Sub

  Private Sub btnValueUp_Click(sender As Object, e As EventArgs) Handles btnValueUp.Click
    Dim _Index As Integer

    _Index = lstValues.SelectedIndex
    If _Index > 0 Then
      MyValueIsLoading = True
      Select Case MyFieldIndex
        Case -3

        Case -2
          MyStatuses.Insert(_Index - 1, MyStatuses(_Index))
          MyStatuses.RemoveAt(_Index + 1)
          lstValues.Items.Insert(_Index - 1, lstValues.Items(_Index))
          lstValues.Items.RemoveAt(_Index + 1)
          lstValues.SelectedIndex = _Index - 1
        Case -1
          MyTypes.Insert(_Index - 1, MyTypes(_Index))
          MyTypes.RemoveAt(_Index + 1)
          lstValues.Items.Insert(_Index - 1, lstValues.Items(_Index))
          lstValues.Items.RemoveAt(_Index + 1)
          lstValues.SelectedIndex = _Index - 1
        Case Else

      End Select
      Call LoadValue()
      MyValueIsLoading = False
    End If

  End Sub



  Private Sub btnValueDown_Click(sender As Object, e As EventArgs) Handles btnValueDown.Click
    Dim _Index As Integer

    _Index = lstValues.SelectedIndex
    If _Index < lstValues.Items.Count - 1 Then
      MyValueIsLoading = True
      Select Case MyFieldIndex
        Case -3

        Case -2
          MyStatuses.Insert(_Index + 2, MyStatuses(_Index))
          MyStatuses.RemoveAt(_Index)
          lstValues.Items.Insert(_Index + 2, lstValues.Items(_Index))
          lstValues.Items.RemoveAt(_Index)
          lstValues.SelectedIndex = _Index + 1
        Case -1
          MyTypes.Insert(_Index + 2, MyTypes(_Index))
          MyTypes.RemoveAt(_Index)
          lstValues.Items.Insert(_Index + 2, lstValues.Items(_Index))
          lstValues.Items.RemoveAt(_Index)
          lstValues.SelectedIndex = _Index + 1
        Case Else

      End Select
      Call LoadValue()
      MyValueIsLoading = False
    End If

  End Sub

  Private Sub frmTableDesign_Load(sender As Object, e As EventArgs) Handles MyBase.Load

  End Sub
End Class