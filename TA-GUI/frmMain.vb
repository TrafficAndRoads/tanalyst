﻿Public Class frmMain
  Private MyEdition As SoftwareEditions = SoftwareEditions.FullMode
 
  Private MyProject As clsProject
  Private WithEvents MyDisplay As usrDisplay
  Private MyUserControls As List(Of clsUserControl)

  'flags
  Private MyIsLoadingProject As Boolean
  Private MyIsLoadingRecord As Boolean
  Private MyIsLoadingRoadUser As Boolean
  Private MyMainDataShown As Boolean
  'Private MySelectedRoadUserChangedByUser As Boolean
  Private MyMediaForm As frmMedia
  'Private WithEvents MyZoomedViewForm As frmZoom

  Public MediaFormShown As Boolean = False
    ' Public ZommedViewShown As Boolean = False

#Region "Local Subs"

    Private Sub LoadProject(Optional ByVal ProjectFile As String = "")
    Dim _ProjectFile As String

    MyIsLoadingProject = True

    _ProjectFile = ProjectFile

    If _ProjectFile.Length = 0 Then
      'Open dialog box to select a project file
      With digOpenFile
        .Title = "Open project file"
        .FileName = ""
        .CheckFileExists = True
        .Multiselect = False
        digOpenFile.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\My T-projects\"
        .Filter = "T-Analyst projects (*.taprj)|*.taprj"
        .ShowDialog(Me)
        _ProjectFile = .FileName
      End With
    End If

    'if a selected project file valid
    If _ProjectFile.Length > 0 Then
      'unload old project (if any) and load the new one
      If MyProject IsNot Nothing Then Call UnloadProject()

      'create new project
      MyProject = New clsProject()
      If MyProject.Load(_ProjectFile) Then
        Me.Text = MyProject.ProjectName
        cmbRUType.Items.Clear()
        For Each _RoadUserType In MyProject.RoadUserTypes
          cmbRUType.Items.Add(_RoadUserType.Name)
        Next
        Call LoadLayoutMode(MyProject.LayoutMode)
        Call LoadRecord(0)
        Call DisplayRecordData()
        Call ShowControlsOnLoad()
        mnuDisplayRoadUsers.Checked = MyProject.VisualiserOptions.DrawRoadUsers
        mnuDisplayTrajectories.Checked = MyProject.VisualiserOptions.DrawTrajectories
        mnuDisplaySpeedVectors.Checked = MyProject.VisualiserOptions.DrawSpeeds
        chkRUKey1.ForeColor = MyProject.VisualiserOptions.LineColourSpeedRoadUserKey1
        chkRUKey2.ForeColor = MyProject.VisualiserOptions.LineColourSpeedRoadUserKey2
      End If
    Else
      '   MyProject = Nothing
    End If

    MyMainDataShown = True

    MyIsLoadingProject = False
  End Sub

  Private Sub UnloadProject()
    If MyProject IsNot Nothing Then
      Call UpdateAndCloseCurrentRecord()
      MyProject.Close()
      MyDisplay.Visible = False
      MyProject = Nothing
      Call HideControlsOnUnload()
    End If


    MyMainDataShown = True
    '  MyZoomedViewForm.ZoomedBitmap = Nothing
    ' MyZoomedViewForm.Hide()
    MyDisplay.ZoomedViewVisible = False
    mnuDisplayZoomView.Checked = False

    Call AdjustFormSize()
  End Sub

  Private Sub LoadUserControlValues()
    Dim _UserControlValues As New List(Of Object)
    Dim _UserControlValue As Object
    Dim _UserControl As clsUserControl
    Dim _TextBox As New TextBox
    Dim _CheckBox As New CheckBox
    Dim _ComboBox As New ComboBox
    Dim _ClickCounter As New usrClickCounter
        Dim _i As Integer



        Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        _UserControlValues = MyProject.CurrentRecordVR.UserControlValues
      Case ProjectLayoutModes.Detections
        _UserControlValues = MyProject.CurrentRecordDT.UserControlValues
    End Select

    For _i = 0 To MyUserControls.Count - 1
      _UserControl = MyUserControls(_i)
      _UserControlValue = _UserControlValues(_i)
      Select Case _UserControl.Type
        Case UserControlTypes.Textbox
          _TextBox = CType(grpUserControls.Controls("utxt" & _UserControl.Name), TextBox)
          If _UserControlValue.Equals(DBNull.Value) Then
            _TextBox.Text = ""
          Else
            _TextBox.Text = CStr(_UserControlValue)
          End If

        Case UserControlTypes.Numberbox
          _TextBox = CType(grpUserControls.Controls("utxt" & _UserControl.Name), TextBox)
          If _UserControlValue.Equals(DBNull.Value) Then
            _UserControlValue = ""
          Else
            _UserControlValue = FormatNumber(_UserControlValue, _UserControl.Decimals)
          End If
          _TextBox.Text = CStr(_UserControlValue)

        Case UserControlTypes.Checkbox
          _CheckBox = CType(grpUserControls.Controls("uchk" & _UserControl.Name), CheckBox)
          If _UserControlValue.Equals(DBNull.Value) Then
            _UserControlValue = False
          Else
            _CheckBox.Checked = CBool(_UserControlValue)
          End If

        Case UserControlTypes.Combobox
          _ComboBox = CType(grpUserControls.Controls("ucmb" & _UserControl.Name), ComboBox)
          If _UserControlValue.Equals(DBNull.Value) Then
            _ComboBox.SelectedIndex = -1
          Else
            _ComboBox.SelectedValue = _UserControlValue
          End If

        Case UserControlTypes.ClickCounter
          _ClickCounter = CType(grpUserControls.Controls("uclc" & _UserControl.Name), usrClickCounter)
          _ClickCounter.Value = CInt(_UserControlValue)

      End Select
    Next _i
  End Sub

  Private Sub DisplayRecordData(Optional ByVal StartTime As Double = NoValue)
    Dim _RecordExists As Boolean

    MyIsLoadingRecord = True

    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        If MyProject.CurrentRecordVR IsNot Nothing Then
          _RecordExists = True
          lblID.Text = "ID   " & Format(MyProject.CurrentRecordVR.ID, "####0")
          dtpTime.Value = MyProject.CurrentRecordVR.DateTime
          cmbStatus.SelectedValue = MyProject.CurrentRecordVR.Status
          cmbType.SelectedValue = MyProject.CurrentRecordVR.Type
          txtComment.Text = MyProject.CurrentRecordVR.Comment

          If MyProject.NumberOfFilteredRecordsVR = MyProject.NumberOfRecordsVR Then
            lblCurrentRecord.Text = (MyProject.CurrentRecordIndexVR + 1).ToString & " of " & MyProject.NumberOfRecordsVR
          Else
            lblCurrentRecord.Text = (MyProject.CurrentRecordIndexVR + 1).ToString & " of " & MyProject.NumberOfFilteredRecordsVR & " (" & MyProject.NumberOfRecordsVR & ")"
          End If

          If MyProject.RelatedRecordsVR.Count > 0 Then btnRelatedRecords.Enabled = True Else btnRelatedRecords.Enabled = False
          Call LoadDisplay(MyProject.CurrentRecordVR, StartTime)
        Else
          _RecordExists = False
        End If


      Case ProjectLayoutModes.Detections
        If MyProject.CurrentRecordDT IsNot Nothing Then
          _RecordExists = True
          lblID.Text = "ID   " & Format(MyProject.CurrentRecordDT.ID, "####0")
          dtpTime.Value = MyProject.CurrentRecordDT.DateTime
          cmbStatus.SelectedValue = MyProject.CurrentRecordDT.Status
          cmbType.SelectedValue = MyProject.CurrentRecordDT.Type
          txtComment.Text = MyProject.CurrentRecordDT.Comment

          If MyProject.NumberOfFilteredRecordsDT = MyProject.NumberOfRecordsDT Then
            lblCurrentRecord.Text = (MyProject.CurrentRecordIndexDT + 1).ToString & " of " & MyProject.NumberOfRecordsDT
          Else
            lblCurrentRecord.Text = (MyProject.CurrentRecordIndexDT + 1).ToString & " of " & MyProject.NumberOfFilteredRecordsDT & " (" & MyProject.NumberOfRecordsDT & ")"
          End If

          If MyProject.RelatedRecordsDT.Count > 0 Then btnRelatedRecords.Enabled = True Else btnRelatedRecords.Enabled = False
          Call LoadDisplay(MyProject.CurrentRecordDT, StartTime)
        Else
          _RecordExists = False
        End If

    End Select

    If _RecordExists Then
      grpMainData.Enabled = True
      grpRoadUsers.Enabled = True
      grpUserControls.Enabled = True
      grpMainData.Visible = True
      grpRoadUsers.Visible = True
      grpUserControls.Visible = True

      dtpTime.Enabled = True
      cmbStatus.Enabled = True
      cmbType.Enabled = True
      txtComment.Enabled = True
      btnMap.Enabled = True
      btnMedia.Enabled = True
      lblCurrentRecord.Enabled = True
      btnFirst.Enabled = True
      btnJumpBack.Enabled = True
      btnPrevious.Enabled = True
      btnNext.Enabled = True
      btnJumpForward.Enabled = True
      btnLast.Enabled = True
      btnDelete.Enabled = True
      mnuDataFilter.Enabled = True
      mnuRecord.Enabled = True
      mnuDisplay.Enabled = True

      If MyMediaForm IsNot Nothing Then MyMediaForm.MediaPath = MyProject.CurrentRecord.MediaPath

      Call LoadRoadUsers()
      Call LoadUserControlValues()

    Else  'Load dummy
      lblID.Text = "ID   ######"
      dtpTime.Enabled = False
      cmbStatus.Enabled = False
      cmbType.Enabled = False
      txtComment.Enabled = False
      btnMap.Enabled = False
      btnRelatedRecords.Enabled = False
      btnMedia.Enabled = False
      btnFirst.Enabled = False
      btnJumpBack.Enabled = False
      btnPrevious.Enabled = False
      btnNext.Enabled = False
      btnJumpForward.Enabled = False
      btnLast.Enabled = False
      btnDelete.Enabled = False
      mnuDataFilter.Enabled = True
      mnuRecord.Enabled = False
      mnuDisplay.Enabled = False

      If MyMediaForm IsNot Nothing Then
        MyMediaForm.MediaPath = ""
        MyMediaForm.Hide()
        Me.MediaFormShown = False
      End If


      grpRoadUsers.Visible = False
      grpUserControls.Visible = False

      Select Case MyProject.LayoutMode
        Case ProjectLayoutModes.VideoRecordings
          If MyProject.NumberOfRecordsVR > 0 Then
            lblCurrentRecord.Text = "0 of 0 (" & MyProject.NumberOfRecordsVR & ")"
          Else
            lblCurrentRecord.Text = "no records"
          End If
        Case ProjectLayoutModes.Detections
          If MyProject.NumberOfRecordsDT > 0 Then
            lblCurrentRecord.Text = "0 of 0 (" & MyProject.NumberOfRecordsDT & ")"
          Else
            lblCurrentRecord.Text = "no records"
          End If
      End Select
      lblCurrentRecord.Enabled = False

      MyDisplay.Visible = False


      MsgBox("No records to display", MsgBoxStyle.Information)
    End If

    MyIsLoadingRecord = False

    Call UpdateDisplayMenu()
    Call AdjustFormSize()
  End Sub


  Private Sub UpdateDisplayMenu()
    MyIsLoadingRecord = True

    If MyProject.CurrentRecord IsNot Nothing Then
      With MyProject.CurrentRecord
        If .Map.Map.Length > 0 Then
          mnuDisplayMap.Visible = True
          If .Map.Visible Then
            mnuDisplayMap.Checked = True
          Else
            mnuDisplayMap.Checked = False
          End If
        Else
          mnuDisplayMap.Visible = False
        End If

        For _i = 1 To 12
          If _i <= .VideoFiles.Count Then
            Select Case _i
              Case 1
                mnuDisplayCamera1.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera1.Checked = True
                Else
                  mnuDisplayCamera1.Checked = False
                End If

              Case 2
                mnuDisplayCamera2.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera2.Checked = True
                Else
                  mnuDisplayCamera2.Checked = False
                End If

              Case 3
                mnuDisplayCamera3.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera3.Checked = True
                Else
                  mnuDisplayCamera3.Checked = False
                End If

              Case 4
                mnuDisplayCamera4.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera4.Checked = True
                Else
                  mnuDisplayCamera4.Checked = False
                End If

              Case 5
                mnuDisplayCamera5.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera5.Checked = True
                Else
                  mnuDisplayCamera5.Checked = False
                End If

              Case 6
                mnuDisplayCamera6.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera6.Checked = True
                Else
                  mnuDisplayCamera6.Checked = False
                End If

              Case 7
                mnuDisplayCamera7.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera7.Checked = True
                Else
                  mnuDisplayCamera7.Checked = False
                End If

              Case 8
                mnuDisplayCamera8.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera8.Checked = True
                Else
                  mnuDisplayCamera8.Checked = False
                End If

              Case 9
                mnuDisplayCamera9.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera9.Checked = True
                Else
                  mnuDisplayCamera9.Checked = False
                End If

              Case 10
                mnuDisplayCamera10.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera10.Checked = True
                Else
                  mnuDisplayCamera10.Checked = False
                End If

              Case 11
                mnuDisplayCamera11.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera11.Checked = True
                Else
                  mnuDisplayCamera11.Checked = False
                End If

              Case 12
                mnuDisplayCamera12.Visible = True
                If .VideoFiles(_i - 1).Visible Then
                  mnuDisplayCamera12.Checked = True
                Else
                  mnuDisplayCamera12.Checked = False
                End If
            End Select
          Else
            Select Case _i
              Case 1
                mnuDisplayCamera1.Visible = False
              Case 2
                mnuDisplayCamera2.Visible = False
              Case 3
                mnuDisplayCamera3.Visible = False
              Case 4
                mnuDisplayCamera4.Visible = False
              Case 5
                mnuDisplayCamera5.Visible = False
              Case 6
                mnuDisplayCamera6.Visible = False
              Case 7
                mnuDisplayCamera7.Visible = False
              Case 8
                mnuDisplayCamera8.Visible = False
              Case 9
                mnuDisplayCamera9.Visible = False
              Case 10
                mnuDisplayCamera10.Visible = False
              Case 11
                mnuDisplayCamera11.Visible = False
              Case 12
                mnuDisplayCamera12.Visible = False
            End Select
          End If
        Next
      End With

      mnuDisplay.Visible = True
    Else
      mnuDisplay.Visible = False
    End If

    MyIsLoadingRecord = False
  End Sub
  Private Sub LoadRoadUsers()

    grpRoadUsers.Visible = False
    lstRoadUsers.Items.Clear()

    If MyProject.CurrentRecord.RoadUsers.Count > 0 Then
      For i = 1 To MyProject.CurrentRecord.RoadUsers.Count
        lstRoadUsers.Items.Add("Trajectory " & i.ToString)
      Next i
      lstRoadUsers.Enabled = True
    Else
      lstRoadUsers.Enabled = False
    End If

    'If MyProject.LayoutMode = ProjectLayoutModes.VideoRecordings Then
    '  chkRUKey1.Visible = False
    '  chkRUKey2.Visible = False
    'Else
    '  chkRUKey1.Visible = True
    '  chkRUKey2.Visible = True
    'End If

    grpRoadUsers.Visible = True

    Call DisplayRoadUserData(-1)
  End Sub

  Private Sub LoadDisplay(ByVal CurrentRecord As clsRecord, Optional ByVal StartAbsoluteTime As Double = NoValue)
    Dim _StartFrame As Integer

    MyDisplay.Visible = False

    If IsValue(StartAbsoluteTime) Then
      _StartFrame = CInt((StartAbsoluteTime - CurrentRecord.StartTime) * CurrentRecord.FrameRate)
      If Not IsBetween(_StartFrame, 0, CurrentRecord.FrameCount - 1) Then
        _StartFrame = 0
      End If
    Else
      _StartFrame = 0
    End If
    MyDisplay.LoadDisplay(CurrentRecord, MyProject.VisualiserOptions, _StartFrame)
    MyDisplay.DisplayMode = DisplayModes.View
    MyDisplay.Visible = True
  End Sub

  Private Sub DisplayRoadUserData(ByVal RoadUserIndex As Integer)
    Dim _RoadUser As clsRoadUser


    MyIsLoadingRoadUser = True

    If RoadUserIndex > -1 Then
      _RoadUser = MyProject.CurrentRecord.RoadUsers(RoadUserIndex)
      If MyDisplay.DisplayMode = DisplayModes.Edit Then
        btnDeleteRU.Enabled = True
        cmbRUType.Enabled = True
        txtRUlength.Enabled = True
        txtRUwidth.Enabled = True
        txtRUheight.Enabled = True
        txtRUweight.Enabled = True
        chkRUKey1.Enabled = False
        chkRUKey2.Enabled = False
      Else
        btnDeleteRU.Enabled = False
        cmbRUType.Enabled = False
        txtRUlength.Enabled = False
        txtRUwidth.Enabled = False
        txtRUheight.Enabled = False
        txtRUweight.Enabled = False
        chkRUKey1.Enabled = True
        chkRUKey2.Enabled = True
      End If

      cmbRUType.SelectedItem = _RoadUser.Type.Name
      txtRUlength.Text = Format(_RoadUser.Length, "0.00")
      txtRUwidth.Text = Format(_RoadUser.Width, "0.00")
      txtRUheight.Text = Format(_RoadUser.Height, "0.00")
      txtRUweight.Text = Format(_RoadUser.Weight, "0")
      Select Case _RoadUser.Key
        Case clsRoadUser.KeyRoadUser.Key1
          chkRUKey1.Checked = True
          chkRUKey2.Checked = False
        Case clsRoadUser.KeyRoadUser.Key2
          chkRUKey1.Checked = False
          chkRUKey2.Checked = True
        Case Else
          chkRUKey1.Checked = False
          chkRUKey2.Checked = False
      End Select

    Else
      cmbRUType.SelectedItem = Nothing
      cmbRUType.Enabled = False
      txtRUlength.Text = ""
      txtRUlength.Enabled = False
      txtRUwidth.Text = ""
      txtRUwidth.Enabled = False
      txtRUheight.Text = ""
      txtRUheight.Enabled = False
      txtRUweight.Text = ""
      txtRUweight.Enabled = False
      chkRUKey1.Checked = False
      chkRUKey1.Enabled = False
      chkRUKey2.Checked = False
      chkRUKey2.Enabled = False

      btnDeleteRU.Enabled = False
    End If

    MyIsLoadingRoadUser = False

  End Sub

  Private Sub ShowControlsOnLoad()



    mnuProjectNew.Visible = True
    mnuProjectLoad.Visible = True
    mnuProjectClose.Visible = True
    mnuProjectSeparator1.Visible = True
    mnuProjectImportData.Visible = True
    mnuProjectExport.Visible = True
    mnuProjectSeparator2.Visible = True
    mnuProjectProperties.Visible = True
    mnuProjectSeparator3.Visible = True
    mnuProjectExit.Visible = True

    mnuData.Visible = True
    mnuRecord.Visible = True
    mnuDisplay.Visible = True
    mnuHelp.Visible = True

    grpMainData.Visible = True

    btnHide.Visible = True
    btnEditRU.Visible = True


    tlbNew.Visible = True

    'Select Case MyEdition
    '  Case SoftwareEditions.FullMode
    '    btnEditRU.Visible = True
    '    mnuRecord.Visible = True
    '    mnuDisplay.Visible = True
    '    tlbNew.Visible = True
    '    mnuProjectNew.Visible = True
    '    mnuProjectSeparator1.Visible = True
    '    mnuProjectImportData.Visible = True

    '  Case SoftwareEditions.Viewer
    '    btnEditRU.Visible = False
    '    mnuRecord.Visible = False
    '    mnuDisplay.Visible = False
    '    tlbNew.Visible = False
    '    mnuProjectNew.Visible = False
    '    mnuProjectSeparator1.Visible = False
    '    mnuProjectImportData.Visible = False
    'End Select


  End Sub

  Private Sub HideControlsOnUnload()
    Select Case MyEdition
      Case SoftwareEditions.FullMode
        mnuProjectNew.Visible = True
        tlbNew.Visible = True
      Case SoftwareEditions.Viewer
        mnuProjectNew.Visible = False
        tlbNew.Visible = False
    End Select


    mnuProjectLoad.Visible = True
    mnuProjectClose.Visible = False
    mnuProjectSeparator1.Visible = False
    mnuProjectImportData.Visible = False
    mnuProjectExport.Visible = False
    mnuProjectSeparator2.Visible = False
    mnuProjectProperties.Visible = False
    mnuProjectSeparator3.Visible = True
    mnuProjectExit.Visible = True

    mnuData.Visible = False
    mnuRecord.Visible = False
    mnuDisplay.Visible = False
    mnuHelp.Visible = True

    grpMainData.Visible = False
    grpRoadUsers.Visible = False
    grpUserControls.Visible = False

    MyDisplay.Visible = False

    btnHide.Visible = False

    Me.Text = "T-Analyst"
    stsWorldCoordinates.Text = ""
    stsImageCoordinates.Text = ""
    stsOnePixelError.Text = ""

    '  If MyMediaForm IsNot Nothing Then MyMediaForm.Dispose()
  End Sub


  Private Sub LoadRecord(RecordIndex As Integer)
    Me.Cursor = Cursors.AppStarting
    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        MyProject.LoadRecordVR(RecordIndex)
      Case ProjectLayoutModes.Detections
        MyProject.LoadRecordDT(RecordIndex)
    End Select
    Me.Cursor = Cursors.Default
  End Sub

  Private Sub LoadLayoutMode(ByVal LayoutMode As ProjectLayoutModes, Optional ByVal Filter As String = "")

    'Update the DataMode value in the database
    MyProject.LayoutMode = LayoutMode

    'Update menu, status bar, user controls and combos values
    Select Case LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        mnuProjectImportData.Visible = True
        mnuProjectImportExportedEvent.Visible = False

        mnuProjectSeparator2.Visible = True

        mnuDataVideoRecordings.Checked = True
        mnuDataDetections.Checked = False

        mnuRecordSeparator3.Visible = False
        mnuRecordCreateDuplicate.Visible = False


        grpMainData.Text = "Video recordings"
        btnRelatedRecords.ImageKey = "RelatedDetections"
        tltToolTip.SetToolTip(btnRelatedRecords, "Selected all the detections done from this video recording")

        cmbStatus.DataSource = MyProject.StatusValuesVR
        cmbStatus.DisplayMember = "cmName"
        cmbStatus.ValueMember = "cmID"

        cmbType.DataSource = MyProject.TypeValuesVR
        cmbType.DisplayMember = "cmName"
        cmbType.ValueMember = "cmID"

        chkRUKey1.Visible = False
        chkRUKey2.Visible = False

        MyUserControls = MyProject.GetUserControlsVR

      Case ProjectLayoutModes.Detections
        mnuProjectImportData.Visible = True
        mnuProjectSeparator2.Visible = True
        mnuProjectImportExportedEvent.Visible = True

        mnuDataVideoRecordings.Checked = False
        mnuDataDetections.Checked = True

        mnuRecordSeparator3.Visible = True
        mnuRecordCreateDuplicate.Visible = True

        grpMainData.Text = "Detections"
        btnRelatedRecords.ImageKey = "ParentVideoRecording"
        tltToolTip.SetToolTip(btnRelatedRecords, "Go to the parent video recording and find the moment of the current detection")

        cmbStatus.DataSource = MyProject.StatusValuesDT
        cmbStatus.DisplayMember = "cmName"
        cmbStatus.ValueMember = "cmID"

        cmbType.DataSource = MyProject.TypeValuesDT
        cmbType.DisplayMember = "cmName"
        cmbType.ValueMember = "cmID"

        chkRUKey1.Visible = True
        chkRUKey2.Visible = True
        MyUserControls = MyProject.GetUserControlsDT
    End Select

    Call LoadUserControls()

  End Sub

  Private Sub LoadUserControls()
    Const _ControlColumnCount As Integer = 2
    Const _ControlHeight As Integer = 18

    Dim _UserControl As clsUserControl
    Dim _NewTextBox As TextBox
    Dim _NewCheckBox As CheckBox
    Dim _NewCombo As ComboBox
    Dim _NewLabel As Label
    Dim _NewClickCounter As usrClickCounter
    Dim _i As Integer
    Dim _PositionX As Integer
    Dim _PositionY As Integer
    Dim _ControlWidth As Integer

    grpUserControls.Controls.Clear()
    If MyUserControls.Count > 0 Then

      grpUserControls.Height = (_ControlHeight + 10) * ((MyUserControls.Count - 1) \ _ControlColumnCount + 1) + 20
      _i = 0
      _ControlWidth = grpUserControls.Width \ (2 * _ControlColumnCount)

      For Each _UserControl In MyUserControls
        _i = _i + 1
        _PositionX = (((_i - 1) Mod _ControlColumnCount) * 2) * _ControlWidth
        _PositionY = 20 + ((_i - 1) \ _ControlColumnCount) * (_ControlHeight + 10)

        If _UserControl.Type <> UserControlTypes.Placeholder Then
          _NewLabel = New Label
          _NewLabel.Text = _UserControl.Label
          _NewLabel.TextAlign = Drawing.ContentAlignment.TopRight
          _NewLabel.Width = _ControlWidth - 5
          _NewLabel.Height = _ControlHeight
          _NewLabel.Left = _PositionX
          _NewLabel.Top = _PositionY + 1
          grpUserControls.Controls.Add(_NewLabel)
        End If

        _PositionX = (((_i - 1) Mod _ControlColumnCount) * 2 + 1) * _ControlWidth
        Select Case _UserControl.Type
          Case UserControlTypes.Textbox, UserControlTypes.Numberbox
            _NewTextBox = New TextBox
            _NewTextBox.Name = "utxt" & _UserControl.Name
            _NewTextBox.Width = _UserControl.Scale * _ControlWidth - 5
            _NewTextBox.Height = _ControlHeight
            _NewTextBox.Left = _PositionX
            _NewTextBox.Top = _PositionY
            grpUserControls.Controls.Add(_NewTextBox)

          Case UserControlTypes.Checkbox
            _NewCheckBox = New CheckBox
            _NewCheckBox.Name = "uchk" & _UserControl.Name
            _NewCheckBox.Width = _UserControl.Scale * _ControlWidth - 5
            _NewCheckBox.Height = _ControlHeight
            _NewCheckBox.Left = _PositionX
            _NewCheckBox.Top = _PositionY
            grpUserControls.Controls.Add(_NewCheckBox)

          Case UserControlTypes.Combobox
            _NewCombo = New ComboBox
            _NewCombo.FormattingEnabled = True
            _NewCombo.Name = "ucmb" & _UserControl.Name
            _NewCombo.Width = _UserControl.Scale * _ControlWidth - 5
            _NewCombo.Height = _ControlHeight
            _NewCombo.Left = _PositionX
            _NewCombo.Top = _PositionY
            _NewCombo.DropDownStyle = ComboBoxStyle.DropDownList
            _NewCombo.DataSource = _UserControl.ValueTable
            _NewCombo.DisplayMember = "cmName"
            _NewCombo.ValueMember = "cmID"
            grpUserControls.Controls.Add(_NewCombo)

          Case UserControlTypes.ClickCounter
            _NewClickCounter = New usrClickCounter
            _NewClickCounter.Name = "uclc" & _UserControl.Name
            _NewClickCounter.Width = _UserControl.Scale * _ControlWidth - 5
            _NewClickCounter.Height = _ControlHeight
            _NewClickCounter.Left = _PositionX
            _NewClickCounter.Top = _PositionY
            grpUserControls.Controls.Add(_NewClickCounter)

          Case UserControlTypes.Placeholder
            'do nothing

        End Select
      Next
      grpUserControls.Visible = True
    Else
      grpUserControls.Visible = False
    End If

  End Sub

  Private Sub UpdateAndCloseCurrentRecord()
    Dim _Record As clsRecord
    Dim _UserControl As clsUserControl
    Dim _CheckBox As New CheckBox
    Dim _ComboBox As New ComboBox
    Dim _Value As Double
    Dim _UserControlValues As List(Of Object)



    Me.Cursor = Cursors.AppStarting

    _Record = MyProject.CurrentRecord

    If _Record IsNot Nothing Then
      _Record.DateTime = dtpTime.Value
      _Record.Status = CInt(cmbStatus.SelectedValue)
      _Record.Type = CInt(cmbType.SelectedValue)
      _Record.Comment = txtComment.Text

      'read user-control values
      If MyUserControls IsNot Nothing Then
        _UserControlValues = New List(Of Object)
        For i = 0 To MyUserControls.Count - 1
          _UserControl = MyUserControls(i)
          Select Case _UserControl.Type
            Case UserControlTypes.Textbox
              _UserControlValues.Add(grpUserControls.Controls("utxt" & _UserControl.Name).Text)
            Case UserControlTypes.Numberbox
              If Double.TryParse(grpUserControls.Controls("utxt" & _UserControl.Name).Text, _Value) Then
                _UserControlValues.Add(_Value)
              Else
                _UserControlValues.Add(DBNull.Value)
              End If
            Case UserControlTypes.Checkbox
              _CheckBox = CType(grpUserControls.Controls("uchk" & _UserControl.Name), CheckBox)
              _UserControlValues.Add(_CheckBox.Checked)
            Case UserControlTypes.Combobox
              _ComboBox = CType(grpUserControls.Controls("ucmb" & _UserControl.Name), ComboBox)
              If _ComboBox.SelectedIndex = -1 Then
                _UserControlValues.Add(DBNull.Value)
              Else
                _UserControlValues.Add(_ComboBox.SelectedValue)
              End If
            Case UserControlTypes.ClickCounter
              _Value = CType(grpUserControls.Controls("uclc" & _UserControl.Name), usrClickCounter).Value
              If IsValue(_Value) Then
                _UserControlValues.Add(_Value)
              Else
                _UserControlValues.Add(DBNull.Value)
              End If
            Case UserControlTypes.Placeholder
              _UserControlValues.Add(DBNull.Value)
          End Select
        Next i
        _Record.UserControlValues = _UserControlValues
      End If
    End If

    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        MyProject.SaveCurrentRecordVR()
        MyProject.CloseCurrentRecordVR()
      Case ProjectLayoutModes.Detections
        MyProject.SaveCurrentRecordDT()
        MyProject.CloseCurrentRecordDT()
    End Select

    Me.Cursor = Cursors.Default
  End Sub

  Private Sub AdjustFormSize()
    Dim _Height As Integer, _Width As Integer

    If MyMainDataShown Then
      grpMainData.Width = 360
      grpRoadUsers.Width = 360
      grpUserControls.Width = 360
      btnHide.Left = grpMainData.Right + 2

      btnHide.Text = "<"
      _Width = grpMainData.Width + 20
      MyDisplay.Left = _Width
    Else
      grpMainData.Width = 0
      grpRoadUsers.Width = 0
      grpUserControls.Width = 0
      btnHide.Left = 2
      btnHide.Text = ">"
      _Width = 20
      MyDisplay.Left = 20
    End If


    If MyDisplay.Visible Then _Width = _Width + MyDisplay.Width
    _Width = _Width + 5

    _Height = grpMainData.Height

    If grpRoadUsers.Visible Then _Height = _Height + grpRoadUsers.Height
    If grpUserControls.Visible Then _Height = _Height + grpUserControls.Height
    If MyDisplay.Visible Then _Height = Math.Max(_Height, MyDisplay.Height)

    Me.Width = _Width + 20
    Me.Height = _Height + 130
  End Sub

  'Private Sub UpdateRoadUser()
  ' If lstRoadUsers.SelectedIndex <> -1 Then
  ' myProject.CurrentRecord.RoadUsers(lstRoadUsers.SelectedIndex).Type = clsRoadUser.FindRUTypeByName(cmbRUType.SelectedItem)
  '  End If
  'End Sub

#End Region



#Region "Menu Subs"

  Private Sub mnuProjectLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProjectLoad.Click
    Call LoadProject()
  End Sub

  Private Sub mnuProjectClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProjectClose.Click
    Call UnloadProject()
  End Sub

  Private Sub mnuProjectExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProjectExit.Click
    If MyDisplay.DisplayMode = DisplayModes.Edit Then
      If MyProject.CurrentRecord.RoadUsersChanged Then
        If MsgBox("Do you want to save the edited data?", vbYesNo, "Save edit") = vbYes Then
          MyProject.CurrentRecord.SaveRoadUsers()
        End If
      End If
    End If

    Call UnloadProject()
    Application.Exit()
  End Sub

  Private Sub mnuProjectImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProjectImportData.Click

    Exit Sub

    ' Call NotReadyYet("Import")
    'Exit Sub

    With digOpenFile

      .Title = "Import data"
      .FileName = ""
      .CheckFileExists = True
      .Multiselect = True
      digOpenFile.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\My T-projects\Data\"
      .Filter = "T-Analyst import files (*.taimp)|*.taimp|Clipped video files (*.avi)|*.avi"
      .ShowDialog(Me)

      If .FileName.Length > 0 Then
        Select Case MyProject.LayoutMode
          Case ProjectLayoutModes.VideoRecordings
            MyProject.ImportDataVR(.FileNames)
          Case ProjectLayoutModes.VideoRecordings
            MyProject.ImportDataDT(.FileNames)
        End Select

      End If
    End With
  End Sub

  Private Sub mnuDataVideoRecordings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDataVideoRecordings.Click
    If Not MyIsLoadingProject Then
      If Not mnuDataVideoRecordings.Checked Then
        Call UpdateAndCloseCurrentRecord()
        Call LoadLayoutMode(ProjectLayoutModes.VideoRecordings)
        Call LoadRecord(0)
        Call DisplayRecordData()
        MyProject.SaveProjectFile()
      End If
    End If
  End Sub

  Private Sub mnuDataDetections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDataDetections.Click
    If Not MyIsLoadingProject Then
      If Not mnuDataDetections.Checked Then
        Call UpdateAndCloseCurrentRecord()
        Call LoadLayoutMode(ProjectLayoutModes.Detections)
        Call LoadRecord(0)
        Call DisplayRecordData()
        MyProject.SaveProjectFile()
      End If
    End If
  End Sub



#End Region



#Region "Toolbar subs"

  Private Sub tlbOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tlbOpen.Click
    LoadProject()
  End Sub

#End Region



#Region "Control Subs"

  Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    MyMainDataShown = True

    MyDisplay = New usrDisplay
    MyDisplay.Location = New Point(grpMainData.Right + 10, grpMainData.Top)
    Me.Controls.Add(MyDisplay)
    Call HideControlsOnUnload()

    Call AdjustFormSize()

    '   MyZoomedViewForm = New frmZoom
  End Sub

  Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    If MyDisplay.DisplayMode = DisplayModes.Edit Then
      If MyProject.CurrentRecord.RoadUsersChanged Then
        If MsgBox("Do you want to save the edited data?", vbYesNo, "Save edit") = vbYes Then
          MyProject.CurrentRecord.SaveRoadUsers()
        End If
      End If
    End If

    Call UnloadProject()
  End Sub



  Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click

    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        GoToRecord(MyProject.CurrentRecordIndexVR + 1)
      Case ProjectLayoutModes.Detections
        GoToRecord(MyProject.CurrentRecordIndexDT + 1)
    End Select
  End Sub

  Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        GoToRecord(MyProject.CurrentRecordIndexVR - 1)
      Case ProjectLayoutModes.Detections
        GoToRecord(MyProject.CurrentRecordIndexDT - 1)
    End Select
  End Sub

  Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
    GoToRecord(0)
  End Sub

  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        Call GoToRecord(MyProject.NumberOfFilteredRecordsVR - 1)
      Case ProjectLayoutModes.Detections
        Call GoToRecord(MyProject.NumberOfFilteredRecordsDT - 1)
    End Select
  End Sub

  Private Sub btnJumpForward_Click(sender As System.Object, e As System.EventArgs) Handles btnJumpForward.Click
    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        GoToRecord(MyProject.CurrentRecordIndexVR + 50)
      Case ProjectLayoutModes.Detections
        GoToRecord(MyProject.CurrentRecordIndexDT + 50)
    End Select
  End Sub

  Private Sub btnJumpBack_Click(sender As System.Object, e As System.EventArgs) Handles btnJumpBack.Click
    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        GoToRecord(MyProject.CurrentRecordIndexVR - 50)
      Case ProjectLayoutModes.Detections
        GoToRecord(MyProject.CurrentRecordIndexDT - 50)
    End Select
  End Sub

  Private Function GoToRecord(ByVal RecordIndex As Integer) As Boolean

    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        If IsBetween(RecordIndex, 0, MyProject.NumberOfFilteredRecordsVR - 1) Then
          Call UpdateAndCloseCurrentRecord()
          Call LoadRecord(RecordIndex)
          Call DisplayRecordData()
          Return True
        Else
          Return False
        End If
      Case ProjectLayoutModes.Detections
        If IsBetween(RecordIndex, 0, MyProject.NumberOfFilteredRecordsDT - 1) Then
          Call UpdateAndCloseCurrentRecord()
          Call LoadRecord(RecordIndex)
          Call DisplayRecordData()
          Return True
        Else
          Return False
        End If
    End Select

    Return False
  End Function


  Private Sub lblCurrentRecord_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCurrentRecord.SizeChanged
    btnNext.Left = lblCurrentRecord.Right
    btnJumpForward.Left = btnNext.Right
    btnLast.Left = btnJumpForward.Right
  End Sub

  Private Sub btnEditRU_Click() Handles btnEditRU.Click

    Select Case MyDisplay.DisplayMode
      Case DisplayModes.View 'start editing
        grpMainData.Enabled = False
        grpUserControls.Enabled = False
        MyDisplay.DisplayMode = DisplayModes.Edit
        btnEditRU.Text = "Stop edit"
        btnNewRU.Enabled = True
        btnImportRU.Enabled = True

        mnuProject.Enabled = False
        mnuData.Enabled = False
        mnuRecord.Enabled = False
        'mnuRecordClearTTC.Enabled = False
        'mnuRecordMultiple.Enabled = False
        'mnuRecordCreateDuplicate.Enabled = False


      Case DisplayModes.Edit 'stop editing
        If MyProject.CurrentRecord.RoadUsersChanged Then
          If MsgBox("Do you want to save the edited data?", vbYesNo, "Save edit") = vbYes Then
            For Each _RoadUser In MyProject.CurrentRecord.RoadUsers
              _RoadUser.SmoothTrajectory()
            Next
            MyProject.CurrentRecord.SaveRoadUsers()
          Else
            Select Case MyProject.LayoutMode
              Case ProjectLayoutModes.VideoRecordings
                Call LoadRecord(MyProject.CurrentRecordIndexVR)
              Case ProjectLayoutModes.Detections
                Call LoadRecord(MyProject.CurrentRecordIndexDT)
            End Select
            Call DisplayRecordData()
          End If
        End If

        grpMainData.Enabled = True
        grpUserControls.Enabled = True
        MyDisplay.DisplayMode = DisplayModes.View
        btnEditRU.Text = "Edit"
        btnImportRU.Enabled = False
        btnNewRU.Enabled = False

        mnuProject.Enabled = True
        mnuData.Enabled = True
        mnuRecord.Enabled = True
        'mnuRecordClearTTC.Enabled = True
        'm'nuRecordMultiple.Enabled = True
        'mnuRecordCreateDuplicate.Enabled = True
    End Select

    Call DisplayRoadUserData(lstRoadUsers.SelectedIndex)
  End Sub

  Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
    Dim _f As frmNewRecord

    _f = New frmNewRecord(MyProject)
    _f.ShowDialog(Me)
    If _f.AddRecord Then
      Call UpdateAndCloseCurrentRecord()

      Select Case MyProject.LayoutMode
        Case ProjectLayoutModes.VideoRecordings
          MyProject.AddRecordVR(_f.StartTime, _f.DataPointCount, _f.DataPointFrequency, _f.VideoFile)
        Case ProjectLayoutModes.Detections
          MyProject.AddRecordDT(_f.StartTime, _f.DataPointCount, _f.DataPointFrequency, _f.VideoFile)
      End Select

      _f.Dispose()
      Call DisplayRecordData()
    End If
  End Sub



  Private Sub lstRoadUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstRoadUsers.SelectedIndexChanged

    If Not MyIsLoadingRecord Then
      If MyProject.CurrentRecord.SelectedRoadUserIndex = lstRoadUsers.SelectedIndex Then
        MyProject.CurrentRecord.SelectedRoadUserIndex = -1
        lstRoadUsers.ContextMenuStrip = Nothing
        lstRoadUsers.SelectedIndex = -1
      Else
        If Not IsBetween(MyDisplay.CurrentFrame, MyProject.CurrentRecord.RoadUsers(lstRoadUsers.SelectedIndex).FirstFrame, MyProject.CurrentRecord.RoadUsers(lstRoadUsers.SelectedIndex).LastFrame) Then _
           MyDisplay.CurrentFrame = MyProject.CurrentRecord.RoadUsers(lstRoadUsers.SelectedIndex).FirstFrame
        MyProject.CurrentRecord.SelectedRoadUserIndex = lstRoadUsers.SelectedIndex
        lstRoadUsers.ContextMenuStrip = mnuRoadUser
      End If
    End If
    Call DisplayRoadUserData(lstRoadUsers.SelectedIndex)

  End Sub

  Private Sub btnRelatedRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRelatedRecords.Click
    Dim _Filter As String
    Dim _ID, _RecordIndex As Integer


    Call UpdateAndCloseCurrentRecord()

    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        _Filter = "cmVideoRecording = " & MyProject.CurrentRecordVR.ID.ToString
        MyProject.FilterDT = _Filter
        Call LoadLayoutMode(ProjectLayoutModes.Detections)
        Call LoadRecord(0)
        Call DisplayRecordData()

      Case ProjectLayoutModes.Detections
                If MyProject.CurrentRecordDT.RelatedRecords.Count > 0 Then
                    _ID = MyProject.CurrentRecordDT.RelatedRecords(0)
                    _RecordIndex = MyProject.FindRecordIndexByID_VR(_ID)
                    If _RecordIndex > -1 Then
                        Call LoadLayoutMode(ProjectLayoutModes.VideoRecordings)
                        Call LoadRecord(_RecordIndex)
                        Call DisplayRecordData(MyProject.CurrentRecordDT.StartTime)
                    End If
                End If
        End Select

    MyProject.SaveProjectFile()

  End Sub

  Private Sub cmbRUType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbRUType.SelectedIndexChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      MyIsLoadingRoadUser = True
      MyProject.CurrentRecord.SelectedRoadUser.Type = MyProject.GetRoadUserTypeByName(CStr(cmbRUType.SelectedItem))
      txtRUlength.Text = MyProject.CurrentRecord.SelectedRoadUser.Length.ToString
      txtRUwidth.Text = MyProject.CurrentRecord.SelectedRoadUser.Width.ToString
      txtRUheight.Text = MyProject.CurrentRecord.SelectedRoadUser.Height.ToString
      txtRUweight.Text = MyProject.CurrentRecord.SelectedRoadUser.Weight.ToString
      MyIsLoadingRoadUser = False
    End If
  End Sub

  Private Sub txtRUlength_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRUlength.TextChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      Double.TryParse(txtRUlength.Text, MyProject.CurrentRecord.SelectedRoadUser.Length)
    End If
  End Sub

  Private Sub txtRUwidth_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRUwidth.TextChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      Double.TryParse(txtRUwidth.Text, MyProject.CurrentRecord.SelectedRoadUser.Width)
    End If
  End Sub

  Private Sub txtRUheight_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRUheight.TextChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      Double.TryParse(txtRUheight.Text, MyProject.CurrentRecord.SelectedRoadUser.Height)
    End If
  End Sub

  Private Sub btnNewRU_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewRU.Click
    Dim _NewRoadUser As clsRoadUser

    MyIsLoadingRecord = True

    lstRoadUsers.Items.Add("Trajectory " & (MyProject.CurrentRecord.RoadUsers.Count + 1).ToString)
    lstRoadUsers.Enabled = True
    _NewRoadUser = MyProject.CurrentRecord.AddRoadUser(MyDisplay.CurrentFrame, MyProject.RoadUserTypes(1))
    lstRoadUsers.SelectedIndex = MyProject.CurrentRecord.SelectedRoadUserIndex
    Call DisplayRoadUserData(MyProject.CurrentRecord.SelectedRoadUserIndex)

    MyIsLoadingRecord = False
  End Sub

  Private Sub btnDeleteRU_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteRU.Click

    MyIsLoadingRecord = True

    With MyProject.CurrentRecord

      If .SelectedRoadUser IsNot Nothing Then
        MyProject.CurrentRecord.RemoveRoadUser(.SelectedRoadUser)
        lstRoadUsers.SelectedIndex = -1
        lstRoadUsers.Items.RemoveAt(lstRoadUsers.Items.Count - 1)
        If lstRoadUsers.Items.Count = 0 Then lstRoadUsers.Enabled = False
      End If
    End With

    MyIsLoadingRecord = False
  End Sub

  Private Sub btnImportRU_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportRU.Click
    Dim f As frmImportRoadUsers

    ' MyDisplay.Close()

    f = New frmImportRoadUsers(MyProject.CurrentRecord)
    f.ShowDialog(Me)

    If f.ImportResults Then
      MyProject.CurrentRecord.ImportRoadUsers(f.ImportFile, f.VideoFile, f.KeepRoadUsers, f.ConnectTrajectories)
      Call LoadRoadUsers()
    End If
    ' Call LoadMyDisplay()

    ' MyDisplay.SelectDataPointForm()
  End Sub

#End Region



#Region "MyDisplay events"

  Private Sub myDisplay_ExportDetection(ByVal StartFrame As Integer, EndFrame As Integer) Handles MyDisplay.ExportDetection
    Dim _Filter As String
    Dim _RecordID, _RecordIndex As Integer


    If MyProject.LayoutMode = ProjectLayoutModes.VideoRecordings Then
      Call UpdateAndCloseCurrentRecord()
      _RecordID = MyProject.ExportDetection(StartFrame, EndFrame)

      _Filter = "cmVideoRecording = " & MyProject.CurrentRecordVR.ID.ToString
      MyProject.FilterDT = _Filter
      _RecordIndex = MyProject.FindRecordIndexByID_DT(_RecordID)
      MyProject.LayoutMode = ProjectLayoutModes.Detections
      Call LoadRecord(_RecordIndex)
      Call LoadLayoutMode(MyProject.LayoutMode)
      Call DisplayRecordData()
    End If
  End Sub

  Private Sub MyDisplay_PlaybackStarted() Handles MyDisplay.PlaybackStarted
    mnuMain.Enabled = False
    tlbMain.Enabled = False
    grpMainData.Enabled = False
    grpRoadUsers.Enabled = False
    ' grpUserControls.Enabled = False
    ' For Each _Control In grpUserControls.Controls
    'If _Control.GetType.ToString = "TA_GUI.usrClickCounter" Then
    '_Control.enabled = False
    ' End If
    ' Next
  End Sub

  Private Sub MyDisplay_PlaybackStopped() Handles MyDisplay.PlaybackStopped
    mnuMain.Enabled = True
    tlbMain.Enabled = True
    grpRoadUsers.Enabled = True
    grpMainData.Enabled = True
    grpUserControls.Enabled = True
  End Sub

  Private Sub MyDisplay_MouseMove(ByVal PointM As clsGeomPoint3D, ByVal PointPxl As Point, ByVal OnePixelError As Double) Handles MyDisplay.Display_MouseMove
    Dim _Message As String

    If Not IsNothing(PointM) Then
      _Message = "X: " & Format(PointM.X, "###0.0 m") & ", Y: " & Format(PointM.Y, "###0.0 m") & ", Z: " & Format(PointM.Z, "###0.0 m")
    Else
      _Message = "X: ??? m, Y: ??? m, Z: ??? m"
    End If
    stsWorldCoordinates.Text = _Message

    If Not IsNothing(PointPxl) Then
      _Message = "|X: " & PointPxl.X.ToString & " pxl, Y: " & PointPxl.Y.ToString & " pxl"
    Else
      _Message = "|X: ??? pxl, Y: ??? pxl"
    End If
    stsImageCoordinates.Text = _Message

    If IsValue(OnePixelError) Then
      _Message = "|1 pixel = " & FormatNumber(OnePixelError, 2) & " m"
    Else
      _Message = "|1 pixel = ??? m"
    End If
    stsOnePixelError.Text = _Message

  End Sub

#End Region





    Private Sub lblCurrentRecord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblCurrentRecord.DoubleClick
    Dim _Input As String
    Dim _GoToRecord As Integer

    _Input = InputBox("Go to record nr.", "Go to record")

    If _Input.Length > 0 Then
      If Integer.TryParse(_Input, _GoToRecord) Then
        Call GoToRecord(_GoToRecord - 1)
      End If
    End If
  End Sub


    Private Sub btnMap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMap.Click
    Dim _f As frmMapVideo
    Dim _CurrentFrame As Integer

    _CurrentFrame = MyDisplay.CurrentFrame
    _f = New frmMapVideo(MyProject.CurrentRecord)
    _f.ShowDialog(Me)
    _f.Dispose()
    MyProject.SaveCurrentRecord()
    MyDisplay.LoadDisplay(MyProject.CurrentRecord, MyProject.VisualiserOptions, _CurrentFrame)
    Call UpdateDisplayMenu()
  End Sub



  Private Sub mnuHelpAbout_Click(sender As System.Object, e As System.EventArgs) Handles mnuHelpAbout.Click
    Dim _f As frmAbout

    _f = New frmAbout
    _f.ShowDialog(Me)
  End Sub

  Private Sub mnuProjectNew_Click(sender As System.Object, e As System.EventArgs) Handles mnuProjectNew.Click
    Call CreateNewProject()
  End Sub

  Private Sub CreateNewProject()
    Dim _ProjectName As String
    Dim _ProjectDir As String
    Dim _ProjectFile As String


    _ProjectName = InputBox("Save new project as", "New project")

    If _ProjectName.Length > 0 Then
      If MyProject IsNot Nothing Then Call UnloadProject()

      _ProjectDir = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\My T-projects\"
      _ProjectFile = _ProjectDir & _ProjectName & "\" & _ProjectName & ".taprj"

      'create new project
      MyProject = New clsProject
      MyProject.CreateNewProject(_ProjectName, _ProjectDir)
      MyProject = Nothing

      Call LoadProject(_ProjectFile)
    End If
  End Sub

  Private Sub tlbNew_Click(sender As System.Object, e As System.EventArgs) Handles tlbNew.Click
    Call CreateNewProject()
  End Sub


  Private Sub MyDisplay_SizeChanged() Handles MyDisplay.SizeChanged
    Call AdjustFormSize()
  End Sub




  Private Sub frmMain_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel
    If MyDisplay.CurrentRecord IsNot Nothing Then MyDisplay.ReadMouseWheel(e)
  End Sub

  Private Sub mnuDataFilter_Click(sender As Object, e As EventArgs) Handles mnuDataFilter.Click
    Dim f As New frmFilter
    Dim _NewFilter As String


    Select Case MyProject.LayoutMode
      Case ProjectLayoutModes.VideoRecordings
        f.txtFilter.Text = MyProject.FilterVR
        f.ShowDialog(Me)
        _NewFilter = f.txtFilter.Text
        f.Dispose()

        If MyProject.FilterVR <> _NewFilter Then
          Call UpdateAndCloseCurrentRecord()
          MyProject.FilterVR = _NewFilter
          Call LoadRecord(0)
          Call DisplayRecordData()
        End If


      Case ProjectLayoutModes.Detections
        f.txtFilter.Text = MyProject.FilterDT
        f.ShowDialog(Me)
        _NewFilter = f.txtFilter.Text
        f.Dispose()

        If MyProject.FilterDT <> _NewFilter Then
          Call UpdateAndCloseCurrentRecord()
          MyProject.FilterDT = _NewFilter
          Call LoadRecord(0)
          Call DisplayRecordData()
        End If
    End Select
  End Sub



  Private Sub mnuRecordCalculateTTCTrajectoryBased_Click(sender As Object, e As EventArgs) Handles mnuRecordCalculateTTCTrajectoryBased.Click
    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      MyProject.CurrentRecord.CalculateTTCindicators(True)
      MyDisplay.RefreshDisplay(True)
      Me.Cursor = Cursors.Default
    End If
  End Sub



  Private Sub mnuRecordCalculateTTCStraight_Click(sender As Object, e As EventArgs) Handles mnuRecordCalculateTTCStraight.Click
    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      MyProject.CurrentRecord.CalculateTTCindicators(False)
      MyDisplay.RefreshDisplay(True)
      Me.Cursor = Cursors.Default
    End If
  End Sub

  Private Sub mnuRecordCalculateDistance_Click(sender As Object, e As EventArgs) Handles mnuRecordCalculateDistance.Click
    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      MyProject.CurrentRecord.CalculateDistanceIndicators()
      MyDisplay.RefreshDisplay(True)
      Me.Cursor = Cursors.Default
    End If
  End Sub

  Private Sub mnuRecordClearTTC_Click(sender As Object, e As EventArgs) Handles mnuRecordClearTTC.Click
    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      MyProject.CurrentRecord.ClearMessagesAndIndicators()
      MyDisplay.RefreshDisplay()
      Me.Cursor = Cursors.Default
    End If
  End Sub





  Private Sub CurvedToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CurvedToolStripMenuItem.Click

    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      For Each RoadUser In MyProject.CurrentRecord.RoadUsers
        RoadUser.SmoothTrajectory()
      Next
      MyProject.CurrentRecord.SaveRoadUsers()
      Me.Cursor = Cursors.Default
      MsgBox("Done!")
    End If
  End Sub

  Private Sub StraightToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StraightToolStripMenuItem.Click

    If MyProject.CurrentRecord IsNot Nothing Then
      Me.Cursor = Cursors.WaitCursor
      For Each RoadUser In MyProject.CurrentRecord.RoadUsers
        RoadUser.SmoothTrajectory(, , , )
      Next
      MyProject.CurrentRecord.SaveRoadUsers()
      Me.Cursor = Cursors.Default
      MsgBox("Done!")
    End If
  End Sub



  Private Sub chkRUKey1_CheckedChanged(sender As Object, e As EventArgs) Handles chkRUKey1.CheckedChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      MyIsLoadingRoadUser = True
      If chkRUKey1.Checked Then
        chkRUKey2.Checked = False
        MyProject.CurrentRecord.KeyRoadUser1 = MyProject.CurrentRecord.SelectedRoadUser
      Else
        MyProject.CurrentRecord.KeyRoadUser1 = Nothing
      End If
      MyIsLoadingRoadUser = False
      Call MyProject.CurrentRecord.SaveRoadUsers()
    End If
  End Sub

  Private Sub chkRUKey2_CheckedChanged(sender As Object, e As EventArgs) Handles chkRUKey2.CheckedChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      MyIsLoadingRoadUser = True
      If chkRUKey2.Checked Then
        chkRUKey1.Checked = False
        MyProject.CurrentRecord.KeyRoadUser2 = MyProject.CurrentRecord.SelectedRoadUser
      Else
        MyProject.CurrentRecord.KeyRoadUser2 = Nothing
      End If
      MyIsLoadingRoadUser = False
      Call MyProject.CurrentRecord.SaveRoadUsers()
    End If
  End Sub

  Private Sub mnuProjectImportDataIndividualVideos_Click(sender As Object, e As EventArgs) Handles mnuProjectImportDataIndividualVideos.Click
    Dim _f As frmImportDataIndividualVideos
    Dim _VideoReader As clsVideoReader
    Dim _FrameCount As Integer
    Dim _FrameRate As Double
    Dim _StartTime As Double


    _f = New frmImportDataIndividualVideos(MyProject)
    _f.ShowDialog(Me)

    If _f.ImportFiles Then
      Call UpdateAndCloseCurrentRecord()

      Me.Cursor = Cursors.WaitCursor

      For Each _VideoFile In _f.FileList
        _VideoReader = New clsVideoReader(_VideoFile)

        If _f.UseExistingFPS Then
          _FrameCount = _VideoReader.FrameCount
          _FrameRate = _VideoReader.FPS
        Else
          _FrameCount = CInt(_VideoReader.FrameCount / _VideoReader.FPS * _f.FrameRate)
          _FrameRate = _f.FrameRate
        End If
        _StartTime = _VideoReader.GetTimeStamp(0)
        ' _DateTime = Date.FromOADate(_StartTime / 24 / 60 / 60)
        _VideoReader.Close()

        Select Case MyProject.LayoutMode
          Case ProjectLayoutModes.VideoRecordings
            MyProject.AddRecordVR(_StartTime, _FrameCount, _FrameRate, _VideoFile, "", True)
            If _f.CalibrationFile.Length > 0 Then
              MyProject.CurrentRecordVR.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
              MyProject.CurrentRecordVR.VideoFiles(0).Status = VideoFileStatuses.Updated
            End If
            If _f.MapFile.Length > 0 Then _
              MyProject.CurrentRecordVR.ImportMap(_f.MapFile, True)
            MyProject.SaveCurrentRecordVR()

          Case ProjectLayoutModes.Detections
            MyProject.AddRecordDT(_StartTime, _FrameCount, _FrameRate, _VideoFile, "", True)
            If _f.CalibrationFile.Length > 0 Then
              MyProject.CurrentRecordDT.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
              MyProject.CurrentRecordDT.VideoFiles(0).Status = VideoFileStatuses.Updated
            End If
            If _f.MapFile.Length > 0 Then _
              MyProject.CurrentRecordDT.ImportMap(_f.MapFile, True)
            MyProject.SaveCurrentRecordDT()
        End Select

        Me.Cursor = Cursors.Default
      Next
      Call DisplayRecordData()
    End If

    _f.Dispose()
    _f = Nothing
  End Sub


  Private Sub grpRoadUsers_Enter(sender As Object, e As EventArgs) Handles grpRoadUsers.Enter

  End Sub

  Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
    MyProject.CurrentRecordDT.SaveGraphs()
  End Sub



  Private Sub mnuDataTableDesign_Click(sender As Object, e As EventArgs) Handles mnuDataTableDesign.Click
    With New frmTableDesign(MyProject)
      .ShowDialog(Me)

    End With
  End Sub

  Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
    Dim _GotoRecord As Integer


    If MsgBox("Are you sure you want to delete the current record?", CType(4 + 256, MsgBoxStyle), "Delete record") = MsgBoxResult.Yes Then
      Select Case MyProject.LayoutMode
        Case ProjectLayoutModes.VideoRecordings
          _GotoRecord = MyProject.CurrentRecordIndexVR - 1
          If _GotoRecord < 0 Then _GotoRecord = 0
          MyProject.DeleteCurrentRecordVR()
          Call LoadRecord(_GotoRecord)
          Call DisplayRecordData()

        Case ProjectLayoutModes.Detections
          _GotoRecord = MyProject.CurrentRecordIndexDT - 1
          If _GotoRecord < 0 Then _GotoRecord = 0
          MyProject.DeleteCurrentRecordDT()
          Call LoadRecord(_GotoRecord)
          Call DisplayRecordData()
      End Select
    End If
  End Sub

  Private Sub VisualisationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuDisplaySettings.Click
    Dim _VisualiserOptionForm As frmVisualiserOptions


    _VisualiserOptionForm = New frmVisualiserOptions(MyProject)
    _VisualiserOptionForm.ShowDialog(Me)
    _VisualiserOptionForm.Dispose()

    MyProject.SaveProjectFile()
    MyDisplay.RefreshDisplay()
    chkRUKey1.ForeColor = MyProject.VisualiserOptions.LineColourSpeedRoadUserKey1
    chkRUKey2.ForeColor = MyProject.VisualiserOptions.LineColourSpeedRoadUserKey2
  End Sub

  Private Sub txtRUweight_TextChanged(sender As Object, e As EventArgs) Handles txtRUweight.TextChanged
    If (Not MyIsLoadingRecord) And (Not MyIsLoadingRoadUser) And (MyProject.CurrentRecord.SelectedRoadUser IsNot Nothing) Then
      Integer.TryParse(txtRUweight.Text, MyProject.CurrentRecord.SelectedRoadUser.Weight)
    End If
  End Sub



  Private Sub mnuRoaduserCheckGates_Click(sender As Object, e As EventArgs) Handles mnuRoaduserCheckGates.Click
    Dim _MessageString As String
    Dim _CrossingPoint As clsGateCrossPoint
    Dim _MessagePoint As clsMessagePoint
    Dim _MessageBox As frmMessage

    _MessageString = ""

    With MyProject.CurrentRecord
      If .Gates.Count > 0 Then
        MyProject.CurrentRecord.LoadGatesDisplay()
        For Each _Gate In .Gates
          If _MessageString.Length > 0 Then _MessageString = _MessageString & vbNewLine

          _CrossingPoint = Nothing
          _CrossingPoint = .SelectedRoadUser.CrossGateAt(_Gate)
          If _CrossingPoint IsNot Nothing Then
            _MessageString = _MessageString & _Gate.Name &
                             ": X = " & FormatNumber(_CrossingPoint.X, 1) &
                             "m   Y = " & FormatNumber(_CrossingPoint.Y, 1) &
                             "m   t = " & FormatNumber(_CrossingPoint.Time, 2) &
                             " sec.   v = " & FormatNumber(_CrossingPoint.V, 1) & " m/s."

            For _Frame = .SelectedRoadUser.FirstFrame To .SelectedRoadUser.LastFrame
              _MessagePoint = New clsMessagePoint
              _MessagePoint.X = _CrossingPoint.X
              _MessagePoint.Y = _CrossingPoint.Y
              _MessagePoint.Colour = Color.Red
              MyProject.CurrentRecord.TimeLine(_Frame).MessagePoints.Add(_MessagePoint)
            Next
          Else
            _MessageString = _MessageString & _Gate.Name & ": does not cross the gate"
          End If
        Next
      Else
        _MessageString = "You don't have any gates loaded"
      End If
    End With

    MyDisplay.RefreshDisplay()
    _MessageBox = New frmMessage(_MessageString, "Check gates")
    _MessageBox.Show(Me)


  End Sub

  Private Sub mnuRecordLoadGates_Click(sender As Object, e As EventArgs) Handles mnuRecordLoadGates.Click

    With New OpenFileDialog
      .Filter = "gate-files (*.tagat)|*.tagat"
      .Multiselect = False
      .CheckFileExists = True
      .FileName = ""
      .InitialDirectory = MyProject.CurrentRecord.ImportDirectory
      .ShowDialog()

      If .FileName.Length > 0 Then
        MyProject.CurrentRecord.LoadGates(.FileName)
      End If

    End With
  End Sub


  Private Sub mnuDisplayRoadUsers_Click(sender As Object, e As EventArgs) Handles mnuDisplayRoadUsers.Click
    If MyProject.VisualiserOptions.DrawRoadUsers Then
      MyProject.VisualiserOptions.DrawRoadUsers = False
      mnuDisplayRoadUsers.Checked = False
    Else
      MyProject.VisualiserOptions.DrawRoadUsers = True
      mnuDisplayRoadUsers.Checked = True
    End If

    MyDisplay.RefreshDisplay()
  End Sub

  Private Sub mnuDisplayTrajectories_Click(sender As Object, e As EventArgs) Handles mnuDisplayTrajectories.Click
    If MyProject.VisualiserOptions.DrawTrajectories Then
      MyProject.VisualiserOptions.DrawTrajectories = False
      mnuDisplayTrajectories.Checked = False
    Else
      MyProject.VisualiserOptions.DrawTrajectories = True
      mnuDisplayTrajectories.Checked = True
    End If

    MyDisplay.RefreshDisplay()
  End Sub

  Private Sub mnuDisplayMap_Click(sender As Object, e As EventArgs) Handles mnuDisplayMap.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayMap.Checked Then
        MyProject.CurrentRecord.Map.Visible = False
        mnuDisplayMap.Checked = False
      Else
        MyProject.CurrentRecord.Map.Visible = True
        mnuDisplayMap.Checked = True
      End If
      MyDisplay.RefreshDisplay()
    End If
  End Sub



  Private Sub mnuDisplayCamera1_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera1.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera1.Checked Then
        MyProject.CurrentRecord.VideoFiles(0).Visible = False
        mnuDisplayCamera1.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(0).Visible = True
        mnuDisplayCamera1.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(0).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera2_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera2.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera2.Checked Then
        MyProject.CurrentRecord.VideoFiles(1).Visible = False
        mnuDisplayCamera2.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(1).Visible = True
        mnuDisplayCamera2.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(1).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera3_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera3.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera3.Checked Then
        MyProject.CurrentRecord.VideoFiles(2).Visible = False
        mnuDisplayCamera3.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(2).Visible = True
        mnuDisplayCamera3.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(2).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera4_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera4.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera4.Checked Then
        MyProject.CurrentRecord.VideoFiles(3).Visible = False
        mnuDisplayCamera4.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(3).Visible = True
        mnuDisplayCamera4.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(3).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera5_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera5.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera5.Checked Then
        MyProject.CurrentRecord.VideoFiles(4).Visible = False
        mnuDisplayCamera5.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(4).Visible = True
        mnuDisplayCamera5.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(4).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera6_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera6.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera6.Checked Then
        MyProject.CurrentRecord.VideoFiles(5).Visible = False
        mnuDisplayCamera6.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(5).Visible = True
        mnuDisplayCamera6.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(5).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera7_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera7.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera7.Checked Then
        MyProject.CurrentRecord.VideoFiles(6).Visible = False
        mnuDisplayCamera7.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(6).Visible = True
        mnuDisplayCamera7.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(6).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera8_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera8.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera8.Checked Then
        MyProject.CurrentRecord.VideoFiles(7).Visible = False
        mnuDisplayCamera8.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(7).Visible = True
        mnuDisplayCamera8.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(7).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera9_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera9.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera9.Checked Then
        MyProject.CurrentRecord.VideoFiles(8).Visible = False
        mnuDisplayCamera9.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(8).Visible = True
        mnuDisplayCamera9.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(8).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera10_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera10.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera10.Checked Then
        MyProject.CurrentRecord.VideoFiles(9).Visible = False
        mnuDisplayCamera10.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(9).Visible = True
        mnuDisplayCamera10.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(9).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera11_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera11.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera11.Checked Then
        MyProject.CurrentRecord.VideoFiles(10).Visible = False
        mnuDisplayCamera11.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(10).Visible = True
        mnuDisplayCamera11.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(10).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub

  Private Sub mnuDisplayCamera12_Click(sender As Object, e As EventArgs) Handles mnuDisplayCamera12.Click
    If Not MyIsLoadingRecord Then
      If mnuDisplayCamera12.Checked Then
        MyProject.CurrentRecord.VideoFiles(11).Visible = False
        mnuDisplayCamera12.Checked = False
      Else
        MyProject.CurrentRecord.VideoFiles(11).Visible = True
        mnuDisplayCamera12.Checked = True
      End If
      MyProject.CurrentRecord.VideoFiles(11).Status = VideoFileStatuses.Updated
      MyDisplay.RefreshDisplay()
    End If
  End Sub
  Private Sub mnuDisplaySpeedVectors_Click(sender As Object, e As EventArgs) Handles mnuDisplaySpeedVectors.Click
    If MyProject.VisualiserOptions.DrawSpeeds Then
      MyProject.VisualiserOptions.DrawSpeeds = False
      mnuDisplaySpeedVectors.Checked = False
    Else
      MyProject.VisualiserOptions.DrawSpeeds = True
      mnuDisplaySpeedVectors.Checked = True
    End If

    MyDisplay.RefreshDisplay()
  End Sub

  Private Sub mnuProjectExportVideos_Click(sender As Object, e As EventArgs) Handles mnuProjectExportVideos.Click
    Dim _RecordCount As Integer
    ' Dim _Visualiser As clsVisualiser
    ' Dim _VideoFilePath As String
    Dim _CurrentRecordIndex As Integer

    If MyProject.LayoutMode = ProjectLayoutModes.Detections Then
      _RecordCount = MyProject.NumberOfFilteredRecordsDT
      _CurrentRecordIndex = MyProject.CurrentRecordIndexDT

      If MsgBox("You are about to export videos from " & _RecordCount & " records. This may take long time. Do you want to procede?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        For _i = 0 To _RecordCount - 1
          MyProject.LoadRecordDT(_i)
          MyProject.CurrentRecord.ExportVideos(2)



          '_Visualiser = New clsVisualiser(MyProject.CurrentRecordDT, MyProject.VisualiserOptions)
          '_VideoFilePath = MyProject.ExportDirectory & "Video\" & Format(MyProject.CurrentRecordDT.ID, "00000000") & ".avi"
          '_Visualiser.SaveVideoSequence(_VideoFilePath, 0, MyProject.CurrentRecordDT.FrameCount - 1)
        Next
        GoToRecord(_CurrentRecordIndex)
        MsgBox("Done!!!")
      End If
    End If
  End Sub

  Private Sub bntHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
    If MyMainDataShown Then
      MyMainDataShown = False
    Else
      MyMainDataShown = True
    End If

    Call AdjustFormSize()

  End Sub

  Private Sub btnMedia_Click(sender As Object, e As EventArgs) Handles btnMedia.Click

    If MyMediaForm Is Nothing Then MyMediaForm = New frmMedia

    If Not Me.MediaFormShown Then
      MyMediaForm.Show(Me)
    End If
    If MyMediaForm IsNot Nothing Then MyMediaForm.MediaPath = MyProject.CurrentRecord.MediaPath

    MediaFormShown = True
  End Sub

  Private Sub mnuProjectImportDataRubaOutput_Click(sender As Object, e As EventArgs) Handles mnuProjectImportDataRubaOutput.Click
    Dim _f As frmImportDataRuba
    Dim _VideoReader As clsVideoReader
    Dim _FrameCount As Integer
    Dim _FrameRate As Double
    Dim _StartTime As Double
    Dim _FileIndex As Integer
    Dim _Fields() As String
    Dim _Line As String
    Dim _MediaFileName As String
    Dim _VideoFileName As String
    Dim _Bitmap As Bitmap
    Dim _Entering1 As DateTime
    Dim _Entering2 As DateTime
    Dim _Leaving1 As DateTime
    Dim _Leaving2 As DateTime
    Dim _FirstDetected As String
    Dim _TimeGap As Integer
    Dim _EarliestEnter As Date


    _f = New frmImportDataRuba(MyProject)
    _f.ShowDialog(Me)

    If _f.DoImport Then
      Call UpdateAndCloseCurrentRecord()
      Me.Cursor = Cursors.WaitCursor


      MyProject.AddFieldDetections("uDT_Entering1", Type.GetType("System.DateTime"))
      MyProject.AddFieldDetections("uDT_Leaving1", Type.GetType("System.DateTime"))
      MyProject.AddFieldDetections("uDT_Entering2", Type.GetType("System.DateTime"))
      MyProject.AddFieldDetections("uDT_Leaving2", Type.GetType("System.DateTime"))
      MyProject.AddFieldDetections("uDT_FirstDetected", Type.GetType("System.String"))
      MyProject.AddFieldDetections("uDT_TimeGap", Type.GetType("System.Int32"))


      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _f.RubaOtputFile, OpenMode.Input)
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        If Not _Line.StartsWith("File;Date;Entering") Then
          _Fields = _Line.Split(CChar(";"))

          _Entering1 = New Date(Integer.Parse(_Fields(2).Substring(0, 4)),
                                Integer.Parse(_Fields(2).Substring(5, 2)),
                                Integer.Parse(_Fields(2).Substring(8, 2)),
                                Integer.Parse(_Fields(2).Substring(11, 2)),
                                Integer.Parse(_Fields(2).Substring(14, 2)),
                                Integer.Parse(_Fields(2).Substring(17, 2)),
                                Integer.Parse(_Fields(2).Substring(20, 3)))
          _Leaving1 = New Date(Integer.Parse(_Fields(3).Substring(0, 4)),
                               Integer.Parse(_Fields(3).Substring(5, 2)),
                               Integer.Parse(_Fields(3).Substring(8, 2)),
                               Integer.Parse(_Fields(3).Substring(11, 2)),
                               Integer.Parse(_Fields(3).Substring(14, 2)),
                               Integer.Parse(_Fields(3).Substring(17, 2)),
                               Integer.Parse(_Fields(3).Substring(20, 3)))
          _Entering2 = New Date(Integer.Parse(_Fields(5).Substring(0, 4)),
                                Integer.Parse(_Fields(5).Substring(5, 2)),
                                Integer.Parse(_Fields(5).Substring(8, 2)),
                                Integer.Parse(_Fields(5).Substring(11, 2)),
                                Integer.Parse(_Fields(5).Substring(14, 2)),
                                Integer.Parse(_Fields(5).Substring(17, 2)),
                                Integer.Parse(_Fields(5).Substring(20, 3)))
          _Leaving2 = New Date(Integer.Parse(_Fields(6).Substring(0, 4)),
                               Integer.Parse(_Fields(6).Substring(5, 2)),
                               Integer.Parse(_Fields(6).Substring(8, 2)),
                               Integer.Parse(_Fields(6).Substring(11, 2)),
                               Integer.Parse(_Fields(6).Substring(14, 2)),
                               Integer.Parse(_Fields(6).Substring(17, 2)),
                               Integer.Parse(_Fields(6).Substring(20, 3)))
          _TimeGap = Integer.Parse(_Fields(8))
          _FirstDetected = _Fields(9)
          _MediaFileName = _Fields(10)

          If _Entering1 < _Entering2 Then
            _EarliestEnter = _Entering1
          Else
            _EarliestEnter = _Entering2
          End If
          _VideoFileName = _EarliestEnter.Year.ToString("0000") &
                           _EarliestEnter.Month.ToString("00") &
                           _EarliestEnter.Day.ToString("00") & "-" &
                           _EarliestEnter.Hour.ToString("00") &
                           _EarliestEnter.Minute.ToString("00") &
                           _EarliestEnter.Second.ToString("00") & "." &
                           _EarliestEnter.Millisecond.ToString("000") & ".zip"



          For Each _VideoFilePath In _f.VideoFileList
            If Path.GetFileName(_VideoFilePath) = _VideoFileName Then
              For Each _MediaFilePath In _f.MediaFileList
                If Path.GetFileName(_MediaFilePath) = _MediaFileName Then
                  _Bitmap = New Bitmap(_MediaFilePath)
                  _MediaFileName = Path.GetFileNameWithoutExtension(_MediaFilePath) & ".jpg"

                  _VideoReader = New clsVideoReader(_VideoFilePath)
                  If _f.UseExistingFPS Then
                    _FrameCount = _VideoReader.FrameCount
                    _FrameRate = _VideoReader.FPS
                  Else
                    _FrameCount = CInt(_VideoReader.FrameCount / _VideoReader.FPS * _f.FrameRate)
                    _FrameRate = _f.FrameRate
                  End If
                  _StartTime = _VideoReader.GetTimeStamp(0)
                  _VideoReader.Close()


                  Select Case MyProject.LayoutMode
                    'Case ProjectLayoutModes.VideoRecordings
                    '  MyProject.AddRecordVR(_StartTime, _FrameCount, _FrameRate, _VideoFilePath, _MediaFileName, True)
                    '  Call SaveJpgImage(_Bitmap, MyProject.MediaDirectory & "Video recordings\" & _MediaFileName)
                    '  If _f.CalibrationFile.Length > 0 Then
                    '    MyProject.CurrentRecordVR.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
                    '    MyProject.CurrentRecordVR.VideoFiles(0).Status = VideoFileStatuses.Updated
                    '  End If
                    '  If _f.MapFile.Length > 0 Then _
                    '    MyProject.CurrentRecordVR.ImportMap(_f.MapFile, True)
                    '  MyProject.SaveCurrentRecordVR()

                    Case ProjectLayoutModes.Detections


                      MyProject.AddRecordDT(_StartTime, _FrameCount, _FrameRate, _VideoFilePath, _MediaFileName, True)
                      MyProject.UpdateValueDetections("uDT_Entering1", _Entering1, Type.GetType("System.DateTime"))
                      MyProject.UpdateValueDetections("uDT_Leaving1", _Leaving1, Type.GetType("System.DateTime"))
                      MyProject.UpdateValueDetections("uDT_Entering2", _Entering2, Type.GetType("System.DateTime"))
                      MyProject.UpdateValueDetections("uDT_Leaving2", _Leaving2, Type.GetType("System.DateTime"))
                      MyProject.UpdateValueDetections("uDT_FirstDetected", _FirstDetected, Type.GetType("System.String"))
                      MyProject.UpdateValueDetections("uDT_TimeGap", _TimeGap, Type.GetType("System.Int32"))

                      Call SaveJpgImage(_Bitmap, MyProject.MediaDirectory & "Detections\" & _MediaFileName)
                      If _f.CalibrationFile.Length > 0 Then
                        MyProject.CurrentRecordDT.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
                        MyProject.CurrentRecordDT.VideoFiles(0).Status = VideoFileStatuses.Updated
                      End If
                      If _f.MapFile.Length > 0 Then _
                        MyProject.CurrentRecordDT.ImportMap(_f.MapFile, True)




                      '  MyProject.use
                      Dim _UserControl As clsUserControl

                      If MyUserControls IsNot Nothing Then
                        Dim _UserControlValues = New List(Of Object)
                        For _i = 0 To MyUserControls.Count - 1
                          _UserControl = MyUserControls(_i)
                          If _UserControl.Name = "TimeGap" Then
                            _UserControlValues.Add(_TimeGap)
                          ElseIf _UserControl.Name = "FirstDetected" Then
                            _UserControlValues.Add(_FirstDetected)
                          Else
                            _UserControlValues.Add(DBNull.Value)
                          End If
                        Next _i
                        MyProject.CurrentRecordDT.UserControlValues = _UserControlValues
                      End If
                      ' 
                      ' Me.UpdateAndCloseCurrentRecord()

                      MyProject.SaveCurrentRecordDT()
                  End Select
                  Exit For
                End If
              Next
              Exit For
            End If
          Next
        End If
      Loop
      FileClose(_FileIndex)

      Me.Cursor = Cursors.Default
      Call DisplayRecordData()
    End If

    _f.Dispose()
    _f = Nothing
  End Sub

  Private Sub MultipleIndicatorsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuRecordMultiple.Click

    Dim _multi As frmMultiple
    _multi = New frmMultiple(MyProject)
    _multi.Show()

  End Sub

  Private Sub mnuDisplayZoomView_Click(sender As Object, e As EventArgs) Handles mnuDisplayZoomView.Click
    If mnuDisplayZoomView.Checked Then
      MyDisplay.ZoomedViewVisible = False
      mnuDisplayZoomView.Checked = False
    Else
      MyDisplay.ZoomedViewVisible = True
      mnuDisplayZoomView.Checked = True
    End If
  End Sub

  Private Sub UpdateMenuDisplayZoom() Handles MyDisplay.ZoomedViewBeingClosed
    mnuDisplayZoomView.Checked = False
  End Sub

  Private Sub UpgradeVideoToolStripMenuItem_Click(sender As Object, e As EventArgs)
    MyProject.CurrentRecord.ExportVideos()
  End Sub

  Private Sub mnuRecordCreateDuplicate_Click(sender As Object, e As EventArgs) Handles mnuRecordCreateDuplicate.Click
    Dim _NewRecordID As Integer


    If MyProject.LayoutMode = ProjectLayoutModes.Detections Then
      Call UpdateAndCloseCurrentRecord()

      _NewRecordID = MyProject.DuplicateCurrentDetection()

      Call LoadRecord(_NewRecordID)
      Call DisplayRecordData()
    End If



    '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    'If MyProject.LayoutMode = ProjectLayoutModes.Detections Then
    '  For _i = 0 To MyProject.NumberOfRecordsDT - 1
    '    Call GoToRecord(_i)
    '    _NewRecordID = MyProject.DuplicateCurrentDetection()
    '  Next

    '  Call LoadRecord(_NewRecordID)
    '  Call DisplayRecordData()
    'End If
    '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    'For _i = 0 To MyProject.NumberOfRecordsDT - 1
    '  Call GoToRecord(_i)

    '  MyProject.CurrentRecordDT.ExportVideos()


    'Next

  End Sub

  Private Sub ExportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportToolStripMenuItem.Click


    Dim folderlocation, videofilename As String
    Dim go As Boolean
    Dim _RecordIndex As Integer
    go = False

    If folderlocation = vbNullString Then

      Dim fod As FolderBrowserDialog
      fod = New FolderBrowserDialog()

      If (fod.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
        folderlocation = fod.SelectedPath
      End If

    End If


    If Dir(folderlocation & "/" & "Detections.csv") = "Detections.csv" Then
      Dim svar = MsgBox("Detections.csv already exists. Overwrite the file?", MsgBoxStyle.YesNo, "File already exists")
      If svar = MsgBoxResult.Yes Then
        My.Computer.FileSystem.DeleteFile(folderlocation & "/" & "Detections.csv")
        go = True
      End If

    Else
      go = True
    End If

    If Dir(folderlocation & "/" & "Trajectories.csv") = "Trajectories.csv" Then
      Dim svar = MsgBox("Trajectories.csv already exists. Overwrite the file?", MsgBoxStyle.YesNo, "File already exists")
      If svar = MsgBoxResult.Yes Then
        My.Computer.FileSystem.DeleteFile(folderlocation & "/" & "Trajectories.csv")
        go = True
      End If
    Else
      go = True
    End If

    If go = True Then

      Dim detections As IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(folderlocation & "/" & "Detections.csv", False)

      Dim trajectories As IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(folderlocation & "/" & "Trajectories.csv", False)

      detections.WriteLine("ID;Video File;Date;Start Time;Start Frame;Length (s);Length (Frames);Status;Type;Number of Trajectories;Comment")
            trajectories.WriteLine("Detection ID;Road user ID;Type of road user;Frame ID;Time Stamp;X (m);Y (m);Z (m);V (m/s); X (pixels); Y (pixels); Direction (Dx); Direction (Dy)")

            Dim _wait As Waitbar
      _wait = New Waitbar()
      _wait.UserForm_Initialize(MyProject.NumberOfRecordsDT)

      For i = 0 To MyProject.NumberOfRecordsDT - 1
        MyProject.LoadRecordDT(i)

        '###########################

        detections.WriteLine(MyProject.CurrentRecordDT.ID.ToString & ";" & MyProject.CurrentRecordDT.VideoFiles(0).GetVideoFileName & ";" & MyProject.CurrentRecordDT.DateTime.ToShortDateString & ";" & DateTime.FromOADate(MyProject.CurrentRecordDT.StartTime / (24 * 60 * 60)).ToLongTimeString & "." & DateTime.FromOADate(MyProject.CurrentRecordDT.StartTime / (24 * 60 * 60)).Millisecond & ";" & MyProject.CurrentRecordDT.VideoFiles(0).GetFrameIndex(MyProject.CurrentRecordDT.StartTime) & ";" & (MyProject.CurrentRecordDT.FrameCount / MyProject.CurrentRecordDT.FrameRate).ToString & ";" & MyProject.CurrentRecordDT.FrameCount.ToString & ";" & MyProject.CurrentRecordDT.Status.ToString & ";" & MyProject.CurrentRecordDT.Type.ToString & ";" & MyProject.CurrentRecordDT.RoadUsers.Count.ToString & ";" & MyProject.CurrentRecordDT.Comment)

        Dim count As Integer
        count = 1

        For Each _roaduser In MyProject.CurrentRecordDT.RoadUsers

                    For j = _roaduser.FirstFrame To _roaduser.LastFrame

                        Dim height As Double
                        height = -1.0
                        If MyProject.CurrentRecordDT.Map IsNot Nothing Then
                            height = MyProject.CurrentRecordDT.Map.EstimateHeight(_roaduser.DataPoint(j).X, _roaduser.DataPoint(j).Y)
                        End If

                        trajectories.WriteLine(MyProject.CurrentRecordDT.ID.ToString & ";" & count.ToString & ";" & _roaduser.Type.Name & ";" & j.ToString & ";" & DateTime.FromOADate(MyProject.CurrentRecordDT.GetAbsoluteTime(j) / (24 * 60 * 60)).ToLongTimeString & "." & DateTime.FromOADate(MyProject.CurrentRecordDT.GetAbsoluteTime(j) / (24 * 60 * 60)).Millisecond & ";" & _roaduser.DataPoint(j).X.ToString & ";" & _roaduser.DataPoint(j).Y.ToString & ";" & height.ToString & ";" & _roaduser.DataPoint(j).V.ToString & ";" & _roaduser.DataPoint(j).Xpxl.ToString & ";" & _roaduser.DataPoint(j).Ypxl.ToString & ";" & _roaduser.DataPoint(j).DxDy.Dx.ToString & ";" & _roaduser.DataPoint(j).DxDy.Dy.ToString)
                    Next
                    count = count + 1
        Next

        '###########################
        MyProject.CloseCurrentRecordDT()
        trajectories.Flush()
        detections.Flush()
        _wait.advance()
      Next

      _wait.Close()

      trajectories.Close()
      detections.Close()
      Console.WriteLine(My.Computer.FileSystem.ReadAllText(folderlocation & "/" & "Detections.csv"))
      Console.WriteLine(My.Computer.FileSystem.ReadAllText(folderlocation & "/" & "Trajectories.csv"))
    End If


    MsgBox("Detections.csv and Trajectories.csv Exported to " & folderlocation)

  End Sub

  Private Sub mnuProjectImportRecord_Click(sender As Object, e As EventArgs) Handles mnuProjectImportExportedEvent.Click


    Me.Cursor = Cursors.WaitCursor

    With New OpenFileDialog
      .FileName = ""
      .Filter = "Preview files|Preview.avi"
      .Multiselect = False
      .ShowDialog()
      If .FileName.Length > 0 Then
        Call UpdateAndCloseCurrentRecord()
        MyProject.ImportExportedEvent(.FileName)
      End If
    End With

    Call DisplayRecordData()
    Me.Cursor = Cursors.Default
  End Sub


  Private Sub ExportGateDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportGateDataToolStripMenuItem.Click

    Dim folderlocation, _gatefilepath As String
    Dim go As Boolean
    Dim _CrossingPoint As clsGateCrossPoint


    go = False

    If folderlocation = vbNullString Then

      Dim fod As FolderBrowserDialog
      fod = New FolderBrowserDialog()

      If (fod.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
        folderlocation = fod.SelectedPath
      End If

    End If

    If Dir(folderlocation & "/" & "Gates.csv") = "Gates.csv" Then
      Dim svar = MsgBox("Gates.csv already exists. Overwrite the file?", MsgBoxStyle.YesNo, "File already exists")
      If svar = MsgBoxResult.Yes Then
        My.Computer.FileSystem.DeleteFile(folderlocation & "/" & "Gates.csv")
        go = True
      End If

    Else
      go = True
    End If

    With New OpenFileDialog
      .Title = "Select gate file"
      .Filter = "gate-files (*.tagat)|*.tagat"
      .Multiselect = False
      .CheckFileExists = True
      .FileName = ""
      .InitialDirectory = MyProject.CurrentRecord.ImportDirectory
      .ShowDialog()

      If .FileName.Length > 0 Then
        MyProject.CurrentRecord.LoadGates(.FileName)
        _gatefilepath = .FileName
      End If

    End With


    If go = True Then

      Dim gates As IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(folderlocation & "/" & "Gates.csv", False)

      gates.WriteLine("ID;Video File;Date;Type;Road User;Gate;Time (s);Speed (m/s);Comment")

      Dim _wait As Waitbar
      _wait = New Waitbar()
      _wait.UserForm_Initialize(MyProject.NumberOfRecordsDT)

      For i = 0 To MyProject.NumberOfRecordsDT - 1
        MyProject.LoadRecordDT(i)

        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        With MyProject.CurrentRecord

          .LoadGates(_gatefilepath)

          For Each _Gate In .Gates
            For Each _roaduser In .RoadUsers

              _CrossingPoint = Nothing
              _CrossingPoint = _roaduser.CrossGateAt(_Gate)

              If _CrossingPoint IsNot Nothing Then

                gates.WriteLine(.ID.ToString & ";" & .VideoFiles(0).VideoFileName & ";" & .DateTime.ToShortDateString & ";" & .Type.ToString & ";" & _roaduser.Type.Name & ";" & _Gate.Name & ";" & FormatNumber(_CrossingPoint.Time, 2).ToString & ";" & FormatNumber(_CrossingPoint.V, 1) & ";")

              Else
                gates.WriteLine(.ID.ToString & ";" & .VideoFiles(0).VideoFileName & ";" & .DateTime.ToShortDateString & ";" & .Type.ToString & ";" & _roaduser.Type.Name & ";" & _Gate.Name & ";" & ";" & ";" & "Road user does not cross gate")

              End If


            Next
          Next

        End With
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        _wait.advance()
      Next

      _wait.Close()
      gates.Close()
      MsgBox("Done!")
    End If




  End Sub

  Private Sub Create_Gates_Strudl_Click(sender As Object, e As EventArgs) Handles Create_Gates_Strudl.Click

    Dim gate_name As String
    Dim startX As Double
    Dim startY As Double
    Dim endX As Double
    Dim endY As Double

    Dim message, title, defaultValue As String
    Dim myValue As Object
    ' Set prompt.
    message = "Enter a name for the gate, then mouseclick on the start and end position of the gate in the camera viewer"
    ' Set title.
    title = "InputBox Demo"
    defaultValue = "Gate"   ' Set default value.

    ' Display message, title, and default value.
    myValue = InputBox(message, title, defaultValue)
    ' If user has clicked Cancel, set myValue to defaultValue
    If myValue Is "" Then myValue = defaultValue


  End Sub

    Private Sub GroundTruthsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GroundTruthsToolStripMenuItem.Click

    End Sub






    ' Private Sub UpdateZoomedView(ByVal ZoomedView As Bitmap) Handles MyDisplay.DisplayChanged
    '  MyZoomedViewForm.ZoomedBitmap = ZoomedView
    'End Sub
End Class