﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSyncVideoFiles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.lstFramePairs = New System.Windows.Forms.ListBox()
    Me.btnAdd = New System.Windows.Forms.Button()
    Me.btnRemove = New System.Windows.Forms.Button()
    Me.btnClear = New System.Windows.Forms.Button()
    Me.lblClockDiscrepancy = New System.Windows.Forms.Label()
    Me.pnlTstart = New System.Windows.Forms.Panel()
    Me.pnlTstart.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(62, 231)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 25)
    Me.btnOK.TabIndex = 12
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(128, 231)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 25)
    Me.btnCancel.TabIndex = 13
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'lstFramePairs
    '
    Me.lstFramePairs.FormattingEnabled = True
    Me.lstFramePairs.Location = New System.Drawing.Point(3, 3)
    Me.lstFramePairs.Name = "lstFramePairs"
    Me.lstFramePairs.Size = New System.Drawing.Size(148, 212)
    Me.lstFramePairs.TabIndex = 14
    '
    'btnAdd
    '
    Me.btnAdd.Location = New System.Drawing.Point(157, 39)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(60, 25)
    Me.btnAdd.TabIndex = 15
    Me.btnAdd.Text = "+"
    Me.btnAdd.UseVisualStyleBackColor = True
    '
    'btnRemove
    '
    Me.btnRemove.Location = New System.Drawing.Point(157, 70)
    Me.btnRemove.Name = "btnRemove"
    Me.btnRemove.Size = New System.Drawing.Size(60, 25)
    Me.btnRemove.TabIndex = 16
    Me.btnRemove.Text = "-"
    Me.btnRemove.UseVisualStyleBackColor = True
    '
    'btnClear
    '
    Me.btnClear.Location = New System.Drawing.Point(157, 143)
    Me.btnClear.Name = "btnClear"
    Me.btnClear.Size = New System.Drawing.Size(60, 25)
    Me.btnClear.TabIndex = 18
    Me.btnClear.Text = "Clear"
    Me.btnClear.UseVisualStyleBackColor = True
    '
    'lblClockDiscrepancy
    '
    Me.lblClockDiscrepancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblClockDiscrepancy.Location = New System.Drawing.Point(5, 231)
    Me.lblClockDiscrepancy.Name = "lblClockDiscrepancy"
    Me.lblClockDiscrepancy.Size = New System.Drawing.Size(214, 34)
    Me.lblClockDiscrepancy.TabIndex = 19
    Me.lblClockDiscrepancy.Text = "Clock discrepancy = 0,546 sec."
    '
    'pnlTstart
    '
    Me.pnlTstart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.pnlTstart.Controls.Add(Me.lstFramePairs)
    Me.pnlTstart.Controls.Add(Me.btnRemove)
    Me.pnlTstart.Controls.Add(Me.lblClockDiscrepancy)
    Me.pnlTstart.Controls.Add(Me.btnAdd)
    Me.pnlTstart.Controls.Add(Me.btnClear)
    Me.pnlTstart.Location = New System.Drawing.Point(208, 12)
    Me.pnlTstart.Name = "pnlTstart"
    Me.pnlTstart.Size = New System.Drawing.Size(223, 294)
    Me.pnlTstart.TabIndex = 21
    '
    'frmSyncVideoFiles
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(440, 355)
    Me.ControlBox = False
    Me.Controls.Add(Me.pnlTstart)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmSyncVideoFiles"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frmSyncVideoFiles"
    Me.pnlTstart.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents lstFramePairs As System.Windows.Forms.ListBox
  Friend WithEvents btnAdd As System.Windows.Forms.Button
  Friend WithEvents btnRemove As System.Windows.Forms.Button
  Friend WithEvents btnClear As System.Windows.Forms.Button
  Friend WithEvents lblClockDiscrepancy As System.Windows.Forms.Label
  Friend WithEvents pnlTstart As System.Windows.Forms.Panel
End Class
