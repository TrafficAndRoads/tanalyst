﻿Public Class frmImportDataIndividualVideos
  Public ImportFiles As Boolean
  Public FileList As List(Of String)
  Public CalibrationFile As String
  Public MapFile As String
  Public FrameRate As Double
  Public UseExistingFPS As Boolean

  Private MyParentProject As clsProject


  Public Sub New(ParentProject As clsProject)
    InitializeComponent()

    ImportFiles = False
    FileList = New List(Of String)
    Me.MapFile = ""
    Me.CalibrationFile = ""
    CalibrationFile = ""
    UseExistingFPS = False
    FrameRate = 15
    rbtSetFPSTo.Checked = True
    MyParentProject = ParentProject
  End Sub



  Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
    Me.ImportFiles = True
    Me.Hide()
  End Sub



  Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    Me.ImportFiles = False
    Me.Hide()
  End Sub



  Private Sub btnSelectFiles_Click(sender As Object, e As EventArgs) Handles btnSelectFiles.Click
    Dim _Dialog As OpenFileDialog
    Dim _FileList As String

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = True
            .Filter = "Video files (avi, mov, mp4)|*.avi;*.mp4;*.mov|Zipped frames (*.zip)|*.zip"
            .CheckFileExists = True
      .InitialDirectory = MyParentProject.VideoDirectory
      .ShowDialog()

      If .FileNames.Length > 0 Then
        Me.FileList.Clear()
        _FileList = ""
        For _i = 1 To .FileNames.Length
          _FileList = _FileList & .FileNames(_i - 1) & Environment.NewLine
          Me.FileList.Add(.FileNames(_i - 1))
        Next
        txtImportFileList.Text = _FileList
      End If
    End With
  End Sub



  Private Sub btnClearFiles_Click(sender As Object, e As EventArgs) Handles btnClearFiles.Click
    Me.FileList.Clear()
    txtImportFileList.Text = ""
  End Sub



  Private Sub btnSelectCalibration_Click(sender As Object, e As EventArgs) Handles btnSelectCalibration.Click
    Dim _Dialog As OpenFileDialog

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = False
      .Filter = "Calibration files (*.tacal)|*.tacal"
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .FileName = ""
      .ShowDialog()

      If .FileName.Length > 0 Then
        Me.CalibrationFile = .FileName
        txtCalibrationFile.Text = .FileName
      End If
    End With
  End Sub



  Private Sub btnClearCalibration_Click(sender As Object, e As EventArgs) Handles btnClearCalibration.Click
    Me.CalibrationFile = ""
    txtCalibrationFile.Text = ""
  End Sub



  Private Sub rbtUseVideoFPS_CheckedChanged(sender As Object, e As EventArgs) Handles rbtUseVideoFPS.CheckedChanged
    If rbtUseVideoFPS.Checked Then
      Me.UseExistingFPS = True
      txtFPS.Enabled = False
      btnOK.Enabled = True
    End If
  End Sub



  Private Sub rbtSetFPSTo_CheckedChanged(sender As Object, e As EventArgs) Handles rbtSetFPSTo.CheckedChanged
    If rbtSetFPSTo.Checked Then
      Me.UseExistingFPS = False
      txtFPS.Enabled = True
      Call CheckFPStext()
    End If
  End Sub



  Private Sub txtFPS_TextChanged(sender As Object, e As EventArgs) Handles txtFPS.TextChanged
    Call CheckFPStext()
  End Sub



  Private Sub CheckFPStext()
    Dim _TempFPS As Double

    If Double.TryParse(txtFPS.Text, _TempFPS) Then
      txtFPS.ForeColor = Color.Black
      Me.FrameRate = _TempFPS
      btnOK.Enabled = True
    Else
      txtFPS.ForeColor = Color.Red
      btnOK.Enabled = False
    End If
  End Sub

  Private Sub btnSelectMap_Click(sender As Object, e As EventArgs) Handles btnSelectMap.Click
    Dim _Dialog As OpenFileDialog

    _Dialog = New OpenFileDialog
    With _Dialog
      .Multiselect = False
      .Filter = "Map files (*.tamap)|*.tamap"
      .FileName = ""
      .CheckFileExists = True
      .InitialDirectory = MyParentProject.ImportDirectory
      .ShowDialog()

      If .FileName.Length > 0 Then
        Me.MapFile = .FileName
        txtMapFile.Text = .FileName
      End If
    End With
  End Sub

  Private Sub btnClearMap_Click(sender As Object, e As EventArgs) Handles btnClearMap.Click
    Me.MapFile = ""
    txtMapFile.Text = ""
  End Sub
End Class