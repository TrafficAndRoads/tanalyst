﻿Public Class frmAbout

  Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
    Me.Hide()
  End Sub


  Private Sub llbTFT_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles llbTFT.LinkClicked
    System.Diagnostics.Process.Start(llbTFT.Text)
  End Sub

  Private Sub frmAbout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    Dim _Version As Version

    _Version = Reflection.Assembly.GetExecutingAssembly().GetName().Version
    lblVersion.Text = "Version: " & _Version.Major.ToString & "." & _Version.Minor.ToString
  End Sub

  Private Sub llbSupportMail_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles llbSupportMail.LinkClicked
    Process.Start("Mailto:" & llbSupportMail.Text)
  End Sub
End Class