﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmNewRecord
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()>
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()>
  Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.picFirstFrame = New System.Windows.Forms.PictureBox()
    Me.txtVideoFile = New System.Windows.Forms.TextBox()
    Me.btnLoadVideoFile = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.lblDataPointCount = New System.Windows.Forms.Label()
    Me.lblDataPointFrequency = New System.Windows.Forms.Label()
    Me.lblLengthValue = New System.Windows.Forms.Label()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.btnCancel = New System.Windows.Forms.Button()
    Me.lblLength = New System.Windows.Forms.Label()
    Me.lblFrameCount = New System.Windows.Forms.Label()
    Me.lblFrameCountValue = New System.Windows.Forms.Label()
    Me.lblFrameSize = New System.Windows.Forms.Label()
    Me.lblFPS = New System.Windows.Forms.Label()
    Me.lblFPSValue = New System.Windows.Forms.Label()
    Me.lblFrameSizeValue = New System.Windows.Forms.Label()
    Me.lblDateTime = New System.Windows.Forms.Label()
    Me.pnlVideo = New System.Windows.Forms.Panel()
    Me.pnlRecord = New System.Windows.Forms.Panel()
    Me.lblDataPointCountValue = New System.Windows.Forms.Label()
    Me.lblDataPointFrequencyValue = New System.Windows.Forms.Label()
    Me.txtDateTime = New System.Windows.Forms.TextBox()
    Me.btnChangeFPS = New System.Windows.Forms.Button()
    CType(Me.picFirstFrame, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.pnlVideo.SuspendLayout()
    Me.pnlRecord.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(204, 319)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 6
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'picFirstFrame
    '
    Me.picFirstFrame.Location = New System.Drawing.Point(215, 38)
    Me.picFirstFrame.Name = "picFirstFrame"
    Me.picFirstFrame.Size = New System.Drawing.Size(100, 75)
    Me.picFirstFrame.TabIndex = 4
    Me.picFirstFrame.TabStop = False
    Me.picFirstFrame.Visible = False
    '
    'txtVideoFile
    '
    Me.txtVideoFile.Location = New System.Drawing.Point(65, 10)
    Me.txtVideoFile.Name = "txtVideoFile"
    Me.txtVideoFile.ReadOnly = True
    Me.txtVideoFile.Size = New System.Drawing.Size(188, 20)
    Me.txtVideoFile.TabIndex = 5
    Me.txtVideoFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'btnLoadVideoFile
    '
    Me.btnLoadVideoFile.Location = New System.Drawing.Point(255, 9)
    Me.btnLoadVideoFile.Name = "btnLoadVideoFile"
    Me.btnLoadVideoFile.Size = New System.Drawing.Size(60, 22)
    Me.btnLoadVideoFile.TabIndex = 1
    Me.btnLoadVideoFile.Text = "Load"
    Me.btnLoadVideoFile.UseVisualStyleBackColor = True
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(0, 13)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(64, 13)
    Me.Label1.TabIndex = 7
    Me.Label1.Text = "Video file:"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblDataPointCount
    '
    Me.lblDataPointCount.AutoSize = True
    Me.lblDataPointCount.Location = New System.Drawing.Point(48, 80)
    Me.lblDataPointCount.Name = "lblDataPointCount"
    Me.lblDataPointCount.Size = New System.Drawing.Size(38, 13)
    Me.lblDataPointCount.TabIndex = 9
    Me.lblDataPointCount.Text = "Count:"
    Me.lblDataPointCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblDataPointFrequency
    '
    Me.lblDataPointFrequency.AutoSize = True
    Me.lblDataPointFrequency.Location = New System.Drawing.Point(26, 100)
    Me.lblDataPointFrequency.Name = "lblDataPointFrequency"
    Me.lblDataPointFrequency.Size = New System.Drawing.Size(61, 13)
    Me.lblDataPointFrequency.TabIndex = 10
    Me.lblDataPointFrequency.Text = "Points/sec:"
    Me.lblDataPointFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblLengthValue
    '
    Me.lblLengthValue.AutoSize = True
    Me.lblLengthValue.Location = New System.Drawing.Point(89, 38)
    Me.lblLengthValue.Name = "lblLengthValue"
    Me.lblLengthValue.Size = New System.Drawing.Size(43, 13)
    Me.lblLengthValue.TabIndex = 11
    Me.lblLengthValue.Text = "0:30:00"
    Me.lblLengthValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblLengthValue.Visible = False
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label6.Location = New System.Drawing.Point(10, 55)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(76, 13)
    Me.Label6.TabIndex = 12
    Me.Label6.Text = "Data points:"
    Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(270, 319)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(60, 22)
    Me.btnCancel.TabIndex = 0
    Me.btnCancel.Text = "Cancel"
    Me.btnCancel.UseVisualStyleBackColor = True
    '
    'lblLength
    '
    Me.lblLength.AutoSize = True
    Me.lblLength.Location = New System.Drawing.Point(43, 38)
    Me.lblLength.Name = "lblLength"
    Me.lblLength.Size = New System.Drawing.Size(43, 13)
    Me.lblLength.TabIndex = 19
    Me.lblLength.Text = "Length:"
    Me.lblLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblLength.Visible = False
    '
    'lblFrameCount
    '
    Me.lblFrameCount.AutoSize = True
    Me.lblFrameCount.Location = New System.Drawing.Point(17, 78)
    Me.lblFrameCount.Name = "lblFrameCount"
    Me.lblFrameCount.Size = New System.Drawing.Size(69, 13)
    Me.lblFrameCount.TabIndex = 20
    Me.lblFrameCount.Text = "Frame count:"
    Me.lblFrameCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblFrameCount.Visible = False
    '
    'lblFrameCountValue
    '
    Me.lblFrameCountValue.AutoSize = True
    Me.lblFrameCountValue.Location = New System.Drawing.Point(89, 78)
    Me.lblFrameCountValue.Name = "lblFrameCountValue"
    Me.lblFrameCountValue.Size = New System.Drawing.Size(37, 13)
    Me.lblFrameCountValue.TabIndex = 8
    Me.lblFrameCountValue.Text = "30000"
    Me.lblFrameCountValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblFrameCountValue.Visible = False
    '
    'lblFrameSize
    '
    Me.lblFrameSize.AutoSize = True
    Me.lblFrameSize.Location = New System.Drawing.Point(26, 58)
    Me.lblFrameSize.Name = "lblFrameSize"
    Me.lblFrameSize.Size = New System.Drawing.Size(60, 13)
    Me.lblFrameSize.TabIndex = 21
    Me.lblFrameSize.Text = "Frame size:"
    Me.lblFrameSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblFrameSize.Visible = False
    '
    'lblFPS
    '
    Me.lblFPS.AutoSize = True
    Me.lblFPS.Location = New System.Drawing.Point(56, 98)
    Me.lblFPS.Name = "lblFPS"
    Me.lblFPS.Size = New System.Drawing.Size(30, 13)
    Me.lblFPS.TabIndex = 22
    Me.lblFPS.Text = "FPS:"
    Me.lblFPS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblFPS.Visible = False
    '
    'lblFPSValue
    '
    Me.lblFPSValue.AutoSize = True
    Me.lblFPSValue.Location = New System.Drawing.Point(92, 98)
    Me.lblFPSValue.Name = "lblFPSValue"
    Me.lblFPSValue.Size = New System.Drawing.Size(19, 13)
    Me.lblFPSValue.TabIndex = 23
    Me.lblFPSValue.Text = "20"
    Me.lblFPSValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblFPSValue.Visible = False
    '
    'lblFrameSizeValue
    '
    Me.lblFrameSizeValue.AutoSize = True
    Me.lblFrameSizeValue.Location = New System.Drawing.Point(89, 58)
    Me.lblFrameSizeValue.Name = "lblFrameSizeValue"
    Me.lblFrameSizeValue.Size = New System.Drawing.Size(48, 13)
    Me.lblFrameSizeValue.TabIndex = 24
    Me.lblFrameSizeValue.Text = "640x480"
    Me.lblFrameSizeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblFrameSizeValue.Visible = False
    '
    'lblDateTime
    '
    Me.lblDateTime.AutoSize = True
    Me.lblDateTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblDateTime.Location = New System.Drawing.Point(26, 13)
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Size = New System.Drawing.Size(38, 13)
    Me.lblDateTime.TabIndex = 25
    Me.lblDateTime.Text = "Date:"
    Me.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'pnlVideo
    '
    Me.pnlVideo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.pnlVideo.Controls.Add(Me.picFirstFrame)
    Me.pnlVideo.Controls.Add(Me.txtVideoFile)
    Me.pnlVideo.Controls.Add(Me.btnLoadVideoFile)
    Me.pnlVideo.Controls.Add(Me.Label1)
    Me.pnlVideo.Controls.Add(Me.lblFrameCountValue)
    Me.pnlVideo.Controls.Add(Me.lblFrameSizeValue)
    Me.pnlVideo.Controls.Add(Me.lblFrameCount)
    Me.pnlVideo.Controls.Add(Me.lblLength)
    Me.pnlVideo.Controls.Add(Me.lblFPSValue)
    Me.pnlVideo.Controls.Add(Me.lblLengthValue)
    Me.pnlVideo.Controls.Add(Me.lblFrameSize)
    Me.pnlVideo.Controls.Add(Me.lblFPS)
    Me.pnlVideo.Location = New System.Drawing.Point(10, 10)
    Me.pnlVideo.Name = "pnlVideo"
    Me.pnlVideo.Size = New System.Drawing.Size(320, 122)
    Me.pnlVideo.TabIndex = 37
    '
    'pnlRecord
    '
    Me.pnlRecord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.pnlRecord.Controls.Add(Me.lblDataPointCountValue)
    Me.pnlRecord.Controls.Add(Me.lblDataPointFrequencyValue)
    Me.pnlRecord.Controls.Add(Me.txtDateTime)
    Me.pnlRecord.Controls.Add(Me.btnChangeFPS)
    Me.pnlRecord.Controls.Add(Me.lblDataPointCount)
    Me.pnlRecord.Controls.Add(Me.lblDateTime)
    Me.pnlRecord.Controls.Add(Me.lblDataPointFrequency)
    Me.pnlRecord.Controls.Add(Me.Label6)
    Me.pnlRecord.Location = New System.Drawing.Point(10, 138)
    Me.pnlRecord.Name = "pnlRecord"
    Me.pnlRecord.Size = New System.Drawing.Size(320, 130)
    Me.pnlRecord.TabIndex = 38
    Me.pnlRecord.Visible = False
    '
    'lblDataPointCountValue
    '
    Me.lblDataPointCountValue.AutoSize = True
    Me.lblDataPointCountValue.Location = New System.Drawing.Point(89, 80)
    Me.lblDataPointCountValue.Name = "lblDataPointCountValue"
    Me.lblDataPointCountValue.Size = New System.Drawing.Size(37, 13)
    Me.lblDataPointCountValue.TabIndex = 34
    Me.lblDataPointCountValue.Text = "30000"
    Me.lblDataPointCountValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lblDataPointFrequencyValue
    '
    Me.lblDataPointFrequencyValue.AutoSize = True
    Me.lblDataPointFrequencyValue.Location = New System.Drawing.Point(89, 100)
    Me.lblDataPointFrequencyValue.Name = "lblDataPointFrequencyValue"
    Me.lblDataPointFrequencyValue.Size = New System.Drawing.Size(19, 13)
    Me.lblDataPointFrequencyValue.TabIndex = 33
    Me.lblDataPointFrequencyValue.Text = "15"
    Me.lblDataPointFrequencyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'txtDateTime
    '
    Me.txtDateTime.Location = New System.Drawing.Point(65, 10)
    Me.txtDateTime.Name = "txtDateTime"
    Me.txtDateTime.ReadOnly = True
    Me.txtDateTime.Size = New System.Drawing.Size(188, 20)
    Me.txtDateTime.TabIndex = 30
    Me.txtDateTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'btnChangeFPS
    '
    Me.btnChangeFPS.Location = New System.Drawing.Point(255, 95)
    Me.btnChangeFPS.Name = "btnChangeFPS"
    Me.btnChangeFPS.Size = New System.Drawing.Size(60, 22)
    Me.btnChangeFPS.TabIndex = 29
    Me.btnChangeFPS.Text = "new fps"
    Me.btnChangeFPS.UseVisualStyleBackColor = True
    '
    'frmNewRecord
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(336, 345)
    Me.ControlBox = False
    Me.Controls.Add(Me.pnlRecord)
    Me.Controls.Add(Me.pnlVideo)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
    Me.Name = "frmNewRecord"
    Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
    Me.Text = "New record"
    CType(Me.picFirstFrame, System.ComponentModel.ISupportInitialize).EndInit()
    Me.pnlVideo.ResumeLayout(False)
    Me.pnlVideo.PerformLayout()
    Me.pnlRecord.ResumeLayout(False)
    Me.pnlRecord.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents picFirstFrame As System.Windows.Forms.PictureBox
  Friend WithEvents txtVideoFile As System.Windows.Forms.TextBox
  Friend WithEvents btnLoadVideoFile As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents lblDataPointCount As System.Windows.Forms.Label
  Friend WithEvents lblDataPointFrequency As System.Windows.Forms.Label
  Friend WithEvents lblLengthValue As System.Windows.Forms.Label
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents lblLength As System.Windows.Forms.Label
  Friend WithEvents lblFrameCount As System.Windows.Forms.Label
  Friend WithEvents lblFrameCountValue As System.Windows.Forms.Label
  Friend WithEvents lblFrameSize As System.Windows.Forms.Label
  Friend WithEvents lblFPS As System.Windows.Forms.Label
  Friend WithEvents lblFPSValue As System.Windows.Forms.Label
  Friend WithEvents lblFrameSizeValue As System.Windows.Forms.Label
  Friend WithEvents lblDateTime As System.Windows.Forms.Label
  Friend WithEvents pnlVideo As System.Windows.Forms.Panel
  Friend WithEvents pnlRecord As System.Windows.Forms.Panel
  Friend WithEvents btnChangeFPS As System.Windows.Forms.Button
  Friend WithEvents txtDateTime As System.Windows.Forms.TextBox
  Friend WithEvents lblDataPointFrequencyValue As Label
  Friend WithEvents lblDataPointCountValue As Label
End Class
