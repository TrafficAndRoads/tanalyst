﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBackground
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.digOpenFile = New System.Windows.Forms.OpenFileDialog()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.lblpxlm = New System.Windows.Forms.Label()
    Me.lblScale = New System.Windows.Forms.Label()
    Me.picBackground = New System.Windows.Forms.PictureBox()
    Me.txtScale = New System.Windows.Forms.TextBox()
    Me.btnLoadBackground = New System.Windows.Forms.Button()
    Me.lblBackground = New System.Windows.Forms.Label()
    Me.txtBackground = New System.Windows.Forms.TextBox()
    Me.GroupBox1.SuspendLayout()
    CType(Me.picBackground, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(474, 12)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 1
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'digOpenFile
    '
    Me.digOpenFile.FileName = "digOpenFile"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.lblpxlm)
    Me.GroupBox1.Controls.Add(Me.lblScale)
    Me.GroupBox1.Controls.Add(Me.picBackground)
    Me.GroupBox1.Controls.Add(Me.txtScale)
    Me.GroupBox1.Controls.Add(Me.btnLoadBackground)
    Me.GroupBox1.Controls.Add(Me.lblBackground)
    Me.GroupBox1.Controls.Add(Me.txtBackground)
    Me.GroupBox1.Location = New System.Drawing.Point(274, 125)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(514, 240)
    Me.GroupBox1.TabIndex = 26
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'lblpxlm
    '
    Me.lblpxlm.AutoSize = True
    Me.lblpxlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblpxlm.Location = New System.Drawing.Point(161, 86)
    Me.lblpxlm.Name = "lblpxlm"
    Me.lblpxlm.Size = New System.Drawing.Size(64, 13)
    Me.lblpxlm.TabIndex = 32
    Me.lblpxlm.Text = "pixels/meter"
    Me.lblpxlm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblpxlm.Visible = False
    '
    'lblScale
    '
    Me.lblScale.AutoSize = True
    Me.lblScale.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblScale.Location = New System.Drawing.Point(63, 86)
    Me.lblScale.Name = "lblScale"
    Me.lblScale.Size = New System.Drawing.Size(37, 13)
    Me.lblScale.TabIndex = 31
    Me.lblScale.Text = "Scale:"
    Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblScale.Visible = False
    '
    'picBackground
    '
    Me.picBackground.Location = New System.Drawing.Point(234, 56)
    Me.picBackground.Name = "picBackground"
    Me.picBackground.Size = New System.Drawing.Size(100, 75)
    Me.picBackground.TabIndex = 30
    Me.picBackground.TabStop = False
    '
    'txtScale
    '
    Me.txtScale.Location = New System.Drawing.Point(106, 83)
    Me.txtScale.Name = "txtScale"
    Me.txtScale.Size = New System.Drawing.Size(49, 20)
    Me.txtScale.TabIndex = 29
    Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.txtScale.Visible = False
    '
    'btnLoadBackground
    '
    Me.btnLoadBackground.Location = New System.Drawing.Point(340, 30)
    Me.btnLoadBackground.Name = "btnLoadBackground"
    Me.btnLoadBackground.Size = New System.Drawing.Size(60, 22)
    Me.btnLoadBackground.TabIndex = 28
    Me.btnLoadBackground.Text = "Load"
    Me.btnLoadBackground.UseVisualStyleBackColor = True
    '
    'lblBackground
    '
    Me.lblBackground.AutoSize = True
    Me.lblBackground.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblBackground.Location = New System.Drawing.Point(8, 33)
    Me.lblBackground.Name = "lblBackground"
    Me.lblBackground.Size = New System.Drawing.Size(92, 13)
    Me.lblBackground.TabIndex = 27
    Me.lblBackground.Text = "Projected map:"
    Me.lblBackground.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txtBackground
    '
    Me.txtBackground.Enabled = False
    Me.txtBackground.Location = New System.Drawing.Point(106, 30)
    Me.txtBackground.Name = "txtBackground"
    Me.txtBackground.Size = New System.Drawing.Size(228, 20)
    Me.txtBackground.TabIndex = 26
    Me.txtBackground.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'frmBackground
    '
    Me.AcceptButton = Me.btnOK
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(800, 386)
    Me.ControlBox = False
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.btnOK)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frmBackground"
    Me.ShowInTaskbar = False
    Me.Text = "Projected map"
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    CType(Me.picBackground, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents digOpenFile As System.Windows.Forms.OpenFileDialog
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents lblpxlm As System.Windows.Forms.Label
  Friend WithEvents lblScale As System.Windows.Forms.Label
  Friend WithEvents picBackground As System.Windows.Forms.PictureBox
  Friend WithEvents txtScale As System.Windows.Forms.TextBox
  Friend WithEvents btnLoadBackground As System.Windows.Forms.Button
  Friend WithEvents lblBackground As System.Windows.Forms.Label
  Friend WithEvents txtBackground As System.Windows.Forms.TextBox
End Class
