﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("T-Analyst")> 
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Lund University, LTH")>
<Assembly: AssemblyProduct("T-Analyst")>
<Assembly: AssemblyCopyright("Copyright ©  2010-2018")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f53e815d-3361-4d31-b6b3-457a8a352c63")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("7.4.0.0")>
<Assembly: AssemblyFileVersion("0.0.0.0")> 
