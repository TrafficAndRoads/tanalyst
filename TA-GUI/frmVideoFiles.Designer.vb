﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVideoFiles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVideoFiles))
    Me.imlButtons = New System.Windows.Forms.ImageList(Me.components)
    Me.digOpenFile = New System.Windows.Forms.OpenFileDialog()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.txtTstart = New System.Windows.Forms.TextBox()
    Me.lblTstart = New System.Windows.Forms.Label()
    Me.cmbSyncFile2 = New System.Windows.Forms.ComboBox()
    Me.chkCallibrationOK = New System.Windows.Forms.CheckBox()
    Me.btnLoadTimeStamps = New System.Windows.Forms.Button()
    Me.btnLoadCalibration = New System.Windows.Forms.Button()
    Me.chkVisible = New System.Windows.Forms.CheckBox()
    Me.btnSyncVideoFiles = New System.Windows.Forms.Button()
    Me.lstVideoFiles = New System.Windows.Forms.ListBox()
    Me.txtVideoFileComment = New System.Windows.Forms.TextBox()
    Me.btnVideoFileUp = New System.Windows.Forms.Button()
    Me.btnVideoFileDown = New System.Windows.Forms.Button()
    Me.btnVideoFileRemove = New System.Windows.Forms.Button()
    Me.btnVideoFileAdd = New System.Windows.Forms.Button()
    Me.btnOK = New System.Windows.Forms.Button()
    Me.grpMap = New System.Windows.Forms.GroupBox()
    Me.lblpxlm = New System.Windows.Forms.Label()
    Me.lblScale = New System.Windows.Forms.Label()
    Me.picBackground = New System.Windows.Forms.PictureBox()
    Me.txtScale = New System.Windows.Forms.TextBox()
    Me.btnLoadBackground = New System.Windows.Forms.Button()
    Me.lblMap = New System.Windows.Forms.Label()
    Me.txtMap = New System.Windows.Forms.TextBox()
    Me.btnRemoveMap = New System.Windows.Forms.Button()
    Me.lblMapSize = New System.Windows.Forms.Label()
    Me.lblMapSizeLabel = New System.Windows.Forms.Label()
    Me.GroupBox1.SuspendLayout()
    Me.grpMap.SuspendLayout()
    CType(Me.picBackground, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'imlButtons
    '
    Me.imlButtons.ImageStream = CType(resources.GetObject("imlButtons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.imlButtons.TransparentColor = System.Drawing.Color.White
    Me.imlButtons.Images.SetKeyName(0, "Up")
    Me.imlButtons.Images.SetKeyName(1, "Down")
    Me.imlButtons.Images.SetKeyName(2, "AddNew")
    Me.imlButtons.Images.SetKeyName(3, "Remove")
    '
    'digOpenFile
    '
    Me.digOpenFile.FileName = "digOpenFile"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.txtTstart)
    Me.GroupBox1.Controls.Add(Me.lblTstart)
    Me.GroupBox1.Controls.Add(Me.cmbSyncFile2)
    Me.GroupBox1.Controls.Add(Me.chkCallibrationOK)
    Me.GroupBox1.Controls.Add(Me.btnLoadTimeStamps)
    Me.GroupBox1.Controls.Add(Me.btnLoadCalibration)
    Me.GroupBox1.Controls.Add(Me.chkVisible)
    Me.GroupBox1.Controls.Add(Me.btnSyncVideoFiles)
    Me.GroupBox1.Controls.Add(Me.lstVideoFiles)
    Me.GroupBox1.Controls.Add(Me.txtVideoFileComment)
    Me.GroupBox1.Controls.Add(Me.btnVideoFileUp)
    Me.GroupBox1.Controls.Add(Me.btnVideoFileDown)
    Me.GroupBox1.Controls.Add(Me.btnVideoFileRemove)
    Me.GroupBox1.Controls.Add(Me.btnVideoFileAdd)
    Me.GroupBox1.Controls.Add(Me.btnOK)
    Me.GroupBox1.Location = New System.Drawing.Point(306, 296)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(404, 167)
    Me.GroupBox1.TabIndex = 120
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'txtTstart
    '
    Me.txtTstart.Enabled = False
    Me.txtTstart.Location = New System.Drawing.Point(365, 26)
    Me.txtTstart.Name = "txtTstart"
    Me.txtTstart.Size = New System.Drawing.Size(59, 20)
    Me.txtTstart.TabIndex = 134
    '
    'lblTstart
    '
    Me.lblTstart.AutoSize = True
    Me.lblTstart.Location = New System.Drawing.Point(330, 29)
    Me.lblTstart.Name = "lblTstart"
    Me.lblTstart.Size = New System.Drawing.Size(37, 13)
    Me.lblTstart.TabIndex = 133
    Me.lblTstart.Text = "Tstart:"
    '
    'cmbSyncFile2
    '
    Me.cmbSyncFile2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmbSyncFile2.FormattingEnabled = True
    Me.cmbSyncFile2.Location = New System.Drawing.Point(289, 193)
    Me.cmbSyncFile2.Name = "cmbSyncFile2"
    Me.cmbSyncFile2.Size = New System.Drawing.Size(135, 21)
    Me.cmbSyncFile2.TabIndex = 132
    '
    'chkCallibrationOK
    '
    Me.chkCallibrationOK.AutoSize = True
    Me.chkCallibrationOK.Enabled = False
    Me.chkCallibrationOK.Location = New System.Drawing.Point(329, 76)
    Me.chkCallibrationOK.Name = "chkCallibrationOK"
    Me.chkCallibrationOK.Size = New System.Drawing.Size(95, 17)
    Me.chkCallibrationOK.TabIndex = 131
    Me.chkCallibrationOK.Text = "Callibration OK"
    Me.chkCallibrationOK.UseVisualStyleBackColor = True
    Me.chkCallibrationOK.Visible = False
    '
    'btnLoadTimeStamps
    '
    Me.btnLoadTimeStamps.Location = New System.Drawing.Point(349, 145)
    Me.btnLoadTimeStamps.Name = "btnLoadTimeStamps"
    Me.btnLoadTimeStamps.Size = New System.Drawing.Size(75, 22)
    Me.btnLoadTimeStamps.TabIndex = 130
    Me.btnLoadTimeStamps.Text = "Time stamps"
    Me.btnLoadTimeStamps.UseVisualStyleBackColor = True
    Me.btnLoadTimeStamps.Visible = False
    '
    'btnLoadCalibration
    '
    Me.btnLoadCalibration.Location = New System.Drawing.Point(207, 145)
    Me.btnLoadCalibration.Name = "btnLoadCalibration"
    Me.btnLoadCalibration.Size = New System.Drawing.Size(75, 22)
    Me.btnLoadCalibration.TabIndex = 129
    Me.btnLoadCalibration.Text = "Calibration"
    Me.btnLoadCalibration.UseVisualStyleBackColor = True
    Me.btnLoadCalibration.Visible = False
    '
    'chkVisible
    '
    Me.chkVisible.AutoSize = True
    Me.chkVisible.Location = New System.Drawing.Point(329, 53)
    Me.chkVisible.Name = "chkVisible"
    Me.chkVisible.Size = New System.Drawing.Size(99, 17)
    Me.chkVisible.TabIndex = 128
    Me.chkVisible.Text = "Show in display"
    Me.chkVisible.UseVisualStyleBackColor = True
    Me.chkVisible.Visible = False
    '
    'btnSyncVideoFiles
    '
    Me.btnSyncVideoFiles.Location = New System.Drawing.Point(207, 193)
    Me.btnSyncVideoFiles.Name = "btnSyncVideoFiles"
    Me.btnSyncVideoFiles.Size = New System.Drawing.Size(75, 22)
    Me.btnSyncVideoFiles.TabIndex = 127
    Me.btnSyncVideoFiles.Text = "Synchronise"
    Me.btnSyncVideoFiles.UseVisualStyleBackColor = True
    Me.btnSyncVideoFiles.Visible = False
    '
    'lstVideoFiles
    '
    Me.lstVideoFiles.FormattingEnabled = True
    Me.lstVideoFiles.Items.AddRange(New Object() {"Line 1", "Line 2", "Line 3", "Line 4", "Line 5", "Line 6"})
    Me.lstVideoFiles.Location = New System.Drawing.Point(6, 20)
    Me.lstVideoFiles.Name = "lstVideoFiles"
    Me.lstVideoFiles.Size = New System.Drawing.Size(156, 147)
    Me.lstVideoFiles.TabIndex = 126
    '
    'txtVideoFileComment
    '
    Me.txtVideoFileComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtVideoFileComment.Location = New System.Drawing.Point(6, 204)
    Me.txtVideoFileComment.Multiline = True
    Me.txtVideoFileComment.Name = "txtVideoFileComment"
    Me.txtVideoFileComment.Size = New System.Drawing.Size(156, 37)
    Me.txtVideoFileComment.TabIndex = 125
    Me.txtVideoFileComment.Visible = False
    '
    'btnVideoFileUp
    '
    Me.btnVideoFileUp.Enabled = False
    Me.btnVideoFileUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileUp.ImageKey = "Up"
    Me.btnVideoFileUp.ImageList = Me.imlButtons
    Me.btnVideoFileUp.Location = New System.Drawing.Point(168, 53)
    Me.btnVideoFileUp.Name = "btnVideoFileUp"
    Me.btnVideoFileUp.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileUp.TabIndex = 124
    Me.btnVideoFileUp.UseVisualStyleBackColor = True
    '
    'btnVideoFileDown
    '
    Me.btnVideoFileDown.Enabled = False
    Me.btnVideoFileDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileDown.ImageKey = "Down"
    Me.btnVideoFileDown.ImageList = Me.imlButtons
    Me.btnVideoFileDown.Location = New System.Drawing.Point(168, 84)
    Me.btnVideoFileDown.Name = "btnVideoFileDown"
    Me.btnVideoFileDown.Size = New System.Drawing.Size(25, 25)
    Me.btnVideoFileDown.TabIndex = 123
    Me.btnVideoFileDown.UseVisualStyleBackColor = True
    '
    'btnVideoFileRemove
    '
    Me.btnVideoFileRemove.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileRemove.ImageKey = "Remove"
    Me.btnVideoFileRemove.ImageList = Me.imlButtons
    Me.btnVideoFileRemove.Location = New System.Drawing.Point(87, 173)
    Me.btnVideoFileRemove.Name = "btnVideoFileRemove"
    Me.btnVideoFileRemove.Size = New System.Drawing.Size(75, 25)
    Me.btnVideoFileRemove.TabIndex = 122
    Me.btnVideoFileRemove.UseVisualStyleBackColor = True
    Me.btnVideoFileRemove.Visible = False
    '
    'btnVideoFileAdd
    '
    Me.btnVideoFileAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btnVideoFileAdd.ImageKey = "AddNew"
    Me.btnVideoFileAdd.ImageList = Me.imlButtons
    Me.btnVideoFileAdd.Location = New System.Drawing.Point(6, 173)
    Me.btnVideoFileAdd.Name = "btnVideoFileAdd"
    Me.btnVideoFileAdd.Size = New System.Drawing.Size(75, 25)
    Me.btnVideoFileAdd.TabIndex = 121
    Me.btnVideoFileAdd.UseVisualStyleBackColor = True
    '
    'btnOK
    '
    Me.btnOK.Location = New System.Drawing.Point(364, 239)
    Me.btnOK.Name = "btnOK"
    Me.btnOK.Size = New System.Drawing.Size(60, 22)
    Me.btnOK.TabIndex = 120
    Me.btnOK.Text = "OK"
    Me.btnOK.UseVisualStyleBackColor = True
    '
    'grpMap
    '
    Me.grpMap.Controls.Add(Me.lblMapSize)
    Me.grpMap.Controls.Add(Me.lblMapSizeLabel)
    Me.grpMap.Controls.Add(Me.btnRemoveMap)
    Me.grpMap.Controls.Add(Me.lblpxlm)
    Me.grpMap.Controls.Add(Me.lblScale)
    Me.grpMap.Controls.Add(Me.picBackground)
    Me.grpMap.Controls.Add(Me.txtScale)
    Me.grpMap.Controls.Add(Me.btnLoadBackground)
    Me.grpMap.Controls.Add(Me.lblMap)
    Me.grpMap.Controls.Add(Me.txtMap)
    Me.grpMap.Location = New System.Drawing.Point(12, 12)
    Me.grpMap.Name = "grpMap"
    Me.grpMap.Size = New System.Drawing.Size(514, 240)
    Me.grpMap.TabIndex = 121
    Me.grpMap.TabStop = False
    Me.grpMap.Text = "Map"
    '
    'lblpxlm
    '
    Me.lblpxlm.AutoSize = True
    Me.lblpxlm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblpxlm.Location = New System.Drawing.Point(161, 114)
    Me.lblpxlm.Name = "lblpxlm"
    Me.lblpxlm.Size = New System.Drawing.Size(64, 13)
    Me.lblpxlm.TabIndex = 32
    Me.lblpxlm.Text = "pixels/meter"
    Me.lblpxlm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblpxlm.Visible = False
    '
    'lblScale
    '
    Me.lblScale.AutoSize = True
    Me.lblScale.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblScale.Location = New System.Drawing.Point(63, 114)
    Me.lblScale.Name = "lblScale"
    Me.lblScale.Size = New System.Drawing.Size(37, 13)
    Me.lblScale.TabIndex = 31
    Me.lblScale.Text = "Scale:"
    Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblScale.Visible = False
    '
    'picBackground
    '
    Me.picBackground.Location = New System.Drawing.Point(234, 56)
    Me.picBackground.Name = "picBackground"
    Me.picBackground.Size = New System.Drawing.Size(100, 75)
    Me.picBackground.TabIndex = 30
    Me.picBackground.TabStop = False
    '
    'txtScale
    '
    Me.txtScale.Location = New System.Drawing.Point(106, 111)
    Me.txtScale.Name = "txtScale"
    Me.txtScale.Size = New System.Drawing.Size(49, 20)
    Me.txtScale.TabIndex = 29
    Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.txtScale.Visible = False
    '
    'btnLoadBackground
    '
    Me.btnLoadBackground.Location = New System.Drawing.Point(340, 30)
    Me.btnLoadBackground.Name = "btnLoadBackground"
    Me.btnLoadBackground.Size = New System.Drawing.Size(60, 22)
    Me.btnLoadBackground.TabIndex = 28
    Me.btnLoadBackground.Text = "Load"
    Me.btnLoadBackground.UseVisualStyleBackColor = True
    '
    'lblMap
    '
    Me.lblMap.AutoSize = True
    Me.lblMap.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMap.Location = New System.Drawing.Point(8, 33)
    Me.lblMap.Name = "lblMap"
    Me.lblMap.Size = New System.Drawing.Size(92, 13)
    Me.lblMap.TabIndex = 27
    Me.lblMap.Text = "Projected map:"
    Me.lblMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txtMap
    '
    Me.txtMap.Enabled = False
    Me.txtMap.Location = New System.Drawing.Point(106, 30)
    Me.txtMap.Name = "txtMap"
    Me.txtMap.Size = New System.Drawing.Size(228, 20)
    Me.txtMap.TabIndex = 26
    Me.txtMap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'btnRemoveMap
    '
    Me.btnRemoveMap.Location = New System.Drawing.Point(340, 58)
    Me.btnRemoveMap.Name = "btnRemoveMap"
    Me.btnRemoveMap.Size = New System.Drawing.Size(60, 22)
    Me.btnRemoveMap.TabIndex = 33
    Me.btnRemoveMap.Text = "Load"
    Me.btnRemoveMap.UseVisualStyleBackColor = True
    '
    'lblMapSize
    '
    Me.lblMapSize.AutoSize = True
    Me.lblMapSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapSize.Location = New System.Drawing.Point(103, 88)
    Me.lblMapSize.Name = "lblMapSize"
    Me.lblMapSize.Size = New System.Drawing.Size(64, 13)
    Me.lblMapSize.TabIndex = 36
    Me.lblMapSize.Text = "pixels/meter"
    Me.lblMapSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblMapSize.Visible = False
    '
    'lblMapSizeLabel
    '
    Me.lblMapSizeLabel.AutoSize = True
    Me.lblMapSizeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMapSizeLabel.Location = New System.Drawing.Point(63, 88)
    Me.lblMapSizeLabel.Name = "lblMapSizeLabel"
    Me.lblMapSizeLabel.Size = New System.Drawing.Size(30, 13)
    Me.lblMapSizeLabel.TabIndex = 35
    Me.lblMapSizeLabel.Text = "Size:"
    Me.lblMapSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lblMapSizeLabel.Visible = False
    '
    'frmVideoFiles
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(770, 474)
    Me.ControlBox = False
    Me.Controls.Add(Me.grpMap)
    Me.Controls.Add(Me.GroupBox1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frmVideoFiles"
    Me.ShowInTaskbar = False
    Me.Text = "Video files"
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.grpMap.ResumeLayout(False)
    Me.grpMap.PerformLayout()
    CType(Me.picBackground, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents imlButtons As System.Windows.Forms.ImageList
  Friend WithEvents digOpenFile As System.Windows.Forms.OpenFileDialog
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents txtTstart As System.Windows.Forms.TextBox
  Friend WithEvents lblTstart As System.Windows.Forms.Label
  Friend WithEvents cmbSyncFile2 As System.Windows.Forms.ComboBox
  Friend WithEvents chkCallibrationOK As System.Windows.Forms.CheckBox
  Friend WithEvents btnLoadTimeStamps As System.Windows.Forms.Button
  Friend WithEvents btnLoadCalibration As System.Windows.Forms.Button
  Friend WithEvents chkVisible As System.Windows.Forms.CheckBox
  Friend WithEvents btnSyncVideoFiles As System.Windows.Forms.Button
  Friend WithEvents lstVideoFiles As System.Windows.Forms.ListBox
  Friend WithEvents txtVideoFileComment As System.Windows.Forms.TextBox
  Friend WithEvents btnVideoFileUp As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileDown As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileRemove As System.Windows.Forms.Button
  Friend WithEvents btnVideoFileAdd As System.Windows.Forms.Button
  Friend WithEvents btnOK As System.Windows.Forms.Button
  Friend WithEvents grpMap As System.Windows.Forms.GroupBox
  Friend WithEvents lblpxlm As System.Windows.Forms.Label
  Friend WithEvents lblScale As System.Windows.Forms.Label
  Friend WithEvents picBackground As System.Windows.Forms.PictureBox
  Friend WithEvents txtScale As System.Windows.Forms.TextBox
  Friend WithEvents btnLoadBackground As System.Windows.Forms.Button
  Friend WithEvents lblMap As System.Windows.Forms.Label
  Friend WithEvents txtMap As System.Windows.Forms.TextBox
  Friend WithEvents lblMapSize As System.Windows.Forms.Label
  Friend WithEvents lblMapSizeLabel As System.Windows.Forms.Label
  Friend WithEvents btnRemoveMap As System.Windows.Forms.Button
End Class
