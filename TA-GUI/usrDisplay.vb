﻿Public Class usrDisplay
  Public Event PlaybackStarted()
  Public Event PlaybackStopped()
  Public Event Display_MouseMove(ByVal PointM As clsGeomPoint3D, ByVal PointPxl As Point, ByVal OnePixelError As Double)
  Public Event ExportDetection(ByVal StartFrame As Integer, EndFrame As Integer)
  Public Event DisplayChanged(ByVal ZoomedView As Bitmap)
  Public Event ZoomedViewBeingClosed()

  Private WithEvents MyRecord As clsRecord

  Private MyVisualiser As clsVisualiser
  Private MyVisualiserOptions As clsVisualiserOptions
  Private MyCurrentFrame As Integer
  Private MyIsPlaying As Boolean
  Private MyWatch As Stopwatch
  Private MyPreviousTick As Long
  Private MyCurrentSpeedFactor, MyDefaultSpeedFactor As Integer
  Private MyDisplayMode As DisplayModes
  Private MyIsLoading As Boolean = False
  Private MyIndicatorsFirstFrame, MyIndicatorsLastFrame As Integer
  Private WithEvents MyChart As Chart
  Private WithEvents MyDataGrid As DataGridView
    Private WithEvents MyZoomedView As frmZoom
    Private _LengthBefore As Integer

    Private MyZoomedViewVisible As Boolean





#Region "Properties"



  Public ReadOnly Property CurrentRecord As clsRecord
    Get
      CurrentRecord = MyRecord
    End Get
  End Property



  Public ReadOnly Property VisualiserOptions As clsVisualiserOptions
    Get
      Return MyVisualiserOptions
    End Get
  End Property



  Public Property DisplayMode As DisplayModes
    Get
      DisplayMode = MyDisplayMode
    End Get
    Set(ByVal value As DisplayModes)
      MyDisplayMode = value
      Select Case MyDisplayMode
        Case DisplayModes.View 'Stop editing road user trajectory
          pnlNavigation.Visible = True
          pnlSpeed.Visible = True
          pnlPlay.Visible = True
          pnlEdit.Visible = False
          pnlPlay.Left = pnlSpeed.Right + 20
          chkGraph.Left = pnlPlay.Right + 20

          If MyRecord.RecordType = RecordTypes.VideoRecording Then
            btnExport.Visible = True
          Else
            btnExport.Visible = False
          End If
          MyVisualiser.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed

        Case DisplayModes.Edit 'Start editing road user trajectory
          pnlNavigation.Visible = True
          pnlSpeed.Visible = True
          pnlPlay.Visible = False
          pnlEdit.Visible = True
          pnlEdit.Left = pnlSpeed.Right + 20
          btnExport.Visible = False
          chkGraph.Left = pnlEdit.Right + 20
          MyVisualiser.VisualiserOptions.TrajectoryType = TrajectoryTypes.Original
      End Select

      Call LoadChartDesign()
      Call LoadChartData()
      Call Display(MyCurrentFrame)
    End Set
  End Property


  Public ReadOnly Property IsPlaying As Boolean
    Get
      IsPlaying = MyIsPlaying
    End Get
  End Property


  Public Property CurrentFrame As Integer
    Get
      CurrentFrame = MyCurrentFrame
    End Get
    Set(ByVal value As Integer)
      MyCurrentFrame = value
    End Set
  End Property

  Public Property ZoomedViewVisible As Boolean
    Get
      Return MyZoomedViewVisible
    End Get
    Set(value As Boolean)
      MyZoomedViewVisible = value
      If MyZoomedViewVisible Then
        MyZoomedView.Show(Me.ParentForm)
      Else
        MyZoomedView.Hide()
      End If
    End Set
  End Property


  Private Sub RaiseEventZoomedViewBeingClosed() Handles MyZoomedView.BeingClosed
    MyZoomedViewVisible = False
    RaiseEvent ZoomedViewBeingClosed()
  End Sub

#End Region



#Region "Public methods"

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    MyChart = New Chart
    MyDataGrid = New DataGridView
    MyZoomedView = New frmZoom

    Me.Controls.Add(MyChart)
    Me.Controls.Add(MyDataGrid)
  End Sub

  Public Sub LoadDisplay(ByVal Record As clsRecord,
                         ByVal VisualiserOptions As clsVisualiserOptions,
                         Optional ByVal FrameToDisplay As Integer = 0)

    MyRecord = Record
    MyVisualiserOptions = VisualiserOptions
    MyVisualiser = New clsVisualiser(MyRecord, MyVisualiserOptions)


    MyWatch = New Stopwatch
    MyDisplayMode = DisplayModes.View
    MyCurrentSpeedFactor = 1
    MyDefaultSpeedFactor = 4
    btnSpeedFactor.Text = "x1"
    tmrFrames.Enabled = False
        _LengthBefore = 5

        MyCurrentFrame = FrameToDisplay

    Call LoadChartDesign()
    Call RefreshDisplay()
  End Sub

  Public Sub RefreshDisplay(Optional ByVal ReloadChartData As Boolean = False)

    MyVisualiser.UpdatePerspectives()

    Call AdjustSize()
    If ReloadChartData Then Call LoadChartData()
    Call Display(MyCurrentFrame)
  End Sub

#End Region



#Region "Private subs"

  Private Sub AdjustSize()
    Dim _TestBitmap As Bitmap


    _TestBitmap = MyVisualiser.CombinedBitmap(0)
    If _TestBitmap IsNot Nothing Then
      picDisplay.Size = _TestBitmap.Size
      MyDataGrid.Height = picDisplay.Height
      MyDataGrid.Width = 340
      MyDataGrid.Location = picDisplay.Location
      MyChart.Height = picDisplay.Height
      MyChart.Width = picDisplay.Width - MyDataGrid.Width
      MyChart.Top = picDisplay.Top
      MyChart.Left = MyDataGrid.Right
      trbFrames.Minimum = 0
      trbFrames.Maximum = MyRecord.FrameCount - 1
      trbFrames.Top = picDisplay.Bottom
      trbFrames.Width = picDisplay.Width
      lblCurrentTime.Top = trbFrames.Bottom
      lblTotalTime.Top = trbFrames.Bottom
      lblCurrentTime.Left = 0
      lblTotalTime.Left = trbFrames.Right - lblTotalTime.Width

      Me.Visible = True
      Me.Size = New Size(picDisplay.Width + 10, lblCurrentTime.Bottom + 10)


    Else
      Me.Size = New Size(0, 0)
      Me.Visible = False
    End If

  End Sub

  ' Private Sub MyRecord_Gates_Changed() Handles MyRecord.Gates_Changed
  '  Call Display(MyCurrentFrame)
  'End Sub

  '  Private Sub MyRecord_Indicators_Changed() Handles MyRecord.Indicators_Changed
  '   If MyDisplayMode = DisplayModes.View Then
  '    Call LoadChartData()
  '   Call Display(MyCurrentFrame)
  'End If
  'End Sub


  'Private Sub MyCurrentRecord_SelectedRoadUserChanged() Handles MyRecord.SelectedRoadUser_Changed
  '  Dim _Frame As Integer


  '  If MyRecord.SelectedRoadUser IsNot Nothing Then
  '    If IsBetween(MyCurrentFrame, MyRecord.SelectedRoadUser.FirstFrame, MyRecord.SelectedRoadUser.LastFrame) Then
  '      _Frame = MyCurrentFrame
  '    Else
  '      _Frame = MyRecord.SelectedRoadUser.FirstFrame
  '    End If
  '  Else
  '    _Frame = MyCurrentFrame
  '  End If

  '  If MyDisplayMode = DisplayModes.Edit Then
  '    Call LoadChartData()
  '  End If
  '  Call Display(_Frame)

  'End Sub

  ''  Private Sub MyCurrentRecord_KeyRoadUser1Changed() Handles MyRecord.KeyRoadUser1_Changed
  '   Call Display(MyCurrentFrame)
  'End Sub

  'Private Sub MyCurrentRecord_KeyRoadUser2Changed() Handles MyRecord.KeyRoadUser2_Changed
  ' Call Display(MyCurrentFrame)
  'End Sub

  'Private Sub MyCurrentRecord_SelectedRoadUserDimensionsChanged() Handles MyRecord.SelectedRoadUser_ModelChanged
  ' If MyRecord.SelectedRoadUser IsNot Nothing Then
  '  Call Display(MyCurrentFrame)
  'E'nd If
  'End Sub

  Private Sub MyCurrentRecord_SelectedRoadUserDimensionsChanged() Handles MyRecord.AppearanceChanged
    Call RefreshDisplay(True)
  End Sub

  Private Sub Display(ByVal Frame As Integer)
    Dim _i As Integer
    Dim _ZoomedBitmap As Bitmap
    Dim _dX, _dY As List(Of Integer)
    Dim _ZoomedPerspectives As List(Of Integer)


    MyCurrentFrame = Frame

    If chkGraph.Checked Then
      MyDataGrid.Visible = True
      MyChart.Visible = True
      picDisplay.Visible = False
      Call LoadChartCurrentFrame()
    Else
      MyDataGrid.Visible = True
      MyChart.Visible = False
      picDisplay.Visible = True
      picDisplay.Image = MyVisualiser.CombinedBitmap(Frame)
    End If


    btnPrevious.Text = "<"
    btnNext.Text = ">"

    If MyDisplayMode = DisplayModes.Edit Then
      If MyCurrentFrame < MyCurrentSpeedFactor Then
        btnPrevious.Text = "+<"
      End If

      If MyCurrentFrame > MyRecord.FrameCount - MyCurrentSpeedFactor - 1 Then btnNext.Text = ">+"
    End If

    lblCurrentFrame.Text = MyCurrentFrame.ToString("00000")
    btnNext.Left = lblCurrentFrame.Right
    btnJumpForward.Left = btnNext.Right + 1

    trbFrames.Value = MyCurrentFrame
    lblCurrentTime.Text = SecondsToTimeDisplay(MyRecord.GetLocalTime(MyCurrentFrame))
    lblTotalTime.Text = SecondsToTimeDisplay(MyRecord.GetLocalTime(MyRecord.FrameCount - 1))



    If (MyDisplayMode = DisplayModes.Edit) And (MyRecord.SelectedRoadUser IsNot Nothing) Then
      btnSmooth.Visible = True
      btnSmoothStraight.Visible = True

      With MyRecord.SelectedRoadUser
        If IsBetween(MyCurrentFrame, .FirstFrame, .LastFrame) Then
          lblXpxl.Visible = True
          lblYpxl.Visible = True
          lblZpxl.Visible = True
          If .DataPoint(MyCurrentFrame - _i).OriginalXYExists Then
            lblXpxl.Text = "X: " & Format(.DataPoint(MyCurrentFrame - _i).Xpxl, "#0.00") & " m"
            lblYpxl.Text = "Y: " & Format(.DataPoint(MyCurrentFrame - _i).Ypxl, "#0.00") & " m"
            lblZpxl.Text = "Z: " & Format(MyRecord.Map.EstimateHeight(.DataPoint(MyCurrentFrame - _i).Xpxl, .DataPoint(MyCurrentFrame - _i).Ypxl), "#0.00") & " m"
            btnCutStart.Enabled = True
            btnCutEnd.Enabled = True
          Else
            lblXpxl.Text = "X: NoValue"
            lblYpxl.Text = "Y: NoValue"
            lblZpxl.Text = "Z: NoValue"
            btnCutStart.Enabled = False
            btnCutEnd.Enabled = False
          End If

          If IsBetween(MyCurrentFrame, .FirstFrame + 1, .LastFrame - 1) Then
            btnCutStart.Visible = True
            btnCutEnd.Visible = True
          Else
            btnCutStart.Visible = False
            btnCutEnd.Visible = False
          End If
        Else
          lblXpxl.Visible = False
          lblYpxl.Visible = False
          lblZpxl.Visible = False

          btnCutStart.Visible = False
          btnCutEnd.Visible = False
        End If
      End With

    Else
      lblXpxl.Visible = False
      lblYpxl.Visible = False
      lblZpxl.Visible = False
      btnCutStart.Visible = False
      btnCutEnd.Visible = False
      btnSmooth.Visible = False
      btnSmoothStraight.Visible = False
    End If

    _ZoomedPerspectives = Nothing
    _dX = Nothing : _dY = Nothing
    _ZoomedBitmap = MyVisualiser.ZoomedBitmap(Frame, _ZoomedPerspectives, _dX, _dY)

    MyZoomedView.SetZoomedBitmap(_ZoomedBitmap, _ZoomedPerspectives, _dX, _dY)
  End Sub

  Private Sub StopPlaying()
    MyWatch.Stop()
    tmrFrames.Enabled = False
    chkPlay.Checked = False
    chkPlay.ImageKey = "Play"
    tltToolTip.SetToolTip(chkPlay, "Play")
    trbFrames.Enabled = True
    MyIsPlaying = False
    Call Display(0)
  End Sub
  Public Sub ZoomedViewWheelMoved(ByVal e As MouseEventArgs) Handles MyZoomedView.MouseWheelMoved
    Call ReadMouseWheel(e)
  End Sub

  Public Sub ReadMouseWheel(ByVal e As MouseEventArgs)
    Dim _i As Integer
    Dim _AllowEdit As Boolean

    If My.Computer.Keyboard.CtrlKeyDown Or My.Computer.Keyboard.ShiftKeyDown Then
      If MyDisplayMode = DisplayModes.Edit Then
        If MyRecord.SelectedRoadUser IsNot Nothing Then
          With MyRecord.SelectedRoadUser
            _i = 0
            _AllowEdit = True

            If chkGraph.Checked Then
              If Not IsBetween(MyCurrentFrame, .FirstFrame, .LastFrame) Then
                _AllowEdit = False
              ElseIf Not .DataPoint(MyCurrentFrame - _i).OriginalXYExists() Then
                _AllowEdit = False
              End If
            Else
              If Not IsBetween(MyCurrentFrame, .FirstFrame, .LastFrame) Then
                .ExtendTrajectory(MyCurrentFrame)
              Else
                Do Until .DataPoint(MyCurrentFrame - _i).OriginalXYExists()
                  _i = _i + 1
                Loop
              End If
            End If

            If _AllowEdit Then
              If My.Computer.Keyboard.CtrlKeyDown And My.Computer.Keyboard.ShiftKeyDown Then
                .DataPoint(MyCurrentFrame).DxDypxl = .DataPoint(MyCurrentFrame - _i).DxDypxl.CreateCopy(e.Delta / 60)
                .DataPoint(MyCurrentFrame).DxDy = .DataPoint(MyCurrentFrame).DxDypxl.CreateCopy
                MyRecord.RoadUsersChanged = True
                Call Display(MyCurrentFrame)
              ElseIf My.Computer.Keyboard.CtrlKeyDown Then
                .DataPoint(MyCurrentFrame).Xpxl = .DataPoint(MyCurrentFrame - _i).Xpxl + e.Delta / 1200 * .DataPoint(MyCurrentFrame - _i).Dxpxl '=0.1 m
                .DataPoint(MyCurrentFrame).Ypxl = .DataPoint(MyCurrentFrame - _i).Ypxl + e.Delta / 1200 * .DataPoint(MyCurrentFrame - _i).Dypxl '=0.1 m
                .DataPoint(MyCurrentFrame).DxDypxl = .DataPoint(MyCurrentFrame - _i).DxDypxl.CreateCopy
                .DataPoint(MyCurrentFrame).X = .DataPoint(MyCurrentFrame).Xpxl
                .DataPoint(MyCurrentFrame).Y = .DataPoint(MyCurrentFrame).Ypxl
                .DataPoint(MyCurrentFrame).DxDy = .DataPoint(MyCurrentFrame).DxDypxl.CreateCopy
                MyRecord.RoadUsersChanged = True
                Call Display(MyCurrentFrame)
              ElseIf My.Computer.Keyboard.ShiftKeyDown Then
                .DataPoint(MyCurrentFrame).Xpxl = .DataPoint(MyCurrentFrame - _i).Xpxl + e.Delta / 1200 * .DataPoint(MyCurrentFrame - _i).Dypxl '=0.1 m
                .DataPoint(MyCurrentFrame).Ypxl = .DataPoint(MyCurrentFrame - _i).Ypxl - e.Delta / 1200 * .DataPoint(MyCurrentFrame - _i).Dxpxl '=0.1 m
                .DataPoint(MyCurrentFrame).DxDypxl = .DataPoint(MyCurrentFrame - _i).DxDypxl.CreateCopy
                .DataPoint(MyCurrentFrame).X = .DataPoint(MyCurrentFrame).Xpxl
                .DataPoint(MyCurrentFrame).Y = .DataPoint(MyCurrentFrame).Ypxl
                .DataPoint(MyCurrentFrame).DxDy = .DataPoint(MyCurrentFrame).DxDypxl.CreateCopy
                MyRecord.RoadUsersChanged = True
                Call LoadChartData()
                Call Display(MyCurrentFrame)
              End If
            End If
          End With
        End If
      End If
    End If
  End Sub


#End Region



#Region "Control Subs"

  Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
    Dim _FramesToAdd As Integer

    If MyCurrentFrame + MyCurrentSpeedFactor <= MyRecord.FrameCount - 1 Then
      Call Display(MyCurrentFrame + MyCurrentSpeedFactor)
    ElseIf (MyDisplayMode = DisplayModes.Edit) And (MyCurrentFrame > MyRecord.FrameCount - MyCurrentSpeedFactor - 1) Then
      If MsgBox("Do you really want to extend the current event by adding a frame at the end?", vbYesNo) = vbYes Then
        _FramesToAdd = 2 * CInt(MyRecord.FrameRate) * MyCurrentSpeedFactor
        For _j = 1 To _FramesToAdd
          MyRecord.AddFrame()
        Next
        trbFrames.Maximum = MyRecord.FrameCount - 1
        Call Display(MyCurrentFrame)
      End If
    End If
  End Sub

  Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    Dim _FramesToAdd As Integer

    If MyCurrentFrame - MyCurrentSpeedFactor >= 0 Then
      Call Display(MyCurrentFrame - MyCurrentSpeedFactor)
    ElseIf MyDisplayMode = DisplayModes.Edit And MyCurrentFrame < MyCurrentSpeedFactor Then
      If MsgBox("Do you really want to extend the current event by adding frames at the beginning?", vbYesNo) = vbYes Then
        _FramesToAdd = 2 * CInt(MyRecord.FrameRate) * MyCurrentSpeedFactor
        For _j = 1 To _FramesToAdd
          MyRecord.AddStartFrame()
        Next
        trbFrames.Maximum = MyRecord.FrameCount - 1
        Call Display(MyCurrentFrame + _FramesToAdd)
      End If
    End If
  End Sub






  Private Sub picDisplay_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles picDisplay.MouseDoubleClick
    Dim _Perspective As Integer
    Dim _X, _Y As Integer


    Call MyVisualiser.PerspectiveXY(e.X, e.Y, _Perspective, _X, _Y)

    If My.Computer.Keyboard.CtrlKeyDown Then
      Call DisplayMouseDoubleClick(_Perspective, _X, _Y, True)
    Else
      Call DisplayMouseDoubleClick(_Perspective, _X, _Y, False)
    End If

  End Sub

  Private Sub ZoomedViewMouseDoubleClick(ByVal Perspective As Integer, ByVal X As Integer, ByVal Y As Integer) Handles MyZoomedView.ZoomedViewMouseDoubleClick
    If My.Computer.Keyboard.CtrlKeyDown Then
      Call DisplayMouseDoubleClick(Perspective, X, Y, True)
    Else
      Call DisplayMouseDoubleClick(Perspective, X, Y, False)
    End If
    'Call DisplayMouseDoubleClick(Perspective, X, Y)
  End Sub

  Private Sub DisplayMouseDoubleClick(ByVal Perspective As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal TopMiddlePoint As Boolean)
    Dim _PointW As clsGeomPoint3D


    If IsValue(Perspective) And
      (MyDisplayMode = DisplayModes.Edit) And
      (MyRecord.SelectedRoadUser IsNot Nothing) _
    Then
      Select Case Perspective
        Case -1
          _PointW = MyRecord.Map.TransformImageToWorld(X, Y)
        Case Else
          If TopMiddlePoint Then
            _PointW = MyRecord.VideoFiles(Perspective).TransformImageToWorld(X, Y, -MyRecord.SelectedRoadUser.Height, MyRecord.Map)
          Else
            _PointW = MyRecord.VideoFiles(Perspective).TransformImageToWorld(X, Y, 0, MyRecord.Map)
          End If
      End Select

      If _PointW IsNot Nothing Then
        If Not IsBetween(MyCurrentFrame, MyRecord.SelectedRoadUser.FirstFrame, MyRecord.SelectedRoadUser.LastFrame) Then
          MyRecord.SelectedRoadUser.ExtendTrajectory(MyCurrentFrame)
        End If
        MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).Xpxl = _PointW.X
        MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).Ypxl = _PointW.Y
        MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).DxDypxl = MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).DxDy
        MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).X = _PointW.X
        MyRecord.SelectedRoadUser.DataPoint(MyCurrentFrame).Y = _PointW.Y
        Call Display(MyCurrentFrame)
      End If
    End If
  End Sub



  Private Sub picDisplay_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picDisplay.MouseMove
    Dim _Perspective As Integer
    Dim _X, _Y As Integer
    Dim _PointM As clsGeomPoint3D
    Dim _PointPxl As Point
    Dim _OnePixelError As Double


    _PointM = Nothing
    _PointPxl = Nothing
    _OnePixelError = NoValue

    Call MyVisualiser.PerspectiveXY(e.X, e.Y, _Perspective, _X, _Y)

    If IsValue(_Perspective) And IsValue(_X) And IsValue(_Y) Then
      _PointPxl = New Point(_X, _Y)
      Call TransformImageToWorldSinglePerspective(_Perspective, _PointPxl, _PointM, _OnePixelError)
    End If

    RaiseEvent Display_MouseMove(_PointM, _PointPxl, _OnePixelError)
  End Sub




  Private Sub ZoomedViewMouseMove(ByVal Perspective As Integer, ByVal X As Integer, ByVal Y As Integer) Handles MyZoomedView.ZoomedViewMouseMove
    Dim _PointPxl As Point
    Dim _PointM As clsGeomPoint3D
    Dim _OnePixelError As Double


    _PointPxl = Nothing
    _PointM = Nothing
    _OnePixelError = Nothing

    If IsValue(Perspective) And IsValue(X) And IsValue(Y) Then
      If IsBetween(X, 0, MyRecord.VideoFiles(Perspective).FrameSize.Width) And IsBetween(Y, 0, MyRecord.VideoFiles(Perspective).FrameSize.Height) Then
        _PointPxl = New Point(X, Y)
        Call TransformImageToWorldSinglePerspective(Perspective, _PointPxl, _PointM, _OnePixelError)
      End If
    End If

    RaiseEvent Display_MouseMove(_PointM, _PointPxl, _OnePixelError)
  End Sub

  Private Sub TransformImageToWorldSinglePerspective(ByVal Perspective As Integer, ByRef PointPxl As Point, ByRef PointM As clsGeomPoint3D, ByRef OnePixelError As Double)

    Select Case Perspective
      Case -1
        PointM = MyRecord.Map.TransformImageToWorld(PointPxl.X, PointPxl.Y)
        OnePixelError = MyRecord.Map.OnePixelError(PointPxl.X, PointPxl.Y)
      Case Else
        PointM = MyRecord.VideoFiles(Perspective).TransformImageToWorld(PointPxl.X, PointPxl.Y, 0, MyRecord.Map)
        OnePixelError = MyRecord.VideoFiles(Perspective).OnePixelError(PointPxl.X, PointPxl.Y, MyRecord.Map)
    End Select
  End Sub

  Private Sub tmrFrames_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrFrames.Tick
    Dim iStep As Integer
    Dim iTimeToLoad As Integer
    Dim iDelay As Integer
    Dim iLostFrames As Integer = 0
    Dim iFrameExposureTime As Integer
    Dim lngTickNow As Long

    tmrFrames.Enabled = False
    If MyCurrentFrame < MyRecord.FrameCount - 1 Then 'MyCurrentRecord.StartFrame + MyCurrentRecord.FrameCount - 1 Then
      Call Display(MyCurrentFrame) ', MyShowTrajectory, False)
      lngTickNow = MyWatch.ElapsedMilliseconds
      iTimeToLoad = CInt(lngTickNow - MyPreviousTick)
      iFrameExposureTime = CInt(1000 * (MyRecord.GetLocalTime(MyCurrentFrame + 1) - MyRecord.GetLocalTime(MyCurrentFrame)) / MyCurrentSpeedFactor)
      iDelay = iFrameExposureTime - (iTimeToLoad Mod iFrameExposureTime)
      iStep = (iTimeToLoad \ iFrameExposureTime) + 1
      MyCurrentFrame = MyCurrentFrame + iStep
      MyPreviousTick = lngTickNow + iDelay
      tmrFrames.Interval = iDelay
      tmrFrames.Enabled = True
    Else
      chkPlay.Checked = False
      chkPlay.ImageKey = "Play"
      tltToolTip.SetToolTip(chkPlay, "Play")
      trbFrames.Enabled = True
      MyIsPlaying = False
      Call Display(0)
    End If
  End Sub

  Private Sub chkPlay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPlay.CheckedChanged
    If chkPlay.Checked Then
      pnlNavigation.Enabled = False
      btnExport.Enabled = False
      RaiseEvent PlaybackStarted()
      MyWatch.Restart()
      MyPreviousTick = 1
      tmrFrames.Interval = 1
      tmrFrames.Enabled = True
      chkPlay.ImageKey = "Pause"
      tltToolTip.SetToolTip(chkPlay, "Pause")
      trbFrames.Enabled = False
      MyIsPlaying = True
    Else
      btnExport.Enabled = True
      RaiseEvent PlaybackStopped()
      pnlNavigation.Enabled = True
      MyWatch.Stop()
      tmrFrames.Enabled = False
      chkPlay.ImageKey = "Play"
      tltToolTip.SetToolTip(chkPlay, "Play")
      trbFrames.Enabled = True
      MyIsPlaying = False
    End If
  End Sub

  Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
    Call StopPlaying()
  End Sub

  Private Sub trbFrames_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles trbFrames.MouseUp
    Call Display(trbFrames.Value)
  End Sub

  Private Sub trbFrames_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles trbFrames.Scroll
    tltToolTip.SetToolTip(trbFrames, SecondsToTimeDisplay(MyRecord.GetLocalTime(trbFrames.Value)))
  End Sub

  Private Sub lblCurrentFrame_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblCurrentFrame.DoubleClick
    Dim sFrame As String
    Dim iFrame As Integer

    If Not MyIsPlaying Then
      sFrame = InputBox("Move to frame:", "Frame navigation")
      If Integer.TryParse(sFrame, iFrame) Then
        If IsBetween(iFrame, 0, MyRecord.FrameCount - 1) Then
          Call Display(iFrame)
        End If
      End If
    End If
  End Sub

  Private Sub btnSpeedFactor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpeedFactor.Click
    If MyCurrentSpeedFactor = 1 Then
      Call SetSpeedFactor(MyDefaultSpeedFactor)
    Else
      Call SetSpeedFactor(1)
    End If
  End Sub

  Private Sub SetSpeedFactor(ByVal SpeedFactor As Integer)

    MyCurrentSpeedFactor = SpeedFactor
    btnSpeedFactor.Text = "x" & SpeedFactor.ToString
    If SpeedFactor > 1 Then MyDefaultSpeedFactor = SpeedFactor

    If MyDisplayMode = DisplayModes.Edit Then
      If MyCurrentFrame < MyCurrentSpeedFactor Then btnPrevious.Text = "+<"
      If MyCurrentFrame > MyRecord.FrameCount - MyCurrentSpeedFactor Then btnNext.Text = ">+"
    End If

  End Sub

  Private Sub mnuSaveImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveImage.Click

    With digSave
      .InitialDirectory = MyRecord.ExportDirectory & "Images\"
      .FileName = ""
      .Filter = ".PNG|*.png|.JPEG|*.jpg"
      .ShowDialog()
      If .FileName.Length > 0 Then
        Select Case .FilterIndex
          Case 1
            picDisplay.Image.Save(.FileName, Imaging.ImageFormat.Png)
          Case 2
            Call SaveJpgImage(CType(picDisplay.Image, Bitmap), .FileName)
        End Select
      End If
    End With

  End Sub


  Private Sub mnuCopyImage_Click(sender As Object, e As EventArgs) Handles mnuCopyImage.Click
    Clipboard.SetImage(picDisplay.Image)
  End Sub

#End Region





  Private Sub btnJumpForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJumpForward.Click
    Dim _JumpFrames As Integer

    _JumpFrames = 2 * CInt(MyRecord.FrameRate) * MyCurrentSpeedFactor
    If IsBetween(MyCurrentFrame + _JumpFrames, 0, MyRecord.FrameCount - 1) Then
      Call Display(MyCurrentFrame + _JumpFrames)
    End If
  End Sub

  Private Sub bntJumpBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntJumpBack.Click
    Dim _JumpFrames As Integer

    _JumpFrames = 2 * CInt(MyRecord.FrameRate) * MyCurrentSpeedFactor
    If IsBetween(MyCurrentFrame - _JumpFrames, 0, MyRecord.FrameCount - 1) Then
      Call Display(MyCurrentFrame - _JumpFrames)
    End If
  End Sub

  Private Sub mnuSaveVideoSequence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveVideoSequence.Click
    Dim _VideoFilePath As String
    Dim _Process As Process
    Dim _ExportParameters As frmExportVideoParameters


    _VideoFilePath = MyRecord.ExportDirectory & "Video\" & Format(MyRecord.ID, "00000000") & ".avi"
    _ExportParameters = New frmExportVideoParameters
    _ExportParameters.ShowDialog(Me)

    If _ExportParameters.OK Then
      Me.ParentForm.Cursor = Cursors.WaitCursor
      If MyVisualiser.SaveVideoSequence(_VideoFilePath, 0, MyRecord.FrameCount - 1, MyCurrentSpeedFactor, _ExportParameters.KBS) Then
        _Process = New Process
        _Process.StartInfo.FileName = _VideoFilePath
        _Process.Start()
      Else
        MsgBox("Something went wrong. Sorry :-(")
      End If
      Me.ParentForm.Cursor = Cursors.Default
    End If
  End Sub

  Private Sub TransformImagePointsToWorldXYZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _InputFile As String
    Dim _OutputFile As String

    With digOpen
      .Title = "Select file with points to transform"
      .FileName = ""
      .Filter = "Text file (*.txt)|*.txt"
      .ShowDialog()
      If .FileName.Length > 0 Then
        _InputFile = .FileName
        _OutputFile = _InputFile.Substring(0, _InputFile.Length - 4) & "_transformed.txt"
        'MyVisualiser.TransformImagePointsToWorld(_InputFile, _OutputFile, SelectedDisplayIndex)
      End If
    End With
  End Sub


  Private Sub TransformWorldXYZToImagePoints_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _InputFile As String
    Dim _OutputFile As String

    With digOpen
      .Title = "Select file with points to transform"
      .FileName = ""
      .Filter = "Text file (*.txt)|*.txt"
      .ShowDialog()
      If .FileName.Length > 0 Then
        _InputFile = .FileName
        _OutputFile = _InputFile.Substring(0, _InputFile.Length - 4) & "_transformed.txt"
      End If
    End With

  End Sub


  Private Sub btnCutStart_Click(sender As Object, e As EventArgs) Handles btnCutStart.Click
    Dim _RoadUser As clsRoadUser

    _RoadUser = MyRecord.SelectedRoadUser
    For i = 1 To MyCurrentFrame - _RoadUser.FirstFrame
      _RoadUser.RemoveFirstPoint()
    Next
    MyRecord.RoadUsersChanged = True
    Call LoadChartData()
    Call Display(MyCurrentFrame)
  End Sub

  Private Sub btnCutEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCutEnd.Click
    Dim _RoadUser As clsRoadUser

    _RoadUser = MyRecord.SelectedRoadUser
    For i = 1 To _RoadUser.LastFrame - MyCurrentFrame
      _RoadUser.RemoveLastPoint()
    Next
    MyRecord.RoadUsersChanged = True
    Call LoadChartData()
    Call Display(MyCurrentFrame)
  End Sub


  Private Sub chkGraph_CheckedChanged(sender As Object, e As EventArgs) Handles chkGraph.CheckedChanged
    Call Display(MyCurrentFrame)
  End Sub


  Private Sub LoadChartDesign()
    MyDataGrid.Columns.Clear()
    MyDataGrid.RowHeadersVisible = False
    MyDataGrid.AllowUserToAddRows = False
    MyDataGrid.AllowUserToResizeColumns = False
    MyDataGrid.AllowUserToResizeRows = False
    MyDataGrid.AllowUserToDeleteRows = False
    MyDataGrid.AllowUserToOrderColumns = False
    MyDataGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    MyDataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
    MyDataGrid.ReadOnly = True

    MyChart.ChartAreas.Clear()
    MyChart.Series.Clear()
    MyChart.Titles.Clear()

    Select Case MyDisplayMode
      Case DisplayModes.Edit
        With MyDataGrid
          .Columns.Add("cmFrame", "Frame")
          .Columns("cmFrame").Width = 40
          .Columns("cmFrame").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmFrame").Resizable = DataGridViewTriState.False

          .Columns.Add("cmX", "X")
          .Columns("cmX").Width = 40
          .Columns("cmX").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmX").Resizable = DataGridViewTriState.False

          .Columns.Add("cmY", "Y")
          .Columns("cmY").Width = 40
          .Columns("cmY").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmY").Resizable = DataGridViewTriState.False

          .Columns.Add("cmV", "V")
          .Columns("cmV").Width = 40
          .Columns("cmV").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmV").Resizable = DataGridViewTriState.False

          .Columns.Add("cmA", "A")
          .Columns("cmA").Width = 40
          .Columns("cmA").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmA").Resizable = DataGridViewTriState.False

          .Columns.Add("cmXpxl", "Xpxl")
          .Columns("cmXpxl").Width = 40
          .Columns("cmXpxl").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmXpxl").Resizable = DataGridViewTriState.False

          .Columns.Add("cmYpxl", "Ypxl")
          .Columns("cmYpxl").Width = 40
          .Columns("cmYpxl").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmYpxl").Resizable = DataGridViewTriState.False
        End With




        MyChart.ChartAreas.Add(New ChartArea("XY"))
        With MyChart.ChartAreas("XY")
          .Position = New ElementPosition(0, 0, 50, 100)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .IsSameFontSizeForAllAxes = True
          '.AxisX.Title = "X, m"
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          .AxisX.Interval = 5
          .AxisX.MajorGrid.Interval = 5
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 1
          .AxisX.MinorGrid.LineColor = Color.LightGray
          '.AxisY.Title = "Y, m"
          .AxisY.IsReversed = True
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          .AxisY.Interval = 5
          .AxisY.MajorGrid.Interval = 5
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 1
          .AxisY.MinorGrid.LineColor = Color.LightGray
        End With

        MyChart.ChartAreas.Add(New ChartArea("V"))
        With MyChart.ChartAreas("V")
          .Position = New ElementPosition(50, 3, 50, 47)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .IsSameFontSizeForAllAxes = True
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          ' .AxisX.Title = "Frame"
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Crossing = 0
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          ' .AxisY.Title = "V, m/s"
          .AxisY.Interval = 4
          .AxisY.MajorGrid.Interval = 4
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 1
          .AxisY.MinorGrid.LineColor = Color.LightGray
          MyChart.ChartAreas("V").AxisY.Minimum = 0
          MyChart.ChartAreas("V").AxisY.Maximum = 24
          .InnerPlotPosition = New ElementPosition(10, 5, 85, 90)
        End With


        MyChart.ChartAreas.Add(New ChartArea("A"))
        With MyChart.ChartAreas("A")
          .Position = New ElementPosition(50, 53, 50, 47)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .IsSameFontSizeForAllAxes = True
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          ' .AxisX.Title = "Frame"
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Crossing = 0
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          ' .AxisY.Title = "A, m/s"
          .AxisY.Interval = 4
          .AxisY.MajorGrid.Interval = 4
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 1
          .AxisY.MinorGrid.LineColor = Color.LightGray
          .AxisY.Minimum = -8
          .AxisY.Maximum = 8
          .InnerPlotPosition = New ElementPosition(10, 5, 85, 90)
        End With

        With MyChart.Titles.Add("Speed, m/s")
          .Position = New ElementPosition(50, 2, 50, 3)
          .Font = New Font("Times New Roman", 11, FontStyle.Bold)
        End With

        With MyChart.Titles.Add("Acceleration, m/s2")
          .Position = New ElementPosition(50, 52, 50, 3)
          .Font = New Font("Times New Roman", 11, FontStyle.Bold)
        End With

        With MyChart.Series.Add("XYpxl")
          .ChartArea = "XY"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Red
        End With
        With MyChart.Series.Add("XYpxlCurrent")
          .ChartArea = "XY"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Red
        End With
        With MyChart.Series.Add("XY")
          .ChartArea = "XY"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Black
        End With
        With MyChart.Series.Add("XYCurrent")
          .ChartArea = "XY"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Black
        End With
        With MyChart.Series.Add("V")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.DarkGreen
        End With
        With MyChart.Series.Add("Vcurrent")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.DarkGreen
        End With
        With MyChart.Series.Add("A")
          .ChartArea = "A"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Red
        End With
        With MyChart.Series.Add("Acurrent")
          .ChartArea = "A"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Red
        End With






      Case DisplayModes.View
        With MyDataGrid
          .Columns.Add("cmFrame", "Frame")
          .Columns("cmFrame").Width = 40
          .Columns("cmFrame").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmFrame").Resizable = DataGridViewTriState.False

          .Columns.Add("cmV1", "V1")
          .Columns("cmV1").Width = 40
          .Columns("cmV1").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmV1").Resizable = DataGridViewTriState.False
          .Columns("cmV1").CellTemplate.Style.ForeColor = MyVisualiserOptions.Key1RUColour

          .Columns.Add("cmV2", "V2")
          .Columns("cmV2").Width = 40
          .Columns("cmV2").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmV2").Resizable = DataGridViewTriState.False
          .Columns("cmV2").CellTemplate.Style.ForeColor = MyVisualiserOptions.Key2RUColour

          .Columns.Add("cmVrel", "Vrel")
          .Columns("cmVrel").Width = 40
          .Columns("cmVrel").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmVrel").Resizable = DataGridViewTriState.False
          .Columns("cmVrel").CellTemplate.Style.ForeColor = MyVisualiserOptions.LineColourRelativeSpeed

          .Columns.Add("cmTTC", "TTC")
          .Columns("cmTTC").Width = 40
          .Columns("cmTTC").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmTTC").Resizable = DataGridViewTriState.False

          .Columns.Add("cmT2", "T2")
          .Columns("cmT2").Width = 40
          .Columns("cmT2").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmT2").Resizable = DataGridViewTriState.False

          .Columns.Add("cmTA", "TAdv")
          .Columns("cmTA").Width = 40
          .Columns("cmTA").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmTA").Resizable = DataGridViewTriState.False

          .Columns.Add("cmDeltaV0", "dV0")
          .Columns("cmDeltaV0").Width = 40
          .Columns("cmDeltaV0").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmDeltaV0").Resizable = DataGridViewTriState.False

          .Columns.Add("cmDeltaV4", "dV4")
          .Columns("cmDeltaV4").Width = 40
          .Columns("cmDeltaV4").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmDeltaV4").Resizable = DataGridViewTriState.False

          .Columns.Add("cmDeltaV6", "dV6")
          .Columns("cmDeltaV6").Width = 40
          .Columns("cmDeltaV6").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmDeltaV6").Resizable = DataGridViewTriState.False

          .Columns.Add("cmDeltaV8", "dV8")
          .Columns("cmDeltaV8").Width = 40
          .Columns("cmDeltaV8").SortMode = DataGridViewColumnSortMode.NotSortable
          .Columns("cmDeltaV8").Resizable = DataGridViewTriState.False
        End With



        MyChart.ChartAreas.Add(New ChartArea("V"))
        With MyChart.ChartAreas("V")
          .Position = New ElementPosition(0, 0, 50, 50)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          '.AxisX.Title = "Frame"
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Crossing = 0
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          .AxisY.Title = "V, m/s"
          .AxisY.Interval = 4
          .AxisY.MajorGrid.Interval = 4
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 1
          .AxisY.MinorGrid.LineColor = Color.LightGray
          .AxisY.Minimum = 0
          .AxisY.Maximum = 28
        End With

        MyChart.ChartAreas.Add(New ChartArea("dV"))
        With MyChart.ChartAreas("dV")
          .Position = New ElementPosition(0, 50, 50, 50)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          .AxisX.Title = "Frame"
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Crossing = 0
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          .AxisY.Title = "delta V, m/s"
          .AxisY.Interval = 4
          .AxisY.MajorGrid.Interval = 4
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 1
          .AxisY.MinorGrid.LineColor = Color.LightGray
          .AxisY.Minimum = 0
          .AxisY.Maximum = 28
        End With

        MyChart.ChartAreas.Add(New ChartArea("TTC"))
        With MyChart.ChartAreas("TTC")
          .Position = New ElementPosition(50, 0, 50, 50)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          '.AxisX.Title = "Frame"
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Title = "TTC / T2, s."
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          .AxisY.Interval = 1
          .AxisY.MajorGrid.Interval = 1
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 0.5
          .AxisY.MinorGrid.LineColor = Color.LightGray
          .AxisY.Minimum = 0
          .AxisY.Maximum = 10
        End With

        MyChart.ChartAreas.Add(New ChartArea("TAdv"))
        With MyChart.ChartAreas("TAdv")
          .Position = New ElementPosition(50, 50, 50, 50)
          .BorderColor = Color.Gray
          .CursorX.IsUserSelectionEnabled = True
          .CursorY.IsUserSelectionEnabled = True
          .AxisX.LineColor = Color.Black
          .AxisX.LineWidth = 1
          .AxisX.Title = "Frame"
          .AxisX.Interval = 10
          .AxisX.MajorGrid.Interval = 10
          .AxisX.MajorGrid.LineColor = Color.Gray
          .AxisX.MinorGrid.Enabled = True
          .AxisX.MinorGrid.Interval = 2
          .AxisX.MinorGrid.LineColor = Color.LightGray
          .AxisY.Crossing = 0
          .AxisY.LineColor = Color.Black
          .AxisY.LineWidth = 1
          .AxisY.Title = "TAdv / PET, s."
          .AxisY.Interval = 1
          .AxisY.MajorGrid.Interval = 1
          .AxisY.MajorGrid.LineColor = Color.Gray
          .AxisY.MinorGrid.Enabled = True
          .AxisY.MinorGrid.Interval = 0.5
          .AxisY.MinorGrid.LineColor = Color.LightGray
          .AxisY.Minimum = 0
          .AxisY.Maximum = 10
        End With

        With MyChart.Series.Add("V1")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("V1current")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("V2")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With
        With MyChart.Series.Add("V2current")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With
        With MyChart.Series.Add("Vrel")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.LineColourRelativeSpeed
        End With
        With MyChart.Series.Add("Vrelcurrent")
          .ChartArea = "V"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = MyVisualiser.VisualiserOptions.LineColourRelativeSpeed
        End With
        With MyChart.Series.Add("dV01")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("dV02")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With
        With MyChart.Series.Add("dV41")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("dV42")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With
        With MyChart.Series.Add("dV61")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("dV62")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With
        With MyChart.Series.Add("dV81")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key1RUColour
        End With
        With MyChart.Series.Add("dV82")
          .ChartArea = "dV"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = MyVisualiser.VisualiserOptions.Key2RUColour
        End With

        'With MyChart.Series.Add("dV1current")
        '  .ChartArea = "dV"
        '  .ChartType = SeriesChartType.Point
        '  .MarkerStyle = MarkerStyle.Star6
        '  .MarkerSize = 12
        '  .Color = MyVisualiser.VisualiserOptions.LineColourRoadUserKey1
        'End With
        'With MyChart.Series.Add("dV2current")
        '  .ChartArea = "dV"
        '  .ChartType = SeriesChartType.Point
        '  .MarkerStyle = MarkerStyle.Star6
        '  .MarkerSize = 12
        '  .Color = MyVisualiser.VisualiserOptions.LineColourRoadUserKey2
        'End With
        With MyChart.Series.Add("TTC")
          .ChartArea = "TTC"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Red
        End With
        With MyChart.Series.Add("TTCcurrent")
          .ChartArea = "TTC"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Red
        End With
        With MyChart.Series.Add("T2")
          .ChartArea = "TTC"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Black
        End With
        With MyChart.Series.Add("T2current")
          .ChartArea = "TTC"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Black
        End With
        With MyChart.Series.Add("TAdv")
          .ChartArea = "TAdv"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Circle
          .MarkerSize = 3
          .Color = Color.Black
        End With
        With MyChart.Series.Add("TAdvCurrent")
          .ChartArea = "TAdv"
          .ChartType = SeriesChartType.Point
          .MarkerStyle = MarkerStyle.Star6
          .MarkerSize = 12
          .Color = Color.Black
        End With
    End Select

  End Sub


  Public Sub LoadChartData()
    Dim _RowIndex As Integer
    Dim _AxesRange As Integer


    For Each _Serie In MyChart.Series
      _Serie.Points.Clear()
    Next
    MyDataGrid.Rows.Clear()


    Select Case MyDisplayMode
      Case DisplayModes.Edit
        If MyRecord.SelectedRoadUser IsNot Nothing Then
          With MyRecord.SelectedRoadUser
            MyDataGrid.Rows.Add(.LastFrame - .FirstFrame + 1)


            'MyChart.ChartAreas("XY").AxisX.Minimum = Int(.MinX / 10) * 10
            'MyChart.ChartAreas("XY").AxisX.Maximum = Int(.MaxX / 10 + 1) * 10
            'MyChart.ChartAreas("XY").AxisY.Minimum = Int(.MinY / 10) * 10
            'MyChart.ChartAreas("XY").AxisY.Maximum = Int(.MaxY / 10 + 1) * 10

            _AxesRange = CInt(Math.Max(Int(.MaxX / 10 + 1) * 10 - Int(.MinX / 10) * 10, Int(.MaxY / 10 + 1) * 10 - Int(.MinY / 10) * 10))
            MyChart.ChartAreas("XY").AxisX.Minimum = CInt(((Int(.MinX / 10) * 10 + Int(.MaxX / 10 + 1) * 10) - _AxesRange) / 2)
            MyChart.ChartAreas("XY").AxisY.Minimum = CInt(((Int(.MinY / 10) * 10 + Int(.MaxY / 10 + 1) * 10) - _AxesRange) / 2)
            MyChart.ChartAreas("XY").AxisX.Maximum = MyChart.ChartAreas("XY").AxisX.Minimum + _AxesRange
            MyChart.ChartAreas("XY").AxisY.Maximum = MyChart.ChartAreas("XY").AxisY.Minimum + _AxesRange

            MyChart.ChartAreas("V").AxisX.Minimum = Int(.FirstFrame / 10) * 10
            MyChart.ChartAreas("V").AxisX.Maximum = Int(.LastFrame / 10 + 1) * 10
            MyChart.ChartAreas("A").AxisX.Minimum = Int(.FirstFrame / 10) * 10
            MyChart.ChartAreas("A").AxisX.Maximum = Int(.LastFrame / 10 + 1) * 10


            MyChart.ChartAreas("XY").InnerPlotPosition = New ElementPosition(10,
                                                                             10,
                                                                             CInt(Math.Min(MyChart.Width / 2, MyChart.Height) / MyChart.Width * 2 * 90),
                                                                             CInt(Math.Min(MyChart.Width / 2, MyChart.Height) / MyChart.Height * 90))



            For _Frame = .FirstFrame To .LastFrame
              _RowIndex = _Frame - .FirstFrame
              MyDataGrid.Rows(_RowIndex).Height = 18
              MyDataGrid.Rows(_RowIndex).Cells("cmFrame").Value = _Frame.ToString

              If .DataPoint(_Frame).OriginalXYExists Then
                MyDataGrid.Rows(_RowIndex).Cells("cmXpxl").Value = FormatNumber(.DataPoint(_Frame).Xpxl, 1)
                MyDataGrid.Rows(_RowIndex).Cells("cmYpxl").Value = FormatNumber(.DataPoint(_Frame).Ypxl, 1)
                MyChart.Series("XYpxl").Points.AddXY(.DataPoint(_Frame).Xpxl, .DataPoint(_Frame).Ypxl)
              End If

              MyDataGrid.Rows(_RowIndex).Cells("cmX").Value = FormatNumber(.DataPoint(_Frame).X, 1)
              MyDataGrid.Rows(_RowIndex).Cells("cmY").Value = FormatNumber(.DataPoint(_Frame).Y, 1)
              MyChart.Series("XY").Points.AddXY(.DataPoint(_Frame).X, .DataPoint(_Frame).Y)

              If .DataPoint(_Frame).VExists Then
                MyDataGrid.Rows(_RowIndex).Cells("cmV").Value = FormatNumber(.DataPoint(_Frame).V, 1)
                MyChart.Series("V").Points.AddXY(_Frame, .DataPoint(_Frame).V)
              End If

              If .DataPoint(_Frame).AExists Then
                MyDataGrid.Rows(_RowIndex).Cells("cmA").Value = FormatNumber(.DataPoint(_Frame).A, 1)
                MyChart.Series("A").Points.AddXY(_Frame, .DataPoint(_Frame).A)
              End If
            Next
          End With
        End If







      Case DisplayModes.View
        MyIndicatorsFirstFrame = NoValue
        MyIndicatorsLastFrame = NoValue

        For _Frame = 0 To MyRecord.FrameCount - 1
          If MyRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
            If IsNotValue(MyIndicatorsFirstFrame) Then
              MyIndicatorsFirstFrame = _Frame
            End If
            MyIndicatorsLastFrame = _Frame
          End If
        Next
        If IsNotValue(MyIndicatorsFirstFrame) Or IsNotValue(MyIndicatorsLastFrame) Then Exit Sub


        'Fill table with data
        MyDataGrid.Rows.Add(MyIndicatorsLastFrame - MyIndicatorsFirstFrame + 1)
        For _Frame = MyIndicatorsFirstFrame To MyIndicatorsLastFrame
          _RowIndex = _Frame - MyIndicatorsFirstFrame
          With MyDataGrid.Rows(_RowIndex)
            .Height = 18
            .Cells("cmFrame").Value = _Frame.ToString
            If MyRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.V1) Then _
                .Cells("cmV1").Value = FormatNumber(MyRecord.TimeLine(_Frame).Indicators.V1, 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.V2) Then _
                .Cells("cmV2").Value = FormatNumber(MyRecord.TimeLine(_Frame).Indicators.V2, 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.Vrel) Then _
                .Cells("cmVrel").Value = FormatNumber(MyRecord.TimeLine(_Frame).Indicators.Vrel, 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV01) And IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV02) Then _
                .Cells("cmDeltaV0").Value = FormatNumber(Math.Max(MyRecord.TimeLine(_Frame).Indicators.DeltaV01, MyRecord.TimeLine(_Frame).Indicators.DeltaV02), 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV41) And IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV42) Then _
                .Cells("cmDeltaV4").Value = FormatNumber(Math.Max(MyRecord.TimeLine(_Frame).Indicators.DeltaV41, MyRecord.TimeLine(_Frame).Indicators.DeltaV42), 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV61) And IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV62) Then _
                .Cells("cmDeltaV6").Value = FormatNumber(Math.Max(MyRecord.TimeLine(_Frame).Indicators.DeltaV61, MyRecord.TimeLine(_Frame).Indicators.DeltaV62), 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV81) And IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV82) Then _
                .Cells("cmDeltaV8").Value = FormatNumber(Math.Max(MyRecord.TimeLine(_Frame).Indicators.DeltaV81, MyRecord.TimeLine(_Frame).Indicators.DeltaV82), 1)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.TTC) Then _
                .Cells("cmTTC").Value = FormatNumber(MyRecord.TimeLine(_Frame).Indicators.TTC, 2)
              If IsValue(MyRecord.TimeLine(_Frame).Indicators.T1) And IsValue(MyRecord.TimeLine(_Frame).Indicators.T2) Then
                .Cells("cmTA").Value = FormatNumber(Math.Abs(MyRecord.TimeLine(_Frame).Indicators.T1 - MyRecord.TimeLine(_Frame).Indicators.T2), 2)
                .Cells("cmT2").Value = FormatNumber(Math.Max(MyRecord.TimeLine(_Frame).Indicators.T1, MyRecord.TimeLine(_Frame).Indicators.T2), 2)
              ElseIf IsValue(MyRecord.TimeLine(_Frame).Indicators.PET) Then
                .Cells("cmTA").Value = FormatNumber(MyRecord.TimeLine(_Frame).Indicators.PET, 2)
              End If
            End If
          End With
        Next


        'Fill charts with data
        MyChart.ChartAreas("V").AxisX.Minimum = Int(MyIndicatorsFirstFrame / 10) * 10
        MyChart.ChartAreas("V").AxisX.Maximum = Int(MyIndicatorsLastFrame / 10 + 1) * 10

        MyChart.ChartAreas("dV").AxisX.Minimum = Int(MyIndicatorsFirstFrame / 10) * 10
        MyChart.ChartAreas("dV").AxisX.Maximum = Int(MyIndicatorsLastFrame / 10 + 1) * 10

        MyChart.ChartAreas("TTC").AxisX.Minimum = Int(MyIndicatorsFirstFrame / 10) * 10
        MyChart.ChartAreas("TTC").AxisX.Maximum = Int(MyIndicatorsLastFrame / 10 + 1) * 10

        MyChart.ChartAreas("TAdv").AxisX.Minimum = Int(MyIndicatorsFirstFrame / 10) * 10
        MyChart.ChartAreas("TAdv").AxisX.Maximum = Int(MyIndicatorsLastFrame / 10 + 1) * 10


        For _Frame = MyIndicatorsFirstFrame To MyIndicatorsLastFrame
          If MyRecord.TimeLine(_Frame).Indicators IsNot Nothing Then
            MyChart.Series("V1").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.V1)
            MyChart.Series("V2").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.V2)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.Vrel) Then MyChart.Series("Vrel").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.Vrel)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV01) Then MyChart.Series("dV01").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV01)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV02) Then MyChart.Series("dV02").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV02)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV41) Then MyChart.Series("dV41").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV41)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV42) Then MyChart.Series("dV42").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV42)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV61) Then MyChart.Series("dV61").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV61)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV62) Then MyChart.Series("dV62").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV62)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV81) Then MyChart.Series("dV81").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV81)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.DeltaV82) Then MyChart.Series("dV82").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.DeltaV82)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.TTC) Then MyChart.Series("TTC").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.TTC)
            If IsValue(MyRecord.TimeLine(_Frame).Indicators.T1) And IsValue(MyRecord.TimeLine(_Frame).Indicators.T2) Then
              MyChart.Series("TAdv").Points.AddXY(_Frame, Math.Abs(MyRecord.TimeLine(_Frame).Indicators.T1 - MyRecord.TimeLine(_Frame).Indicators.T2))
              MyChart.Series("T2").Points.AddXY(_Frame, Math.Max(MyRecord.TimeLine(_Frame).Indicators.T1, MyRecord.TimeLine(_Frame).Indicators.T2))
            ElseIf IsValue(MyRecord.TimeLine(_Frame).Indicators.PET) Then
              MyChart.Series("TAdv").Points.AddXY(_Frame, MyRecord.TimeLine(_Frame).Indicators.PET)
            End If
          End If
        Next
    End Select
  End Sub

  Private Sub LoadChartCurrentFrame()

    Select Case MyDisplayMode
      Case DisplayModes.Edit
        If MyRecord.SelectedRoadUser IsNot Nothing Then
          With MyRecord.SelectedRoadUser
            For _RowIndex = 0 To MyDataGrid.Rows.Count - 1
              If _RowIndex = MyCurrentFrame - .FirstFrame Then
                MyDataGrid.Rows(_RowIndex).Selected = True
              Else
                MyDataGrid.Rows(_RowIndex).Selected = False
              End If
            Next

            MyChart.Series("XYCurrent").Points.Clear()
            MyChart.Series("XYpxlCurrent").Points.Clear()
            MyChart.Series("Vcurrent").Points.Clear()
            MyChart.Series("Acurrent").Points.Clear()

            If IsBetween(MyCurrentFrame, .FirstFrame, .LastFrame) Then
              If .DataPoint(MyCurrentFrame).OriginalXYExists Then _
                MyChart.Series("XYpxlCurrent").Points.AddXY(.DataPoint(MyCurrentFrame).Xpxl, .DataPoint(MyCurrentFrame).Ypxl)
              MyChart.Series("XYCurrent").Points.AddXY(.DataPoint(MyCurrentFrame).X, .DataPoint(MyCurrentFrame).Y)
              If .DataPoint(MyCurrentFrame).VExists Then _
                MyChart.Series("Vcurrent").Points.AddXY(MyCurrentFrame, .DataPoint(MyCurrentFrame).V)
              If .DataPoint(MyCurrentFrame).AExists Then _
                MyChart.Series("Acurrent").Points.AddXY(MyCurrentFrame, .DataPoint(MyCurrentFrame).A)
            End If
          End With
        End If



      Case DisplayModes.View

        For _RowIndex = 0 To MyDataGrid.Rows.Count - 1
          If _RowIndex = MyCurrentFrame - MyIndicatorsFirstFrame Then
            MyDataGrid.Rows(_RowIndex).Selected = True
          Else
            MyDataGrid.Rows(_RowIndex).Selected = False
          End If
        Next

        MyChart.Series("V1current").Points.Clear()
        MyChart.Series("V2current").Points.Clear()
        MyChart.Series("Vrelcurrent").Points.Clear()
        '  MyChart.Series("dV1current").Points.Clear()
        '  MyChart.Series("dV2current").Points.Clear()
        MyChart.Series("TTCcurrent").Points.Clear()
        MyChart.Series("T2current").Points.Clear()
        MyChart.Series("TAdvCurrent").Points.Clear()

        If MyRecord.TimeLine(MyCurrentFrame).Indicators IsNot Nothing Then
          MyChart.Series("V1current").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.V1)
          MyChart.Series("V2current").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.V2)
          MyChart.Series("Vrelcurrent").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.Vrel)
          '   MyChart.Series("dV1current").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.DeltaV1)
          '  MyChart.Series("dV2current").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.DeltaV2)
          If IsValue(MyRecord.TimeLine(MyCurrentFrame).Indicators.TTC) Then _
              MyChart.Series("TTCcurrent").Points.AddXY(MyCurrentFrame, MyRecord.TimeLine(MyCurrentFrame).Indicators.TTC)
          If IsValue(MyRecord.TimeLine(MyCurrentFrame).Indicators.T1) And IsValue(MyRecord.TimeLine(MyCurrentFrame).Indicators.T2) Then _
              MyChart.Series("TAdvCurrent").Points.AddXY(MyCurrentFrame, Math.Abs(MyRecord.TimeLine(MyCurrentFrame).Indicators.T1 - MyRecord.TimeLine(MyCurrentFrame).Indicators.T2))
          If IsValue(MyRecord.TimeLine(MyCurrentFrame).Indicators.T1) And IsValue(MyRecord.TimeLine(MyCurrentFrame).Indicators.T2) Then _
               MyChart.Series("T2current").Points.AddXY(MyCurrentFrame, Math.Max(MyRecord.TimeLine(MyCurrentFrame).Indicators.T1, MyRecord.TimeLine(MyCurrentFrame).Indicators.T2))
        End If
    End Select

  End Sub




  Private Sub MyChart_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyChart.MouseDoubleClick
    Dim _a As HitTestResult
    Dim _HitChartArea As ChartArea

    _a = MyChart.HitTest(e.X, e.Y)
    _HitChartArea = _a.ChartArea

    If _HitChartArea IsNot Nothing Then
      _HitChartArea.AxisX.ScaleView.ZoomReset()
      _HitChartArea.AxisY.ScaleView.ZoomReset()
    End If
  End Sub


  Private Sub btnSmooth_Click(sender As Object, e As EventArgs) Handles btnSmooth.Click

    Me.Parent.Cursor = Cursors.WaitCursor
    If MyRecord.SelectedRoadUser IsNot Nothing Then
      MyRecord.SelectedRoadUser.SmoothTrajectory()
      Call LoadChartData()
      Call Display(MyCurrentFrame)
    End If
    Me.Parent.Cursor = Cursors.Default
  End Sub


  Private Sub btnSmoothStraight_Click(sender As Object, e As EventArgs) Handles btnSmoothStraight.Click

    Me.Parent.Cursor = Cursors.WaitCursor
    If MyRecord.SelectedRoadUser IsNot Nothing Then
      MyRecord.SelectedRoadUser.SmoothTrajectory(, , , , 20)
      Call LoadChartData()
      Call Display(MyCurrentFrame)
    End If
    Me.Parent.Cursor = Cursors.Default
  End Sub


    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click



        RaiseEvent ExportDetection(Math.Max(0, MyCurrentFrame - CInt(_LengthBefore * MyRecord.FrameRate)), Math.Min(MyCurrentFrame + CInt(10 * MyRecord.FrameRate) - 1, MyRecord.FrameCount - 1))
    End Sub

    Private Sub btnExport_Click_double(sender As Object, e As EventArgs) Handles btnExport.DoubleClick

        Dim _LengthBefore As Integer

        _LengthBefore = 5
        _LengthBefore = CInt(InputBox("How many seconds BEFORE? (always 10 seconfs AFTER) "))

        RaiseEvent ExportDetection(Math.Max(0, MyCurrentFrame - CInt(_LengthBefore * MyRecord.FrameRate)), Math.Min(MyCurrentFrame + CInt(10 * MyRecord.FrameRate) - 1, MyRecord.FrameCount - 1))
    End Sub




    Private Sub mnuExportAsGroundTruth_Click(sender As Object, e As EventArgs) Handles mnuExportAsGroundTruth.Click
    Me.Parent.Cursor = Cursors.WaitCursor
    MyVisualiser.ExportDetectionAsGroundTruth()
    Me.Parent.Cursor = Cursors.Default
    MsgBox("Export completed!")

    Process.Start("explorer.exe", MyRecord.ExportDirectory & "Ground truth\")
  End Sub


  Private Sub mnuSpeedFactor_x1_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x1.Click
    Call SetSpeedFactor(1)
  End Sub

  Private Sub mnuSpeedFactor_x2_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x2.Click
    Call SetSpeedFactor(2)
  End Sub

  Private Sub mnuSpeedFactor_x3_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x3.Click
    Call SetSpeedFactor(3)
  End Sub

  Private Sub mnuSpeedFactor_x4_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x4.Click
    Call SetSpeedFactor(4)
  End Sub

  Private Sub mnuSpeedFactor_x5_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x5.Click
    Call SetSpeedFactor(5)
  End Sub

  Private Sub mnuSpeedFactor_x6_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x6.Click
    Call SetSpeedFactor(6)
  End Sub

  Private Sub mnuSpeedFactor_x7_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x7.Click
    Call SetSpeedFactor(7)
  End Sub

  Private Sub mnuSpeedFactor_x8_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x8.Click
    Call SetSpeedFactor(8)
  End Sub

  Private Sub mnuSpeedFactor_x9_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x9.Click
    Call SetSpeedFactor(9)
  End Sub

  Private Sub mnuContext_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles mnuContext.Opening

  End Sub


    Private Sub mnuSpeedFactor_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles mnuSpeedFactor.Opening

    End Sub

    Private Sub ToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem5.Click
        _LengthBefore = 5
    End Sub

    Private Sub ToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem6.Click
        _LengthBefore = 10
    End Sub

    Private Sub ToolStripMenuItem7_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem7.Click
        _LengthBefore = 15
    End Sub

    Private Sub ToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem8.Click
        _LengthBefore = 20
    End Sub

    Private Sub ToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem9.Click
        _LengthBefore = 30
    End Sub

    Private Sub ToolStripMenuItem10_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem10.Click
        _LengthBefore = 40
    End Sub

    Private Sub ExportClickedPositionsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportClickedPositionsToolStripMenuItem.Click
        Me.Parent.Cursor = Cursors.WaitCursor
        MyVisualiser.ExportClickedPositions()
        Me.Parent.Cursor = Cursors.Default
        MsgBox("Export completed!")
    End Sub


    Private Sub mnuSpeedFactor_x10_Click(sender As Object, e As EventArgs) Handles mnuSpeedFactor_x10.Click
    Call SetSpeedFactor(10)
  End Sub



End Class
