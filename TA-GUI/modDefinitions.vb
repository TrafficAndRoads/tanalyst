﻿Public Module modDefinitions

  Public Enum SoftwareEditions As Integer
    FullMode = 0
    Viewer = 1
  End Enum

  Public Enum DisplayModes As Integer
    View = 0
    Edit = 1
  End Enum
End Module
