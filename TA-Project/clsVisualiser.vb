﻿Public Class clsVisualiser
  Private MyParentRecord As clsRecord
  Private MyVisualiserOptions As clsVisualiserOptions
  Private MyPerspectives As List(Of Integer)
  Private MyPerspectiveSizes As List(Of Size)
  Private MyMaxWidth, MyMaxHeight As Integer


  Public Structure RoadUserGraphicPointsStructure
    Public Available As Boolean

    Public LeftP1 As Point
    Public RightP1 As Point
    Public LeftP2 As Point
    Public RightP2 As Point
    Public LeftP3 As Point
    Public RightP3 As Point
    Public LeftP4 As Point
    Public RightP4 As Point
    Public LeftP5 As Point
    Public RightP5 As Point
    Public LeftP6 As Point
    Public RightP6 As Point
    Public LeftP7 As Point
    Public RightP7 As Point
    Public LeftP8 As Point
    Public RightP8 As Point
    Public Centre As Point
    Public MiddleFront As Point
    Public MiddleLeft As Point
    Public MiddleRight As Point

    Public Trajectory As List(Of Point)
  End Structure



#Region "Properties"

  Public ReadOnly Property ParentRecord As clsRecord
    Get
      Return MyParentRecord
    End Get
  End Property

  Public ReadOnly Property VisualiserOptions As clsVisualiserOptions
    Get
      Return MyVisualiserOptions
    End Get
  End Property

#End Region



#Region "Public subs"



  Public Sub New(ByRef ParentRecord As clsRecord, ByRef VisualiserOptions As clsVisualiserOptions)

    MyParentRecord = ParentRecord
    MyVisualiserOptions = VisualiserOptions

    MyPerspectives = New List(Of Integer)
    MyPerspectiveSizes = New List(Of Size)

    Call UpdatePerspectives()
  End Sub


  Public Sub UpdatePerspectives()
    Dim _i As Integer

    MyPerspectives.Clear()
    MyPerspectiveSizes.Clear()

    _i = -1
    For Each _VideoFile In MyParentRecord.VideoFiles
      _i = _i + 1
      If _VideoFile.Visible Then
        MyPerspectives.Add(_i)
        MyPerspectiveSizes.Add(_VideoFile.FrameSize)
      End If
    Next

    If MyParentRecord.Map.Available Then
      If MyParentRecord.Map.Visible Then
        MyPerspectives.Add(-1)
        MyPerspectiveSizes.Add(MyParentRecord.Map.Size)
      End If
    End If


    MyMaxWidth = 0
    MyMaxHeight = 0

    For Each _VideoFile In MyParentRecord.VideoFiles
      If _VideoFile.Visible Then
        MyMaxWidth = Math.Max(MyMaxWidth, _VideoFile.FrameSize.Width)
        MyMaxHeight = Math.Max(MyMaxHeight, _VideoFile.FrameSize.Height)
      End If
    Next

    If MyParentRecord.Map.Available Then
      If MyParentRecord.Map.Visible Then
        MyMaxWidth = Math.Max(MyMaxWidth, MyParentRecord.Map.Size.Width)
        MyMaxHeight = Math.Max(MyMaxHeight, MyParentRecord.Map.Size.Height)
      End If
    End If
  End Sub

  Public Function ProjectedBitmap(ByVal Frame As Integer,
                                  Optional ByVal NoSelections As Boolean = False) As Bitmap

    Dim _g As System.Drawing.Graphics
    Dim _MapBitmap As Bitmap
    Dim _PenRoadUser As Pen
    Dim _PenTrajectory As Pen
    Dim _SolidBrushID As SolidBrush
    Dim _i As Integer

    If MyParentRecord.Map.Available Then
      'select background image to draw on
      _MapBitmap = MyParentRecord.Map.Bitmap
      _g = System.Drawing.Graphics.FromImage(_MapBitmap)

      'message lines
      If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
        For Each _MessageLine In MyParentRecord.TimeLine(Frame).MessageLines
          Call DrawMessageLine(_g, -1, _MessageLine)
        Next
      End If

      'message points
      If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
        For Each _MessagePoint In MyParentRecord.TimeLine(Frame).MessagePoints
          Call DrawMessagePoint(_g, -1, _MessagePoint)
        Next
      End If

      ' _i = 0
      For Each _RoadUser In MyParentRecord.TimeLine(Frame).RoadUsers
        'choose pens & brushes
        If (Not NoSelections) And _RoadUser.Equals(MyParentRecord.SelectedRoadUser) Then
          _PenRoadUser = VisualiserOptions.PenRoadUserSelected
          _PenTrajectory = VisualiserOptions.PenTrajectorySelected
          _SolidBrushID = VisualiserOptions.SolidBrushIDSelected
        ElseIf _RoadUser.Key = clsRoadUser.KeyRoadUser.Key1 Then
          _PenRoadUser = VisualiserOptions.PenRoadUserKey1
          _PenTrajectory = VisualiserOptions.PenTrajectoryKey1
          _SolidBrushID = VisualiserOptions.SolidBrushIDKey
        ElseIf _RoadUser.Key = clsRoadUser.KeyRoadUser.Key2 Then
          _PenRoadUser = VisualiserOptions.PenRoadUserKey2
          _PenTrajectory = VisualiserOptions.PenTrajectoryKey2
          _SolidBrushID = VisualiserOptions.SolidBrushIDKey
        Else
          _PenRoadUser = VisualiserOptions.PenRoadUserNormal
          _PenTrajectory = VisualiserOptions.PenTrajectoryNormal
          _SolidBrushID = VisualiserOptions.SolidBrushIDNormal
        End If

        Call DrawRoadUser(_g, _RoadUser, Frame, -1, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _MapBitmap.Size)
      Next

      If (Not NoSelections) And (MyParentRecord.SelectedRoadUser IsNot Nothing) Then
        If (Frame > MyParentRecord.SelectedRoadUser.LastFrame) And
           (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) _
        Then
          _PenRoadUser = VisualiserOptions.PenRoadUserProjected
          _PenTrajectory = VisualiserOptions.PenTrajectoryProjected
          _SolidBrushID = VisualiserOptions.SolidBrushIDProjected

          Call DrawRoadUser(_g, MyParentRecord.SelectedRoadUser, Frame, -1, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _MapBitmap.Size)
        End If

        If (Frame < MyParentRecord.SelectedRoadUser.FirstFrame) And
           (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) _
        Then
          _PenRoadUser = VisualiserOptions.PenRoadUserProjected
          _PenTrajectory = VisualiserOptions.PenTrajectoryProjected
          _SolidBrushID = VisualiserOptions.SolidBrushIDProjected

          Call DrawRoadUser(_g, MyParentRecord.SelectedRoadUser, Frame, -1, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _MapBitmap.Size)
        End If
      End If

      _g.Dispose()

      Return _MapBitmap
    Else
      Return Nothing
    End If

  End Function


  Public Function CameraMask(ByVal Frame As Integer, ByVal CameraIndex As Integer, ByVal RoadUser As clsRoadUser) As Bitmap
    Dim _g As System.Drawing.Graphics
    Dim _FrameSize As Size
    Dim _BackgroundTemp As Bitmap
    Dim _RoadUserPoints As RoadUserGraphicPointsStructure
    Dim _Points() As Point

    'select background image to draw on
    _FrameSize = MyParentRecord.VideoFiles(CameraIndex).FrameSize
    _RoadUserPoints = RoadUserPoints(RoadUser, Frame, CameraIndex)

    If IsBetween(_RoadUserPoints.Centre.X, 0, _FrameSize.Width - 1) And IsBetween(_RoadUserPoints.Centre.Y, 0, _FrameSize.Height - 1) Then
      _BackgroundTemp = New Bitmap(_FrameSize.Width, _FrameSize.Height)
      _g = Graphics.FromImage(_BackgroundTemp)

      ReDim _Points(7)
      _Points(0) = _RoadUserPoints.LeftP1
      _Points(1) = _RoadUserPoints.LeftP2
      _Points(2) = _RoadUserPoints.LeftP3
      _Points(3) = _RoadUserPoints.LeftP4
      _Points(4) = _RoadUserPoints.LeftP5
      _Points(5) = _RoadUserPoints.LeftP6
      _Points(6) = _RoadUserPoints.LeftP7
      _Points(7) = _RoadUserPoints.LeftP8
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(7)
      _Points(0) = _RoadUserPoints.RightP1
      _Points(1) = _RoadUserPoints.RightP2
      _Points(2) = _RoadUserPoints.RightP3
      _Points(3) = _RoadUserPoints.RightP4
      _Points(4) = _RoadUserPoints.RightP5
      _Points(5) = _RoadUserPoints.RightP6
      _Points(6) = _RoadUserPoints.RightP7
      _Points(7) = _RoadUserPoints.RightP8
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP1
      _Points(1) = _RoadUserPoints.LeftP2
      _Points(2) = _RoadUserPoints.RightP2
      _Points(3) = _RoadUserPoints.RightP1
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP2
      _Points(1) = _RoadUserPoints.LeftP3
      _Points(2) = _RoadUserPoints.RightP3
      _Points(3) = _RoadUserPoints.RightP2
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP3
      _Points(1) = _RoadUserPoints.LeftP4
      _Points(2) = _RoadUserPoints.RightP4
      _Points(3) = _RoadUserPoints.RightP3
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP4
      _Points(1) = _RoadUserPoints.LeftP5
      _Points(2) = _RoadUserPoints.RightP5
      _Points(3) = _RoadUserPoints.RightP4
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP5
      _Points(1) = _RoadUserPoints.LeftP6
      _Points(2) = _RoadUserPoints.RightP6
      _Points(3) = _RoadUserPoints.RightP5
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP6
      _Points(1) = _RoadUserPoints.LeftP7
      _Points(2) = _RoadUserPoints.RightP7
      _Points(3) = _RoadUserPoints.RightP6
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP7
      _Points(1) = _RoadUserPoints.LeftP8
      _Points(2) = _RoadUserPoints.RightP8
      _Points(3) = _RoadUserPoints.RightP7
      _g.FillPolygon(Brushes.White, _Points)

      ReDim _Points(3)
      _Points(0) = _RoadUserPoints.LeftP1
      _Points(1) = _RoadUserPoints.LeftP8
      _Points(2) = _RoadUserPoints.RightP8
      _Points(3) = _RoadUserPoints.RightP1
      _g.FillPolygon(Brushes.Yellow, _Points)

      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP1, _RoadUserPoints.LeftP2)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP2, _RoadUserPoints.LeftP3)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP3, _RoadUserPoints.LeftP4)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP4, _RoadUserPoints.LeftP5)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP5, _RoadUserPoints.LeftP6)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP6, _RoadUserPoints.LeftP7)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP7, _RoadUserPoints.LeftP8)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP8, _RoadUserPoints.LeftP1)

      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP1, _RoadUserPoints.RightP2)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP2, _RoadUserPoints.RightP3)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP3, _RoadUserPoints.RightP4)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP4, _RoadUserPoints.RightP5)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP5, _RoadUserPoints.RightP6)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP6, _RoadUserPoints.RightP7)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP7, _RoadUserPoints.RightP8)
      _g.DrawLine(Pens.Red, _RoadUserPoints.RightP8, _RoadUserPoints.RightP1)

      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP1, _RoadUserPoints.RightP1)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP2, _RoadUserPoints.RightP2)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP3, _RoadUserPoints.RightP3)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP4, _RoadUserPoints.RightP4)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP5, _RoadUserPoints.RightP5)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP6, _RoadUserPoints.RightP6)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP7, _RoadUserPoints.RightP7)
      _g.DrawLine(Pens.Red, _RoadUserPoints.LeftP8, _RoadUserPoints.RightP8)

      _g.Dispose()
      Return _BackgroundTemp
    Else
      Return Nothing
    End If
  End Function


  Public Function CameraBitmap(ByVal Frame As Integer,
                               ByVal CameraIndex As Integer,
                               Optional ByVal NoSelections As Boolean = False) As Bitmap

    Dim _G As System.Drawing.Graphics
    Dim _BackgroundTemp As Bitmap
    Dim _PenRoadUser As Pen
    Dim _PenTrajectory As Pen
    Dim _SolidBrushID As SolidBrush
    Dim _i As Integer


    'select background image to draw on
    _BackgroundTemp = MyParentRecord.VideoFiles(CameraIndex).GetFrameBitmap(MyParentRecord.TimeLine(Frame).VideoFileFrames(CameraIndex))
    _G = Graphics.FromImage(_BackgroundTemp)

    'message lines
    If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
      For Each _MessageLine In MyParentRecord.TimeLine(Frame).MessageLines
        Call DrawMessageLine(_G, CameraIndex, _MessageLine)
      Next
    End If

    'message points
    If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
      For Each _MessagePoint In MyParentRecord.TimeLine(Frame).MessagePoints
        Call DrawMessagePoint(_G, CameraIndex, _MessagePoint)
      Next
    End If

    'road users
    _i = 0
    For Each _RoadUser In MyParentRecord.TimeLine(Frame).RoadUsers

      'choose pens & brushes
      If (Not NoSelections) And _RoadUser.Equals(MyParentRecord.SelectedRoadUser) Then
        _PenRoadUser = VisualiserOptions.PenRoadUserSelected
        _PenTrajectory = VisualiserOptions.PenTrajectorySelected
        _SolidBrushID = VisualiserOptions.SolidBrushIDSelected
      ElseIf _RoadUser.Key = clsRoadUser.KeyRoadUser.Key1 Then
        _PenRoadUser = VisualiserOptions.PenRoadUserKey1
        _PenTrajectory = VisualiserOptions.PenTrajectoryKey1
        _SolidBrushID = VisualiserOptions.SolidBrushIDKey
      ElseIf _RoadUser.Key = clsRoadUser.KeyRoadUser.Key2 Then
        _PenRoadUser = VisualiserOptions.PenRoadUserKey2
        _PenTrajectory = VisualiserOptions.PenTrajectoryKey2
        _SolidBrushID = VisualiserOptions.SolidBrushIDKey
      Else
        _PenRoadUser = VisualiserOptions.PenRoadUserNormal
        _PenTrajectory = VisualiserOptions.PenTrajectoryNormal
        _SolidBrushID = VisualiserOptions.SolidBrushIDNormal
      End If

      Call DrawRoadUser(_G, _RoadUser, Frame, CameraIndex, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _BackgroundTemp.Size)
    Next

    If (Not NoSelections) And (MyParentRecord.SelectedRoadUser IsNot Nothing) Then
      If (Frame > MyParentRecord.SelectedRoadUser.LastFrame) And
         (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) _
      Then
        _PenRoadUser = VisualiserOptions.PenRoadUserProjected
        _PenTrajectory = VisualiserOptions.PenTrajectoryProjected
        _SolidBrushID = VisualiserOptions.SolidBrushIDProjected

        Call DrawRoadUser(_G, MyParentRecord.SelectedRoadUser, Frame, CameraIndex, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _BackgroundTemp.Size)
      End If

      If (Frame < MyParentRecord.SelectedRoadUser.FirstFrame) And
         (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) _
      Then
        _PenRoadUser = VisualiserOptions.PenRoadUserProjected
        _PenTrajectory = VisualiserOptions.PenTrajectoryProjected
        _SolidBrushID = VisualiserOptions.SolidBrushIDProjected

        Call DrawRoadUser(_G, MyParentRecord.SelectedRoadUser, Frame, CameraIndex, _i.ToString, _PenRoadUser, _PenTrajectory, _SolidBrushID, _BackgroundTemp.Size)
      End If
    End If


    _G.Dispose()

    Return _BackgroundTemp
  End Function

  Public Function ZoomedBitmap(ByVal Frame As Integer,
                               ByRef Perspectives As List(Of Integer),
                               ByRef Xc As List(Of Integer),
                               ByRef Yc As List(Of Integer)) As Bitmap

    Const MAX_PERSPECTIVES_COUNT = 3

    Dim _VideoFile As clsVideoFile
    Dim _OriginalBitmap As Bitmap
    Dim _ZoomedBitmap As Bitmap
    Dim _g As Graphics
    Dim _RoadUserPoints As RoadUserGraphicPointsStructure
    Dim _RectFrom, _RectTo As Rectangle
    Dim _i As Integer
    Dim _Bitmaps As List(Of Bitmap)
    Dim _Bitmap As Bitmap


    Perspectives = New List(Of Integer)
    Xc = New List(Of Integer)
    Yc = New List(Of Integer)

    _Bitmaps = New List(Of Bitmap)


    If MyParentRecord.SelectedRoadUser IsNot Nothing Then
      _i = -1
      For Each _VideoFile In MyParentRecord.VideoFiles
        _i = _i + 1
        If _VideoFile.Visible And _VideoFile.CallibrationOK Then
          If Perspectives.Count <= MAX_PERSPECTIVES_COUNT Then

            _OriginalBitmap = Me.CameraBitmap(Frame, _i)

            If (Frame < MyParentRecord.SelectedRoadUser.FirstFrame) And (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) Then
              _RoadUserPoints = RoadUserPoints(MyParentRecord.SelectedRoadUser, MyParentRecord.SelectedRoadUser.FirstFrame, _i)
            ElseIf IsBetween(Frame, MyParentRecord.SelectedRoadUser.FirstFrame, MyParentRecord.SelectedRoadUser.LastFrame) Then
              _RoadUserPoints = RoadUserPoints(MyParentRecord.SelectedRoadUser, Frame, _i)
            ElseIf (Frame > MyParentRecord.SelectedRoadUser.LastFrame) And (MyVisualiserOptions.TrajectoryType = TrajectoryTypes.Original) Then
              _RoadUserPoints = RoadUserPoints(MyParentRecord.SelectedRoadUser, MyParentRecord.SelectedRoadUser.LastFrame, _i)
            End If

            If IsBetween(_RoadUserPoints.Centre.X, 1, _OriginalBitmap.Width - 2) And IsBetween(_RoadUserPoints.Centre.Y, 1, _OriginalBitmap.Height - 2) Then
              Perspectives.Add(_i)
              Xc.Add(_RoadUserPoints.Centre.X)
              Yc.Add(_RoadUserPoints.Centre.Y)

              _Bitmap = New Bitmap(300, 300)
              _g = Graphics.FromImage(_Bitmap)
              _RectFrom = New Rectangle(_RoadUserPoints.Centre.X - 75, _RoadUserPoints.Centre.Y - 75, 150, 150)
              _RectTo = New Rectangle(0, 0, 300, 300)
              _g.DrawImage(_OriginalBitmap, _RectTo, _RectFrom, GraphicsUnit.Pixel)
              _Bitmaps.Add(_Bitmap)
              _OriginalBitmap.Dispose()
              _g.Dispose()
            End If
          End If
        End If
      Next
    End If


    If Perspectives.Count > 0 Then
      _ZoomedBitmap = New Bitmap(Perspectives.Count * 300, 300)
      _g = Graphics.FromImage(_ZoomedBitmap)
      _i = -1
      For Each _Bitmap In _Bitmaps
        _i = _i + 1
        _g.DrawImage(_Bitmap, _i * 300, 0, 300, 300)
        _Bitmap.Dispose()
      Next
      _g.Dispose()
    Else
      _ZoomedBitmap = Nothing
    End If

    Return _ZoomedBitmap
  End Function

  Public Function CombinedBitmap(ByVal Frame As Integer,
                                 Optional ByVal NoSelections As Boolean = False) As Bitmap
    Dim _g As Graphics
    Dim _Bitmaps As List(Of Bitmap)
    Dim _VideoFile As clsVideoFile
    Dim _CombinedBitmap As Bitmap
    Dim _PositionTemp As Rectangle
    Dim _Column As Integer
    '-   Dim _MaxWidth, _MaxHeight As Integer
    Dim _i As Integer



    _Bitmaps = New List(Of Bitmap)

    _i = -1
    For Each _VideoFile In MyParentRecord.VideoFiles
      _i = _i + 1
      If _VideoFile.Visible Then
        _Bitmaps.Add(CameraBitmap(Frame, _i, NoSelections))
      End If
    Next

    If MyParentRecord.Map.Available Then
      If MyParentRecord.Map.Visible Then
        _Bitmaps.Add(ProjectedBitmap(Frame, NoSelections))
      End If
    End If




    Select Case _Bitmaps.Count
      Case 0
        _CombinedBitmap = Nothing
        Return _CombinedBitmap
      Case 2
        _CombinedBitmap = New Bitmap(2 * MyMaxWidth, MyMaxHeight)
      Case 3
        _CombinedBitmap = New Bitmap(CInt(1.5 * MyMaxWidth), MyMaxHeight)
      Case 5, 6
        _CombinedBitmap = New Bitmap(CInt(2 * MyMaxWidth / 3), MyMaxHeight)
      Case Else '1,4,7,8,9
        _CombinedBitmap = New Bitmap(MyMaxWidth, MyMaxHeight)
    End Select


    _g = Graphics.FromImage(_CombinedBitmap)
    For _i = 0 To Math.Min(_Bitmaps.Count - 1, 8)
      Select Case _Bitmaps.Count
        Case 0

        Case 1
          _PositionTemp = New Rectangle(0, 0, _Bitmaps(0).Width, _Bitmaps(0).Height)
        Case 2
          _PositionTemp = New Rectangle(_i * MyMaxWidth, 0,
                                        _Bitmaps(_i).Width, _Bitmaps(_i).Height)
        Case 3
          _Column = CInt((_i + 0.5) / 2)
          _PositionTemp = New Rectangle(CInt(MyMaxWidth * _Column),
                                        CInt(MyMaxHeight * (_i - _Column) / 2),
                                        CInt(_Bitmaps(_i).Width / (1 + _Column)),
                                        CInt(_Bitmaps(_i).Height / (1 + _Column)))
        Case 4
          _Column = _i \ 2
          _PositionTemp = New Rectangle(CInt(MyMaxWidth / 2 * _Column),
                                        CInt(MyMaxHeight * (_i - 2 * _Column) / 2),
                                        CInt(_Bitmaps(_i).Width / 2),
                                        CInt(_Bitmaps(_i).Height / 2))
        Case Else
          _Column = _i \ 3
          _PositionTemp = New Rectangle(CInt(MyMaxWidth / 3 * _Column),
                                        CInt(MyMaxHeight / 3 * (_i - 3 * _Column)),
                                        CInt(MyMaxWidth / 3),
                                        CInt(MyMaxHeight / 3))
      End Select
      _g.DrawImage(_Bitmaps(_i), _PositionTemp)
    Next

    Return _CombinedBitmap
  End Function


  Public Function SaveVideoSequence(ByVal VideoFilePath As String,
                                    ByVal StartFrame As Integer,
                                    ByVal EndFrame As Integer,
                                    Optional ByVal SpeedFactor As Integer = 1,
                                    Optional ByVal KBS As Integer = 6000) As Boolean
    Dim _TempDir As String
    Dim _i As Integer
    Dim _Process As Process
    Dim _FrameRate, _BitRate As String
    Dim _Filelist() As String
    Dim _PathToFFMPEG As String = "ffmpeg.exe"


    If File.Exists(_PathToFFMPEG) Then
      _i = 0

      _TempDir = Path.GetDirectoryName(VideoFilePath) & "\Temp\"
      If Not Directory.Exists(_TempDir) Then
        Directory.CreateDirectory(_TempDir)
      Else
        _Filelist = Directory.GetFiles(_TempDir, "*.jpg")
        For Each _File In _Filelist
          File.Delete(_File)
        Next
      End If

      For _Frame = StartFrame To EndFrame Step SpeedFactor
        SaveJpgImage(Me.CombinedBitmap(_Frame, True), _TempDir & Format(_i, "00000000") & ".jpg", JpgCOMPRESSION)
        _i = _i + 1
      Next

      Call MakeAVIfromImages(VideoFilePath, _TempDir, MyParentRecord.FrameRate, KBS)

      '_FrameRate = " -r " & MyParentRecord.FrameRate.ToString.Replace(",", ".")
      '_BitRate = " -b:v " & KBS.ToString & "k"
      '_Process = New Process
      '_Process.StartInfo.UseShellExecute = False
      '_Process.StartInfo.CreateNoWindow = True
      '_Process.StartInfo.FileName = _PathToFFMPEG
      '_Process.StartInfo.Arguments = "-y" & _FrameRate & " -i """ & _TempDir & "%08d.jpg" & """ -codec:v libxvid -tag:v XVID" & _BitRate & " """ & VideoFilePath & """"
      '_Process.Start()
      '_Process.WaitForExit()


      _Filelist = Directory.GetFiles(_TempDir, "*.*")
      For Each _File In _Filelist
        File.Delete(_File)
      Next
      Directory.Delete(_TempDir)

      Return True
    Else
      MsgBox("Make sure that you have file '" & _PathToFFMPEG & "' in place!")
      Return False
    End If

  End Function


  Public Sub PerspectiveXY(ByVal CombinedX As Integer, ByVal CombinedY As Integer, ByRef Perspective As Integer, ByRef LocalX As Integer, ByRef LocalY As Integer)


    Perspective = NoValue
    LocalX = NoValue
    LocalY = NoValue

    If IsBetween(MyPerspectives.Count, 1, 3) Then
      If IsBetween(CombinedX, 0, MyPerspectiveSizes(0).Width - 1) And IsBetween(CombinedY, 0, MyPerspectiveSizes(0).Height - 1) Then
        Perspective = MyPerspectives(0)
        LocalX = CombinedX
        LocalY = CombinedY
        Exit Sub
      End If

      Select Case MyPerspectives.Count
        Case 2
          If IsBetween(CombinedX, MyMaxWidth, MyMaxWidth + MyPerspectiveSizes(1).Width - 1) And IsBetween(CombinedY, 0, MyMaxHeight - 1) Then
            Perspective = MyPerspectives(1)
            LocalX = CombinedX - MyMaxWidth
            LocalY = CombinedY
          End If
        Case 3
          'If IsBetween(CombinedX, MyPerspectiveSizes(0).Width, MyPerspectiveSizes(0).Width + CInt(MyPerspectiveSizes(1).Width / 2) - 1) And IsBetween(CombinedY, 0, CInt(MyPerspectiveSizes(1).Height / 2) - 1) Then
          '  Perspective = MyPerspectives(1)
          '  LocalX = 2 * (CombinedX - MyPerspectiveSizes(0).Width)
          '  LocalY = 2 * CombinedY
          'ElseIf IsBetween(CombinedX, MyPerspectiveSizes(0).Width, MyPerspectiveSizes(0).Width + CInt(MyPerspectiveSizes(1).Width / 2) - 1) And IsBetween(CombinedY, CInt(MyPerspectiveSizes(1).Height / 2), CInt(MyPerspectiveSizes(0).Height / 2) + CInt(MyPerspectiveSizes(2).Height / 2) - 1) Then
          '  Perspective = MyPerspectives(2)
          '  LocalX = 2 * (CombinedX - MyPerspectiveSizes(0).Width)
          '  LocalY = 2 * (CombinedY - CInt(MyPerspectiveSizes(0).Height / 2))
          'End If
        Case 4

        Case 5


      End Select
    End If






  End Sub


  Public Sub ExportDetectionAsGroundTruthOLD()
    Const _MARGIN_LENGTH As Integer = 150

    Dim _ExportDirectory As String
    Dim _VideoDirectory As String
    Dim _CalibrationDirectory As String
    Dim _TrajectoryDirectory As String
    Dim _VideoFileName As String
    Dim _ClockDescrepancyFileIndex As Integer
    Dim _StartVideoFrame, _EndVideoFrame As Integer
    Dim _StartAbsRecordTime, _EndAbsRecordTime As Double
    Dim _VideoFileFrame As Integer
    Dim _ZipWriter As clsZipWriter
    Dim _i, _j As Integer
    Dim _FileIndex As Integer
    Dim _Mask As Bitmap
    Dim _MaskCount As Integer
    Dim _MaskFilePath As String
    Dim _Line As String
    Dim _Point As Point
    Dim _CharArray() As Char



    _CharArray = "abcdefghijklmnopqrstuvwxyz".ToCharArray
    _ExportDirectory = MyParentRecord.ExportDirectory & "Ground truth\" & Format(MyParentRecord.DateTime, "yyyyMMdd-HHmm")

    _i = 0
    Do Until Not Directory.Exists(_ExportDirectory)
      If _i > 25 Then
        MsgBox("You have too many exported events with the same start time '" & Format(MyParentRecord.DateTime, "yyyyMMdd-HHmm") &
               "'. Remove some of them from your '\Export\Ground truth' folder.")
        Exit Sub
      End If
      If _i > 0 Then _ExportDirectory = _ExportDirectory.Substring(0, _ExportDirectory.Length - 1)
      _ExportDirectory = _ExportDirectory & _CharArray(_i).ToString
      _i = _i + 1
    Loop

    _ExportDirectory = _ExportDirectory & "\"
    _VideoDirectory = _ExportDirectory & "Video\"
    _CalibrationDirectory = _ExportDirectory & "Calibration\"
    _TrajectoryDirectory = _ExportDirectory & "Trajectories\"

    Directory.CreateDirectory(_ExportDirectory)
    Directory.CreateDirectory(_VideoDirectory)
    Directory.CreateDirectory(_CalibrationDirectory)
    Directory.CreateDirectory(_TrajectoryDirectory)

    Call SaveVideoSequence(_ExportDirectory & "Preview.avi", 0, MyParentRecord.FrameCount - 1)

    _ClockDescrepancyFileIndex = FreeFile()
    FileOpen(_ClockDescrepancyFileIndex, _VideoDirectory & "Clock descrepancy.txt", OpenMode.Output)

    _EndAbsRecordTime = MyParentRecord.StartTime + MyParentRecord.FrameCount / MyParentRecord.FrameRate + 15
    _StartAbsRecordTime = _EndAbsRecordTime - _MARGIN_LENGTH

    _i = 0
    For Each _VideoFile In MyParentRecord.VideoFiles
      _i = _i + 1
      _VideoFileName = "Camera_" & _i.ToString
      _ZipWriter = New clsZipWriter(_VideoDirectory & _VideoFileName & ".zip")

      _StartVideoFrame = _VideoFile.GetFrameIndex(_StartAbsRecordTime)
      _EndVideoFrame = _VideoFile.GetFrameIndex(_EndAbsRecordTime)

      For _VideoFileFrame = _StartVideoFrame To _EndVideoFrame
        _ZipWriter.AddFrame(_VideoFile.GetFrameBitmap(_VideoFileFrame), Format(_VideoFile.GetTimeStampDate(_VideoFileFrame), "yyyyMMdd-HHmmss.fff"))
      Next
      _ZipWriter.Close()
      'PrintLine(_ClockDescrepancyFileIndex, "Camera_" & _i.ToString & ": " &
      '          FormatNumber(_VideoFile.ClockDiscrepancy + (_VideoFile.ClockDiscrepancyRate - 1) * _StartAbsRecordTime, 3) & ";" &
      '          FormatNumber(_VideoFile.ClockDiscrepancyRate, 15))

      If _VideoFile.CallibrationOK Then
        _VideoFile.ExportCalibration(_CalibrationDirectory & "Camera_" & _i.ToString & ".tacal")
        _j = 0
        For Each _RoadUser In MyParentRecord.RoadUsers
          _j = _j + 1
          _MaskCount = 0
          _MaskFilePath = _TrajectoryDirectory & _VideoFileName & "_RoadUser_" & _j.ToString & ".zip"
          _ZipWriter = New clsZipWriter(_MaskFilePath)
          For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
            _VideoFileFrame = MyParentRecord.TimeLine(_Frame).VideoFileFrames(_i - 1)
            _Mask = CameraMask(_Frame, _i - 1, _RoadUser)
            If _Mask IsNot Nothing Then
              _ZipWriter.AddFrame(_Mask, Format(_VideoFile.GetTimeStampDate(_VideoFileFrame), "yyyyMMdd-HHmmss.fff"))
              _MaskCount = _MaskCount + 1
            End If
          Next
          _ZipWriter.Close()
          If _MaskCount = 0 Then File.Delete(_MaskFilePath)
        Next
      End If
    Next
    FileClose(_ClockDescrepancyFileIndex)



    _FileIndex = FreeFile()
    FileOpen(_FileIndex, _VideoDirectory & "Frame correspondence at " & FormatNumber(MyParentRecord.FrameRate, 3) & " fps.txt", OpenMode.Output)
    _Line = "Abs index; Abs time, s."
    For _i = 0 To MyParentRecord.VideoFiles.Count - 1
      _Line = _Line & "; Camera_" & (_i + 1).ToString
    Next
    PrintLine(_FileIndex, _Line)

    For _Frame = 0 To (MyParentRecord.FrameRate * (_EndAbsRecordTime - _StartAbsRecordTime))
      _Line = _Frame.ToString & ";" & FormatNumber(_Frame / MyParentRecord.FrameRate, 3)
      '_Line = _Frame.ToString & ";" & Format(Date.FromOADate((_StartAbsRecordTime + _Frame / MyParentRecord.FrameRate) / 24 / 60 / 60), "yyyyMMdd-HHmmss.fff")
      For _i = 0 To MyParentRecord.VideoFiles.Count - 1
        _VideoFileFrame = MyParentRecord.VideoFiles(_i).GetFrameIndex(_StartAbsRecordTime + _Frame / MyParentRecord.FrameRate)
        _Line = _Line & ";" & Format(MyParentRecord.VideoFiles(_i).GetTimeStampDate(_VideoFileFrame), "yyyyMMdd-HHmmss.fff")
      Next
      PrintLine(_FileIndex, _Line)
    Next
    FileClose(_FileIndex)


    _j = 0
    For Each _RoadUser In MyParentRecord.RoadUsers
      _j = _j + 1
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _TrajectoryDirectory & "RoadUser_" & _j.ToString & ".txt", OpenMode.Output)
      PrintLine(_FileIndex, "Type: " & _RoadUser.Type.Name)
      PrintLine(_FileIndex, "Length, m: " & FormatNumber(_RoadUser.Length, 1))
      PrintLine(_FileIndex, "Width, m: " & FormatNumber(_RoadUser.Width, 1))
      PrintLine(_FileIndex, "Height, m: " & FormatNumber(_RoadUser.Height, 1))
      _Line = "Abs index; Abs time, s; X, m; Y, m; dX; dY"
      _i = 0
      For Each _VideoFile In MyParentRecord.VideoFiles
        _i = _i + 1
        If _VideoFile.Visible Then
          _Line = _Line & "; Camera_" & _i.ToString & "; X, pxl; Y, pxl"
        End If
      Next
      PrintLine(_FileIndex, _Line)

      For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
        _Line = FormatNumber((MyParentRecord.GetAbsoluteTime(_Frame) - _StartAbsRecordTime) * MyParentRecord.FrameRate, 0, , , TriState.False)
        _Line = _Line & ";" & FormatNumber(MyParentRecord.GetAbsoluteTime(_Frame) - _StartAbsRecordTime, 3)
        '_Line = _Line & ";" & Format(Date.FromOADate((MyParentRecord.GetAbsoluteTime(_Frame)) / 24 / 60 / 60), "yyyyMMdd-HHmmss.fff")

        _Line = _Line & ";" & FormatNumber(_RoadUser.DataPoint(_Frame).X, 3) & ";" &
                FormatNumber(_RoadUser.DataPoint(_Frame).Y, 3) & ";" &
                FormatNumber(_RoadUser.DataPoint(_Frame).Dx, 3) & ";" &
                FormatNumber(_RoadUser.DataPoint(_Frame).Dy, 3)

        _i = 0
        For Each _VideoFile In MyParentRecord.VideoFiles
          _i = _i + 1
          If _VideoFile.CallibrationOK Then
            _Point = _VideoFile.TransformWorldToImage(_RoadUser.DataPoint(_Frame).X, _RoadUser.DataPoint(_Frame).Y)
            If IsBetween(_Point.X, 0, _VideoFile.FrameSize.Width - 1) And IsBetween(_Point.Y, 0, _VideoFile.FrameSize.Height - 1) Then
              _Line = _Line & ";" & Format(_VideoFile.GetTimeStampDate(MyParentRecord.TimeLine(_Frame).VideoFileFrames(_i - 1)), "yyyyMMdd-HHmmss.fff") & ";" & _Point.X & ";" & _Point.Y
            Else
              _Line = _Line & ";-;-;-"
            End If
          End If
        Next
        PrintLine(_FileIndex, _Line)
      Next

      FileClose(_FileIndex)
    Next

  End Sub

  Public Sub ExportDetectionAsGroundTruth()
    Const _VIDEO_MARGIN As Integer = 15

    Dim _ExportDirectory As String
    Dim _TempDirectory As String
    Dim _VideoDirectory As String
    Dim _CalibrationDirectory As String
    Dim _TrajectoryDirectory As String
    Dim _VideoFileName As String
    Dim _ClockDescrepancyFileIndex As Integer
    Dim _StartVideoFrame, _EndVideoFrame As Integer
    Dim _StartAbsVideoTime, _EndAbsVideoTime As Double
    Dim _VideoFileFrameIndex As Integer
    Dim _ZipWriter As clsZipWriter
    Dim _i, _j As Integer
    Dim _FileIndex As Integer
    Dim _Mask As Bitmap
    Dim _MaskCount As Integer
    Dim _MaskFilePath As String
    Dim _Line As String
    Dim _Point As Point
    Dim _FrameImage As Bitmap
    Dim _FPS As Double
    Dim _Frame As Integer
    Dim _StartVideoFrames As List(Of Integer)
    Dim _CharArray() As Char




    _CharArray = "abcdefghijklmnopqrstuvwxyz".ToCharArray

    'Create export (sub-)directories
    _i = 0
    Do
      If _i > 25 Then
        MsgBox("You have too many exported events with the same ID = '" & MyParentRecord.ID.ToString &
               "'. Remove some of them from your '\Export\Ground truth\' folder.")
        Exit Sub
      End If
      _ExportDirectory = MyParentRecord.ExportDirectory & "Ground truth\" & MyParentRecord.ID.ToString("00000") & _CharArray(_i).ToString
      _i = _i + 1
    Loop Until Not Directory.Exists(_ExportDirectory)



    _ExportDirectory = _ExportDirectory & "\"
    _TempDirectory = _ExportDirectory & "Temp\"
    _VideoDirectory = _ExportDirectory & "Video\"
    _CalibrationDirectory = _ExportDirectory & "Calibration\"
    _TrajectoryDirectory = _ExportDirectory & "Trajectories\"

    Directory.CreateDirectory(_ExportDirectory)
    Directory.CreateDirectory(_VideoDirectory)
    Directory.CreateDirectory(_CalibrationDirectory)
    Directory.CreateDirectory(_TrajectoryDirectory)

    'create a preview of the evnt
    Call SaveVideoSequence(_ExportDirectory & "Preview.avi", 0, MyParentRecord.FrameCount - 1)



    'exports map calibration
    MyParentRecord.Map.ExportCalibration(_CalibrationDirectory & "Map.tamap")
    Call SaveJpgImage(MyParentRecord.Map.Bitmap, _CalibrationDirectory & "Map.jpg")


    'define the start end end time of videos to be exported (with _VIDEO_MARGIN seconds margins on both sies)
    _StartAbsVideoTime = MyParentRecord.StartTime - _VIDEO_MARGIN
    _EndAbsVideoTime = _StartAbsVideoTime + MyParentRecord.FrameCount / MyParentRecord.FrameRate + 2 * _VIDEO_MARGIN



    'Opens for writing a file with clock descripancy parameters for each video
    _ClockDescrepancyFileIndex = FreeFile()
    FileOpen(_ClockDescrepancyFileIndex, _VideoDirectory & "Clock descrepancies.txt", OpenMode.Output)




    'this is a list of frame ID's in the original videos at which each video clip starts — needed later
    _StartVideoFrames = New List(Of Integer)



    'Save relevant video clips from each video
    _i = 0
    For Each _VideoFile In MyParentRecord.VideoFiles
      _i = _i + 1
      _VideoFileName = "Camera_" & _i.ToString

      _StartVideoFrame = _VideoFile.GetFrameIndex(_StartAbsVideoTime)
      _EndVideoFrame = _VideoFile.GetFrameIndex(_EndAbsVideoTime)
      _StartVideoFrames.Add(_StartVideoFrame)

      'opens .log file for writing time stamps
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _VideoDirectory & _VideoFileName & ".log", OpenMode.Output)
      Directory.CreateDirectory(_TempDirectory)
      For _VideoFileFrameIndex = _StartVideoFrame To _EndVideoFrame
        'fetch the frame image and save it on disc
        _FrameImage = _VideoFile.GetFrameBitmap(_VideoFileFrameIndex)
        Call SaveJpgImage(_FrameImage, _TempDirectory & (_VideoFileFrameIndex - _StartVideoFrame).ToString("00000000") & ".jpg")
        _FrameImage.Dispose()

        'writes time stamp
        _Line = (_VideoFileFrameIndex - _StartVideoFrame).ToString("00000") & " " & _VideoFile.GetTimeStampDate(_VideoFileFrameIndex).ToString("yyyy MM dd HH:mm:ss.fff")
        PrintLine(_FileIndex, _Line)
      Next
      FileClose(_FileIndex)



      'creates video from the saved frames
      _FPS = (_EndVideoFrame - _StartVideoFrame) / (_VideoFile.GetTimeStamp(_EndVideoFrame) - _VideoFile.GetTimeStamp(_StartVideoFrame))
      Call MakeAVIfromImages(_VideoDirectory & _VideoFileName & ".avi", _TempDirectory, _FPS, 5000)
      Directory.Delete(_TempDirectory, True)



      'writes clock descrpancy parameters for the current video
      PrintLine(_ClockDescrepancyFileIndex, "Camera_" & _i.ToString & ": " &
                _VideoFile.ClockDiscrepancy.ToString("0.000", CultureInfo.InvariantCulture)) ' + (_VideoFile.ClockDiscrepancyRate - 1) * _StartAbsVideoTime, 3) & ";" &      FormatNumber(_VideoFile.ClockDiscrepancyRate, 15))



      'exports video calibration
      If _VideoFile.CallibrationOK Then _VideoFile.ExportCalibration(_CalibrationDirectory & "Camera_" & _i.ToString & ".tacal")

    Next '_VideoFile

    FileClose(_ClockDescrepancyFileIndex)



    'Creates a list of frames corresponding to each dapa point of the event pacemaker
    _FileIndex = FreeFile()
    FileOpen(_FileIndex, _VideoDirectory & "Frame correspondence at " & MyParentRecord.FrameRate.ToString("0.000", CultureInfo.InvariantCulture) & " fps.txt", OpenMode.Output)

    'heading
    _Line = "Abs pacemaker"
    For _i = 0 To MyParentRecord.VideoFiles.Count - 1
      _Line = _Line & "; Camera_" & (_i + 1).ToString
    Next
    PrintLine(_FileIndex, _Line)

    'contents
    For _Frame = 0 To CInt(MyParentRecord.FrameRate * (_EndAbsVideoTime - _StartAbsVideoTime))
      'pacemaker data point ID and time stamp
      _Line = _Frame.ToString("00000") & " " & Format(Date.FromOADate((MyParentRecord.StartTime + _Frame / MyParentRecord.FrameRate) / 24 / 60 / 60), "yyyy MM dd HH:mm:ss.fff")

      'video frame IDs and time stamps (ID counted as in the newly saved video clips)
      For _i = 0 To MyParentRecord.VideoFiles.Count - 1
        _VideoFileFrameIndex = MyParentRecord.VideoFiles(_i).GetFrameIndex(MyParentRecord.StartTime + _Frame / MyParentRecord.FrameRate)
        _Line = _Line & ";" & (_VideoFileFrameIndex - _StartVideoFrames(_i)).ToString("00000") & " " & Format(MyParentRecord.VideoFiles(_i).GetTimeStampDate(_VideoFileFrameIndex), "yyyy MM dd HH:mm:ss.fff")
      Next
      PrintLine(_FileIndex, _Line)
    Next
    FileClose(_FileIndex)







    'write to separate files each road users positions per pacemaker data point with corresponding positions in video images
    _j = 0
    For Each _RoadUser In MyParentRecord.RoadUsers
      _j = _j + 1
      _FileIndex = FreeFile()

      'heading
      FileOpen(_FileIndex, _TrajectoryDirectory & "RoadUser_" & _j.ToString & ".txt", OpenMode.Output)
      PrintLine(_FileIndex, "Type: " & _RoadUser.Type.Name)
      PrintLine(_FileIndex, "Length, m: " & _RoadUser.Length.ToString("0.00", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Width, m: " & _RoadUser.Width.ToString("0.00", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Height, m: " & _RoadUser.Height.ToString("0.00", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Weight, kg: " & _RoadUser.Weight.ToString("0", CultureInfo.InvariantCulture))
      _Line = "Abs pacemaker; X, m; Y, m; Z, m; dX; dY; V, m/s; A, m/s^2; J, m/s^3; Xpxl, m; Ypxl, m; Zpxl, m; dXpxl; dYpxl"
      _i = 0
      For Each _VideoFile In MyParentRecord.VideoFiles
        _i = _i + 1
        If _VideoFile.Visible Then
          _Line = _Line & "; Camera_" & _i.ToString & "; X" & _i.ToString & ", pxl; Y" & _i.ToString & ", pxl"
        End If
      Next
      PrintLine(_FileIndex, _Line)

      'contents
      'world co-ordinates
      For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
        _Line = _Frame.ToString("00000") & " " & Format(Date.FromOADate((MyParentRecord.GetAbsoluteTime(_Frame)) / 24 / 60 / 60), "yyyy MM dd HH:mm:ss.fff")

        _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).X.ToString("0.000", CultureInfo.InvariantCulture) & ";" & _RoadUser.DataPoint(_Frame).Y.ToString("0.000", CultureInfo.InvariantCulture)
        If MyParentRecord.Map IsNot Nothing Then
          _Line = _Line & ";" & MyParentRecord.Map.EstimateHeight(_RoadUser.DataPoint(_Frame).X, _RoadUser.DataPoint(_Frame).Y).ToString("0.000", CultureInfo.InvariantCulture)
        Else
          _Line = _Line & ";0.000"
        End If
        _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).Dx.ToString("0.00000", CultureInfo.InvariantCulture) & ";" & _RoadUser.DataPoint(_Frame).Dy.ToString("0.00000", CultureInfo.InvariantCulture)
        If IsValue(_RoadUser.DataPoint(_Frame).V) Then _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).V.ToString("0.000", CultureInfo.InvariantCulture) Else _Line = _Line & ";-"
        If IsValue(_RoadUser.DataPoint(_Frame).A) Then _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).A.ToString("0.000", CultureInfo.InvariantCulture) Else _Line = _Line & ";-"
        If IsValue(_RoadUser.DataPoint(_Frame).J) Then _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).J.ToString("0.000", CultureInfo.InvariantCulture) Else _Line = _Line & ";-"

        If _RoadUser.DataPoint(_Frame).OriginalXYExists Then
          _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).Xpxl.ToString("0.000", CultureInfo.InvariantCulture) & ";" & _RoadUser.DataPoint(_Frame).Ypxl.ToString("0.000", CultureInfo.InvariantCulture)
          If MyParentRecord.Map IsNot Nothing Then
            _Line = _Line & ";" & MyParentRecord.Map.EstimateHeight(_RoadUser.DataPoint(_Frame).Xpxl, _RoadUser.DataPoint(_Frame).Ypxl).ToString("0.000", CultureInfo.InvariantCulture)
          Else
            _Line = _Line & ";0.000"
          End If
          _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).Dxpxl.ToString("0.00000", CultureInfo.InvariantCulture) & ";" & _RoadUser.DataPoint(_Frame).Dypxl.ToString("0.00000", CultureInfo.InvariantCulture)
        Else
          _Line = _Line & ";-;-;-;-;-"
        End If

        'video image positions (smoothed)
        _i = 0
        For Each _VideoFile In MyParentRecord.VideoFiles
          _i = _i + 1
          _Line = _Line & ";" & (MyParentRecord.TimeLine(_Frame).VideoFileFrames(_i - 1) - _StartVideoFrames(_i - 1)).ToString("00000") & " " &
                                 Format(_VideoFile.GetTimeStampDate(MyParentRecord.TimeLine(_Frame).VideoFileFrames(_i - 1)), "yyyy MM dd HH:mm:ss.fff")
          If _VideoFile.CallibrationOK Then
            _Point = _VideoFile.TransformWorldToImage(_RoadUser.DataPoint(_Frame).X, _RoadUser.DataPoint(_Frame).Y)
            If IsBetween(_Point.X, 0, _VideoFile.FrameSize.Width - 1) And IsBetween(_Point.Y, 0, _VideoFile.FrameSize.Height - 1) Then
              _Line = _Line & ";" & _Point.X & ";" & _Point.Y
            Else
              _Line = _Line & ";-;-"
            End If
          End If
        Next
        PrintLine(_FileIndex, _Line)
      Next

      FileClose(_FileIndex)
    Next

  End Sub


  Public Sub ExportClickedPositions()
    Dim _ExportDirectory As String
    Dim _i, _j As Integer
    Dim _FileIndex As Integer
    Dim _Line As String


    _ExportDirectory = MyParentRecord.ExportDirectory & "Ground truth\" '& Format(MyParentRecord.DateTime, "yyyyMMdd-HHmm")

    Call SaveVideoSequence(_ExportDirectory & MyParentRecord.ID.ToString("00000") & ".avi", 0, MyParentRecord.FrameCount - 1)

    _j = 0
    For Each _RoadUser In MyParentRecord.RoadUsers
      _j = _j + 1
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _ExportDirectory & MyParentRecord.ID.ToString("00000") & "_RoadUser_" & _j.ToString & ".txt", OpenMode.Output)
      PrintLine(_FileIndex, "Type: " & _RoadUser.Type.Name)
      PrintLine(_FileIndex, "Length, m: " & _RoadUser.Length.ToString("0.0", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Width, m: " & _RoadUser.Width.ToString("0.0", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Height, m: " & _RoadUser.Height.ToString("0.0", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Weight, kg: " & _RoadUser.Weight.ToString("0", CultureInfo.InvariantCulture))
      _Line = "Time; X_clicked, m; Y_clicked, m; Z_estimated, m"
      ' _i = 0
      'For Each _VideoFile In MyParentRecord.VideoFiles
      '  _i = _i + 1
      '  If _VideoFile.Visible Then
      '    _Line = _Line & "; Camera_" & _i.ToString & "; X, pxl; Y, pxl"
      '  End If
      'Next
      PrintLine(_FileIndex, _Line)


      For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
        If IsValue(_RoadUser.DataPoint(_Frame).Xpxl) And IsValue(_RoadUser.DataPoint(_Frame).Ypxl) Then
          _Line = Format(Date.FromOADate((MyParentRecord.GetAbsoluteTime(_Frame)) / 24 / 60 / 60), "yyyy-MM-dd;HH:mm:ss.fff")
          _Line = _Line & ";" & _RoadUser.DataPoint(_Frame).Xpxl.ToString("0.000", CultureInfo.InvariantCulture) & ";" &
                  _RoadUser.DataPoint(_Frame).Ypxl.ToString("0.000", CultureInfo.InvariantCulture)
          If MyParentRecord.Map IsNot Nothing Then
            _Line = _Line & ";" & MyParentRecord.Map.EstimateHeight(_RoadUser.DataPoint(_Frame).Xpxl, _RoadUser.DataPoint(_Frame).Ypxl).ToString("0.000", CultureInfo.InvariantCulture)
          Else
            _Line = _Line & ";0.000"
          End If
          PrintLine(_FileIndex, _Line)
        End If
      Next

      FileClose(_FileIndex)
    Next
  End Sub
#End Region





#Region "Private subs"

  Private Sub DrawRoadUser(ByRef G As Graphics,
                           ByVal RoadUser As clsRoadUser,
                           ByVal Frame As Integer,
                           ByVal CameraIndex As Integer,
                           ByVal IDstring As String,
                           ByVal PenRoadUser As Pen,
                           ByVal PenTrajectory As Pen,
                           ByVal SolidBrushID As SolidBrush,
                           ByVal ImageSize As Size)

    Dim _RoadUserPoints As RoadUserGraphicPointsStructure


    _RoadUserPoints = RoadUserPoints(RoadUser, Frame, CameraIndex)

    If IsBetween(_RoadUserPoints.Centre.X, 1, ImageSize.Width - 2) And IsBetween(_RoadUserPoints.Centre.Y, 1, ImageSize.Height - 2) Then
      'Draw road user
      If MyVisualiserOptions.DrawRoadUsers Then
        If _RoadUserPoints.Available Then PenRoadUser.DashStyle = Drawing2D.DashStyle.Solid Else PenRoadUser.DashStyle = Drawing2D.DashStyle.Dot

        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP1, _RoadUserPoints.LeftP2)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP2, _RoadUserPoints.LeftP3)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP3, _RoadUserPoints.LeftP4)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP4, _RoadUserPoints.LeftP5)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP5, _RoadUserPoints.LeftP6)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP6, _RoadUserPoints.LeftP7)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP7, _RoadUserPoints.LeftP8)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP8, _RoadUserPoints.LeftP1)

        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP1, _RoadUserPoints.RightP2)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP2, _RoadUserPoints.RightP3)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP3, _RoadUserPoints.RightP4)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP4, _RoadUserPoints.RightP5)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP5, _RoadUserPoints.RightP6)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP6, _RoadUserPoints.RightP7)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP7, _RoadUserPoints.RightP8)
        G.DrawLine(PenRoadUser, _RoadUserPoints.RightP8, _RoadUserPoints.RightP1)

        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP1, _RoadUserPoints.RightP1)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP2, _RoadUserPoints.RightP2)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP3, _RoadUserPoints.RightP3)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP4, _RoadUserPoints.RightP4)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP5, _RoadUserPoints.RightP5)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP6, _RoadUserPoints.RightP6)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP7, _RoadUserPoints.RightP7)
        G.DrawLine(PenRoadUser, _RoadUserPoints.LeftP8, _RoadUserPoints.RightP8)
      End If

      'Draw ID
      If MyVisualiserOptions.DrawID Then
        G.DrawString(IDstring, MyVisualiserOptions.TextFontID, SolidBrushID, _RoadUserPoints.Centre)
      End If

      'Draw trajectory
      If MyVisualiserOptions.DrawTrajectories Then
        For Each _TrajectoryPoint In _RoadUserPoints.Trajectory
          G.DrawEllipse(PenTrajectory, _TrajectoryPoint.X, _TrajectoryPoint.Y, 1, 1)
        Next
      End If

    End If
  End Sub


  Private Sub DrawMessagePoint(ByRef G As Graphics, ByVal DisplayIndex As Integer, ByVal MessagePoint As clsMessagePoint)
    Const RADIUS = 2

    Dim _Point As Point
    Dim _Pen As Pen
    Dim _Rect As Rectangle
    Dim _TextBrush As SolidBrush


    _Pen = New Pen(MessagePoint.Colour)
    _TextBrush = New SolidBrush(MessagePoint.Colour)

    With MyParentRecord
      Select Case DisplayIndex
        Case DisplayIndices.Projected
          _Point = .Map.TransformWorldToImage(MessagePoint.X, MessagePoint.Y)
          _Rect = New Rectangle(_Point.X - RADIUS, _Point.Y - RADIUS, 2 * RADIUS, 2 * RADIUS)

        Case Else
          _Point = .VideoFiles(DisplayIndex).TransformWorldToImage(MessagePoint.X, MessagePoint.Y)
          _Rect = New Rectangle(_Point.X - RADIUS, _Point.Y - RADIUS, 2 * RADIUS, 2 * RADIUS)
      End Select
    End With

    G.DrawEllipse(_Pen, _Rect)
    G.DrawString(MessagePoint.Message, VisualiserOptions.TextFontMessage, _TextBrush, _Point)
  End Sub


  Private Sub DrawMessageLine(ByRef G As Graphics, ByVal DisplayIndex As Integer, ByVal MessageLine As clsMessageLine)
    Dim _Point1 As Point
    Dim _Point2 As Point
    Dim _Pen As Pen
    Dim _TextBrush As SolidBrush
    Dim _PointString As Point
    Dim _TempPointsCount As Integer
    Dim _X, _Y As Double

    _Pen = New Pen(MessageLine.Colour, MessageLine.LineWidth)
    _TextBrush = New SolidBrush(MessageLine.Colour)

    With MyParentRecord
      Select Case DisplayIndex
        Case DisplayIndices.Projected
          _Point1 = .Map.TransformWorldToImage(MessageLine.X1, MessageLine.Y1)
          _Point2 = .Map.TransformWorldToImage(MessageLine.X2, MessageLine.Y2)
          G.DrawLine(_Pen, _Point1, _Point2)

        Case Else
          _TempPointsCount = CInt(Int(Math.Sqrt((MessageLine.X1 - MessageLine.X2) ^ 2 + (MessageLine.Y1 - MessageLine.Y2) ^ 2) / 0.2)) + 1

          _Point1 = .VideoFiles(DisplayIndex).TransformWorldToImage(MessageLine.X1, MessageLine.Y1)

          For _i = 1 To _TempPointsCount
            _X = Interpolate(_i, 0, MessageLine.X1, _TempPointsCount, MessageLine.X2)
            _Y = Interpolate(_i, 0, MessageLine.Y1, _TempPointsCount, MessageLine.Y2)
            _Point2 = .VideoFiles(DisplayIndex).TransformWorldToImage(_X, _Y)
            G.DrawLine(_Pen, _Point1, _Point2)
            _Point1 = _Point2
          Next _i

          _Point1 = .VideoFiles(DisplayIndex).TransformWorldToImage(MessageLine.X1, MessageLine.Y1)
      End Select
    End With

    'If _Point1.Y > _Point2.Y Then
    '  _PointString = _Point1
    'Else
    '  _PointString = _Point2
    'End If
    _PointString = New Point(CInt((_Point1.X + _Point2.X) / 2), CInt((_Point1.Y + _Point2.Y) / 2))

    G.DrawString(MessageLine.Message, VisualiserOptions.TextFontMessage, _TextBrush, _PointString)
  End Sub


  Private Function RoadUserPoints(ByVal RoadUser As clsRoadUser,
                                  ByVal Frame As Integer,
                                  ByVal DisplayIndex As Integer) As RoadUserGraphicPointsStructure

    Dim _RoadUserPoints As RoadUserGraphicPointsStructure
    Dim _X, _Y As Double
    Dim _DxDy As clsDxDy
    Dim _Position As clsDPoint
    Dim _3Dmodel As clsRoadUserModel.RoadUser3DmodelStructure
    Dim _i, _iStart, _iEnd As Integer

    _RoadUserPoints.Trajectory = New List(Of Point)

    _i = 0
    _DxDy = Nothing

    Select Case Me.VisualiserOptions.TrajectoryType
      Case TrajectoryTypes.Original
        If IsBetween(Frame, RoadUser.FirstFrame, RoadUser.LastFrame) Then
          Do Until RoadUser.DataPoint(Frame - _i).OriginalXYExists
            _i = _i + 1
          Loop
          If _i = 0 Then _RoadUserPoints.Available = True Else _RoadUserPoints.Available = False
          _X = RoadUser.DataPoint(Frame - _i).Xpxl
          _Y = RoadUser.DataPoint(Frame - _i).Ypxl
          _DxDy = RoadUser.DataPoint(Frame - _i).DxDypxl
          _iStart = Frame
          _iEnd = RoadUser.LastFrame
        ElseIf Frame < RoadUser.FirstFrame Then
          _RoadUserPoints.Available = False
          _X = RoadUser.DataPoint(RoadUser.FirstFrame).Xpxl
          _Y = RoadUser.DataPoint(RoadUser.FirstFrame).Ypxl
          _DxDy = RoadUser.DataPoint(RoadUser.FirstFrame).DxDypxl
          _iStart = RoadUser.FirstFrame
          _iEnd = RoadUser.LastFrame
        ElseIf Frame > RoadUser.LastFrame Then
          _RoadUserPoints.Available = False
          _X = RoadUser.DataPoint(RoadUser.LastFrame).Xpxl
          _Y = RoadUser.DataPoint(RoadUser.LastFrame).Ypxl
          _DxDy = RoadUser.DataPoint(RoadUser.LastFrame).DxDypxl
          _iStart = RoadUser.LastFrame
          _iEnd = RoadUser.FirstFrame
        End If


      Case TrajectoryTypes.Smoothed
        If IsBetween(Frame, RoadUser.FirstFrame, RoadUser.LastFrame) Then
          _RoadUserPoints.Available = True
          _X = RoadUser.DataPoint(Frame).X
          _Y = RoadUser.DataPoint(Frame).Y
          _DxDy = RoadUser.DataPoint(Frame).DxDy
          _iStart = Frame
          _iEnd = RoadUser.LastFrame
        ElseIf Frame < RoadUser.FirstFrame Then
          _RoadUserPoints.Available = False
          _X = RoadUser.DataPoint(RoadUser.FirstFrame).X
          _Y = RoadUser.DataPoint(RoadUser.FirstFrame).Y
          _DxDy = RoadUser.DataPoint(RoadUser.FirstFrame).DxDy
          _iStart = RoadUser.FirstFrame
          _iEnd = RoadUser.LastFrame
        ElseIf Frame > RoadUser.LastFrame Then
          _RoadUserPoints.Available = False
          _X = RoadUser.DataPoint(RoadUser.LastFrame).X
          _Y = RoadUser.DataPoint(RoadUser.LastFrame).Y
          _DxDy = RoadUser.DataPoint(RoadUser.LastFrame).DxDy
          _iStart = RoadUser.LastFrame
          _iEnd = RoadUser.FirstFrame
        End If
    End Select


    _Position = New clsDPoint(_X, _Y, _DxDy)
    _3Dmodel = RoadUser.RoadUser3Dmodel(_Position)

    Select Case DisplayIndex
      Case DisplayIndices.Projected
        With MyParentRecord
          _RoadUserPoints.LeftP1 = .Map.TransformWorldToImage(_3Dmodel.LeftP1.X, _3Dmodel.LeftP1.Y)
          _RoadUserPoints.LeftP2 = .Map.TransformWorldToImage(_3Dmodel.LeftP2.X, _3Dmodel.LeftP2.Y)
          _RoadUserPoints.LeftP3 = .Map.TransformWorldToImage(_3Dmodel.LeftP3.X, _3Dmodel.LeftP3.Y)
          _RoadUserPoints.LeftP4 = .Map.TransformWorldToImage(_3Dmodel.LeftP4.X, _3Dmodel.LeftP4.Y)
          _RoadUserPoints.LeftP5 = .Map.TransformWorldToImage(_3Dmodel.LeftP5.X, _3Dmodel.LeftP5.Y)
          _RoadUserPoints.LeftP6 = .Map.TransformWorldToImage(_3Dmodel.LeftP6.X, _3Dmodel.LeftP6.Y)
          _RoadUserPoints.LeftP7 = .Map.TransformWorldToImage(_3Dmodel.LeftP7.X, _3Dmodel.LeftP7.Y)
          _RoadUserPoints.LeftP8 = .Map.TransformWorldToImage(_3Dmodel.LeftP8.X, _3Dmodel.LeftP8.Y)

          _RoadUserPoints.RightP1 = .Map.TransformWorldToImage(_3Dmodel.RightP1.X, _3Dmodel.RightP1.Y)
          _RoadUserPoints.RightP2 = .Map.TransformWorldToImage(_3Dmodel.RightP2.X, _3Dmodel.RightP2.Y)
          _RoadUserPoints.RightP3 = .Map.TransformWorldToImage(_3Dmodel.RightP3.X, _3Dmodel.RightP3.Y)
          _RoadUserPoints.RightP4 = .Map.TransformWorldToImage(_3Dmodel.RightP4.X, _3Dmodel.RightP4.Y)
          _RoadUserPoints.RightP5 = .Map.TransformWorldToImage(_3Dmodel.RightP5.X, _3Dmodel.RightP5.Y)
          _RoadUserPoints.RightP6 = .Map.TransformWorldToImage(_3Dmodel.RightP6.X, _3Dmodel.RightP6.Y)
          _RoadUserPoints.RightP7 = .Map.TransformWorldToImage(_3Dmodel.RightP7.X, _3Dmodel.RightP7.Y)
          _RoadUserPoints.RightP8 = .Map.TransformWorldToImage(_3Dmodel.RightP8.X, _3Dmodel.RightP8.Y)
          _RoadUserPoints.Centre = .Map.TransformWorldToImage(_3Dmodel.Centre.X, _3Dmodel.Centre.Y)

          If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Original Then
            For _i = _iStart To _iEnd
              If RoadUser.DataPoint(_i).OriginalXYExists Then _
                _RoadUserPoints.Trajectory.Add(MyParentRecord.Map.TransformWorldToImage(RoadUser.DataPoint(_i).Xpxl, RoadUser.DataPoint(_i).Ypxl))
            Next
          ElseIf Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
            For _i = _iStart To _iEnd
              _RoadUserPoints.Trajectory.Add(MyParentRecord.Map.TransformWorldToImage(RoadUser.DataPoint(_i).X, RoadUser.DataPoint(_i).Y))
            Next
          End If
        End With

      Case Else
        With MyParentRecord
          _RoadUserPoints.LeftP1 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP1.X, _3Dmodel.LeftP1.Y, -_3Dmodel.LeftP1.Z + .Map.EstimateHeight(_3Dmodel.LeftP1.X, _3Dmodel.LeftP1.Y))
          _RoadUserPoints.LeftP2 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP2.X, _3Dmodel.LeftP2.Y, -_3Dmodel.LeftP2.Z + .Map.EstimateHeight(_3Dmodel.LeftP2.X, _3Dmodel.LeftP2.Y))
          _RoadUserPoints.LeftP3 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP3.X, _3Dmodel.LeftP3.Y, -_3Dmodel.LeftP3.Z + .Map.EstimateHeight(_3Dmodel.LeftP3.X, _3Dmodel.LeftP3.Y))
          _RoadUserPoints.LeftP4 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP4.X, _3Dmodel.LeftP4.Y, -_3Dmodel.LeftP4.Z + .Map.EstimateHeight(_3Dmodel.LeftP4.X, _3Dmodel.LeftP4.Y))
          _RoadUserPoints.LeftP5 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP5.X, _3Dmodel.LeftP5.Y, -_3Dmodel.LeftP5.Z + .Map.EstimateHeight(_3Dmodel.LeftP5.X, _3Dmodel.LeftP5.Y))
          _RoadUserPoints.LeftP6 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP6.X, _3Dmodel.LeftP6.Y, -_3Dmodel.LeftP6.Z + .Map.EstimateHeight(_3Dmodel.LeftP6.X, _3Dmodel.LeftP6.Y))
          _RoadUserPoints.LeftP7 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP7.X, _3Dmodel.LeftP7.Y, -_3Dmodel.LeftP7.Z + .Map.EstimateHeight(_3Dmodel.LeftP7.X, _3Dmodel.LeftP7.Y))
          _RoadUserPoints.LeftP8 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.LeftP8.X, _3Dmodel.LeftP8.Y, -_3Dmodel.LeftP8.Z + .Map.EstimateHeight(_3Dmodel.LeftP8.X, _3Dmodel.LeftP8.Y))

          _RoadUserPoints.RightP1 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP1.X, _3Dmodel.RightP1.Y, -_3Dmodel.RightP1.Z + .Map.EstimateHeight(_3Dmodel.RightP1.X, _3Dmodel.RightP1.Y))
          _RoadUserPoints.RightP2 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP2.X, _3Dmodel.RightP2.Y, -_3Dmodel.RightP2.Z + .Map.EstimateHeight(_3Dmodel.RightP2.X, _3Dmodel.RightP2.Y))
          _RoadUserPoints.RightP3 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP3.X, _3Dmodel.RightP3.Y, -_3Dmodel.RightP3.Z + .Map.EstimateHeight(_3Dmodel.RightP3.X, _3Dmodel.RightP3.Y))
          _RoadUserPoints.RightP4 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP4.X, _3Dmodel.RightP4.Y, -_3Dmodel.RightP4.Z + .Map.EstimateHeight(_3Dmodel.RightP4.X, _3Dmodel.RightP4.Y))
          _RoadUserPoints.RightP5 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP5.X, _3Dmodel.RightP5.Y, -_3Dmodel.RightP5.Z + .Map.EstimateHeight(_3Dmodel.RightP5.X, _3Dmodel.RightP5.Y))
          _RoadUserPoints.RightP6 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP6.X, _3Dmodel.RightP6.Y, -_3Dmodel.RightP6.Z + .Map.EstimateHeight(_3Dmodel.RightP6.X, _3Dmodel.RightP6.Y))
          _RoadUserPoints.RightP7 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP7.X, _3Dmodel.RightP7.Y, -_3Dmodel.RightP7.Z + .Map.EstimateHeight(_3Dmodel.RightP7.X, _3Dmodel.RightP7.Y))
          _RoadUserPoints.RightP8 = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.RightP8.X, _3Dmodel.RightP8.Y, -_3Dmodel.RightP8.Z + .Map.EstimateHeight(_3Dmodel.RightP8.X, _3Dmodel.RightP8.Y))
          _RoadUserPoints.Centre = .VideoFiles(DisplayIndex).TransformWorldToImage(_3Dmodel.Centre.X, _3Dmodel.Centre.Y, -_3Dmodel.Centre.Z + .Map.EstimateHeight(_3Dmodel.Centre.X, _3Dmodel.Centre.Y))

          If Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Original Then
            For _i = _iStart To _iEnd
              If RoadUser.DataPoint(_i).OriginalXYExists Then _
                _RoadUserPoints.Trajectory.Add(.VideoFiles(DisplayIndex).TransformWorldToImage(RoadUser.DataPoint(_i).Xpxl, RoadUser.DataPoint(_i).Ypxl, .Map.EstimateHeight(RoadUser.DataPoint(_i).Xpxl, RoadUser.DataPoint(_i).Ypxl)))
            Next
          ElseIf Me.VisualiserOptions.TrajectoryType = TrajectoryTypes.Smoothed Then
            For _i = _iStart To _iEnd
              _RoadUserPoints.Trajectory.Add(.VideoFiles(DisplayIndex).TransformWorldToImage(RoadUser.DataPoint(_i).X, RoadUser.DataPoint(_i).Y, .Map.EstimateHeight(RoadUser.DataPoint(_i).X, RoadUser.DataPoint(_i).Y)))
            Next
          End If
        End With
    End Select

    Return _RoadUserPoints
  End Function

#End Region



End Class
