﻿Public Module modIndicators_



  Public Sub CrossingCourse_(ByVal AlongTrajectory As Boolean,
                            ByVal RoadUser1 As clsRoadUser, ByVal RoadUser2 As clsRoadUser,
                            ByVal CurrentFrame As Integer,
                            ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double,
                            ByRef SpeedVectorDxDy1 As clsDxDy, ByRef SpeedVectorDxDy2 As clsDxDy,
                            ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double,
                            Optional ByVal TrimFrames As Integer = 0)

    Const TIME_ERROR = 0.3
    Const TRAVEL_MIN = 0.2

    Dim _X1, _Y1 As Double
    Dim _m1 As Integer
    Dim _V1, _A1, _J1 As Double
    Dim _DxDy1 As clsDxDy
    Dim _Position1 As clsXPoint

    Dim _X2, _Y2 As Double

    Dim _m2 As Integer
    Dim _V2, _A2, _J2 As Double
    Dim _DxDy2 As clsDxDy
    Dim _Position2 As clsXPoint


    Dim _Frame1, _Frame2 As Integer
    Dim _Time1, _Time2 As Double
    Dim _FrameMax1, _FrameMax2 As Integer
    Dim _Tlim11, _Tlim12 As Double
    Dim _Tlim21, _Tlim22 As Double
    Dim _Travel1, _TravelSum1 As Double
    Dim _Travel2, _TravelSum2 As Double

    Dim _MinDistance As Double
    Dim _TTC_TA_found, _TG_found As Boolean

    Dim _TTCtemp As Double
    Dim _TA1temp, _TA2temp As Double

    Dim _X_TTCtemp, _Y_TTCtemp As Double
    Dim _X_TAtemp, _Y_TAtemp As Double


    TTC = NoValue
    X_TTC = NoValue : Y_TTC = NoValue
    TA1 = NoValue : TA2 = NoValue
    X_TA = NoValue : Y_TA = NoValue

    _FrameMax1 = RoadUser1.LastFrame - TrimFrames
    _FrameMax2 = RoadUser2.LastFrame - TrimFrames
    If IsBetween(CurrentFrame, RoadUser1.FirstFrame + TrimFrames, _FrameMax1 - 1) And IsBetween(CurrentFrame, RoadUser2.FirstFrame + TrimFrames, _FrameMax2 - 1) Then
      _TTC_TA_found = False

      _m1 = RoadUser1.Weight
      _V1 = RoadUser1.DataPoint(CurrentFrame).V
      _A1 = RoadUser1.DataPoint(CurrentFrame).A
      _J1 = RoadUser1.DataPoint(CurrentFrame).J
      _FrameMax1 = RoadUser1.LastFrame - TrimFrames

      _m2 = RoadUser2.Weight
      _V2 = RoadUser2.DataPoint(CurrentFrame).V
      _A2 = RoadUser2.DataPoint(CurrentFrame).A
      _J2 = RoadUser2.DataPoint(CurrentFrame).J


      If Not AlongTrajectory Then
        '   Call CrossingCourseStraight(RoadUser1, RoadUser1.DataPoint(CurrentFrame), RoadUser2, RoadUser2.DataPoint(CurrentFrame), TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA)
        SpeedVectorDxDy1 = RoadUser1.DataPoint(CurrentFrame).DxDy
        SpeedVectorDxDy2 = RoadUser2.DataPoint(CurrentFrame).DxDy
      Else
        'This makes that TTC, etc. are calculated only when RUs are near the collision point at distance _Diag
        _MinDistance = 10 + 0.5 * (Math.Sqrt(RoadUser1.Width ^ 2 + RoadUser1.Length ^ 2) + Math.Sqrt(RoadUser2.Width ^ 2 + RoadUser2.Length ^ 2))

        _Travel1 = 0 'used to avoid calculations when road user is not moving, i.e. travel distance close to 0
        _TravelSum1 = 0
        _Tlim12 = TIME_ERROR

        For _Frame1 = CurrentFrame + 1 To _FrameMax1
          _Travel1 = _Travel1 + RoadUser1.DataPoint(_Frame1 - 1).DistanceTo(RoadUser1.DataPoint(_Frame1))
          If _Travel1 > TRAVEL_MIN Then
            _TravelSum1 = _TravelSum1 + _Travel1
            _Travel1 = 0

            _Time1 = RoadUser1.ParentRecord.GetLocalTime(_Frame1)
            _Tlim11 = _Tlim12 - 2 * TIME_ERROR
            _Tlim12 = _TravelSum1 / _V1 + TIME_ERROR

            _DxDy1 = RoadUser1.DataPoint(_Frame1 - 1).DxDy
            ' _DxDy1 = New clsDxDy()
            _X1 = RoadUser1.DataPoint(_Frame1).X - _TravelSum1 * _DxDy1.Dx
            _Y1 = RoadUser1.DataPoint(_Frame1).Y - _TravelSum1 * _DxDy1.Dy
            _Position1 = New clsXPoint(_X1, _Y1, _DxDy1, _V1, _A1, _J1)

            _TravelSum2 = 0
            _Travel2 = 0
            _Tlim22 = TIME_ERROR

            For _Frame2 = CurrentFrame + 1 To _FrameMax2
              _Travel2 = _Travel2 + RoadUser2.DataPoint(_Frame2 - 1).DistanceTo(RoadUser2.DataPoint(_Frame2))
              If _Travel2 > TRAVEL_MIN Then
                _TravelSum2 = _TravelSum2 + _Travel2
                _Travel2 = 0

                _Time2 = RoadUser2.ParentRecord.GetLocalTime(_Frame2)
                _Tlim21 = _Tlim22 - 2 * TIME_ERROR
                _Tlim22 = _TravelSum2 / _V2 + TIME_ERROR

                _DxDy2 = RoadUser2.DataPoint(_Frame2 - 1).DxDy
                _X2 = RoadUser2.DataPoint(_Frame2).X - _TravelSum2 * _DxDy2.Dx
                _Y2 = RoadUser2.DataPoint(_Frame2).Y - _TravelSum2 * _DxDy2.Dy
                _Position2 = New clsXPoint(_X2, _Y2, _DxDy2, _V2, _A2, _J2)

                'MinDistance
                If IsLessOrEqual(RoadUser1.DataPoint(_Frame1).DistanceTo(RoadUser2.DataPoint(_Frame2)), _MinDistance) Then

                  '    Call CrossingCourseStraight(RoadUser1, _Position1, RoadUser2, _Position2, _TTCtemp, _X_TTCtemp, _Y_TTCtemp, _TA1temp, _TA2temp, _X_TAtemp, _Y_TAtemp)

                  If Not _TTC_TA_found Then
                    If IsValue(_TTCtemp) Then
                      If IsBetween(_TTCtemp, _Tlim11, _Tlim12) And IsBetween(_TTCtemp, _Tlim21, _Tlim22) Then
                        _TTC_TA_found = True
                        TTC = _TTCtemp
                        X_TTC = _X_TTCtemp
                        Y_TTC = _Y_TTCtemp
                        SpeedVectorDxDy1 = _Position1.DxDy
                        SpeedVectorDxDy2 = _Position2.DxDy
                        If _TG_found Then Exit For
                      End If
                    ElseIf IsValue(_TA1temp) And IsValue(_TA2temp) Then
                      If IsBetween(_TA1temp, _Tlim11, _Tlim12) And IsBetween(_TA2temp, _Tlim21, _Tlim22) Then
                        _TTC_TA_found = True
                        TA1 = _TA1temp
                        TA2 = _TA2temp
                        X_TA = _X_TAtemp
                        Y_TA = _Y_TAtemp
                        SpeedVectorDxDy1 = _Position1.DxDy
                        SpeedVectorDxDy2 = _Position2.DxDy

                        If _TG_found Then Exit For
                      End If
                    End If 'IsValue(TTC)
                  End If '_TTC_TA_found

                End If '_MinDistance
              End If '_Travel2
            Next _Frame2 'Loop
          End If 'Traveli > 1

          If _TG_found And _TTC_TA_found Then Exit For

        Next _Frame1 'Loop
      End If
    End If

  End Sub

End Module
