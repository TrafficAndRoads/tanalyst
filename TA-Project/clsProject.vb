﻿Public Class clsProject
  Inherits clsDatabase

  Private MyProjectFilePath As String
  Private MyProjectName As String
  Private MyCreated As DateTime
  Private MyLastOpened As DateTime
  Private MyProgramVersion As System.Version
  Private MyTrajectoryDirectory As String
  Private MyMediaDirectory As String
  Private MyMapDirectory As String
  Private MyVideoDirectory As String
  Private MyImportDirectory As String
  Private MyExportDirectory As String
  Private MyLayoutMode As ProjectLayoutModes
  Private MyVisualiserOptions As clsVisualiserOptions
  Private MyRoadUserTypes As List(Of clsRoadUserType)
  Private MyCurrentRecordVR As clsRecord
  Private MyCurrentRecordDT As clsRecord



#Region "Public properties"

  Public ReadOnly Property ProjectFilePath() As String
    Get
      ProjectFilePath = MyProjectFilePath
    End Get
  End Property

  Public ReadOnly Property ProjectName() As String
    Get
      ProjectName = MyProjectName
    End Get
  End Property

  Public ReadOnly Property TrajectoryDirectory As String
    Get
      Return MyTrajectoryDirectory
    End Get
  End Property

  Public ReadOnly Property MediaDirectory As String
    Get
      Return MyMediaDirectory
    End Get
  End Property

  Public ReadOnly Property MapDirectory As String
    Get
      Return MyMapDirectory
    End Get
  End Property

  Public ReadOnly Property VideoDirectory As String
    Get
      Return MyVideoDirectory
    End Get
  End Property

  Public ReadOnly Property ImportDirectory As String
    Get
      Return MyImportDirectory
    End Get
  End Property

  Public ReadOnly Property ExportDirectory As String
    Get
      Return MyExportDirectory
    End Get
  End Property
 
  Public Property LayoutMode As ProjectLayoutModes
    Get
      Return MyLayoutMode
    End Get
    Set(value As ProjectLayoutModes)
      MyLayoutMode = value
    End Set
  End Property

  Public ReadOnly Property VisualiserOptions As clsVisualiserOptions
    Get
      VisualiserOptions = MyVisualiserOptions
    End Get
  End Property

  Public ReadOnly Property CurrentRecordVR As clsRecord
    Get
      CurrentRecordVR = MyCurrentRecordVR
    End Get
  End Property

  Public ReadOnly Property CurrentRecordDT As clsRecord
    Get
      CurrentRecordDT = MyCurrentRecordDT
    End Get
  End Property

  Public ReadOnly Property CurrentRecord As clsRecord
    Get
      Select Case MyLayoutMode
        Case ProjectLayoutModes.VideoRecordings
          Return MyCurrentRecordVR
        Case ProjectLayoutModes.Detections
          Return MyCurrentRecordDT
        Case Else
          Return Nothing
      End Select
    End Get
  End Property

  Public Shadows ReadOnly Property RoadUserTypes As List(Of clsRoadUserType)
    Get
      Return MyRoadUserTypes
    End Get
  End Property

  Public Shadows Property FilterVR As String
    Get
      Return MyBase.FilterVR
    End Get
    Set(ByVal value As String)
      MyBase.FilterVR = value
      MyCurrentRecordVR = Nothing
    End Set
  End Property

  Public Shadows Property FilterDT As String
    Get
      Return MyBase.FilterDT
    End Get
    Set(ByVal value As String)
      MyBase.FilterDT = value
      MyCurrentRecordDT = Nothing
    End Set
  End Property

#End Region



#Region "Public methods"

  Public Function CreateNewProject(ProjectName As String, ProjectDirectory As String) As Boolean

    If Not Directory.Exists(ProjectDirectory & ProjectName & "\") Then
      Try
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Export\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Export\Ground truth\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Export\Images\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Export\Video\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Import\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Maps\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Trajectories\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Trajectories\Detections\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Trajectories\VideoRecordings\")
        Directory.CreateDirectory(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Video\")

        MyBase.CreateDB(ProjectDirectory & ProjectName & "\" & ProjectName & ".Data\Data.accdb")

        MyProjectFilePath = ProjectDirectory & ProjectName & "\" & ProjectName & ".taprj"
        MyCreated = Date.Now()
        MyVisualiserOptions = New clsVisualiserOptions
        MyLayoutMode = ProjectLayoutModes.VideoRecordings
        MyProgramVersion = My.Application.Info.Version
        Me.SaveProjectFile()
        Return True
      Catch
        Return False
      End Try
    Else
      MsgBox("Project '" & ProjectName & "' already exists!", MsgBoxStyle.Exclamation)
      Return False
    End If

  End Function



  Public Function Load(ProjectFilePath As String) As Boolean
    Dim _FileInfo As FileInfo
    Dim _DatabasePath As String
    Dim _ProjectDirectory As String

    MyProjectFilePath = ProjectFilePath
    _FileInfo = New FileInfo(ProjectFilePath)
    If _FileInfo.Exists Then
      MyProjectFilePath = ProjectFilePath
      MyProjectName = _FileInfo.Name.Substring(0, _FileInfo.Name.Length - 6)
      _ProjectDirectory = _FileInfo.DirectoryName & "\"
      _DatabasePath = _ProjectDirectory & MyProjectName & ".Data\Data.accdb"
      MyVideoDirectory = _ProjectDirectory & MyProjectName & ".Data\Video\"
      MyMapDirectory = _ProjectDirectory & MyProjectName & ".Data\Maps\"
      MyMediaDirectory = _ProjectDirectory & MyProjectName & ".Data\Media\"
      MyTrajectoryDirectory = _ProjectDirectory & MyProjectName & ".Data\Trajectories\"
      MyExportDirectory = _ProjectDirectory & MyProjectName & ".Data\Export\"
      MyImportDirectory = _ProjectDirectory & MyProjectName & ".Data\Import\"

      'Updates!!!!!!!!!!!!!
      If Not Directory.Exists(MyMediaDirectory) Then Directory.CreateDirectory(MyMediaDirectory)
      If Not Directory.Exists(MyMediaDirectory & "Video recordings") Then Directory.CreateDirectory(MyMediaDirectory & "Video recordings")
      If Not Directory.Exists(MyMediaDirectory & "Detections") Then Directory.CreateDirectory(MyMediaDirectory & "Detections")
      'Updates!!!!!!!!!!!!!

      If ConnectDB(_DatabasePath) Then
        MyRoadUserTypes = GetRoadUserTypes()
        If LoadProjectFile() Then
          Call SaveProjectFile()
          Return True
        End If
      End If
    End If

    Call Close()
    Return False
  End Function



  Public Sub Close()
    '  Call SaveProjectFile()
    Call DisconnectDB()
  End Sub



  Private Function LoadProjectFile() As Boolean
    Dim _FileIndex As Integer
    Dim _Line As String


    Try
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, MyProjectFilePath, OpenMode.Input)

      _Line = LineInput(_FileIndex)
      MyCreated = DateTime.Parse(_Line.Substring(30), CultureInfo.InvariantCulture)
      MyLastOpened = DateTime.Now
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      MyProgramVersion = System.Version.Parse(_Line.Substring(30))
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      MyLayoutMode = CType(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture), ProjectLayoutModes)

      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)

      MyVisualiserOptions = New clsVisualiserOptions
      With MyVisualiserOptions
        _Line = LineInput(_FileIndex)
        .DrawRoadUsers = Boolean.Parse(_Line.Substring(30))
        _Line = LineInput(_FileIndex)
        _Line = LineInput(_FileIndex)
        .NormalRUColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .SelectedRUColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .Key1RUColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .Key2RUColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .LineWidthRoadUser = Single.Parse(_Line.Substring(30), CultureInfo.InvariantCulture)
        _Line = LineInput(_FileIndex)
        _Line = LineInput(_FileIndex)
        .DrawTrajectories = Boolean.Parse(_Line.Substring(30))
        _Line = LineInput(_FileIndex)
        _Line = LineInput(_FileIndex)
        .NormalTrajectoryColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .SelectedTrajectoryColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .Key1TrajectoryColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .Key2TrajectoryColour = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .LineWidthTrajectory = Single.Parse(_Line.Substring(30), CultureInfo.InvariantCulture)
        _Line = LineInput(_FileIndex)
        _Line = LineInput(_FileIndex)
        .DrawID = Boolean.Parse(_Line.Substring(30))
        _Line = LineInput(_FileIndex)
        _Line = LineInput(_FileIndex)
        .TextColourIDNormal = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .TextColourIDSelected = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))
        _Line = LineInput(_FileIndex)
        .TextColourIDKey = Color.FromArgb(Integer.Parse(_Line.Substring(30), CultureInfo.InvariantCulture))

        '...
        FileClose(_FileIndex)
      End With
    Catch
      If MyProgramVersion Is Nothing Then MyProgramVersion = My.Application.Info.Version
      If Not ((MyLayoutMode = ProjectLayoutModes.VideoRecordings) Or (MyLayoutMode = ProjectLayoutModes.Detections)) Then MyLayoutMode = ProjectLayoutModes.VideoRecordings
      Return False
    End Try

    Return True
  End Function



  Public Sub SaveProjectFile()
    Dim _FileIndex As Integer


    _FileIndex = FreeFile()
    FileOpen(_FileIndex, MyProjectFilePath, OpenMode.Output)

    PrintLine(_FileIndex, "Created:", TAB(31), MyCreated.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Last opened:", TAB(31), MyLastOpened.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Program version:", TAB(31), MyProgramVersion.ToString)

    PrintLine(_FileIndex)
    PrintLine(_FileIndex)
    PrintLine(_FileIndex)
    PrintLine(_FileIndex)
    PrintLine(_FileIndex)
    PrintLine(_FileIndex, "Layout mode:", TAB(31), CInt(MyLayoutMode).ToString)
    PrintLine(_FileIndex)

    With MyVisualiserOptions
      PrintLine(_FileIndex, "Visualiser options")
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Draw road users:", TAB(31), .DrawRoadUsers.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Road user colour, normal:", TAB(31), .NormalRUColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Road user colour, selected:", TAB(31), .SelectedRUColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Road user colour, key1:", TAB(31), .Key1RUColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Road user colour, key2:", TAB(31), .Key2RUColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Road user line width:", TAB(31), .LineWidthRoadUser.ToString("0.00", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Draw trajectories:", TAB(31), .DrawTrajectories.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Trajectory colour, normal:", TAB(31), .NormalTrajectoryColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Trajectory colour, selected:", TAB(31), .SelectedTrajectoryColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Trajectory colour, key1:", TAB(31), .Key1TrajectoryColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Trajectory colour, key2:", TAB(31), .Key2TrajectoryColour.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Trajectory line width:", TAB(31), .LineWidthTrajectory.ToString("0.00", CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Draw ID:", TAB(31), .DrawID.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Text ID colour, normal:", TAB(31), .TextColourIDNormal.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text ID colour, selected:", TAB(31), .TextColourIDSelected.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text ID colour, key:", TAB(31), .TextColourIDKey.ToArgb.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text ID font name:", TAB(31), .TextFontID.Name)
      PrintLine(_FileIndex, "Text ID font size:", TAB(31), .TextFontID.Size.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text ID font style:", TAB(31), .TextFontID.Style.ToString)
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Draw message points:", TAB(31), .DrawMessagePoints.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Draw message lines:", TAB(31), .DrawMessageLines.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex)
      PrintLine(_FileIndex, "Text message font name:", TAB(31), .TextFontMessage.Name.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text message font size:", TAB(31), .TextFontMessage.Size.ToString(CultureInfo.InvariantCulture))
      PrintLine(_FileIndex, "Text message font style:", TAB(31), .TextFontMessage.Style.ToString)
    End With

    FileClose(_FileIndex)
  End Sub



  Public Sub ImportDataVR(ByVal FileNames() As String)

  End Sub

  Public Sub ImportDataDT(ByVal FileNames() As String)

  End Sub






  '' Public Function FirstRecordVR() As Boolean
  '  Return GotoRecordVR(0)
  'End Function

  'Public Function FirstRecordDT() As Boolean
  '  Return GotoRecordDT(0)
  'End Function



  'Public Function LastRecordVR() As Boolean
  '  Return GotoRecordVR(Me.NumberOfFilteredRecordsVR - 1)
  'End Function

  'Public Function LastRecordDT() As Boolean
  '  Return GotoRecordDT(Me.NumberOfFilteredRecordsDT - 1)
  'End Function



  'Public Function NextRecordVR() As Boolean
  '  Return GotoRecordVR(MyRecordIndexVR + 1)
  'End Function

  'Public Function NextRecordDT() As Boolean
  '  Return GotoRecordDT(MyRecordIndexDT + 1)
  'End Function



  'Public Function PreviousRecordVR() As Boolean
  '  Return GotoRecordVR(MyRecordIndexVR - 1)
  'End Function

  'Public Function PreviousRecordDT() As Boolean
  '  Return GotoRecordDT(MyRecordIndexDT - 1)
  'End Function



  'Public Sub AddRecordVR(ByVal FrameCount As Integer, ByVal FrameRate As Double, Optional ByVal Background As String = "", Optional ByVal Scale As Double = NoValue, Optional ByVal VideoFile As String = "", Optional ByVal VideoFileNameContainsDate As Boolean = False)
  'Dim _BackgroundName As String = ""

  'If Background.Length > 0 Then
  '  _BackgroundName = Path.GetFileName(Background)
  '  If File.Exists(Background) Then
  '    If Not File.Exists(clsRecord.BackgroundDirectory & _BackgroundName) Then FileCopy(Background, clsRecord.BackgroundDirectory & _BackgroundName)
  '  Else
  '    _BackgroundName = ""
  '    Scale = NoValue
  '  End If
  'End If
  'Call AddRecordDB(FrameCount, FrameRate, _BackgroundName, Scale, )
  'LoadRecord()
  'If VideoFile.Length > 0 Then
  '  If MyCurrentRecord.ImportVideoFile(VideoFile) Then
  '    If VideoFileNameContainsDate Then MyCurrentRecord.VideoFiles(0).TryConvertFileNameToDate(MyCurrentRecord.DateTime)
  '  End If
  '  Call Me.SaveCurrentRecord()
  'End If
  ' End Sub

  'Public Sub AddRecord7DT() 'ByVal FrameCount As Integer,
  'ByVal FrameRate As Double,
  'Optional ByVal Map As String = "",
  'Optional ByVal Scale As Double = NoValue,
  'Optional ByVal VideoFile As String = "",
  'Optional ByVal VideoFileNameContainsDate As Boolean = False)
  'Dim _BackgroundName As String = ""

  'If Background.Length > 0 Then
  '  _BackgroundName = Path.GetFileName(Background)
  '  If File.Exists(Background) Then
  '    If Not File.Exists(clsRecord.BackgroundDirectory & _BackgroundName) Then FileCopy(Background, clsRecord.BackgroundDirectory & _BackgroundName)
  '  Else
  '    _BackgroundName = ""
  '    Scale = NoValue
  '  End If
  'End If
  '  Call AddRecordDT(FrameCount, FrameRate) ', _BackgroundName, Scale, )
  'LoadRecord()
  'If VideoFile.Length > 0 Then
  '  If MyCurrentRecord.ImportVideoFile(VideoFile) Then
  '    If VideoFileNameContainsDate Then MyCurrentRecord.VideoFiles(0).TryConvertFileNameToDate(MyCurrentRecord.DateTime)
  '  End If
  '  Call Me.SaveCurrentRecord()
  'End If
  'End Sub



  Public Sub DeleteCurrentRecordVR()
    If MyCurrentRecordVR IsNot Nothing Then

      MyCurrentRecordVR.ClearRoadUsers()
      MyBase.DeleteRecordVR(MyCurrentRecordVR.ID)
      MyCurrentRecordVR = Nothing
    End If
  End Sub

  Public Sub DeleteCurrentRecordDT()
    If MyCurrentRecordDT IsNot Nothing Then

      MyCurrentRecordDT.ClearRoadUsers()
      MyBase.DeleteRecordDT(MyCurrentRecordDT.ID)
      MyCurrentRecordDT = Nothing
    End If
  End Sub





  Public Overloads Function ExportDetection(ByVal StartFrame As Integer, EndFrame As Integer) As Integer
    Dim ExportRoadUsers As List(Of clsRoadUser)


    ExportRoadUsers = MyCurrentRecordVR.RoadUsers
    Return DoExportDetection(StartFrame, EndFrame, ExportRoadUsers)
  End Function

  Public Overloads Sub ExportDetection(ByVal ExportAllRoadUsers As Boolean, ByVal ParamArray KeyRU() As Integer)

  End Sub

  Public Overloads Sub ExportDetection(ByVal KeyRoadUsers As List(Of clsRoadUser))
    'Dim _ExportRoadUser As clsRoadUser
    'Dim _KeyRoadUsersIndexes As List(Of Integer)
    'Dim _StartFrame As Integer, _EndFrame As Integer
    'Dim _ExportRoadUsers As List(Of clsRoadUser)
    'Dim i As Integer


    'If KeyRoadUsers.Count > 0 Then
    '  _ExportRoadUsers = New List(Of clsRoadUser)
    '  _KeyRoadUsersIndexes = New List(Of Integer)

    '  i = 0
    '  _StartFrame = KeyRoadUsers(0).FirstFrame
    '  _EndFrame = KeyRoadUsers(0).LastFrame
    '  For Each _ExportRoadUser In KeyRoadUsers
    '    _StartFrame = Math.Min(_ExportRoadUser.FirstFrame, _StartFrame)
    '    _EndFrame = Math.Max(_ExportRoadUser.LastFrame, _EndFrame)
    '    _ExportRoadUsers.Add(_ExportRoadUser)
    '    _KeyRoadUsersIndexes.Add(i)
    '    i = i + 1
    '  Next

    '  Call DoExportDetection(_StartFrame, _EndFrame, _ExportRoadUsers, _KeyRoadUsersIndexes)
    'End If
  End Sub






#End Region



#Region "Private subs"

  Public Function LoadRecordVR(ByVal RecordIndexVR As Integer) As Boolean

    If IsBetween(RecordIndexVR, 0, NumberOfFilteredRecordsVR - 1) Then
      MyRecordIndexVR = RecordIndexVR
      MyCurrentRecordVR = New clsRecord(Me,
                                        RecordTypes.VideoRecording,
                                        MyBase.RecordID_VR,
                                        MyBase.DateTimeVR,
                                        MyBase.StatusVR,
                                        MyBase.TypeVR,
                                        MyBase.CommentVR,
                                        MyBase.MapAvailableVR, MyBase.MapVR, MyBase.MapWidthVR, MyBase.MapHeightVR,
                                        MyBase.MapScaleVR, MyBase.MapX_VR, MyBase.MapY_VR, MyBase.MapDxVR, MyBase.MapDyVR,
                                        MyBase.MapVisibleVR, MyBase.MapCommentVR,
                                        MyBase.MediaVR,
                                        MyBase.StartTimeVR, MyBase.FrameCountVR, MyBase.FrameRateVR,
                                        MyBase.UserControlValuesVR,
                                        MyBase.RelatedRecordsVR)

      MyCurrentRecordVR.VideoFiles = MyBase.GetVideoFilesVR(MyCurrentRecordVR)
      If MyCurrentRecordVR.Map.Available Then _
        MyCurrentRecordVR.Map.ImportMapParameters(MyMapDirectory & Path.GetFileNameWithoutExtension(MyMapDirectory & MyCurrentRecordVR.Map.Map) & ".tamap")

      '  MyCurrentRecordVR.Map.ImportMapParameters(MyMapDirectory & Path.GetFileNameWithoutExtension(MyMapDirectory & MyBase.MapVR) & ".tamap")
      MyCurrentRecordVR.LoadVideoFiles()
      MyCurrentRecordVR.LoadRoadUsers()
      Return True
     Else
      MyRecordIndexVR = -1
      MyCurrentRecordVR = Nothing
      Return False
    End If

  End Function

  Public Function LoadRecordDT(ByVal RecordIndexDT As Integer) As Boolean

    If IsBetween(RecordIndexDT, 0, NumberOfFilteredRecordsDT - 1) Then
      MyRecordIndexDT = RecordIndexDT
      MyCurrentRecordDT = New clsRecord(Me,
                                        RecordTypes.Detection,
                                        MyBase.RecordID_DT,
                                        MyBase.DateTimeDT,
                                        MyBase.StatusDT,
                                        MyBase.TypeDT,
                                        MyBase.CommentDT,
                                        MyBase.MapAvailableDT, MyBase.MapDT, MyBase.MapWidthDT, MyBase.MapHeightDT,
                                        MyBase.MapScaleDT, MyBase.MapX_DT, MyBase.MapY_DT, MyBase.MapDxDT, MyBase.MapDyDT,
                                        MyBase.MapVisibleDT, MyBase.MapCommentDT,
                                        MyBase.MediaDT,
                                        MyBase.StartTimeDT, MyBase.FrameCountDT, MyBase.FrameRateDT,
                                        MyBase.UserControlValuesDT,
                                        MyBase.RelatedRecordsDT)

      MyCurrentRecordDT.VideoFiles = MyBase.GetVideoFilesDT(MyCurrentRecordDT)
      If MyCurrentRecordDT.Map.Available Then _
        MyCurrentRecordDT.Map.ImportMapParameters(MyMapDirectory & Path.GetFileNameWithoutExtension(MyMapDirectory & MyCurrentRecordDT.Map.Map) & ".tamap")

      MyCurrentRecordDT.LoadRoadUsers()
      MyCurrentRecordDT.LoadVideoFiles()
      Return True
    Else
      MyRecordIndexDT = -1
      MyCurrentRecordDT = Nothing
      Return False
    End If
  End Function



  Public Sub SaveCurrentRecordVR()
    If MyCurrentRecordVR IsNot Nothing Then
      With MyCurrentRecordVR
        MyBase.DateTimeVR = .DateTime
        MyBase.TypeVR = .Type
        MyBase.StatusVR = .Status
        MyBase.CommentVR = .Comment

        MyBase.MapAvailableVR = .Map.Available
        MyBase.MapCommentVR = .Map.Comment
        MyBase.MapVR = .Map.Map
        MyBase.MapWidthVR = .Map.Size.Width
        MyBase.MapHeightVR = .Map.Size.Height
        MyBase.MapScaleVR = .Map.Scale
        MyBase.MapX_VR = .Map.X
        MyBase.MapY_VR = .Map.Y
        MyBase.MapDxVR = .Map.Dx
        MyBase.MapDyVR = .Map.Dy
        MyBase.MapVisibleVR = .Map.Visible
        MyBase.MediaVR = .Media
        MyBase.StartTimeVR = .StartTime
        MyBase.FrameRateVR = .FrameRate
        MyBase.FrameCountVR = .FrameCount
        MyBase.UserControlValuesVR = .UserControlValues
      End With

      Call MyBase.UpdateRecordVR()
      Call MyBase.SaveVideoFilesVR(MyCurrentRecordVR)
    End If

  End Sub

  Public Sub SaveCurrentRecordDT()
    If MyCurrentRecordDT IsNot Nothing Then
      With MyCurrentRecordDT
        MyBase.DateTimeDT = .DateTime
        MyBase.StatusDT = .Status
        MyBase.TypeDT = .Type
        MyBase.CommentDT = .Comment
        MyBase.MapAvailableDT = .Map.Available
        MyBase.MapDT = .Map.Map
        MyBase.MapWidthDT = .Map.Size.Width
        MyBase.MapHeightDT = .Map.Size.Height
        MyBase.MapScaleDT = .Map.Scale
        MyBase.MapX_DT = .Map.X
        MyBase.MapY_DT = .Map.Y
        MyBase.MapDxDT = .Map.Dx
        MyBase.MapDyDT = .Map.Dy
        MyBase.MapVisibleDT = .Map.Visible
        MyBase.MapCommentDT = .Map.Comment
        MyBase.MediaDT = .Media
        MyBase.StartTimeDT = .StartTime
        MyBase.FrameCountDT = .FrameCount
        MyBase.FrameRateDT = .FrameRate
        MyBase.RelatedRecordsDT = .RelatedRecords
        MyBase.UserControlValuesDT = .UserControlValues
      End With

      Call MyBase.UpdateRecordDT()
      Call MyBase.SaveVideoFilesDT(MyCurrentRecordDT)
    End If
  End Sub

  Public Sub SaveCurrentRecord()
    Select Case MyLayoutMode
      Case ProjectLayoutModes.VideoRecordings
        Call SaveCurrentRecordVR()
      Case ProjectLayoutModes.Detections
        Call SaveCurrentRecordDT()
    End Select
  End Sub


  Public Sub CloseCurrentRecordVR()
    If MyCurrentRecordVR IsNot Nothing Then
      For Each _VideoFile In MyCurrentRecordVR.VideoFiles
        _VideoFile.Close()
      Next
    End If
  End Sub


  Public Sub CloseCurrentRecordDT()
    If MyCurrentRecordDT IsNot Nothing Then
      For Each _VideoFile In MyCurrentRecordDT.VideoFiles
        _VideoFile.Close()
      Next
    End If
  End Sub

  'Public Sub CloseCurrentRecord()
  '  Select Case MyLayoutMode
  '    Case ProjectLayoutModes.VideoRecordings
  '      Call CloseCurrentRecordVR()
  '    Case ProjectLayoutModes.Detections
  '      Call CloseCurrentRecordDT()
  '  End Select
  'End Sub






  Public Overloads Function AddRecordVR(ByVal StartTime As Double,
                                        ByVal FrameCount As Integer,
                                        ByVal FrameRate As Double,
                                        ByVal VideoFile As String,
                                        Optional ByVal MediaFile As String = "",
                                        Optional ByVal SupressMessages As Boolean = False) As Integer

    MyRecordIndexVR = MyBase.AddRecordVR(StartTime, FrameCount, FrameRate)
    Call Me.LoadRecordVR(MyRecordIndexVR)

    With MyCurrentRecordVR
      .DateTime = Date.FromOADate(StartTime / 3600 / 24)
      .Media = MediaFile
      .ImportVideoFile(VideoFile, , SupressMessages)
    End With

    Call SaveCurrentRecordVR()

    Return MyRecordIndexVR
  End Function



  Public Overloads Function AddRecordDT(ByVal StartTime As Double,
                                        ByVal FrameCount As Integer,
                                        ByVal FrameRate As Double,
                                        ByVal VideoFile As String,
                                        Optional ByVal MediaFile As String = "",
                                        Optional ByVal SupressMessages As Boolean = False) As Integer


    MyRecordIndexDT = MyBase.AddRecordDT(StartTime, FrameCount, FrameRate)
    Me.LoadRecordDT(MyRecordIndexDT)

    With MyCurrentRecordDT
      .DateTime = Date.FromOADate(StartTime / 3600 / 24)
      .Media = MediaFile
      .ImportVideoFile(VideoFile,  , SupressMessages)
    End With

    Call SaveCurrentRecordDT()

    Return MyRecordIndexDT
  End Function



#End Region



  Public ReadOnly Property DatabaseConnection As OleDbConnection
    Get
      Return MyOleDBConnection
    End Get
  End Property



  Public Function GetRoadUserTypeByName(TypeName As String) As clsRoadUserType
    Dim _RoadUserType As clsRoadUserType

    _RoadUserType = Me.RoadUserTypes.Find(Function(Type As clsRoadUserType)
                                            Return String.Equals(Type.Name, TypeName)
                                          End Function)
    If _RoadUserType IsNot Nothing Then
      Return _RoadUserType
    Else
      Return Me.RoadUserTypes(0)
    End If
  End Function




    Private Function DoExportDetection(ByVal StartFrame As Integer, ByVal EndFrame As Integer, Optional ByVal ExportRoadUsers As List(Of clsRoadUser) = Nothing) As Integer
    Dim _StartTime As Double
    Dim _NewRoadUser As clsRoadUser


    _StartTime = MyCurrentRecordVR.GetAbsoluteTime(StartFrame)
    MyRecordIndexDT = MyBase.AddRecordDT(_StartTime, EndFrame - StartFrame + 1, MyCurrentRecordVR.FrameRate, MyCurrentRecordVR.ID)
    Call Me.LoadRecordDT(MyRecordIndexDT)

    With MyCurrentRecordDT
      .DateTime = Date.FromOADate(_StartTime / 3600 / 24)
      .Map = MyCurrentRecordVR.Map.CreateClone(MyCurrentRecordDT)
      .RelatedRecords.Add(MyCurrentRecordVR.ID)

      For Each _VideoFile In MyCurrentRecordVR.VideoFiles
        .VideoFiles.Add(_VideoFile.CreateCopy(MyCurrentRecordDT))
      Next
    End With
    Call SaveCurrentRecordDT()

    If ExportRoadUsers IsNot Nothing Then
      For Each _RoadUser In ExportRoadUsers
        _NewRoadUser = Nothing
        For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
          If IsBetween(_Frame, StartFrame, EndFrame) Then
            If _NewRoadUser Is Nothing Then _NewRoadUser = New clsRoadUser(MyCurrentRecordDT, _Frame - StartFrame, _RoadUser.Type, _RoadUser.Length, _RoadUser.Width, _RoadUser.Height)
            _NewRoadUser.AddPoint(_RoadUser.DataPoint(_Frame).CreateCopy)

            If (_Frame = StartFrame) Or (_Frame = EndFrame) Then
              With _NewRoadUser.DataPoint(_Frame - StartFrame)
                If Not .OriginalXYExists Then
                  .Xpxl = .X
                  .Ypxl = .Y
                  .DxDypxl = New clsDxDy(.Dx, .Dy)
                End If
              End With
            End If
          End If
        Next _Frame

        If _NewRoadUser IsNot Nothing Then
          MyCurrentRecordDT.RoadUsers.Add(_NewRoadUser)
        End If
      Next
    End If
    MyCurrentRecordDT.SaveRoadUsers()

    Return MyCurrentRecordDT.ID
  End Function



  Public Function DuplicateCurrentDetection() As Integer
    Dim _OldRecord As clsRecord
    Dim _VideoRecordingID As Integer


    _OldRecord = Me.CurrentRecordDT

    If _OldRecord.RelatedRecords.Count > 0 Then
      _VideoRecordingID = _OldRecord.RelatedRecords(0)
    Else
      _VideoRecordingID = NoValue
    End If


    MyRecordIndexDT = MyBase.AddRecordDT(_OldRecord.StartTime, _OldRecord.FrameCount, _OldRecord.FrameRate, _VideoRecordingID)

    Call Me.LoadRecordDT(MyRecordIndexDT)

    With MyCurrentRecordDT
      .DateTime = Date.FromOADate(_OldRecord.StartTime / 3600 / 24)
      .Status = _OldRecord.Status
      .Type = _OldRecord.Type
      .Map = _OldRecord.Map.CreateClone(MyCurrentRecordDT)
      If IsValue(_VideoRecordingID) Then .RelatedRecords.Add(_VideoRecordingID)
      .Comment = "duplicate of a detection ID = " & _OldRecord.ID.ToString & Environment.NewLine & _OldRecord.Comment
      ' .Comment = _OldRecord.Comment

      .UserControlValues = _OldRecord.UserControlValues

      For Each _VideoFile In _OldRecord.VideoFiles
        .VideoFiles.Add(_VideoFile.CreateCopy(MyCurrentRecordDT))
      Next
    End With
    Call SaveCurrentRecordDT()

    For Each _RoadUser In _OldRecord.RoadUsers
      MyCurrentRecordDT.RoadUsers.Add(_RoadUser)
    Next
    MyCurrentRecordDT.SaveRoadUsers()

    _OldRecord = Nothing

    Return MyCurrentRecordDT.ID
  End Function




  Public Sub ImportExportedEvent(ByVal PreviewFile As String)
    Dim _StartTime, _StartTimeTemp As Double
    Dim _FrameRate As Double
    Dim _FrameCount As Integer
    Dim _VideoFilePath As String
    Dim _RoadUsers As List(Of clsRoadUser)
    Dim _RoadUser As clsRoadUser
    Dim _RoadUserFolder As String
    Dim _VideoFolder As String
    Dim _CalibrationFolder As String
    Dim _i As Integer
    Dim _FilePath As String
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _StartDate As Date

    Dim _Values() As String
    Dim _CurrentTime As Date
    Dim _CurrentFrame As Integer
    Dim _ClockDescripancies As List(Of Double)
    Dim _Length, _Width, _Height As Double
    Dim _RU_type As clsRoadUserType
    Dim _Weight As Integer

    Dim _DataPoint As clsDataPoint
    Dim _X, _Y As Double
    Dim _Xpxl, _Ypxl As Double
    Dim _V, _A, _J As Double
    Dim _Dx, _Dy As Double
    Dim _Dxpxl, _Dypxl As Double



    'Assign folders to red from
    _RoadUserFolder = PreviewFile.Substring(0, PreviewFile.Length - 11) & "Trajectories\"
    _VideoFolder = PreviewFile.Substring(0, PreviewFile.Length - 11) & "Video\"
    _CalibrationFolder = PreviewFile.Substring(0, PreviewFile.Length - 11) & "Calibration\"



    'discover _StartTime, _FPS and _FrameCount
    _FilePath = ""
    For Each _FilePath In Directory.GetFiles(_VideoFolder, "Frame correspondence at * fps.txt")
    Next

    If _FilePath.Length > 0 Then
      _FrameRate = Double.Parse(Path.GetFileName(_FilePath).Substring(24).Substring(0, Path.GetFileName(_FilePath).Substring(24).Length - 8), CultureInfo.InvariantCulture)

      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _FilePath, OpenMode.Input)
      _Line = LineInput(_FileIndex)
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        _Values = _Line.Split(CChar(";"))
        _CurrentTime = InterpretTimestampLine(_Values(0), _CurrentFrame)

        If _CurrentFrame = 0 Then _StartTime = _CurrentTime.ToOADate * 24 * 3600

      Loop
      FileClose(_FileIndex)
    End If
    _FrameCount = _CurrentFrame



    'discover clock descripancies
    _ClockDescripancies = New List(Of Double)

    _FilePath = _VideoFolder & "Clock descrepancies.txt"

    _FileIndex = FreeFile()
    FileOpen(_FileIndex, _FilePath, OpenMode.Input)
    Do Until EOF(_FileIndex)
      _Line = LineInput(_FileIndex)
      _ClockDescripancies.Add(Double.Parse(_Line.Substring(10, _Line.Length - 10), CultureInfo.InvariantCulture))
    Loop
    FileClose(_FileIndex)
















    Select Case MyLayoutMode
      Case ProjectLayoutModes.Detections



        MyRecordIndexDT = MyBase.AddRecordDT(_StartTime, _FrameCount, _FrameRate)
        Me.LoadRecordDT(MyRecordIndexDT)

        With MyCurrentRecordDT
          .DateTime = Date.FromOADate(_StartTime / 3600 / 24)
          .ImportMap(_CalibrationFolder & "Map" & ".tamap", True, Date.FromOADate(_StartTime / 3600 / 24).ToString("yyyyMMdd-HHmmss") & ".tamap")

          For _i = 1 To _ClockDescripancies.Count
            .ImportVideoFile(_VideoFolder & "Camera_" & _i.ToString & ".avi", _ClockDescripancies(_i - 1), True, Date.FromOADate(_StartTime / 3600 / 24).ToString("yyyyMMdd-HHmmss") & "_" & _i.ToString("00") & ".avi")
            .VideoFiles(_i - 1).ImportCallibration(_CalibrationFolder & "Camera_" & _i.ToString & ".tacal")
          Next _i

          'discover road users


          For Each _FilePath In Directory.GetFiles(_RoadUserFolder, "RoadUser_*.txt")
            _FileIndex = FreeFile()
            FileOpen(_FileIndex, _FilePath, OpenMode.Input)
            _Line = LineInput(_FileIndex)
            _RU_type = Me.GetRoadUserTypeByName(_Line.Substring(6, _Line.Length - 6))
            _Line = LineInput(_FileIndex)
            _Length = Double.Parse(_Line.Substring(11, _Line.Length - 11), CultureInfo.InvariantCulture)
            _Line = LineInput(_FileIndex)
            _Width = Double.Parse(_Line.Substring(10, _Line.Length - 10), CultureInfo.InvariantCulture)
            _Line = LineInput(_FileIndex)
            _Height = Double.Parse(_Line.Substring(11, _Line.Length - 11), CultureInfo.InvariantCulture)
            _Line = LineInput(_FileIndex)
            _Weight = Integer.Parse(_Line.Substring(12, _Line.Length - 12), CultureInfo.InvariantCulture)



            _i = 0
            _Line = LineInput(_FileIndex)
            Do Until EOF(_FileIndex)
              _Line = LineInput(_FileIndex)
              _Values = _Line.Split(CChar(";"))

              If _i = 0 Then _RoadUser = New clsRoadUser(MyCurrentRecordDT, CInt(_Values(0).Substring(0, 5)), _RU_type, _Length, _Width, _Height, _Weight)

              If Not Double.TryParse(_Values(1), NumberStyles.Any, CultureInfo.InvariantCulture, _X) Then _X = NoValue
              If Not Double.TryParse(_Values(2), NumberStyles.Any, CultureInfo.InvariantCulture, _Y) Then _Y = NoValue
              If Not Double.TryParse(_Values(4), NumberStyles.Any, CultureInfo.InvariantCulture, _Dx) Then _Dx = NoValue
              If Not Double.TryParse(_Values(5), NumberStyles.Any, CultureInfo.InvariantCulture, _Dy) Then _Dy = NoValue
              If Not Double.TryParse(_Values(6), NumberStyles.Any, CultureInfo.InvariantCulture, _V) Then _V = NoValue
              If Not Double.TryParse(_Values(7), NumberStyles.Any, CultureInfo.InvariantCulture, _A) Then _A = NoValue
              If Not Double.TryParse(_Values(8), NumberStyles.Any, CultureInfo.InvariantCulture, _J) Then _J = NoValue
              If Not Double.TryParse(_Values(9), NumberStyles.Any, CultureInfo.InvariantCulture, _Xpxl) Then _Xpxl = NoValue
              If Not Double.TryParse(_Values(10), NumberStyles.Any, CultureInfo.InvariantCulture, _Ypxl) Then _Ypxl = NoValue
              If Not Double.TryParse(_Values(12), NumberStyles.Any, CultureInfo.InvariantCulture, _Dxpxl) Then _Dxpxl = NoValue
              If Not Double.TryParse(_Values(13), NumberStyles.Any, CultureInfo.InvariantCulture, _Dypxl) Then _Dypxl = NoValue
              _DataPoint = New clsDataPoint(_Xpxl, _Ypxl, New clsDxDy(_Dxpxl, _Dypxl), _X, _Y, New clsDxDy(_Dx, _Dy), _V, _A, _j)

              _RoadUser.AddPoint(_DataPoint)
              _i = _i + 1
            Loop
            FileClose(_FileIndex)



            .RoadUsers.Add(_RoadUser)
          Next



          .SaveRoadUsers()


        End With

        Call SaveCurrentRecordDT()


        '  If _f.CalibrationFile.Length > 0 Then
        'MyProject.CurrentRecordDT.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
        '  MyProject.CurrentRecordDT.VideoFiles(0).Status = VideoFileStatuses.Updated
        ''   End If
        ''   If _f.MapFile.Length > 0 Then _
        'MyProject.CurrentRecordDT.ImportMap(_f.MapFile, True)

    End Select








    '_i = 1
    '_FilePath = _RoadUserFolder & "Road user_" & (_i).ToString & ".txt"
    '_StartTime = Date.Now.ToOADate * 24 * 60 * 60
    'Do While File.Exists(_FilePath)
    '  _FileIndex = FreeFile()
    '  FileOpen(_FileIndex, _FilePath, OpenMode.Input)
    '  _Line = LineInput(_FileIndex)
    '  _StartDate = New Date(CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)),
    '                       CInt(_Line.Substring(0, 4)))
    '  _StartTimeTemp = _StartDate.ToOADate * 24 * 60 * 60

    '  If _StartTimeTemp < _StartTime Then _StartTime = _StartTimeTemp

    '  FileClose(_FileIndex)

    '  _i = _i + 1
    '  _FilePath = _RoadUserFolder & "Road user_" & (_i).ToString & ".txt"
    'Loop

    ''  MyRecordIndexDT = MyBase.AddRecordDT(_StartTime, EndFrame - StartFrame + 1, MyCurrentRecordVR.FrameRate, MyCurrentRecordVR.ID)
    'Call Me.LoadRecordDT(MyRecordIndexDT)

    ''_StartTime = MyCurrentRecordVR.GetAbsoluteTime(StartFrame)
    ''MyRecordIndexDT = MyBase.AddRecordDT(_StartTime, EndFrame - StartFrame + 1, MyCurrentRecordVR.FrameRate, MyCurrentRecordVR.ID)
    ''Call Me.LoadRecordDT(MyRecordIndexDT)
    'Select Case MyLayoutMode
    '  Case ProjectLayoutModes.VideoRecordings




    '  Case ProjectLayoutModes.Detections
    '    '_StartTime = MyCurrentRecordVR.GetAbsoluteTime(StartFrame)
    '    'MyRecordIndexDT = MyBase.AddRecordDT(_StartTime, EndFrame - StartFrame + 1, MyCurrentRecordVR.FrameRate, MyCurrentRecordVR.ID)
    '    'Call Me.LoadRecordDT(MyRecordIndexDT)

    'End Select
    'With MyCurrentRecordDT
    '  .DateTime = Date.FromOADate(_StartTime / 3600 / 24)
    '  .Map = MyCurrentRecordVR.Map.CreateClone(MyCurrentRecordDT)
    '  .RelatedRecords.Add(MyCurrentRecordVR.ID)

    '  For Each _VideoFile In MyCurrentRecordVR.VideoFiles
    '    .VideoFiles.Add(_VideoFile.CreateCopy(MyCurrentRecordDT))
    '  Next
    'End With
    'Call SaveCurrentRecordDT()

    'If ExportRoadUsers IsNot Nothing Then
    '  For Each _RoadUser In ExportRoadUsers
    '    _NewRoadUser = Nothing
    '    For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
    '      If IsBetween(_Frame, StartFrame, EndFrame) Then
    '        If _NewRoadUser Is Nothing Then _NewRoadUser = New clsRoadUser(MyCurrentRecordDT, _Frame - StartFrame, _RoadUser.Type, _RoadUser.Length, _RoadUser.Width, _RoadUser.Height)
    '        _NewRoadUser.AddPoint(_RoadUser.DataPoint(_Frame).CreateCopy)

    '        If (_Frame = StartFrame) Or (_Frame = EndFrame) Then
    '          With _NewRoadUser.DataPoint(_Frame - StartFrame)
    '            If Not .OriginalXYExists Then
    '              .Xpxl = .X
    '              .Ypxl = .Y
    '              .DxDypxl = New clsDxDy(.Dx, .Dy)
    '            End If
    '          End With
    '        End If
    '      End If
    '    Next _Frame

    '    If _NewRoadUser IsNot Nothing Then
    '      MyCurrentRecordDT.RoadUsers.Add(_NewRoadUser)
    '    End If
    '  Next
    'End If
    'MyCurrentRecordDT.SaveRoadUsers()

    ''Return MyCurrentRecordDT.ID


    'If _f.DoImport Then



    '  MyProject.AddFieldDetections("uDT_Entering1", Type.GetType("System.DateTime"))
    '  MyProject.AddFieldDetections("uDT_Leaving1", Type.GetType("System.DateTime"))
    '  MyProject.AddFieldDetections("uDT_Entering2", Type.GetType("System.DateTime"))
    '  MyProject.AddFieldDetections("uDT_Leaving2", Type.GetType("System.DateTime"))
    '  MyProject.AddFieldDetections("uDT_FirstDetected", Type.GetType("System.String"))
    '  MyProject.AddFieldDetections("uDT_TimeGap", Type.GetType("System.Int32"))


    '  _FileIndex = FreeFile()
    '  FileOpen(_FileIndex, _f.RubaOtputFile, OpenMode.Input)
    '  Do Until EOF(_FileIndex)
    '    _Line = LineInput(_FileIndex)
    '    If Not _Line.StartsWith("File;Date;Entering") Then
    '      _Fields = _Line.Split(CChar(";"))

    '      _Entering1 = New Date(Integer.Parse(_Fields(2).Substring(0, 4)),
    '                            Integer.Parse(_Fields(2).Substring(5, 2)),
    '                            Integer.Parse(_Fields(2).Substring(8, 2)),
    '                            Integer.Parse(_Fields(2).Substring(11, 2)),
    '                            Integer.Parse(_Fields(2).Substring(14, 2)),
    '                            Integer.Parse(_Fields(2).Substring(17, 2)),
    '                            Integer.Parse(_Fields(2).Substring(20, 3)))
    '      _Leaving1 = New Date(Integer.Parse(_Fields(3).Substring(0, 4)),
    '                           Integer.Parse(_Fields(3).Substring(5, 2)),
    '                           Integer.Parse(_Fields(3).Substring(8, 2)),
    '                           Integer.Parse(_Fields(3).Substring(11, 2)),
    '                           Integer.Parse(_Fields(3).Substring(14, 2)),
    '                           Integer.Parse(_Fields(3).Substring(17, 2)),
    '                           Integer.Parse(_Fields(3).Substring(20, 3)))
    '      _Entering2 = New Date(Integer.Parse(_Fields(5).Substring(0, 4)),
    '                            Integer.Parse(_Fields(5).Substring(5, 2)),
    '                            Integer.Parse(_Fields(5).Substring(8, 2)),
    '                            Integer.Parse(_Fields(5).Substring(11, 2)),
    '                            Integer.Parse(_Fields(5).Substring(14, 2)),
    '                            Integer.Parse(_Fields(5).Substring(17, 2)),
    '                            Integer.Parse(_Fields(5).Substring(20, 3)))
    '      _Leaving2 = New Date(Integer.Parse(_Fields(6).Substring(0, 4)),
    '                           Integer.Parse(_Fields(6).Substring(5, 2)),
    '                           Integer.Parse(_Fields(6).Substring(8, 2)),
    '                           Integer.Parse(_Fields(6).Substring(11, 2)),
    '                           Integer.Parse(_Fields(6).Substring(14, 2)),
    '                           Integer.Parse(_Fields(6).Substring(17, 2)),
    '                           Integer.Parse(_Fields(6).Substring(20, 3)))
    '      _TimeGap = Integer.Parse(_Fields(8))
    '      _FirstDetected = _Fields(9)
    '      _MediaFileName = _Fields(10)

    '      If _Entering1 < _Entering2 Then
    '        _EarliestEnter = _Entering1
    '      Else
    '        _EarliestEnter = _Entering2
    '      End If
    '      _VideoFileName = _EarliestEnter.Year.ToString("0000") &
    '                       _EarliestEnter.Month.ToString("00") &
    '                       _EarliestEnter.Day.ToString("00") & "-" &
    '                       _EarliestEnter.Hour.ToString("00") &
    '                       _EarliestEnter.Minute.ToString("00") &
    '                       _EarliestEnter.Second.ToString("00") & "." &
    '                       _EarliestEnter.Millisecond.ToString("000") & ".zip"



    '      For Each _VideoFilePath In _f.VideoFileList
    '        If Path.GetFileName(_VideoFilePath) = _VideoFileName Then
    '          For Each _MediaFilePath In _f.MediaFileList
    '            If Path.GetFileName(_MediaFilePath) = _MediaFileName Then
    '              _Bitmap = New Bitmap(_MediaFilePath)
    '              _MediaFileName = Path.GetFileNameWithoutExtension(_MediaFilePath) & ".jpg"

    '              _VideoReader = New clsVideoReader(_VideoFilePath)
    '              If _f.UseExistingFPS Then
    '                _FrameCount = _VideoReader.FrameCount
    '                _FrameRate = _VideoReader.FPS
    '              Else
    '                _FrameCount = CInt(_VideoReader.FrameCount / _VideoReader.FPS * _f.FrameRate)
    '                _FrameRate = _f.FrameRate
    '              End If
    '              _StartTime = _VideoReader.GetTimeStamp(0)
    '              _VideoReader.Close()


    '              Select Case MyProject.LayoutMode
    '                'Case ProjectLayoutModes.VideoRecordings
    '                '  MyProject.AddRecordVR(_StartTime, _FrameCount, _FrameRate, _VideoFilePath, _MediaFileName, True)
    '                '  Call SaveJpgImage(_Bitmap, MyProject.MediaDirectory & "Video recordings\" & _MediaFileName)
    '                '  If _f.CalibrationFile.Length > 0 Then
    '                '    MyProject.CurrentRecordVR.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
    '                '    MyProject.CurrentRecordVR.VideoFiles(0).Status = VideoFileStatuses.Updated
    '                '  End If
    '                '  If _f.MapFile.Length > 0 Then _
    '                '    MyProject.CurrentRecordVR.ImportMap(_f.MapFile, True)
    '                '  MyProject.SaveCurrentRecordVR()

    '                Case ProjectLayoutModes.Detections


    '                  MyProject.AddRecordDT(_StartTime, _FrameCount, _FrameRate, _VideoFilePath, _MediaFileName, True)
    '                  MyProject.UpdateValueDetections("uDT_Entering1", _Entering1, Type.GetType("System.DateTime"))
    '                  MyProject.UpdateValueDetections("uDT_Leaving1", _Leaving1, Type.GetType("System.DateTime"))
    '                  MyProject.UpdateValueDetections("uDT_Entering2", _Entering2, Type.GetType("System.DateTime"))
    '                  MyProject.UpdateValueDetections("uDT_Leaving2", _Leaving2, Type.GetType("System.DateTime"))
    '                  MyProject.UpdateValueDetections("uDT_FirstDetected", _FirstDetected, Type.GetType("System.String"))
    '                  MyProject.UpdateValueDetections("uDT_TimeGap", _TimeGap, Type.GetType("System.Int32"))

    '                  Call SaveJpgImage(_Bitmap, MyProject.MediaDirectory & "Detections\" & _MediaFileName)
    '                  If _f.CalibrationFile.Length > 0 Then
    '                    MyProject.CurrentRecordDT.VideoFiles(0).ImportCallibration(_f.CalibrationFile)
    '                    MyProject.CurrentRecordDT.VideoFiles(0).Status = VideoFileStatuses.Updated
    '                  End If
    '                  If _f.MapFile.Length > 0 Then _
    '                    MyProject.CurrentRecordDT.ImportMap(_f.MapFile, True)




  End Sub



End Class
