﻿Public MustInherit Class clsDatabaseNew
  Public Const DataModeVIDEORECORDINGS As Integer = 1
  Public Const DataModeDETECTIONS As Integer = 2

  Private Const SORT_ORDER As String = "cmDateTime ASC, cmID ASC"



  Private MyOleDbConnection As OleDbConnection
  Private MyDataSet As DataSet
  Private MyVideoRecordingRows() As DataRow
  Private MyDetectionRows() As DataRow
  Private MyDatabaseFilePath As String
  Private MyDataMode As Byte
  Private MyRecordIndexVR As Integer
  Private MyRecordIndexDT As Integer
  Private MyFilter As String
  'Private MyUpdateRequired As Boolean
  Private MyMaxVideoRecordingID As Integer
  Private MyMaxDetectionID As Integer
  Private MyMaxVideoFileID As Integer


  'Private iMyRecordIndex As Integer




#Region "Public properties"

  Public ReadOnly Property NumberOfRecordsVR() As Integer
    Get
      NumberOfRecordsVR = MyDataSet.Tables("VideoRecordings").Rows.Count
    End Get
  End Property

  Public ReadOnly Property NumberOfRecordsDT() As Integer
    Get
      NumberOfRecordsDT = MyDataSet.Tables("Detections").Rows.Count
    End Get
  End Property

  Public ReadOnly Property NumberOfFilteredRecordsVR() As Integer
    Get
      NumberOfFilteredRecordsVR = MyVideoRecordingRows.Length
    End Get
  End Property

  Public ReadOnly Property NumberOfFilteredRecordsDT() As Integer
    Get
      NumberOfFilteredRecordsDT = MyDetectionRows.Length
    End Get
  End Property

  Public Property CurrentRecordIndexVR() As Integer
    Get
      CurrentRecordIndexVR = MyRecordIndexVR
    End Get
    Set(ByVal value As Integer)
      MyRecordIndexVR = value
    End Set
  End Property

  Public Property CurrentRecordIndexDT() As Integer
    Get
      CurrentRecordIndexDT = MyRecordIndexDT
    End Get
    Set(ByVal value As Integer)
      MyRecordIndexDT = value
    End Set
  End Property



  Protected ReadOnly Property RecordID_VR() As Integer
    Get
      RecordID_VR = GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmID"))
    End Get
  End Property

  Protected ReadOnly Property RecordID_DT() As Integer
    Get
      RecordID_DT = GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmID"))
    End Get
  End Property

  Protected Property BackgroundVR As String
    Get
      BackgroundVR = GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("cmBackground"))
    End Get
    Set(ByVal value As String)
      MyVideoRecordingRows(MyRecordIndexVR)("cmBackground") = value
    End Set
  End Property

  Protected Property BackgroundDT As String
    Get
      BackgroundDT = GetSQLValueString(MyDetectionRows(MyRecordIndexDT)("cmBackground"))
    End Get
    Set(ByVal value As String)
      MyDetectionRows(MyRecordIndexDT)("cmBackground") = value
    End Set
  End Property

  Protected Property ScaleVR() As Double
    Get
      ScaleVR = GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmScale"))
    End Get
    Set(ByVal value As Double)
      MyVideoRecordingRows(MyRecordIndexVR)("cmScale") = SetSQLValueDouble(value)
    End Set
  End Property

  Protected Property ScaleDT() As Double
    Get
      ScaleDT = GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmScale"))
    End Get
    Set(ByVal value As Double)
      MyDetectionRows(MyRecordIndexDT)("cmScale") = SetSQLValueDouble(value)
    End Set
  End Property



  Protected Function ConnectDB(ByVal DatabaseFilePath As String) As Boolean

    MyDatabaseFilePath = DatabaseFilePath
    If File.Exists(MyDatabaseFilePath) Then
      MyOleDBConnection = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & MyDatabaseFilePath)
      Try
        MyOleDBConnection.Open()
      Catch ex As Exception
        MsgBox("Problems connecting to the database file '" & MyDatabaseFilePath & "'!" & Environment.NewLine & Environment.NewLine & _
               "error: """ & ex.Message & """", MsgBoxStyle.Critical)
        MyOleDBConnection.Dispose()
        Return False
      End Try

      If ReadTables() Then
        Call SelectVideoRecordings()
        Call SelectDetections()
        Return True
      Else
        Call DisconnectDB()
        Return False
      End If
    Else 'If database file exists
      MsgBox("Cannot find the database file '" & MyDatabaseFilePath & "'!", MsgBoxStyle.Critical)
      Return False
    End If
  End Function

  Protected Sub DisconnectDB()

    MyOleDBConnection.Close()
    MyOleDBConnection.Dispose()

  End Sub







  Public Function RelatedRecordExist() As Boolean
    Dim iNumRelatedRecords As Integer

    Select Case DataModeDB
      Case DataModeVIDEORECORDINGS
        iNumRelatedRecords = CInt(MyDataSet.Tables("Detections").Compute("Count(cmID)", "cmVideoRecording = " & RecordID_DB.ToString))
        If iNumRelatedRecords > 0 Then Return True Else Return False
      Case DataModeDETECTIONS
        iNumRelatedRecords = CInt(MyDataSet.Tables("VideoRecordings").Compute("Count(cmID)", "cmID = " & RelatedRecordID.ToString))
        If iNumRelatedRecords > 0 Then Return True Else Return False
      Case Else
        Return False
    End Select

  End Function

  Public ReadOnly Property UserControls() As List(Of clsUserControl)
    Get
      Dim myControl As clsUserControl
      Dim dbRows() As DataRow
      Dim dbRow As DataRow

      UserControls = New List(Of clsUserControl)
      dbRows = MyDataSet.Tables("Controls").Select("cmDataMode = " & MyDataMode.ToString, "cmPosition ASC")
      For Each dbRow In dbRows
        myControl = New clsUserControl
        myControl.Name = dbRow("cmName").ToString
        myControl.Label = dbRow("cmLabel").ToString
        myControl.Position = CInt(dbRow("cmPosition"))
        myControl.Type = CByte(dbRow("cmType"))
        myControl.ValueTable = MyDataSet.Tables("u" & dbRow("cmName").ToString & "Values")
        UserControls.Add(myControl)
      Next
    End Get
  End Property

  'returns the datatable with values for the Status-field
  Public ReadOnly Property StatusValues() As DataTable
    Get
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          StatusValues = MyDataSet.Tables("VideoRecordingStatus")
        Case DataModeDETECTIONS
          StatusValues = MyDataSet.Tables("DetectionStatus")
        Case Else
          StatusValues = Nothing
      End Select
    End Get
  End Property

  'returns the datatable with values for the Type-field
  Public ReadOnly Property TypeValues() As DataTable
    Get
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          TypeValues = MyDataSet.Tables("VideoRecordingType")
        Case DataModeDETECTIONS
          TypeValues = MyDataSet.Tables("DetectionType")
        Case Else
          TypeValues = Nothing
      End Select
    End Get
  End Property


  Protected Property UserControlValuesDB() As List(Of Object)
    Get
      Dim myControl As clsUserControl
      Dim sValue As Object

      UserControlValuesDB = New List(Of Object)
      For Each myControl In UserControls
        Select Case MyDataMode
          Case DataModeVIDEORECORDINGS
            sValue = MyVideoRecordingRows(iMyRecordIndex)("u" & myControl.Name)
          Case DataModeDETECTIONS
            sValue = MyDetectionRows(iMyRecordIndex)("u" & myControl.Name)
          Case Else
            sValue = DBNull.Value
        End Select
        UserControlValuesDB.Add(sValue)
      Next
    End Get
    Set(ByVal value As List(Of Object))
      Dim myControl As clsUserControl
      Dim oValue As Object = DBNull.Value
      Dim i As Integer
      Dim myUserControls As List(Of clsUserControl)

      myUserControls = UserControls
      For i = 0 To myUserControls.Count - 1
        myControl = myUserControls(i)
        oValue = value(i)
        Select Case MyDataMode
          Case DataModeVIDEORECORDINGS
            If Not oValue.Equals(MyVideoRecordingRows(iMyRecordIndex)("u" & myUserControls(i).Name)) Then
              MyVideoRecordingRows(iMyRecordIndex)("u" & myUserControls(i).Name) = oValue
              MyUpdateRequired = True
            End If
          Case DataModeDETECTIONS
            If Not oValue.Equals(MyDetectionRows(iMyRecordIndex)("u" & myUserControls(i).Name)) Then
              MyDetectionRows(iMyRecordIndex)("u" & myUserControls(i).Name) = oValue
              MyUpdateRequired = True
            End If
        End Select
      Next i
    End Set
  End Property

#End Region



#Region "Protected Properties"




  Protected Property DataModeDB() As Byte
    Get
      DataModeDB = MyDataMode
    End Get
    Set(ByVal value As Byte)
      Dim cmSQL As New OleDbCommand
      If Not MyDataMode.Equals(value) Then
        MyDataMode = value
        cmSQL.Connection = MyOleDBConnection
        cmSQL.CommandText = "UPDATE ProjectInfo SET cmDataMode = " & MyDataMode.ToString
        cmSQL.ExecuteNonQuery()
        cmSQL.Dispose()
      End If
    End Set
  End Property



  Protected Property FilterDB As String
    Get
      FilterDB = MyFilter
    End Get
    Set(ByVal value As String)
      MyFilter = value
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          Call SelectVideoRecordings()
        Case DataModeDETECTIONS
          Call SelectDetections()
      End Select
    End Set
  End Property

  'the value in StartFrame-field of the current record in the database table
  Public Property StartFrameDB() As Integer
    Get
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          StartFrameDB = 0
        Case DataModeDETECTIONS
          StartFrameDB = CInt(MyDetectionRows(iMyRecordIndex)("cmStartFrame"))
        Case Else
          StartFrameDB = NoValue
      End Select
    End Get
    Set(ByVal value As Integer)
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS

        Case DataModeDETECTIONS
          If Not MyDetectionRows(iMyRecordIndex)("cmStartFrame").Equals(value) Then
            MyDetectionRows(iMyRecordIndex)("cmStartFrame") = value
            MyUpdateRequired = True
          End If
      End Select
    End Set
  End Property



  'Retrieves the records about video files from VideoFiles table
  Protected Property VideoFilesDB() As List(Of clsVideoFile)
    Get
      Dim VideoFile As clsVideoFile
      Dim dbRow As DataRow
      Dim dbRows() As DataRow

      VideoFilesDB = New List(Of clsVideoFile)
      dbRows = MyDataSet.Tables("VideoFiles").Select("(cmOwner = " & RecordID_DB.ToString & ") AND (cmDataMode = " & MyDataMode.ToString & ")", _
                                                  "cmPosition ASC")
      For Each dbRow In dbRows
        VideoFile = New clsVideoFile
        With VideoFile
          .Status = clsVideoFile.VideoFileStatusUNCHANGED
          If Not IsDBNull(dbRow("cmID")) Then .ID = CInt(dbRow("cmID")) Else .ID = NoValue
          If Not IsDBNull(dbRow("cmPosition")) Then .Position = CInt(dbRow("cmPosition")) Else .Position = NoValue
          If Not IsDBNull(dbRow("cmVisible")) Then .Visible = CBool(dbRow("cmVisible")) Else .Visible = False
          If Not IsDBNull(dbRow("cmVideoFile")) Then .VideoFileName = CStr(dbRow("cmVideoFile")) Else .VideoFileName = ""
          If Not IsDBNull(dbRow("cmFrameCount")) Then .FrameCount = CInt(dbRow("cmFrameCount")) Else .FrameCount = NoValue
          If Not IsDBNull(dbRow("cmFrameRate")) Then .FrameRate = CDbl(dbRow("cmFrameRate")) Else .FrameRate = NoValue
          If Not IsDBNull(dbRow("cmStartTime")) Then .StartTime = CDbl(dbRow("cmStartTime")) Else .StartTime = NoValue
          If Not IsDBNull(dbRow("cmWidth")) Then .FrameSize.Width = CInt(dbRow("cmWidth")) Else .FrameSize.Width = NoValue
          If Not IsDBNull(dbRow("cmHeight")) Then .FrameSize.Height = CInt(dbRow("cmHeight")) Else .FrameSize.Height = NoValue
          If Not IsDBNull(dbRow("cmComment")) Then .Comment = CStr(dbRow("cmComment")) Else .Comment = ""
          If Not IsDBNull(dbRow("cmCallibrationOK")) Then .CallibrationOK = CBool(dbRow("cmCallibrationOK")) Else .CallibrationOK = False
          If Not IsDBNull(dbRow("cmr1")) Then .TSAI.r1 = CDbl(dbRow("cmr1")) Else .TSAI.r1 = NoValue
          If Not IsDBNull(dbRow("cmr2")) Then .TSAI.r2 = CDbl(dbRow("cmr2")) Else .TSAI.r2 = NoValue
          If Not IsDBNull(dbRow("cmr3")) Then .TSAI.r3 = CDbl(dbRow("cmr3")) Else .TSAI.r3 = NoValue
          If Not IsDBNull(dbRow("cmr4")) Then .TSAI.r4 = CDbl(dbRow("cmr4")) Else .TSAI.r4 = NoValue
          If Not IsDBNull(dbRow("cmr5")) Then .TSAI.r5 = CDbl(dbRow("cmr5")) Else .TSAI.r5 = NoValue
          If Not IsDBNull(dbRow("cmr6")) Then .TSAI.r6 = CDbl(dbRow("cmr6")) Else .TSAI.r6 = NoValue
          If Not IsDBNull(dbRow("cmr7")) Then .TSAI.r7 = CDbl(dbRow("cmr7")) Else .TSAI.r7 = NoValue
          If Not IsDBNull(dbRow("cmr8")) Then .TSAI.r8 = CDbl(dbRow("cmr8")) Else .TSAI.r8 = NoValue
          If Not IsDBNull(dbRow("cmr9")) Then .TSAI.r9 = CDbl(dbRow("cmr9")) Else .TSAI.r9 = NoValue
          If Not IsDBNull(dbRow("cmTx")) Then .TSAI.Tx = CDbl(dbRow("cmTx")) Else .TSAI.Tx = NoValue
          If Not IsDBNull(dbRow("cmTy")) Then .TSAI.Ty = CDbl(dbRow("cmTy")) Else .TSAI.Ty = NoValue
          If Not IsDBNull(dbRow("cmTz")) Then .TSAI.Tz = CDbl(dbRow("cmTz")) Else .TSAI.Tz = NoValue
          If Not IsDBNull(dbRow("cmf")) Then .TSAI.f = CDbl(dbRow("cmf")) Else .TSAI.f = NoValue
          If Not IsDBNull(dbRow("cmk")) Then .TSAI.k = CDbl(dbRow("cmk")) Else .TSAI.k = NoValue
          If Not IsDBNull(dbRow("cmSx")) Then .TSAI.Sx = CDbl(dbRow("cmSx")) Else .TSAI.Sx = NoValue
          If Not IsDBNull(dbRow("cmdx")) Then .TSAI.dx = CDbl(dbRow("cmdx")) Else .TSAI.dx = NoValue
          If Not IsDBNull(dbRow("cmdy")) Then .TSAI.dy = CDbl(dbRow("cmdy")) Else .TSAI.dy = NoValue
          If Not IsDBNull(dbRow("cmCx")) Then .TSAI.Cx = CDbl(dbRow("cmCx")) Else .TSAI.Cx = NoValue
          If Not IsDBNull(dbRow("cmCy")) Then .TSAI.Cy = CDbl(dbRow("cmCy")) Else .TSAI.Cy = NoValue
        End With
        VideoFilesDB.Add(VideoFile)
      Next
    End Get

    Set(ByVal value As List(Of clsVideoFile))
      Dim VideoFile As clsVideoFile
      Dim dbRow As DataRow
      Dim dbRows() As DataRow
      Dim cmSQL As OleDbCommand
      Dim _DeletedVideoFiles As New List(Of clsVideoFile)

      For Each VideoFile In value
        Select Case VideoFile.Status
          Case clsVideoFile.VideoFileStatusUPDATED
            dbRows = MyDataSet.Tables("VideoFiles").Select("cmID = " & VideoFile.ID)
            dbRows(0)("cmVideoFile") = VideoFile.VideoFileName
            dbRows(0)("cmPosition") = VideoFile.Position
            dbRows(0)("cmVisible") = VideoFile.Visible
            dbRows(0)("cmStartTime") = Math.Round(VideoFile.StartTime, 3)
            dbRows(0)("cmFrameRate") = Math.Round(VideoFile.FrameRate, 3)
            dbRows(0)("cmComment") = VideoFile.Comment
            dbRows(0)("cmWidth") = VideoFile.FrameSize.Width
            dbRows(0)("cmHeight") = VideoFile.FrameSize.Height
            dbRows(0)("cmCallibrationOK") = VideoFile.CallibrationOK
            dbRows(0)("cmr1") = SetSQLValueDouble(VideoFile.TSAI.r1)
            dbRows(0)("cmr2") = SetSQLValueDouble(VideoFile.TSAI.r2)
            dbRows(0)("cmr3") = SetSQLValueDouble(VideoFile.TSAI.r3)
            dbRows(0)("cmr4") = SetSQLValueDouble(VideoFile.TSAI.r4)
            dbRows(0)("cmr5") = SetSQLValueDouble(VideoFile.TSAI.r5)
            dbRows(0)("cmr6") = SetSQLValueDouble(VideoFile.TSAI.r6)
            dbRows(0)("cmr7") = SetSQLValueDouble(VideoFile.TSAI.r7)
            dbRows(0)("cmr8") = SetSQLValueDouble(VideoFile.TSAI.r8)
            dbRows(0)("cmr9") = SetSQLValueDouble(VideoFile.TSAI.r9)
            dbRows(0)("cmTx") = SetSQLValueDouble(VideoFile.TSAI.Tx)
            dbRows(0)("cmTy") = SetSQLValueDouble(VideoFile.TSAI.Ty)
            dbRows(0)("cmTz") = SetSQLValueDouble(VideoFile.TSAI.Tz)
            dbRows(0)("cmf") = SetSQLValueDouble(VideoFile.TSAI.f)
            dbRows(0)("cmk") = SetSQLValueDouble(VideoFile.TSAI.k)
            dbRows(0)("cmSx") = SetSQLValueDouble(VideoFile.TSAI.Sx)
            dbRows(0)("cmdx") = SetSQLValueDouble(VideoFile.TSAI.dx)
            dbRows(0)("cmdy") = SetSQLValueDouble(VideoFile.TSAI.dy)
            dbRows(0)("cmCx") = SetSQLValueDouble(VideoFile.TSAI.Cx)
            dbRows(0)("cmCy") = SetSQLValueDouble(VideoFile.TSAI.Cy)
            MyDataSet.Tables("VideoFiles").AcceptChanges()

            cmSQL = New OleDbCommand
            cmSQL.Connection = MyOleDBConnection
            cmSQL.CommandText = "UPDATE VideoFiles SET " & _
                               "cmPosition = @Position, " & _
                               "cmVisible = @Visible, " & _
                              "cmComment = @Comment, " & _
                              "cmWidth = @Width, " & _
                              "cmHeight = @Height, " & _
                              "cmStartTime = @StartTime, " & _
                              "cmFrameRate = @FrameRate, " & _
                              "cmCallibrationOK = @CallibrationOK, " & _
                              "cmr1 = @r1, " & _
                              "cmr2 = @r2, " & _
                              "cmr3 = @r3, " & _
                              "cmr4 = @r4, " & _
                              "cmr5 = @r5, " & _
                              "cmr6 = @r6, " & _
                              "cmr7 = @r7, " & _
                              "cmr8 = @r8, " & _
                              "cmr9 = @r9, " & _
                              "cmTx = @Tx, " & _
                              "cmTy = @Ty, " & _
                              "cmTz = @Tz, " & _
                              "cmf = @f, " & _
                              "cmk = @k, " & _
                              "cmSx = @Sx, " & _
                              "cmdx = @dx, " & _
                              "cmdy = @dy, " & _
                              "cmCx = @Cx, " & _
                              "cmCy = @Cy " & _
                             "WHERE cmID = @ID"
            cmSQL.Parameters.AddWithValue("@Position", dbRows(0)("cmPosition"))
            cmSQL.Parameters.AddWithValue("@Visible", dbRows(0)("cmVisible"))
            cmSQL.Parameters.AddWithValue("@Comment", dbRows(0)("cmComment"))
            cmSQL.Parameters.AddWithValue("@Width", dbRows(0)("cmWidth"))
            cmSQL.Parameters.AddWithValue("@Height", dbRows(0)("cmHeight"))
            cmSQL.Parameters.AddWithValue("@StartFrame", dbRows(0)("cmStartTime"))
            cmSQL.Parameters.AddWithValue("@FrameRate", dbRows(0)("cmFrameRate"))
            cmSQL.Parameters.AddWithValue("@CallibrationOK", dbRows(0)("cmCallibrationOK"))
            cmSQL.Parameters.AddWithValue("@r1", dbRows(0)("cmr1"))
            cmSQL.Parameters.AddWithValue("@r2", dbRows(0)("cmr2"))
            cmSQL.Parameters.AddWithValue("@r3", dbRows(0)("cmr3"))
            cmSQL.Parameters.AddWithValue("@r4", dbRows(0)("cmr4"))
            cmSQL.Parameters.AddWithValue("@r5", dbRows(0)("cmr5"))
            cmSQL.Parameters.AddWithValue("@r6", dbRows(0)("cmr6"))
            cmSQL.Parameters.AddWithValue("@r7", dbRows(0)("cmr7"))
            cmSQL.Parameters.AddWithValue("@r8", dbRows(0)("cmr8"))
            cmSQL.Parameters.AddWithValue("@r9", dbRows(0)("cmr9"))
            cmSQL.Parameters.AddWithValue("@Tx", dbRows(0)("cmTx"))
            cmSQL.Parameters.AddWithValue("@Ty", dbRows(0)("cmTy"))
            cmSQL.Parameters.AddWithValue("@Tz", dbRows(0)("cmTz"))
            cmSQL.Parameters.AddWithValue("@f", dbRows(0)("cmf"))
            cmSQL.Parameters.AddWithValue("@k", dbRows(0)("cmk"))
            cmSQL.Parameters.AddWithValue("@Sx", dbRows(0)("cmSx"))
            cmSQL.Parameters.AddWithValue("@dx", dbRows(0)("cmdx"))
            cmSQL.Parameters.AddWithValue("@dy", dbRows(0)("cmdy"))
            cmSQL.Parameters.AddWithValue("@Cx", dbRows(0)("cmCx"))
            cmSQL.Parameters.AddWithValue("@Cy", dbRows(0)("cmCy"))
            cmSQL.Parameters.AddWithValue("@ID", VideoFile.ID)
            cmSQL.ExecuteNonQuery()
            cmSQL.Dispose()
            VideoFile.Status = clsVideoFile.VideoFileStatusUNCHANGED

          Case clsVideoFile.VideoFileStatusNEW
            dbRow = MyDataSet.Tables("VideoFiles").NewRow
            MyMaxVideoFileID = MyMaxVideoFileID + 1
            VideoFile.ID = MyMaxVideoFileID
            dbRow("cmID") = VideoFile.ID
            dbRow("cmVideoFile") = VideoFile.VideoFileName
            dbRow("cmPosition") = VideoFile.Position
            dbRow("cmVisible") = VideoFile.Visible
            dbRow("cmComment") = VideoFile.Comment
            dbRow("cmWidth") = VideoFile.FrameSize.Width
            dbRow("cmHeight") = VideoFile.FrameSize.Height
            dbRow("cmStartTime") = VideoFile.StartTime
            dbRow("cmFrameCount") = VideoFile.FrameCount
            dbRow("cmFrameRate") = VideoFile.FrameRate
            dbRow("cmComment") = VideoFile.Comment
            dbRow("cmDataMode") = MyDataMode
            dbRow("cmOwner") = RecordID_DB
            dbRow("cmCallibrationOK") = VideoFile.CallibrationOK
            dbRow("cmr1") = SetSQLValueDouble(VideoFile.TSAI.r1)
            dbRow("cmr2") = SetSQLValueDouble(VideoFile.TSAI.r2)
            dbRow("cmr3") = SetSQLValueDouble(VideoFile.TSAI.r3)
            dbRow("cmr4") = SetSQLValueDouble(VideoFile.TSAI.r4)
            dbRow("cmr5") = SetSQLValueDouble(VideoFile.TSAI.r5)
            dbRow("cmr6") = SetSQLValueDouble(VideoFile.TSAI.r6)
            dbRow("cmr7") = SetSQLValueDouble(VideoFile.TSAI.r7)
            dbRow("cmr8") = SetSQLValueDouble(VideoFile.TSAI.r8)
            dbRow("cmr9") = SetSQLValueDouble(VideoFile.TSAI.r9)
            dbRow("cmTx") = SetSQLValueDouble(VideoFile.TSAI.Tx)
            dbRow("cmTy") = SetSQLValueDouble(VideoFile.TSAI.Ty)
            dbRow("cmTz") = SetSQLValueDouble(VideoFile.TSAI.Tz)
            dbRow("cmf") = SetSQLValueDouble(VideoFile.TSAI.f)
            dbRow("cmk") = SetSQLValueDouble(VideoFile.TSAI.k)
            dbRow("cmSx") = SetSQLValueDouble(VideoFile.TSAI.Sx)
            dbRow("cmdx") = SetSQLValueDouble(VideoFile.TSAI.dx)
            dbRow("cmdy") = SetSQLValueDouble(VideoFile.TSAI.dy)
            dbRow("cmCx") = SetSQLValueDouble(VideoFile.TSAI.Cx)
            dbRow("cmCy") = SetSQLValueDouble(VideoFile.TSAI.Cy)
            MyDataSet.Tables("VideoFiles").Rows.Add(dbRow)
            MyDataSet.Tables("VideoFiles").AcceptChanges()

            cmSQL = New OleDbCommand
            cmSQL.Connection = MyOleDBConnection
            cmSQL.CommandText = "INSERT INTO VideoFiles " & _
                               "(cmID, cmVideoFile, cmPosition, cmVisible, cmComment, cmWidth, cmHeight, cmDataMode, cmOwner, cmStartTime, cmFrameCount, cmFrameRate, cmCallibrationOK, " & _
                              "cmr1, cmr2, cmr3, cmr4, cmr5, cmr6, cmr7, cmr8, cmr9, cmTx, cmTy, cmTz, cmf, cmk, cmSx, cmdx, cmdy, cmCx, cmCy) " & _
                             "VALUES (@ID, @VideoFile, @Position, @Visible, @Comment, @Width, @Height, @DataMode, @Owner, @StartTime, @FrameCount, @FrameRate, @CallibrationOK, " & _
                            "@r1, @r2, @r3, @r4, @r5, @r6, @r7, @r8, @r9, @Tx, @Ty, @Tz, @f, @k, @Sx, @dx, @dy, @Cx, @Cy)"

            cmSQL.Parameters.AddWithValue("@ID", dbRow("cmID"))
            cmSQL.Parameters.AddWithValue("@VideoFile", dbRow("cmVideoFile"))
            cmSQL.Parameters.AddWithValue("@Position", dbRow("cmPosition"))
            cmSQL.Parameters.AddWithValue("@Visible", dbRow("cmVisible"))
            cmSQL.Parameters.AddWithValue("@Comment", dbRow("cmComment"))
            cmSQL.Parameters.AddWithValue("@Width", dbRow("cmWidth"))
            cmSQL.Parameters.AddWithValue("@Height", dbRow("cmHeight"))
            cmSQL.Parameters.AddWithValue("@DataMode", dbRow("cmDataMode"))
            cmSQL.Parameters.AddWithValue("@Owner", dbRow("cmOwner"))
            cmSQL.Parameters.AddWithValue("@StartTime", dbRow("cmStartTime"))
            cmSQL.Parameters.AddWithValue("@FrameCount", dbRow("cmFrameCount"))
            cmSQL.Parameters.AddWithValue("@FrameRate", dbRow("cmFrameRate"))
            cmSQL.Parameters.AddWithValue("@CallibrationOK", dbRow("cmCallibrationOK"))
            cmSQL.Parameters.AddWithValue("@r1", dbRow("cmr1"))
            cmSQL.Parameters.AddWithValue("@r2", dbRow("cmr2"))
            cmSQL.Parameters.AddWithValue("@r3", dbRow("cmr3"))
            cmSQL.Parameters.AddWithValue("@r4", dbRow("cmr4"))
            cmSQL.Parameters.AddWithValue("@r5", dbRow("cmr5"))
            cmSQL.Parameters.AddWithValue("@r6", dbRow("cmr6"))
            cmSQL.Parameters.AddWithValue("@r7", dbRow("cmr7"))
            cmSQL.Parameters.AddWithValue("@r8", dbRow("cmr8"))
            cmSQL.Parameters.AddWithValue("@r9", dbRow("cmr9"))
            cmSQL.Parameters.AddWithValue("@Tx", dbRow("cmTx"))
            cmSQL.Parameters.AddWithValue("@Ty", dbRow("cmTy"))
            cmSQL.Parameters.AddWithValue("@Tz", dbRow("cmTz"))
            cmSQL.Parameters.AddWithValue("@f", dbRow("cmf"))
            cmSQL.Parameters.AddWithValue("@k", dbRow("cmk"))
            cmSQL.Parameters.AddWithValue("@Sx", dbRow("cmSx"))
            cmSQL.Parameters.AddWithValue("@dx", dbRow("cmdx"))
            cmSQL.Parameters.AddWithValue("@dy", dbRow("cmdy"))
            cmSQL.Parameters.AddWithValue("@Cx", dbRow("cmCx"))
            cmSQL.Parameters.AddWithValue("@Cy", dbRow("cmCy"))
            cmSQL.ExecuteNonQuery()
            cmSQL.Dispose()
            VideoFile.Status = clsVideoFile.VideoFileStatusUNCHANGED



          Case clsVideoFile.VideoFileStatusDELETED
            If Not VideoFile.ID.Equals(0) Then
              dbRows = MyDataSet.Tables("VideoFiles").Select("cmID = " & VideoFile.ID)
              If dbRows.Count > 0 Then dbRows(0).Delete()
              MyDataSet.Tables("VideoFiles").AcceptChanges()

              cmSQL = New OleDbCommand
              cmSQL.Connection = MyOleDBConnection
              cmSQL.CommandText = "DELETE * FROM VideoFiles WHERE cmID = @ID"
              cmSQL.Parameters.AddWithValue("@ID", VideoFile.ID)
              cmSQL.ExecuteNonQuery()
              cmSQL.Dispose()
            End If
            _DeletedVideoFiles.Add(VideoFile)
        End Select
      Next
      For Each _VideoFile In _DeletedVideoFiles
        value.Remove(_VideoFile)
      Next
    End Set
  End Property

  Protected Property RelatedRecordID() As Integer
    Get
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          RelatedRecordID = NoValue
        Case DataModeDETECTIONS
          If Not IsDBNull(MyDetectionRows(iMyRecordIndex)("cmVideoRecording")) Then
            RelatedRecordID = CInt(MyDetectionRows(iMyRecordIndex)("cmVideoRecording"))
          Else
            RelatedRecordID = NoValue
          End If
        Case Else
          RelatedRecordID = NoValue
      End Select
    End Get
    Set(ByVal value As Integer)
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS

        Case 2
          If Not MyDetectionRows(iMyRecordIndex)("cmVideoRecording").Equals(value) Then
            MyDetectionRows(iMyRecordIndex)("cmVideoRecording") = value
            MyUpdateRequired = True
          End If
      End Select
    End Set
  End Property

  Protected Property RoadUserTypesDB As List(Of clsRoadUserType)
    Get
      Dim RoadUserType As clsRoadUserType
      Dim dbRow As DataRow
      Dim dbRows() As DataRow

      RoadUserTypesDB = New List(Of clsRoadUserType)
      dbRows = MyDataSet.Tables("RoadUserTypes").Select("", "cmID ASC")
      For Each dbRow In dbRows
        RoadUserType = New clsRoadUserType
        With RoadUserType
          If Not IsDBNull(dbRow("cmName")) Then .Name = CStr(dbRow("cmName")) Else .Name = ""
          If Not IsDBNull(dbRow("cmLength")) Then .Length = CDbl(dbRow("cmLength")) Else .Length = NoValue
          If Not IsDBNull(dbRow("cmWidth")) Then .Width = CDbl(dbRow("cmWidth")) Else .Width = NoValue
          If Not IsDBNull(dbRow("cmHeight")) Then .Height = CDbl(dbRow("cmHeight")) Else .Height = NoValue
        End With
        RoadUserTypesDB.Add(RoadUserType)
      Next
    End Get

    Set(ByVal value As List(Of clsRoadUserType))

    End Set
  End Property

#End Region



#Region "Protected methods"

  'Makes a connection to the database file
 

  Protected Sub AddRecordDB(ByVal FrameCount As Integer, ByVal FrameRate As Double, Optional ByVal Background As String = "", Optional ByVal MapScale As Double = NoValue, Optional ByVal VideoRecording As Integer = NoValue)
    Dim cmSQL As OleDbCommand
    Dim drvNewRow As DataRow
    Dim i As Integer

    Select Case MyDataMode
      Case DataModeVIDEORECORDINGS
        MyMaxVideoRecordingID = MyMaxVideoRecordingID + 1
        drvNewRow = MyDataSet.Tables("VideoRecordings").NewRow()
        drvNewRow("cmID") = MyMaxVideoRecordingID
        drvNewRow("cmDateTime") = Now()
        drvNewRow("cmFrameCount") = FrameCount
        drvNewRow("cmFrameRate") = FrameRate
        drvNewRow("cmBackground") = Background
        If IsValue(MapScale) Then drvNewRow("cmScale") = MapScale Else drvNewRow("cmScale") = DBNull.Value

        MyDataSet.Tables("VideoRecordings").Rows.Add(drvNewRow)
        MyDataSet.Tables("VideoRecordings").AcceptChanges()
        Call SelectVideoRecordings()
        i = FindIndex(MyMaxVideoRecordingID)

        cmSQL = New OleDbCommand("INSERT INTO VideoRecordings (cmID, cmDateTime, cmFrameCount, cmFrameRate, cmBackground, cmScale) VALUES (@ID, @DateTime, @FrameCount, @FrameRate, @Background, @Scale)", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(i)("cmID"))
        cmSQL.Parameters.AddWithValue("@DateTime", MyVideoRecordingRows(i)("cmDateTime").ToString)
        cmSQL.Parameters.AddWithValue("@FrameCount", MyVideoRecordingRows(i)("cmFrameCount"))
        cmSQL.Parameters.AddWithValue("@FrameRate", MyVideoRecordingRows(i)("cmFrameRate"))
        cmSQL.Parameters.AddWithValue("@Background", MyVideoRecordingRows(i)("cmBackground"))
        cmSQL.Parameters.AddWithValue("@Scale", MyVideoRecordingRows(i)("cmScale"))
        cmSQL.ExecuteNonQuery()

      Case DataModeDETECTIONS
        MyMaxDetectionID = MyMaxDetectionID + 1
        drvNewRow = MyDataSet.Tables("Detections").NewRow()
        drvNewRow("cmID") = MyMaxDetectionID
        drvNewRow("cmDateTime") = Now()
        drvNewRow("cmVideoRecording") = VideoRecording
        drvNewRow("cmFrameCount") = FrameCount
        drvNewRow("cmFrameRate") = FrameRate
        drvNewRow("cmBackground") = Background
        If IsValue(MapScale) Then drvNewRow("cmScale") = MapScale Else drvNewRow("cmScale") = DBNull.Value

        MyDataSet.Tables("Detections").Rows.Add(drvNewRow)
        MyDataSet.Tables("Detections").AcceptChanges()
        Call SelectDetections()
        i = FindIndex(MyMaxDetectionID)

        cmSQL = New OleDbCommand("INSERT INTO Detections (cmID, cmDateTime, cmVideoRecording, cmFrameCount, cmFrameRate) VALUES (@ID, @DateTime, @VideoRecording, @FrameCount, @FrameRate)", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyDetectionRows(i)("cmID"))
        cmSQL.Parameters.AddWithValue("@DateTime", MyDetectionRows(i)("cmDateTime").ToString)
        If Not VideoRecording.Equals(NoValue) Then
          cmSQL.Parameters.AddWithValue("@VideoRecording", MyDetectionRows(i)("cmVideoRecording").ToString)
        Else
          cmSQL.Parameters.AddWithValue("@VideoRecording", DBNull.Value)
        End If
        cmSQL.Parameters.AddWithValue("@FrameCount", MyDetectionRows(i)("cmFrameCount"))
        cmSQL.Parameters.AddWithValue("@FrameRate", MyDetectionRows(i)("cmFrameRate"))
        cmSQL.Parameters.AddWithValue("@Background", MyDetectionRows(i)("cmBackground"))
        cmSQL.Parameters.AddWithValue("@Scale", MyDetectionRows(i)("cmScale"))
        cmSQL.ExecuteNonQuery()
    End Select

    iMyRecordIndex = i
  End Sub

  Protected Function DeleteRecordDB() As Integer
    Dim cmSQL As OleDbCommand
    Dim i As Integer
    Dim dbRows() As DataRow
    Dim dbRow As DataRow

    Select Case MyDataMode
      Case DataModeVIDEORECORDINGS
        cmSQL = New OleDbCommand("DELETE * FROM VideoRecordings WHERE cmID = @ID", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
        cmSQL.ExecuteNonQuery()

        cmSQL = New OleDbCommand("DELETE * FROM VideoFiles WHERE (cmDatamode = 1) AND (cmOwner = @ID)", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
        cmSQL.ExecuteNonQuery()

        cmSQL = New OleDbCommand("UPDATE Detections SET " & _
                                 "cmVideoRecording = @Null " & _
                                 "WHERE cmVideoRecording = @VideoRecordingID", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@Null", DBNull.Value)
        cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
        cmSQL.ExecuteNonQuery()

        dbRows = MyDataSet.Tables("Detections").Select("cmVideoRecording = " & MyVideoRecordingRows(iMyRecordIndex)("cmID").ToString)
        For Each dbRow In dbRows
          dbRow("cmVideoRecording") = DBNull.Value
        Next
        MyDataSet.Tables("Detections").AcceptChanges()

        dbRows = MyDataSet.Tables("VideoFiles").Select("(cmDataMode = 1) AND (cmOwner = " & MyVideoRecordingRows(iMyRecordIndex)("cmID").ToString & ")")
        For Each dbRow In dbRows
          dbRow.Delete()
        Next
        MyDataSet.Tables("VideoFiles").AcceptChanges()

        MyVideoRecordingRows(iMyRecordIndex).Delete()
        MyDataSet.Tables("VideoRecordings").AcceptChanges()
        Call FindMax()
        Call SelectVideoRecordings()

      Case DataModeDETECTIONS
        cmSQL = New OleDbCommand("DELETE * FROM Detections WHERE cmID = @ID", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyDetectionRows(iMyRecordIndex)("cmID"))
        cmSQL.ExecuteNonQuery()

        cmSQL = New OleDbCommand("DELETE * FROM VideoFiles WHERE (cmDatamode = 2) AND (cmOwner = @ID)", MyOleDBConnection)
        cmSQL.Parameters.AddWithValue("@ID", MyDetectionRows(iMyRecordIndex)("cmID"))
        cmSQL.ExecuteNonQuery()

        dbRows = MyDataSet.Tables("VideoFiles").Select("(cmDataMode = 2) AND (cmOwner = " & MyDetectionRows(iMyRecordIndex)("cmID").ToString & ")")
        For Each dbRow In dbRows
          dbRow.Delete()
        Next
        MyDataSet.Tables("VideoFiles").AcceptChanges()

        MyDetectionRows(iMyRecordIndex).Delete()
        MyDataSet.Tables("Detections").AcceptChanges()
        Call FindMax()
        Call SelectDetections()
    End Select

    If iMyRecordIndex > 0 Then
      i = iMyRecordIndex - 1
    ElseIf NumberOfFilteredRecords > 0 Then
      i = iMyRecordIndex
    Else
      i = -1
    End If

    Return i
  End Function

  'finds record's index in the database table by its ID-field value
  Protected Function FindIndex(ByVal iID As Integer) As Integer
    Dim i As Integer

    For i = 0 To NumberOfFilteredRecords - 1
      Select Case MyDataMode
        Case DataModeVIDEORECORDINGS
          If MyVideoRecordingRows(i)("cmID").Equals(iID) Then Return i
        Case DataModeDETECTIONS
          If MyDetectionRows(i)("cmID").Equals(iID) Then Return i
      End Select
    Next i

    Return -1
  End Function

#End Region



#Region "Internal subs"

  'Reads the tables to a dataset from the database
  Private Function ReadTables() As Boolean
    Dim _DataAdapter As OleDbDataAdapter
    Dim _DataRows() As DataRow
    Dim _DataRow As DataRow
    Dim _TableName As String

    MyDataSet = New DataSet
    Try
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM ProjectInfo", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "ProjectInfo")
      MyDataMode = CByte(MyDataSet.Tables("ProjectInfo").Rows(0)("cmDataMode"))

      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordings ORDER BY cmDateTime, cmID", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordings")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM Detections ORDER BY cmDateTime, cmID", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "Detections")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingStatus", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingStatus")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingType", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingType")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionStatus", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionStatus")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionType", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionType")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoFiles", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoFiles")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM RoadUserTypes", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "RoadUserTypes")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM Controls", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "Controls")
      _DataRows = MyDataSet.Tables("Controls").Select("cmType = 4")
      For Each _DataRow In _DataRows
        _TableName = "u" & _DataRow("cmName").ToString & "Values"
        _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM " & _TableName & " ORDER BY cmID", MyOleDBConnection)
        _DataAdapter.Fill(MyDataSet, _TableName)
      Next
      _DataAdapter.Dispose()

      Call FindMax()
    Catch ex As Exception
      MyDataSet.Dispose()
      MsgBox("Cannot open the necessary tables in the database file '" & MyDatabaseFilePath & "'!" & Environment.NewLine & Environment.NewLine & _
               "error message: """ & ex.Message & """", MsgBoxStyle.Critical)
      Return False
    End Try

    Return True
  End Function

  'Finds the highest values of ID-filed in the tables
  Private Sub FindMax()
    If MyDataSet.Tables("VideoRecordings").Rows.Count > 0 Then
      MyMaxVideoRecordingID = CInt(MyDataSet.Tables("VideoRecordings").Compute("Max(cmID)", ""))
    Else
      MyMaxVideoRecordingID = 9999
    End If
    If MyDataSet.Tables("Detections").Rows.Count > 0 Then
      MyMaxDetectionID = CInt(MyDataSet.Tables("Detections").Compute("Max(cmID)", ""))
    Else
      MyMaxDetectionID = -1
    End If
    If MyDataSet.Tables("VideoFiles").Rows.Count > 0 Then
      MyMaxVideoFileID = CInt(MyDataSet.Tables("VideoFiles").Compute("Max(cmID)", ""))
    Else
      MyMaxVideoFileID = -1
    End If
  End Sub

  'Selects the records from the VideoRecordings table
  Private Sub SelectVideoRecordings()
    MyVideoRecordingRows = MyDataSet.Tables("VideoRecordings").Select(MyFilter, SORT_ORDER)
    If MyVideoRecordingRows.Length = 0 Then iMyRecordIndex = -1
  End Sub

  'Selects the records from the Detections table
  Private Sub SelectDetections()
    MyDetectionRows = MyDataSet.Tables("Detections").Select(MyFilter, SORT_ORDER)
    If MyDetectionRows.Length = 0 Then iMyRecordIndex = -1
  End Sub






  Private Function GetSQLValueInteger(ByVal value As Object) As Integer
    If IsDBNull(value) Then Return NoValue Else Return CInt(value)
  End Function

  Private Function GetSQLValueDouble(ByVal value As Object) As Double
    If IsDBNull(value) Then Return NoValue Else Return CDbl(value)
  End Function

  Private Function GetSQLValueString(ByVal value As Object) As String
    If IsDBNull(value) Then Return "" Else Return CStr(value)
  End Function

  Private Function GetSQLValueDate(ByVal value As Object) As Date
    If IsDBNull(value) Then Return NoValueDate Else Return CDate(value)
  End Function



  Private Function SetSQLValueInteger(ByVal value As Integer) As Object
    If IsNotValue(value) Then Return DBNull.Value Else Return value
  End Function

  Private Function SetSQLValueDouble(ByVal value As Double) As Object
    If IsNotValue(value) Then Return DBNull.Value Else Return value
  End Function

  Private Function SetSQLValueDate(ByVal value As Date) As Object
    If IsNotValue(value) Then Return DBNull.Value Else Return value
  End Function

#End Region

End Class
