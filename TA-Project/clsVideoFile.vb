﻿Public Class clsVideoFile
  Public Status As VideoFileStatuses
  Public ID As Integer
  Public Position As Integer
  Public Visible As Boolean
  Public Comment As String
  Public ClockDiscrepancy As Double
  ' Public ClockDiscrepancyRate As Double
  Public FrameRate As Double
  Public FrameCount As Integer
  Public FrameSize As Size
  Public CallibrationOK As Boolean
  Public TSAI As Callibration_parameters_TSAI
  'Public TimeStamps


  Public Structure Callibration_parameters_TSAI
    Dim dx As Double
    Dim dy As Double
    Dim Cx As Double
    Dim Cy As Double
    Dim Sx As Double
    Dim f As Double
    Dim k As Double
    Dim Tx As Double
    Dim Ty As Double
    Dim Tz As Double
    Dim r1 As Double
    Dim r2 As Double
    Dim r3 As Double
    Dim r4 As Double
    Dim r5 As Double
    Dim r6 As Double
    Dim r7 As Double
    Dim r8 As Double
    Dim r9 As Double
  End Structure



  Private MyParentRecord As clsRecord
  Private MyVideoFileName As String
  Private MyReader As clsVideoReader
  Private MyIsOpened As Boolean



  Public Sub New(ByVal ParentRecord As clsRecord,
                 ByVal VideoFileName As String)

    MyParentRecord = ParentRecord
    MyVideoFileName = VideoFileName

    Me.ID = NoValue
    Me.Status = VideoFileStatuses.Created
    Me.Position = 0
    Me.ClockDiscrepancy = 0
    'Me.ClockDiscrepancyRate = 1
    Me.Visible = True
    Me.Comment = ""
    Me.FrameRate = NoValue
    Me.FrameCount = NoValue
    Me.FrameSize = Nothing
    Me.CallibrationOK = False
    Me.TSAI.dx = NoValue
    Me.TSAI.dy = NoValue
    Me.TSAI.Cx = NoValue
    Me.TSAI.Cy = NoValue
    Me.TSAI.Sx = NoValue
    Me.TSAI.f = NoValue
    Me.TSAI.k = NoValue
    Me.TSAI.Tx = NoValue
    Me.TSAI.Ty = NoValue
    Me.TSAI.Tz = NoValue
    Me.TSAI.r1 = NoValue
    Me.TSAI.r2 = NoValue
    Me.TSAI.r3 = NoValue
    Me.TSAI.r4 = NoValue
    Me.TSAI.r5 = NoValue
    Me.TSAI.r6 = NoValue
    Me.TSAI.r7 = NoValue
    Me.TSAI.r8 = NoValue
    Me.TSAI.r9 = NoValue

    MyIsOpened = False
  End Sub



#Region "Properties"



  Public ReadOnly Property ParentRecord As clsRecord
    Get
      Return MyParentRecord
    End Get
  End Property



  Public ReadOnly Property VideoFileName As String
    Get
      Return MyVideoFileName
    End Get
  End Property



  Public ReadOnly Property VideoFilePath As String
    Get
      VideoFilePath = MyParentRecord.VideoDirectory & MyVideoFileName
    End Get
  End Property



  Public ReadOnly Property Type As VideoFileTypes
    Get
      Return MyReader.Type
    End Get
  End Property



  Public ReadOnly Property IsOpened As Boolean
    Get
      Return MyIsOpened
    End Get
  End Property


  
#End Region



#Region "Public methods"



  Public Function CreateCopy(ByVal ParentRecord As clsRecord) As clsVideoFile
    Dim _NewVideoFile As clsVideoFile

    _NewVideoFile = New clsVideoFile(ParentRecord, MyVideoFileName)
    With _NewVideoFile
      .Comment = Me.Comment
      .ClockDiscrepancy = Me.ClockDiscrepancy
      ' .ClockDiscrepancyRate = Me.ClockDiscrepancyRate
      .FrameRate = Me.FrameRate
      .FrameCount = Me.FrameCount
      .FrameSize = Me.FrameSize
      .Position = Me.Position
      .Visible = Me.Visible
      .CallibrationOK = Me.CallibrationOK
      .TSAI.dx = Me.TSAI.dx
      .TSAI.dy = Me.TSAI.dy
      .TSAI.Cx = Me.TSAI.Cx
      .TSAI.Cy = Me.TSAI.Cy
      .TSAI.Sx = Me.TSAI.Sx
      .TSAI.f = Me.TSAI.f
      .TSAI.k = Me.TSAI.k
      .TSAI.Tx = Me.TSAI.Tx
      .TSAI.Ty = Me.TSAI.Ty
      .TSAI.Tz = Me.TSAI.Tz
      .TSAI.r1 = Me.TSAI.r1
      .TSAI.r2 = Me.TSAI.r2
      .TSAI.r3 = Me.TSAI.r3
      .TSAI.r4 = Me.TSAI.r4
      .TSAI.r5 = Me.TSAI.r5
      .TSAI.r6 = Me.TSAI.r6
      .TSAI.r7 = Me.TSAI.r7
      .TSAI.r8 = Me.TSAI.r8
      .TSAI.r9 = Me.TSAI.r9
    End With

    Return _NewVideoFile
  End Function



  Public Sub Open()
    MyReader = New clsVideoReader(Me.VideoFilePath)
    MyIsOpened = True
    Call LoadDataFromReader()
  End Sub



  Public Sub Close()
    MyReader.Close()
    MyIsOpened = False
  End Sub



  Public Sub LoadDataFromReader()
    If Not MyIsOpened Then Me.Open()
    Me.FrameCount = MyReader.FrameCount
    Me.FrameRate = MyReader.FPS
    Me.FrameSize = MyReader.FrameSize
  End Sub



  Public Function GetTimeStamp(ByVal Frame As Integer) As Double
    If Not MyIsOpened Then Me.Open()
    Return MyReader.GetTimeStamp(Frame)
  End Function


    Public Function GetTimeStampDate(ByVal Frame As Integer) As Date
    If Not MyIsOpened Then Me.Open()
    Return Date.FromOADate(MyReader.GetTimeStamp(Frame) / 24 / 60 / 60)
  End Function

    Public Function GetVideoFileName() As String
        Return Me.VideoFileName
    End Function


    Public Function ImportCallibration(ByVal CallibrationFilePath As String) As Boolean
    Dim _FileIndex As Integer
    Dim _Line As String

    If File.Exists(CallibrationFilePath) Then
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, CallibrationFilePath, OpenMode.Input)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.dx = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.dy = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Cx = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Cy = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Sx = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.f = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.k = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Tx = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Ty = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.Tz = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r1 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r2 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r3 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r4 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r5 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r6 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r7 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r8 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex).Substring(9) : Me.TSAI.r9 = Double.Parse(_Line, CultureInfo.InvariantCulture)
      FileClose(_FileIndex)
      Me.CallibrationOK = True
    Else
      Me.CallibrationOK = False
    End If

    Return CallibrationOK
  End Function



  Public Sub ExportTimeStamps(ByVal FilePath As String,
                              Optional ByVal FromFrame As Integer = NoValue,
                              Optional ByVal ToFrame As Integer = NoValue)
    Dim _FileIndex As Integer
    Dim _TimeStamp As Double
    Dim _TimeStampDate As Date
    Dim _Line As String
    Dim _StartFrame, _EndFrame As Integer

    _FileIndex = FreeFile()
    FileOpen(_FileIndex, FilePath, OpenMode.Output)

    If IsValue(FromFrame) Then _StartFrame = FromFrame Else _StartFrame = 0
    If IsValue(ToFrame) Then _EndFrame = ToFrame Else _EndFrame = Me.FrameCount - 1

    For _Frame = _StartFrame To _EndFrame
      _TimeStamp = MyReader.GetTimeStamp(_Frame)
      _TimeStampDate = Date.FromOADate(_TimeStamp / 60 / 60 / 24)
      _Line = (_Frame - _StartFrame).ToString("00000") & " " & Format(_TimeStampDate, "yyyy MM dd HH:mm:ss.fff")
      PrintLine(_FileIndex, _Line)
    Next

    FileClose(_FileIndex)

  End Sub

  Public Sub ExportCalibration(FilePath As String)
    Dim _FileIndex As Integer

    _FileIndex = FreeFile()
    FileOpen(_FileIndex, FilePath, OpenMode.Output)

    PrintLine(_FileIndex, "dx:", TAB(10), Me.TSAI.dx.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "dy:", TAB(10), Me.TSAI.dy.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Cx:", TAB(10), Me.TSAI.Cx.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Cy:", TAB(10), Me.TSAI.Cy.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Sx:", TAB(10), Me.TSAI.Sx.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, " f:", TAB(10), Me.TSAI.f.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, " k:", TAB(10), Me.TSAI.k.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Tx:", TAB(10), Me.TSAI.Tx.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Ty:", TAB(10), Me.TSAI.Ty.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Tz:", TAB(10), Me.TSAI.Tz.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r1:", TAB(10), Me.TSAI.r1.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r2:", TAB(10), Me.TSAI.r2.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r3:", TAB(10), Me.TSAI.r3.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r4:", TAB(10), Me.TSAI.r4.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r5:", TAB(10), Me.TSAI.r5.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r6:", TAB(10), Me.TSAI.r6.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r7:", TAB(10), Me.TSAI.r7.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r8:", TAB(10), Me.TSAI.r8.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "r9:", TAB(10), Me.TSAI.r9.ToString(CultureInfo.InvariantCulture))

    FileClose(_FileIndex)

  End Sub

  Public Function GetFrameBitmap(ByVal Frame As Integer) As Bitmap
    If Not MyIsOpened Then Me.Open()
    Try
      Return MyReader.GetFrame(Frame)
    Catch
      Return EmptyBitmap(Me.FrameSize)
    End Try
  End Function



  Public Function TransformWorldToImage(ByVal WorldX As Double, ByVal WorldY As Double, Optional ByVal WorldZ As Double = 0) As Point
    Const _SQRT3 As Double = 1.7320508075688772
    Dim _Xc, _Yc, _Zc As Double
    Dim _Xu, _Yu As Double
    Dim _Xd As Double, _Yd As Double
    Dim _Ru As Double
    Dim _Rd As Double
    Dim _c As Double
    Dim _d As Double
    Dim _Q As Double
    Dim _R As Double
    Dim _Di As Double
    Dim _S As Double
    Dim _T As Double
    Dim _sinT As Double, _cosT As Double
    Dim _Lambda As Double
    Dim _Xf As Double
    Dim _Yf As Double


    If CallibrationOK Then
      'world to camera XYZ
      _Xc = Me.TSAI.r1 * WorldX + Me.TSAI.r2 * WorldY + Me.TSAI.r3 * WorldZ + Me.TSAI.Tx
      _Yc = Me.TSAI.r4 * WorldX + Me.TSAI.r5 * WorldY + Me.TSAI.r6 * WorldZ + Me.TSAI.Ty
      _Zc = Me.TSAI.r7 * WorldX + Me.TSAI.r8 * WorldY + Me.TSAI.r9 * WorldZ + Me.TSAI.Tz


      'camera XYZ to undistrorted sensor
      _Xu = Me.TSAI.f * _Xc / _Zc
      _Yu = Me.TSAI.f * _Yc / _Zc


      'undistorted sensor to distorted sensor
      _Ru = Math.Sqrt(_Xu ^ 2 + _Yu ^ 2)
      _c = 1 / Me.TSAI.k
      _d = -_c * _Ru

      _Q = _c / 3
      _R = -_d / 2
      _Di = _Q ^ 3 + _R ^ 2

      If _Di >= 0 Then
        _Di = Math.Sqrt(_Di)
        _S = Sqrt3(_R + _Di)
        _T = Sqrt3(_R - _Di)
        _Rd = _S + _T

        If _Rd < 0 Then
          _Rd = Math.Sqrt(-1 / (3 * Me.TSAI.k))
        End If
      Else
        _Di = Math.Sqrt(-_Di)
        _S = Sqrt3(Math.Sqrt(_R ^ 2 + _Di ^ 2))
        _T = Math.Atan2(_Di, _R) / 3
        _sinT = Math.Sin(_T)
        _cosT = Math.Cos(_T)
        _Rd = -_S * _cosT + _SQRT3 * _S * _sinT
      End If

      _Lambda = _Rd / _Ru

      _Xd = _Xu * _Lambda
      _Yd = _Yu * _Lambda


      'distorted sensor to final image
      _Xf = Me.TSAI.Sx * _Xd / Me.TSAI.dx + Me.TSAI.Cx
      _Yf = _Yd / Me.TSAI.dy + Me.TSAI.Cy

      Return New Point(CInt(_Xf), CInt(_Yf))
    Else
      Return Nothing
    End If
  End Function



  Public Function TransformImageToWorld(ByVal FrameX As Integer, ByVal FrameY As Integer, ByVal Height As Double, ByVal Map As clsMap) As clsGeomPoint3D
    Dim _PointW, _PointW_Old As clsGeomPoint3D
    Dim _Height As Double
    Dim _i As Integer


    _PointW = TransformImageToWorldXYZ(FrameX, FrameY, Height)

    If (_PointW IsNot Nothing) And (Map IsNot Nothing) Then
      _i = 0
      Do
        _PointW_Old = _PointW.CreateClone
        _Height = Map.EstimateHeight(_PointW_Old.X, _PointW_Old.Y) + Height
        _PointW = TransformImageToWorldXYZ(FrameX, FrameY, _Height)
        _i = _i + 1
      Loop Until (_i > 50) Or (_PointW.DistanceTo(_PointW_Old) < 0.01)
    End If

    Return _PointW
  End Function


  Private Function TransformImageToWorldXYZ(ByVal FrameX As Integer, ByVal FrameY As Integer, ByVal WorldZ As Double) As clsGeomPoint3D
    Dim _Xw, _Yw As Double
    Dim _Xu, _Yu As Double
    Dim _Xd As Double, _Yd As Double
    Dim _r As Double
    Dim _cd As Double

    If Me.CallibrationOK Then
      'convert from image to distorted sensor coordinates
      _Xd = Me.TSAI.dx * (FrameX - Me.TSAI.Cx) / Me.TSAI.Sx
      _Yd = Me.TSAI.dy * (FrameY - Me.TSAI.Cy)


      'convert from distorted sensor to undistorted sensor plane coordinates
      _r = _Xd ^ 2 + _Yd ^ 2
      _Xu = _Xd * (1 + Me.TSAI.k * _r)
      _Yu = _Yd * (1 + Me.TSAI.k * _r)


      'calculate the corresponding _Xw and _Yw world coordinates
      _cd = ((Me.TSAI.r1 * Me.TSAI.r8 - Me.TSAI.r2 * Me.TSAI.r7) * _Yu +
            (Me.TSAI.r5 * Me.TSAI.r7 - Me.TSAI.r4 * Me.TSAI.r8) * _Xu -
            Me.TSAI.f * Me.TSAI.r1 * Me.TSAI.r5 + Me.TSAI.f * Me.TSAI.r2 * Me.TSAI.r4)

      _Xw = (((Me.TSAI.r2 * Me.TSAI.r9 - Me.TSAI.r3 * Me.TSAI.r8) * _Yu +
              (Me.TSAI.r6 * Me.TSAI.r8 - Me.TSAI.r5 * Me.TSAI.r9) * _Xu -
               Me.TSAI.f * Me.TSAI.r2 * Me.TSAI.r6 + Me.TSAI.f * Me.TSAI.r3 * Me.TSAI.r5) * WorldZ +
              (Me.TSAI.r2 * Me.TSAI.Tz - Me.TSAI.r8 * Me.TSAI.Tx) * _Yu +
              (Me.TSAI.r8 * Me.TSAI.Ty - Me.TSAI.r5 * Me.TSAI.Tz) * _Xu -
               Me.TSAI.f * Me.TSAI.r2 * Me.TSAI.Ty + Me.TSAI.f * Me.TSAI.r5 * Me.TSAI.Tx) / _cd

      _Yw = -(((Me.TSAI.r1 * Me.TSAI.r9 - Me.TSAI.r3 * Me.TSAI.r7) * _Yu +
               (Me.TSAI.r6 * Me.TSAI.r7 - Me.TSAI.r4 * Me.TSAI.r9) * _Xu -
                Me.TSAI.f * Me.TSAI.r1 * Me.TSAI.r6 + Me.TSAI.f * Me.TSAI.r3 * Me.TSAI.r4) * WorldZ +
               (Me.TSAI.r1 * Me.TSAI.Tz - Me.TSAI.r7 * Me.TSAI.Tx) * _Yu +
               (Me.TSAI.r7 * Me.TSAI.Ty - Me.TSAI.r4 * Me.TSAI.Tz) * _Xu -
                Me.TSAI.f * Me.TSAI.r1 * Me.TSAI.Ty + Me.TSAI.f * Me.TSAI.r4 * Me.TSAI.Tx) / _cd


      Return New clsGeomPoint3D(_Xw, _Yw, WorldZ)
    Else
      Return Nothing
    End If
  End Function



  Public Function OnePixelError(ByVal FrameX As Integer, ByVal FrameY As Integer, ByRef Map As clsMap) As Double
    Dim _Point1, _Point2 As clsGeomPoint3D

    If Map IsNot Nothing Then
      _Point1 = Me.TransformImageToWorld(FrameX, FrameY, 0, Map)
      _Point2 = Me.TransformImageToWorld(FrameX + 1, FrameY + 1, 0, Map)
    Else
      _Point1 = Me.TransformImageToWorldXYZ(FrameX, FrameY, 0)
      _Point2 = Me.TransformImageToWorldXYZ(FrameX + 1, FrameY + 1, 0)
    End If
    If IsNothing(_Point1) Or IsNothing(_Point2) Then
      Return NoValue
    Else
      Return _Point1.DistanceTo(_Point2)
    End If

  End Function



  Public Function GetAbsTime(ByVal Frame As Integer) As Double
    Return Me.ClockDiscrepancy + Me.GetTimeStamp(Frame) '* Me.ClockDiscrepancyRate
  End Function


  Public Function GetFrameIndex(ByVal AbsTimeToDisplay As Double) As Integer
    Dim _FirstGuess, _SecondGuess As Integer
    Dim _BestGuessFrame1 As Integer, _BestGuessFrame2 As Integer
    Dim _Iteration As Integer
    Dim _dT1 As Double, _dT2 As Double


    _FirstGuess = CInt((AbsTimeToDisplay - GetAbsTime(0)) * Me.FrameRate)
    If _FirstGuess < 0 Then _FirstGuess = 0
    If _FirstGuess > (Me.FrameCount - 1) Then _FirstGuess = Me.FrameCount - 1

    _SecondGuess = CInt(_FirstGuess + (AbsTimeToDisplay - GetAbsTime(_FirstGuess)) * Me.FrameRate)
    If _SecondGuess < 1 Then _SecondGuess = 1
    If _SecondGuess > (Me.FrameCount - 2) Then _SecondGuess = Me.FrameCount - 2

    _BestGuessFrame1 = _SecondGuess
    _dT1 = AbsTimeToDisplay - GetAbsTime(_BestGuessFrame1)
    _Iteration = Math.Sign(_dT1)

    Do
      _BestGuessFrame1 = _BestGuessFrame1 + _Iteration
      _dT1 = AbsTimeToDisplay - GetAbsTime(_BestGuessFrame1)
    Loop While (_dT1 * _Iteration > 0) And IsBetween(_BestGuessFrame1, 1, Me.FrameCount - 2)

    _BestGuessFrame2 = _BestGuessFrame1 - _Iteration
    If IsBetween(_BestGuessFrame2, 0, Me.FrameCount - 1) Then
      _dT2 = AbsTimeToDisplay - GetAbsTime(_BestGuessFrame2)
    Else
      _BestGuessFrame2 = _BestGuessFrame1
      _dT2 = _dT1
    End If

    If Math.Abs(_dT1) < Math.Abs(_dT2) Then
      Return _BestGuessFrame1
    Else
      Return _BestGuessFrame2
    End If

  End Function






#End Region

End Class
