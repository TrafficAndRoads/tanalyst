﻿Imports System.IO
Imports System.IO.Compression

Public Class clsZipTimeReader
  Private MyZipFilePath As String
  Private MyZipArchieve As ZipArchive
  Private MyFrameCount As Integer
  Private MyFrameSize As Size
  Private MyFPS As Double




  Public Sub New(ZipFilePath As String)
    MyZipFilePath = ZipFilePath
    MyZipArchieve = ZipFile.OpenRead(ZipFilePath)
    Call Me.ReadInfo()
  End Sub



  Public ReadOnly Property FPS As Double
    Get
      Return MyFPS
    End Get
  End Property



  Public ReadOnly Property FrameCount As Integer
    Get
      FrameCount = MyFrameCount
    End Get
  End Property



  Public ReadOnly Property FrameSize As Size
    Get
      Dim _FirstFrame As Bitmap

      _FirstFrame = Me.GetFrame(0)
      FrameSize = New Size(_FirstFrame.Width, _FirstFrame.Height)
    End Get
  End Property



  Public Sub Close()
    MyZipArchieve.Dispose()
  End Sub



  Public Function GetFrame(ByVal FrameIndex As Integer) As Bitmap
    Dim _OriginalBitmap, _NewBitmap As Bitmap
    Dim _BitmapStream As Stream
    Dim _g As Graphics

    _BitmapStream = MyZipArchieve.Entries(FrameIndex).Open
    _OriginalBitmap = New Bitmap(_BitmapStream)
    '_BitmapStream.Close()
    '_BitmapStream.Dispose()

    '_NewBitmap = New Bitmap(_OriginalBitmap.Width, _OriginalBitmap.Height)
    '_g = Graphics.FromImage(_NewBitmap)
    '_g.DrawImage(_OriginalBitmap, 0, 0)
    '_OriginalBitmap.Dispose()
    '_g.Dispose()

    'Return _NewBitmap
    Return _OriginalBitmap
  End Function


  Public Function GetTimeStamps() As List(Of Double)
    Dim _TimeStamps As List(Of Double)
    Dim _TempDate As Date
    Dim _FileName As String
    Dim _Year, _Month, _Day, _Hour, _Minute, _Second, _Millisecond As Integer


    _TimeStamps = New List(Of Double)
    For _i = 0 To MyZipArchieve.Entries.Count - 2
      _FileName = MyZipArchieve.Entries(_i).Name
      _Year = Integer.Parse(_FileName.Substring(0, 4))
      _Month = Integer.Parse(_FileName.Substring(4, 2))
      _Day = Integer.Parse(_FileName.Substring(6, 2))
      _Hour = Integer.Parse(_FileName.Substring(9, 2))
      _Minute = Integer.Parse(_FileName.Substring(11, 2))
      _Second = Integer.Parse(_FileName.Substring(13, 2))
      _Millisecond = Integer.Parse(_FileName.Substring(16, 3))

      _TempDate = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second, _Millisecond)
      _TimeStamps.Add(_TempDate.ToOADate * 24 * 60 * 60)
    Next

    Return _TimeStamps
  End Function



  Private Sub ReadInfo()
    Dim _DataStream As Stream
    Dim _StreamReader As StreamReader
    Dim _Line As String
    Dim _Size() As String
    Dim _Width, _Height As Integer


    _DataStream = MyZipArchieve.GetEntry("info.txt").Open
    _StreamReader = New StreamReader(_DataStream)

    _Line = _StreamReader.ReadLine
    _Size = _Line.Substring(6).Split(CChar("x"))
    Integer.TryParse(_Size(0), _Width)
    Integer.TryParse(_Size(1), _Height)
    MyFrameSize = New Size(_Width, _Height)

    _Line = _StreamReader.ReadLine
    Integer.TryParse(_Line.Substring(13), MyFrameCount)

    _Line = _StreamReader.ReadLine
    Double.TryParse(_Line.Substring(5), MyFPS)

    _StreamReader.Close()
    _StreamReader.Dispose()
    _DataStream.Close()
    _DataStream.Dispose()

  End Sub
End Class
