﻿Imports AviFile
Imports System.Drawing

Public Class AviWriter
  Public HorisontalCount As Integer = 1

  Private MyAviManager As AviManager
  Private MyVideoStream As VideoStream
  Private MyBitmapsPerFrame As Integer = 0
  Private MyFrameRate As Double
  Private pImagePositions As Point()
  Private MyFrameWidth As Integer
  Private MyFrameHeight As Integer



  Public Sub New(ByVal sFile As String, ByVal FrameRate As Double)
    MyAviManager = New AviManager(sFile, False)
    MyFrameRate = FrameRate
  End Sub

  Public Sub Close()
    MyAviManager.Close()
  End Sub

  Public Sub AddFrame(ByVal ParamArray Frames() As Bitmap)
    Dim opts As Avi.AVICOMPRESSOPTIONS

    If MyVideoStream Is Nothing Then
      MyBitmapsPerFrame = Frames.Length
      pImagePositions = FramePositions(Frames)

      opts = New Avi.AVICOMPRESSOPTIONS()
      opts.fccHandler = CUInt(Avi.mmioStringToFOURCC("XVID", 0))
      MyVideoStream = MyAviManager.AddVideoStream(opts, MyFrameRate, CombineFrames(Frames))

    ElseIf Frames.Length <> MyBitmapsPerFrame Then
      Throw New Exception("Must use same number of bitmaps as preceeding calls, got " & Frames.Length.ToString & " bitmaps, expected " & MyBitmapsPerFrame & " bitmaps.")
    Else
      MyVideoStream.AddFrame(CombineFrames(Frames))
    End If

  End Sub



  Private Function CombineFrames(ByVal Frames() As Bitmap) As Bitmap
    Dim g As Graphics
    Dim i As Integer
    Dim bCombinedFrame As Bitmap

    bCombinedFrame = New Bitmap(MyFrameWidth, MyFrameHeight)
    g = Graphics.FromImage(bCombinedFrame)

    For i = 0 To MyBitmapsPerFrame - 1
      g.DrawImage(Frames(i), pImagePositions(i))
    Next i

    Return bCombinedFrame
  End Function

  Private Function FramePositions(ByVal Frames() As Bitmap) As Point()
    Dim ps() As Point
    Dim maxWidth As Integer = 0
    Dim maxHeight As Integer = 0
    Dim i As Integer

    ReDim ps(Frames.Length)

    For i = 0 To MyBitmapsPerFrame - 1
      If Frames(i).Width > maxWidth Then maxWidth = Frames(i).Width
      If Frames(i).Height > maxHeight Then maxHeight = Frames(i).Height
    Next i
    MyFrameWidth = HorisontalCount * maxWidth
    MyFrameHeight = maxHeight * ((MyBitmapsPerFrame - 1) \ HorisontalCount + 1)

    For i = 0 To MyBitmapsPerFrame - 1
      ps(i) = New Point(CInt((i Mod HorisontalCount) * maxWidth + (maxWidth - Frames(i).Width) / 2), _
                        CInt((i \ HorisontalCount) * maxHeight + (maxHeight - Frames(i).Height) / 2))
    Next i

    Return ps
  End Function

End Class
