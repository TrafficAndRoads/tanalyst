﻿Public Class clsVisualiserOptions
  Public TrajectoryType As TrajectoryTypes = TrajectoryTypes.Smoothed

  Public DrawRoadUsers As Boolean = True
  Public NormalRUColour As Color = Color.White
  Public SelectedRUColour As Color = Color.Yellow
  Public Key1RUColour As Color = Color.Red
  Public Key2RUColour As Color = Color.Green
  Public ProjectedRUColour As Color = Color.Violet
  Public LineWidthRoadUser As Single = 1

  Public DrawTrajectories As Boolean = True
  Public NormalTrajectoryColour As Color = Color.White
  Public SelectedTrajectoryColour As Color = Color.Yellow
  Public Key1TrajectoryColour As Color = Color.Red
  Public Key2TrajectoryColour As Color = Color.Green
  Public ProjectedTrajectoryColour As Color = Color.Violet
  Public LineWidthTrajectory As Single = 1

  Public DrawSpeeds As Boolean = True
  Public LineColourSpeedRoadUserKey1 As Color = Color.Red
  Public LineColourSpeedRoadUserKey2 As Color = Color.Green
  Public LineColourSpeedAfterCollision As Color = Color.SteelBlue
  Public LineColourRelativeSpeed As Color = Color.Blue
  Public LineWidthSpeed As Single = 3

  Public DrawID As Boolean = False
  Public TextColourIDNormal As Color = Color.Red
  Public TextColourIDSelected As Color = Color.Red
  Public TextColourIDKey As Color = Color.Red
  Public TextColourIDProjected As Color = Color.Red
  Public TextFontID As Font = New Font("Times New Roman", 12, FontStyle.Bold)

  Public TextFontMessage As Font = New Font("Times New Roman", 12, FontStyle.Bold)

  Public LineColourGate As Color = Color.Aqua
  Public LineWidthGate As Integer = 1

  Public DrawMessagePoints As Boolean = True
  Public DrawMessageLines As Boolean = True


  Public ReadOnly Property PenRoadUserNormal As Pen
    Get
      Return New Pen(NormalRUColour, LineWidthRoadUser)
    End Get
  End Property

  Public ReadOnly Property PenRoadUserSelected As Pen
    Get
      Return New Pen(SelectedRUColour, LineWidthRoadUser)
    End Get
  End Property

  Public ReadOnly Property PenRoadUserKey1 As Pen
    Get
      Return New Pen(Key1RUColour, LineWidthRoadUser)
    End Get
  End Property

  Public ReadOnly Property PenRoadUserKey2 As Pen
    Get
      Return New Pen(Key2RUColour, LineWidthRoadUser)
    End Get
  End Property
  Public ReadOnly Property PenRoadUserProjected As Pen
    Get
      Return New Pen(ProjectedRUColour, LineWidthRoadUser)
    End Get
  End Property

  Public ReadOnly Property PenTrajectoryNormal As Pen
    Get
      Return New Pen(NormalTrajectoryColour, LineWidthTrajectory)
    End Get
  End Property

  Public ReadOnly Property PenTrajectorySelected As Pen
    Get
      Return New Pen(SelectedTrajectoryColour, LineWidthTrajectory)
    End Get
  End Property

  Public ReadOnly Property PenTrajectoryKey1 As Pen
    Get
      Return New Pen(Key1TrajectoryColour, LineWidthTrajectory)
    End Get
  End Property

  Public ReadOnly Property PenTrajectoryKey2 As Pen
    Get
      Return New Pen(Key2TrajectoryColour, LineWidthTrajectory)
    End Get
  End Property

  Public ReadOnly Property PenTrajectoryProjected As Pen
    Get
      Return New Pen(ProjectedTrajectoryColour, LineWidthTrajectory)
    End Get
  End Property

  Public ReadOnly Property SolidBrushIDNormal As SolidBrush
    Get
      Return New SolidBrush(TextColourIDNormal)
    End Get
  End Property

  Public ReadOnly Property SolidBrushIDSelected As SolidBrush
    Get
      Return New SolidBrush(TextColourIDSelected)
    End Get
  End Property

  Public ReadOnly Property SolidBrushIDKey As SolidBrush
    Get
      Return New SolidBrush(TextColourIDKey)
    End Get
  End Property

  Public ReadOnly Property SolidBrushIDProjected As SolidBrush
    Get
      Return New SolidBrush(TextColourIDProjected)
    End Get
  End Property

  Public Sub LoadVisualiserOptions(FilePath As String)
    'Dim _FileIndex As Integer

  End Sub


  Public Sub SaveVisualiserOptions(FilePath As String)
    'Dim _FileIndex As Integer

  End Sub

End Class
