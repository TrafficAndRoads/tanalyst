﻿Public Class clsTimeLinePoint
  Public RoadUsers As New List(Of clsRoadUser)
  Public VideoFileFrames As New List(Of Integer)
  Public MessagePoints As New List(Of clsMessagePoint)
  Public MessageLines As New List(Of clsMessageLine)
  Public Indicators As clsIndicatorList
End Class
