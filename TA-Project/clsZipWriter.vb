﻿Imports System.IO
Imports System.IO.Compression


Public Class clsZipWriter
  Private MyZipFilePath As String
  Private MyZipArchive As ZipArchive
  Private MyFrameCount As Integer
  Private MyFrameSize As Size



  Public ReadOnly Property FrameCount As Integer
    Get
      FrameCount = MyFrameCount
    End Get
  End Property



  Public ReadOnly Property FrameSize As Size
    Get
      FrameSize = MyFrameSize
    End Get
  End Property



  Public Sub New(ZipFilePath As String)
    MyZipFilePath = ZipFilePath
    If File.Exists(MyZipFilePath) Then File.Delete(MyZipFilePath)
    MyZipArchive = ZipFile.Open(MyZipFilePath, ZipArchiveMode.Create)
    MyFrameCount = 0
  End Sub



  Public Sub Close()
    MyZipArchive.Dispose()
  End Sub



  Public Sub CreateFromDirectory(ByVal InputDirectory As String, Optional ByVal FrameImageFormat As String = "jpg")
    'Dim _InputFrameFile As String
    'Dim _i As Integer

    'Me.AddInfo(InputDirectory & "info.txt")

    '_i = 0
    '_InputFrameFile = InputDirectory & Format(_i, "00000000") & "." & FrameImageFormat
    'If Not File.Exists(_InputFrameFile) Then
    '  _i = 1
    '  _InputFrameFile = InputDirectory & Format(_i, "00000000") & "." & FrameImageFormat
    'End If

    'Do While File.Exists(_InputFrameFile)
    '  Me.AddFrame(_InputFrameFile)
    '  _i = _i + 1
    '  _InputFrameFile = InputDirectory & Format(_i, "00000000") & "." & FrameImageFormat
    'Loop

  End Sub



  Public Sub AddFrame(FrameFilePath As String)
    'Dim _FileInfo As FileInfo
    'Dim _Bitmap As Bitmap

    'If MyFrameCount = 0 Then
    '  _Bitmap = New Bitmap(FrameFilePath)
    '  MyFrameSize = New Size(_Bitmap.Width, _Bitmap.Height)
    '  _Bitmap.Dispose()
    'End If

    '_FileInfo = New FileInfo(FrameFilePath)
    'MyZipArchive.CreateEntryFromFile(FrameFilePath, Format(MyFrameCount, "00000000") & _FileInfo.Extension, CompressionLevel.Optimal)

    'MyFrameCount = MyFrameCount + 1
  End Sub



  Public Sub AddFrame(ByVal FrameBitmap As Bitmap, ByVal FileTitle As String, Optional ByVal JpgCompression As Long = JpgCOMPRESSION)
    Dim _NewZipEntry As ZipArchiveEntry
    Dim _ZipEntryStream As Stream


    If MyFrameCount = 0 Then MyFrameSize = New Size(FrameBitmap.Width, FrameBitmap.Height)

    _NewZipEntry = MyZipArchive.CreateEntry(FileTitle & ".jpg", CompressionLevel.Optimal)
    _ZipEntryStream = _NewZipEntry.Open
    Call SaveJpgImage(FrameBitmap, _ZipEntryStream, JpgCompression)
    _ZipEntryStream.Close()
    MyFrameCount = MyFrameCount + 1
  End Sub


End Class


