﻿Public Class clsGate
  Inherits clsGeomLine

  Private MyName As String

  Sub New(ByVal Name As String,
          ByVal X1 As Double,
          ByVal Y1 As Double,
          ByVal X2 As Double,
          ByVal Y2 As Double)

    MyBase.New(X1, Y1, X2, Y2)
    MyName = Name
  End Sub

  Public ReadOnly Property Name As String
    Get
      Return MyName
    End Get
  End Property

End Class
