﻿
Public Class clsZipReader
  Private MyZipFilePath As String
  Private MyZipArchieve As ZipArchive
  'Private MyType As ZipType
  Private MyFrameCount As Integer
  Private MyFrameSize As Size
  Private MyFPS As Double
  Private MyTimeStamps As List(Of Double)



  'Private Enum ZipType As Integer
  '  Frames = 0
  '  TimeStamps = 1
  'End Enum



  Public Sub New(ZipFilePath As String)

    If File.Exists(ZipFilePath) Then
      MyZipFilePath = ZipFilePath
      MyZipArchieve = ZipFile.OpenRead(ZipFilePath)

      '   If Not MyZipArchieve.GetEntry("00000000.jpg") Is Nothing Then
      '  MyType = ZipType.Frames
      'Else
      ' MyType = ZipType.TimeStamps
      'End If

      Call Me.ReadInfo()
    Else
      MsgBox("Cannot find the file '" & ZipFilePath & "'!")
    End If

  End Sub



  Public Sub Close()
    MyFrameSize = Nothing
    MyTimeStamps = Nothing
    MyZipArchieve.Dispose()
  End Sub



  Public ReadOnly Property FPS As Double
    Get
      Return MyFPS
    End Get
  End Property



  Public ReadOnly Property FrameCount As Integer
    Get
      FrameCount = MyFrameCount
    End Get
  End Property



  Public ReadOnly Property FrameSize As Size
    Get
      Return MyFrameSize
    End Get
  End Property



  Public Function GetFrame(FrameIndex As Integer) As Bitmap
    Dim _OriginalBitmap, _NewBitmap As Bitmap
    Dim _BitmapStream As Stream
    Dim _g As Graphics
    Dim _ZipEntry As ZipArchiveEntry
    Dim _FrameName As String


    _FrameName = FrameFileName(FrameIndex)
    _ZipEntry = MyZipArchieve.GetEntry(_FrameName)
    If IsNothing(_ZipEntry) Then
      _FrameName = FrameFileName(FrameIndex, ".jpeg")
      _ZipEntry = MyZipArchieve.GetEntry(_FrameName)
    End If
    If IsNothing(_ZipEntry) Then
      _FrameName = FrameFileName(FrameIndex, ".Jpeg")
      _ZipEntry = MyZipArchieve.GetEntry(_FrameName)
    End If
    If IsNothing(_ZipEntry) Then Return EmptyBitmap(New Size(640, 480))



    _BitmapStream = _ZipEntry.Open
    _OriginalBitmap = New Bitmap(_BitmapStream)
    _BitmapStream.Close()
    _BitmapStream.Dispose()

    _NewBitmap = New Bitmap(_OriginalBitmap.Width, _OriginalBitmap.Height)
    _g = Graphics.FromImage(_NewBitmap)
    _g.DrawImage(_OriginalBitmap, 0, 0)
    _OriginalBitmap.Dispose()
    _g.Dispose()


    Return _NewBitmap
  End Function



  Public Function GetTimeStamp(ByVal FrameIndex As Integer) As Double
    Return MyTimeStamps(FrameIndex)
  End Function



  Private Sub ReadInfo()
    Dim _TempDate As Date
    Dim _FileName As String
    Dim _Year, _Month, _Day, _Hour, _Minute, _Second, _Millisecond As Integer
    Dim _TempBitmap As Bitmap


    MyTimeStamps = New List(Of Double)
    For _i = 0 To MyZipArchieve.Entries.Count - 1
      _FileName = MyZipArchieve.Entries(_i).Name
      _Year = Integer.Parse(_FileName.Substring(0, 4))
      _Month = Integer.Parse(_FileName.Substring(4, 2))
      _Day = Integer.Parse(_FileName.Substring(6, 2))
      _Hour = Integer.Parse(_FileName.Substring(9, 2))
      _Minute = Integer.Parse(_FileName.Substring(11, 2))
      _Second = Integer.Parse(_FileName.Substring(13, 2))
      _Millisecond = Integer.Parse(_FileName.Substring(16, 3))

      _TempDate = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second, _Millisecond)
      MyTimeStamps.Add(_TempDate.ToOADate * 24 * 60 * 60)
    Next
    MyTimeStamps.Sort(Function(x1 As Double, x2 As Double) x1.CompareTo(x2))

    _TempBitmap = Me.GetFrame(0)
    MyFrameCount = MyZipArchieve.Entries.Count
    MyFPS = (MyFrameCount - 1) / (MyTimeStamps(MyFrameCount - 1) - MyTimeStamps(0))
    MyFrameSize = New Size(_TempBitmap.Width, _TempBitmap.Height)
    _TempBitmap.Dispose()
  End Sub



  Private Function FrameFileName(ByVal FrameIndex As Integer, Optional ByVal Extension As String = ".jpg") As String
    Return Format(Date.FromOADate(MyTimeStamps(FrameIndex) / 24 / 60 / 60), "yyyyMMdd-HHmmss.fff") & Extension
  End Function



End Class
