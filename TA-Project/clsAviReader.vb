﻿

Public Class clsAviReader
  Private MyAviFilePath As String
  Private MyCapture As VideoCapture
  Private MyFrameCount As Integer
  Private MyFrameSize As Size
  Private MyFPS As Double
  Private MyTimeStamps As List(Of Double)



  Public Sub New(ByVal AviFilePath As String)
    If File.Exists(AviFilePath) Then
      MyAviFilePath = AviFilePath
      MyCapture = New VideoCapture(AviFilePath)
      Call Me.ReadInfo()
    Else
      MsgBox("Cannot find the file '" & AviFilePath & "'!")
    End If
  End Sub


  Public Sub Close()
    MyFrameSize = Nothing
    MyTimeStamps = Nothing
    MyCapture.Dispose()
  End Sub


  Public ReadOnly Property FPS As Double
    Get
      Return MyFPS
    End Get
  End Property


  Public ReadOnly Property FrameCount As Integer
    Get
      FrameCount = MyFrameCount
    End Get
  End Property


  Public ReadOnly Property FrameSize As Size
    Get
      Return MyFrameSize
    End Get
  End Property


  Public Function GetFrame(FrameIndex As Integer) As Bitmap
    Dim _OriginalBitmap, _NewBitmap As Bitmap
    Dim _g As Graphics


    MyCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, FrameIndex)
    _OriginalBitmap = MyCapture.QueryFrame.Bitmap()

    _NewBitmap = New Bitmap(_OriginalBitmap.Width, _OriginalBitmap.Height)
    _g = Graphics.FromImage(_NewBitmap)
    _g.DrawImage(_OriginalBitmap, 0, 0)
    _OriginalBitmap.Dispose()
    _g.Dispose()

    Return _NewBitmap
  End Function


  Public Function GetTimeStamp(ByVal FrameIndex As Integer) As Double
    Return MyTimeStamps(FrameIndex)
  End Function








  Private Sub ReadInfo()
    MyFPS = MyCapture.GetCaptureProperty(CvEnum.CapProp.Fps)
    MyFrameSize = New Size(CInt(MyCapture.GetCaptureProperty(CvEnum.CapProp.FrameWidth)), CInt(MyCapture.GetCaptureProperty(CvEnum.CapProp.FrameHeight)))
    MyFrameCount = CInt(MyCapture.GetCaptureProperty(CvEnum.CapProp.FrameCount))
    Call LoadTimeStamps() '(MyAviFilePath)
  End Sub


  Private Function LoadTimeStamps() As Boolean
    Dim _FileInfo As FileInfo
    Dim _LogFilePath As String
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _CurrentTime As Date
    Dim _Year As Integer
    Dim _Month As Integer
    Dim _Day As Integer
    Dim _Hour As Integer
    Dim _Minute As Integer
    Dim _Second As Integer
    Dim _MilliSecond As Integer
    Dim _i As Integer


    _FileInfo = New FileInfo(MyAviFilePath)
    _LogFilePath = _FileInfo.FullName.Substring(0, _FileInfo.FullName.Length - _FileInfo.Extension.Length) & ".log"


    MyTimeStamps = New List(Of Double)

    If File.Exists(_LogFilePath) Then
      Try
        _FileIndex = FreeFile()
        FileOpen(_FileIndex, _LogFilePath, OpenMode.Input)

        _i = -1
        Do Until EOF(_FileIndex)
          _Line = LineInput(_FileIndex)
          _Year = CInt(_Line.Substring(6, 4))
          _Month = CInt(_Line.Substring(11, 2))
          _Day = CInt(_Line.Substring(14, 2))
          _Hour = CInt(_Line.Substring(17, 2))
          _Minute = CInt(_Line.Substring(20, 2))
          _Second = CInt(_Line.Substring(23, 2))
          _MilliSecond = CInt(_Line.Substring(26, 3))

          _CurrentTime = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second, _MilliSecond)
          MyTimeStamps.Add(_CurrentTime.ToOADate * 24 * 60 * 60)

          _i = _i + 1
        Loop
        FileClose(_FileIndex)
        Return True
      Catch
        Return False
      End Try
    Else
      If Not TryConvertFileNameToDate(_FileInfo.Name, _CurrentTime) Then _CurrentTime = New Date(1970, 1, 1, 0, 0, 0, 0)

      For _i = 0 To MyFrameCount - 1
        MyTimeStamps.Add(_CurrentTime.AddSeconds(_i / MyFPS).ToOADate * 24 * 60 * 60)
      Next

      Return True
    End If
  End Function

End Class
