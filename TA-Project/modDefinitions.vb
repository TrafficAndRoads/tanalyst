﻿Public Module modDefinitions

  Public MessageFont As New Font("Times New Roman", 10, FontStyle.Regular)


  Public Enum VideoFileStatuses As Integer
    Unchanged = 0
    Updated = 1
    Created = 2
    Deleted = 3
  End Enum

  Public Enum RecordTypes As Integer
    VideoRecording = 1
    Detection = 2
  End Enum

  Public Enum VideoFileTypes As Integer
    Unknown = -1
    Avi = 0
    Zip = 1
  End Enum


  Public Enum DisplayIndices As Integer
    Combined = -2
    Projected = -1
  End Enum

  Public Enum TrajectoryTypes
    Original = 1
    Smoothed = 2
  End Enum

  Public Enum UserControlTypes As Integer
    NotDefined = 0
    Textbox = 1
    Numberbox = 2
    Checkbox = 3
    Combobox = 4
    Placeholder = 5
    ClickCounter = 6
  End Enum


  Public Enum ProjectLayoutModes As Integer
    VideoRecordings = 1
    Detections = 2
  End Enum

End Module
