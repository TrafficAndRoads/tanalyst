﻿Public Class usrInteractionAnalyser
  Private WithEvents MyChart As Chart
  Private WithEvents MyDataGrid As DataGridView

  Private MyRoadUser1, MyRoaduser2 As clsRoadUser
  Private MyCurrentFrame As clsRoadUser

  Private MyResults As List(Of ResultLine)
  Private MyFrameStart, MyFrameEnd As Integer
  Private MyIsLoaded As Boolean = False


  Private Structure ResultLine
    Dim Frame As Integer
    Dim Time As Double
    Dim V1 As Double
    Dim V2 As Double
    Dim Vrel As Double
    Dim TTC As Double
    Dim T1 As Double
    Dim T2 As Double

  End Structure


  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    MyDataGrid = New DataGridView
    With MyDataGrid
      .RowHeadersVisible = True
      .RowHeadersWidth = 60
      .RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing
      '.Columns.Add("cmFrame", "Frame")
      '.Columns("cmFrame").Width = 40
      '.Columns("cmFrame").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmTTC", "TTC")
      .Columns("cmTTC").Width = 40
      .Columns("cmTTC").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmTA", "TAdv")
      .Columns("cmTA").Width = 40
      .Columns("cmTA").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmT2", "T2")
      .Columns("cmT2").Width = 40
      .Columns("cmT2").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmVrel", "Vrel")
      .Columns("cmVrel").Width = 40
      .Columns("cmVrel").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmV1", "V1")
      .Columns("cmV1").Width = 40
      .Columns("cmV1").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmV2", "V2")
      .Columns("cmV2").Width = 40
      .Columns("cmV2").SortMode = DataGridViewColumnSortMode.NotSortable
      .Columns.Add("cmTime", "Time")
      .Columns("cmTime").Width = 40
      .Columns("cmTime").SortMode = DataGridViewColumnSortMode.NotSortable
      MyDataGrid.Top = 3
      MyDataGrid.Left = 3
      MyDataGrid.Width = 300

    End With

    MyChart = New Chart
    MyChart.Top = 3
    MyChart.Left = MyDataGrid.Right + 6


    MyChart.ChartAreas.Add(New ChartArea("V"))
    MyChart.ChartAreas("V").BorderColor = Color.Gray
    MyChart.ChartAreas("V").CursorX.IsUserSelectionEnabled = True
    MyChart.ChartAreas("V").CursorY.IsUserSelectionEnabled = True

    MyChart.ChartAreas("V").AxisX.LineColor = Color.Black
    MyChart.ChartAreas("V").AxisX.LineWidth = 1
    MyChart.ChartAreas("V").AxisX.Title = "Frame"
    MyChart.ChartAreas("V").AxisX.Interval = 10
    MyChart.ChartAreas("V").AxisX.MajorGrid.Interval = 10
    MyChart.ChartAreas("V").AxisX.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("V").AxisX.MinorGrid.Enabled = True
    MyChart.ChartAreas("V").AxisX.MinorGrid.Interval = 2
    MyChart.ChartAreas("V").AxisX.MinorGrid.LineColor = Color.LightGray

    MyChart.ChartAreas("V").AxisY.Crossing = 0
    MyChart.ChartAreas("V").AxisY.LineColor = Color.Black
    MyChart.ChartAreas("V").AxisY.LineWidth = 1
    MyChart.ChartAreas("V").AxisY.Title = "V, m/s"
    MyChart.ChartAreas("V").AxisY.Interval = 4
    MyChart.ChartAreas("V").AxisY.MajorGrid.Interval = 4
    MyChart.ChartAreas("V").AxisY.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("V").AxisY.MinorGrid.Enabled = True
    MyChart.ChartAreas("V").AxisY.MinorGrid.Interval = 1
    MyChart.ChartAreas("V").AxisY.MinorGrid.LineColor = Color.LightGray


    MyChart.ChartAreas.Add(New ChartArea("TTC"))
    MyChart.ChartAreas("TTC").BorderColor = Color.Gray
    MyChart.ChartAreas("TTC").CursorX.IsUserSelectionEnabled = True
    MyChart.ChartAreas("TTC").CursorY.IsUserSelectionEnabled = True

    MyChart.ChartAreas("TTC").AxisX.Title = "Frame"
    MyChart.ChartAreas("TTC").AxisX.LineColor = Color.Black
    MyChart.ChartAreas("TTC").AxisX.LineWidth = 1
    MyChart.ChartAreas("TTC").AxisX.Interval = 10
    MyChart.ChartAreas("TTC").AxisX.MajorGrid.Interval = 10
    MyChart.ChartAreas("TTC").AxisX.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("TTC").AxisX.MinorGrid.Enabled = True
    MyChart.ChartAreas("TTC").AxisX.MinorGrid.Interval = 2
    MyChart.ChartAreas("TTC").AxisX.MinorGrid.LineColor = Color.LightGray

    MyChart.ChartAreas("TTC").AxisY.Title = "TTC/T2, s."
    'MyChart.ChartAreas("TTC").AxisY.IsReversed = True
    MyChart.ChartAreas("TTC").AxisY.LineColor = Color.Black
    MyChart.ChartAreas("TTC").AxisY.LineWidth = 1
    MyChart.ChartAreas("TTC").AxisY.Interval = 1
    MyChart.ChartAreas("TTC").AxisY.MajorGrid.Interval = 1
    MyChart.ChartAreas("TTC").AxisY.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("TTC").AxisY.MinorGrid.Enabled = True
    MyChart.ChartAreas("TTC").AxisY.MinorGrid.Interval = 0.5
    MyChart.ChartAreas("TTC").AxisY.MinorGrid.LineColor = Color.LightGray


    MyChart.ChartAreas.Add(New ChartArea("TAdv"))
    MyChart.ChartAreas("TAdv").BorderColor = Color.Gray
    MyChart.ChartAreas("TAdv").CursorX.IsUserSelectionEnabled = True
    MyChart.ChartAreas("TAdv").CursorY.IsUserSelectionEnabled = True

    MyChart.ChartAreas("TAdv").AxisX.LineColor = Color.Black
    MyChart.ChartAreas("TAdv").AxisX.LineWidth = 1
    MyChart.ChartAreas("TAdv").AxisX.Title = "Frame"
    MyChart.ChartAreas("TAdv").AxisX.Interval = 10
    MyChart.ChartAreas("TAdv").AxisX.MajorGrid.Interval = 10
    MyChart.ChartAreas("TAdv").AxisX.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("TAdv").AxisX.MinorGrid.Enabled = True
    MyChart.ChartAreas("TAdv").AxisX.MinorGrid.Interval = 2
    MyChart.ChartAreas("TAdv").AxisX.MinorGrid.LineColor = Color.LightGray


    MyChart.ChartAreas("TAdv").AxisY.Crossing = 0
    MyChart.ChartAreas("TAdv").AxisY.LineColor = Color.Black
    MyChart.ChartAreas("TAdv").AxisY.LineWidth = 1
    MyChart.ChartAreas("TAdv").AxisY.Title = "TAdv, s"
    MyChart.ChartAreas("TAdv").AxisY.Interval = 1
    MyChart.ChartAreas("TAdv").AxisY.MajorGrid.Interval = 1
    MyChart.ChartAreas("TAdv").AxisY.MajorGrid.LineColor = Color.Gray
    MyChart.ChartAreas("TAdv").AxisY.MinorGrid.Enabled = True
    MyChart.ChartAreas("TAdv").AxisY.MinorGrid.Interval = 0.5
    MyChart.ChartAreas("TAdv").AxisY.MinorGrid.LineColor = Color.LightGray




    MyChart.Series.Add("V1")
    MyChart.Series("V1").ChartArea = "V"
    MyChart.Series("V1").ChartType = SeriesChartType.Point
    MyChart.Series("V1").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("V1").MarkerSize = 3
    MyChart.Series("V1").Color = Color.Black

    MyChart.Series.Add("V2")
    MyChart.Series("V2").ChartArea = "V"
    MyChart.Series("V2").ChartType = SeriesChartType.Point
    MyChart.Series("V2").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("V2").MarkerSize = 3
    MyChart.Series("V2").Color = Color.Black

    MyChart.Series.Add("Vrel")
    MyChart.Series("Vrel").ChartArea = "V"
    MyChart.Series("Vrel").ChartType = SeriesChartType.Point
    MyChart.Series("Vrel").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("Vrel").MarkerSize = 3
    MyChart.Series("Vrel").Color = Color.Black

    MyChart.Series.Add("TTC")
    MyChart.Series("TTC").ChartArea = "TTC"
    MyChart.Series("TTC").ChartType = SeriesChartType.Point
    MyChart.Series("TTC").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("TTC").MarkerSize = 3
    MyChart.Series("TTC").Color = Color.Red

    MyChart.Series.Add("T2")
    MyChart.Series("T2").ChartArea = "TTC"
    MyChart.Series("T2").ChartType = SeriesChartType.Point
    MyChart.Series("T2").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("T2").MarkerSize = 3
    MyChart.Series("T2").Color = Color.Black

    MyChart.Series.Add("TAdv")
    MyChart.Series("TAdv").ChartArea = "TAdv"
    MyChart.Series("TAdv").ChartType = SeriesChartType.Point
    MyChart.Series("TAdv").MarkerStyle = MarkerStyle.Circle
    MyChart.Series("TAdv").MarkerSize = 3
    MyChart.Series("TAdv").Color = Color.Black

    'MyChart.Series.Add("V")
    'MyChart.Series("V").ChartArea = "V"
    'MyChart.Series("V").ChartType = SeriesChartType.Point
    'MyChart.Series("V").MarkerStyle = MarkerStyle.Circle
    'MyChart.Series("V").MarkerSize = 3
    'MyChart.Series("V").Color = Color.DarkGreen

    'MyChart.Series.Add("A")
    'MyChart.Series("A").ChartArea = "A"
    'MyChart.Series("A").ChartType = SeriesChartType.Point
    'MyChart.Series("A").MarkerStyle = MarkerStyle.Circle
    'MyChart.Series("A").MarkerSize = 3
    'MyChart.Series("A").Color = Color.Red



    Me.Controls.Add(MyChart)
    Me.Controls.Add(MyDataGrid)
    MyIsLoaded = True
    Call AdjustSize()

  End Sub


  Private Sub AdjustSize()
  
    MyDataGrid.Height = Me.Height - 6
    
    MyChart.Height = Me.Height - 6
    MyChart.Width = Me.Width - MyDataGrid.Width - 12
  End Sub
  Public Property RoadUser1 As clsRoadUser
    Set(value As clsRoadUser)
      MyRoadUser1 = value
    End Set
    Get
      Return MyRoadUser1
    End Get
  End Property

  Public Property RoadUser2 As clsRoadUser
    Set(value As clsRoadUser)
      MyRoaduser2 = value
    End Set
    Get
      Return MyRoaduser2
    End Get
  End Property



  Public Sub CalculateTTC()
    '  Dim _RoadUser1, _RoadUser2 As clsRoadUser
    Dim _MessagePoint As clsMessagePoint
    Dim _i, _j, _k As Integer
    Dim _TTC As Double
    Dim _TA1, _TA2 As Double
    Dim _TG1, _TG2 As Double
    Dim _X_TTC, _Y_TTC As Double
    Dim _X_TA, _Y_TA As Double
    Dim _X_TG, _Y_TG As Double

    Dim _TTCmin As Double
    Dim _TAmin, _T2min As Double
    Dim _TAlast, _T2last As Double
    Dim _ResultLine As ResultLine
    Dim _GotValue As Boolean
    ' Dim _Vrel As Double
    Dim _Frame1, _Frame2 As Integer
    Dim _DxDy1, _DxDy2 As clsDxDy

    _Frame1 = Math.Max(RoadUser1.FirstFrame, RoadUser2.FirstFrame)
    _Frame2 = Math.Min(RoadUser1.LastFrame, RoadUser2.LastFrame)
    MyResults = New List(Of ResultLine)

    For _Frame = _Frame1 To _Frame2
      _ResultLine.Frame = _Frame
      '  _ResultLine.Time = pa
      _ResultLine.V1 = MyRoadUser1.DataPoint(_Frame).V
      _ResultLine.V2 = MyRoaduser2.DataPoint(_Frame).V
      _ResultLine.TTC = NoValue
      _ResultLine.T1 = NoValue
      _ResultLine.T2 = NoValue
      _ResultLine.Vrel = NoValue

      Call CrossingCourse(True, MyRoadUser1, MyRoaduser2, _Frame, _TTC, _X_TTC, _Y_TTC, _DxDy1, _DxDy2, _TA1, _TA2, _X_TA, _Y_TA, _TG1, _TG2, _X_TG, _Y_TG)
      If IsValue(_TTC) Then
        _ResultLine.TTC = _TTC
        _ResultLine.Vrel = RelativeSpeed(MyRoadUser1.DataPoint(_Frame).V, _DxDy1, MyRoaduser2.DataPoint(_Frame).V, _DxDy2)
        MyResults.Add(_ResultLine)
      Else
        If IsValue(_TA1) And IsValue(_TA2) Then
          _ResultLine.T1 = _TA1
          _ResultLine.T2 = _TA2
          _ResultLine.Vrel = RelativeSpeed(MyRoadUser1.DataPoint(_Frame).V, _DxDy1, MyRoaduser2.DataPoint(_Frame).V, _DxDy2)
          MyResults.Add(_ResultLine)
        End If
        If IsValue(_TG1) And IsValue(_TG2) Then
        End If
      End If
    Next


    MyFrameStart = _Frame1
    MyFrameEnd = _Frame2

    ' For _Fra


    '_TTCmin = NoValue
    '_TAmin = NoValue
    '_T2min = NoValue
    '_TAlast = NoValue
    '_T2last = NoValue

    'If IsValue(_TTCmin) Then
    '  If _TTC < _TTCmin Then
    '    _TTCmin = _TTC
    '  End If
    'Else
    '  _TTCmin = _TTC
    'End If

    'If IsValue(_TAmin) Then
    '  If Math.Abs(_TA1 - _TA2) < _TAmin Then
    '    _TAmin = Math.Abs(_TA1 - _TA2)
    '    _T2min = Math.Max(_TA1, _TA2)
    '  End If
    'Else
    '  _TAmin = Math.Abs(_TA1 - _TA2)
    '  _T2min = Math.Max(_TA1, _TA2)
    'End If


  End Sub

  Private Sub ArrangeControls()


  End Sub


  Public Sub DisplayResults()
    Dim _Row As DataGridViewRow
    Dim _RowIndex As Integer


    MyDataGrid.Rows.Clear()
    If MyResults IsNot Nothing Then
      For Each _ResultLine In MyResults
        _RowIndex = MyDataGrid.Rows.Add()
        With MyDataGrid.Rows(_RowIndex)
          '.Cells("cmFrame").Value = _ResultLine.Frame
          .HeaderCell.Value = _ResultLine.Frame.ToString
          .Cells("cmV1").Value = Format(_ResultLine.V1, "0.0")
          .Cells("cmV2").Value = Format(_ResultLine.V2, "0.0")
          If IsValue(_ResultLine.Vrel) Then .Cells("cmVrel").Value = Format(_ResultLine.Vrel, "0.0")
          If IsValue(_ResultLine.TTC) Then
            .Cells("cmTTC").Value = Format(_ResultLine.TTC, "0.00")
            .Cells("cmTTC").Style.ForeColor = Color.Red
          End If
       
          If IsValue(_ResultLine.T1) And IsValue(_ResultLine.T2) Then .Cells("cmTA").Value = Format(Math.Abs(_ResultLine.T1 - _ResultLine.T2), "0.00")
          If IsValue(_ResultLine.T1) And IsValue(_ResultLine.T2) Then .Cells("cmT2").Value = Format(Math.Max(_ResultLine.T1, _ResultLine.T2), "0.00")
        End With
      Next


      '  MyDataGrid.AutoResizeColumns()
      Call LoadChartData()
    End If
  End Sub

  Private Sub usrInteractionAnalyser_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
    If MyIsLoaded Then Call AdjustSize()
  End Sub








  Private Sub LoadChartData()
    MyChart.Series("V1").Points.Clear()
    MyChart.Series("V2").Points.Clear()
    MyChart.Series("Vrel").Points.Clear()
    MyChart.Series("TTC").Points.Clear()
    MyChart.Series("TAdv").Points.Clear()
    MyChart.Series("T2").Points.Clear()
    

    For Each _ResultLine In MyResults
      MyChart.Series("V1").Points.AddXY(_ResultLine.Frame, _ResultLine.V1)
      MyChart.Series("V2").Points.AddXY(_ResultLine.Frame, _ResultLine.V2)
      MyChart.Series("Vrel").Points.AddXY(_ResultLine.Frame, _ResultLine.Vrel)
      MyChart.Series("TTC").Points.AddXY(_ResultLine.Frame, _ResultLine.TTC)
      MyChart.Series("TAdv").Points.AddXY(_ResultLine.Frame, Math.Abs(_ResultLine.T1 - _ResultLine.T2))
      MyChart.Series("T2").Points.AddXY(_ResultLine.Frame, Math.Max(_ResultLine.T1, _ResultLine.T2))
    Next

  

    MyChart.ChartAreas("V").AxisX.Minimum = Int(MyResults(0).Frame / 10) * 10
    MyChart.ChartAreas("V").AxisX.Maximum = Int(MyResults(MyResults.Count - 1).Frame / 10 + 1) * 10
    MyChart.ChartAreas("V").AxisY.Minimum = 0 'Math.Min(0, Int(MyRoadUser.MinV / 2) * 2)
    MyChart.ChartAreas("V").AxisY.Maximum = 24 'Int(.MaxV / 4 + 1) * 4
    MyChart.ChartAreas("V").Position = New ElementPosition(0, 0, 50, 50)



    MyChart.ChartAreas("TTC").AxisX.Minimum = Int(MyResults(0).Frame / 10) * 10
    MyChart.ChartAreas("TTC").AxisX.Maximum = Int(MyResults(MyResults.Count - 1).Frame / 10 + 1) * 10
    MyChart.ChartAreas("TTC").AxisY.Minimum = 0 'Math.Min(0, Int(MyRoadUser.MinV / 2) * 2)
    MyChart.ChartAreas("TTC").AxisY.Maximum = 8 'Int(.MaxV / 4 + 1) * 4
    MyChart.ChartAreas("TTC").Position = New ElementPosition(50, 0, 50, 50)

    MyChart.ChartAreas("TAdv").AxisX.Minimum = Int(MyResults(0).Frame / 10) * 10
    MyChart.ChartAreas("TAdv").AxisX.Maximum = Int(MyResults(MyResults.Count - 1).Frame / 10 + 1) * 10
    MyChart.ChartAreas("TAdv").AxisY.Minimum = 0 'Math.Min(0, Int(MyRoadUser.MinV / 2) * 2)
    MyChart.ChartAreas("TAdv").AxisY.Maximum = 8 'Int(.MaxV / 4 + 1) * 4
    MyChart.ChartAreas("TAdv").Position = New ElementPosition(50, 50, 50, 50)

    '      MyChart.ChartAreas("A").AxisX.Minimum = Int(.FirstFrame / 10) * 10
    '      MyChart.ChartAreas("A").AxisX.Maximum = Int(.LastFrame / 10 + 1) * 10
    '      MyChart.ChartAreas("A").AxisY.Minimum = -4 'Int(.Math.Min(0, Int(.mina) + 1))
    '      MyChart.ChartAreas("A").AxisY.Maximum = 8 'Int(.Max / 4 + 1) * 4
    '      MyChart.ChartAreas("A").Position = New ElementPosition(52, 50, 48, 50)


    '    End With
    '  End If


  End Sub




  'Private Sub MyChart_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyChart.MouseDoubleClick
  '  Dim _a As HitTestResult
  '  Dim _HitChartArea As ChartArea

  '  _a = MyChart.HitTest(e.X, e.Y)
  '  _HitChartArea = _a.ChartArea

  '  If _HitChartArea IsNot Nothing Then
  '    _HitChartArea.AxisX.ScaleView.ZoomReset()
  '    _HitChartArea.AxisY.ScaleView.ZoomReset()
  '  End If
  'End Sub
End Class
