﻿Public Class clsVideoReader
  Private MyType As VideoFileTypes
  Private MyZipReader As clsZipReader
  Private MyAviReader As clsAviReader




  Public Sub New(ByVal VideoFilePath As String)
        If VideoFilePath.EndsWith(".avi") Or
      VideoFilePath.EndsWith(".mp4") Or
      VideoFilePath.EndsWith(".mpg") Or
      VideoFilePath.EndsWith(".mov") Or
      VideoFilePath.EndsWith(".mkv") _
    Then
            MyType = VideoFileTypes.Avi
        ElseIf VideoFilePath.EndsWith(".zip") Then
            MyType = VideoFileTypes.Zip
        Else
            MyType = VideoFileTypes.Unknown
    End If


    Select Case MyType
      Case VideoFileTypes.Avi
        MyAviReader = New clsAviReader(VideoFilePath)
      Case VideoFileTypes.Zip
        MyZipReader = New clsZipReader(VideoFilePath)
    End Select
  End Sub



  Public Sub Close()
    Select Case MyType
      Case VideoFileTypes.Avi
        MyAviReader.Close()

      Case VideoFileTypes.Zip
        MyZipReader.Close()
    End Select
  End Sub



  Public ReadOnly Property FrameCount As Integer
    Get
      Select Case MyType
        Case VideoFileTypes.Avi
          Return MyAviReader.FrameCount
        Case VideoFileTypes.Zip
          Return MyZipReader.FrameCount
        Case Else
          Return NoValue
      End Select
    End Get
  End Property



  Public ReadOnly Property FrameSize As Size
    Get
      Select Case MyType
        Case VideoFileTypes.Avi
          Return MyAviReader.FrameSize
        Case VideoFileTypes.Zip
          Return MyZipReader.FrameSize
        Case Else
          Return Nothing
      End Select
    End Get
  End Property



  Public ReadOnly Property FPS As Double
    Get
      Select Case MyType
        Case VideoFileTypes.Avi
          Return MyAviReader.FPS
        Case VideoFileTypes.Zip
          Return MyZipReader.FPS
        Case Else
          Return NoValue
      End Select
    End Get
  End Property



  Public Function Type() As VideoFileTypes
    Return MyType
  End Function



  Public Function GetFrame(Frame As Integer) As Bitmap
    Try
      Select Case MyType
        Case VideoFileTypes.Avi
          Return MyAviReader.GetFrame(Frame)
        Case VideoFileTypes.Zip
          Return MyZipReader.GetFrame(Frame)
        Case Else
          Return EmptyBitmap(New Size(640, 480))
      End Select
    Catch
      Return EmptyBitmap(New Size(640, 480))
    End Try
  End Function



  Public Function GetTimeStamp(ByVal Frame As Integer) As Double
    Select Case MyType
      Case VideoFileTypes.Avi
        Return MyAviReader.GetTimeStamp(Frame)
      Case VideoFileTypes.Zip
        Return MyZipReader.GetTimeStamp(Frame)
      Case Else
        Return NoValue
    End Select
  End Function


End Class
