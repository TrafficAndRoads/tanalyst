﻿Public Class clsRecord
  Public RecordType As RecordTypes
  Public ID As Integer
  Public DateTime As Date
  Public Status As Integer
  Public Type As Integer
  Public Comment As String
  Public RoadUsersChanged As Boolean
  Public Map As clsMap
  Public Media As String
  Public VideoFiles As List(Of clsVideoFile)
  Public Directions As List(Of clsDirection)
  Public UserControlValues As List(Of Object)
  Public Gates As List(Of clsGate)

  Public Event AppearanceChanged()

  Private MyParentProject As clsProject
  Private MyStartTime As Double
  Private MyFrameRate As Double
  Private MyFrameCount As Integer

  Private MyRelatedRecords As List(Of Integer)
  Private MyTimeLine As List(Of clsTimeLinePoint)
  Private MyRoadUsers As List(Of clsRoadUser)
  Private MyRoadUsersLoaded As Boolean
  Private MyVideoFilesLoaded As Boolean

  Private WithEvents MySelectedRoadUser As clsRoadUser
  Private MyKeyRoadUser1 As clsRoadUser
  Private MyKeyRoadUser2 As clsRoadUser
  Private MySelectedRoadUserIndex As Integer
  Private MyKeyRoadUser1Index As Integer
  Private MyKeyRoadUser2Index As Integer






#Region "Properties"


  Public ReadOnly Property TrajectoryDirectory As String
    Get
      Select Case Me.RecordType
        Case RecordTypes.VideoRecording
          Return MyParentProject.TrajectoryDirectory & "VideoRecordings\"
        Case RecordTypes.Detection
          Return MyParentProject.TrajectoryDirectory & "Detections\"
        Case Else
          Return ""
      End Select
    End Get
  End Property

  Public ReadOnly Property MediaDirectory As String
    Get
      Select Case Me.RecordType
        Case RecordTypes.VideoRecording
          Return MyParentProject.MediaDirectory & "VideoRecordings\"
        Case RecordTypes.Detection
          Return MyParentProject.MediaDirectory & "Detections\"
        Case Else
          Return ""
      End Select
    End Get
  End Property

  Public ReadOnly Property MapDirectory As String
    Get
      Return MyParentProject.MapDirectory
    End Get
  End Property

  Public ReadOnly Property VideoDirectory As String
    Get
      Return MyParentProject.VideoDirectory
    End Get
  End Property



  Public ReadOnly Property ExportDirectory As String
    Get
      Return MyParentProject.ExportDirectory
    End Get
  End Property



  Public ReadOnly Property ImportDirectory As String
    Get
      Return MyParentProject.ImportDirectory
    End Get
  End Property



  Public ReadOnly Property RoadUserTypes As List(Of clsRoadUserType)
    Get
      Return MyParentProject.RoadUserTypes
    End Get
  End Property


  Public ReadOnly Property MediaPath As String
    Get
      If File.Exists(Me.MediaDirectory & Me.Media) Then
        Return Me.MediaDirectory & Me.Media
      Else
        Return ""
      End If
    End Get
  End Property
  Public ReadOnly Property StartTime As Double
    Get
      Return MyStartTime
    End Get
  End Property



  Public ReadOnly Property FrameRate As Double
    Get
      Return MyFrameRate
    End Get
  End Property



  Public ReadOnly Property FrameCount As Integer
    Get
      Return MyFrameCount
    End Get
  End Property



  Public ReadOnly Property RelatedRecords As List(Of Integer)
    Get
      Return MyRelatedRecords
    End Get
  End Property



  Public ReadOnly Property RoadUsers As List(Of clsRoadUser)
    Get
      Return MyRoadUsers
    End Get
  End Property



  Public ReadOnly Property TimeLine As List(Of clsTimeLinePoint)
    Get
      Return MyTimeLine
    End Get
  End Property


  Public Function GetLocalTime(Frame As Integer) As Double
    If IsMore(Me.FrameRate, 0) Then
      Return Frame / Me.FrameRate
    Else
      Return NoValue
    End If
  End Function



  Public Function GetAbsoluteTime(Frame As Integer) As Double
    If IsMore(Me.FrameRate, 0) Then
      Return MyStartTime + Me.GetLocalTime(Frame)
    Else
      Return NoValue
    End If
  End Function


  Public Function ClosestFrameFromAbsoluteTime(ByVal AbsTime As Double) As Integer
    Dim _Frame As Integer


    _Frame = CInt((AbsTime - MyStartTime) * Me.FrameRate)

    If _Frame < 0 Then
      Return 0
    ElseIf _Frame > (MyFrameCount - 1) Then
      Return MyFrameCount - 1
    Else
      Return _Frame
    End If

  End Function


  Public Property SelectedRoadUser As clsRoadUser
    Get
      Return MySelectedRoadUser
    End Get
    Set(ByVal value As clsRoadUser)
      If MyRoadUsers.Contains(value) Then
        MySelectedRoadUser = value
        MySelectedRoadUserIndex = FindRoadUserIndex(MySelectedRoadUser)
      Else
        MySelectedRoadUser = Nothing
        MySelectedRoadUserIndex = -1
      End If
      RaiseEvent AppearanceChanged() 'SelectedRoadUser_Changed()
    End Set
  End Property



  Public Property SelectedRoadUserIndex As Integer
    Get
      Return MySelectedRoadUserIndex
    End Get
    Set(ByVal value As Integer)
      If IsBetween(value, 0, MyRoadUsers.Count - 1) Then
        MySelectedRoadUserIndex = value
        MySelectedRoadUser = MyRoadUsers(MySelectedRoadUserIndex)
      Else
        MySelectedRoadUserIndex = -1
        MySelectedRoadUser = Nothing
      End If
      RaiseEvent AppearanceChanged() ' SelectedRoadUser_Changed()
    End Set
  End Property



  Public Property KeyRoadUser1 As clsRoadUser
    Get
      Return MyKeyRoadUser1
    End Get
    Set(ByVal value As clsRoadUser)
      For Each _RoadUser In MyRoadUsers
        If _RoadUser.Key = clsRoadUser.KeyRoadUser.Key1 Then _RoadUser.Key = clsRoadUser.KeyRoadUser.NotKey
      Next
      If MyRoadUsers.Contains(value) Then
        MyKeyRoadUser1 = value
        MyKeyRoadUser1.Key = clsRoadUser.KeyRoadUser.Key1
        MyKeyRoadUser1Index = FindRoadUserIndex(MyKeyRoadUser1)
      Else
        MyKeyRoadUser1 = Nothing
        MyKeyRoadUser1Index = -1
      End If
      RaiseEvent AppearanceChanged()  'KeyRoadUser1_Changed()
    End Set
  End Property



  Public Property KeyRoadUser1Index As Integer
    Get
      Return MyKeyRoadUser1Index
    End Get
    Set(ByVal value As Integer)
      If IsBetween(value, 0, MyRoadUsers.Count - 1) Then
        Me.KeyRoadUser1 = MyRoadUsers(KeyRoadUser1Index)
      End If
    End Set
  End Property



  Public Property KeyRoadUser2 As clsRoadUser
    Get
      Return MyKeyRoadUser2
    End Get
    Set(ByVal value As clsRoadUser)
      For Each _RoadUser In MyRoadUsers
        If _RoadUser.Key = clsRoadUser.KeyRoadUser.Key2 Then _RoadUser.Key = clsRoadUser.KeyRoadUser.NotKey
      Next
      If MyRoadUsers.Contains(value) Then
        MyKeyRoadUser2 = value
        MyKeyRoadUser2.Key = clsRoadUser.KeyRoadUser.Key2
        MyKeyRoadUser2Index = FindRoadUserIndex(MyKeyRoadUser2)
      Else
        MyKeyRoadUser2 = Nothing
        MyKeyRoadUser2Index = -1
      End If
      RaiseEvent AppearanceChanged() ' KeyRoadUser2_Changed()
    End Set
  End Property



  Public Property KeyRoadUser2Index As Integer
    Get
      Return MyKeyRoadUser2Index
    End Get
    Set(ByVal value As Integer)
      If IsBetween(value, 0, MyRoadUsers.Count - 1) Then
        Me.KeyRoadUser2 = MyRoadUsers(KeyRoadUser2Index)
      End If
    End Set
  End Property


  'Public Property Media As Bitmap
  '  Get
  '    Dim _FilePath As String
  '    Dim _Bitmap As Bitmap

  '    _FilePath = MediaDirectory & Format(Me.ID, "00000000") & ".jpg"
  '    If File.Exists(_FilePath) Then
  '      Try
  '        _Bitmap = New Bitmap(_FilePath)
  '        Return _Bitmap
  '      Catch ex As Exception
  '        Return Nothing
  '      End Try
  '    Else
  '      Return Nothing
  '    End If

  '  End Get
  '  Set(value As Bitmap)

  '  End Set
  'End Property

#End Region



#Region "Public methods"



  Public Sub New(ByVal ParentProject As clsProject,
                 ByVal RecordType As RecordTypes,
                 ByVal ID As Integer,
                 ByVal DateTime As Date,
                 ByVal Status As Integer,
                 ByVal Type As Integer,
                 ByVal Comment As String,
                 ByVal MapAvailable As Boolean,
                 ByVal Map As String,
                 ByVal MapWidth As Integer,
                 ByVal MapHeight As Integer,
                 ByVal MapScale As Double,
                 ByVal MapX0 As Double,
                 ByVal MapY0 As Double,
                 ByVal MapDx As Double,
                 ByVal MapDy As Double,
                 ByVal MapVisible As Boolean,
                 ByVal MapComment As String,
                 ByVal Media As String,
                 ByVal StartTime As Double,
                 ByVal FrameCount As Integer,
                 ByVal FrameRate As Double,
                 ByVal UserControlValues As List(Of Object),
                 ByVal RelatedRecords As List(Of Integer))

    MyParentProject = ParentProject
    Me.RecordType = RecordType
    Me.ID = ID
    Me.DateTime = DateTime
    Me.Status = Status
    Me.Type = Type
    Me.Comment = Comment

    Me.Map = New clsMap(Me)
    If MapAvailable Then
      With Me.Map
        .Map = Map
        .Available = True
        .Size = New Size(MapWidth, MapHeight)
        .Scale = MapScale
        .X = MapX0
        .Y = MapY0
        .Dx = MapDx
        .Dy = MapDy
        .Visible = MapVisible
        .Comment = MapComment
      End With
    End If

    Me.Media = Media

    MyStartTime = StartTime
    MyFrameCount = FrameCount
    MyFrameRate = FrameRate
    Me.UserControlValues = UserControlValues
    MyRelatedRecords = RelatedRecords

    Call CreateEmptyTimeLine()

    Me.VideoFiles = New List(Of clsVideoFile)
    MyVideoFilesLoaded = False

    MyRoadUsers = New List(Of clsRoadUser)
    MyRoadUsersLoaded = False

    MySelectedRoadUser = Nothing
    MyKeyRoadUser1 = Nothing
    MyKeyRoadUser2 = Nothing

    MySelectedRoadUserIndex = -1
    MyKeyRoadUser1Index = -1
    MyKeyRoadUser2Index = -1

    Me.Gates = New List(Of clsGate)
  End Sub



  Public Sub LoadRoadUsers()
    Dim _FilePath As String
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _RoadUserTemp As clsRoadUser
    Dim _TypeString As String, _Type As clsRoadUserType
    Dim _Length As Double
    Dim _Width As Double
    Dim _Height As Double
    Dim _Weight As Integer = NoValue
    Dim _FirstFrame As Integer
    Dim _DataPoint As clsDataPoint
    Dim _Values() As String
    Dim _X, _Y As Double
    Dim _Dx, _Dy As Double
    Dim _V, _A, _J As Double
    Dim _Xpxl, _Ypxl As Double
    Dim _Dxpxl, _Dypxl As Double
    Dim _Key As clsRoadUser.KeyRoadUser

    MyRoadUsers = New List(Of clsRoadUser)

    _FilePath = RoadUsersFile()
    If File.Exists(_FilePath) Then
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, _FilePath, OpenMode.Input)
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        _TypeString = _Line.Remove(0, 6)
        _Key = clsRoadUser.KeyRoadUser.NotKey
        If _TypeString.EndsWith("*1") Then
          _Key = clsRoadUser.KeyRoadUser.Key1
          _TypeString = _TypeString.Remove(_TypeString.Length - 3)
        ElseIf _TypeString.EndsWith("*2") Then
          _Key = clsRoadUser.KeyRoadUser.Key2
          _TypeString = _TypeString.Remove(_TypeString.Length - 3)
        End If
        _Type = MyParentProject.GetRoadUserTypeByName(_TypeString)
        _Line = LineInput(_FileIndex)
        _Length = Double.Parse(_Line.Remove(0, 11), CultureInfo.InvariantCulture)
        _Line = LineInput(_FileIndex)
        _Width = Double.Parse(_Line.Remove(0, 10), CultureInfo.InvariantCulture)
        _Line = LineInput(_FileIndex)
        _Height = Double.Parse(_Line.Remove(0, 10), CultureInfo.InvariantCulture)
        _Line = LineInput(_FileIndex)
        If _Line.StartsWith("Weight") Then
          _Weight = Integer.Parse(_Line.Remove(0, 12), CultureInfo.InvariantCulture)
          _Line = LineInput(_FileIndex)
        End If
        _FirstFrame = Integer.Parse(_Line.Remove(0, 8).Split(Chr(Asc("-")))(0), CultureInfo.InvariantCulture)


        _RoadUserTemp = New clsRoadUser(Me, _FirstFrame, _Type, _Length, _Width, _Height, _Weight)
        _Line = LineInput(_FileIndex)
        _Weight = NoValue

        Do
          If Not EOF(_FileIndex) Then
            _Line = LineInput(_FileIndex)
            If _Line.Length > 0 Then
              _Values = _Line.Split(Chr(Asc(";")))
              If Not Double.TryParse(_Values(0), NumberStyles.Any, CultureInfo.InvariantCulture, _X) Then _X = NoValue
              If Not Double.TryParse(_Values(1), NumberStyles.Any, CultureInfo.InvariantCulture, _Y) Then _Y = NoValue
              If Not Double.TryParse(_Values(2), NumberStyles.Any, CultureInfo.InvariantCulture, _V) Then _V = NoValue
              If Not Double.TryParse(_Values(3), NumberStyles.Any, CultureInfo.InvariantCulture, _A) Then _A = NoValue
              If Not Double.TryParse(_Values(4), NumberStyles.Any, CultureInfo.InvariantCulture, _J) Then _J = NoValue
              If Not Double.TryParse(_Values(5), NumberStyles.Any, CultureInfo.InvariantCulture, _Dx) Then _Dx = NoValue
              If Not Double.TryParse(_Values(6), NumberStyles.Any, CultureInfo.InvariantCulture, _Dy) Then _Dy = NoValue
              If Not Double.TryParse(_Values(7), NumberStyles.Any, CultureInfo.InvariantCulture, _Xpxl) Then _Xpxl = NoValue
              If Not Double.TryParse(_Values(8), NumberStyles.Any, CultureInfo.InvariantCulture, _Ypxl) Then _Ypxl = NoValue
              If Not Double.TryParse(_Values(9), NumberStyles.Any, CultureInfo.InvariantCulture, _Dxpxl) Then _Dxpxl = NoValue
              If Not Double.TryParse(_Values(10), NumberStyles.Any, CultureInfo.InvariantCulture, _Dypxl) Then _Dypxl = NoValue
              _DataPoint = New clsDataPoint(_Xpxl, _Ypxl, New clsDxDy(_Dxpxl, _Dypxl), _X, _Y, New clsDxDy(_Dx, _Dy), _V, _A, _J)
              _RoadUserTemp.AddPoint(_DataPoint)
            End If
          Else
            _Line = ""
          End If
        Loop Until _Line.Length = 0
        If _RoadUserTemp.DataPointsCount > 0 Then
          MyRoadUsers.Add(_RoadUserTemp)
          If _Key = clsRoadUser.KeyRoadUser.Key1 Then
            Me.KeyRoadUser1 = _RoadUserTemp
          ElseIf _Key = clsRoadUser.KeyRoadUser.Key2 Then
            Me.KeyRoadUser2 = _RoadUserTemp
          End If
        End If

      Loop
      FileClose(_FileIndex)
    End If

    Me.RoadUsersChanged = False
  End Sub


  Public Sub LoadGates(ByVal FilePath As String)
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _Strings As String()
    Dim _Name As String
    Dim _X1, _Y1, _X2, _Y2 As Double
    Dim _Gate As clsGate


    Me.Gates = New List(Of clsGate)

    If File.Exists(FilePath) Then
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, FilePath, OpenMode.Input)
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        _Strings = _Line.Split(Char.Parse(";"))
        _Name = _Strings(0)

        Double.TryParse(_Strings(1), NumberStyles.Any, CultureInfo.InvariantCulture, _X1)
        Double.TryParse(_Strings(2), NumberStyles.Any, CultureInfo.InvariantCulture, _Y1)
        Double.TryParse(_Strings(3), NumberStyles.Any, CultureInfo.InvariantCulture, _X2)
        Double.TryParse(_Strings(4), NumberStyles.Any, CultureInfo.InvariantCulture, _Y2)
        _Gate = New clsGate(_Name, _X1, _Y1, _X2, _Y2)
        Me.Gates.Add(_Gate)
      Loop

      FileClose(_FileIndex)
      Call LoadGatesDisplay()
      RaiseEvent AppearanceChanged() ' Gates_Changed()
    End If
  End Sub

  Public Sub LoadVideoFiles()

    For Each _VideoFile In Me.VideoFiles
      _VideoFile.Open()
    Next

    Call ReloadFramesInTimeLine()

  End Sub


  Public Sub ReloadFramesInTimeLine()
    For _Frame = 0 To Me.FrameCount - 1
      MyTimeLine(_Frame).VideoFileFrames.Clear()
      For Each _VideoFile In Me.VideoFiles
        MyTimeLine(_Frame).VideoFileFrames.Add(_VideoFile.GetFrameIndex(Me.StartTime + _Frame / Me.FrameRate))
      Next
    Next
  End Sub

  Public Sub AddStartFrame()
    Dim _TimePoint As clsTimeLinePoint
    Dim _RoadUser As clsRoadUser
    Dim _VideoFile As clsVideoFile

    MyFrameCount = MyFrameCount + 1
    MyStartTime = MyStartTime - 1 / MyFrameRate

    For Each _RoadUser In MyRoadUsers
      _RoadUser.FirstFrame = _RoadUser.FirstFrame + 1
    Next
    If MyRoadUsers.Count > 0 Then Me.RoadUsersChanged = True

    _TimePoint = New clsTimeLinePoint
    For Each _VideoFile In Me.VideoFiles
      _TimePoint.VideoFileFrames.Add(_VideoFile.GetFrameIndex(MyStartTime))
    Next

    MyTimeLine.Insert(0, _TimePoint)
  End Sub



  Public Sub AddFrame()
    Dim _TimePoint As clsTimeLinePoint

    MyFrameCount = MyFrameCount + 1
    _TimePoint = New clsTimeLinePoint
    For Each _VideoFile In Me.VideoFiles
      _TimePoint.VideoFileFrames.Add(_VideoFile.GetFrameIndex(MyStartTime + (Me.FrameCount - 1) / Me.FrameRate))
    Next
    MyTimeLine.Add(_TimePoint)
  End Sub

  Public Sub ClearRoadUsers()
    Dim _FilePath As String

    _FilePath = RoadUsersFile()
    If File.Exists(_FilePath) Then File.Delete(_FilePath)
    Me.RoadUsers.Clear()

  End Sub

  Public Sub SaveRoadUsers()
    Dim _FilePath As String
    Dim _FileIndex As Integer
    Dim _OutputLine As String
    Dim _Type As String

    _FilePath = RoadUsersFile()
    If MyRoadUsers.Count > 0 Then
      Call SortRoadUsers()
      _FileIndex = FileSystem.FreeFile()
      FileSystem.FileOpen(_FileIndex, _FilePath, OpenMode.Output)
      For Each _RoadUser In MyRoadUsers
        _Type = _RoadUser.Type.Name
        If _RoadUser.Key = clsRoadUser.KeyRoadUser.Key1 Then _Type = _Type & " *1"
        If _RoadUser.Key = clsRoadUser.KeyRoadUser.Key2 Then _Type = _Type & " *2"
        FileSystem.PrintLine(_FileIndex, "Type: " & _Type)
        FileSystem.PrintLine(_FileIndex, "Length, m: " & _RoadUser.Length.ToString("0.00", CultureInfo.InvariantCulture))
        FileSystem.PrintLine(_FileIndex, "Width, m: " & _RoadUser.Width.ToString("0.00", CultureInfo.InvariantCulture))
        FileSystem.PrintLine(_FileIndex, "Height, m: " & _RoadUser.Height.ToString("0.00", CultureInfo.InvariantCulture))
        FileSystem.PrintLine(_FileIndex, "Weight, kg: " & _RoadUser.Weight.ToString("0", CultureInfo.InvariantCulture))
        FileSystem.PrintLine(_FileIndex, "Frames: " & _RoadUser.FirstFrame.ToString("0", CultureInfo.InvariantCulture) & "-" &
                                                      _RoadUser.LastFrame.ToString("0", CultureInfo.InvariantCulture))
        FileSystem.PrintLine(_FileIndex, "X, m; Y, m; V, m/s; A, m/s2; J, m/s3; DirX; DirY; Xpxl, m; Ypxl, m; Dxpxl; Dypxl")

        For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
          With _RoadUser.DataPoint(_Frame)
            _OutputLine = .X.ToString("0.000", CultureInfo.InvariantCulture) & ";"
            _OutputLine = _OutputLine & .Y.ToString("0.000", CultureInfo.InvariantCulture) & ";"
            If IsValue(.V) Then
              _OutputLine = _OutputLine & .V.ToString("0.000", CultureInfo.InvariantCulture) & ";"
            Else
              _OutputLine = _OutputLine & "NoValue;"
            End If
            If IsValue(.A) Then
              _OutputLine = _OutputLine & .A.ToString("0.000", CultureInfo.InvariantCulture) & ";"
            Else
              _OutputLine = _OutputLine & "NoValue;"
            End If
            If IsValue(.J) Then
              _OutputLine = _OutputLine & .J.ToString("0.000", CultureInfo.InvariantCulture) & ";"
            Else
              _OutputLine = _OutputLine & "NoValue;"
            End If
            _OutputLine = _OutputLine & .DxDy.Dx.ToString("0.00000", CultureInfo.InvariantCulture) & ";"
            _OutputLine = _OutputLine & .DxDy.Dy.ToString("0.00000", CultureInfo.InvariantCulture) & ";"
            If .OriginalXYExists Then
              _OutputLine = _OutputLine & .Xpxl.ToString("0.00000", CultureInfo.InvariantCulture) & ";"
              _OutputLine = _OutputLine & .Ypxl.ToString("0.00000", CultureInfo.InvariantCulture) & ";"
              _OutputLine = _OutputLine & .DxDypxl.Dx.ToString("0.00000", CultureInfo.InvariantCulture) & ";"
              _OutputLine = _OutputLine & .DxDypxl.Dy.ToString("0.00000", CultureInfo.InvariantCulture)
            Else
              _OutputLine = _OutputLine & "NoValue;"
              _OutputLine = _OutputLine & "NoValue;"
              _OutputLine = _OutputLine & "NoValue;"
              _OutputLine = _OutputLine & "NoValue"
            End If
          End With
          PrintLine(_FileIndex, _OutputLine)
        Next _Frame
        PrintLine(_FileIndex, "")
      Next _RoadUser
      FileClose(_FileIndex)
    Else
      Me.ClearRoadUsers()
    End If
    Me.RoadUsersChanged = False

  End Sub



  Public Function AddRoadUser(ByVal FirstFrame As Integer,
                              ByVal Type As clsRoadUserType,
                              Optional ByVal Length As Double = NoValue,
                              Optional ByVal Width As Double = NoValue,
                              Optional ByVal Height As Double = NoValue) As clsRoadUser

    Dim _NewRoadUser As clsRoadUser
    Dim _NewDataPoint As clsDataPoint
    Dim _MiddlePoint As clsGeomPoint

    _NewRoadUser = New clsRoadUser(Me, FirstFrame, Type, Length, Width, Height)
    _MiddlePoint = Me.RoadUserDefaultPosition
    _NewDataPoint = New clsDataPoint(_MiddlePoint.X, _MiddlePoint.Y)
    _NewRoadUser.AddPoint(_NewDataPoint)
    MyRoadUsers.Add(_NewRoadUser)
    Call SortRoadUsers()
    Me.SelectedRoadUser = _NewRoadUser

    Me.RoadUsersChanged = True

    Return _NewRoadUser
  End Function



  Public Sub RemoveRoadUser(ByVal RoadUser As clsRoadUser)
    Dim _i As Integer

    For _i = RoadUser.FirstFrame To RoadUser.LastFrame
      MyTimeLine(_i).RoadUsers.Remove(RoadUser)
    Next
    MyRoadUsers.Remove(RoadUser)

    If RoadUser.Equals(MySelectedRoadUser) Then
      Me.SelectedRoadUser = Nothing
      RaiseEvent AppearanceChanged()
      'RaiseEvent SelectedRoadUser_Changed()
    End If

    If RoadUser.Equals(MyKeyRoadUser1) Then
      Me.KeyRoadUser1 = Nothing
      RaiseEvent AppearanceChanged()
      ' RaiseEvent KeyRoadUser1_Changed()
    End If

    If RoadUser.Equals(MyKeyRoadUser2) Then
      Me.KeyRoadUser2 = Nothing
      RaiseEvent AppearanceChanged()
      ' RaiseEvent KeyRoadUser2_Changed()
    End If

    Me.RoadUsersChanged = True

  End Sub

  Public Sub RemoveRoadUserAt(ByVal RoadUserIndex As Integer)
    Dim _RoadUser As clsRoadUser

    If IsBetween(RoadUserIndex, 0, MyRoadUsers.Count - 1) Then
      _RoadUser = MyRoadUsers(RoadUserIndex)
      Call RemoveRoadUser(_RoadUser)
    End If
  End Sub


  Public Function ImportMap(ByVal InputMapFilePath As String, Optional ByVal SuppressMessages As Boolean = False, Optional ChangeNameTo As String = "") As Boolean
    Dim _MapImageFileName, _MapCalibrationFileName As String
    Dim _InputMapImageFilePath As String
    Dim _OutputMapImageFilePath, _OutputMapCalibrationFilePath As String
    Dim _Bitmap As Bitmap


    If InputMapFilePath.Length > 0 Then
      If File.Exists(InputMapFilePath) Then
        _MapImageFileName = Path.GetFileNameWithoutExtension(InputMapFilePath) & ".jpg"
        _MapCalibrationFileName = Path.GetFileName(InputMapFilePath)
        _InputMapImageFilePath = Path.GetDirectoryName(InputMapFilePath) & "\" & _MapImageFileName

        If ChangeNameTo.Length > 0 Then
          _OutputMapCalibrationFilePath = MyParentProject.MapDirectory & ChangeNameTo
          _OutputMapImageFilePath = MyParentProject.MapDirectory & Path.GetFileNameWithoutExtension(_OutputMapCalibrationFilePath) & ".jpg"
        Else
          _OutputMapCalibrationFilePath = MyParentProject.MapDirectory & _MapCalibrationFileName
          _OutputMapImageFilePath = MyParentProject.MapDirectory & _MapImageFileName
        End If


        If File.Exists(_InputMapImageFilePath) Then
          _Bitmap = New Bitmap(_InputMapImageFilePath)
        Else
          If Not SuppressMessages Then MsgBox("The selected map calibration file does not have a corresponding image '" & _MapImageFileName & "'! The map will not be imported.", MsgBoxStyle.Critical)
          Return False
        End If


        Dim _MsgBoxResult As MsgBoxResult

        If File.Exists(_OutputMapCalibrationFilePath) Or File.Exists(_OutputMapImageFilePath) Then
          If Not SuppressMessages Then
            _MsgBoxResult = MsgBox("File '" & _MapCalibrationFileName & "' already exists in the '/Map' directory. If you want to re-use it, press 'Yes'. If you want to create a new file with a different name, press 'No'", MsgBoxStyle.YesNo)
            If _MsgBoxResult = MsgBoxResult.No Then
              Do Until (Not File.Exists(_OutputMapImageFilePath)) And (Not File.Exists(_OutputMapCalibrationFilePath))
                _MapImageFileName = Path.GetFileNameWithoutExtension(_OutputMapImageFilePath) & "+.jpg"
                _MapCalibrationFileName = Path.GetFileNameWithoutExtension(_OutputMapCalibrationFilePath) & "+.tamap"
                _OutputMapImageFilePath = Me.MapDirectory & _MapImageFileName
                _OutputMapCalibrationFilePath = Me.MapDirectory & _MapCalibrationFileName
              Loop
              File.Copy(InputMapFilePath, _OutputMapCalibrationFilePath)
              SaveJpgImage(_Bitmap, _OutputMapImageFilePath)
            End If
          Else
            If Not File.Exists(_OutputMapCalibrationFilePath) Then File.Copy(InputMapFilePath, _OutputMapCalibrationFilePath)
            If Not File.Exists(_OutputMapImageFilePath) Then Call SaveJpgImage(_Bitmap, _OutputMapImageFilePath)
          End If
        Else
          File.Copy(InputMapFilePath, _OutputMapCalibrationFilePath)
          SaveJpgImage(_Bitmap, _OutputMapImageFilePath)
        End If

        With Me.Map
          .Available = True
          .Map = Path.GetFileName(_OutputMapImageFilePath)
          .Size = New Size(_Bitmap.Width, _Bitmap.Height)
          .ImportMapParameters(_OutputMapCalibrationFilePath)
          .Comment = ""
          .Visible = False
        End With
        _Bitmap.Dispose()
      End If
    End If

    Return True
  End Function



  Public Function ImportVideoFile(ByVal InputVideoFilePath As String,
                                  Optional ByVal ClockDescrepancy As Double = 0,
                                  Optional ByVal SuppressMessages As Boolean = False,
                                  Optional ByVal ChangeNameTo As String = "") As Boolean
    Dim _InputLogFilePath As String
    Dim _OutputVideoFilePath, _OutputLogFilePath As String
    Dim _VideoFileName As String
    Dim _VideoFile As clsVideoFile
    Dim _UseExistingFile As Boolean


    If InputVideoFilePath.Length > 0 Then
      _InputLogFilePath = Path.GetDirectoryName(InputVideoFilePath) & "\" & Path.GetFileNameWithoutExtension(InputVideoFilePath) & ".log"
      If File.Exists(InputVideoFilePath) Then
        If ChangeNameTo.Length > 0 Then
          _VideoFileName = ChangeNameTo
        Else
          _VideoFileName = Path.GetFileName(InputVideoFilePath)
        End If
        _OutputVideoFilePath = MyParentProject.VideoDirectory & _VideoFileName
        _OutputLogFilePath = MyParentProject.VideoDirectory & Path.GetFileNameWithoutExtension(_OutputVideoFilePath) & ".log"
        _UseExistingFile = False

        If File.Exists(_OutputVideoFilePath) Then
          If SuppressMessages Then
            _UseExistingFile = True
          Else
            If MsgBox("File '" & _VideoFileName & "' already exists in the '/Video' directory. If you want to use the existing file, press 'Yes'. If you want to create a new file with a different name, press 'No'", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
              Do Until Not File.Exists(_OutputVideoFilePath)
                _VideoFileName = Path.GetFileNameWithoutExtension(_OutputVideoFilePath) & "+" & Path.GetExtension(InputVideoFilePath)
                _OutputVideoFilePath = MyParentProject.VideoDirectory & Path.GetFileNameWithoutExtension(_OutputVideoFilePath) & "+" & Path.GetExtension(_OutputVideoFilePath)
                _OutputLogFilePath = MyParentProject.VideoDirectory & Path.GetFileNameWithoutExtension(_OutputLogFilePath) & "+" & Path.GetExtension(_OutputLogFilePath)
              Loop
            Else
              _UseExistingFile = True
            End If
          End If
        End If

        If Not _UseExistingFile Then
          FileCopy(InputVideoFilePath, _OutputVideoFilePath)
          If File.Exists(_InputLogFilePath) Then FileCopy(_InputLogFilePath, _OutputLogFilePath)
        End If

        _VideoFile = New clsVideoFile(Me, _VideoFileName)
        _VideoFile.Open()

        _VideoFile.Position = VideoFiles.Count
        _VideoFile.ClockDiscrepancy = ClockDescrepancy
        _VideoFile.Status = VideoFileStatuses.Created

        Me.VideoFiles.Add(_VideoFile)

        For _Frame = 0 To Me.FrameCount - 1
          MyTimeLine(_Frame).VideoFileFrames.Add(_VideoFile.GetFrameIndex(MyStartTime + _Frame / Me.FrameRate))
        Next

        Return True
      Else
        Return False
      End If
    Else
      Return False
    End If

  End Function





  Public Sub ImportRoadUsers(ByVal InputTrajecrotyFilePath As String, ByVal VideoFileIndex As Integer, Optional ByVal PreserveExistingRoadUsers As Boolean = True, Optional ByVal TryConnectTrajectories As Boolean = True)
    Dim _ImportedTrajectory As List(Of clsTPoint)
    Dim _FirstFrame As Integer
    Dim _ImportedFrame As Integer
    Dim _ImportedDataPoint As clsTPoint
    Dim _ImportedTime As Double
    Dim _ImportedX As Double
    Dim _ImportedY As Double
    Dim _ImportedDx As Double
    Dim _ImportedDy As Double
    Dim _ImportedV As Double

    Dim _NewRoadUser As clsRoadUser
    Dim _ImportedRUType As String
    Dim _InputFile As Integer
    Dim _InputLine As String
    Dim _Values As String()
    Dim _RoadUserID As Integer

    Dim _Type As clsRoadUserType
    Dim _DataPoint As clsDataPoint



    If Not PreserveExistingRoadUsers Then
      RoadUsers.Clear()
      For Each TimePoint In Me.TimeLine
        TimePoint.RoadUsers.Clear()
      Next
    End If

    _InputFile = FreeFile()
    FileOpen(_InputFile, InputTrajecrotyFilePath, OpenMode.Input)

    '_ImportedTrajectory = Nothing
    _NewRoadUser = Nothing
    '_ImportedRUType = ""
    _RoadUserID = -1

    Do Until FileSystem.EOF(_InputFile)
      _InputLine = FileSystem.LineInput(_InputFile)
      If _InputLine.StartsWith("track_id,class,t,frame_number,x,y,dx,dy,speed") Then

      ElseIf _InputLine.Length > 0 Then
        _Values = _InputLine.Split(Chr(Asc(",")))

        If _RoadUserID < Integer.Parse(_Values(0)) Then
          If _NewRoadUser IsNot Nothing Then Me.RoadUsers.Add(_NewRoadUser)

          'create new road user
          _RoadUserID = Integer.Parse(_Values(0))
          _FirstFrame = Integer.Parse(_Values(3))
          _Type = MyParentProject.GetRoadUserTypeByName(Trim(_Values(1)))
          _NewRoadUser = New clsRoadUser(Me, _FirstFrame, _Type)

        End If


        _ImportedX = Double.Parse(_Values(4), CultureInfo.InvariantCulture)
        _ImportedY = Double.Parse(_Values(5), CultureInfo.InvariantCulture)
        _ImportedDx = Double.Parse(_Values(6), CultureInfo.InvariantCulture)
        _ImportedDy = Double.Parse(_Values(7), CultureInfo.InvariantCulture)
        _ImportedV = Double.Parse(_Values(8), CultureInfo.InvariantCulture)

        _DataPoint = New clsDataPoint(_ImportedX, _ImportedY, New clsDxDy(_ImportedDx, _ImportedDy), _ImportedX, _ImportedY, New clsDxDy(_ImportedDx, _ImportedDy), _ImportedV, NoValue, NoValue)
        _NewRoadUser.AddPoint(_DataPoint)

        'If _ImportedTrajectory Is Nothing Then
        '    _ImportedTrajectory = New List(Of clsTPoint)
        '    _ImportedRUFirstFrame = Integer.Parse(_Values(3)) - 1
        '    _ImportedFrame = _ImportedRUFirstFrame
        '    _ImportedRUType = Trim(_Values(2))
        '  End If

        '_ImportedTime = Me.VideoFiles(VideoFileIndex).StartTime + Me.VideoFiles(VideoFileIndex).GetTimeStamp(_ImportedFrame)

        '_ImportedDataPoint = New clsTPoint(_ImportedX, _ImportedY, _ImportedTime)
        '_ImportedTrajectory.Add(_ImportedDataPoint)
        '_ImportedFrame = _ImportedFrame + 1
      End If
    Loop

    FileClose(_InputFile)
    _NewRoadUser = Nothing

    Call SortRoadUsers()
    'If TryConnectTrajectories Then Call ConnectRoadUsers()

    'MyRoadUsersChanged = True
  End Sub


  Public Sub ConnectRoadUsers()
    'Dim _CurrentRoadUser As clsRoadUser
    'Dim _RoadUser As clsRoadUser
    'Dim _Frame As Integer
    'Dim _FirstFrame As Integer
    'Dim _OverlappingFrames As Integer
    'Dim _D As Double
    'Dim _RUToBeRemoved As New List(Of clsRoadUser)
    'Dim _i As Integer, _j As Integer

    'For _i = 0 To Me.RoadUsers.Count - 1
    '  _CurrentRoadUser = Me.RoadUsers(_i)
    '  For Each _RoadUser In Me.TimeLine(_CurrentRoadUser.FirstFrame).RoadUsers
    '    If Not _CurrentRoadUser.Equals(_RoadUser) Then
    '      _OverlappingFrames = 1
    '      _FirstFrame = _CurrentRoadUser.FirstFrame
    '      Do While IsBetween(_FirstFrame + _OverlappingFrames, _CurrentRoadUser.FirstFrame, _CurrentRoadUser.LastFrame) And _
    '               IsBetween(_FirstFrame + _OverlappingFrames, _RoadUser.FirstFrame, _RoadUser.LastFrame)
    '        _OverlappingFrames = _OverlappingFrames + 1
    '      Loop

    '      _D = 0
    '      For _Frame = _FirstFrame To _FirstFrame + _OverlappingFrames - 1
    '        _D = _D + _CurrentRoadUser.DataPoint(_Frame).DistanceTo(_RoadUser.DataPoint(_Frame))
    '      Next
    '      _D = _D / _OverlappingFrames

    '      '          D = Math.Sqrt((CurrentRoadUser.DataPoint(Frame).X - _RoadUser.DataPoint(Frame).X) ^ 2 + (CurrentRoadUser.DataPoint(Frame).Y - _RoadUser.DataPoint(Frame).Y) ^ 2)
    '      If _D < 7 Then
    '        For _Frame = _CurrentRoadUser.FirstFrame To _CurrentRoadUser.LastFrame
    '          If _Frame <= _RoadUser.LastFrame Then
    '            _RoadUser.DataPoint(_Frame).Xpxl = (_RoadUser.DataPoint(_Frame).Xpxl + _CurrentRoadUser.DataPoint(_Frame).Xpxl) / 2
    '            _RoadUser.DataPoint(_Frame).Ypxl = (_RoadUser.DataPoint(_Frame).Ypxl + _CurrentRoadUser.DataPoint(_Frame).Ypxl) / 2
    '          Else
    '            _RoadUser.AddPoint(_CurrentRoadUser.DataPoint(_Frame))
    '          End If
    '        Next _Frame
    '        _RUToBeRemoved.Add(_CurrentRoadUser)
    '      End If
    '    End If
    '  Next
    'Next _i


    'For Each _RoadUser In _RUToBeRemoved
    '  Me.RemoveRoadUser(_RoadUser)
    'Next

    'For Each _RoadUser In Me.RoadUsers
    '  _RoadUser.SmoothTrajectory()
    'Next


    'Dim _InterpolateFramesForward As Integer
    'Dim _v As Double
    'Dim _F1 As Integer, _F2 As Integer
    'Dim _PredictedPoint As clsGeomPoint

    '_RUToBeRemoved.Clear()
    ''Connect by interpolation
    'For _i = 0 To Me.RoadUsers.Count - 1
    '  _CurrentRoadUser = Me.RoadUsers(_i)
    '  For _j = _i + 1 To Me.RoadUsers.Count - 1
    '    _RoadUser = Me.RoadUsers(_j)
    '    If _RoadUser.FirstFrame > _CurrentRoadUser.LastFrame Then
    '      _InterpolateFramesForward = _RoadUser.FirstFrame - _CurrentRoadUser.LastFrame
    '      If _InterpolateFramesForward < 200 Then
    '        _F1 = _CurrentRoadUser.FirstFrame
    '        _F2 = _CurrentRoadUser.LastFrame
    '        _v = (_CurrentRoadUser.DataPoint(_F2).Y - _CurrentRoadUser.DataPoint(_F1).Y) / (Me.GetTime(_F2) - Me.GetTime(_F1))
    '        _PredictedPoint = New clsGeomPoint(_CurrentRoadUser.DataPoint(_F2).X, _CurrentRoadUser.DataPoint(_F2).Y + _v * (Me.GetTime(_F2 + _InterpolateFramesForward) - Me.GetTime(_F2)))

    '        _D = _PredictedPoint.DistanceTo(_RoadUser.DataPoint(_RoadUser.FirstFrame))
    '        If _D < 10 Then
    '          For _Frame = _CurrentRoadUser.LastFrame + 1 To _RoadUser.LastFrame
    '            If _Frame < _RoadUser.FirstFrame Then
    '              ' _CurrentRoadUser.AddPoint(New clsDataPoint())
    '            Else
    '              _CurrentRoadUser.AddPoint(_RoadUser.DataPoint(_Frame))
    '            End If
    '          Next _Frame
    '          _RUToBeRemoved.Add(_RoadUser)
    '        End If
    '      End If
    '    End If
    '  Next
    'Next


    'For Each _RoadUser In _RUToBeRemoved
    '  Me.RemoveRoadUser(_RoadUser)
    'Next

    'For Each _RoadUser In Me.RoadUsers
    '  _RoadUser.SmoothTrajectory()
    'Next

  End Sub


  Public Sub AssignDirections()
    'Dim _Direction As clsDirection
    'Dim _RoadUser As clsRoadUser


    'Dim _GateA As clsGeomLine
    'Dim _GateB As clsGeomLine
    'Dim _GateC As clsGeomLine
    'Dim _GateD As clsGeomLine






    'Me.Directions = New List(Of clsDirection)

    '_GateA = New clsGeomLine(320 / Scale, 600 / Scale, 470 / Scale, 600 / Scale)
    '_GateB = New clsGeomLine(320 / Scale, 600 / Scale, 330 / Scale, 510 / Scale)
    '_GateC = New clsGeomLine(330 / Scale, 510 / Scale, 470 / Scale, 510 / Scale)
    '_GateD = New clsGeomLine(470 / Scale, 510 / Scale, 470 / Scale, 600 / Scale)

    ''A+
    '_Direction = New clsDirection(1, _GateA, _GateD, "A+")
    'Me.Directions.Add(_Direction)
    ''A
    '_Direction = New clsDirection(2, _GateA, _GateC, "A")
    'Me.Directions.Add(_Direction)
    ''A-
    '_Direction = New clsDirection(3, _GateA, _GateB, "A-")
    'Me.Directions.Add(_Direction)

    ''B+
    '_Direction = New clsDirection(4, _GateB, _GateA, "B+")
    'Me.Directions.Add(_Direction)
    ''B
    '_Direction = New clsDirection(5, _GateB, _GateD, "B")
    'Me.Directions.Add(_Direction)
    ''B-
    '_Direction = New clsDirection(6, _GateB, _GateC, "B-")
    'Me.Directions.Add(_Direction)

    ''C+
    '_Direction = New clsDirection(7, _GateC, _GateB, "C+")
    'Me.Directions.Add(_Direction)
    ''C
    '_Direction = New clsDirection(8, _GateC, _GateA, "C")
    'Me.Directions.Add(_Direction)
    ''C-
    '_Direction = New clsDirection(9, _GateC, _GateD, "C-")
    'Me.Directions.Add(_Direction)

    ''D+
    '_Direction = New clsDirection(10, _GateD, _GateC, "D+")
    'Me.Directions.Add(_Direction)
    ''D
    '_Direction = New clsDirection(11, _GateD, _GateB, "D")
    'Me.Directions.Add(_Direction)
    ''D-
    '_Direction = New clsDirection(12, _GateD, _GateA, "D-")
    'Me.Directions.Add(_Direction)


    'Dim i As Integer = 0
    'For Each _RoadUser In Me.RoadUsers
    '  i = i + 1
    '  _RoadUser.Direction.Clear()
    '  For Each _Direction In Me.Directions
    '    If _RoadUser.FitDirection(_Direction) Then
    '      _RoadUser.Direction.Add(_Direction.ID)
    '    End If
    '  Next _Direction
    'Next _RoadUser

  End Sub


  Public Sub SaveGraphs()
    Dim _gMap, _gCamera As Graphics
    Dim _BitmapMap, _BitmapCamera As Bitmap
    Dim _CameraIndex As Integer = 0
    Dim _Point1, _Point2 As Point
    Dim _WorldPoint As clsGeomPoint3D
    Dim _Xfrom, _Xto As Integer
    Dim _Yfrom, _Yto As Integer
    Dim _MajorGridStep, _MinorGridStep As Integer
    Dim _Pen As Pen, _Brush As Brush
    Dim _x, _y As Integer
    Dim _Error As Double
    Dim _i As Integer
    Dim _FirstPoint As Boolean
    Dim _Font As Font
    Dim _StringFormat As StringFormat


    _Xfrom = 598 : _Xto = 634
    _Yfrom = -885 : _Yto = -849
    _MajorGridStep = 4
    _MinorGridStep = 1

    _BitmapMap = New Bitmap(Me.Map.Size.Width, Me.Map.Size.Height)
    _BitmapCamera = New Bitmap(Me.VideoFiles(_CameraIndex).FrameSize.Width, Me.VideoFiles(_CameraIndex).FrameSize.Height)

    _gMap = Graphics.FromImage(_BitmapMap)
    _gMap.DrawImage(Me.Map.Bitmap, 0, 0, _BitmapMap.Width, _BitmapMap.Height)
    _gCamera = Graphics.FromImage(_BitmapCamera)
    _gCamera.DrawImage(Me.VideoFiles(_CameraIndex).GetFrameBitmap(0), 0, 0, _BitmapCamera.Width, _BitmapCamera.Height)


    'one pixel fel
    For _x = 0 To _BitmapMap.Width - 1 Step 4
      For _y = 0 To _BitmapMap.Height - 1 Step 4
        _WorldPoint = Me.Map.TransformImageToWorld(_x, _y)
        _Point1 = Me.VideoFiles(_CameraIndex).TransformWorldToImage(_WorldPoint.X, _WorldPoint.Y)
        If IsBetween(_Point1.X, 0, _BitmapCamera.Width - 1) And IsBetween(_Point1.Y, 0, _BitmapCamera.Height - 1) Then
          _Error = Me.VideoFiles(_CameraIndex).OnePixelError(_Point1.X, _Point1.Y, Me.Map)
          If IsValue(_Error) Then
            If _Error < 0.1 Then
              _Brush = Brushes.Green
            ElseIf _Error < 0.2 Then
              _Brush = Brushes.GreenYellow
            ElseIf _Error < 0.3 Then
              _Brush = Brushes.Yellow
            ElseIf _Error < 0.4 Then
              _Brush = Brushes.Orange
            ElseIf _Error < 0.5 Then
              _Brush = Brushes.OrangeRed
            ElseIf _Error < 0.6 Then
              _Brush = Brushes.Red
            Else
              _Brush = Brushes.Brown
            End If
            _gMap.FillRectangle(_Brush, _x, _y, 2, 2)
          End If
        End If
      Next
    Next






    'gridlines
    _Pen = New Pen(Brushes.White, 1)
    For _x = _Xfrom To _Xto Step _MinorGridStep
      _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_x, _Yfrom), Me.Map.TransformWorldToImage(_x, _Yto))
    Next
    For _y = _Yfrom To _Yto Step _MinorGridStep
      _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_Xfrom, _y), Me.Map.TransformWorldToImage(_Xto, _y))
    Next

    _Pen = New Pen(Brushes.Black, 1)
    For _x = _Xfrom To _Xto Step _MajorGridStep
      _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_x, _Yfrom), Me.Map.TransformWorldToImage(_x, _Yto))
    Next
    For _y = _Yfrom To _Yto Step _MajorGridStep
      _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_Xfrom, _y), Me.Map.TransformWorldToImage(_Xto, _y))
    Next

    'axes
    _Font = New Font("Times New Roman", 14, FontStyle.Bold)
    _StringFormat = New StringFormat
    _StringFormat.Alignment = StringAlignment.Far
    _Pen = New Pen(Brushes.Black, 2)
    _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_Xfrom, _Yfrom), Me.Map.TransformWorldToImage(_Xfrom, _Yto))
    _gMap.DrawLine(_Pen, Me.Map.TransformWorldToImage(_Xfrom, _Yfrom), Me.Map.TransformWorldToImage(_Xto, _Yfrom))
    For _x = _Xfrom To _Xto Step _MajorGridStep
      _Point1 = Me.Map.TransformWorldToImage(_x, _Yfrom)
      _Point2 = New Point(_Point1.X - 10, _Point1.Y - 25)
      _gMap.DrawString((_x - _Xfrom).ToString, _Font, Brushes.Black, _Point2)
    Next
    For _y = _Yfrom To _Yto Step _MajorGridStep
      _Point1 = Me.Map.TransformWorldToImage(_Xfrom, _y)
      _Point2 = New Point(_Point1.X - 10, _Point1.Y - 10)
      _gMap.DrawString((_y - _Yfrom).ToString, _Font, Brushes.Black, _Point2, _StringFormat)
    Next




    _i = 0
    For Each _RoadUser In Me.RoadUsers
      _i = _i + 1
      _FirstPoint = True
      If (_i Mod 2) = 0 Then
        _Pen = New Pen(Brushes.Black, 2)
      Else
        _Pen = New Pen(Brushes.Red, 2)
      End If

      For _Frame = _RoadUser.FirstFrame To _RoadUser.LastFrame
        With _RoadUser.DataPoint(_Frame)
          If .OriginalXYExists Then
            If IsBetween(.Xpxl, _Xfrom, _Xto) And IsBetween(.Ypxl, _Yfrom, _Yto) Then
              _Point1 = _Point2
              _Point2 = Me.Map.TransformWorldToImage(.Xpxl, .Ypxl)
              If Not _FirstPoint Then
                ' _gMap.DrawEllipse(_Pen, _Point2.X - 2, _Point2.Y - 2, 4, 4)
                '_gMap.DrawEllipse(_Pen, _Point2.X - 2, _Point2.Y - 2, 4, 4)
                _gMap.DrawLine(_Pen, _Point1, _Point2)
              End If
              _FirstPoint = False
            End If
          End If
        End With

      Next
    Next




    _BitmapMap.Save("C:\Map.png", Imaging.ImageFormat.Png)
    _BitmapCamera.Save("C:\Camera.png", Imaging.ImageFormat.Png)
    '   Call SaveJpgImage(_BitmapMap, "C:\Map.jpg")
    'Call SaveJpgImage(_BitmapCamera, "C:\Camera.jpg")







  End Sub




  Public Sub ClearMessagesAndIndicators()
    For _Frame = 0 To MyFrameCount - 1
      MyTimeLine(_Frame).MessagePoints.Clear()
      MyTimeLine(_Frame).MessageLines.Clear()
      MyTimeLine(_Frame).Indicators = Nothing
    Next _Frame
  End Sub

  Public Sub LoadGatesDisplay()
    Dim _MessageLine As clsMessageLine

    Call ClearMessagesAndIndicators()

    For _Frame = 0 To MyFrameCount - 1
      For Each _Gate In Me.Gates
        _MessageLine = New clsMessageLine
        _MessageLine.X1 = _Gate.X1
        _MessageLine.Y1 = _Gate.Y1
        _MessageLine.X2 = _Gate.X2
        _MessageLine.Y2 = _Gate.Y2
        _MessageLine.Colour = MyParentProject.VisualiserOptions.LineColourGate
        _MessageLine.LineWidth = MyParentProject.VisualiserOptions.LineWidthGate
        _MessageLine.Message = _Gate.Name
        _MessageLine.Colour = MyParentProject.VisualiserOptions.LineColourGate
        MyTimeLine(_Frame).MessageLines.Add(_MessageLine)
      Next _Gate
    Next _Frame
  End Sub

  Public Sub CalculateTTCindicators(Optional ByVal AlongTrajectory As Boolean = True)
    Dim _TTC_point, _TA_point As clsMessagePoint
    Dim _Speed1_line, _Speed2_line, _SpeedAfter_line As clsMessageLine
    Dim _TTC As Double
    Dim _TA1, _TA2 As Double
    Dim _X_TTC, _Y_TTC As Double
    Dim _X_TA, _Y_TA As Double
    Dim _Vafter As Double
    Dim _SpeedVectorDxDy1, _SpeedVectorDxDy2, _SpeedVectorDxDyAfter As clsDxDy
    Dim _Vacc1, _Vacc2 As Double
    Dim _Decceleration As Double
    Dim _StartFrame, _EndFrame As Integer
    Dim _Trajectory1, _Trajectory2 As List(Of clsDPoint)
    Dim _LatestCollisionPoint As clsGeomPoint
    Dim _X1_PET, _Y1_PET As Double
    Dim _X2_PET, _Y2_PET As Double

    Dim _PET_CentrePoint1, _PET_CentrePoint2 As clsMessagePoint
    'Dim _Distance As Double
    'Dim _X1_Distance, _Y1_Distance As Double
    'Dim _X2_Distance, _Y2_Distance As Double
    'Dim _Distance_line As clsMessageLine



    Call ClearMessagesAndIndicators()
    _StartFrame = NoValue
    _EndFrame = NoValue

    If (MyKeyRoadUser1 IsNot Nothing) And (MyKeyRoadUser2 IsNot Nothing) Then
      _X1_PET = NoValue
      _Y1_PET = NoValue
      _X2_PET = NoValue
      _Y2_PET = NoValue


      For _Frame = 0 To MyFrameCount - 1
        _SpeedVectorDxDy1 = Nothing
        _SpeedVectorDxDy2 = Nothing
        _SpeedVectorDxDyAfter = Nothing

        _TTC = NoValue
        _TA1 = NoValue
        _TA2 = NoValue

        If IsBetween(_Frame, MyKeyRoadUser1.FirstFrame, MyKeyRoadUser1.LastFrame) And IsBetween(_Frame, MyKeyRoadUser2.FirstFrame, MyKeyRoadUser2.LastFrame) Then
          _Trajectory1 = MyKeyRoadUser1.ResampledTrajectory(_Frame, 2)
          _Trajectory2 = MyKeyRoadUser2.ResampledTrajectory(_Frame, 2)


          ''Draws the resampled trajectory (with 2 meters step between the points)
          'Dim _MP1 As clsMessagePoint
          'For _i = 0 To _Trajectory1.Count - 1
          '  _MP1 = New clsMessagePoint()
          '  _MP1.X = _Trajectory1(_i).X
          '  _MP1.Y = _Trajectory1(_i).Y
          '  _MP1.PointColour = Color.Aquamarine
          '  MyTimeLine(_Frame).MessagePoints.Add(_MP1)
          'Next

          'For _i = 0 To _Trajectory2.Count - 1
          '  _MP1 = New clsMessagePoint()
          '  _MP1.X = _Trajectory2(_i).X
          '  _MP1.Y = _Trajectory2(_i).Y
          '  _MP1.PointColour = Color.Aquamarine
          '  MyTimeLine(_Frame).MessagePoints.Add(_MP1)
          'Next


          Call CrossingCoursePlus(MyKeyRoadUser1,
                                  MyKeyRoadUser1.DataPoint(_Frame).V,
                                  _Trajectory1,
                                  MyKeyRoadUser2,
                                  MyKeyRoadUser2.DataPoint(_Frame).V,
                                 _Trajectory2,
                                  _TTC, _X_TTC, _Y_TTC,
                                  _TA1, _TA2, _X_TA, _Y_TA,
                                  _SpeedVectorDxDy1, _SpeedVectorDxDy2,
                                  _X1_PET, _Y1_PET, _X2_PET, _Y2_PET)
        End If

        If IsValue(_TTC) Then
          _LatestCollisionPoint = New clsGeomPoint(_X_TTC, _Y_TTC)

          MyTimeLine(_Frame).Indicators = New clsIndicatorList
          SpeedDifference(MyKeyRoadUser1.DataPoint(_Frame).V, _SpeedVectorDxDy1, MyKeyRoadUser2.DataPoint(_Frame).V, _SpeedVectorDxDy2, MyTimeLine(_Frame).Indicators.Vrel)
          MyTimeLine(_Frame).Indicators.TTC = _TTC



          If Not IsValue(_StartFrame) Then _StartFrame = _Frame
          _EndFrame = _Frame

          '      _Decceleration = -8
          '      _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc1 < 0 Then _Vacc1 = 0
          '      _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc2 < 0 Then _Vacc2 = 0
          '      If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
          '        Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV81 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV82 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '      Else
          '        MyTimeLine(_Frame).Indicators.DeltaV81 = NoValue
          '        MyTimeLine(_Frame).Indicators.DeltaV82 = NoValue
          '      End If

          '      _Decceleration = -6
          '      _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc1 < 0 Then _Vacc1 = 0
          '      _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc2 < 0 Then _Vacc2 = 0
          '      If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
          '        Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV61 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV62 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '      Else
          '        MyTimeLine(_Frame).Indicators.DeltaV61 = NoValue
          '        MyTimeLine(_Frame).Indicators.DeltaV62 = NoValue
          '      End If

          '      _Decceleration = -4
          '      _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc1 < 0 Then _Vacc1 = 0
          '      _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc2 < 0 Then _Vacc2 = 0
          '      If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
          '        Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV41 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV42 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '      Else
          '        MyTimeLine(_Frame).Indicators.DeltaV41 = NoValue
          '        MyTimeLine(_Frame).Indicators.DeltaV42 = NoValue
          '      End If

          '      _Decceleration = 0
          '      _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc1 < 0 Then _Vacc1 = 0
          '      _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * _TTC
          '      If _Vacc2 < 0 Then _Vacc2 = 0
          '      If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
          Call SpeedAfterCollision(MyKeyRoadUser1.DataPoint(_Frame).V,
                                   MyKeyRoadUser1.Weight,
                                   _SpeedVectorDxDy1,
                                   MyKeyRoadUser2.DataPoint(_Frame).V,
                                   MyKeyRoadUser2.Weight,
                                   _SpeedVectorDxDy2,
                                   _Vafter,
                                   _SpeedVectorDxDyAfter,
                                   MyTimeLine(_Frame).Indicators.DeltaV01,
                                   MyTimeLine(_Frame).Indicators.DeltaV02)
          '        MyTimeLine(_Frame).Indicators.DeltaV01 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
          '        MyTimeLine(_Frame).Indicators.DeltaV02 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
          '      End If

          If MyParentProject.VisualiserOptions.DrawSpeeds Then
            _Speed1_line = New clsMessageLine
            _Speed1_line.X1 = _X_TTC
            _Speed1_line.Y1 = _Y_TTC
            _Speed1_line.X2 = _X_TTC + MyKeyRoadUser1.DataPoint(_Frame).V * _SpeedVectorDxDy1.Dx
            _Speed1_line.Y2 = _Y_TTC + MyKeyRoadUser1.DataPoint(_Frame).V * _SpeedVectorDxDy1.Dy
            _Speed1_line.Colour = MyParentProject.VisualiserOptions.Key1RUColour
            _Speed1_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_Speed1_line)

            _Speed2_line = New clsMessageLine
            _Speed2_line.X1 = _X_TTC
            _Speed2_line.Y1 = _Y_TTC
            _Speed2_line.X2 = _X_TTC + MyKeyRoadUser2.DataPoint(_Frame).V * _SpeedVectorDxDy2.Dx
            _Speed2_line.Y2 = _Y_TTC + MyKeyRoadUser2.DataPoint(_Frame).V * _SpeedVectorDxDy2.Dy
            _Speed2_line.Colour = MyParentProject.VisualiserOptions.Key2RUColour
            _Speed2_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_Speed2_line)


            _SpeedAfter_line = New clsMessageLine
            _SpeedAfter_line.X1 = _X_TTC
            _SpeedAfter_line.Y1 = _Y_TTC
            _SpeedAfter_line.X2 = _X_TTC + _Vafter * _SpeedVectorDxDyAfter.Dx
            _SpeedAfter_line.Y2 = _Y_TTC + _Vafter * _SpeedVectorDxDyAfter.Dy
            _SpeedAfter_line.Colour = MyParentProject.VisualiserOptions.LineColourSpeedAfterCollision
            _SpeedAfter_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_SpeedAfter_line)
          End If


          _TTC_point = New clsMessagePoint
          _TTC_point.X = _X_TTC
          _TTC_point.Y = _Y_TTC
          _TTC_point.Message = "TTC = " & Format(_TTC, "0.00s.")
          _TTC_point.Colour = Color.Red
          _TTC_point.Colour = Color.Red
          MyTimeLine(_Frame).MessagePoints.Add(_TTC_point)

        Else
          If IsValue(_TA1) And IsValue(_TA2) Then
            _LatestCollisionPoint = New clsGeomPoint(_X_TA, _Y_TA)

            MyTimeLine(_Frame).Indicators = New clsIndicatorList
            SpeedDifference(MyKeyRoadUser1.DataPoint(_Frame).V, _SpeedVectorDxDy1, MyKeyRoadUser2.DataPoint(_Frame).V, _SpeedVectorDxDy2, MyTimeLine(_Frame).Indicators.Vrel)
            MyTimeLine(_Frame).Indicators.T1 = _TA1
            MyTimeLine(_Frame).Indicators.T2 = _TA2
            '  _LatestT2 = Math.Max(_TA1, _TA2)


            If Not IsValue(_StartFrame) Then _StartFrame = _Frame
            _EndFrame = _Frame

            '        _Decceleration = -8
            '        _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc1 < 0 Then _Vacc1 = 0
            '        _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc2 < 0 Then _Vacc2 = 0
            '        If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
            '          Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV81 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV82 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '        Else
            '          MyTimeLine(_Frame).Indicators.DeltaV81 = NoValue
            '          MyTimeLine(_Frame).Indicators.DeltaV82 = NoValue
            '        End If

            '        _Decceleration = -6
            '        _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc1 < 0 Then _Vacc1 = 0
            '        _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc2 < 0 Then _Vacc2 = 0
            '        If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
            '          Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV61 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV62 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '        Else
            '          MyTimeLine(_Frame).Indicators.DeltaV61 = NoValue
            '          MyTimeLine(_Frame).Indicators.DeltaV62 = NoValue
            '        End If

            '        _Decceleration = -4
            '        _Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc1 < 0 Then _Vacc1 = 0
            '        _Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            '        If _Vacc2 < 0 Then _Vacc2 = 0
            '        If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
            '          Call SpeedAfterCollision(_Vacc1, MyKeyRoadUser1.Weight, _SpeedVectorDxDy1, _Vacc2, MyKeyRoadUser2.Weight, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV41 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
            '          MyTimeLine(_Frame).Indicators.DeltaV42 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            '        Else
            '          MyTimeLine(_Frame).Indicators.DeltaV41 = NoValue
            '          MyTimeLine(_Frame).Indicators.DeltaV42 = NoValue
            '        End If

            _Decceleration = 0
            '_Vacc1 = MyKeyRoadUser1.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            'If _Vacc1 < 0 Then _Vacc1 = 0
            '_Vacc2 = MyKeyRoadUser2.DataPoint(_Frame).V + _Decceleration * Math.Max(_TA1, _TA2)
            'If _Vacc2 < 0 Then _Vacc2 = 0
            'If (_Vacc1 > 0) Or (_Vacc2 > 0) Then
            Call SpeedAfterCollision(MyKeyRoadUser1.DataPoint(_Frame).V,
                                     MyKeyRoadUser1.Weight,
                                     _SpeedVectorDxDy1,
                                     MyKeyRoadUser2.DataPoint(_Frame).V,
                                     MyKeyRoadUser2.Weight,
                                     _SpeedVectorDxDy2,
                                     _Vafter,
                                     _SpeedVectorDxDyAfter,
                                     MyTimeLine(_Frame).Indicators.DeltaV01,
                                     MyTimeLine(_Frame).Indicators.DeltaV02)
            ' MyTimeLine(_Frame).Indicators.DeltaV01 = SpeedDifference(_Vacc1, _SpeedVectorDxDy1, _Vafter, _SpeedVectorDxDyAfter)
            'MyTimeLine(_Frame).Indicators.DeltaV02 = SpeedDifference(_Vacc2, _SpeedVectorDxDy2, _Vafter, _SpeedVectorDxDyAfter)
            'End If

            _Speed1_line = New clsMessageLine
              _Speed1_line.X1 = _X_TA
              _Speed1_line.Y1 = _Y_TA
              _Speed1_line.X2 = _X_TA + MyKeyRoadUser1.DataPoint(_Frame).V * _SpeedVectorDxDy1.Dx
              _Speed1_line.Y2 = _Y_TA + MyKeyRoadUser1.DataPoint(_Frame).V * _SpeedVectorDxDy1.Dy
            _Speed1_line.Colour = MyParentProject.VisualiserOptions.Key1RUColour
            _Speed1_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_Speed1_line)

            _Speed2_line = New clsMessageLine
              _Speed2_line.X1 = _X_TA
              _Speed2_line.Y1 = _Y_TA
              _Speed2_line.X2 = _X_TA + MyKeyRoadUser2.DataPoint(_Frame).V * _SpeedVectorDxDy2.Dx
              _Speed2_line.Y2 = _Y_TA + MyKeyRoadUser2.DataPoint(_Frame).V * _SpeedVectorDxDy2.Dy
            _Speed2_line.Colour = MyParentProject.VisualiserOptions.Key2RUColour
            _Speed2_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_Speed2_line)

            _SpeedAfter_line = New clsMessageLine
              _SpeedAfter_line.X1 = _X_TA
              _SpeedAfter_line.Y1 = _Y_TA
              _SpeedAfter_line.X2 = _X_TA + _Vafter * _SpeedVectorDxDyAfter.Dx
              _SpeedAfter_line.Y2 = _Y_TA + _Vafter * _SpeedVectorDxDyAfter.Dy
              _SpeedAfter_line.Colour = MyParentProject.VisualiserOptions.LineColourSpeedAfterCollision
              _SpeedAfter_line.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
            MyTimeLine(_Frame).MessageLines.Add(_SpeedAfter_line)

            _TA_point = New clsMessagePoint
              _TA_point.X = _X_TA
              _TA_point.Y = _Y_TA
              _TA_point.Message = "TA = " & Format(Math.Abs(_TA1 - _TA2), "0.00s.") & ", T2 = " & Format(Math.Max(_TA1, _TA2), "0.00s.")
              _TA_point.Colour = Color.Yellow
              _TA_point.Colour = Color.Yellow
            MyTimeLine(_Frame).MessagePoints.Add(_TA_point)

            '_PET_CentrePoint1 = New clsMessagePoint
            '_PET_CentrePoint1.X = _X1_PET
            '_PET_CentrePoint1.Y = _Y1_PET
            '_PET_CentrePoint1.PointColour = MyParentProject.VisualiserOptions.PenRoadUserKey1.Color
            '_PET_CentrePoint1.Message = "1"
            '_PET_CentrePoint1.MessageColour = MyParentProject.VisualiserOptions.PenRoadUserKey1.Color
            'MyTimeLine(_Frame).MessagePoints.Add(_PET_CentrePoint1)

            '_PET_CentrePoint2 = New clsMessagePoint
            '_PET_CentrePoint2.X = _X2_PET
            '_PET_CentrePoint2.Y = _Y2_PET
            '_PET_CentrePoint2.PointColour = MyParentProject.VisualiserOptions.PenRoadUserKey2.Color
            '_PET_CentrePoint2.Message = "2"
            '_PET_CentrePoint2.MessageColour = MyParentProject.VisualiserOptions.PenRoadUserKey2.Color
            'MyTimeLine(_Frame).MessagePoints.Add(_PET_CentrePoint2)
          End If
          End If
      Next





      'Fill in the speed values
      If IsValue(_StartFrame) And IsValue(_EndFrame) Then
        For _Frame = _StartFrame To _EndFrame
          If MyTimeLine(_Frame).Indicators Is Nothing Then
            MyTimeLine(_Frame).Indicators = New clsIndicatorList
          End If
          MyTimeLine(_Frame).Indicators.V1 = MyKeyRoadUser1.DataPoint(_Frame).V
          MyTimeLine(_Frame).Indicators.V2 = MyKeyRoadUser2.DataPoint(_Frame).V
        Next





        'Calculate PET based on the latest available values
        If IsValue(_X1_PET) And IsValue(_Y1_PET) And IsValue(_X2_PET) And IsValue(_Y2_PET) Then
          Dim _Time1, _Time2 As Double
          Dim _PET As Double
          Dim _Message As String
          Dim _LatestFrame As Integer
          Dim _PetMessagePoint As clsMessagePoint

          If (_LatestCollisionPoint IsNot Nothing) Then
            _Time1 = MyKeyRoadUser1.ComesClosestToPointAtTime(New clsGeomPoint(_X1_PET, _Y1_PET))
            _Time2 = MyKeyRoadUser2.ComesClosestToPointAtTime(New clsGeomPoint(_X2_PET, _Y2_PET))
            _PET = Math.Abs(_Time1 - _Time2)

            '_Message = "PET = " & _PET.ToString("0.00") & " s."
            'If IsValue(_LatestT2) Then _Message = _Message & Environment.NewLine & "latest T2 = " & _LatestT2.ToString("0.00") & " s."
            'MsgBox(_Message)

            _LatestFrame = Me.ClosestFrameFromAbsoluteTime(Math.Max(_Time1, _Time2))

            For _Frame = _EndFrame + 1 To _LatestFrame
              _PetMessagePoint = New clsMessagePoint
              _PetMessagePoint.X = _LatestCollisionPoint.X
              _PetMessagePoint.Y = _LatestCollisionPoint.Y
              _PetMessagePoint.Colour = Color.Yellow
              _PetMessagePoint.Message = "PET=" & _PET.ToString("0.00s.")
              _PetMessagePoint.Colour = Color.Yellow
              ' MyTimeLine(_Frame).MessagePoints.Add(_PetMessagePoint)
              If MyTimeLine(_Frame).Indicators Is Nothing Then MyTimeLine(_Frame).Indicators = New clsIndicatorList
              MyTimeLine(_Frame).Indicators.PET = _PET

              If IsBetween(_Frame, MyKeyRoadUser1.FirstFrame, MyKeyRoadUser1.LastFrame) Then MyTimeLine(_Frame).Indicators.V1 = MyKeyRoadUser1.DataPoint(_Frame).V
              If IsBetween(_Frame, MyKeyRoadUser2.FirstFrame, MyKeyRoadUser2.LastFrame) Then MyTimeLine(_Frame).Indicators.V2 = MyKeyRoadUser2.DataPoint(_Frame).V

            Next
          End If
        End If
      End If

    Else
      MsgBox("You have to select two key road users for which TTC to be calculated!")
    End If
  End Sub


  Public Sub CalculateDistanceIndicators()
    Dim _StartFrame, _EndFrame As Integer
    Dim _Distance As Double
    Dim _DistanceLine As clsGeomLine

    Dim _SpeedAfter, _DeltaV1, _DeltaV2 As Double
    Dim _SpeedAfterDxDy, _DeltaVDxDy1, _DeltaVDxDy2 As clsDxDy
    ' Dim _DeltaV_Messageline1, _DeltaV_Messageline2 As clsMessageLine

    Dim _Messageline As clsMessageLine
    Dim _MessagePoint As clsMessagePoint

    Call ClearMessagesAndIndicators()


    If (MyKeyRoadUser1 IsNot Nothing) And (MyKeyRoadUser2 IsNot Nothing) Then
      _StartFrame = CInt(Math.Max(MyKeyRoadUser1.FirstFrame, MyKeyRoadUser2.FirstFrame))
      _EndFrame = CInt(Math.Min(MyKeyRoadUser1.LastFrame, MyKeyRoadUser2.LastFrame))

      For _Frame = _StartFrame To _EndFrame
        If MyTimeLine(_Frame).Indicators Is Nothing Then MyTimeLine(_Frame).Indicators = New clsIndicatorList

        _DistanceLine = Nothing
        Call ClosestDistance(MyKeyRoadUser1, MyKeyRoadUser1.DataPoint(_Frame), MyKeyRoadUser2, MyKeyRoadUser2.DataPoint(_Frame), _Distance, _DistanceLine)

        _DeltaVDxDy1 = Nothing : _DeltaVDxDy2 = Nothing : _SpeedAfterDxDy = Nothing
        Call SpeedAfterCollision(MyKeyRoadUser1.DataPoint(_Frame).V,
                                 MyKeyRoadUser1.Weight,
                                 MyKeyRoadUser1.DataPoint(_Frame).DxDy,
                                 MyKeyRoadUser2.DataPoint(_Frame).V,
                                 MyKeyRoadUser2.Weight,
                                 MyKeyRoadUser2.DataPoint(_Frame).DxDy,
                                 _SpeedAfter,
                                 _SpeedAfterDxDy,
                                 _DeltaV1,
                                 _DeltaV2)

        MyTimeLine(_Frame).Indicators.Distance = _Distance
        MyTimeLine(_Frame).Indicators.DeltaV1AsIs = _DeltaV1
        MyTimeLine(_Frame).Indicators.DeltaV2AsIs = _DeltaV2
        MyTimeLine(_Frame).Indicators.V1 = MyKeyRoadUser1.DataPoint(_Frame).V
        MyTimeLine(_Frame).Indicators.V2 = MyKeyRoadUser2.DataPoint(_Frame).V


        _Messageline = New clsMessageLine
        _Messageline.X1 = _DistanceLine.X1
        _Messageline.Y1 = _DistanceLine.Y1
        _Messageline.X2 = _DistanceLine.X2
        _Messageline.Y2 = _DistanceLine.Y2
        _Messageline.Message = _Distance.ToString("0.0") & " m"
        MyTimeLine(_Frame).MessageLines.Add(_Messageline)




        _Messageline = New clsMessageLine
        _Messageline.X1 = (_DistanceLine.X1 + _DistanceLine.X2) / 2
        _Messageline.Y1 = (_DistanceLine.Y1 + _DistanceLine.Y2) / 2
        _Messageline.X2 = _Messageline.X1 + _SpeedAfter * _SpeedAfterDxDy.Dx
        _Messageline.Y2 = _Messageline.Y1 + _SpeedAfter * _SpeedAfterDxDy.Dy
        _Messageline.Colour = MyParentProject.VisualiserOptions.LineColourSpeedAfterCollision
        _Messageline.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
        MyTimeLine(_Frame).MessageLines.Add(_Messageline)



        _Messageline = New clsMessageLine
        _Messageline.X1 = (_DistanceLine.X1 + _DistanceLine.X2) / 2
        _Messageline.Y1 = (_DistanceLine.Y1 + _DistanceLine.Y2) / 2
        _Messageline.X2 = _Messageline.X1 + MyKeyRoadUser1.DataPoint(_Frame).V * MyKeyRoadUser1.DataPoint(_Frame).Dx
        _Messageline.Y2 = _Messageline.Y1 + MyKeyRoadUser1.DataPoint(_Frame).V * MyKeyRoadUser1.DataPoint(_Frame).Dy
        _Messageline.Colour = MyParentProject.VisualiserOptions.Key1RUColour
        _Messageline.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
        MyTimeLine(_Frame).MessageLines.Add(_Messageline)


        _Messageline = New clsMessageLine
        _Messageline.X1 = (_DistanceLine.X1 + _DistanceLine.X2) / 2
        _Messageline.Y1 = (_DistanceLine.Y1 + _DistanceLine.Y2) / 2
        _Messageline.X2 = _Messageline.X1 + MyKeyRoadUser2.DataPoint(_Frame).V * MyKeyRoadUser2.DataPoint(_Frame).Dx
        _Messageline.Y2 = _Messageline.Y1 + MyKeyRoadUser2.DataPoint(_Frame).V * MyKeyRoadUser2.DataPoint(_Frame).Dy
        _Messageline.Colour = MyParentProject.VisualiserOptions.Key2RUColour
        _Messageline.LineWidth = MyParentProject.VisualiserOptions.LineWidthSpeed
        MyTimeLine(_Frame).MessageLines.Add(_Messageline)



        Dim _DeltaVMessage As clsMessagePoint
        _DeltaVMessage = New clsMessagePoint
        _DeltaVMessage.X = MyKeyRoadUser1.DataPoint(_Frame).X
        _DeltaVMessage.Y = MyKeyRoadUser1.DataPoint(_Frame).Y
        _DeltaVMessage.Colour = MyParentProject.VisualiserOptions.LineColourSpeedAfterCollision
        _DeltaVMessage.Message = "dV1=" & _DeltaV1.ToString("0.0")
        MyTimeLine(_Frame).MessagePoints.Add(_DeltaVMessage)

        _DeltaVMessage = New clsMessagePoint
        _DeltaVMessage.X = MyKeyRoadUser2.DataPoint(_Frame).X
        _DeltaVMessage.Y = MyKeyRoadUser2.DataPoint(_Frame).Y
        _DeltaVMessage.Colour = MyParentProject.VisualiserOptions.LineColourSpeedAfterCollision
        _DeltaVMessage.Message = "dV2=" & _DeltaV2.ToString("0.0")
        MyTimeLine(_Frame).MessagePoints.Add(_DeltaVMessage)
      Next

    Else
      MsgBox("You have to select two key road users for which TTC to be calculated!")
    End If
  End Sub

  Public Sub ExportVideos(Optional ByVal ScalingFactor As Double = 1)
    Dim _FrameDirectories As List(Of String)
    Dim _VideoFilePath As String
    Dim _LogFilePath As String
    Dim _StartFrame, _EndFrame As Integer
    Dim _OriginalBitmap, _NewBitmap As Bitmap
    Dim _g As Graphics


    _FrameDirectories = New List(Of String)
    For _VideoFileIndex = 0 To Me.VideoFiles.Count - 1
      _VideoFilePath = Me.ExportDirectory & "Video\" & Me.ID.ToString("00000000") & "_" & (_VideoFileIndex + 1).ToString("00") & ".avi"
      _LogFilePath = Me.ExportDirectory & "Video\" & Me.ID.ToString("00000000") & "_" & (_VideoFileIndex + 1).ToString("00") & ".log"


      _StartFrame = Me.TimeLine(0).VideoFileFrames(_VideoFileIndex)
      _EndFrame = Me.TimeLine(Me.FrameCount - 1).VideoFileFrames(_VideoFileIndex)

      If (_EndFrame - _StartFrame) > 0 Then
        _FrameDirectories.Add(Me.ExportDirectory & "Video\" & Me.ID.ToString("00000000") & "_" & (_VideoFileIndex + 1).ToString("00") & "\")
        Directory.CreateDirectory(_FrameDirectories(_VideoFileIndex))


        For _Frame = _StartFrame To _EndFrame
          _OriginalBitmap = Me.VideoFiles(_VideoFileIndex).GetFrameBitmap(_Frame)
          _NewBitmap = New Bitmap(CInt(ScalingFactor * _OriginalBitmap.Width), CInt(ScalingFactor * _OriginalBitmap.Height))
          _g = Graphics.FromImage(_NewBitmap)
          _g.DrawImage(_OriginalBitmap, 0, 0, _NewBitmap.Width, _NewBitmap.Height)
          Call SaveJpgImage(_NewBitmap, _FrameDirectories(_VideoFileIndex) & (_Frame - _StartFrame + 1).ToString("00000000") & ".jpg")
        Next

        Call MakeAVIfromImages(_VideoFilePath,
                               _FrameDirectories(_VideoFileIndex),
                               Me.VideoFiles(_VideoFileIndex).FrameRate,
                               5000)
        Call DeleteDirectory(_FrameDirectories(_VideoFileIndex))

        Me.VideoFiles(_VideoFileIndex).ExportTimeStamps(_LogFilePath, _StartFrame, _EndFrame)
      End If
    Next


  End Sub

#End Region



#Region "Private subs"

  Private Sub CreateEmptyTimeLine()
    Dim _TimePoint As clsTimeLinePoint
    Dim _Frame As Integer

    MyTimeLine = New List(Of clsTimeLinePoint)
    For _Frame = 0 To MyFrameCount - 1
      _TimePoint = New clsTimeLinePoint
      MyTimeLine.Add(_TimePoint)
    Next _Frame
  End Sub



  Private Function RoadUserDefaultPosition() As clsGeomPoint
    If Me.Map.Available Then
      Return Me.Map.MiddlePoint
    Else
      Return New clsGeomPoint(10, 10)
    End If
  End Function



  Private Function RoadUsersFile() As String
    Return Me.TrajectoryDirectory & Format(Me.ID, "00000000") & ".txt"
  End Function



  Private Function FindRoadUserIndex(ByVal RoadUser As clsRoadUser) As Integer
    Dim _i As Integer

    _i = MyRoadUsers.FindIndex(Function(value As clsRoadUser)
                                 Return value.Equals(RoadUser)
                               End Function)
    Return _i
  End Function



  Private Sub SortRoadUsers()
    MyRoadUsers.Sort(Function(value1 As clsRoadUser, value2 As clsRoadUser)
                       If value1.FirstFrame = value2.FirstFrame Then
                         Return 0
                       ElseIf value1.FirstFrame > value2.FirstFrame Then
                         Return 1
                       Else
                         Return -1
                       End If
                     End Function)
  End Sub






  Private Sub SelectedRoadUserModel_Changed() Handles MySelectedRoadUser.ModelChanged
    Me.RoadUsersChanged = True
    RaiseEvent AppearanceChanged()
  End Sub


  Private Function ImportTrajectory() As clsRoadUser '(ByVal ImportedRUType As String, ByVal ImportedTrajectory As List(Of clsTPoint)) As clsRoadUser
    'Dim _FirstRUframe As Integer
    'Dim _LastRUframe As Integer
    'Dim _NewDataPoint As clsDataPoint
    'Dim _NewFrame As Integer
    'Dim _NewRoadUser As clsRoadUser
    'Dim _DataPoints As List(Of clsTPoint)
    'Dim _NewPoint As clsTPoint
    'Dim _PointsToUse As Integer
    'Dim _dT, _dTmin As Double
    'Dim _iMin As Integer
    'Dim _i1, _i2 As Integer


    '_FirstRUframe = CInt(Int(ImportedTrajectory(0).Time * Me.FrameRate)) + 1
    '_LastRUframe = CInt(Int(ImportedTrajectory(ImportedTrajectory.Count - 1).Time * Me.FrameRate))

    '_PointsToUse = CInt(ImportedTrajectory.Count / (_LastRUframe - _FirstRUframe) / 2)
    'If _PointsToUse < 1 Then _PointsToUse = 1
    '_PointsToUse = 0

    'If _FirstRUframe < 0 Then _FirstRUframe = 0
    'If _LastRUframe > (Me.FrameCount - 1) Then _LastRUframe = (Me.FrameCount - 1)
    'If (_FirstRUframe > (Me.FrameCount - 1)) Or (_LastRUframe < 0) Or (_FirstRUframe = _LastRUframe) Then Return Nothing

    ''_NewRoadUser = New clsRoadUser(_FirstRUframe, ImportedRUType)
    '' _NewRoadUser.CheckDimensions()
    ''  _NewRoadUser.TimeLine = Me.TimeLine


    'For _NewFrame = _FirstRUframe To _LastRUframe
    '  _dTmin = Double.MaxValue
    '  For _i = 0 To ImportedTrajectory.Count - 1
    '    _dT = Math.Abs(ImportedTrajectory(_i).Time - _NewFrame / Me.FrameRate)
    '    If _dT < _dTmin Then
    '      _iMin = _i
    '      _dTmin = _dT
    '    End If
    '  Next

    '  _i1 = _iMin - _PointsToUse
    '  _i2 = _iMin + _PointsToUse
    '  If _i1 < 0 Then _i1 = 0
    '  If _i2 > (ImportedTrajectory.Count - 1) Then _i2 = ImportedTrajectory.Count - 1

    '  _DataPoints = New List(Of clsTPoint)
    '  For _i = _i1 To _i2
    '    _DataPoints.Add(ImportedTrajectory(_i))
    '  Next

    '  _NewPoint = InterpolateLinear(Me.TimeLine(_NewFrame).Time, _DataPoints)
    '  _NewDataPoint = New clsDataPoint(_NewPoint.X, _NewPoint.Y, 1, 0, , , , _NewPoint.X, _NewPoint.Y, 1, 0)
    '  '  _NewRoadUser.AddPoint(_NewDataPoint)
    'Next

    'Return _NewRoadUser


    '' If _NewRoadUser IsNot Nothing Then _NewRoadUser.SmoothTrajectory()





    ''   For _ImportedFrame = 0 To ImportedTrajectory.Count - 1
    ''_ImportedPoint = ImportedTrajectory(_ImportedFrame)

    ''_NewFrame1 = CInt(Int(_ImportedPoint.Time * Me.FrameRate))
    ''    _NewFrame2 = _NewFrame1 + 1

    ''If IsBetween(_NewFrame1, 0, Me.FrameCount - 1) Then
    ''  If _NewRoadUser Is Nothing Then
    ''_NewRUFirstFrame = _NewFrame1
    ''_NewRoadUser = New clsRoadUser(_NewRUFirstFrame, ImportedRUType)
    ''_NewRoadUser.CheckDimensions()
    ''End If
    ''   If _DataPointsList.Count <= (_NewFrame1 - _NewFirstFrame) Then
    ''Do
    ''_DataPoints = New List(Of clsXPoint)
    ''   _DataPointsList.Add(_DataPoints)
    ''  Loop Until _DataPointsList.Count > (_NewFrame1 - _NewFirstFrame)
    ''  End If
    ''   _DataPointsList(_NewFrame1 - _NewFirstFrame).Add(_ImportedPoint)
    ''    End If

    ''   If IsBetween(_NewFrame2, 0, Me.FrameCount - 1) Then
    '' If _NewRoadUser Is Nothing Then
    ''_NewRUFirstFrame = _NewFrame2
    ''_NewRoadUser = New clsRoadUser(_NewRUFirstFrame, ImportedRUType)
    ''_NewRoadUser.CheckDimensions()
    '' End If
    ''If _DataPointsList.Count <= (_NewFrame2 - _NewFirstFrame) Then
    ''Do
    ''_DataPoints = New List(Of clsXPoint)
    ''   _DataPointsList.Add(_DataPoints)
    ''    Loop Until _DataPointsList.Count > (_NewFrame2 - _NewRUFirstFrame)
    ''    End If
    ''   _DataPointsList(_NewFrame2 - _NewRUFirstFrame).Add(_ImportedPoint)
    ''    End If
    ''    Next

    ''   For _NewFrame = _NewRUFirstFrame To _NewRUFirstFrame + _DataPointsList.Count - 2
    ''_DataPoints = _DataPointsList(_NewFrame - _NewRUFirstFrame)
    ''   _NewPoint = InterpolateLinear(Me.TimeLine(_NewFrame).Time, _DataPoints)
    ''  _NewDataPoint = New clsDataPoint(, , , , , , , , _NewPoint.X, _NewPoint.Y, 1, 0)
    ''  _NewRoadUser.AddPoint(_NewDataPoint)
    ''  Next

    ''  If _NewRoadUser IsNot Nothing Then _NewRoadUser.SmoothTrajectory()
    Return Nothing
  End Function



  'Private Function VideoFileFrameIndex(ByVal VideoFile As clsVideoFile, ByVal RecordFrame As Integer) As Integer
  '  Dim _FirstGuess, _SecondGuess As Integer
  '  Dim _BestGuessFrame1 As Integer, _BestGuessFrame2 As Integer
  '  Dim _Iteration As Integer
  '  Dim _TimeToDisplay As Double
  '  Dim _dT1 As Double, _dT2 As Double

  '  _TimeToDisplay = RecordFrame / Me.FrameRate

  '  _FirstGuess = CInt((_TimeToDisplay - (VideoFile.GetTimeStamp(0) + VideoFile.StartTime)) * VideoFile.FrameRate)
  '  If _FirstGuess < 0 Then _FirstGuess = 0
  '  If _FirstGuess > (VideoFile.FrameCount - 1) Then _FirstGuess = VideoFile.FrameCount - 1

  '  _SecondGuess = CInt(_FirstGuess + (_TimeToDisplay - (VideoFile.GetTimeStamp(_FirstGuess) + VideoFile.StartTime)) * VideoFile.FrameRate)
  '  If _SecondGuess < 0 Then _SecondGuess = 0
  '  If _SecondGuess > (VideoFile.FrameCount - 1) Then _SecondGuess = VideoFile.FrameCount - 1

  '  _BestGuessFrame1 = _SecondGuess
  '  _dT1 = _TimeToDisplay - (VideoFile.GetTimeStamp(_BestGuessFrame1) + VideoFile.StartTime)
  '  _Iteration = Math.Sign(_dT1)

  '  Do While (_dT1 * _Iteration > 0) And IsBetween(_BestGuessFrame1, 1, VideoFile.FrameCount - 2)
  '    _BestGuessFrame1 = _BestGuessFrame1 + _Iteration
  '    _dT1 = _TimeToDisplay - (VideoFile.GetTimeStamp(_BestGuessFrame1) + VideoFile.StartTime)
  '  Loop

  '  _BestGuessFrame2 = _BestGuessFrame1 - _Iteration
  '  If IsBetween(_BestGuessFrame2, 0, VideoFile.FrameCount - 1) Then
  '    _dT2 = _TimeToDisplay - (VideoFile.StartTime + VideoFile.GetTimeStamp(_BestGuessFrame2))
  '  Else
  '    _BestGuessFrame2 = _BestGuessFrame1
  '    _dT2 = _dT1
  '  End If

  '  If Math.Abs(_dT1) < Math.Abs(_dT2) Then
  '    Return _BestGuessFrame1
  '  Else
  '    Return _BestGuessFrame2
  '  End If

  'End Function



#End Region


End Class
