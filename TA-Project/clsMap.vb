Public Class clsMap
  Public Available As Boolean
  Public Map As String
  Public Size As Size
  Public Scale As Double
  Public X As Double
  Public Y As Double
  Public Dx As Double
  Public Dy As Double
  Public GroundPlanePoints As List(Of clsGeomPoint3D)
  Public Alpha As Double
  Public Visible As Boolean
  Public Comment As String

  Private MyRecord As clsRecord



  Public Sub New(ByVal ParentRecord As clsRecord)
    MyRecord = ParentRecord
    Me.Available = False
    Me.Map = ""
    Me.Size = Nothing
    Me.Scale = NoValue
    Me.X = NoValue
    Me.Y = NoValue
    Me.Dx = NoValue
    Me.Dy = NoValue
    Me.Visible = False
    Me.Comment = ""
    Me.GroundPlanePoints = New List(Of clsGeomPoint3D)
  End Sub



  Public Function CreateClone(ByVal ParentRecord As clsRecord) As clsMap
    Dim _NewMap As clsMap

    _NewMap = New clsMap(ParentRecord)
    With _NewMap
      .Available = Me.Available
      .Map = Me.Map
      .Size = New Size(Me.Size.Width, Me.Size.Height)
      .Scale = Me.Scale
      .X = Me.X
      .Y = Me.Y
      .Dx = Me.Dx
      .Dy = Me.Dy
      .Visible = Me.Visible
      .Comment = Me.Comment
    End With

    Return _NewMap
  End Function



  Public ReadOnly Property Bitmap As Bitmap
    Get
      Dim _g As Graphics
      Dim _NewBitmap, _OldBitmap As Bitmap

      If Me.Available Then
        Try
          _OldBitmap = New Bitmap(MyRecord.MapDirectory & Me.Map)
          _NewBitmap = New Bitmap(_OldBitmap.Width, _OldBitmap.Height)
          _g = Graphics.FromImage(_NewBitmap)
          _g.DrawImage(_OldBitmap, 0, 0, Me.Size.Width, Me.Size.Height)
          _OldBitmap.Dispose()
          _g.Dispose()

          Return _NewBitmap
        Catch ex As Exception
          Return EmptyBitmap(Me.Size)
        End Try
      Else
        Return Nothing
      End If
    End Get
  End Property



  Public Function TransformWorldToImage(ByVal WorldX As Double, ByVal WorldY As Double, Optional WorldZ As Double = 0) As Point
    Dim _Xm, _Ym As Integer

    If Me.Available Then
      _Xm = CInt(((WorldX - Me.X) * Me.Dx + (WorldY - Me.Y) * Me.Dy) * Me.Scale)
      _Ym = CInt((-(WorldX - Me.X) * Me.Dy + (WorldY - Me.Y) * Me.Dx) * Me.Scale)

      Return New Point(_Xm, _Ym)
    Else
      Return Nothing
    End If
  End Function



  Public Function TransformImageToWorld(ByVal ImageX As Integer, ByVal ImageY As Integer) As clsGeomPoint3D
    Dim _Xw, _Yw, _Zw As Double

    If Me.Available Then
      _Xw = Me.X + (ImageX * Me.Dx - ImageY * Me.Dy) / Me.Scale
      _Yw = Me.Y + (ImageX * Me.Dy + ImageY * Me.Dx) / Me.Scale
      _Zw = Me.EstimateHeight(_Xw, _Yw)

      Return New clsGeomPoint3D(_Xw, _Yw, _Zw)
    Else
      Return Nothing
    End If
  End Function

  Public Function OnePixelError(ByVal ImageX As Integer, ByVal ImageY As Integer) As Double
    Dim _Point1, _Point2 As clsGeomPoint3D

    _Point1 = Me.TransformImageToWorld(ImageX, ImageY)
    _Point2 = Me.TransformImageToWorld(ImageX + 1, ImageY + 1)

    If IsNothing(_Point1) Or IsNothing(_Point2) Then
      Return NoValue
    Else
      Return _Point1.DistanceTo(_Point2)
    End If
  End Function

  Public ReadOnly Property MiddlePoint As clsGeomPoint
    Get
      If Me.Available Then
        Return Me.TransformImageToWorld(CInt(Me.Size.Width / 2), CInt(Me.Size.Height / 2))
      Else
        Return Nothing
      End If
    End Get
  End Property



  Public Function IsPointWithin(ByVal Position As clsGeomPoint3D) As Boolean
    Dim _Point As Point

    If Me.Available Then
      _Point = Me.TransformWorldToImage(Position.X, Position.Y, Position.Z)
      If IsBetween(_Point.X, 0, Me.Size.Width - 1) And IsBetween(_Point.Y, 0, Me.Size.Height - 1) Then Return True
    End If

    Return False
  End Function




  Public Function ImportMapParameters(FilePath As String) As Boolean
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _Values() As String
    Dim _X, _Y, _Z As Double


    Me.GroundPlanePoints.Clear()

    Try
      _FileIndex = FreeFile()
      FileOpen(_FileIndex, FilePath, OpenMode.Input)

      _Line = LineInput(_FileIndex) : Me.X = Double.Parse(_Line.Substring(9).Trim, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex) : Me.Y = Double.Parse(_Line.Substring(9).Trim, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex) : Me.Dx = Double.Parse(_Line.Substring(9).Trim, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex) : Me.Dy = Double.Parse(_Line.Substring(9).Trim, CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex) : Me.Scale = Double.Parse(_Line.Substring(9).Trim, CultureInfo.InvariantCulture)

      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)

      Me.Alpha = Double.Parse(_Line.Substring(9), CultureInfo.InvariantCulture)
      _Line = LineInput(_FileIndex)
      _Line = LineInput(_FileIndex)
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        _Values = Strings.Split(_Line, ";")
        _X = Double.Parse(_Values(0), CultureInfo.InvariantCulture)
        _Y = Double.Parse(_Values(1), CultureInfo.InvariantCulture)
        _Z = Double.Parse(_Values(2), CultureInfo.InvariantCulture)
        Me.GroundPlanePoints.Add(New clsGeomPoint3D(_X, _Y, _Z))
      Loop
      FileClose(_FileIndex)

      Return True
    Catch
      Return False
    End Try

  End Function


  Public Function EstimateHeight(ByVal x As Double, ByVal Y As Double) As Double
    Dim _Height As Double
    Dim _Distance As Double
    Dim _Weight As Double
    Dim _SumOfWeights As Double
    Dim _i As Integer


    _SumOfWeights = 0
    _Height = 0

    For _i = 0 To Me.GroundPlanePoints.Count - 1
      _Distance = Math.Sqrt((x - Me.GroundPlanePoints(_i).X) ^ 2 + (Y - Me.GroundPlanePoints(_i).Y) ^ 2)

      _Weight = Math.Exp(-Me.Alpha * _Distance)
      _SumOfWeights = _SumOfWeights + _Weight
      _Height = _Height + Me.GroundPlanePoints(_i).Z * _Weight
    Next _i
    If _SumOfWeights > 0 Then
      _Height = _Height / _SumOfWeights 
    Else
      _Height = 0
    End If

    Return _Height
  End Function




  Public Sub ExportCalibration(ByVal FilePath As String)
    Dim _FileIndex As Integer

    _FileIndex = FreeFile()
    FileOpen(_FileIndex, FilePath, OpenMode.Output)

    PrintLine(_FileIndex, "X0:", TAB(10), Me.X.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Y0:", TAB(10), Me.Y.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Dx:", TAB(10), Me.Dx.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Dy:", TAB(10), Me.Dy.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex, "Scale:", TAB(10), Me.Scale.ToString(CultureInfo.InvariantCulture))
    PrintLine(_FileIndex)
    PrintLine(_FileIndex)
    PrintLine(_FileIndex, "***Ground plane***")
    PrintLine(_FileIndex)
    PrintLine(_FileIndex, "Alpha: ", TAB(10), Me.Alpha.ToString("0.000", CultureInfo.InvariantCulture))
    PrintLine(_FileIndex)
    PrintLine(_FileIndex, "X,m;Y,m;Z,m")
    For _i = 0 To Me.GroundPlanePoints.Count - 1
      PrintLine(_FileIndex, Me.GroundPlanePoints(_i).X.ToString("0.000", CultureInfo.InvariantCulture) & ";" &
                            Me.GroundPlanePoints(_i).Y.ToString("0.000", CultureInfo.InvariantCulture) & ";" &
                            Me.GroundPlanePoints(_i).Z.ToString("0.000", CultureInfo.InvariantCulture))
    Next _i
    FileClose(_FileIndex)

  End Sub
End Class