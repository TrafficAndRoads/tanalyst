﻿Imports AviFile
Imports System.Drawing

Public Class AviReader
  Private MyAviManager As AviManager
  Private MyVideoStream As VideoStream
  Private MyFrameCount As Integer = 0

  Public Sub New(ByVal sFile As String)
    File.Exists(sFile)
    MyAviManager = New AviManager(sFile, True)
    MyVideoStream = MyAviManager.GetVideoStream()
    MyVideoStream.GetFrameOpen()
    MyFrameCount = MyVideoStream.CountFrames
  End Sub

  Public Sub Close()
    MyVideoStream.GetFrameClose()
    MyAviManager.Close()
  End Sub

  Public Function GetFrame(ByVal n As Integer) As Bitmap
    If n < 0 Then
      Return MyVideoStream.GetBitmap(0)
    ElseIf n >= MyFrameCount Then
      Return MyVideoStream.GetBitmap(MyFrameCount - 1)
    Else
      Return MyVideoStream.GetBitmap(n)
    End If
  End Function

  Public Function FrameCount() As Integer
    Return MyFrameCount
  End Function

  Public Function FrameSize() As Size
    Return New Size(MyVideoStream.Width, MyVideoStream.Height)
  End Function

  Public Function FrameRate() As Double
    Return MyVideoStream.FrameRate
  End Function

End Class
