﻿Public Class clsRoadUser
  Inherits clsRoadUserModel


  Public Directions As List(Of clsDirection)
  Public Key As KeyRoadUser

  Private MyFirstFrame As Integer
  Private MyTrajectory As List(Of clsDataPoint)
  '  Private MyProjectedTrajectory As List(Of clsDPoint)
  Private MyParentRecord As clsRecord

  Public Enum KeyRoadUser
    NotKey = 0
    Key1 = 1
    Key2 = 2
  End Enum

#Region "Properties"


  Public Property FirstFrame() As Integer
    Set(ByVal value As Integer)
      MyFirstFrame = value
    End Set
    Get
      FirstFrame = MyFirstFrame
    End Get
  End Property


  Public ReadOnly Property LastFrame() As Integer
    Get
      LastFrame = MyFirstFrame + MyTrajectory.Count - 1
    End Get
  End Property


  Public ReadOnly Property DataPoint(ByVal Frame As Integer) As clsDataPoint
    Get
      If IsBetween(Frame, MyFirstFrame, MyFirstFrame + MyTrajectory.Count - 1) Then
        DataPoint = MyTrajectory(Frame - MyFirstFrame)
      Else
        DataPoint = Nothing
      End If
    End Get
  End Property


  Public ReadOnly Property DataPointsCount() As Integer
    Get
      DataPointsCount = MyTrajectory.Count
    End Get
  End Property


  Public ReadOnly Property TrajectoryLength As Double
    Get
      TrajectoryLength = 0
      For i = 1 To Me.DataPointsCount - 1
        TrajectoryLength = TrajectoryLength + MyTrajectory(i).DistanceTo(MyTrajectory(i - 1))
      Next
    End Get
  End Property


  Public ReadOnly Property ParentRecord As clsRecord
    Get
      Return MyParentRecord
    End Get
  End Property


  ' Public ReadOnly Property RoadUserTypes As List(Of clsRoadUserType)
  '   Get
  '     Return MyParentRecord.RoadUserTypes
  '   End Get
  ' End Property

#End Region



#Region "Public subs"

  Public Sub New(ByVal ParentRecord As clsRecord,
                 ByVal FirstFrame As Integer,
                 ByVal Type As clsRoadUserType,
                 Optional ByVal Length As Double = NoValue,
                 Optional ByVal Width As Double = NoValue,
                 Optional ByVal Height As Double = NoValue,
                 Optional ByVal Weight As Integer = NoValue)

    MyBase.New(Type, Length, Width, Height, Weight)

    MyParentRecord = ParentRecord

    MyFirstFrame = FirstFrame
    Me.Key = KeyRoadUser.NotKey
    Me.Directions = New List(Of clsDirection)
    MyTrajectory = New List(Of clsDataPoint)
  End Sub

  Public Function CreateCopy(ParentRecord As clsRecord) As clsRoadUser
    Dim _NewRoadUser As clsRoadUser

    _NewRoadUser = New clsRoadUser(ParentRecord, MyFirstFrame, Me.Type, Me.Length, Me.Width, Me.Height)
    With _NewRoadUser
      .Key = KeyRoadUser.NotKey
      For Each _DataPoint In MyTrajectory
        .AddPoint(_DataPoint.CreateCopy)
      Next
    End With

    Return _NewRoadUser
  End Function


  Public Sub AddStartPoint(ByVal NewPoint As clsDataPoint)
    MyTrajectory.Insert(0, NewPoint)
    MyFirstFrame = MyFirstFrame - 1
    Me.ParentRecord.TimeLine(MyFirstFrame).RoadUsers.Add(Me)
    Me.ParentRecord.RoadUsersChanged = True
  End Sub

  Public Sub AddPoint(ByVal NewPoint As clsDataPoint)
    MyTrajectory.Add(NewPoint)
    MyParentRecord.TimeLine(Me.LastFrame).RoadUsers.Add(Me)
    MyParentRecord.RoadUsersChanged = True
  End Sub

  Public Sub RemoveFirstPoint()
    If MyTrajectory.Count > 1 Then
      MyTrajectory.RemoveAt(0)
      MyParentRecord.TimeLine(MyFirstFrame).RoadUsers.Remove(Me)
      MyFirstFrame = MyFirstFrame + 1
      MyParentRecord.RoadUsersChanged = True
    End If
  End Sub

  Public Sub RemoveLastPoint()
    If MyTrajectory.Count > 1 Then
      MyParentRecord.TimeLine(LastFrame).RoadUsers.Remove(Me)
      MyTrajectory.RemoveAt(MyTrajectory.Count - 1)
      MyParentRecord.RoadUsersChanged = True
    End If
  End Sub

  Public Sub SmoothTrajectory(Optional ByVal UseExternalProgram As Boolean = False,
                              Optional ByVal SmoothingFactorTrajectoy As Double = 0.3,
                              Optional ByVal SmoothingFactorSpeed As Double = 0.3,
                              Optional ByVal SmoothingFactorAcceleration As Double = 0.3,
                              Optional ByVal SmoothingFactorDirection As Double = 3)
    Dim _FileIndex As Integer
    Dim _FilePath1, _FilePath2 As String
    Dim _Line As String
    Dim _Process As Process
    Dim _Values() As String
    Dim _X, _Y, _V, _A, _J, _dX, _dY As Double
    Dim _i, _k As Integer
    Dim _Xvalues, _Yvalues, _Vvalues, _Avalues, _Jvalues As New List(Of Double)
    Dim _PointCountTrajectory, _PointCountSpeed, _PointCountAcceleration As Integer
    Dim _dT, _dL As Double



    If Me.DataPointsCount > 1 Then
      _dT = MyParentRecord.GetLocalTime(MyFirstFrame + 1) - MyParentRecord.GetLocalTime(MyFirstFrame)

      Select Case UseExternalProgram
        Case False
          _PointCountTrajectory = CInt(SmoothingFactorTrajectoy / _dT)
          _PointCountSpeed = CInt(SmoothingFactorSpeed / _dT)
          _PointCountAcceleration = CInt(SmoothingFactorAcceleration / _dT)

          'interpolate missing data
          For _Frame = Me.FirstFrame To Me.LastFrame
            _i = 0 : _k = 0
            Do Until Me.DataPoint(_Frame + _i).OriginalXYExists
              _i = _i + 1
            Loop
            Do Until Me.DataPoint(_Frame - _k).OriginalXYExists
              _k = _k + 1
            Loop
            _Xvalues.Add(Interpolate(_Frame, _Frame - _k, Me.DataPoint(_Frame - _k).Xpxl, _Frame + _i, Me.DataPoint(_Frame + _i).Xpxl))
            _Yvalues.Add(Interpolate(_Frame, _Frame - _k, Me.DataPoint(_Frame - _k).Ypxl, _Frame + _i, Me.DataPoint(_Frame + _i).Ypxl))
          Next _Frame

          'trajectory
          _Xvalues = SmoothMovingAverage(_Xvalues, _PointCountTrajectory)
          _Yvalues = SmoothMovingAverage(_Yvalues, _PointCountTrajectory)

          For _Frame = Me.FirstFrame To Me.LastFrame
            Me.DataPoint(_Frame).X = _Xvalues(_Frame - MyFirstFrame)
            Me.DataPoint(_Frame).Y = _Yvalues(_Frame - MyFirstFrame)
          Next


          'speed
          For _Frame = Me.FirstFrame To Me.LastFrame - 1
            _Vvalues.Add(Math.Sqrt((Me.DataPoint(_Frame + 1).X - Me.DataPoint(_Frame).X) ^ 2 + (Me.DataPoint(_Frame + 1).Y - Me.DataPoint(_Frame).Y) ^ 2) / _dT)
          Next _Frame
          _Vvalues.Add(_Vvalues(_Vvalues.Count - 1))

          _Vvalues = SmoothMovingAverage(_Vvalues, _PointCountSpeed)

          For _Frame = Me.FirstFrame To Me.LastFrame
            Me.DataPoint(_Frame).V = _Vvalues(_Frame - MyFirstFrame)
          Next

          'acceleration
          For _Frame = Me.FirstFrame To Me.LastFrame - 2
            _Avalues.Add((Me.DataPoint(_Frame + 1).V - Me.DataPoint(_Frame).V) / _dT)
          Next _Frame
          _Avalues.Add(_Avalues(_Avalues.Count - 1))
          _Avalues.Add(_Avalues(_Avalues.Count - 1))

          _Avalues = SmoothMovingAverage(_Avalues, _PointCountAcceleration)

          For _Frame = Me.FirstFrame To Me.LastFrame
            Me.DataPoint(_Frame).A = _Avalues(_Frame - MyFirstFrame)
          Next



          SmoothingFactorDirection = Me.Length
          For _Frame = Me.LastFrame To Me.FirstFrame Step -1
            _i = 0 : _k = 0
            '  Do Until (Math.Sqrt((Me.DataPoint(_Frame).X - Me.DataPoint(_Frame + _i).X) ^ 2 + (Me.DataPoint(_Frame).Y - Me.DataPoint(_Frame + _i).Y) ^ 2) > 0.15 * SmoothingFactorDirection) Or ((_Frame + _i) = Me.LastFrame)
            '   _i = _i + 1
            '   Loop
            Do Until (Math.Sqrt((Me.DataPoint(_Frame).X - Me.DataPoint(_Frame - _k).X) ^ 2 + (Me.DataPoint(_Frame).Y - Me.DataPoint(_Frame - _k).Y) ^ 2) > 0.5 * SmoothingFactorDirection) Or ((_Frame - _k) = MyFirstFrame)
              _k = _k + 1
            Loop
            _i = CInt(0.3 * _k)
            _dL = Math.Sqrt((Me.DataPoint(_Frame - _k).X - Me.DataPoint(_Frame - _i).X) ^ 2 + (Me.DataPoint(_Frame - _k).Y - Me.DataPoint(_Frame - _i).Y) ^ 2)
            ' _dL = Math.Sqrt((Me.DataPoint(_Frame - _k).X - Me.DataPoint(_Frame + _i).X) ^ 2 + (Me.DataPoint(_Frame - _k).Y - Me.DataPoint(_Frame + _i).Y) ^ 2)
            If _dL > CompError Then
              '  MsgBox(_dL)
              'Me.DataPoint(_Frame).DxDy = New clsDxDy((Me.DataPoint(_Frame + _i).X - Me.DataPoint(_Frame - _k).X) / _dL, (Me.DataPoint(_Frame + _i).Y - Me.DataPoint(_Frame - _k).Y) / _dL)
              Me.DataPoint(_Frame).DxDy = New clsDxDy((Me.DataPoint(_Frame - _i).X - Me.DataPoint(_Frame - _k).X) / _dL, (Me.DataPoint(_Frame - _i).Y - Me.DataPoint(_Frame - _k).Y) / _dL)
            Else
              Me.DataPoint(_Frame).DxDy = New clsDxDy(Me.DataPoint(_Frame + 1).DxDy.Dx, Me.DataPoint(_Frame + 1).DxDy.Dy)
            End If
          Next _Frame


          'For _i = 0 To MyT.Count - 1
          '  _j1 = 0
          '  Do Until (Math.Sqrt((MyX(_i) - MyX(_i - _j1)) ^ 2 + (MyY(_i) - MyY(_i - _j1)) ^ 2) > SmoothingFactorDirection / 2) Or ((_i - _j1) = 0)
          '    _j1 = _j1 + 1
          '  Loop

          '  _j2 = 0
          '  Do Until (Math.Sqrt((MyX(_i) - MyX(_i + _j2)) ^ 2 + (MyY(_i) - MyY(_i + _j2)) ^ 2) > SmoothingFactorDirection / 2) Or ((_i + _j2) = (MyT.Length - 1))
          '    _j2 = _j2 + 1
          '  Loop

          '  _dL = Math.Sqrt((MyX(_i - _j1) - MyX(_i + _j2)) ^ 2 + (MyY(_i - _j1) - MyY(_i + _j2)) ^ 2)
          '  If _dL > CompError Then
          '    MyDx(_i) = (MyX(_i + _j2) - MyX(_i - _j1)) / _dL
          '    MyDy(_i) = (MyY(_i + _j2) - MyY(_i - _j1)) / _dL
          '  Else
          '    MyDx(_i) = 1
          '    MyDy(_i) = 0
          '  End If
          'Next _i












        Case True
          _FileIndex = FreeFile()
          _FilePath1 = MyParentRecord.TrajectoryDirectory & "Temp1.txt"
          _FilePath2 = MyParentRecord.TrajectoryDirectory & "Temp2.txt"
          FileOpen(_FileIndex, _FilePath1, OpenMode.Output)

          For _Frame = Me.FirstFrame To Me.LastFrame
            If Me.DataPoint(_Frame).OriginalXYExists Then
              _Line = Format(MyParentRecord.GetLocalTime(_Frame), "0.000").Replace(",", ".") & ";" & Format(Me.DataPoint(_Frame).Xpxl, "0.000").Replace(",", ".") & ";" & Format(Me.DataPoint(_Frame).Ypxl, "0.000").Replace(",", ".")
            Else
              _Line = Format(MyParentRecord.GetLocalTime(_Frame), "0.000").Replace(",", ".") & ";NoValue;NoValue"
            End If
            PrintLine(_FileIndex, _Line)
          Next
          FileClose(_FileIndex)

          _Process = New Process
          _Process.StartInfo.FileName = "smoother.exe"
          _Process.StartInfo.Arguments = _FilePath1 & "," & _FilePath2 & "," & SmoothingFactorTrajectoy & "," & SmoothingFactorSpeed & "," & SmoothingFactorAcceleration & "," & SmoothingFactorDirection
          _Process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
          _Process.Start()
          _Process.WaitForExit()
          'File.Delete(_FilePath1)

          _FileIndex = FreeFile()
          If File.Exists(_FilePath2) Then
            FileOpen(_FileIndex, _FilePath2, OpenMode.Input)
            For _Frame = Me.FirstFrame To Me.LastFrame
              _Line = LineInput(_FileIndex)
              _Values = _Line.Split(CChar(";"))

              If Double.TryParse(_Values(0).Replace(",", "."), _X) Then Me.DataPoint(_Frame).X = _X
              If Double.TryParse(_Values(1).Replace(",", "."), _Y) Then Me.DataPoint(_Frame).Y = _Y
              If Double.TryParse(_Values(2).Replace(",", "."), _V) Then Me.DataPoint(_Frame).V = _V
              If Double.TryParse(_Values(3).Replace(",", "."), _A) Then Me.DataPoint(_Frame).A = _A
              If Double.TryParse(_Values(4).Replace(",", "."), _J) Then Me.DataPoint(_Frame).J = _J
              If Double.TryParse(_Values(5).Replace(",", "."), _dX) And Double.TryParse(_Values(6).Replace(",", "."), _dY) Then Me.DataPoint(_Frame).DxDy = New clsDxDy(_dX, _dY)
            Next
            FileClose(_FileIndex)
            'File.Delete(_FilePath2)
          End If
      End Select





      MyParentRecord.RoadUsersChanged = True

    Else
      'Has to be commented when running "multiple"

      'MsgBox("Too few points to calculate the trajectory!")

    End If


    'Utjämningsparametrar, default values 3 and 0.4 sec.
    'Const _T_POSITION As Double = 3
    'Const _T_SPEED As Double = 0.4


    'Dim _TimeInput(), _TimeOutput() As Double
    'Dim _XInput(), _YInput() As Double
    'Dim _i, _j As Integer
    'Dim _OriginalTrackPoints As List(Of TrackPoint)
    'Dim _SmoothedTrackPoints As List(Of TrackPoint)
    'Dim _TrajectorySmooth As TrajectorySmooth




    'ReDim _TimeInput(Me.LastFrame - MyFirstFrame)
    'ReDim _TimeOutput(Me.LastFrame - MyFirstFrame)
    'ReDim _XInput(Me.LastFrame - MyFirstFrame)
    'ReDim _YInput(Me.LastFrame - MyFirstFrame)


    'If MyTrajectory.Count > 5 Then


    '  Fill in missing original XY
    '  For _Frame = MyFirstFrame To LastFrame
    '    _TimeInput(_Frame - MyFirstFrame) = ParentRecord.GetLocalTime(_Frame)
    '    _TimeOutput(_Frame - MyFirstFrame) = ParentRecord.GetLocalTime(_Frame)
    '    If Me.DataPoint(_Frame).OriginalXYExists Then
    '      _XInput(_Frame - MyFirstFrame) = Me.DataPoint(_Frame).Xpxl
    '      _YInput(_Frame - MyFirstFrame) = Me.DataPoint(_Frame).Ypxl
    '    Else
    '      _i = 1
    '      Do Until Me.DataPoint(_Frame + _i).OriginalXYExists
    '        _i = _i + 1
    '      Loop
    '      _j = 1
    '      Do Until Me.DataPoint(_Frame - _j).OriginalXYExists
    '        _j = _j + 1
    '      Loop
    '      _XInput(_Frame - MyFirstFrame) = Interpolate(_Frame, _Frame - _j, Me.DataPoint(_Frame - _j).Xpxl, _Frame + _i, Me.DataPoint(_Frame + _i).Xpxl)
    '      _YInput(_Frame - MyFirstFrame) = Interpolate(_Frame, _Frame - _j, Me.DataPoint(_Frame - _j).Ypxl, _Frame + _i, Me.DataPoint(_Frame + _i).Ypxl)
    '    End If
    '  Next _Frame


    '  _OriginalTrackPoints = New List(Of TrackPoint)
    '  For _Frame = MyFirstFrame To LastFrame
    '    _OriginalTrackPoints.Add(New TrackPoint(_XInput(_Frame - MyFirstFrame), _YInput(_Frame - MyFirstFrame), _TimeInput(_Frame - MyFirstFrame)))
    '  Next

    '  _TrajectorySmooth = New TrajectorySmooth(_OriginalTrackPoints)
    '  _TrajectorySmooth.Process(_T_POSITION, _T_SPEED)

    '  _SmoothedTrackPoints = _TrajectorySmooth.GetResult

    '  For _Frame = MyFirstFrame To LastFrame
    '    Me.DataPoint(_Frame).X = _SmoothedTrackPoints(_Frame - MyFirstFrame).X
    '    Me.DataPoint(_Frame).Y = _SmoothedTrackPoints(_Frame - MyFirstFrame).Y
    '    Me.DataPoint(_Frame).V = Math.Sqrt(_SmoothedTrackPoints(_Frame - MyFirstFrame).VX ^ 2 + _SmoothedTrackPoints(_Frame - MyFirstFrame).VY ^ 2)
    '    Me.DataPoint(_Frame).A = NoValue
    '    Me.DataPoint(_Frame).J = NoValue
    '  Next _Frame


    '  Calculate(Dx, Dy)
    '  Dim _dL, _dLmin As Double
    '  Dim _p, _p1, _p2 As clsGeomPoint


    '  Calculate Directions
    '  _dLmin = 0.5
    '  For _i = 0 To MyTrajectory.Count - 1
    '    _p = New clsGeomPoint(MyTrajectory(_i).X, MyTrajectory(_i).Y)
    '    _j = -1
    '    Do
    '      _j = _j + 1
    '      _p1 = New clsGeomPoint(MyTrajectory(_i - _j).X, MyTrajectory(_i - _j).Y)
    '    Loop Until (_p.DistanceTo(_p1) > _dLmin / 2) Or ((_i - _j) = 0)

    '    _j = -1
    '    Do
    '      _j = _j + 1
    '      _p2 = New clsGeomPoint(MyTrajectory(_i + _j).X, MyTrajectory(_i + _j).Y)
    '      _dL = _p1.DistanceTo(_p2)
    '    Loop Until (_dL > _dLmin) Or ((_i + _j) = MyTrajectory.Count - 1)


    '    If Not IsZero(_dL) Then
    '      MyTrajectory(_i).DxDy = New clsDxDy((_p2.X - _p1.X) / _dL, (_p2.Y - _p1.Y) / _dL)
    '    Else
    '      MyTrajectory(_i).DxDy = New clsDxDy(1, 0)
    '    End If
    '  Next _i

    '  MyParentRecord.RoadUsersChanged = True
    'Else
    '  MsgBox("Too few points to get areliable trajectory data")
    'End If

  End Sub

  Public Sub SmoothTrajectory_(Optional ByVal UseExternalProgram As Boolean = False,
                              Optional ByVal SmoothingFactorTrajectoy As Double = 0.4,
                              Optional ByVal SmoothingFactorSpeed As Double = 0.3,
                              Optional ByVal SmoothingFactorAcceleration As Double = 4,
                              Optional ByVal SmoothingFactorDirection As Double = 3)
    '   Dim _FileIndex As Integer
    '   Dim _FilePath1, _FilePath2 As String
    '  Dim _Line As String
    '  Dim _Process As Process
    '  Dim _Values() As String
    '  Dim _X, _Y, _V, _A, _Jrk, _dX, _dY As Double
    '  Dim _i, _k, _j As Integer
    Dim _Xvalues, _Yvalues, _Vvalues, _Avalues, _Jvalues As New List(Of Double)
    '   Dim _PointCountTrajectory, _PointCountSpeed, _PointCountAcceleration As Integer
    '   Dim _dT, _dL As Double




    ' Utjämningsparametrar, default values 3 and 0.4 sec.
    Const _T_POSITION As Double = 3
    Const _T_SPEED As Double = 0.4


    ' Dim _TimeInput() As Double
    Dim _TimeOutput() As Double
    '  Dim _XInput(), _YInput() As Double
    ' Dim _j As Integer
    Dim _OriginalTrackPoints As List(Of TrackPoint)
    Dim _SmoothedTrackPoints As List(Of TrackPoint)
    Dim _TrajectorySmooth As TrajectorySmooth






    If MyTrajectory.Count > 5 Then

      _OriginalTrackPoints = New List(Of TrackPoint)
      For _Frame = MyFirstFrame To LastFrame
        If Me.DataPoint(_Frame).OriginalXYExists Then
          _OriginalTrackPoints.Add(New TrackPoint(Me.DataPoint(_Frame).Xpxl, Me.DataPoint(_Frame).Ypxl, ParentRecord.GetLocalTime(_Frame)))
        End If

        ReDim _TimeOutput(_Frame - MyFirstFrame)
        _TimeOutput(_Frame - MyFirstFrame) = ParentRecord.GetLocalTime(_Frame)
      Next


      _TrajectorySmooth = New TrajectorySmooth(_OriginalTrackPoints)
      _TrajectorySmooth.Process(_T_POSITION, _T_SPEED, _TimeOutput)

      _SmoothedTrackPoints = _TrajectorySmooth.GetResult

      For _Frame = MyFirstFrame To LastFrame
        Me.DataPoint(_Frame).X = _SmoothedTrackPoints(_Frame - MyFirstFrame).X
        Me.DataPoint(_Frame).Y = _SmoothedTrackPoints(_Frame - MyFirstFrame).Y
        Me.DataPoint(_Frame).V = Math.Sqrt(_SmoothedTrackPoints(_Frame - MyFirstFrame).VX ^ 2 + _SmoothedTrackPoints(_Frame - MyFirstFrame).VY ^ 2)
        Me.DataPoint(_Frame).A = Math.Sqrt(_SmoothedTrackPoints(_Frame - MyFirstFrame).AX ^ 2 + _SmoothedTrackPoints(_Frame - MyFirstFrame).AY ^ 2)
        Me.DataPoint(_Frame).J = NoValue
      Next _Frame


      ' Calculate(Dx, Dy)
      ' Dim _dL, _dLmin As Double
      ' Dim _p, _p1, _p2 As clsGeomPoint


      ' Calculate Directions
      '_dLmin = 0.5
      'For _i = 0 To MyTrajectory.Count - 1
      '  _p = New clsGeomPoint(MyTrajectory(_i).X, MyTrajectory(_i).Y)
      '  _j = -1
      '  Do
      '    _j = _j + 1
      '    _p1 = New clsGeomPoint(MyTrajectory(_i - _j).X, MyTrajectory(_i - _j).Y)
      '  Loop Until (_p.DistanceTo(_p1) > _dLmin / 2) Or ((_i - _j) = 0)

      '  _j = -1
      '  Do
      '    _j = _j + 1
      '    _p2 = New clsGeomPoint(MyTrajectory(_i + _j).X, MyTrajectory(_i + _j).Y)
      '    _dL = _p1.DistanceTo(_p2)
      '  Loop Until (_dL > _dLmin) Or ((_i + _j) = MyTrajectory.Count - 1)


      '  If Not IsZero(_dL) Then
      '    MyTrajectory(_i).DxDy = New clsDxDy((_p2.X - _p1.X) / _dL, (_p2.Y - _p1.Y) / _dL)
      '  Else
      '    MyTrajectory(_i).DxDy = New clsDxDy(1, 0)
      '  End If
      'Next _i

      MyParentRecord.RoadUsersChanged = True
    Else
      MsgBox("Too few points to get a reliable trajectory data")
    End If

  End Sub

  Public Function SimultaneouslyWith(ByVal RoadUser2 As clsRoadUser) As Boolean

    Return IsBetween(Me.FirstFrame, RoadUser2.FirstFrame, RoadUser2.LastFrame) Or
           IsBetween(Me.LastFrame, RoadUser2.FirstFrame, RoadUser2.LastFrame) Or
           IsBetween(RoadUser2.FirstFrame, Me.FirstFrame, Me.LastFrame) Or
           IsBetween(RoadUser2.LastFrame, Me.FirstFrame, Me.LastFrame)
  End Function

  Public Function MaxX() As Double
    Dim _MaxX As Double

    _MaxX = NoValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If IsMore(Me.DataPoint(_Frame).X, _MaxX) Then _MaxX = Me.DataPoint(_Frame).X
    Next
    Return _MaxX
  End Function

  Public Function MinX() As Double
    Dim _minX As Double

    _minX = Double.MaxValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If Me.DataPoint(_Frame).OriginalXYExists Then
        If IsLess(Me.DataPoint(_Frame).X, _minX) Then _minX = Me.DataPoint(_Frame).X
      End If
    Next
    If IsEqual(_minX, Double.MaxValue) Then Return NoValue Else Return _minX
  End Function

  Public Function MaxY() As Double
    Dim _maxY As Double

    _maxY = NoValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If Me.DataPoint(_Frame).OriginalXYExists Then
        If IsMore(Me.DataPoint(_Frame).Y, _maxY) Then _maxY = Me.DataPoint(_Frame).Y
      End If
    Next
    Return _maxY
  End Function

  Public Function MinY() As Double
    Dim _minY As Double

    _minY = Double.MaxValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If Me.DataPoint(_Frame).OriginalXYExists Then
        If IsLess(Me.DataPoint(_Frame).Y, _minY) Then _minY = Me.DataPoint(_Frame).Y
      End If
    Next
    If IsEqual(_minY, Double.MaxValue) Then Return NoValue Else Return _minY
  End Function

  Public Function MaxV() As Double
    Dim _maxV As Double

    _maxV = NoValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If IsMore(Me.DataPoint(_Frame).V, _maxV) Then _maxV = Me.DataPoint(_Frame).V
    Next
    Return _maxV
  End Function

  Public Function MinV() As Double
    Dim _minV As Double

    _minV = Double.MaxValue
    For _Frame = Me.FirstFrame To Me.LastFrame
      If IsLess(Me.DataPoint(_Frame).Y, _minV) Then _minV = Me.DataPoint(_Frame).Y
    Next
    Return _minV
  End Function


  Public Function CrossGateAt(ByVal Gate As clsGate) As clsGateCrossPoint
    Dim _OneStepMove As clsGeomLine
    Dim _CrossingPoint As clsGeomPoint
    Dim _CrossingTime As Double
    Dim _CrossingV As Double
    Dim _CrossingA As Double
    Dim _CrossingJ As Double
    Dim _MiniMove As Double

    For _i = MyFirstFrame + 1 To Me.LastFrame
      _OneStepMove = New clsGeomLine(Me.DataPoint(_i - 1), Me.DataPoint(_i))
      _CrossingPoint = _OneStepMove.IntersectsAt(Gate)

      If _CrossingPoint IsNot Nothing Then
        If Not IsZero(_OneStepMove.Length) Then
          _MiniMove = Me.DataPoint(_i - 1).DistanceTo(_CrossingPoint)
          _CrossingTime = Interpolate(_MiniMove,
                                      0,
                                      MyParentRecord.GetLocalTime(_i - 1),
                                      _OneStepMove.Length,
                                      MyParentRecord.GetLocalTime(_i))
          _CrossingV = Interpolate(_MiniMove,
                                   0,
                                   Me.DataPoint(_i - 1).V,
                                   _OneStepMove.Length,
                                   Me.DataPoint(_i).V)

          _CrossingA = Interpolate(_MiniMove,
                                   0,
                                   Me.DataPoint(_i - 1).A,
                                   _OneStepMove.Length,
                                   Me.DataPoint(_i).A)

          _CrossingJ = Interpolate(_MiniMove,
                                      0,
                                      Me.DataPoint(_i - 1).J,
                                      _OneStepMove.Length,
                                      Me.DataPoint(_i).J)

        Else
          _CrossingTime = MyParentRecord.GetLocalTime(_i - 1)
          _CrossingV = Me.DataPoint(_i - 1).V
          _CrossingA = Me.DataPoint(_i - 1).A
          _CrossingJ = Me.DataPoint(_i - 1).J
        End If
        Return New clsGateCrossPoint(_CrossingPoint.X, _CrossingPoint.Y, _CrossingTime, _CrossingV, _CrossingA, _CrossingJ)
      End If
    Next

    Return Nothing
  End Function


  Public Function ComesClosestToPointAtTime(ByVal Point As clsGeomPoint, Optional ByRef MinDistance As Double = NoValue) As Double
    Dim _OneStepMove As clsGeomLine
    Dim _Distance As Double
    Dim _ClosestPoint As clsGeomPoint
    Dim _Time1, _Time2, _TimeMinDistance As Double

    _ClosestPoint = Nothing
    MinDistance = Double.MaxValue
    For _Frame = MyFirstFrame + 1 To Me.LastFrame
      _Time1 = MyParentRecord.GetAbsoluteTime(_Frame - 1)
      _Time2 = MyParentRecord.GetAbsoluteTime(_Frame)

      _OneStepMove = New clsGeomLine(Me.DataPoint(_Frame - 1), Me.DataPoint(_Frame))

      _Distance = _OneStepMove.DistanceToPoint(Point, _ClosestPoint)

      If _Distance < MinDistance Then
        MinDistance = _Distance

        If Math.Abs(_OneStepMove.Point1.X - _OneStepMove.Point2.X) > Math.Abs(_OneStepMove.Point1.Y - _OneStepMove.Point2.Y) Then
          _TimeMinDistance = Interpolate(_ClosestPoint.X, _OneStepMove.Point1.X, _Time1, _OneStepMove.Point2.X, _Time2)
        Else
          _TimeMinDistance = Interpolate(_ClosestPoint.Y, _OneStepMove.Point1.Y, _Time1, _OneStepMove.Point2.Y, _Time2)
        End If
      End If

    Next _Frame

    Return _TimeMinDistance
  End Function




#End Region



#Region "Private subs"


#End Region



  Public Sub ExtendTrajectory(ByVal Frame As Integer)
    Dim _OriginalDataPoint, _NewDataPoint As clsDataPoint
    Dim _i As Integer


    If Frame < MyFirstFrame Then
      _OriginalDataPoint = Me.DataPoint(MyFirstFrame).CreateCopy
      _OriginalDataPoint.V = NoValue
      _OriginalDataPoint.A = NoValue
      _OriginalDataPoint.J = NoValue
      For _i = Frame To MyFirstFrame - 2
        _NewDataPoint = New clsDataPoint(NoValue, NoValue, New clsDxDy(), _OriginalDataPoint.X, _OriginalDataPoint.Y, _OriginalDataPoint.DxDy, NoValue, NoValue, NoValue)
        Me.AddStartPoint(_NewDataPoint)
      Next
      Me.AddStartPoint(_OriginalDataPoint)

      MyParentRecord.RoadUsersChanged = True
    ElseIf Frame > Me.LastFrame Then
      _OriginalDataPoint = Me.DataPoint(Me.LastFrame).CreateCopy
      _OriginalDataPoint.V = NoValue
      _OriginalDataPoint.A = NoValue
      _OriginalDataPoint.J = NoValue
      For _i = Me.LastFrame + 2 To Frame
        _NewDataPoint = New clsDataPoint(NoValue, NoValue, New clsDxDy(), _OriginalDataPoint.X, _OriginalDataPoint.Y, _OriginalDataPoint.DxDy, NoValue, NoValue, NoValue)
        Me.AddPoint(_NewDataPoint)
      Next
      Me.AddPoint(_OriginalDataPoint)

      MyParentRecord.RoadUsersChanged = True
    End If

  End Sub



  Public Function ResampledTrajectory(ByVal Frame As Integer, ByVal StepSize As Double) As List(Of clsDPoint)
    Dim _Trajectory As List(Of clsDPoint)
    Dim _Point1, _Point2, _PointTemp As clsGeomPoint
    Dim _DxDy As clsDxDy
    Dim _Travel As Double
    Dim _Distance, _Shortcut As Double
    Dim _x, _y As Double
    Dim _i As Integer


    _Trajectory = New List(Of clsDPoint)

    If IsBetween(Frame, Me.FirstFrame, Me.LastFrame - 2) Then
      _i = Me.LastFrame - 1
      _Point1 = New clsGeomPoint(Me.DataPoint(Me.LastFrame).X, Me.DataPoint(Me.LastFrame).Y)
      _PointTemp = New clsGeomPoint(Me.DataPoint(Me.LastFrame).X, Me.DataPoint(Me.LastFrame).Y)
      _Travel = 0
      _Trajectory.Add(New clsDPoint(_Point1, Me.DataPoint(Me.LastFrame).DxDy))
      Do
        _Distance = _PointTemp.DistanceTo(Me.DataPoint(_i))

        If (_Travel + _Distance) < StepSize Then
          _Travel = _Travel + _Distance
          _PointTemp = New clsGeomPoint(Me.DataPoint(_i).X, Me.DataPoint(_i).Y)
          _i = _i - 1
        Else
          _x = Interpolate((StepSize - _Travel), 0, _PointTemp.X, _Distance, Me.DataPoint(_i).X)
          _y = Interpolate((StepSize - _Travel), 0, _PointTemp.Y, _Distance, Me.DataPoint(_i).Y)
          _Point2 = New clsGeomPoint(_x, _y)

          _Shortcut = _Point1.DistanceTo(_Point2)
          _DxDy = New clsDxDy((_Point1.X - _Point2.X) / _Shortcut, (_Point1.Y - _Point2.Y) / _Shortcut)

          _Trajectory.Insert(0, New clsDPoint(_Point2, _DxDy))
          _Point1 = _Point2
          _PointTemp = _Point2
          _Travel = 0
        End If
      Loop Until _i = Frame


      _Trajectory.Insert(0, New clsDPoint(Me.DataPoint(Frame).X, Me.DataPoint(Frame).Y, Me.DataPoint(Frame).DxDy))

    End If




    Return _Trajectory
  End Function

  'Public ReadOnly Property StartTime As Double
  ' Get
  '  StartTime = TimeLine(MyFirstFrame).Time
  'End Get
  'End Property



  'Public Function TrajectoryCrossesLine(ByVal TestLine As clsGeomLine) As clsTPoint
  '  '  Dim _i As Integer
  '  ' Dim _Movement As clsXMovement
  '  Dim _CrossPoint As clsTPoint


  '  _CrossPoint = Nothing
  '  ' For _i = 1 To MyTrajectory.Count - 1
  '  '  _Movement = New clsXMovement(MyTrajectory(_i - 1), MyTrajectory(_i))

  '  '  If _Movement.Intersects(TestLine, _CrossPoint) Then
  '  '   Exit For
  '  '  End If
  '  '  Next _i

  '  Return _CrossPoint
  'End Function




  'Public Function FitDirection(ByVal Direction As clsDirection) As Boolean
  '  Dim _CrossPoint1 As clsTPoint
  '  Dim _CrossPoint2 As clsTPoint

  '  _CrossPoint1 = Me.TrajectoryCrossesLine(Direction.Gate1)
  '  _CrossPoint2 = Me.TrajectoryCrossesLine(Direction.Gate2)

  '  If (_CrossPoint1 IsNot Nothing) And (_CrossPoint2 IsNot Nothing) Then
  '    If IsLess(_CrossPoint1.Time, _CrossPoint2.Time) Then
  '      Return True
  '    Else
  '      Return False
  '    End If
  '  Else
  '    Return False
  '  End If
  'End Function

End Class
