﻿Public MustInherit Class clsDatabase
  Protected MyRecordIndexVR As Integer
  Protected MyRecordIndexDT As Integer

  Private Const SORT_ORDER As String = "cmDateTime ASC, cmID ASC"

  Protected MyOleDBConnection As OleDbConnection
  Private MyDataSet As DataSet
  Private MyVideoRecordingRows() As DataRow
  Private MyDetectionRows() As DataRow
  Private MyDatabaseFilePath As String
  ' Private MyDataMode As Integer

  Private MyFilterVR As String
  Private MyFilterDT As String

  Private MyMaxVideoRecordingID As Integer
  Private MyMaxDetectionID As Integer
  Private MyMaxVideoFileID_VR As Integer
  Private MyMaxVideoFileID_DT As Integer



#Region "Public properties"

  Public ReadOnly Property NumberOfRecordsVR() As Integer
    Get
      NumberOfRecordsVR = MyDataSet.Tables("VideoRecordings").Rows.Count
    End Get
  End Property

  Public ReadOnly Property NumberOfRecordsDT() As Integer
    Get
      NumberOfRecordsDT = MyDataSet.Tables("Detections").Rows.Count
    End Get
  End Property

  Public ReadOnly Property NumberOfFilteredRecordsVR() As Integer
    Get
      NumberOfFilteredRecordsVR = MyVideoRecordingRows.Length
    End Get
  End Property

  Public ReadOnly Property NumberOfFilteredRecordsDT() As Integer
    Get
      NumberOfFilteredRecordsDT = MyDetectionRows.Length
    End Get
  End Property

  Public ReadOnly Property CurrentRecordIndexVR() As Integer
    Get
      CurrentRecordIndexVR = MyRecordIndexVR
    End Get
  End Property

  Public ReadOnly Property CurrentRecordIndexDT() As Integer
    Get
      CurrentRecordIndexDT = MyRecordIndexDT
    End Get
  End Property

#End Region



#Region "Protected Properties"

  'the value in ID-field of the current record in the database table
  Protected ReadOnly Property RecordID_VR() As Integer
    Get
      Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmID"))
    End Get
  End Property

  Protected ReadOnly Property RecordID_DT() As Integer
    Get
      Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmID"))
    End Get
  End Property

  Protected Property MapAvailableVR As Boolean
    Get
      Return GetSQLValueBoolean(MyVideoRecordingRows(MyRecordIndexVR)("cmMapAvailable"))
    End Get
    Set(value As Boolean)
      MyVideoRecordingRows(MyRecordIndexVR)("cmMapAvailable") = value
    End Set
  End Property

  Protected Property MapVR As String
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("cmMap"))
      Else
        Return ""
      End If
    End Get
    Set(ByVal value As String)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMap") = value
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMap") = ""
      End If
    End Set
  End Property

  Protected Property MapWidthVR As Integer
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmMapWidth"))
      Else
        Return NoValue
      End If
    End Get
    Set(value As Integer)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapWidth") = SetSQLValueInteger(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapWidth") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapHeightVR As Integer
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmMapHeight"))
      Else
        Return NoValue
      End If
    End Get
    Set(value As Integer)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapHeight") = SetSQLValueInteger(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapHeight") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapScaleVR() As Double
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmMapScale"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapScale") = SetSQLValueDouble(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapScale") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapX_VR() As Double
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmMapX"))
      Else
        Return NoValue
      End If
    End Get

    Set(ByVal value As Double)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapX") = SetSQLValueDouble(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapX") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapY_VR() As Double
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmMapY"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapY") = SetSQLValueDouble(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapY") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapDxVR() As Double
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmMapDx"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapDx") = SetSQLValueDouble(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapDx") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapDyVR() As Double
    Get
      If Me.MapAvailableVR Then
        Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmMapDy"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapDy") = SetSQLValueDouble(value)
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapDy") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapVisibleVR As Boolean
    Get
      Return GetSQLValueBoolean(MyVideoRecordingRows(MyRecordIndexVR)("cmMapVisible"))
    End Get
    Set(value As Boolean)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapVisible") = value
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapVisible") = False
      End If
    End Set
  End Property

  Protected Property MapCommentVR As String
    Get
      Return GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("cmMapComment"))
    End Get
    Set(value As String)
      If Me.MapAvailableVR Then
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapComment") = value
      Else
        MyVideoRecordingRows(MyRecordIndexVR)("cmMapComment") = ""
      End If
    End Set
  End Property


  Protected Property MediaVR As String
    Get
      Return GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("cmMedia"))
    End Get
    Set(value As String)
      MyVideoRecordingRows(MyRecordIndexVR)("cmMedia") = value
    End Set
  End Property

  Protected Property MapAvailableDT As Boolean
    Get
      Return GetSQLValueBoolean(MyDetectionRows(MyRecordIndexDT)("cmMapAvailable"))
    End Get
    Set(value As Boolean)
      MyDetectionRows(MyRecordIndexDT)("cmMapAvailable") = value
    End Set
  End Property

  Protected Property MapDT As String
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueString(MyDetectionRows(MyRecordIndexDT)("cmMap"))
      Else
        Return ""
      End If
    End Get
    Set(ByVal value As String)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMap") = value
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMap") = ""
      End If
    End Set
  End Property

  Protected Property MapWidthDT As Integer
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmMapWidth"))
      Else
        Return NoValue
      End If
    End Get
    Set(value As Integer)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapWidth") = SetSQLValueInteger(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapWidth") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapHeightDT As Integer
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmMapHeight"))
      Else
        Return NoValue
      End If
    End Get
    Set(value As Integer)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapHeight") = SetSQLValueInteger(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapHeight") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapScaleDT() As Double
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmMapScale"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapScale") = SetSQLValueDouble(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapScale") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapX_DT() As Double
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmMapX"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapX") = SetSQLValueDouble(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapX") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapY_DT() As Double
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmMapY"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapY") = SetSQLValueDouble(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapY") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapDxDT() As Double
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmMapDx"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapDx") = SetSQLValueDouble(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapDx") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapDyDT() As Double
    Get
      If Me.MapAvailableDT Then
        Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmMapDy"))
      Else
        Return NoValue
      End If
    End Get
    Set(ByVal value As Double)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapDy") = SetSQLValueDouble(value)
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapDy") = DBNull.Value
      End If
    End Set
  End Property

  Protected Property MapVisibleDT As Boolean
    Get
      Return GetSQLValueBoolean(MyDetectionRows(MyRecordIndexDT)("cmMapVisible"))
    End Get
    Set(value As Boolean)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapVisible") = value
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapVisible") = False
      End If
    End Set
  End Property

  Protected Property MapCommentDT As String
    Get
      Return GetSQLValueString(MyDetectionRows(MyRecordIndexDT)("cmMapComment"))
    End Get
    Set(value As String)
      If Me.MapAvailableDT Then
        MyDetectionRows(MyRecordIndexDT)("cmMapComment") = value
      Else
        MyDetectionRows(MyRecordIndexDT)("cmMapComment") = ""
      End If
    End Set
  End Property


  Protected Property MediaDT As String
    Get
      Return GetSQLValueString(MyDetectionRows(MyRecordIndexDT)("cmMedia"))
    End Get
    Set(value As String)
      MyDetectionRows(MyRecordIndexDT)("cmMedia") = value
    End Set
  End Property


  Protected Property StartTimeVR As Double
    Get
      Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmStartTime"))
    End Get
    Set(ByVal value As Double)
      MyVideoRecordingRows(MyRecordIndexVR)("cmStartTime") = SetSQLValueDouble(value)
    End Set
  End Property


  Protected Property StartTimeDT As Double
    Get
      Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmStartTime"))
    End Get
    Set(ByVal value As Double)
      MyDetectionRows(MyRecordIndexDT)("cmStartTime") = SetSQLValueDouble(value)
    End Set
  End Property

  Protected Property FrameRateVR As Double
    Get
      Return GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("cmFrameRate"))
    End Get
    Set(ByVal value As Double)
      MyVideoRecordingRows(MyRecordIndexVR)("cmFrameRate") = SetSQLValueDouble(value)
    End Set
  End Property

  Protected Property FrameRateDT As Double
    Get
      Return GetSQLValueDouble(MyDetectionRows(MyRecordIndexDT)("cmFrameRate"))
    End Get
    Set(ByVal value As Double)
      MyDetectionRows(MyRecordIndexDT)("cmFrameRate") = SetSQLValueDouble(value)
    End Set
  End Property

  Protected Property FrameCountVR As Integer
    Get
      Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmFrameCount"))
    End Get
    Set(ByVal value As Integer)
      MyVideoRecordingRows(MyRecordIndexVR)("cmFrameCount") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property FrameCountDT As Integer
    Get
      Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmFrameCount"))
    End Get
    Set(ByVal value As Integer)
      MyDetectionRows(MyRecordIndexDT)("cmFrameCount") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property DateTimeVR() As Date
    Get
      Return GetSQLValueDate(MyVideoRecordingRows(MyRecordIndexVR)("cmDateTime"))
    End Get
    Set(ByVal value As Date)
      MyVideoRecordingRows(MyRecordIndexVR)("cmDateTime") = SetSQLValueDate(value)
    End Set
  End Property

  Protected Property DateTimeDT() As Date
    Get
      Return GetSQLValueDate(MyDetectionRows(MyRecordIndexDT)("cmDateTime"))
    End Get
    Set(ByVal value As Date)
      MyDetectionRows(MyRecordIndexDT)("cmDateTime") = SetSQLValueDate(value)
    End Set
  End Property

  Protected Property StatusVR() As Integer
    Get
      Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmStatus"))
    End Get
    Set(ByVal value As Integer)
      MyVideoRecordingRows(MyRecordIndexVR)("cmStatus") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property StatusDT() As Integer
    Get
      Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmStatus"))
    End Get
    Set(ByVal value As Integer)
      MyDetectionRows(MyRecordIndexDT)("cmStatus") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property TypeVR() As Integer
    Get
      Return GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("cmType"))
    End Get
    Set(ByVal value As Integer)
      MyVideoRecordingRows(MyRecordIndexVR)("cmType") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property TypeDT() As Integer
    Get
      Return GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmType"))
    End Get
    Set(ByVal value As Integer)
      MyDetectionRows(MyRecordIndexDT)("cmType") = SetSQLValueInteger(value)
    End Set
  End Property

  Protected Property CommentVR() As String
    Get
      Return GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("cmComment"))
    End Get
    Set(ByVal value As String)
      MyVideoRecordingRows(MyRecordIndexVR)("cmComment") = value
    End Set
  End Property

  Protected Property CommentDT() As String
    Get
      Return GetSQLValueString(MyDetectionRows(MyRecordIndexDT)("cmComment"))
    End Get
    Set(ByVal value As String)
      MyDetectionRows(MyRecordIndexDT)("cmComment") = value
    End Set
  End Property

  Public ReadOnly Property RelatedRecordsVR As List(Of Integer)
    Get
      Dim _Row As DataRow
      Dim _VideoRecordingID As Integer
      Dim _RelatedRecords As List(Of Integer)

      _RelatedRecords = New List(Of Integer)
      For Each _Row In MyDetectionRows
        _VideoRecordingID = GetSQLValueInteger(_Row("cmVideoRecording"))
        If _VideoRecordingID = Me.RecordID_VR Then
          _RelatedRecords.Add(GetSQLValueInteger(_Row("cmID")))
        End If
      Next
      Return _RelatedRecords
    End Get
  End Property

  Public Property RelatedRecordsDT As List(Of Integer)
    Get
      Dim _VideoRecording As Integer
      Dim _RelatedRecords As List(Of Integer)

      _RelatedRecords = New List(Of Integer)
      _VideoRecording = GetSQLValueInteger(MyDetectionRows(MyRecordIndexDT)("cmVideoRecording"))
      If IsValue(_VideoRecording) Then _RelatedRecords.Add(_VideoRecording)
      Return _RelatedRecords
    End Get
    Set(value As List(Of Integer))
      If value.Count > 0 Then
        MyDetectionRows(MyRecordIndexDT)("cmVideoRecording") = SetSQLValueInteger(value(0))
      Else
        MyDetectionRows(MyRecordIndexDT)("cmVideoRecording") = DBNull.Value
      End If
    End Set
  End Property

  


  Public ReadOnly Property GetUserControlsVR() As List(Of clsUserControl)
    Get
      Dim _UserControlList As List(Of clsUserControl)
      Dim _Control As clsUserControl
      Dim _Rows() As DataRow
      Dim _Row As DataRow

      _UserControlList = New List(Of clsUserControl)
      _Rows = MyDataSet.Tables("VideoRecordingControls").Select("", "cmPosition ASC")
      For Each _Row In _Rows
        _Control = New clsUserControl
        _Control.Name = _Row("cmName").ToString
        _Control.Label = _Row("cmLabel").ToString
        _Control.Position = CInt(_Row("cmPosition"))
        _Control.Type = CType(GetSQLValueInteger(_Row("cmType")), UserControlTypes)
        _Control.ValueTable = MyDataSet.Tables("uVR_" & _Row("cmName").ToString & "Values")
        _Control.Scale = GetSQLValueInteger(_Row("cmScale"))
        _Control.Decimals = GetSQLValueInteger(_Row("cmDecimals"))
        _UserControlList.Add(_Control)
      Next

      Return _UserControlList
    End Get
  End Property

  Public ReadOnly Property GetUserControlsDT() As List(Of clsUserControl)
    Get
      Dim _UserControlList As List(Of clsUserControl)
      Dim _Control As clsUserControl
      Dim _Rows() As DataRow
      Dim _Row As DataRow


      _UserControlList = New List(Of clsUserControl)
      _Rows = MyDataSet.Tables("DetectionControls").Select("", "cmPosition ASC")
      For Each _Row In _Rows
        _Control = New clsUserControl
        _Control.Name = _Row("cmName").ToString
        _Control.Label = _Row("cmLabel").ToString
        _Control.Position = CInt(_Row("cmPosition"))
        _Control.Type = CType(GetSQLValueInteger(_Row("cmType")), UserControlTypes)
        _Control.ValueTable = MyDataSet.Tables("uDT_" & _Row("cmName").ToString & "Values")
        _Control.Scale = GetSQLValueInteger(_Row("cmScale"))
        _Control.Decimals = GetSQLValueInteger(_Row("cmDecimals"))

        _UserControlList.Add(_Control)
      Next

      Return _UserControlList
    End Get
  End Property





  'returns the datatable with values for the Status-field
  Public ReadOnly Property StatusValuesVR() As DataTable
    Get
      StatusValuesVR = MyDataSet.Tables("VideoRecordingStatus")
    End Get
  End Property

  Public ReadOnly Property StatusValuesDT() As DataTable
    Get
      StatusValuesDT = MyDataSet.Tables("DetectionStatus")
    End Get
  End Property

  'returns the datatable with values for the Type-field
  Public ReadOnly Property TypeValuesVR() As DataTable
    Get
      TypeValuesVR = MyDataSet.Tables("VideoRecordingType")
    End Get
  End Property

  Public ReadOnly Property TypeValuesDT() As DataTable
    Get
      TypeValuesDT = MyDataSet.Tables("DetectionType")
    End Get
  End Property


  Protected Property UserControlValuesVR() As List(Of Object)
    Get
      Dim _ValueList As List(Of Object)
      Dim _Control As clsUserControl
      Dim _Value As Object
      Dim _UserControls As List(Of clsUserControl)

      _ValueList = New List(Of Object)
      _UserControls = Me.GetUserControlsVR
      For Each _Control In _UserControls
        Select Case _Control.Type
          Case UserControlTypes.Textbox
            _Value = GetSQLValueString(MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _Control.Name))
          Case UserControlTypes.Numberbox
            _Value = GetSQLValueDouble(MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _Control.Name))
          Case UserControlTypes.Checkbox
            _Value = GetSQLValueBoolean(MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _Control.Name))
          Case UserControlTypes.Combobox
            _Value = GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _Control.Name))
          Case UserControlTypes.ClickCounter
            _Value = GetSQLValueInteger(MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _Control.Name))
          Case Else
            _Value = DBNull.Value
        End Select
        _ValueList.Add(_Value)
      Next

      Return _ValueList
    End Get

    Set(ByVal values As List(Of Object))
      Dim _Control As clsUserControl
      Dim _Value As Object
      Dim _i As Integer
      Dim _UserControls As List(Of clsUserControl)

      _UserControls = Me.GetUserControlsVR
      For _i = 0 To _UserControls.Count - 1
        _Control = _UserControls(_i)
        _Value = values(_i)
        Select Case _Control.Type
          Case UserControlTypes.Textbox
            MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Numberbox
            MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Checkbox
            MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Combobox
            MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.ClickCounter
            MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControls(_i).Name) = _Value
        End Select
      Next _i
    End Set
  End Property

  Protected Property UserControlValuesDT() As List(Of Object)
    Get
      Dim _ValueList As List(Of Object)
      Dim _Control As clsUserControl
      Dim _Value As Object
      Dim _UserControls As List(Of clsUserControl)

      _ValueList = New List(Of Object)
      _UserControls = Me.GetUserControlsDT
      For Each _Control In _UserControls
        Select Case _Control.Type
          Case UserControlTypes.Textbox
            _Value = MyDetectionRows(MyRecordIndexDT)("uDT_" & _Control.Name)
          Case UserControlTypes.Numberbox
            _Value = MyDetectionRows(MyRecordIndexDT)("uDT_" & _Control.Name)
          Case UserControlTypes.Checkbox
            _Value = MyDetectionRows(MyRecordIndexDT)("uDT_" & _Control.Name)
          Case UserControlTypes.Combobox
            _Value = MyDetectionRows(MyRecordIndexDT)("uDT_" & _Control.Name)
          Case UserControlTypes.ClickCounter
            _Value = MyDetectionRows(MyRecordIndexDT)("uDT_" & _Control.Name)
          Case Else
            _Value = DBNull.Value
        End Select
        _ValueList.Add(_Value)
      Next

      Return _ValueList
    End Get

    Set(ByVal values As List(Of Object))
      Dim _Control As clsUserControl
      Dim _Value As Object
      Dim _i As Integer
      Dim _UserControls As List(Of clsUserControl)

      _UserControls = Me.GetUserControlsDT
      For _i = 0 To _UserControls.Count - 1
        _Control = _UserControls(_i)
        _Value = values(_i)
        Select Case _Control.Type
          Case UserControlTypes.Textbox
            MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Numberbox
            MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Checkbox
            MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.Combobox
            MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControls(_i).Name) = _Value
          Case UserControlTypes.ClickCounter
            MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControls(_i).Name) = _Value
        End Select
      Next _i
    End Set
  End Property


  Protected Property FilterVR As String
    Get
      FilterVR = MyFilterVR
    End Get
    Set(ByVal value As String)
      MyFilterVR = value
      Call SelectVideoRecordings()
    End Set
  End Property

  Protected Property FilterDT As String
    Get
      FilterDT = MyFilterDT
    End Get
    Set(ByVal value As String)
      MyFilterDT = value
      Call SelectDetections()
    End Set
  End Property

  Protected Function GetVideoFilesVR(RecordVR As clsRecord) As List(Of clsVideoFile)
    Dim _VideoFile As clsVideoFile
    Dim _VideoFiles As List(Of clsVideoFile)
    Dim _VideoFileName As String
    Dim _Row As DataRow
    Dim _Rows() As DataRow

    _VideoFiles = New List(Of clsVideoFile)

    _Rows = MyDataSet.Tables("VideoRecordingVideoFiles").Select("cmOwner = " & RecordVR.ID.ToString, "cmPosition ASC")
    For Each _Row In _Rows

      If Not IsDBNull(_Row("cmVideoFile")) Then _VideoFileName = CStr(_Row("cmVideoFile")) Else _VideoFileName = ""
      _VideoFile = New clsVideoFile(RecordVR, _VideoFileName)

      With _VideoFile
        .Status = VideoFileStatuses.Unchanged
        If Not IsDBNull(_Row("cmID")) Then .ID = CInt(_Row("cmID")) Else .ID = NoValue
        If Not IsDBNull(_Row("cmPosition")) Then .Position = CInt(_Row("cmPosition")) Else .Position = NoValue
        If Not IsDBNull(_Row("cmClockDiscrepancy")) Then .ClockDiscrepancy = CDbl(_Row("cmClockDiscrepancy")) Else .ClockDiscrepancy = NoValue
        If Not IsDBNull(_Row("cmVisible")) Then .Visible = CBool(_Row("cmVisible")) Else .Visible = False
        If Not IsDBNull(_Row("cmComment")) Then .Comment = CStr(_Row("cmComment")) Else .Comment = ""
        If Not IsDBNull(_Row("cmCallibrationOK")) Then .CallibrationOK = CBool(_Row("cmCallibrationOK")) Else .CallibrationOK = False
        If Not IsDBNull(_Row("cmr1")) Then .TSAI.r1 = CDbl(_Row("cmr1")) Else .TSAI.r1 = NoValue
        If Not IsDBNull(_Row("cmr2")) Then .TSAI.r2 = CDbl(_Row("cmr2")) Else .TSAI.r2 = NoValue
        If Not IsDBNull(_Row("cmr3")) Then .TSAI.r3 = CDbl(_Row("cmr3")) Else .TSAI.r3 = NoValue
        If Not IsDBNull(_Row("cmr4")) Then .TSAI.r4 = CDbl(_Row("cmr4")) Else .TSAI.r4 = NoValue
        If Not IsDBNull(_Row("cmr5")) Then .TSAI.r5 = CDbl(_Row("cmr5")) Else .TSAI.r5 = NoValue
        If Not IsDBNull(_Row("cmr6")) Then .TSAI.r6 = CDbl(_Row("cmr6")) Else .TSAI.r6 = NoValue
        If Not IsDBNull(_Row("cmr7")) Then .TSAI.r7 = CDbl(_Row("cmr7")) Else .TSAI.r7 = NoValue
        If Not IsDBNull(_Row("cmr8")) Then .TSAI.r8 = CDbl(_Row("cmr8")) Else .TSAI.r8 = NoValue
        If Not IsDBNull(_Row("cmr9")) Then .TSAI.r9 = CDbl(_Row("cmr9")) Else .TSAI.r9 = NoValue
        If Not IsDBNull(_Row("cmTx")) Then .TSAI.Tx = CDbl(_Row("cmTx")) Else .TSAI.Tx = NoValue
        If Not IsDBNull(_Row("cmTy")) Then .TSAI.Ty = CDbl(_Row("cmTy")) Else .TSAI.Ty = NoValue
        If Not IsDBNull(_Row("cmTz")) Then .TSAI.Tz = CDbl(_Row("cmTz")) Else .TSAI.Tz = NoValue
        If Not IsDBNull(_Row("cmf")) Then .TSAI.f = CDbl(_Row("cmf")) Else .TSAI.f = NoValue
        If Not IsDBNull(_Row("cmk")) Then .TSAI.k = CDbl(_Row("cmk")) Else .TSAI.k = NoValue
        If Not IsDBNull(_Row("cmSx")) Then .TSAI.Sx = CDbl(_Row("cmSx")) Else .TSAI.Sx = NoValue
        If Not IsDBNull(_Row("cmdx")) Then .TSAI.dx = CDbl(_Row("cmdx")) Else .TSAI.dx = NoValue
        If Not IsDBNull(_Row("cmdy")) Then .TSAI.dy = CDbl(_Row("cmdy")) Else .TSAI.dy = NoValue
        If Not IsDBNull(_Row("cmCx")) Then .TSAI.Cx = CDbl(_Row("cmCx")) Else .TSAI.Cx = NoValue
        If Not IsDBNull(_Row("cmCy")) Then .TSAI.Cy = CDbl(_Row("cmCy")) Else .TSAI.Cy = NoValue
      End With
      _VideoFiles.Add(_VideoFile)
    Next

    Return _VideoFiles
  End Function

 
  Protected Function GetVideoFilesDT(RecordDT As clsRecord) As List(Of clsVideoFile)
    Dim _VideoFile As clsVideoFile
    Dim _VideoFiles As List(Of clsVideoFile)
    Dim _VideoFileName As String
    Dim _Row As DataRow
    Dim _Rows() As DataRow

    _VideoFiles = New List(Of clsVideoFile)

    _Rows = MyDataSet.Tables("DetectionVideoFiles").Select("cmOwner = " & RecordDT.ID.ToString, "cmPosition ASC")
    For Each _Row In _Rows

      If Not IsDBNull(_Row("cmVideoFile")) Then _VideoFileName = CStr(_Row("cmVideoFile")) Else _VideoFileName = ""
      _VideoFile = New clsVideoFile(RecordDT, _VideoFileName)

      With _VideoFile
        .Status = VideoFileStatuses.Unchanged
        If Not IsDBNull(_Row("cmID")) Then .ID = CInt(_Row("cmID")) Else .ID = NoValue
        If Not IsDBNull(_Row("cmPosition")) Then .Position = CInt(_Row("cmPosition")) Else .Position = NoValue
        If Not IsDBNull(_Row("cmClockDiscrepancy")) Then .ClockDiscrepancy = CDbl(_Row("cmClockDiscrepancy")) Else .ClockDiscrepancy = NoValue
        If Not IsDBNull(_Row("cmVisible")) Then .Visible = CBool(_Row("cmVisible")) Else .Visible = False
        If Not IsDBNull(_Row("cmComment")) Then .Comment = CStr(_Row("cmComment")) Else .Comment = ""
        If Not IsDBNull(_Row("cmCallibrationOK")) Then .CallibrationOK = CBool(_Row("cmCallibrationOK")) Else .CallibrationOK = False
        If Not IsDBNull(_Row("cmr1")) Then .TSAI.r1 = CDbl(_Row("cmr1")) Else .TSAI.r1 = NoValue
        If Not IsDBNull(_Row("cmr2")) Then .TSAI.r2 = CDbl(_Row("cmr2")) Else .TSAI.r2 = NoValue
        If Not IsDBNull(_Row("cmr3")) Then .TSAI.r3 = CDbl(_Row("cmr3")) Else .TSAI.r3 = NoValue
        If Not IsDBNull(_Row("cmr4")) Then .TSAI.r4 = CDbl(_Row("cmr4")) Else .TSAI.r4 = NoValue
        If Not IsDBNull(_Row("cmr5")) Then .TSAI.r5 = CDbl(_Row("cmr5")) Else .TSAI.r5 = NoValue
        If Not IsDBNull(_Row("cmr6")) Then .TSAI.r6 = CDbl(_Row("cmr6")) Else .TSAI.r6 = NoValue
        If Not IsDBNull(_Row("cmr7")) Then .TSAI.r7 = CDbl(_Row("cmr7")) Else .TSAI.r7 = NoValue
        If Not IsDBNull(_Row("cmr8")) Then .TSAI.r8 = CDbl(_Row("cmr8")) Else .TSAI.r8 = NoValue
        If Not IsDBNull(_Row("cmr9")) Then .TSAI.r9 = CDbl(_Row("cmr9")) Else .TSAI.r9 = NoValue
        If Not IsDBNull(_Row("cmTx")) Then .TSAI.Tx = CDbl(_Row("cmTx")) Else .TSAI.Tx = NoValue
        If Not IsDBNull(_Row("cmTy")) Then .TSAI.Ty = CDbl(_Row("cmTy")) Else .TSAI.Ty = NoValue
        If Not IsDBNull(_Row("cmTz")) Then .TSAI.Tz = CDbl(_Row("cmTz")) Else .TSAI.Tz = NoValue
        If Not IsDBNull(_Row("cmf")) Then .TSAI.f = CDbl(_Row("cmf")) Else .TSAI.f = NoValue
        If Not IsDBNull(_Row("cmk")) Then .TSAI.k = CDbl(_Row("cmk")) Else .TSAI.k = NoValue
        If Not IsDBNull(_Row("cmSx")) Then .TSAI.Sx = CDbl(_Row("cmSx")) Else .TSAI.Sx = NoValue
        If Not IsDBNull(_Row("cmdx")) Then .TSAI.dx = CDbl(_Row("cmdx")) Else .TSAI.dx = NoValue
        If Not IsDBNull(_Row("cmdy")) Then .TSAI.dy = CDbl(_Row("cmdy")) Else .TSAI.dy = NoValue
        If Not IsDBNull(_Row("cmCx")) Then .TSAI.Cx = CDbl(_Row("cmCx")) Else .TSAI.Cx = NoValue
        If Not IsDBNull(_Row("cmCy")) Then .TSAI.Cy = CDbl(_Row("cmCy")) Else .TSAI.Cy = NoValue
      End With
      _VideoFiles.Add(_VideoFile)
    Next

    Return _VideoFiles
  End Function


  Protected Sub SaveVideoFilesVR(RecordVR As clsRecord)
    Dim _VideoFile As clsVideoFile
    Dim _Row As DataRow
    Dim _Rows() As DataRow
    Dim _OleDbCommand As OleDbCommand
    Dim _DeletedVideoFiles As List(Of clsVideoFile)



    _DeletedVideoFiles = New List(Of clsVideoFile)

    For Each _VideoFile In RecordVR.VideoFiles
      Select Case _VideoFile.Status
        Case VideoFileStatuses.Updated
          _Rows = MyDataSet.Tables("VideoRecordingVideoFiles").Select("cmID = " & _VideoFile.ID)
          _Rows(0)("cmVideoFile") = _VideoFile.VideoFileName
          _Rows(0)("cmPosition") = _VideoFile.Position
          _Rows(0)("cmClockDiscrepancy") = _VideoFile.ClockDiscrepancy
          _Rows(0)("cmVisible") = _VideoFile.Visible
          _Rows(0)("cmComment") = _VideoFile.Comment
          _Rows(0)("cmCallibrationOK") = _VideoFile.CallibrationOK
          _Rows(0)("cmr1") = SetSQLValueDouble(_VideoFile.TSAI.r1)
          _Rows(0)("cmr2") = SetSQLValueDouble(_VideoFile.TSAI.r2)
          _Rows(0)("cmr3") = SetSQLValueDouble(_VideoFile.TSAI.r3)
          _Rows(0)("cmr4") = SetSQLValueDouble(_VideoFile.TSAI.r4)
          _Rows(0)("cmr5") = SetSQLValueDouble(_VideoFile.TSAI.r5)
          _Rows(0)("cmr6") = SetSQLValueDouble(_VideoFile.TSAI.r6)
          _Rows(0)("cmr7") = SetSQLValueDouble(_VideoFile.TSAI.r7)
          _Rows(0)("cmr8") = SetSQLValueDouble(_VideoFile.TSAI.r8)
          _Rows(0)("cmr9") = SetSQLValueDouble(_VideoFile.TSAI.r9)
          _Rows(0)("cmTx") = SetSQLValueDouble(_VideoFile.TSAI.Tx)
          _Rows(0)("cmTy") = SetSQLValueDouble(_VideoFile.TSAI.Ty)
          _Rows(0)("cmTz") = SetSQLValueDouble(_VideoFile.TSAI.Tz)
          _Rows(0)("cmf") = SetSQLValueDouble(_VideoFile.TSAI.f)
          _Rows(0)("cmk") = SetSQLValueDouble(_VideoFile.TSAI.k)
          _Rows(0)("cmSx") = SetSQLValueDouble(_VideoFile.TSAI.Sx)
          _Rows(0)("cmdx") = SetSQLValueDouble(_VideoFile.TSAI.dx)
          _Rows(0)("cmdy") = SetSQLValueDouble(_VideoFile.TSAI.dy)
          _Rows(0)("cmCx") = SetSQLValueDouble(_VideoFile.TSAI.Cx)
          _Rows(0)("cmCy") = SetSQLValueDouble(_VideoFile.TSAI.Cy)
          MyDataSet.Tables("VideoRecordingVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "UPDATE VideoRecordingVideoFiles SET " &
                                      "cmPosition = @Position, " &
                                      "cmClockDiscrepancy = @ClockDiscrepancy, " &
                                      "cmVisible = @Visible, " &
                                      "cmComment = @Comment, " &
                                      "cmCallibrationOK = @CallibrationOK, " &
                                      "cmr1 = @r1, " &
                                      "cmr2 = @r2, " &
                                      "cmr3 = @r3, " &
                                      "cmr4 = @r4, " &
                                      "cmr5 = @r5, " &
                                      "cmr6 = @r6, " &
                                      "cmr7 = @r7, " &
                                      "cmr8 = @r8, " &
                                      "cmr9 = @r9, " &
                                      "cmTx = @Tx, " &
                                      "cmTy = @Ty, " &
                                      "cmTz = @Tz, " &
                                      "cmf = @f, " &
                                      "cmk = @k, " &
                                      "cmSx = @Sx, " &
                                      "cmdx = @dx, " &
                                      "cmdy = @dy, " &
                                      "cmCx = @Cx, " &
                                      "cmCy = @Cy " &
                                      "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Position", _Rows(0)("cmPosition"))
          _OleDbCommand.Parameters.AddWithValue("@ClockDiscrepancy", _Rows(0)("cmClockDiscrepancy"))
          _OleDbCommand.Parameters.AddWithValue("@Visible", _Rows(0)("cmVisible"))
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Rows(0)("cmComment"))
          _OleDbCommand.Parameters.AddWithValue("@CallibrationOK", _Rows(0)("cmCallibrationOK"))
          _OleDbCommand.Parameters.AddWithValue("@r1", _Rows(0)("cmr1"))
          _OleDbCommand.Parameters.AddWithValue("@r2", _Rows(0)("cmr2"))
          _OleDbCommand.Parameters.AddWithValue("@r3", _Rows(0)("cmr3"))
          _OleDbCommand.Parameters.AddWithValue("@r4", _Rows(0)("cmr4"))
          _OleDbCommand.Parameters.AddWithValue("@r5", _Rows(0)("cmr5"))
          _OleDbCommand.Parameters.AddWithValue("@r6", _Rows(0)("cmr6"))
          _OleDbCommand.Parameters.AddWithValue("@r7", _Rows(0)("cmr7"))
          _OleDbCommand.Parameters.AddWithValue("@r8", _Rows(0)("cmr8"))
          _OleDbCommand.Parameters.AddWithValue("@r9", _Rows(0)("cmr9"))
          _OleDbCommand.Parameters.AddWithValue("@Tx", _Rows(0)("cmTx"))
          _OleDbCommand.Parameters.AddWithValue("@Ty", _Rows(0)("cmTy"))
          _OleDbCommand.Parameters.AddWithValue("@Tz", _Rows(0)("cmTz"))
          _OleDbCommand.Parameters.AddWithValue("@f", _Rows(0)("cmf"))
          _OleDbCommand.Parameters.AddWithValue("@k", _Rows(0)("cmk"))
          _OleDbCommand.Parameters.AddWithValue("@Sx", _Rows(0)("cmSx"))
          _OleDbCommand.Parameters.AddWithValue("@dx", _Rows(0)("cmdx"))
          _OleDbCommand.Parameters.AddWithValue("@dy", _Rows(0)("cmdy"))
          _OleDbCommand.Parameters.AddWithValue("@Cx", _Rows(0)("cmCx"))
          _OleDbCommand.Parameters.AddWithValue("@Cy", _Rows(0)("cmCy"))
          _OleDbCommand.Parameters.AddWithValue("@ID", _VideoFile.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _VideoFile.Status = VideoFileStatuses.Unchanged

        Case VideoFileStatuses.Created
          _Row = MyDataSet.Tables("VideoRecordingVideoFiles").NewRow
          MyMaxVideoFileID_VR = MyMaxVideoFileID_VR + 1
          _VideoFile.ID = MyMaxVideoFileID_VR
          _Row("cmID") = _VideoFile.ID
          _Row("cmVideoFile") = _VideoFile.VideoFileName
          _Row("cmOwner") = _VideoFile.ParentRecord.ID
          _Row("cmPosition") = _VideoFile.Position
          _Row("cmClockDiscrepancy") = _VideoFile.ClockDiscrepancy
          _Row("cmVisible") = _VideoFile.Visible
          _Row("cmComment") = _VideoFile.Comment
          _Row("cmCallibrationOK") = _VideoFile.CallibrationOK
          _Row("cmr1") = SetSQLValueDouble(_VideoFile.TSAI.r1)
          _Row("cmr2") = SetSQLValueDouble(_VideoFile.TSAI.r2)
          _Row("cmr3") = SetSQLValueDouble(_VideoFile.TSAI.r3)
          _Row("cmr4") = SetSQLValueDouble(_VideoFile.TSAI.r4)
          _Row("cmr5") = SetSQLValueDouble(_VideoFile.TSAI.r5)
          _Row("cmr6") = SetSQLValueDouble(_VideoFile.TSAI.r6)
          _Row("cmr7") = SetSQLValueDouble(_VideoFile.TSAI.r7)
          _Row("cmr8") = SetSQLValueDouble(_VideoFile.TSAI.r8)
          _Row("cmr9") = SetSQLValueDouble(_VideoFile.TSAI.r9)
          _Row("cmTx") = SetSQLValueDouble(_VideoFile.TSAI.Tx)
          _Row("cmTy") = SetSQLValueDouble(_VideoFile.TSAI.Ty)
          _Row("cmTz") = SetSQLValueDouble(_VideoFile.TSAI.Tz)
          _Row("cmf") = SetSQLValueDouble(_VideoFile.TSAI.f)
          _Row("cmk") = SetSQLValueDouble(_VideoFile.TSAI.k)
          _Row("cmSx") = SetSQLValueDouble(_VideoFile.TSAI.Sx)
          _Row("cmdx") = SetSQLValueDouble(_VideoFile.TSAI.dx)
          _Row("cmdy") = SetSQLValueDouble(_VideoFile.TSAI.dy)
          _Row("cmCx") = SetSQLValueDouble(_VideoFile.TSAI.Cx)
          _Row("cmCy") = SetSQLValueDouble(_VideoFile.TSAI.Cy)
          MyDataSet.Tables("VideoRecordingVideoFiles").Rows.Add(_Row)
          MyDataSet.Tables("VideoRecordingVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "INSERT INTO VideoRecordingVideoFiles " &
                                      "(cmID, cmVideoFile, cmOwner, cmPosition, cmClockDiscrepancy, cmVisible, cmComment, cmCallibrationOK, " &
                                      "cmr1, cmr2, cmr3, cmr4, cmr5, cmr6, cmr7, cmr8, cmr9, cmTx, cmTy, cmTz, cmf, cmk, cmSx, cmdx, cmdy, cmCx, cmCy) " &
                                      "VALUES (@ID, @VideoFile, @Owner, @Position, @ClockDiscrepancy, @Visible, @Comment, @CallibrationOK, " &
                                      "@r1, @r2, @r3, @r4, @r5, @r6, @r7, @r8, @r9, @Tx, @Ty, @Tz, @f, @k, @Sx, @dx, @dy, @Cx, @Cy)"

          _OleDbCommand.Parameters.AddWithValue("@ID", _Row("cmID"))
          _OleDbCommand.Parameters.AddWithValue("@VideoFile", _Row("cmVideoFile"))
          _OleDbCommand.Parameters.AddWithValue("@Owner", _Row("cmOwner"))
          _OleDbCommand.Parameters.AddWithValue("@Position", _Row("cmPosition"))
          _OleDbCommand.Parameters.AddWithValue("@ClockDiscrepancy", _Row("cmClockDiscrepancy"))
          _OleDbCommand.Parameters.AddWithValue("@Visible", _Row("cmVisible"))
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Row("cmComment"))
          _OleDbCommand.Parameters.AddWithValue("@CallibrationOK", _Row("cmCallibrationOK"))
          _OleDbCommand.Parameters.AddWithValue("@r1", _Row("cmr1"))
          _OleDbCommand.Parameters.AddWithValue("@r2", _Row("cmr2"))
          _OleDbCommand.Parameters.AddWithValue("@r3", _Row("cmr3"))
          _OleDbCommand.Parameters.AddWithValue("@r4", _Row("cmr4"))
          _OleDbCommand.Parameters.AddWithValue("@r5", _Row("cmr5"))
          _OleDbCommand.Parameters.AddWithValue("@r6", _Row("cmr6"))
          _OleDbCommand.Parameters.AddWithValue("@r7", _Row("cmr7"))
          _OleDbCommand.Parameters.AddWithValue("@r8", _Row("cmr8"))
          _OleDbCommand.Parameters.AddWithValue("@r9", _Row("cmr9"))
          _OleDbCommand.Parameters.AddWithValue("@Tx", _Row("cmTx"))
          _OleDbCommand.Parameters.AddWithValue("@Ty", _Row("cmTy"))
          _OleDbCommand.Parameters.AddWithValue("@Tz", _Row("cmTz"))
          _OleDbCommand.Parameters.AddWithValue("@f", _Row("cmf"))
          _OleDbCommand.Parameters.AddWithValue("@k", _Row("cmk"))
          _OleDbCommand.Parameters.AddWithValue("@Sx", _Row("cmSx"))
          _OleDbCommand.Parameters.AddWithValue("@dx", _Row("cmdx"))
          _OleDbCommand.Parameters.AddWithValue("@dy", _Row("cmdy"))
          _OleDbCommand.Parameters.AddWithValue("@Cx", _Row("cmCx"))
          _OleDbCommand.Parameters.AddWithValue("@Cy", _Row("cmCy"))
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _VideoFile.Status = VideoFileStatuses.Unchanged



        Case VideoFileStatuses.Deleted
          _DeletedVideoFiles.Add(_VideoFile)

          _Rows = MyDataSet.Tables("VideoRecordingVideoFiles").Select("cmID = " & _VideoFile.ID)
          If _Rows.Count > 0 Then _Rows(0).Delete()
          MyDataSet.Tables("VideoRecordingVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "DELETE * FROM VideoRecordingVideoFiles WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@ID", _VideoFile.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
      End Select
    Next

    For Each _VideoFile In _DeletedVideoFiles
      RecordVR.VideoFiles.Remove(_VideoFile)
    Next

  End Sub




  Protected Sub SaveVideoFilesDT(RecordDT As clsRecord)
    Dim _VideoFile As clsVideoFile
    Dim _Row As DataRow
    Dim _Rows() As DataRow
    Dim _OleDbCommand As OleDbCommand
    Dim _DeletedVideoFiles As List(Of clsVideoFile)



    _DeletedVideoFiles = New List(Of clsVideoFile)

    For Each _VideoFile In RecordDT.VideoFiles
      Select Case _VideoFile.Status
        Case VideoFileStatuses.Updated
          _Rows = MyDataSet.Tables("DetectionVideoFiles").Select("cmID = " & _VideoFile.ID)
          _Rows(0)("cmVideoFile") = _VideoFile.VideoFileName
          _Rows(0)("cmPosition") = _VideoFile.Position
          _Rows(0)("cmClockDiscrepancy") = _VideoFile.ClockDiscrepancy
          _Rows(0)("cmVisible") = _VideoFile.Visible
          _Rows(0)("cmComment") = _VideoFile.Comment
          _Rows(0)("cmCallibrationOK") = _VideoFile.CallibrationOK
          _Rows(0)("cmr1") = SetSQLValueDouble(_VideoFile.TSAI.r1)
          _Rows(0)("cmr2") = SetSQLValueDouble(_VideoFile.TSAI.r2)
          _Rows(0)("cmr3") = SetSQLValueDouble(_VideoFile.TSAI.r3)
          _Rows(0)("cmr4") = SetSQLValueDouble(_VideoFile.TSAI.r4)
          _Rows(0)("cmr5") = SetSQLValueDouble(_VideoFile.TSAI.r5)
          _Rows(0)("cmr6") = SetSQLValueDouble(_VideoFile.TSAI.r6)
          _Rows(0)("cmr7") = SetSQLValueDouble(_VideoFile.TSAI.r7)
          _Rows(0)("cmr8") = SetSQLValueDouble(_VideoFile.TSAI.r8)
          _Rows(0)("cmr9") = SetSQLValueDouble(_VideoFile.TSAI.r9)
          _Rows(0)("cmTx") = SetSQLValueDouble(_VideoFile.TSAI.Tx)
          _Rows(0)("cmTy") = SetSQLValueDouble(_VideoFile.TSAI.Ty)
          _Rows(0)("cmTz") = SetSQLValueDouble(_VideoFile.TSAI.Tz)
          _Rows(0)("cmf") = SetSQLValueDouble(_VideoFile.TSAI.f)
          _Rows(0)("cmk") = SetSQLValueDouble(_VideoFile.TSAI.k)
          _Rows(0)("cmSx") = SetSQLValueDouble(_VideoFile.TSAI.Sx)
          _Rows(0)("cmdx") = SetSQLValueDouble(_VideoFile.TSAI.dx)
          _Rows(0)("cmdy") = SetSQLValueDouble(_VideoFile.TSAI.dy)
          _Rows(0)("cmCx") = SetSQLValueDouble(_VideoFile.TSAI.Cx)
          _Rows(0)("cmCy") = SetSQLValueDouble(_VideoFile.TSAI.Cy)
          MyDataSet.Tables("DetectionVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "UPDATE DetectionVideoFiles SET " &
                                      "cmPosition = @Position, " &
                                      "cmClockDiscrepancy = @ClockDiscrepancy, " &
                                      "cmVisible = @Visible, " &
                                      "cmComment = @Comment, " &
                                      "cmCallibrationOK = @CallibrationOK, " &
                                      "cmr1 = @r1, " &
                                      "cmr2 = @r2, " &
                                      "cmr3 = @r3, " &
                                      "cmr4 = @r4, " &
                                      "cmr5 = @r5, " &
                                      "cmr6 = @r6, " &
                                      "cmr7 = @r7, " &
                                      "cmr8 = @r8, " &
                                      "cmr9 = @r9, " &
                                      "cmTx = @Tx, " &
                                      "cmTy = @Ty, " &
                                      "cmTz = @Tz, " &
                                      "cmf = @f, " &
                                      "cmk = @k, " &
                                      "cmSx = @Sx, " &
                                      "cmdx = @dx, " &
                                      "cmdy = @dy, " &
                                      "cmCx = @Cx, " &
                                      "cmCy = @Cy " &
                                      "WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@Position", _Rows(0)("cmPosition"))
          _OleDbCommand.Parameters.AddWithValue("@ClockDiscrepancy", _Rows(0)("cmClockDiscrepancy"))
          _OleDbCommand.Parameters.AddWithValue("@Visible", _Rows(0)("cmVisible"))
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Rows(0)("cmComment"))
          _OleDbCommand.Parameters.AddWithValue("@CallibrationOK", _Rows(0)("cmCallibrationOK"))
          _OleDbCommand.Parameters.AddWithValue("@r1", _Rows(0)("cmr1"))
          _OleDbCommand.Parameters.AddWithValue("@r2", _Rows(0)("cmr2"))
          _OleDbCommand.Parameters.AddWithValue("@r3", _Rows(0)("cmr3"))
          _OleDbCommand.Parameters.AddWithValue("@r4", _Rows(0)("cmr4"))
          _OleDbCommand.Parameters.AddWithValue("@r5", _Rows(0)("cmr5"))
          _OleDbCommand.Parameters.AddWithValue("@r6", _Rows(0)("cmr6"))
          _OleDbCommand.Parameters.AddWithValue("@r7", _Rows(0)("cmr7"))
          _OleDbCommand.Parameters.AddWithValue("@r8", _Rows(0)("cmr8"))
          _OleDbCommand.Parameters.AddWithValue("@r9", _Rows(0)("cmr9"))
          _OleDbCommand.Parameters.AddWithValue("@Tx", _Rows(0)("cmTx"))
          _OleDbCommand.Parameters.AddWithValue("@Ty", _Rows(0)("cmTy"))
          _OleDbCommand.Parameters.AddWithValue("@Tz", _Rows(0)("cmTz"))
          _OleDbCommand.Parameters.AddWithValue("@f", _Rows(0)("cmf"))
          _OleDbCommand.Parameters.AddWithValue("@k", _Rows(0)("cmk"))
          _OleDbCommand.Parameters.AddWithValue("@Sx", _Rows(0)("cmSx"))
          _OleDbCommand.Parameters.AddWithValue("@dx", _Rows(0)("cmdx"))
          _OleDbCommand.Parameters.AddWithValue("@dy", _Rows(0)("cmdy"))
          _OleDbCommand.Parameters.AddWithValue("@Cx", _Rows(0)("cmCx"))
          _OleDbCommand.Parameters.AddWithValue("@Cy", _Rows(0)("cmCy"))
          _OleDbCommand.Parameters.AddWithValue("@ID", _VideoFile.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _VideoFile.Status = VideoFileStatuses.Unchanged

        Case VideoFileStatuses.Created
          _Row = MyDataSet.Tables("DetectionVideoFiles").NewRow
          MyMaxVideoFileID_DT = MyMaxVideoFileID_DT + 1
          _VideoFile.ID = MyMaxVideoFileID_DT
          _Row("cmID") = _VideoFile.ID
          _Row("cmVideoFile") = _VideoFile.VideoFileName
          _Row("cmOwner") = RecordDT.ID
          _Row("cmPosition") = _VideoFile.Position
          _Row("cmClockDiscrepancy") = _VideoFile.ClockDiscrepancy
          _Row("cmVisible") = _VideoFile.Visible
          _Row("cmComment") = _VideoFile.Comment
          _Row("cmComment") = _VideoFile.Comment
          _Row("cmCallibrationOK") = _VideoFile.CallibrationOK
          _Row("cmr1") = SetSQLValueDouble(_VideoFile.TSAI.r1)
          _Row("cmr2") = SetSQLValueDouble(_VideoFile.TSAI.r2)
          _Row("cmr3") = SetSQLValueDouble(_VideoFile.TSAI.r3)
          _Row("cmr4") = SetSQLValueDouble(_VideoFile.TSAI.r4)
          _Row("cmr5") = SetSQLValueDouble(_VideoFile.TSAI.r5)
          _Row("cmr6") = SetSQLValueDouble(_VideoFile.TSAI.r6)
          _Row("cmr7") = SetSQLValueDouble(_VideoFile.TSAI.r7)
          _Row("cmr8") = SetSQLValueDouble(_VideoFile.TSAI.r8)
          _Row("cmr9") = SetSQLValueDouble(_VideoFile.TSAI.r9)
          _Row("cmTx") = SetSQLValueDouble(_VideoFile.TSAI.Tx)
          _Row("cmTy") = SetSQLValueDouble(_VideoFile.TSAI.Ty)
          _Row("cmTz") = SetSQLValueDouble(_VideoFile.TSAI.Tz)
          _Row("cmf") = SetSQLValueDouble(_VideoFile.TSAI.f)
          _Row("cmk") = SetSQLValueDouble(_VideoFile.TSAI.k)
          _Row("cmSx") = SetSQLValueDouble(_VideoFile.TSAI.Sx)
          _Row("cmdx") = SetSQLValueDouble(_VideoFile.TSAI.dx)
          _Row("cmdy") = SetSQLValueDouble(_VideoFile.TSAI.dy)
          _Row("cmCx") = SetSQLValueDouble(_VideoFile.TSAI.Cx)
          _Row("cmCy") = SetSQLValueDouble(_VideoFile.TSAI.Cy)
          MyDataSet.Tables("DetectionVideoFiles").Rows.Add(_Row)
          MyDataSet.Tables("DetectionVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "INSERT INTO DetectionVideoFiles " &
                                      "(cmID, cmVideoFile, cmOwner, cmPosition, cmClockDiscrepancy, cmVisible, cmComment, cmCallibrationOK, " &
                                      "cmr1, cmr2, cmr3, cmr4, cmr5, cmr6, cmr7, cmr8, cmr9, cmTx, cmTy, cmTz, cmf, cmk, cmSx, cmdx, cmdy, cmCx, cmCy) " &
                                      "VALUES (@ID, @VideoFile, @Owner, @Position, @ClockDiscrepancy, @Visible, @Comment, @CallibrationOK, " &
                                      "@r1, @r2, @r3, @r4, @r5, @r6, @r7, @r8, @r9, @Tx, @Ty, @Tz, @f, @k, @Sx, @dx, @dy, @Cx, @Cy)"

          _OleDbCommand.Parameters.AddWithValue("@ID", _Row("cmID"))
          _OleDbCommand.Parameters.AddWithValue("@VideoFile", _Row("cmVideoFile"))
          _OleDbCommand.Parameters.AddWithValue("@Owner", _Row("cmOwner"))
          _OleDbCommand.Parameters.AddWithValue("@Position", _Row("cmPosition"))
          _OleDbCommand.Parameters.AddWithValue("@ClockDiscrepancy", _Row("cmClockDiscrepancy"))
          _OleDbCommand.Parameters.AddWithValue("@Visible", _Row("cmVisible"))
          _OleDbCommand.Parameters.AddWithValue("@Comment", _Row("cmComment"))
          _OleDbCommand.Parameters.AddWithValue("@CallibrationOK", _Row("cmCallibrationOK"))
          _OleDbCommand.Parameters.AddWithValue("@r1", _Row("cmr1"))
          _OleDbCommand.Parameters.AddWithValue("@r2", _Row("cmr2"))
          _OleDbCommand.Parameters.AddWithValue("@r3", _Row("cmr3"))
          _OleDbCommand.Parameters.AddWithValue("@r4", _Row("cmr4"))
          _OleDbCommand.Parameters.AddWithValue("@r5", _Row("cmr5"))
          _OleDbCommand.Parameters.AddWithValue("@r6", _Row("cmr6"))
          _OleDbCommand.Parameters.AddWithValue("@r7", _Row("cmr7"))
          _OleDbCommand.Parameters.AddWithValue("@r8", _Row("cmr8"))
          _OleDbCommand.Parameters.AddWithValue("@r9", _Row("cmr9"))
          _OleDbCommand.Parameters.AddWithValue("@Tx", _Row("cmTx"))
          _OleDbCommand.Parameters.AddWithValue("@Ty", _Row("cmTy"))
          _OleDbCommand.Parameters.AddWithValue("@Tz", _Row("cmTz"))
          _OleDbCommand.Parameters.AddWithValue("@f", _Row("cmf"))
          _OleDbCommand.Parameters.AddWithValue("@k", _Row("cmk"))
          _OleDbCommand.Parameters.AddWithValue("@Sx", _Row("cmSx"))
          _OleDbCommand.Parameters.AddWithValue("@dx", _Row("cmdx"))
          _OleDbCommand.Parameters.AddWithValue("@dy", _Row("cmdy"))
          _OleDbCommand.Parameters.AddWithValue("@Cx", _Row("cmCx"))
          _OleDbCommand.Parameters.AddWithValue("@Cy", _Row("cmCy"))
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
          _VideoFile.Status = VideoFileStatuses.Unchanged



        Case VideoFileStatuses.Deleted
          _DeletedVideoFiles.Add(_VideoFile)

          _Rows = MyDataSet.Tables("DetectionVideoFiles").Select("cmID = " & _VideoFile.ID)
          If _Rows.Count > 0 Then _Rows(0).Delete()
          MyDataSet.Tables("DetectionVideoFiles").AcceptChanges()

          _OleDbCommand = New OleDbCommand
          _OleDbCommand.Connection = MyOleDBConnection
          _OleDbCommand.CommandText = "DELETE * FROM DetectionVideoFiles WHERE cmID = @ID"
          _OleDbCommand.Parameters.AddWithValue("@ID", _VideoFile.ID)
          _OleDbCommand.ExecuteNonQuery()
          _OleDbCommand.Dispose()
      End Select
    Next

    For Each _VideoFile In _DeletedVideoFiles
      RecordDT.VideoFiles.Remove(_VideoFile)
    Next

  End Sub


#End Region



#Region "Protected methods"


  Protected Function CreateDB(ByVal DatabaseFilePath As String) As Boolean
    Dim _OleDbConnection As OleDbConnection
    Dim _OleDbCommand As OleDbCommand


    _OleDbConnection = Nothing

    Try
      FileCopy("Data.accdb", DatabaseFilePath)

      _OleDbConnection = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & DatabaseFilePath)
      _OleDbConnection.Open()

      _OleDbCommand = New OleDbCommand("CREATE TABLE DetectionControls " &
                                      "([cmID] LONG PRIMARY KEY, " &
                                       "[cmName] TEXT (255), " &
                                       "[cmType] LONG, " &
                                       "[cmLabel] TEXT (255), " &
                                       "[cmPosition] LONG, " &
                                       "[cmScale] LONG, " &
                                       "[cmDecimals] LONG, " &
                                       "[cmComment] TEXT (255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (0, 'DemoText', 1, 'demo text', 0, 1, 0, 'demonstration of a user-defined text field')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (1, 'DemoNumber', 2, 'demo number', 1, 1, 1, 'demonstration of a user-defined number field')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (2, 'DemoYesNo', 3, 'demo yes/no', 2, 1, 0, 'demonstration of a user-defined yes/no field')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (3, 'placeholder', 5, 'placeholder', 3, 1, 0, 'demonstration of a placeholder')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (4, 'DemoList', 4, 'demo list', 4, 3, 0, 'demonstration of a user-defined list')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionControls (cmID, cmName, cmType, cmLabel, cmPosition, cmScale, cmDecimals, cmComment) " &
                                       "VALUES (5, 'placeholder', 5, 'placeholder', 5, 1, 0, 'demonstration of a placeholder')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbCommand = New OleDbCommand("CREATE TABLE uDT_DemoListValues " &
                                      "([cmID] LONG PRIMARY KEY, " &
                                       "[cmName] TEXT (255), " &
                                       "[cmPosition] LONG, " &
                                       "[cmComment] TEXT (255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO uDT_DemoListValues (cmID, cmName, cmPosition, cmComment) " &
                                       "VALUES (0, 'no value', 0, '')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO uDT_DemoListValues (cmID, cmName, cmPosition, cmComment) " &
                                       "VALUES (1, 'List value 1', 1, 'comment 1')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO uDT_DemoListValues (cmID, cmName, cmPosition, cmComment) " &
                                       "VALUES (2, 'List value 2', 2, 'comment 2')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()





      _OleDbCommand = New OleDbCommand("CREATE TABLE Detections " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmDateTime] DATE, " &
                                       "[cmStatus] LONG, " &
                                       "[cmType] LONG, " &
                                       "[cmComment] TEXT, " &
                                       "[cmMapAvailable] YESNO, " &
                                       "[cmMap] TEXT (255), " &
                                       "[cmMapWidth] LONG, " &
                                       "[cmMapHeight] LONG, " &
                                       "[cmMapScale] DOUBLE, " &
                                       "[cmMapX] DOUBLE, " &
                                       "[cmMapY] DOUBLE, " &
                                       "[cmMapDx] DOUBLE, " &
                                       "[cmMapDy] DOUBLE, " &
                                       "[cmMapVisible] YESNO, " &
                                       "[cmMapComment] TEXT, " &
                                       "[cmMedia] TEXT(255), " &
                                       "[cmStartTime] DOUBLE, " &
                                       "[cmFrameCount] LONG, " &
                                       "[cmFrameRate] DOUBLE, " &
                                       "[cmVideoRecording] LONG, " &
                                       "[uDT_DemoText] Text(255), " &
                                       "[uDT_DemoNumber] DOUBLE, " &
                                       "[uDT_DemoYesNo] YESNO, " &
                                       "[uDT_DemoList] LONG)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()



      _OleDbCommand = New OleDbCommand("CREATE TABLE DetectionStatus " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmName] TEXT (255), " &
                                       "[cmPosition] LONG, " &
                                       "[cmComment] TEXT(255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionStatus (cmID, cmName, cmPosition, cmComment) " &
                                     "VALUES (0, 'not checked', 0, 'Comment 1')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionStatus (cmID, cmName, cmPosition, cmComment) " &
                                 "VALUES (1, 'OK', 1, 'Comment 2')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionStatus (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (2, 'Status 2', 2, 'Comment 3')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionStatus (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (3, 'Status 3', 3, 'Comment 4')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()



      _OleDbCommand = New OleDbCommand("CREATE TABLE DetectionType " &
                                           "([cmID] LONG PRIMARY KEY, " &
                                           "[cmName] TEXT (255), " &
                                           "[cmPosition] LONG, " &
                                           "[cmComment] TEXT(255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionType (cmID, cmName, cmPosition, cmComment) " &
                                     "VALUES (0, 'not known', 0, 'Comment 1')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionType (cmID, cmName, cmPosition, cmComment) " &
                                 "VALUES (1, 'Type 1', 1, 'Comment 2')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionType (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (2, 'Type 2', 2, 'Comment 3')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO DetectionType (cmID, cmName, cmPosition, cmComment) " &
                         "VALUES (3, 'Type 3', 3, 'Comment 4')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()


      _OleDbCommand = New OleDbCommand("CREATE TABLE DetectionVideoFiles " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmVideoFile] TEXT (255), " &
                                       "[cmOwner] LONG, " &
                                       "[cmPosition] LONG, " &
                                       "[cmClockDiscrepancy] DOUBLE, " &
                                       "[cmVisible] YESNO, " &
                                       "[cmComment] TEXT, " &
                                       "[cmCallibrationOK] YESNO, " &
                                       "[cmr1] DOUBLE, " &
                                       "[cmr2] DOUBLE, " &
                                       "[cmr3] DOUBLE, " &
                                       "[cmr4] DOUBLE, " &
                                       "[cmr5] DOUBLE, " &
                                       "[cmr6] DOUBLE, " &
                                       "[cmr7] DOUBLE, " &
                                       "[cmr8] DOUBLE, " &
                                       "[cmr9] DOUBLE, " &
                                       "[cmTx] DOUBLE, " &
                                       "[cmTy] DOUBLE, " &
                                       "[cmTz] DOUBLE, " &
                                       "[cmf] DOUBLE, " &
                                       "[cmk] DOUBLE, " &
                                       "[cmSx] DOUBLE, " &
                                       "[cmdx] DOUBLE, " &
                                       "[cmdy] DOUBLE, " &
                                       "[cmCx] DOUBLE, " &
                                       "[cmCy] DOUBLE)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbCommand = New OleDbCommand("CREATE TABLE RoadUserTypes " &
                                   "([cmID] LONG PRIMARY KEY, " &
                                   "[cmName] TEXT (255), " &
                                   "[cmLength] DOUBLE, " &
                                   "[cmWidth] DOUBLE, " &
                                   "[cmHeight] DOUBLE, " &
                                   "[cmWeight] INTEGER, " &
                                   "[cmL2] DOUBLE, " &
                                   "[cmH2] DOUBLE, " &
                                   "[cmL3] DOUBLE, " &
                                   "[cmH3] DOUBLE, " &
                                   "[cmL4] DOUBLE, " &
                                   "[cmH4] DOUBLE, " &
                                   "[cmL5] DOUBLE, " &
                                   "[cmH5] DOUBLE, " &
                                   "[cmL6] DOUBLE, " &
                                   "[cmH6] DOUBLE, " &
                                   "[cmL7] DOUBLE, " &
                                   "[cmH7] DOUBLE)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                       "VALUES (0, 'unknown', 4.2, 1.57, 1.36, 1300, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                       "VALUES (1, 'car', 4.2, 1.57, 1.36, 1300, 0, 0.5, 0.29, 0.67, 0.45, 1, 0.77, 1, 0.93, 0.71, 1, 0.71)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                      " VALUES (6, 'minivan', 5.3, 2, 2.2, 2700, 0, 0.4, 0.2, 1, 0.2, 1, 1, 1, 1, 0, 1, 0)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                       "VALUES (11, 'truck', 10, 2, 2.8, 10000, 0, 0.4, 0.2, 1, 0.2, 1, 1, 1, 1, 0, 1, 0)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                       "VALUES (21, 'bus', 16, 2, 2.8, 14400, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                      "VALUES (31, 'bicycle', 1.8, 0.5, 1.8, 90, 0, 0.4, 0, 0.4, 0.4, 1, 0.6, 1, 1, 0.4, 1, 0.4)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO RoadUserTypes (cmID, cmName, cmLength, cmWidth, cmHeight, cmWeight, cmL2, cmH2, cmL3, cmH3, cmL4, cmH4, cmL5, cmH5, cmL6, cmH6, cmL7, cmH7) " &
                                       "VALUES (41, 'pedestrian', 0.5, 0.5, 1.8, 80, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()


      _OleDbCommand = New OleDbCommand("CREATE TABLE VideoRecordingControls " &
                                      "([cmID] LONG PRIMARY KEY, " &
                                      "[cmName] TEXT (255), " &
                                      "[cmType] LONG, " &
                                      "[cmLabel] TEXT (255), " &
                                      "[cmPosition] LONG, " &
                                      "[cmScale] LONG, " &
                                      "[cmDecimals] LONG)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbCommand = New OleDbCommand("CREATE TABLE VideoRecordings " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmDateTime] DATE, " &
                                       "[cmStatus] LONG, " &
                                       "[cmType] LONG, " &
                                       "[cmComment] TEXT, " &
                                       "[cmMapAvailable] YESNO, " &
                                       "[cmMap] TEXT (50), " &
                                       "[cmMapWidth] LONG, " &
                                       "[cmMapHeight] LONG, " &
                                       "[cmMapScale] DOUBLE, " &
                                       "[cmMapX] DOUBLE, " &
                                       "[cmMapY] DOUBLE, " &
                                       "[cmMapDx] DOUBLE, " &
                                       "[cmMapDy] DOUBLE, " &
                                       "[cmMapVisible] YESNO, " &
                                       "[cmMapComment] TEXT, " &
                                       "[cmMedia] TEXT(255), " &
                                       "[cmStartTime] DOUBLE, " &
                                       "[cmFrameCount] LONG, " &
                                       "[cmFrameRate] DOUBLE)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()



      _OleDbCommand = New OleDbCommand("CREATE TABLE VideoRecordingStatus " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmName] TEXT (255), " &
                                       "[cmPosition] Long, " &
                                       "[cmComment] TEXT(255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingStatus (cmID, cmName, cmPosition, cmComment) " &
                                  "VALUES (0, 'not viewed', 0, 'Comment 1')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingStatus (cmID, cmName, cmPosition, cmComment) " &
                                 "VALUES (1, 'viewed', 1, 'Comment 2')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingStatus (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (2, 'Status 2', 2, 'Comment 3')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingStatus (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (3, 'Status 3', 3, 'Comment 4')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()



      _OleDbCommand = New OleDbCommand("CREATE TABLE VideoRecordingType " &
                                           "([cmID] LONG PRIMARY KEY, " &
                                           "[cmName] TEXT (255), " &
                                           "[cmPosition] Long, " &
                                           "[cmComment] TEXT(255))", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingType (cmID, cmName, cmPosition, cmComment) " &
                                     "VALUES (0, 'not known', 0, 'Comment 1')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingType (cmID, cmName, cmPosition, cmComment) " &
                                 "VALUES (1, 'Type 1', 1, 'Comment 2')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingType (cmID, cmName, cmPosition, cmComment) " &
                             "VALUES (2, 'Type 2', 2, 'Comment 3')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordingType (cmID, cmName, cmPosition, cmComment) " &
                         "VALUES (3, 'Type 3', 3, 'Comment 4')", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()



      _OleDbCommand = New OleDbCommand("CREATE TABLE VideoRecordingVideoFiles " &
                                       "([cmID] LONG PRIMARY KEY, " &
                                       "[cmVideoFile] TEXT (255), " &
                                       "[cmOwner] LONG, " &
                                       "[cmPosition] LONG, " &
                                       "[cmClockDiscrepancy] DOUBLE, " &
                                       "[cmVisible] YESNO, " &
                                       "[cmComment] TEXT, " &
                                       "[cmCallibrationOK] YESNO, " &
                                       "[cmr1] DOUBLE, " &
                                       "[cmr2] DOUBLE, " &
                                       "[cmr3] DOUBLE, " &
                                       "[cmr4] DOUBLE, " &
                                       "[cmr5] DOUBLE, " &
                                       "[cmr6] DOUBLE, " &
                                       "[cmr7] DOUBLE, " &
                                       "[cmr8] DOUBLE, " &
                                       "[cmr9] DOUBLE, " &
                                       "[cmTx] DOUBLE, " &
                                       "[cmTy] DOUBLE, " &
                                       "[cmTz] DOUBLE, " &
                                       "[cmf] DOUBLE, " &
                                       "[cmk] DOUBLE, " &
                                       "[cmSx] DOUBLE, " &
                                       "[cmdx] DOUBLE, " &
                                       "[cmdy] DOUBLE, " &
                                       "[cmCx] DOUBLE, " &
                                       "[cmCy] DOUBLE)", _OleDbConnection)
      _OleDbCommand.ExecuteNonQuery()

      _OleDbConnection.Close()
      Return True

    Catch ex As Exception
      MsgBox("Problems creating the database file '" & MyDatabaseFilePath & "'!" & Environment.NewLine & Environment.NewLine & _
             "error message: """ & ex.Message & """", MsgBoxStyle.Critical)
      _OleDbConnection.Dispose()
      Return False
    End Try


  End Function


  'Makes a connection to the database file
  Protected Function ConnectDB(ByVal DatabaseFilePath As String) As Boolean

    MyDatabaseFilePath = DatabaseFilePath
    If File.Exists(MyDatabaseFilePath) Then
      MyOleDBConnection = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & MyDatabaseFilePath)
      Try
        MyOleDBConnection.Open()
      Catch ex As Exception
        MsgBox("Problems connecting to the database file '" & MyDatabaseFilePath & "'!" & Environment.NewLine & Environment.NewLine & _
               "error message: """ & ex.Message & """", MsgBoxStyle.Critical)
        MyOleDBConnection.Dispose()
        Return False
      End Try

      Call CheckUpdates()

      If ReadTables() Then
        MyFilterVR = ""
        MyFilterDT = ""
        Call SelectVideoRecordings()
        Call SelectDetections()
        Return True
      Else
        MyOleDBConnection.Close()
        MyOleDBConnection.Dispose()
        Return False
      End If
    Else 'If database file exists
      MsgBox("Cannot find the database file '" & MyDatabaseFilePath & "'!", MsgBoxStyle.Critical)
      Return False
    End If
  End Function

  Protected Sub DisconnectDB()

    If MyDataSet IsNot Nothing Then MyDataSet.Dispose()
    If MyOleDBConnection IsNot Nothing Then
      MyOleDBConnection.Close()
      MyOleDBConnection.Dispose()
    End If
  End Sub

  Protected Function GetRoadUserTypes() As List(Of clsRoadUserType)
    Dim _RoadUserTypes As List(Of clsRoadUserType)
    Dim _RoadUserType As clsRoadUserType
    Dim _Row As DataRow
    Dim _Rows() As DataRow

    _RoadUserTypes = New List(Of clsRoadUserType)
    _Rows = MyDataSet.Tables("RoadUserTypes").Select("", "cmID ASC")
    For Each _Row In _Rows
      _RoadUserType = New clsRoadUserType(GetSQLValueString(_Row("cmName")),
                                          GetSQLValueDouble(_Row("cmLength")),
                                          GetSQLValueDouble(_Row("cmWidth")),
                                          GetSQLValueDouble(_Row("cmHeight")),
                                          GetSQLValueInteger(_Row("cmWeight")),
                                          GetSQLValueDouble(_Row("cmL2")),
                                          GetSQLValueDouble(_Row("cmH2")),
                                          GetSQLValueDouble(_Row("cmL3")),
                                          GetSQLValueDouble(_Row("cmH3")),
                                          GetSQLValueDouble(_Row("cmL4")),
                                          GetSQLValueDouble(_Row("cmH4")),
                                          GetSQLValueDouble(_Row("cmL5")),
                                          GetSQLValueDouble(_Row("cmH5")),
                                          GetSQLValueDouble(_Row("cmL6")),
                                          GetSQLValueDouble(_Row("cmH6")),
                                          GetSQLValueDouble(_Row("cmL7")),
                                          GetSQLValueDouble(_Row("cmH7")))
      _RoadUserTypes.Add(_RoadUserType)
    Next

    Return _RoadUserTypes
  End Function

  Protected Sub UpdateRecordVR()
    Dim _OleDbCommand As OleDbCommand
    Dim _UserControl As clsUserControl
    Dim _UserDefinedString As String

    MyDataSet.Tables("VideoRecordings").AcceptChanges()

    _OleDbCommand = New OleDbCommand
    With _OleDbCommand
      .Connection = MyOleDBConnection
      _UserDefinedString = ""
      For Each _UserControl In Me.GetUserControlsVR
        _UserDefinedString = _UserDefinedString & "uVR_" & _UserControl.Name & " = @" & _UserControl.Name & ", "
        .Parameters.AddWithValue("@" & _UserControl.Name, MyVideoRecordingRows(MyRecordIndexVR)("uVR_" & _UserControl.Name))
      Next
      .CommandText = "UPDATE VideoRecordings SET " &
                     _UserDefinedString &
                    "cmMapAvailable = @MapAvailable, " &
                    "cmMap = @Map, " &
                    "cmMapWidth = @MapWidth, " &
                    "cmMapHeight = @MapHeight, " &
                    "cmMapScale = @MapScale, " &
                    "cmMapX = @MapX, " &
                    "cmMapY = @MapY, " &
                    "cmMapDx = @MapDx, " &
                    "cmMapDy = @MapDy, " &
                    "cmMapVisible = @MapVisible, " &
                    "cmMapComment = @MapComment, " &
                    "cmMedia = @Media, " &
                    "cmDateTime = @DateTime, " &
                    "cmStatus = @Status, " &
                    "cmType = @Type, " &
                    "cmComment = @Comment, " &
                    "cmStartTime = @StartTime, " &
                    "cmFrameCount = @FrameCount, " &
                    "cmFrameRate = @FrameRate " &
                    "WHERE cmID = @ID"
      .Parameters.AddWithValue("@MapAvailable", MyVideoRecordingRows(MyRecordIndexVR)("cmMapAvailable"))
      .Parameters.AddWithValue("@Map", MyVideoRecordingRows(MyRecordIndexVR)("cmMap"))
      .Parameters.AddWithValue("@MapWidth", MyVideoRecordingRows(MyRecordIndexVR)("cmMapWidth"))
      .Parameters.AddWithValue("@MapHeight", MyVideoRecordingRows(MyRecordIndexVR)("cmMapHeight"))
      .Parameters.AddWithValue("@MapScale", MyVideoRecordingRows(MyRecordIndexVR)("cmMapScale"))
      .Parameters.AddWithValue("@MapX", MyVideoRecordingRows(MyRecordIndexVR)("cmMapX"))
      .Parameters.AddWithValue("@MapY", MyVideoRecordingRows(MyRecordIndexVR)("cmMapY"))
      .Parameters.AddWithValue("@MapDx", MyVideoRecordingRows(MyRecordIndexVR)("cmMapDx"))
      .Parameters.AddWithValue("@MapDy", MyVideoRecordingRows(MyRecordIndexVR)("cmMapDy"))
      .Parameters.AddWithValue("@MapVisible", MyVideoRecordingRows(MyRecordIndexVR)("cmMapVisible"))
      .Parameters.AddWithValue("@MapComment", MyVideoRecordingRows(MyRecordIndexVR)("cmMapComment"))
      .Parameters.AddWithValue("@Media", MyVideoRecordingRows(MyRecordIndexVR)("cmMedia"))
      .Parameters.AddWithValue("@DateTime", MyVideoRecordingRows(MyRecordIndexVR)("cmDateTime").ToString)
      .Parameters.AddWithValue("@Status", MyVideoRecordingRows(MyRecordIndexVR)("cmStatus"))
      .Parameters.AddWithValue("@Type", MyVideoRecordingRows(MyRecordIndexVR)("cmType"))
      .Parameters.AddWithValue("@Comment", MyVideoRecordingRows(MyRecordIndexVR)("cmComment"))
      .Parameters.AddWithValue("@StartTime", MyVideoRecordingRows(MyRecordIndexVR)("cmStartTime"))
      .Parameters.AddWithValue("@FrameCount", MyVideoRecordingRows(MyRecordIndexVR)("cmFrameCount"))
      .Parameters.AddWithValue("@FrameRate", MyVideoRecordingRows(MyRecordIndexVR)("cmFrameRate"))
      .Parameters.AddWithValue("@ID", MyVideoRecordingRows(MyRecordIndexVR)("cmID"))
      .ExecuteNonQuery()
      .Dispose()
    End With
  End Sub

  Protected Sub UpdateRecordDT()
    Dim _OleDbCommand As OleDbCommand
    Dim _UserControl As clsUserControl
    Dim _UserDefinedString As String

    MyDataSet.Tables("Detections").AcceptChanges()

    _OleDbCommand = New OleDbCommand
    With _OleDbCommand
      .Connection = MyOleDBConnection
      _UserDefinedString = ""
      For Each _UserControl In Me.GetUserControlsDT
        If _UserControl.Type <> UserControlTypes.Placeholder Then
          _UserDefinedString = _UserDefinedString & "uDT_" & _UserControl.Name & " = @" & _UserControl.Name & ", "
          .Parameters.AddWithValue("@" & _UserControl.Name, MyDetectionRows(MyRecordIndexDT)("uDT_" & _UserControl.Name))
        End If
      Next
      .CommandText = "UPDATE Detections SET " &
                      _UserDefinedString &
                      "cmMapAvailable = @MapAvailable, " &
                      "cmMap = @Map, " &
                      "cmMapWidth = @MapWidth, " &
                      "cmMapHeight = @MapHeight, " &
                      "cmMapScale = @MapScale, " &
                      "cmMapX = @MapX, " &
                      "cmMapY = @MapY, " &
                      "cmMapDx = @MapDx, " &
                      "cmMapDy = @MapDy, " &
                      "cmMapVisible = @MapVisible, " &
                      "cmMapComment = @MapComment, " &
                      "cmMedia = @Media, " &
                      "cmDateTime = @DateTime, " &
                      "cmStatus = @Status, " &
                      "cmType = @Type, " &
                      "cmComment = @Comment, " &
                      "cmStartTime = @StartTime, " &
                      "cmFrameCount = @FrameCount, " &
                      "cmFrameRate = @FrameRate, " &
                      "cmVideoRecording = @VideoRecording " &
                      "WHERE cmID = @ID"
      .Parameters.AddWithValue("@MapAvailable", MyDetectionRows(MyRecordIndexDT)("cmMapAvailable"))
      .Parameters.AddWithValue("@Map", MyDetectionRows(MyRecordIndexDT)("cmMap"))
      .Parameters.AddWithValue("@MapWidth", MyDetectionRows(MyRecordIndexDT)("cmMapWidth"))
      .Parameters.AddWithValue("@MapHeight", MyDetectionRows(MyRecordIndexDT)("cmMapHeight"))
      .Parameters.AddWithValue("@MapScale", MyDetectionRows(MyRecordIndexDT)("cmMapScale"))
      .Parameters.AddWithValue("@MapX", MyDetectionRows(MyRecordIndexDT)("cmMapX"))
      .Parameters.AddWithValue("@MapY", MyDetectionRows(MyRecordIndexDT)("cmMapY"))
      .Parameters.AddWithValue("@MapDx", MyDetectionRows(MyRecordIndexDT)("cmMapDx"))
      .Parameters.AddWithValue("@MapDy", MyDetectionRows(MyRecordIndexDT)("cmMapDy"))
      .Parameters.AddWithValue("@MapVisible", MyDetectionRows(MyRecordIndexDT)("cmMapVisible"))
      .Parameters.AddWithValue("@MapComment", MyDetectionRows(MyRecordIndexDT)("cmMapComment"))
      .Parameters.AddWithValue("@Media", MyDetectionRows(MyRecordIndexDT)("cmMedia"))
      .Parameters.AddWithValue("@DateTime", MyDetectionRows(MyRecordIndexDT)("cmDateTime").ToString)
      .Parameters.AddWithValue("@Status", MyDetectionRows(MyRecordIndexDT)("cmStatus"))
      .Parameters.AddWithValue("@Type", MyDetectionRows(MyRecordIndexDT)("cmType"))
      .Parameters.AddWithValue("@Comment", MyDetectionRows(MyRecordIndexDT)("cmComment"))
      .Parameters.AddWithValue("@StartTime", MyDetectionRows(MyRecordIndexDT)("cmStartTime"))
      .Parameters.AddWithValue("@FrameCount", MyDetectionRows(MyRecordIndexDT)("cmFrameCount"))
      .Parameters.AddWithValue("@FrameRate", MyDetectionRows(MyRecordIndexDT)("cmFrameRate"))
      .Parameters.AddWithValue("@VideoRecording", MyDetectionRows(MyRecordIndexDT)("cmVideoRecording"))
      .Parameters.AddWithValue("@ID", MyDetectionRows(MyRecordIndexDT)("cmID"))
      .ExecuteNonQuery()
      .Dispose()
    End With

  End Sub

  Protected Function AddRecordVR(ByVal StartTime As Double,
                                 ByVal FrameCount As Integer,
                                 ByVal FrameRate As Double) As Integer
    Dim _OleDbCommand As OleDbCommand
    Dim _NewRow As DataRow
    Dim _NewRecordIndex As Integer


    MyMaxVideoRecordingID = MyMaxVideoRecordingID + 1
    _NewRow = MyDataSet.Tables("VideoRecordings").NewRow()
    _NewRow("cmID") = MyMaxVideoRecordingID
    _NewRow("cmStatus") = 0
    _NewRow("cmType") = 0
    _NewRow("cmStartTime") = StartTime
    _NewRow("cmFrameCount") = FrameCount
    _NewRow("cmFrameRate") = FrameRate
    MyDataSet.Tables("VideoRecordings").Rows.Add(_NewRow)
    MyDataSet.Tables("VideoRecordings").AcceptChanges()
    ReDim Preserve MyVideoRecordingRows(MyVideoRecordingRows.Count)
    _NewRecordIndex = MyVideoRecordingRows.Count - 1
    MyVideoRecordingRows(_NewRecordIndex) = _NewRow


    _OleDbCommand = New OleDbCommand("INSERT INTO VideoRecordings (cmID, cmStatus, cmType, cmStartTime, cmFrameCount, cmFrameRate)" &
                                     " VALUES (@ID, @Status, @Type, @StartTime, @FrameCount, @FrameRate)", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", MyVideoRecordingRows(_NewRecordIndex)("cmID"))
    _OleDbCommand.Parameters.AddWithValue("@Status", MyVideoRecordingRows(_NewRecordIndex)("cmStatus"))
    _OleDbCommand.Parameters.AddWithValue("@Type", MyVideoRecordingRows(_NewRecordIndex)("cmType"))
    _OleDbCommand.Parameters.AddWithValue("@StartTime", MyVideoRecordingRows(_NewRecordIndex)("cmStartTime"))
    _OleDbCommand.Parameters.AddWithValue("@FrameCount", MyVideoRecordingRows(_NewRecordIndex)("cmFrameCount"))
    _OleDbCommand.Parameters.AddWithValue("@FrameRate", MyVideoRecordingRows(_NewRecordIndex)("cmFrameRate"))
    _OleDbCommand.ExecuteNonQuery()

    Return _NewRecordIndex
  End Function

  Protected Function AddRecordDT(ByVal StartTime As Double,
                                 ByVal FrameCount As Integer,
                                 ByVal FrameRate As Double,
                                 Optional ByVal VideoRecording As Integer = NoValue) As Integer
    Dim _OleDbCommand As OleDbCommand
    Dim _NewRow As DataRow
    Dim _NewRecordIndex As Integer


    MyMaxDetectionID = MyMaxDetectionID + 1
    _NewRow = MyDataSet.Tables("Detections").NewRow()
    _NewRow("cmID") = MyMaxDetectionID
    _NewRow("cmStatus") = 0
    _NewRow("cmType") = 0
    _NewRow("cmStartTime") = StartTime
    _NewRow("cmFrameCount") = FrameCount
    _NewRow("cmFrameRate") = FrameRate
    If IsValue(VideoRecording) Then _NewRow("cmVideoRecording") = VideoRecording Else _NewRow("cmVideoRecording") = DBNull.Value

    MyDataSet.Tables("Detections").Rows.Add(_NewRow)
    MyDataSet.Tables("Detections").AcceptChanges()
    ReDim Preserve MyDetectionRows(MyDetectionRows.Count)
    _NewRecordIndex = MyDetectionRows.Count - 1
    MyDetectionRows(_NewRecordIndex) = _NewRow


    _OleDbCommand = New OleDbCommand("INSERT INTO Detections (cmID, cmStatus, cmType, cmStartTime, cmFrameCount, cmFrameRate, cmVideoRecording)" &
                                     " VALUES (@ID, @Status, @Type, @StartTime, @FrameCount, @FrameRate, @VideoRecording)", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", MyDetectionRows(_NewRecordIndex)("cmID"))
    _OleDbCommand.Parameters.AddWithValue("@Status", MyDetectionRows(_NewRecordIndex)("cmStatus"))
    _OleDbCommand.Parameters.AddWithValue("@Type", MyDetectionRows(_NewRecordIndex)("cmType"))
    _OleDbCommand.Parameters.AddWithValue("@StartTime", MyDetectionRows(_NewRecordIndex)("cmStartTime"))
    _OleDbCommand.Parameters.AddWithValue("@FrameCount", MyDetectionRows(_NewRecordIndex)("cmFrameCount"))
    _OleDbCommand.Parameters.AddWithValue("@FrameRate", MyDetectionRows(_NewRecordIndex)("cmFrameRate"))
    _OleDbCommand.Parameters.AddWithValue("@VideoRecording", MyDetectionRows(_NewRecordIndex)("cmVideoRecording"))
    _OleDbCommand.ExecuteNonQuery()

    Return _NewRecordIndex
  End Function


  Protected Function DeleteRecordVR(ByVal RecordID As Integer) As Integer
    Dim _OleDbCommand As OleDbCommand
    Dim _DataRows() As DataRow
    Dim _DataRow As DataRow


    _OleDbCommand = New OleDbCommand("UPDATE Detections SET " & _
                                     "cmVideoRecording = @Null " & _
                                     "WHERE cmVideoRecording = @ID", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@Null", DBNull.Value)
    _OleDbCommand.Parameters.AddWithValue("@ID", RecordID)
    _OleDbCommand.ExecuteNonQuery()

    _OleDbCommand = New OleDbCommand("DELETE * FROM VideoRecordingVideoFiles WHERE (cmOwner = @ID)", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", RecordID)
    _OleDbCommand.ExecuteNonQuery()

    _OleDbCommand = New OleDbCommand("DELETE * FROM VideoRecordings WHERE cmID = @ID", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", RecordID)
    _OleDbCommand.ExecuteNonQuery()


    _DataRows = MyDataSet.Tables("Detections").Select("cmVideoRecording = " & RecordID.ToString)
    For Each _DataRow In _DataRows
      _DataRow("cmVideoRecording") = DBNull.Value
    Next
    MyDataSet.Tables("Detections").AcceptChanges()

    _DataRows = MyDataSet.Tables("VideoRecordingVideoFiles").Select("cmOwner = " & RecordID.ToString)
    For Each _DataRow In _DataRows
      _DataRow.Delete()
    Next
    MyDataSet.Tables("VideoRecordingVideoFiles").AcceptChanges()

    _DataRows = MyDataSet.Tables("VideoRecordings").Select("cmID = " & RecordID.ToString)
    For Each _DataRow In _DataRows
      _DataRow.Delete()
    Next
    MyDataSet.Tables("VideoRecordings").AcceptChanges()

    Call SelectVideoRecordings()
    Call SelectDetections()
    Call FindMax()






    'Dim cmSQL As OleDbCommand
    'Dim i As Integer
    'Dim dbRows() As DataRow
    'Dim dbRow As DataRow

    ''  Select Case MyDataMode
    ''    Case DataModeVIDEORECORDINGS
    'cmSQL = New OleDbCommand("DELETE * FROM VideoRecordings WHERE cmID = @ID", MyOleDBConnection)
    ''  cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
    'cmSQL.ExecuteNonQuery()

    'cmSQL = New OleDbCommand("DELETE * FROM VideoFiles WHERE (cmDatamode = 1) AND (cmOwner = @ID)", MyOleDBConnection)
    ''  cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
    'cmSQL.ExecuteNonQuery()

    'cmSQL = New OleDbCommand("UPDATE Detections SET " & _
    '                         "cmVideoRecording = @Null " & _
    '                         "WHERE cmVideoRecording = @VideoRecordingID", MyOleDBConnection)
    'cmSQL.Parameters.AddWithValue("@Null", DBNull.Value)
    '' cmSQL.Parameters.AddWithValue("@ID", MyVideoRecordingRows(iMyRecordIndex)("cmID"))
    'cmSQL.ExecuteNonQuery()

    ''  dbRows = MyDataSet.Tables("Detections").Select("cmVideoRecording = " & MyVideoRecordingRows(iMyRecordIndex)("cmID").ToString)
    'For Each dbRow In dbRows
    '  dbRow("cmVideoRecording") = DBNull.Value
    'Next
    'MyDataSet.Tables("Detections").AcceptChanges()

    '' dbRows = MyDataSet.Tables("VideoFiles").Select("(cmDataMode = 1) AND (cmOwner = " & MyVideoRecordingRows(iMyRecordIndex)("cmID").ToString & ")")
    'For Each dbRow In dbRows
    '  dbRow.Delete()
    'Next
    'MyDataSet.Tables("VideoFiles").AcceptChanges()

    ''  MyVideoRecordingRows(iMyRecordIndex).Delete()
    'MyDataSet.Tables("VideoRecordings").AcceptChanges()
    'Call FindMax()
    'Call SelectVideoRecordings()

    ''    Case DataModeDETECTIONS
    'cmSQL = New OleDbCommand("DELETE * FROM Detections WHERE cmID = @ID", MyOleDBConnection)
    ''  cmSQL.Parameters.AddWithValue("@ID", MyDetectionRows(iMyRecordIndex)("cmID"))
    'cmSQL.ExecuteNonQuery()

    'cmSQL = New OleDbCommand("DELETE * FROM VideoFiles WHERE (cmDatamode = 2) AND (cmOwner = @ID)", MyOleDBConnection)
    '' cmSQL.Parameters.AddWithValue("@ID", MyDetectionRows(iMyRecordIndex)("cmID"))
    'cmSQL.ExecuteNonQuery()

    ''  dbRows = MyDataSet.Tables("VideoFiles").Select("(cmDataMode = 2) AND (cmOwner = " & MyDetectionRows(iMyRecordIndex)("cmID").ToString & ")")
    'For Each dbRow In dbRows
    '  dbRow.Delete()
    'Next
    'MyDataSet.Tables("VideoFiles").AcceptChanges()

    ''   MyDetectionRows(iMyRecordIndex).Delete()
    'MyDataSet.Tables("Detections").AcceptChanges()
    'Call FindMax()
    'Call SelectDetections()
    ''   End Select

    ''   If iMyRecordIndex > 0 Then
    ''i = iMyRecordIndex - 1
    ''  ElseIf NumberOfFilteredRecords > 0 Then
    ''  i = iMyRecordIndex
    ''  Else
    '' i = -1
    '' End If

    'Return i

    Return Nothing
  End Function


  Protected Sub DeleteRecordDT(ByVal RecordID As Integer)
    Dim _OleDbCommand As OleDbCommand
    Dim _DataRows() As DataRow
    Dim _DataRow As DataRow


    _OleDbCommand = New OleDbCommand("DELETE * FROM Detections WHERE cmID = @ID", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", RecordID)
    _OleDbCommand.ExecuteNonQuery()

    _OleDbCommand = New OleDbCommand("DELETE * FROM DetectionVideoFiles WHERE (cmOwner = @ID)", MyOleDBConnection)
    _OleDbCommand.Parameters.AddWithValue("@ID", RecordID)
    _OleDbCommand.ExecuteNonQuery()

    _DataRows = MyDataSet.Tables("DetectionVideoFiles").Select("cmOwner = " & RecordID.ToString)
    For Each _DataRow In _DataRows
      _DataRow.Delete()
    Next
    MyDataSet.Tables("DetectionVideoFiles").AcceptChanges()

    _DataRows = MyDataSet.Tables("Detections").Select("cmID = " & RecordID.ToString)
    For Each _DataRow In _DataRows
      _DataRow.Delete()
    Next
    MyDataSet.Tables("Detections").AcceptChanges()

    Call SelectDetections()
    Call FindMax()

  End Sub
  'finds record's index in the database table by its ID-field value
  Public Function FindRecordIndexByID_VR(ByVal ID_VR As Integer) As Integer
    Dim _i As Integer

    For _i = 0 To Me.NumberOfFilteredRecordsVR - 1
      If GetSQLValueInteger(MyVideoRecordingRows(_i)("cmID")) = ID_VR Then Return _i
    Next _i

    Return -1
  End Function

  Public Function FindRecordIndexByID_DT(ByVal ID_DT As Integer) As Integer
    Dim _i As Integer

    For _i = 0 To Me.NumberOfFilteredRecordsDT - 1
      If GetSQLValueInteger(MyDetectionRows(_i)("cmID")) = ID_DT Then Return _i
    Next _i

    Return -1
  End Function

#End Region



#Region "Internal subs"

  'Reads the tables to a dataset from the database
  Private Function ReadTables() As Boolean
    Dim _DataAdapter As OleDbDataAdapter
    Dim _DataRows() As DataRow
    Dim _DataRow As DataRow
    Dim _TableName As String


    MyDataSet = New DataSet
    Try
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordings", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordings")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM Detections", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "Detections")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingStatus ORDER BY cmPosition", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingStatus")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingType ORDER BY cmPosition", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingType")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionStatus ORDER BY cmPosition", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionStatus")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionType ORDER BY cmPosition", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionType")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingVideoFiles", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingVideoFiles")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionVideoFiles", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionVideoFiles")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM RoadUserTypes ORDER BY cmPosition", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "RoadUserTypes")
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingControls", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "VideoRecordingControls")
      _DataRows = MyDataSet.Tables("VideoRecordingControls").Select("cmType = 4")
      For Each _DataRow In _DataRows
        _TableName = "uVR_" & _DataRow("cmName").ToString & "Values"
        _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM " & _TableName & " ORDER BY cmPosition", MyOleDBConnection)
        _DataAdapter.Fill(MyDataSet, _TableName)
      Next
      _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionControls", MyOleDBConnection)
      _DataAdapter.Fill(MyDataSet, "DetectionControls")
      _DataRows = MyDataSet.Tables("DetectionControls").Select("cmType = 4")
      For Each _DataRow In _DataRows
        _TableName = "uDT_" & _DataRow("cmName").ToString & "Values"
        _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM " & _TableName & " ORDER BY cmPosition", MyOleDBConnection)
        _DataAdapter.Fill(MyDataSet, _TableName)
      Next
      _DataAdapter.Dispose()

      Call FindMax()
    Catch ex As Exception
      MyDataSet.Dispose()
      MsgBox("Cannot open the necessary tables in the database file '" & MyDatabaseFilePath & "'!" & Environment.NewLine & Environment.NewLine & _
               "error message: """ & ex.Message & """", MsgBoxStyle.Critical)
      Return False
    End Try

    Return True
  End Function


  'Finds the highest values of ID-filed in the tables
  Private Sub FindMax()
    If MyDataSet.Tables("VideoRecordings").Rows.Count > 0 Then
      MyMaxVideoRecordingID = CInt(MyDataSet.Tables("VideoRecordings").Compute("Max(cmID)", ""))
    Else
      MyMaxVideoRecordingID = 99999
    End If
    If MyDataSet.Tables("Detections").Rows.Count > 0 Then
      MyMaxDetectionID = CInt(MyDataSet.Tables("Detections").Compute("Max(cmID)", ""))
    Else
      MyMaxDetectionID = -1
    End If
    If MyDataSet.Tables("VideoRecordingVideoFiles").Rows.Count > 0 Then
      MyMaxVideoFileID_VR = CInt(MyDataSet.Tables("VideoRecordingVideoFiles").Compute("Max(cmID)", ""))
    Else
      MyMaxVideoFileID_VR = -1
    End If
    If MyDataSet.Tables("DetectionVideoFiles").Rows.Count > 0 Then
      MyMaxVideoFileID_DT = CInt(MyDataSet.Tables("DetectionVideoFiles").Compute("Max(cmID)", ""))
    Else
      MyMaxVideoFileID_DT = -1
    End If
  End Sub

  'Selects the records from the VideoRecordings table
  Private Sub SelectVideoRecordings()
    Try
      MyVideoRecordingRows = MyDataSet.Tables("VideoRecordings").Select(MyFilterVR, SORT_ORDER)
    Catch ' ex As Exception
      '  MsgBox(ex.ToString, MsgBoxStyle.Critical)
      MyVideoRecordingRows = MyDataSet.Tables("VideoRecordings").Select("cmID<0", SORT_ORDER)
    End Try

    MyRecordIndexVR = -1

  End Sub

  'Selects the records from the Detections table
  Private Sub SelectDetections()
    Try
      MyDetectionRows = MyDataSet.Tables("Detections").Select(MyFilterDT, SORT_ORDER)
    Catch
      MyDetectionRows = MyDataSet.Tables("Detections").Select("cmID<0", SORT_ORDER)
    End Try
    MyRecordIndexDT = -1
  End Sub





#End Region



  Private Sub CheckUpdates()
    Dim _OleDbCommand As OleDbCommand
    '  Dim _Row As DataRow

    Dim _DataAdapter As OleDbDataAdapter
    'Dim _DataRows() As DataRow
    'Dim _DataRow As DataRow
    'Dim _TableName As String
    Dim _DataSet As DataSet



    _DataSet = New DataSet


    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingControls", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "VideoRecordingControls")
    If Not _DataSet.Tables("VideoRecordingControls").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordingControls ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("VideoRecordingControls")

    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingStatus", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "VideoRecordingStatus")
    If Not _DataSet.Tables("VideoRecordingStatus").Columns.Contains("cmPosition") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordingStatus ADD COLUMN cmPosition INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE VideoRecordingStatus SET cmPosition = 0", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    If Not _DataSet.Tables("VideoRecordingStatus").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordingStatus ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("VideoRecordingStatus")

    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordingType", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "VideoRecordingType")
    If Not _DataSet.Tables("VideoRecordingType").Columns.Contains("cmPosition") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordingType ADD COLUMN cmPosition INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE VideoRecordingType SET cmPosition = 0", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    If Not _DataSet.Tables("VideoRecordingType").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordingType ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("VideoRecordingType")




    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionControls", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "DetectionControls")
    If Not _DataSet.Tables("DetectionControls").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE DetectionControls ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("DetectionControls")

    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionStatus", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "DetectionStatus")
    If Not _DataSet.Tables("DetectionStatus").Columns.Contains("cmPosition") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE DetectionStatus ADD COLUMN cmPosition INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE DetectionStatus SET cmPosition = 0", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    If Not _DataSet.Tables("DetectionStatus").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE DetectionStatus ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("DetectionStatus")

    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM DetectionType", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "DetectionType")
    If Not _DataSet.Tables("DetectionType").Columns.Contains("cmPosition") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE DetectionType ADD COLUMN cmPosition INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE DetectionType SET cmPosition = 0", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    If Not _DataSet.Tables("DetectionType").Columns.Contains("cmComment") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE DetectionType ADD COLUMN cmComment TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("DetectionType")








    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM RoadUserTypes", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "RoadUserTypes")
    If Not _DataSet.Tables("RoadUserTypes").Columns.Contains("cmWeight") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE RoadUserTypes ADD COLUMN cmWeight INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE RoadUserTypes SET cmWeight = 1000", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    If Not _DataSet.Tables("RoadUserTypes").Columns.Contains("cmPosition") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE RoadUserTypes ADD COLUMN cmPosition INTEGER", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
      _OleDbCommand = New OleDbCommand("UPDATE RoadUserTypes SET cmPosition = 0", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("RoadUserTypes")





    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM VideoRecordings", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "VideoRecordings")
    If Not _DataSet.Tables("VideoRecordings").Columns.Contains("cmMedia") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE VideoRecordings ADD COLUMN cmMedia TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("VideoRecordings")

    _DataAdapter = New OleDb.OleDbDataAdapter("SELECT * FROM Detections", MyOleDBConnection)
    _DataAdapter.Fill(_DataSet, "Detections")
    If Not _DataSet.Tables("Detections").Columns.Contains("cmMedia") Then
      _OleDbCommand = New OleDbCommand("ALTER TABLE Detections ADD COLUMN cmMedia TEXT(255)", MyOleDBConnection)
      _OleDbCommand.ExecuteNonQuery()
    End If
    _DataSet.Tables.Remove("Detections")










    _DataSet.Dispose()


  End Sub

  Public Sub AddFieldDetections(ByVal FieldName As String, ByVal Type As Type)
    Dim _OleDbCommand As OleDbCommand
    Dim _FieldName As String


    _FieldName = FieldName.Replace(CChar(" "), "")

    _OleDbCommand = New OleDbCommand
    With _OleDbCommand
      .Connection = MyOleDBConnection

      If Not MyDataSet.Tables("Detections").Columns.Contains(_FieldName) Then
        Select Case Type
          Case Type.GetType("System.Double")
            _OleDbCommand = New OleDbCommand("ALTER TABLE Detections ADD COLUMN " & _FieldName & " DOUBLE", MyOleDBConnection)
            MyDataSet.Tables("Detections").Columns.Add(New DataColumn(_FieldName, Type.GetType("System.Double")))
          Case Type.GetType("System.Int32")
            _OleDbCommand = New OleDbCommand("ALTER TABLE Detections ADD COLUMN " & _FieldName & " LONG", MyOleDBConnection)
            MyDataSet.Tables("Detections").Columns.Add(New DataColumn(_FieldName, Type.GetType("System.Int32")))
          Case Type.GetType("System.DateTime")
            _OleDbCommand = New OleDbCommand("ALTER TABLE Detections ADD COLUMN " & _FieldName & " DATE", MyOleDBConnection)
            MyDataSet.Tables("Detections").Columns.Add(New DataColumn(_FieldName, Type.GetType("System.DateTime")))
          Case Type.GetType("System.String")
            _OleDbCommand = New OleDbCommand("ALTER TABLE Detections ADD COLUMN " & _FieldName & " Text(255)", MyOleDBConnection)
            MyDataSet.Tables("Detections").Columns.Add(New DataColumn(_FieldName, Type.GetType("System.String")))
        End Select
        _OleDbCommand.ExecuteNonQuery()
      End If
      .Dispose()
      MyDataSet.Tables("Detections").AcceptChanges()
    End With




  End Sub

  Public Sub UpdateValueDetections(ByVal FieldName As String, ByVal DataValue As Object, ByVal DataType As Type)
    Dim _OleDbCommand As OleDbCommand


    _OleDbCommand = New OleDbCommand
    With _OleDbCommand
      .Connection = MyOleDBConnection

      .CommandText = "UPDATE Detections SET " &
                      FieldName & " = @Value " &
                      "WHERE cmID = @ID"

      Select Case DataType
        Case Type.GetType("System.DateTime")
          MyDetectionRows(MyRecordIndexDT)(FieldName) = CDate(DataValue)
          .Parameters.AddWithValue("@Value", CDate(DataValue).ToString(CultureInfo.InvariantCulture))
        Case Type.GetType("System.String")
          MyDetectionRows(MyRecordIndexDT)(FieldName) = CStr(DataValue)
          .Parameters.AddWithValue("@Value", CStr(DataValue))
        Case Type.GetType("System.Int32")
          MyDetectionRows(MyRecordIndexDT)(FieldName) = CInt(DataValue)
          .Parameters.AddWithValue("@Value", CInt(DataValue))
        Case Type.GetType("System.Double")
          MyDetectionRows(MyRecordIndexDT)(FieldName) = CDbl(DataValue)
          .Parameters.AddWithValue("@Value", CDbl(DataValue).ToString(CultureInfo.InvariantCulture))
      End Select

      .Parameters.AddWithValue("@ID", MyDetectionRows(MyRecordIndexDT)("cmID"))
      .ExecuteNonQuery()
      .Dispose()
      MyDataSet.Tables("Detections").AcceptChanges()
    End With
  End Sub



End Class
