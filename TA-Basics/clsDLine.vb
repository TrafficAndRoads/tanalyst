﻿Public Class clsDLine
  Inherits clsGeomLine

  Public DxDy As clsDxDy


  Public Shadows ReadOnly Property Point1 As clsDPoint
    Get
      Point1 = New clsDPoint(Me.X1, Me.Y1, Me.DxDy)
    End Get
  End Property

  Public Shadows ReadOnly Property Point2 As clsDPoint
    Get
      Point2 = New clsDPoint(Me.X2, Me.Y2, Me.DxDy)
    End Get
  End Property

  Public ReadOnly Property RayLine1 As clsGeomLine
    Get
      RayLine1 = Me.Point1.RayLine
    End Get
  End Property

  Public ReadOnly Property RayLine2 As clsGeomLine
    Get
      RayLine2 = Me.Point2.RayLine
    End Get
  End Property

  Public ReadOnly Property Dx As Double
    Get
      Return Me.DxDy.Dx
    End Get
  End Property

  Public ReadOnly Property Dy As Double
    Get
      Return Me.DxDy.Dy
    End Get
  End Property



  Public Sub New(ByVal X1 As Double, ByVal Y1 As Double,
                 ByVal X2 As Double, ByVal Y2 As Double,
                 ByVal DxDy As clsDxDy)

    MyBase.New(X1, Y1, X2, Y2)
    Me.DxDy = DxDy
  End Sub

  Public Sub New(ByVal Point1 As clsGeomPoint,
                 ByVal Point2 As clsGeomPoint,
                 ByVal DxDy As clsDxDy)

    MyBase.New(Point1, Point2)
    Me.DxDy = DxDy
  End Sub

  Public Shadows Function CreateCopy(Optional ByVal OffsetX As Double = 0,
                                     Optional ByVal OffsetY As Double = 0) As clsDLine

    Return New clsDLine(Me.X1 + OffsetX, Me.Y1 + OffsetY, Me.X2 + OffsetX, Me.Y2 + OffsetY, Me.DxDy)
  End Function

End Class
