﻿Public Module modCommon
  Public Const CompError = 0.001
  Public Const NoValue As Integer = Integer.MinValue
  Public Const NoValueDate As Date = #1/1/1900#
  Public Const Range = 500 'm
  Public Const JpgCOMPRESSION As Long = 85&


  Public Function IsEqual(ByVal a As Double, ByVal b As Double) As Boolean
    If Math.Abs(a - b) < CompError Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsLessOrEqual(ByVal a As Double, ByVal b As Double) As Boolean
    If a < (b + CompError) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsMoreOrEqual(ByVal a As Double, ByVal b As Double) As Boolean
    If a > (b - CompError) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsLess(ByVal a As Double, ByVal b As Double) As Boolean
    If a < (b - CompError) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsMore(ByVal a As Double, ByVal b As Double) As Boolean
    If a > (b + CompError) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsBetween(ByVal x As Integer, ByVal a As Integer, ByVal b As Integer) As Boolean
    If (x >= a) And (x <= b) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsBetween(ByVal x As Double, ByVal a As Double, ByVal b As Double) As Boolean
    If IsMoreOrEqual(x, a) And IsLessOrEqual(x, b) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsBetweenNoOrder(ByVal x As Double, ByVal a As Double, ByVal b As Double) As Boolean
    If a > b Then Swap(a, b)

    If IsMoreOrEqual(x, a) And IsLessOrEqual(x, b) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Function IsValue(ByVal a As Integer) As Boolean
    If a = NoValue Then Return False Else Return True
  End Function

  Public Function IsValue(ByVal a As Double) As Boolean
    If IsEqual(a, NoValue) Then Return False Else Return True
  End Function

  Public Function IsValue(ByVal a As Date) As Boolean
    If a = NoValueDate Then Return False Else Return True
  End Function

  Public Function IsNotValue(ByVal a As Integer) As Boolean
    If a = NoValue Then Return True Else Return False
  End Function

  Public Function IsNotValue(ByVal a As Double) As Boolean
    If IsEqual(a, NoValue) Then Return True Else Return False
  End Function

  Public Function IsNotValue(ByVal a As Date) As Boolean
    If a = NoValueDate Then Return True Else Return False
  End Function

  Public Function IsZero(ByVal a As Double) As Boolean
    If IsEqual(a, 0) Then Return True Else Return False
  End Function

  Public Sub Swap(ByVal a As Integer, ByVal b As Integer)
    Dim c As Integer

    c = a
    a = b
    b = c
  End Sub

  Public Sub Swap(ByRef a As Double, ByRef b As Double)
    Dim c As Double

    c = a
    a = b
    b = c
  End Sub

  ''' <summary>
  '''Calculates cubic root of x, can handle both positive and negative x values
  ''' </summary>
  Public Function Sqrt3(ByVal x As Double) As Double
    If x < 0 Then
      Return -(Math.Abs(x) ^ (1 / 3))
    Else
      Return x ^ (1 / 3)
    End If
  End Function

  Public Function SolveSystem2(ByVal a1 As Double, ByVal a2 As Double, ByVal b1 As Double, ByVal b2 As Double, ByVal c1 As Double, ByVal c2 As Double, ByRef x As Double, ByRef y As Double) As Boolean
    'solves the system of 2 equations in a form
    '  a1*x+b1*y=c1
    '  a2*x+b2*y=c2
    'using Kramer's method. If there's no solutions returns False value, otherwise - True

    Dim D As Double
    Dim D1 As Double, D2 As Double

    D = a1 * b2 - a2 * b1

    If IsZero(D) Then
      x = NoValue
      y = NoValue
      Return False
    Else
      D1 = c1 * b2 - c2 * b1
      D2 = a1 * c2 - a2 * c1

      x = D1 / D
      y = D2 / D
      Return True
    End If
  End Function

  Public Function LineEquationCoeeficientsFromTwoPoints(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByRef a As Double, b As Double, ByRef c As Double) As Boolean
    'calucaltes the coeeficients a, b, c in the line equation a*x+b*y+c=0 based on two pouints that the line passes through
    ' if the points are equal, returns false

    a = Y1 - Y2
    b = X2 - 21
    c = X1 * Y2 - X2 * Y1

    If IsEqual(X1, X2) And IsEqual(Y1, Y2) Then
      Return False
    Else
      Return True
    End If

  End Function


  Public Function PointDistanceToLineSegment(ByVal Xp As Double, ByVal Yp As Double, ByVal Xl1 As Double, ByVal Yl1 As Double, ByVal Xl2 As Double, ByVal Yl2 As Double, ByRef Distance As Double) As Boolean
    Dim _Xlp, _Ylp As Double

    'Check distance

    If IsEqual(Xl2, Xl1) And IsEqual(Yl2, Yl1) Then
      _Xlp = (Xl2 + Xl1) / 2
      _Ylp = (Yl2 + Yl1) / 2
      Distance = Math.Sqrt((Xp - _Xlp) ^ 2 + (Yp - _Ylp) ^ 2)
    Else

      Distance = Math.Abs((Xl2 - Xl1) * (Yl1 - Yp) - (Xl1 - Xp) * (Yl2 - Yl1)) / Math.Sqrt((Xl2 - Xl1) ^ 2 + (Yl2 - Yl1) ^ 2)

    End If


  End Function

  'Public Function PointDistanceToLine(ByVal Xp As Double, ByVal Yp As Double, ByVal a As Double, ByVal b As Double, ByVal Xl2 As Double, ByVal Yl2 As Double, ByRef Distance As Double) As Boolean
  '  Dim _Xlp, _Ylp As Double

  '  'Check distance

  '  If IsEqual(Xl2, Xl1) And IsEqual(Yl2, Yl1) Then
  '    _Xlp = (Xl2 + Xl1) / 2
  '    _Ylp = (Yl2 + Yl1) / 2
  '    Distance = Math.Sqrt((Xp - _Xlp) ^ 2 + (Yp - _Ylp) ^ 2)
  '  Else

  '    Distance = Math.Abs((Xl2 - Xl1) * (Yl1 - Yp) - (Xl1 - Xp) * (Yl2 - Yl1)) / Math.Sqrt((Xl2 - Xl1) ^ 2 + (Yl2 - Yl1) ^ 2)

  '  End If


  'End Function

  Public Function Interpolate(ByVal x As Double, ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double) As Double
    'interpolates between 2 points (x1,y1) and (x2,y2)

    If IsEqual(x1, x2) Then
      Return (y1 + y2) / 2
    Else
      Return y1 + (y2 - y1) / (x2 - x1) * (x - x1)
    End If
  End Function

  Public Function LinearRegressionCoefficients(ByVal Xpoints As List(Of Double), ByVal Ypoints As List(Of Double), ByRef k As Double, ByRef y0 As Double) As Boolean
    'Finds the best coefficients k, y0 by minimal square method for a linear regression line (y=k*x+y0)

    Dim _Sx As Double = 0
    Dim _Sx2 As Double = 0
    Dim _Sxy As Double = 0
    Dim _Sy As Double = 0
    Dim _n As Integer
    Dim _i As Integer

    _n = Math.Min(Xpoints.Count, Ypoints.Count)
    If _n > 1 Then
      For _i = 0 To _n - 1
        _Sx = _Sx + Xpoints(_i)
        _Sx2 = _Sx2 + Xpoints(_i) ^ 2
        _Sxy = _Sxy + Xpoints(_i) * Ypoints(_i)
        _Sy = _Sy + Ypoints(_i)
      Next _i

      Return SolveSystem2(_Sx2, _Sx, _Sx, _n, _Sxy, _Sy, k, y0)
    Else
      k = NoValue
      y0 = NoValue
      Return False
    End If
  End Function


  Public Function SmoothMovingAverage(ByVal Values As List(Of Double), ByVal NumberOfPoints As Integer) As List(Of Double)
    Dim _i, _j, _k As Integer
    Dim _Sum As Double
    Dim _Result As List(Of Double)

    _Result = New List(Of Double)

    For _i = 0 To Values.Count - 1
      _Sum = 0
      _j = Math.Min(Math.Min(_i, NumberOfPoints), Values.Count - _i - 1)


      For _k = _i - _j To _i + _j
        _Sum = _Sum + Values(_k)
      Next _k
      _Result.Add(_Sum / (2 * _j + 1))
    Next _i

    Return _Result
  End Function





  ' Public Function InterpolateLinear() As clsTPoint '(ByVal Time As Double, ByVal TPoints As List(Of clsTPoint)) As clsTPoint
  'Dim _TimeData As New List(Of Double)
  'Dim _Xdata As New List(Of Double)
  'Dim _Ydata As New List(Of Double)
  'Dim _kx As Double, _x0 As Double
  'Dim _ky As Double, _y0 As Double
  'Dim _X As Double, _Y As Double


  'Select Case TPoints.Count
  '  Case 1
  '    _X = TPoints(0).X
  '    _Y = TPoints(0).Y
  '    'Case 2
  '    ' _X = Interpolate(Time, XPoints(0).Time, XPoints(0).X, XPoints(1).Time, XPoints(1).X)
  '    ' _Y = Interpolate(Time, XPoints(0).Time, XPoints(0).Y, XPoints(1).Time, XPoints(1).Y)
  '  Case Else
  '    For Each _TPoint In TPoints
  '      _TimeData.Add(_TPoint.Time)
  '      _Xdata.Add(_TPoint.X)
  '      _Ydata.Add(_TPoint.Y)
  '    Next

  '    If LinearRegressionCoefficients(_TimeData, _Xdata, _kx, _x0) Then
  '      _X = _x0 + _kx * Time
  '    Else
  '      _X = TPoints(0).X
  '    End If
  '    If LinearRegressionCoefficients(_TimeData, _Ydata, _ky, _y0) Then
  '      _Y = _y0 + _ky * Time
  '    Else
  '      _Y = TPoints(0).X
  '    End If
  'End Select

  'Return New clsTPoint(_X, _Y, Time)
  '  Return Nothing
  'End Function



  ''' <summary>
  '''Converts time value in seconds to format "00:00:00".
  ''' </summary>
  Public Function SecondsToTimeDisplay(ByVal TimeInSeconds As Double) As String
    Dim _Hours As Integer
    Dim _Minutes As Integer
    Dim _Seconds As Integer
    ' Dim _Milliseconds As Integer


    _Hours = CInt(Math.Truncate(TimeInSeconds / 3600))
    TimeInSeconds = TimeInSeconds - _Hours * 3600
    _Minutes = CInt(Math.Truncate(TimeInSeconds / 60))
    TimeInSeconds = TimeInSeconds - _Minutes * 60
    _Seconds = CInt(Math.Truncate(TimeInSeconds))
    TimeInSeconds = (TimeInSeconds - _Seconds) * 1000
    '_Milliseconds = CInt(Math.Truncate(TimeInSeconds))

    Return _Hours.ToString & ":" & _Minutes.ToString("00") & ":" & _Seconds.ToString("00") ' & "." & _Milliseconds.ToString("000")
  End Function


  Public Function TryConvertFileNameToDate(ByVal VideoFileName As String, ByRef DateTime As Date) As Boolean
    Dim _Year As Integer
    Dim _Month As Integer
    Dim _Day As Integer
    Dim _Hour As Integer
    Dim _Minute As Integer
    Dim _Second As Integer
    Dim _Millisecond As Integer

    Try
      _Year = CInt(VideoFileName.Substring(0, 4))
      _Month = CInt(VideoFileName.Substring(4, 2))
      _Day = CInt(VideoFileName.Substring(6, 2))
      _Hour = CInt(VideoFileName.Substring(9, 2))
      _Minute = CInt(VideoFileName.Substring(11, 2))
      _Second = CInt(VideoFileName.Substring(13, 2))


      If VideoFileName.Length >= 19 Then
        Integer.TryParse(VideoFileName.Substring(16, 3), _Millisecond)
      End If

      DateTime = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second, _Millisecond)
      Return True

    Catch ex As Exception
      Return False
    End Try


  End Function

  Public Sub NotReadyYet(Optional FunctionName As String = "")
    MsgBox("Sorry, this function is not implemented yet :-)", MsgBoxStyle.Information, FunctionName)
  End Sub



  Public Function EmptyBitmap(ByVal BitmapSize As Size) As Bitmap
    Dim _Bitmap As Bitmap
    Dim _g As Graphics

    _Bitmap = New Bitmap(BitmapSize.Width, BitmapSize.Height)
    _g = Graphics.FromImage(_Bitmap)
    _g.FillRectangle(Brushes.DarkBlue, New Rectangle(0, 0, 640, 480))
    _g.DrawString("Could not retrieve the required image", New Font("Times New Roman", 10), Brushes.Yellow, 10, 10)
    _g.Dispose()

    Return _Bitmap
  End Function


  Public Function ScaleImageToFitGivenSize(ByVal InputImage As Bitmap, ByVal Width As Integer, ByVal Height As Integer) As Bitmap
    Dim _NewBitmap As Bitmap
    Dim _g As Graphics
    Dim _Scale As Double
    Dim _NewWidth, _NewHeight As Integer
    Dim _MarginX, _MarginY As Integer


    _NewBitmap = New Bitmap(Width, Height)
    _g = Graphics.FromImage(_NewBitmap)

    _Scale = Math.Min(Width / InputImage.Width, Height / InputImage.Height)

    _NewWidth = CInt(InputImage.Width * _Scale)
    _NewHeight = CInt(InputImage.Height * _Scale)

    _MarginX = CInt((Width - _NewWidth) / 2)
    _MarginY = CInt((Height - _Newheight) / 2)

    _g.DrawImage(InputImage, _MarginX, _MarginY, _NewWidth, _NewHeight)
    _g.Dispose()

    Return _NewBitmap
  End Function

  Public Sub SaveJpgImage(ByVal Bitmap As Bitmap, ByVal FilePath As String, Optional ByVal JpgCompression As Long = 85&)
    Dim _CodecInfo As ImageCodecInfo
    Dim _Encoder As Encoder
    Dim _EncoderParameters As EncoderParameters
    Dim _EncoderParameter As EncoderParameter

    'Set jpeg parameters
    _CodecInfo = GetCodecInfo(ImageFormat.Jpeg)
    _Encoder = Encoder.Quality
    _EncoderParameters = New EncoderParameters(1)
    _EncoderParameter = New EncoderParameter(_Encoder, JpgCompression)
    _EncoderParameters.Param(0) = _EncoderParameter

    Bitmap.Save(FilePath, _CodecInfo, _EncoderParameters)

  End Sub


  Public Sub SaveJpgImage(ByVal Bitmap As Bitmap, ByRef Stream As IO.Stream, Optional ByVal JpgCompression As Long = 85&)
    Dim _CodecInfo As ImageCodecInfo
    Dim _Encoder As Encoder
    Dim _EncoderParameters As EncoderParameters
    Dim _EncoderParameter As EncoderParameter

    'Set jpeg parameters
    _CodecInfo = GetCodecInfo(ImageFormat.Jpeg)
    _Encoder = Encoder.Quality
    _EncoderParameters = New EncoderParameters(1)
    _EncoderParameter = New EncoderParameter(_Encoder, JpgCompression)
    _EncoderParameters.Param(0) = _EncoderParameter

    Bitmap.Save(Stream, _CodecInfo, _EncoderParameters)

  End Sub
  Private Function GetCodecInfo(ByVal ImageFormat As ImageFormat) As ImageCodecInfo
    Dim _Codecs As ImageCodecInfo() = ImageCodecInfo.GetImageDecoders()
    Dim _Codec As ImageCodecInfo

    For Each _Codec In _Codecs
      If _Codec.FormatID = ImageFormat.Guid Then
        Return _Codec
      End If
    Next
    Return Nothing

  End Function





  Public Function GetSQLValueInteger(ByVal value As Object) As Integer
    If IsDBNull(value) Then Return NoValue Else Return CInt(value)
  End Function

  Public Function GetSQLValueDouble(ByVal value As Object) As Double
    If IsDBNull(value) Then Return NoValue Else Return CDbl(value)
  End Function

  Public Function GetSQLValueString(ByVal value As Object) As String
    If IsDBNull(value) Then Return "" Else Return CStr(value)
  End Function

  Public Function GetSQLValueDate(ByVal value As Object) As Date
    If IsDBNull(value) Then Return NoValueDate Else Return CDate(value)
  End Function

  Public Function GetSQLValueBoolean(ByVal value As Object) As Boolean
    If IsDBNull(value) Then Return False Else Return CBool(value)
  End Function



  Public Function SetSQLValueInteger(ByVal value As Integer) As Object
    If Not IsValue(value) Then Return DBNull.Value Else Return value
  End Function

  Public Function SetSQLValueDouble(ByVal value As Double) As Object
    If Not IsValue(value) Then Return DBNull.Value Else Return value
  End Function

  Public Function SetSQLValueDate(ByVal value As Date) As Object
    If Not IsValue(value) Then Return DBNull.Value Else Return value
  End Function

  Public Sub DeleteDirectory(ByVal DirectoryPath As String)

    If IO.Directory.Exists(DirectoryPath) Then
      For Each _FilePath In IO.Directory.GetFiles(DirectoryPath)
        IO.File.Delete(_FilePath)
      Next

      For Each _SubDirectory As String In IO.Directory.GetDirectories(DirectoryPath)
        DeleteDirectory(_SubDirectory)
      Next

      IO.Directory.Delete(DirectoryPath)
    End If
  End Sub


  Public Sub MakeAVIfromImages(ByVal AviFilePath As String,
                               ByVal ImageFolder As String,
                               ByVal FPS As Double,
                               ByVal KBS As Integer)
    Dim _Process As Process
    Dim _FrameRate As String
    Dim _BitRate As String


    _FrameRate = " -r " & FPS.ToString("0.000", CultureInfo.InvariantCulture).Replace(",", ".")
    _BitRate = " -b:v " & KBS.ToString & "k"

    _Process = New Process
    _Process.StartInfo.UseShellExecute = False
    _Process.StartInfo.CreateNoWindow = True
    _Process.StartInfo.FileName = "ffmpeg.exe"
    _Process.StartInfo.Arguments = "-y" & _FrameRate & " -i """ & ImageFolder & "%08d.jpg" & """ -codec:v libxvid -tag:v XVID" & _BitRate & " """ & AviFilePath & """"
    _Process.Start()
    _Process.WaitForExit()
  End Sub



  Public Function EnhanceImage(ByVal InputBitmap As Bitmap, ByVal Brightness As Single) As Bitmap
    Dim _g As Graphics
    Dim _EnhancedBitmap As Bitmap
    Dim _ColorMatrixVal As Single()()
    Dim _ColorMatrix As ColorMatrix
    Dim _IA As ImageAttributes


    _EnhancedBitmap = New Bitmap(InputBitmap.Width, InputBitmap.Height)
    _g = Graphics.FromImage(_EnhancedBitmap)

    _ColorMatrixVal = {New Single() {1, 0, 0, 0, 0},
                       New Single() {0, 1, 0, 0, 0},
                       New Single() {0, 0, 1, 0, 0},
                       New Single() {0, 0, 0, 1, 0},
                       New Single() {Brightness, Brightness, Brightness, 0, 1}}

    _ColorMatrix = New ColorMatrix(_ColorMatrixVal)
    _IA = New ImageAttributes

    _IA.SetColorMatrix(_ColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap)

    _g.DrawImage(InputBitmap, New Rectangle(0, 0, InputBitmap.Width, InputBitmap.Height), 0, 0, InputBitmap.Width, InputBitmap.Height, GraphicsUnit.Pixel, _ia)
    _g.Dispose()

    Return _EnhancedBitmap
  End Function


  Public Function InterpretTimestampLine(ByVal TimestampLine As String, Optional ByRef FrameID As Integer = NoValue) As Date
    Dim _CurrentTime As Date
    Dim _Year As Integer
    Dim _Month As Integer
    Dim _Day As Integer
    Dim _Hour As Integer
    Dim _Minute As Integer
    Dim _Second As Integer
    Dim _MilliSecond As Integer


    _Year = CInt(TimestampLine.Substring(6, 4))
    _Month = CInt(TimestampLine.Substring(11, 2))
    _Day = CInt(TimestampLine.Substring(14, 2))
    _Hour = CInt(TimestampLine.Substring(17, 2))
    _Minute = CInt(TimestampLine.Substring(20, 2))
    _Second = CInt(TimestampLine.Substring(23, 2))
    _MilliSecond = CInt(TimestampLine.Substring(26, 3))
    _CurrentTime = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second, _MilliSecond)

    FrameID = CInt(TimestampLine.Substring(0, 5))

    Return _CurrentTime
  End Function


End Module
