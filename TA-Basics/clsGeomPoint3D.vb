﻿Public Class clsGeomPoint3D
  Inherits clsGeomPoint

  Public Z As Double



  Public Sub New(ByVal X As Double, ByVal Y As Double, ByVal Z As Double)
    MyBase.New(X, Y)
    Me.Z = Z
  End Sub

  Public Shadows Function CreateClone(Optional ByVal OffsetX As Double = 0, Optional ByVal OffsetY As Double = 0, Optional ByVal OffsetZ As Double = 0) As clsGeomPoint3D
    Return New clsGeomPoint3D(X + OffsetX, Y + OffsetY, Z + OffsetZ)
  End Function

  Public Shadows Function DistanceTo(ByVal GeomPoint3D As clsGeomPoint3D) As Double
    Return Math.Sqrt((X - GeomPoint3D.X) ^ 2 + (Y - GeomPoint3D.Y) ^ 2 + (Z - GeomPoint3D.Z) ^ 2)
  End Function

End Class
