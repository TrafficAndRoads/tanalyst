﻿Public Module modIndicators


  Public Sub CrossingCoursePlus(ByVal RoadUser1 As clsRoadUserModel,
                                ByVal V1 As Double,
                                ByVal Trajectory1 As List(Of clsDPoint),
                                ByVal RoadUser2 As clsRoadUserModel,
                                ByVal V2 As Double,
                                ByVal Trajectory2 As List(Of clsDPoint),
                                ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double,
                                ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double,
                                ByRef SpeedVectorDxDy1 As clsDxDy, ByRef SpeedVectorDxDy2 As clsDxDy,
                                ByRef X1 As Double, ByRef Y1 As Double,
                                ByRef X2 As Double, ByRef Y2 As Double)

    Const TIME_ERROR = 0.05

    Dim _X1, _Y1 As Double
    Dim _DxDy1 As clsDxDy
    Dim _Position1 As clsDPoint

    Dim _X2, _Y2 As Double
    Dim _DxDy2 As clsDxDy
    Dim _Position2 As clsDPoint


    Dim _Tlim11, _Tlim12 As Double
    Dim _Tlim21, _Tlim22 As Double
    Dim _Travel1 As Double
    Dim _Travel2 As Double

    Dim _MinDistance As Double
    Dim _TTC_TA_found As Boolean

    Dim _TTCtemp As Double
    Dim _TA1temp, _TA2temp As Double

    Dim _X_TTCtemp, _Y_TTCtemp As Double
    Dim _X_TAtemp, _Y_TAtemp As Double

    'Dim _X1temp, _Y1temp As Double
    'Dim _X2temp, _Y2temp As Double


    TTC = NoValue
    X_TTC = NoValue : Y_TTC = NoValue
    TA1 = NoValue : TA2 = NoValue
    X_TA = NoValue : Y_TA = NoValue
    _TTC_TA_found = False

    _MinDistance = Math.Sqrt(RoadUser1.Width ^ 2 + RoadUser1.Length ^ 2) + Math.Sqrt(RoadUser2.Width ^ 2 + RoadUser2.Length ^ 2)

    _Travel1 = 0
    _Tlim12 = TIME_ERROR

    For _i1 = 1 To Trajectory1.Count - 1
      _Travel1 = _Travel1 + Trajectory1(_i1 - 1).DistanceTo(Trajectory1(_i1))
      _Tlim11 = _Tlim12 - 2 * TIME_ERROR
      _Tlim12 = _Travel1 / V1 + TIME_ERROR

      _DxDy1 = Trajectory1(_i1 - 1).DxDy
      _X1 = Trajectory1(_i1).X - _Travel1 * _DxDy1.Dx
      _Y1 = Trajectory1(_i1).Y - _Travel1 * _DxDy1.Dy
      _Position1 = New clsDPoint(_X1, _Y1, _DxDy1)

      _Travel2 = 0
      _Tlim22 = TIME_ERROR

      For _i2 = 1 To Trajectory2.Count - 1
        _Travel2 = _Travel2 + +Trajectory2(_i2 - 1).DistanceTo(Trajectory2(_i2))

        _Tlim21 = _Tlim22 - 2 * TIME_ERROR
        _Tlim22 = _Travel2 / V2 + TIME_ERROR

        _DxDy2 = Trajectory2(_i2 - 1).DxDy
        _X2 = Trajectory2(_i2).X - _Travel2 * _DxDy2.Dx
        _Y2 = Trajectory2(_i2).Y - _Travel2 * _DxDy2.Dy
        _Position2 = New clsDPoint(_X2, _Y2, _DxDy2)

        'MinDistance
        If IsLessOrEqual(Trajectory1(_i1).DistanceTo(Trajectory2(_i2)), _MinDistance) Then

          Call CrossingCourseStraight(RoadUser1, V1, _Position1, RoadUser2, V2, _Position2, _TTCtemp, _X_TTCtemp, _Y_TTCtemp, _TA1temp, _TA2temp, _X_TAtemp, _Y_TAtemp, _X1, _Y1, _X2, _Y2)

          If Not _TTC_TA_found Then
            If IsValue(_TTCtemp) Then
              If IsBetween(_TTCtemp, _Tlim11, _Tlim12) And IsBetween(_TTCtemp, _Tlim21, _Tlim22) Then
                _TTC_TA_found = True
                TTC = _TTCtemp
                X_TTC = _X_TTCtemp
                Y_TTC = _Y_TTCtemp
                SpeedVectorDxDy1 = _Position1.DxDy
                SpeedVectorDxDy2 = _Position2.DxDy
                X1 = _X1 : Y1 = _Y1
                X2 = _X2 : Y2 = _Y2

              End If
            ElseIf IsValue(_TA1temp) And IsValue(_TA2temp) Then
              If IsBetween(_TA1temp, _Tlim11, _Tlim12) And IsBetween(_TA2temp, _Tlim21, _Tlim22) Then
                _TTC_TA_found = True
                TA1 = _TA1temp
                TA2 = _TA2temp
                X_TA = _X_TAtemp
                Y_TA = _Y_TAtemp
                SpeedVectorDxDy1 = _Position1.DxDy
                SpeedVectorDxDy2 = _Position2.DxDy
                X1 = _X1 : Y1 = _Y1
                X2 = _X2 : Y2 = _Y2
              End If
            End If 'IsValue(TTC)
          End If '_TTC_TA_found
        End If
      Next _i2 'Loop
      If _TTC_TA_found Then Exit For
    Next _i1 'Loop

  End Sub


  Public Sub CrossingCourseStraight(ByVal RoadUser1 As clsRoadUserModel,
                                    ByVal V1 As Double,
                                    ByVal Position1 As clsDPoint,
                                    ByVal RoadUser2 As clsRoadUserModel,
                                    ByVal V2 As Double,
                                    ByVal Position2 As clsDPoint,
                                    ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double,
                                    ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double,
                                    ByRef X1 As Double, ByRef Y1 As Double,
                                    ByRef X2 As Double, ByRef Y2 As Double)

    Dim _LeftFront1, _RightFront1, _RightBack1, _LeftBack1 As clsXPoint
    Dim _LeftFront2, _RightFront2, _RightBack2, _LeftBack2 As clsXPoint
    Dim _Front1, _Right1, _Back1, _Left1 As clsXLine
    Dim _Front2, _Right2, _Back2, _Left2 As clsXLine
    Dim _Xpoint As clsXPoint, _XLine As clsXLine
    Dim _Footprint1, _Footprint2 As clsRoadUserModel.RoadUserFootprintStructure
    Dim _CrossingPoint As clsGeomPoint


    TTC = NoValue
    TA1 = NoValue
    TA2 = NoValue

    _Footprint1 = RoadUser1.RoadUserFootprint(Position1)
    _Footprint2 = RoadUser2.RoadUserFootprint(Position2)

    'define road users corners and sides 
    _LeftFront1 = New clsXPoint(_Footprint1.LeftFront, V1, 0, 0)
    _RightFront1 = New clsXPoint(_Footprint1.RightFront, V1, 0, 0)
    _RightBack1 = New clsXPoint(_Footprint1.RightBack, V1, 0, 0)
    _LeftBack1 = New clsXPoint(_Footprint1.LeftBack, V1, 0, 0)

    _LeftFront2 = New clsXPoint(_Footprint2.LeftFront, V2, 0, 0)
    _RightFront2 = New clsXPoint(_Footprint2.RightFront, V2, 0, 0)
    _RightBack2 = New clsXPoint(_Footprint2.RightBack, V2, 0, 0)
    _LeftBack2 = New clsXPoint(_Footprint2.LeftBack, V2, 0, 0)

    _Front1 = New clsXLine(_LeftFront1, _RightFront1, Position1.DxDy, V1, 0, 0)
    _Right1 = New clsXLine(_RightFront1, _RightBack1, Position1.DxDy, V1, 0, 0)
    _Back1 = New clsXLine(_RightBack1, _LeftBack1, Position1.DxDy, V1, 0, 0)
    _Left1 = New clsXLine(_LeftBack1, _LeftFront1, Position1.DxDy, V1, 0, 0)

    _Front2 = New clsXLine(_LeftFront2, _RightFront2, Position2.DxDy, V2, 0, 0)
    _Right2 = New clsXLine(_RightFront2, _RightBack2, Position2.DxDy, V2, 0, 0)
    _Back2 = New clsXLine(_RightBack2, _LeftBack2, Position2.DxDy, V2, 0, 0)
    _Left2 = New clsXLine(_LeftBack2, _LeftFront2, Position2.DxDy, V2, 0, 0)



    'Check if the sides already cross (=collision has occured)
    _CrossingPoint = Nothing
    If _Front1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Front2)
    ElseIf _Front1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Right2)
    ElseIf _Front1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Back2)
    ElseIf _Front1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Left2)

    ElseIf _Right1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Front2)
    ElseIf _Right1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Right2)
    ElseIf _Right1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Back2)
    ElseIf _Right1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Left2)

    ElseIf _Back1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Front2)
    ElseIf _Back1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Right2)
    ElseIf _Back1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Back2)
    ElseIf _Back1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Left2)

    ElseIf _Left1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Front2)
    ElseIf _Left1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Right2)
    ElseIf _Left1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Back2)
    ElseIf _Left1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Left2)
    End If





    If _CrossingPoint IsNot Nothing Then
      TTC = 0
      X_TTC = _CrossingPoint.X : Y_TTC = _CrossingPoint.Y
      X1 = Position1.X : Y1 = Position1.Y
      X2 = Position2.X : Y2 = Position2.Y

    Else
      _Xpoint = _LeftFront1
      _XLine = _Front2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Right2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Back2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Left2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)

      _Xpoint = _RightFront1
      _XLine = _Front2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Right2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Back2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Left2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)

      _Xpoint = _RightBack1
      _XLine = _Front2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Right2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Back2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Left2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)

      _Xpoint = _LeftBack1
      _XLine = _Front2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Right2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Back2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)
      _XLine = _Left2
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, Position1.X, Position1.Y, Position2.X, Position2.Y, X1, Y1, X2, Y2)


      _Xpoint = _LeftFront2
      _XLine = _Front1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Right1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Back1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Left1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)

      _Xpoint = _RightFront2
      _XLine = _Front1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Right1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Back1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Left1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)

      _Xpoint = _RightBack2
      _XLine = _Front1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Right1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Back1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Left1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)

      _Xpoint = _LeftBack2
      _XLine = _Front1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Right1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Back1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
      _XLine = _Left1
      Call TTC_TA(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, Position2.X, Position2.Y, Position1.X, Position1.Y, X2, Y2, X1, Y1)
    End If ' some of RU borders cross


  End Sub


  Public Sub TTC_TA(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine,
                    ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double,
                    ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double,
                    ByVal Xp As Double, ByVal Yp As Double,
                    ByVal Xl As Double, ByVal Yl As Double,
                    ByRef XpAfter As Double, ByRef YpAfter As Double,
                    ByRef XlAfter As Double, ByRef YlAfter As Double)

    Dim _TA As Double, _T2 As Double
    '  Dim _T1 As Double

    Dim _TTCtemp As Double
    Dim _TAtemp As Double
    Dim _Tp_temp As Double, _Tln_temp As Double
    Dim _Xtemp As Double, _Ytemp As Double



    'TTC
    If TTC_Point_To_Line(XPoint, XLine, _TTCtemp, _Xtemp, _Ytemp) Then
      If IsNotValue(TTC) Or IsLess(_TTCtemp, TTC) Then
        TTC = _TTCtemp
        X_TTC = _Xtemp
        Y_TTC = _Ytemp
        XpAfter = Xp + XPoint.V * XPoint.Dx * TTC
        YpAfter = Yp + XPoint.V * XPoint.Dy * TTC
        XlAfter = Xl + XLine.V * XLine.Dx * TTC
        YlAfter = Yl + XLine.V * XLine.Dy * TTC
      End If
    End If



    'TA
    If IsNotValue(TTC) Then
      If IsValue(TA1) And IsValue(TA2) Then
        _TA = Math.Abs(TA1 - TA2)
        _T2 = Math.Max(TA1, TA2)
      Else
        _TA = NoValue
        _T2 = NoValue
      End If

      If TA_Point_To_Line(XPoint, XLine, _Tp_temp, _Tln_temp, _Xtemp, _Ytemp) Then
        _TAtemp = Math.Abs(_Tp_temp - _Tln_temp)
        If IsNotValue(_TA) Or IsLess(_TAtemp, _TA) Then
          TA1 = _Tp_temp
          TA2 = _Tln_temp
          X_TA = _Xtemp : Y_TA = _Ytemp
          XpAfter = Xp + XPoint.V * XPoint.Dx * _Tp_temp
          YpAfter = Yp + XPoint.V * XPoint.Dy * _Tp_temp
          XlAfter = Xl + XLine.V * XLine.Dx * _Tln_temp
          YlAfter = Yl + XLine.V * XLine.Dy * _Tln_temp
        End If
      End If
    End If

  End Sub

  Public Function TTC_Point_To_Line(ByVal XPoint As clsXPoint,
                                    ByVal XLine As clsXLine,
                                    ByRef TTC As Double,
                                    Optional ByRef x As Double = NoValue,
                                    Optional ByRef y As Double = NoValue) As Boolean

    Dim _Vpx, _Vpy As Double
    Dim _Vlnx, _Vlny As Double
    Dim _k, _b, _c As Double
    Dim _t As Double
    Dim _XpNew, _YpNew As Double
    Dim _Xln1New, _Yln1New As Double
    Dim _Xln2New, _Yln2New As Double
    Dim _ReturnValue As Boolean


    _ReturnValue = False
    TTC = NoValue
    x = NoValue
    y = NoValue

    If IsZero(XPoint.V) And IsZero(XLine.V) Then Return _ReturnValue

    _Vpx = XPoint.V * XPoint.DxDy.Dx
    _Vpy = XPoint.V * XPoint.DxDy.Dy
    _Vlnx = XLine.V * XLine.DxDy.Dx
    _Vlny = XLine.V * XLine.DxDy.Dy


    'check if k is infinite
    If IsZero(XLine.X1 - XLine.X2) Then
      _b = _Vpx - _Vlnx
      _c = XPoint.X - XLine.X1
    Else
      _k = (XLine.Y1 - XLine.Y2) / (XLine.X1 - XLine.X2)
      _b = _k * (_Vpx - _Vlnx) - (_Vpy - _Vlny)
      _c = _k * (XPoint.X - XLine.X1) - (XPoint.Y - XLine.Y1)
    End If

    If Not IsZero(_b) Then
      _t = -_c / _b
      If IsMoreOrEqual(_t, 0) Then
        _XpNew = XPoint.X + _Vpx * _t
        _YpNew = XPoint.Y + _Vpy * _t
        _Xln1New = XLine.X1 + _Vlnx * _t
        _Yln1New = XLine.Y1 + _Vlny * _t
        _Xln2New = XLine.X2 + _Vlnx * _t
        _Yln2New = XLine.Y2 + _Vlny * _t

        If IsBetweenNoOrder(_XpNew, _Xln1New, _Xln2New) And IsBetweenNoOrder(_YpNew, _Yln1New, _Yln2New) Then
          _ReturnValue = True
          x = _XpNew
          y = _YpNew
          TTC = _t
        End If 'IsBetween
      End If 't>=0
    End If 'Not IsZero(b)

    Return _ReturnValue
  End Function

  Public Function TA_Point_To_Line(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine, ByRef Tp As Double, ByRef Tln As Double, Optional ByRef x As Double = NoValue, Optional ByRef y As Double = NoValue) As Boolean
    Dim _Vpx, _Vpy As Double
    Dim _Vlnx, _Vlny As Double
    Dim _FrozenPoint As clsXPoint
    Dim _FrozenLine As clsXLine

    Dim _Sp1 As Double, _Sln1 As Double
    Dim _Sp2 As Double, _Sln2 As Double

    Dim _Tp1 As Double, _Tln1 As Double
    Dim _Tp2 As Double, _Tln2 As Double
    Dim _Tp3 As Double, _Tln3 As Double 'Vp=0
    Dim _Tp4 As Double, _Tln4 As Double 'Vln=0

    Dim _TA As Double
    Dim _CrossPoint As clsGeomPoint
    Dim _dt1 As Double, _dt2 As Double, _dt3 As Double, _dt4 As Double
    Dim _ReturnValue As Boolean


    _ReturnValue = False
    Tp = NoValue
    Tln = NoValue
    x = NoValue
    y = NoValue
    _TA = NoValue


    'Check speeds
    If IsZero(XPoint.V) Or IsZero(XLine.V) Then Return _ReturnValue


    'Check first if they are on a collision course
    If TTC_Point_To_Line(XPoint, XLine, Tp, x, y) Then
      Tln = Tp
      _ReturnValue = True
      Return _ReturnValue
    End If


    'Check TA
    _Vpx = XPoint.V * XPoint.DxDy.Dx
    _Vpy = XPoint.V * XPoint.DxDy.Dy
    _Vlnx = XLine.V * XLine.DxDy.Dx
    _Vlny = XLine.V * XLine.DxDy.Dy


    _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine1)
    If _CrossPoint IsNot Nothing Then
      _Sp1 = XPoint.DistanceTo(_CrossPoint)
      _Sln1 = XLine.Point1.DistanceTo(_CrossPoint)
    Else
      _Sp1 = NoValue
      _Sln1 = NoValue
    End If

    _Tp1 = NoValue
    _Tln1 = NoValue
    _dt1 = NoValue
    If IsValue(_Sp1) And IsValue(_Sln1) Then
      _Tp1 = _Sp1 / XPoint.V
      _Tln1 = _Sln1 / XLine.V
      _dt1 = _Tp1 - _Tln1
    End If



    _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine2)
    If _CrossPoint IsNot Nothing Then
      _Sp2 = XPoint.DistanceTo(_CrossPoint)
      _Sln2 = XLine.Point2.DistanceTo(_CrossPoint)
    Else
      _Sp2 = NoValue
      _Sln2 = NoValue
    End If

    _Tp2 = NoValue
    _Tln2 = NoValue
    _dt2 = NoValue
    If IsValue(_Sp2) And IsValue(_Sln2) Then
      _Tp2 = _Sp2 / XPoint.V
      _Tln2 = _Sln2 / XLine.V
      _dt2 = _Tp2 - _Tln2
    End If


    'Vp=0 - freeze the point
    _FrozenPoint = XPoint.CreateCopy
    _FrozenPoint.V = 0
    If TTC_Point_To_Line(_FrozenPoint, XLine, _Tln3) Then
      _Tp3 = 0
      _dt3 = _Tp3 - _Tln3
    Else
      _Tp3 = NoValue : _Tln3 = NoValue : _dt3 = NoValue
    End If


    'Vln=0 - freeze the line
    _FrozenLine = XLine.CreateCopy
    _FrozenLine.V = 0

    If TTC_Point_To_Line(XPoint, _FrozenLine, _Tp4) Then
      _Tln4 = 0
      _dt4 = _Tp4 - _Tln4
    Else
      _Tp4 = NoValue : _Tln4 = NoValue : _dt4 = NoValue
    End If



    If IsValue(_dt1) Then
      If IsNotValue(_TA) Or IsLess(Math.Abs(_dt1), _TA) Then
        Tp = _Tp1
        Tln = _Tln1
        _TA = Math.Abs(_Tp1 - _Tln1)
        x = XPoint.X + _Vpx * _Tp1
        y = XPoint.Y + _Vpy * _Tp1
        _ReturnValue = True
      End If
    End If

    If IsValue(_dt2) Then
      If IsNotValue(_TA) Or IsLess(Math.Abs(_dt2), _TA) Then
        Tp = _Tp2
        Tln = _Tln2
        _TA = Math.Abs(_Tp2 - _Tln2)
        x = XPoint.X + _Vpx * _Tp2
        y = XPoint.Y + _Vpy * _Tp2
        _ReturnValue = True
      End If
    End If

    If IsValue(_dt3) Then
      If IsNotValue(_TA) Or IsLess(Math.Abs(_dt3), _TA) Then
        Tp = _Tp3
        Tln = _Tln3
        _TA = Math.Abs(_Tp3 - _Tln3)
        x = XPoint.X + _Vpx * _Tp3
        y = XPoint.Y + _Vpy * _Tp3
        _ReturnValue = True
      End If
    End If

    If IsValue(_dt4) Then
      If IsNotValue(_TA) Or IsLess(Math.Abs(_dt4), _TA) Then
        Tp = _Tp4
        Tln = _Tln4
        _TA = Math.Abs(_Tp4 - _Tln4)
        x = XPoint.X + _Vpx * _Tp4
        y = XPoint.Y + _Vpy * _Tp4
        _ReturnValue = True
      End If
    End If

    Return _ReturnValue
  End Function






  Public Sub SpeedDifference(ByVal V1 As Double,
                                  ByVal dxdy1 As clsDxDy,
                                  ByVal V2 As Double,
                                  ByVal dxdy2 As clsDxDy,
                                  ByRef V_difference As Double,
                                  Optional ByRef dxdy_difference As clsDxDy = Nothing)

    Dim _Vx, _Vy As Double


    dxdy_difference = New clsDxDy()

    _Vx = V1 * dxdy1.Dx - V2 * dxdy2.Dx
    _Vy = V1 * dxdy1.Dy - V2 * dxdy2.Dy

    V_difference = Math.Sqrt(_Vx ^ 2 + _Vy ^ 2)

    If Not IsZero(V_difference) Then
      dxdy_difference = New clsDxDy(_Vx / V_difference, _Vy / V_difference)
    End If

  End Sub

  Public Function SpeedAfterCollision(ByVal v1 As Double,
                                 ByVal m1 As Integer,
                                 ByVal dxdy1 As clsDxDy,
                                 ByVal v2 As Double,
                                 ByVal m2 As Integer,
                                 ByVal dxdy2 As clsDxDy,
                                 ByRef vAfter As Double,
                                 ByRef dxdyAfter As clsDxDy,
                                 ByRef DeltaV1 As Double,
                                 ByRef DeltaV2 As Double) As Boolean

    Dim _J1x, _J1y As Double
    Dim _J2x, _J2y As Double
    Dim _Jafterx, _Jaftery, _Jafter As Double


    vAfter = NoValue
    DeltaV1 = NoValue
    DeltaV2 = NoValue
    dxdyAfter = New clsDxDy

    If IsMore(m1 + m2, 0) Then
      _J1x = m1 * v1 * dxdy1.Dx
      _J1y = m1 * v1 * dxdy1.Dy
      _J2x = m2 * v2 * dxdy2.Dx
      _J2y = m2 * v2 * dxdy2.Dy
      _Jafterx = _J1x + _J2x
      _Jaftery = _J1y + _J2y

      _Jafter = Math.Sqrt(_Jafterx ^ 2 + _Jaftery ^ 2)

      vAfter = _Jafter / (m1 + m2)

      If Not IsZero(_Jafter) Then
        dxdyAfter = New clsDxDy(_Jafterx / _Jafter, _Jaftery / _Jafter)
      End If

      Call SpeedDifference(v1, dxdy1, vAfter, dxdyAfter, DeltaV1)
      Call SpeedDifference(v2, dxdy2, vAfter, dxdyAfter, DeltaV2)

      Return True
    End If

    Return False

  End Function

  'Public Function DeltaV(ByVal v1 As Double,
  '                       ByVal m1 As Integer,
  '                       ByVal dxdy1 As clsDxDy,
  '                       ByVal v2 As Double,
  '                       ByVal m2 As Integer,
  '                       ByVal dxdy2 As clsDxDy,
  '                       ByRef vAfter As Double,
  '                       ByRef dxdyAfter As clsDxDy,
  '                       ByRef DeltaV1 As Double,
  '                       ByRef DeltaV2 As Double) As Boolean



  '  vAfter = NoValue
  '  dxdyAfter = New clsDxDy
  '  DeltaV1 = NoValue
  '  DeltaV2 = NoValue


  '  If SpeedAfterCollision(v1, m1, dxdy1, v2, m2, dxdy2, vAfter, dxdyAfter) Then
  '    Call SpeedDifference(v1, dxdy1, vAfter, dxdyAfter, DeltaV1)
  '    Call SpeedDifference(v2, dxdy2, vAfter, dxdyAfter, DeltaV2)

  '    Return True
  '  End If

  '  Return False

  'End Function


  'Public Function PointDistanceToLineSegment+(ByVal XLine1 As clsXLine, ByVal XLine2 As clsXLine, ByRef Distance As Double) As Double




  'End Function

  Public Sub ClosestDistance(ByVal RoadUser1 As clsRoadUserModel,
                             ByVal Position1 As clsDPoint,
                             ByVal RoadUser2 As clsRoadUserModel,
                             ByVal Position2 As clsDPoint,
                             ByRef Distance As Double,
                             ByRef DistanceLine As clsGeomLine)

    Dim _LeftFront1, _RightFront1, _RightBack1, _LeftBack1 As clsGeomPoint
    Dim _LeftFront2, _RightFront2, _RightBack2, _LeftBack2 As clsGeomPoint
    Dim _Front1, _Right1, _Back1, _Left1 As clsGeomLine
    Dim _Front2, _Right2, _Back2, _Left2 As clsGeomLine
    Dim _Footprint1, _Footprint2 As clsRoadUserModel.RoadUserFootprintStructure
    Dim _CrossingPoint As clsGeomPoint
    Dim _ClosestPoint1, _ClosestPoint2 As clsGeomPoint



    _CrossingPoint = Nothing
    _ClosestPoint1 = Nothing
    _ClosestPoint2 = Nothing


    'define road users corners and sides 
    _Footprint1 = RoadUser1.RoadUserFootprint(Position1)
    _Footprint2 = RoadUser2.RoadUserFootprint(Position2)

    _LeftFront1 = New clsGeomPoint(_Footprint1.LeftFront.X, _Footprint1.LeftFront.Y)
    _RightFront1 = New clsGeomPoint(_Footprint1.RightFront.X, _Footprint1.RightFront.Y)
    _RightBack1 = New clsGeomPoint(_Footprint1.RightBack.X, _Footprint1.RightBack.Y)
    _LeftBack1 = New clsGeomPoint(_Footprint1.LeftBack.X, _Footprint1.LeftBack.Y)

    _LeftFront2 = New clsGeomPoint(_Footprint2.LeftFront.X, _Footprint2.LeftFront.Y)
    _RightFront2 = New clsGeomPoint(_Footprint2.RightFront.X, _Footprint2.RightFront.Y)
    _RightBack2 = New clsGeomPoint(_Footprint2.RightBack.X, _Footprint2.RightBack.Y)
    _LeftBack2 = New clsGeomPoint(_Footprint2.LeftBack.X, _Footprint2.LeftBack.Y)

    _Front1 = New clsGeomLine(_LeftFront1, _RightFront1)
    _Right1 = New clsGeomLine(_RightFront1, _RightBack1)
    _Back1 = New clsGeomLine(_RightBack1, _LeftBack1)
    _Left1 = New clsGeomLine(_LeftBack1, _LeftFront1)

    _Front2 = New clsGeomLine(_LeftFront2, _RightFront2)
    _Right2 = New clsGeomLine(_RightFront2, _RightBack2)
    _Back2 = New clsGeomLine(_RightBack2, _LeftBack2)
    _Left2 = New clsGeomLine(_LeftBack2, _LeftFront2)




    'Check if the sides already cross (=collision has occured)
    If _Front1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Front2)
    ElseIf _Front1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Right2)
    ElseIf _Front1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Back2)
    ElseIf _Front1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Front1.IntersectsAt(_Left2)

    ElseIf _Right1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Front2)
    ElseIf _Right1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Right2)
    ElseIf _Right1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Back2)
    ElseIf _Right1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Right1.IntersectsAt(_Left2)

    ElseIf _Back1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Front2)
    ElseIf _Back1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Right2)
    ElseIf _Back1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Back2)
    ElseIf _Back1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Back1.IntersectsAt(_Left2)

    ElseIf _Left1.IntersectsAt(_Front2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Front2)
    ElseIf _Left1.IntersectsAt(_Right2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Right2)
    ElseIf _Left1.IntersectsAt(_Back2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Back2)
    ElseIf _Left1.IntersectsAt(_Left2) IsNot Nothing Then
      _CrossingPoint = _Left1.IntersectsAt(_Left2)
    End If

    If _CrossingPoint IsNot Nothing Then
      Distance = 0
      DistanceLine = New clsGeomLine(_CrossingPoint, _CrossingPoint)
    Else





      'measure all distances and choose the shortest
      Distance = _Front1.DistanceToPoint(_LeftFront2, _ClosestPoint1)
      _ClosestPoint2 = _LeftFront2

      If _Front1.DistanceToPoint(_RightFront2) < Distance Then
        Distance = _Front1.DistanceToPoint(_RightFront2, _ClosestPoint1)
        _ClosestPoint2 = _RightFront2
      End If
      If _Front1.DistanceToPoint(_RightBack2) < Distance Then
        Distance = _Front1.DistanceToPoint(_RightBack2, _ClosestPoint1)
        _ClosestPoint2 = _RightBack2
      End If
      If _Front1.DistanceToPoint(_LeftBack2) < Distance Then
        Distance = _Front1.DistanceToPoint(_LeftBack2, _ClosestPoint1)
        _ClosestPoint2 = _LeftBack2
      End If

      If _Right1.DistanceToPoint(_LeftFront2) < Distance Then
        Distance = _Right1.DistanceToPoint(_LeftFront2, _ClosestPoint1)
        _ClosestPoint2 = _LeftFront2
      End If
      If _Right1.DistanceToPoint(_RightFront2) < Distance Then
        Distance = _Right1.DistanceToPoint(_RightFront2, _ClosestPoint1)
        _ClosestPoint2 = _RightFront2
      End If
      If _Right1.DistanceToPoint(_RightBack2) < Distance Then
        Distance = _Right1.DistanceToPoint(_RightBack2, _ClosestPoint1)
        _ClosestPoint2 = _RightBack2
      End If
      If _Right1.DistanceToPoint(_LeftBack2) < Distance Then
        Distance = _Right1.DistanceToPoint(_LeftBack2, _ClosestPoint1)
        _ClosestPoint2 = _LeftBack2
      End If

      If _Back1.DistanceToPoint(_LeftFront2) < Distance Then
        Distance = _Back1.DistanceToPoint(_LeftFront2, _ClosestPoint1)
        _ClosestPoint2 = _LeftFront2
      End If
      If _Back1.DistanceToPoint(_RightFront2) < Distance Then
        Distance = _Back1.DistanceToPoint(_RightFront2, _ClosestPoint1)
        _ClosestPoint2 = _RightFront2
      End If
      If _Back1.DistanceToPoint(_RightBack2) < Distance Then
        Distance = _Back1.DistanceToPoint(_RightBack2, _ClosestPoint1)
        _ClosestPoint2 = _RightBack2
      End If
      If _Back1.DistanceToPoint(_LeftBack2) < Distance Then
        Distance = _Back1.DistanceToPoint(_LeftBack2, _ClosestPoint1)
        _ClosestPoint2 = _LeftBack2
      End If

      If _Left1.DistanceToPoint(_LeftFront2) < Distance Then
        Distance = _Left1.DistanceToPoint(_LeftFront2, _ClosestPoint1)
        _ClosestPoint2 = _LeftFront2
      End If
      If _Left1.DistanceToPoint(_RightFront2) < Distance Then
        Distance = _Left1.DistanceToPoint(_RightFront2, _ClosestPoint1)
        _ClosestPoint2 = _RightFront2
      End If
      If _Left1.DistanceToPoint(_RightBack2) < Distance Then
        Distance = _Left1.DistanceToPoint(_RightBack2, _ClosestPoint1)
        _ClosestPoint2 = _RightBack2
      End If
      If _Left1.DistanceToPoint(_LeftBack2) < Distance Then
        Distance = _Left1.DistanceToPoint(_LeftBack2, _ClosestPoint1)
        _ClosestPoint2 = _LeftBack2
      End If

      If _Front2.DistanceToPoint(_LeftFront1) < Distance Then
        Distance = _Front2.DistanceToPoint(_LeftFront1, _ClosestPoint2)
        _ClosestPoint1 = _LeftFront1
      End If
      If _Front2.DistanceToPoint(_RightFront1) < Distance Then
        Distance = _Front2.DistanceToPoint(_RightFront1, _ClosestPoint2)
        _ClosestPoint1 = _RightFront1
      End If
      If _Front2.DistanceToPoint(_RightBack1) < Distance Then
        Distance = _Front2.DistanceToPoint(_RightBack1, _ClosestPoint2)
        _ClosestPoint1 = _RightBack1
      End If
      If _Front2.DistanceToPoint(_LeftBack1) < Distance Then
        Distance = _Front2.DistanceToPoint(_LeftBack1, _ClosestPoint2)
        _ClosestPoint1 = _LeftBack1
      End If

      If _Right2.DistanceToPoint(_LeftFront1) < Distance Then
        Distance = _Right2.DistanceToPoint(_LeftFront1, _ClosestPoint2)
        _ClosestPoint1 = _LeftFront1
      End If
      If _Right2.DistanceToPoint(_RightFront1) < Distance Then
        Distance = _Right2.DistanceToPoint(_RightFront1, _ClosestPoint2)
        _ClosestPoint1 = _RightFront2
      End If
      If _Right2.DistanceToPoint(_RightBack1) < Distance Then
        Distance = _Right2.DistanceToPoint(_RightBack1, _ClosestPoint2)
        _ClosestPoint1 = _RightBack1
      End If
      If _Right2.DistanceToPoint(_LeftBack1) < Distance Then
        Distance = _Right2.DistanceToPoint(_LeftBack1, _ClosestPoint2)
        _ClosestPoint1 = _LeftBack1
      End If

      If _Back2.DistanceToPoint(_LeftFront1) < Distance Then
        Distance = _Back2.DistanceToPoint(_LeftFront1, _ClosestPoint2)
        _ClosestPoint1 = _LeftFront1
      End If
      If _Back2.DistanceToPoint(_RightFront1) < Distance Then
        Distance = _Back2.DistanceToPoint(_RightFront1, _ClosestPoint2)
        _ClosestPoint1 = _RightFront1
      End If
      If _Back2.DistanceToPoint(_RightBack1) < Distance Then
        Distance = _Back2.DistanceToPoint(_RightBack1, _ClosestPoint2)
        _ClosestPoint1 = _RightBack1
      End If
      If _Back2.DistanceToPoint(_LeftBack1) < Distance Then
        Distance = _Back2.DistanceToPoint(_LeftBack1, _ClosestPoint2)
        _ClosestPoint1 = _LeftBack1
      End If

      If _Left2.DistanceToPoint(_LeftFront1) < Distance Then
        Distance = _Left2.DistanceToPoint(_LeftFront1, _ClosestPoint2)
        _ClosestPoint1 = _LeftFront1
      End If
      If _Left2.DistanceToPoint(_RightFront1) < Distance Then
        Distance = _Left2.DistanceToPoint(_RightFront1, _ClosestPoint2)
        _ClosestPoint1 = _RightFront1
      End If
      If _Left2.DistanceToPoint(_RightBack1) < Distance Then
        Distance = _Left2.DistanceToPoint(_RightBack1, _ClosestPoint2)
        _ClosestPoint1 = _RightBack1
      End If
      If _Left2.DistanceToPoint(_LeftBack1) < Distance Then
        Distance = _Left2.DistanceToPoint(_LeftBack1, _ClosestPoint2)
        _ClosestPoint1 = _LeftBack1
      End If


      DistanceLine = New clsGeomLine(_ClosestPoint1, _ClosestPoint2)

    End If


  End Sub


End Module
