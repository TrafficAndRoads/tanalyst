﻿Public Class clsTSAI

  Public Structure Callibration_parameters_TSAI
    Dim dx As Double
    Dim dy As Double
    Dim Cx As Double
    Dim Cy As Double
    Dim Sx As Double
    Dim f As Double 'effective focal length of the pin hole camera
    Dim k As Double '1st order radial lens distortion coefficient
    Dim Tx As Double
    Dim Ty As Double
    Dim Tz As Double
    Dim r1 As Double
    Dim r2 As Double
    Dim r3 As Double
    Dim r4 As Double
    Dim r5 As Double
    Dim r6 As Double
    Dim r7 As Double
    Dim r8 As Double
    Dim r9 As Double
  End Structure

  Public Structure InputData
    Dim PointCount As Integer
    Dim PointsWorld() As clsGeomPoint
    Dim PointsImage() As clsGeomPoint
  End Structure


  Public Function coplanar_calibration_with_full_optimization(ByVal CD As InputData) As Callibration_parameters_TSAI
    Dim _CP As Callibration_parameters_TSAI
    Dim _PointsCount As Integer

    If CD.PointsWorld.Count = CD.PointsImage.Count Then _PointsCount = CD.PointsWorld.Count Else _PointsCount = 0
    If _PointsCount > 0 Then
      'start with a 3 parameter (Tz, f, kappa1) optimization
      Call cc_three_parm_optimization(CD, _CP)

      'do a 5 parameter (Tz, f, kappa1, Cx, Cy) optimization
      ' Call cc_five_parm_optimization_with_late_distortion_removal()

      'do a better 5 parameter (Tz, f, kappa1, Cx, Cy) optimization
      ' Call cc_five_parm_optimization_with_early_distortion_removal()

      'do a full optimization minus the image center
      ' Call cc_nic_optimization()

      'do a full optimization including the image center
      '  Call cc_full_optimization()
      Return _CP
    Else
      Return Nothing
    End If
  End Function








  Private Sub cc_three_parm_optimization(ByVal CD As InputData, ByRef CP As Callibration_parameters_TSAI)
    '  Dim _i As Integer




    '  Call cc_compute_Xd_Yd_and_r_squared()
    '   Call cc_compute_U()
    '    Call cc_compute_Tx_and_Ty()
    '     Call cc_compute_R()
    '      Call cc_compute_approximate_f_and_Tz()
    If CP.f < 0 Then
      '/* try the other solution for the orthonormal matrix */
      CP.r3 = -CP.r3
      CP.r6 = -CP.r6
      CP.r7 = -CP.r7
      CP.r8 = -CP.r8
      'Call solve_RPY_transform()

      'cc_compute_approximate_f_and_Tz()

      'If (cc.f < 0) Then
      ' fprintf(stderr, "error - possible handedness problem with data\n")
      ' return; //exit (-1)
      'End if
    End If


    'Call cc_compute_exact_f_and_Tz()


  End Sub

  Private Sub cc_compute_Xd_Yd_and_r_squared(ByVal CD As InputData, ByRef CP As Callibration_parameters_TSAI)
    ' Dim i As Integer
    '   Dim _Xd, _Yd As Double

    '   For i = 0 To CD.point_count - 1

    '	Xd[i] = 

    '_Xd = cp.dpx * (cd.Xf[i] - cp.Cx) / cp.sx;	/* [mm] */
    '	Yd[i] = Yd_ = cp.dpy * (cd.Yf[i] - cp.Cy);	        /* [mm] */
    '	r_squared[i] = SQR (Xd_) + SQR (Yd_);                   /* [mm^2] */
    '  Next

  End Sub

End Class
