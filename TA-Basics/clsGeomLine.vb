﻿Public Class clsGeomLine
  Public X1 As Double
  Public Y1 As Double
  Public X2 As Double
  Public Y2 As Double



  Public Sub New(ByVal X1 As Double, ByVal Y1 As Double,
                 ByVal X2 As Double, ByVal Y2 As Double)
    Me.X1 = X1
    Me.Y1 = Y1
    Me.X2 = X2
    Me.Y2 = Y2
  End Sub

  Public Sub New(ByVal Point1 As clsGeomPoint,
                 ByVal Point2 As clsGeomPoint)
    Me.X1 = Point1.X
    Me.Y1 = Point1.Y
    Me.X2 = Point2.X
    Me.Y2 = Point2.Y
  End Sub




  Public ReadOnly Property Point1 As clsGeomPoint
    Get
      Point1 = New clsGeomPoint(X1, Y1)
    End Get
  End Property

  Public ReadOnly Property Point2 As clsGeomPoint
    Get
      Point2 = New clsGeomPoint(X2, Y2)
    End Get
  End Property

  Public ReadOnly Property Length() As Double
    Get
      Return Math.Sqrt((Me.X1 - Me.X2) ^ 2 + (Me.Y1 - Me.Y2) ^ 2)
    End Get
  End Property




  Public Function CreateClone(Optional ByVal OffsetX As Double = 0,
                             Optional ByVal OffsetY As Double = 0) As clsGeomLine

    Return New clsGeomLine(Me.X1 + OffsetX, Me.Y1 + OffsetY, Me.X2 + OffsetX, Me.Y2 + OffsetY)
  End Function

  Public Function IntersectsAt(ByVal Line2 As clsGeomLine) As clsGeomPoint
    Dim _a1 As Double, _b1 As Double, _c1 As Double
    Dim _a2 As Double, _b2 As Double, _c2 As Double
    Dim _x As Double, _y As Double
    Dim _LinesIntersect As Boolean


    Call FindABC(X1, Y1, X2, Y2, _a1, _b1, _c1)
    Call FindABC(Line2.X1, Line2.Y1, Line2.X2, Line2.Y2, _a2, _b2, _c2)

    If SolveSystem2(_a1, _a2, _b1, _b2, _c1, _c2, _x, _y) Then
      _LinesIntersect = True
    Else
      _LinesIntersect = False
    End If

    _LinesIntersect = _LinesIntersect And IsBetweenNoOrder(_x, X1, X2) And IsBetweenNoOrder(_y, Y1, Y2) And IsBetweenNoOrder(_x, Line2.X1, Line2.X2) And IsBetweenNoOrder(_y, Line2.Y1, Line2.Y2)

    If _LinesIntersect Then
      Return New clsGeomPoint(_x, _y)
    Else
      Return Nothing
    End If
  End Function

  Public Function DistanceToPoint(ByVal Point As clsGeomPoint, Optional ByRef ClosestLinePoint As clsGeomPoint = Nothing) As Double
    Dim _DistancePoint1 As Double
    Dim _DistancePoint2 As Double
    Dim _MinDistance As Double
    Dim _a, _b, _c As Double



    FindABC(X1, Y1, X2, Y2, _a, _b, _c)

    _DistancePoint1 = Me.Point1.DistanceTo(Point)
    _DistancePoint2 = Me.Point2.DistanceTo(Point)

    ClosestLinePoint = New clsGeomPoint((_b * (_b * Point.X - _a * Point.Y) + _a * _c) / (_a ^ 2 + _b ^ 2), (_a * (-_b * Point.X + _a * Point.Y) + _b * _c) / (_a ^ 2 + _b ^ 2))

    If IsBetweenNoOrder(ClosestLinePoint.X, X1, X2) And IsBetweenNoOrder(ClosestLinePoint.Y, Y1, Y2) Then
      _MinDistance = Math.Abs(_a * Point.X + _b * Point.Y - _c) / Math.Sqrt(_a ^ 2 + _b ^ 2)
    ElseIf _DistancePoint1 < _DistancePoint2 Then
      _MinDistance = _DistancePoint1
      ClosestLinePoint = Me.Point1.CreateClone
    Else
      _MinDistance = _DistancePoint2
      ClosestLinePoint = Me.Point2.CreateClone
    End If

    Return _MinDistance
  End Function





  Private Sub FindABC(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByRef a As Double, ByRef b As Double, ByRef c As Double)
    'finds coeficients for the line equation in form
    '  a*x+b*y=c
    'for a line passing through two points (x1, y1) and (x2, y2)

    If SolveSystem2(Y1, Y2, -1, -1, -X1, -X2, b, c) Then
      a = 1
    Else
      a = 0 : b = 1 : c = Y1
    End If
  End Sub

End Class
