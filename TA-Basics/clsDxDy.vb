﻿Public Class clsDxDy
  Private MyDx As Double
  Private MyDy As Double



  Public ReadOnly Property Dx As Double
    Get
      Dx = MyDx
    End Get
  End Property

  Public ReadOnly Property Dy As Double
    Get
      Dy = MyDy
    End Get
  End Property



  Public Sub New()
    MyDx = 1
    MyDy = 0
  End Sub

  Public Sub New(ByVal Dx As Double, ByVal Dy As Double)
    If CheckConsistency(Dx, Dy) Then
      MyDx = Dx
      MyDy = Dy
    Else
      MyDx = 1
      MyDy = 0
    End If
  End Sub

  Public Sub New(ByVal Angle As Double)
    MyDx = Math.Cos(Angle / 180 * Math.PI)
    MyDy = Math.Sin(Angle / 180 * Math.PI)
  End Sub

  Public Function Angle() As Double
    Return Math.Atan2(MyDy, MyDx) * 180 / Math.PI
  End Function

  Public Function CreateCopy(Optional ByVal OffsetAngle As Double = 0) As clsDxDy
    Return New clsDxDy(Me.Angle + OffsetAngle)
  End Function


  Private Function CheckConsistency(ByRef Dx As Double, ByRef Dy As Double) As Boolean
    If IsEqual(Dx ^ 2 + Dy ^ 2, 1) Then
      Return True
    Else
      Return False
    End If
  End Function

End Class
