﻿Public Class clsGateCrossPoint
  Inherits clsTPoint

  Public V As Double
  Public A As Double
  Public J As Double


  Public Sub New(ByVal X As Double,
                 ByVal Y As Double,
                 ByVal Time As Double,
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(X, Y, Time)
    Me.V = V
    Me.A = A
    Me.J = J
  End Sub

End Class
