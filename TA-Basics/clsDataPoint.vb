﻿Public Class clsDataPoint
  Inherits clsXPoint

  Public Xpxl As Double
  Public Ypxl As Double
  Public DxDypxl As clsDxDy



  Public Sub New(ByVal Xpxl As Double, ByVal Ypxl As Double,
                 ByVal DxDypxl As clsDxDy,
                 ByVal X As Double, ByVal Y As Double,
                 ByVal DxDy As clsDxDy,
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(X, Y, DxDy, V, A, J)

    Me.Xpxl = Xpxl
    Me.Ypxl = Ypxl
    Me.DxDypxl = DxDypxl
  End Sub

  Public Sub New(ByVal Xpxl As Double, ByVal Ypxl As Double)

    MyBase.New(Xpxl, Ypxl, New clsDxDy, NoValue, NoValue, NoValue)

    Me.Xpxl = Xpxl
    Me.Ypxl = Ypxl
    Me.DxDypxl = New clsDxDy
  End Sub




  Public Shadows Function CreateCopy() As clsDataPoint
    Return New clsDataPoint(Me.Xpxl, Me.Ypxl, Me.DxDypxl, Me.X, Me.Y, Me.DxDy, Me.V, Me.A, Me.J)
  End Function

  Public ReadOnly Property Dxpxl As Double
    Get
      Return Me.DxDypxl.Dx
    End Get
  End Property

  Public ReadOnly Property Dypxl As Double
    Get
      Return Me.DxDypxl.Dy
    End Get
  End Property


  Public Function OriginalXYExists() As Boolean
    If IsValue(Me.Xpxl) And IsValue(Me.Ypxl) Then Return True Else Return False
  End Function

  Public Function VExists() As Boolean
    If IsValue(Me.V) Then Return True Else Return False
  End Function

  Public Function AExists() As Boolean
    If IsValue(Me.A) Then Return True Else Return False
  End Function

  Public Function JExists() As Boolean
    If IsValue(Me.J) Then Return True Else Return False
  End Function

End Class
