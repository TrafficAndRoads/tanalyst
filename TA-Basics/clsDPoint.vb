﻿Public Class clsDPoint
  Inherits clsGeomPoint

  Protected RAY_LENGTH As Integer = 500

  Public DxDy As clsDxDy



  Public ReadOnly Property RayLine As clsGeomLine
    Get
      RayLine = New clsGeomLine(X, Y, X + RAY_LENGTH * Me.DxDy.Dx, Y + RAY_LENGTH * Me.DxDy.Dy)
    End Get
  End Property



  Public Sub New(ByVal X As Double, Y As Double, ByVal DxDy As clsDxDy)
    MyBase.New(X, Y)
    Me.DxDy = DxDy
  End Sub

  Public Sub New(ByVal GeomPoint As clsGeomPoint, ByVal DxDy As clsDxDy)
    MyBase.New(GeomPoint.X, GeomPoint.Y)
    Me.DxDy = DxDy
  End Sub




  Public Shadows Function CreateCopy(Optional ByVal OffsetX As Double = 0, Optional ByVal OffsetY As Double = 0) As clsDPoint
    Return New clsDPoint(X + OffsetX, Y + OffsetY, Me.DxDy)
  End Function



  Public Function RayIntersectsLineAt(ByVal Line As clsGeomLine) As clsGeomPoint
    Return Me.RayLine.IntersectsAt(Line)
  End Function

  Public Function RaysIntersectAt(ByVal DPoint2 As clsDPoint) As clsGeomPoint
    Return Me.RayLine.IntersectsAt(DPoint2.RayLine)
  End Function

End Class
