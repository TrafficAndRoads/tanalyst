﻿Public MustInherit Class clsRoadUserModel
  Protected MyType As clsRoadUserType
  Protected MyLength As Double
  Protected MyWidth As Double
  Protected MyHeight As Double
  Protected MyWeight As Integer

  Public Event ModelChanged()


  Public Structure RoadUser3DmodelStructure
    Public Centre As clsGeomPoint3D
    Public LeftP1 As clsGeomPoint3D
    Public RightP1 As clsGeomPoint3D
    Public LeftP2 As clsGeomPoint3D
    Public RightP2 As clsGeomPoint3D
    Public LeftP3 As clsGeomPoint3D
    Public RightP3 As clsGeomPoint3D
    Public LeftP4 As clsGeomPoint3D
    Public RightP4 As clsGeomPoint3D
    Public LeftP5 As clsGeomPoint3D
    Public RightP5 As clsGeomPoint3D
    Public LeftP6 As clsGeomPoint3D
    Public RightP6 As clsGeomPoint3D
    Public LeftP7 As clsGeomPoint3D
    Public RightP7 As clsGeomPoint3D
    Public LeftP8 As clsGeomPoint3D
    Public RightP8 As clsGeomPoint3D
  End Structure

  Public Structure RoadUserFootprintStructure
    Public LeftFront As clsDPoint
    Public RightFront As clsDPoint
    Public LeftBack As clsDPoint
    Public RightBack As clsDPoint
  End Structure




  Public Property Type As clsRoadUserType
    Get
      Type = MyType
    End Get
    Set(ByVal value As clsRoadUserType)
      MyType = value
      MyLength = MyType.Length
      MyWidth = MyType.Width
      MyHeight = MyType.Height
      MyWeight = MyType.Weight
      RaiseEvent ModelChanged()
    End Set
  End Property

  Public Property Length As Double
    Get
      Length = MyLength
    End Get
    Set(ByVal value As Double)
      MyLength = value
      RaiseEvent ModelChanged()
    End Set
  End Property

  Public Property Width As Double
    Get
      Width = MyWidth
    End Get
    Set(ByVal value As Double)
      MyWidth = value
      RaiseEvent ModelChanged()
    End Set
  End Property

  Public Property Height As Double
    Get
      Height = MyHeight
    End Get
    Set(ByVal value As Double)
      MyHeight = value
      RaiseEvent ModelChanged()
    End Set
  End Property

  Public Property Weight As Integer
    Get
      Weight = MyWeight
    End Get
    Set(ByVal value As Integer)
      MyWeight = value
      RaiseEvent ModelChanged()
    End Set
  End Property



  Public Sub New(ByVal Type As clsRoadUserType,
                 Optional ByVal Length As Double = NoValue,
                 Optional ByVal Width As Double = NoValue,
                 Optional ByVal Height As Double = NoValue,
                 Optional ByVal Weight As Integer = NoValue)

    MyType = Type
    If IsValue(Length) Then MyLength = Length Else MyLength = Type.Length
    If IsValue(Width) Then MyWidth = Width Else MyWidth = Type.Width
    If IsValue(Height) Then MyHeight = Height Else MyHeight = Type.Height
    If IsValue(Weight) Then MyWeight = Weight Else MyWeight = Type.Weight
  End Sub

  Public Function RoadUser3Dmodel(ByVal Position As clsDPoint) As RoadUser3DmodelStructure
    Dim _3Dmodel As RoadUser3DmodelStructure

    With Position
      _3Dmodel.Centre = New clsGeomPoint3D(.X, .Y, 0)

      _3Dmodel.LeftP1 = New clsGeomPoint3D(.X + 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, 0)
      _3Dmodel.RightP1 = New clsGeomPoint3D(.X + 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, 0)

      _3Dmodel.LeftP2 = New clsGeomPoint3D(.X + 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H2)
      _3Dmodel.RightP2 = New clsGeomPoint3D(.X + 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H2)

      _3Dmodel.LeftP3 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L3) * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L3) * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H3)
      _3Dmodel.RightP3 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L3) * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L3) * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H3)

      _3Dmodel.LeftP4 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L4) * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L4) * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight)
      _3Dmodel.RightP4 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L4) * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L4) * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight)

      _3Dmodel.LeftP5 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L5) * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L5) * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight)
      _3Dmodel.RightP5 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L5) * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L5) * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight)

      _3Dmodel.LeftP6 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L6) * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L6) * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H6)
      _3Dmodel.RightP6 = New clsGeomPoint3D(.X + Me.Length * (0.5 - MyType.L6) * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + Me.Length * (0.5 - MyType.L6) * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H6)

      _3Dmodel.LeftP7 = New clsGeomPoint3D(.X - 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H7)
      _3Dmodel.RightP7 = New clsGeomPoint3D(.X - 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, MyHeight * MyType.H7)

      _3Dmodel.LeftP8 = New clsGeomPoint3D(.X - 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, 0)
      _3Dmodel.RightP8 = New clsGeomPoint3D(.X - 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, 0)
    End With

    Return _3Dmodel
  End Function

  Public Function RoadUserFootprint(ByVal Position As clsDPoint) As RoadUserFootprintStructure
    Dim _Projected As RoadUserFootprintStructure

    With Position
      _Projected.LeftFront = New clsDPoint(.X + 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, .DxDy)
      _Projected.RightFront = New clsDPoint(.X + 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y + 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, .DxDy)
      _Projected.RightBack = New clsDPoint(.X - 0.5 * Me.Length * .DxDy.Dx + 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy - 0.5 * Me.Width * .DxDy.Dx, .DxDy)
      _Projected.LeftBack = New clsDPoint(.X - 0.5 * Me.Length * .DxDy.Dx - 0.5 * Me.Width * .DxDy.Dy, .Y - 0.5 * Me.Length * .DxDy.Dy + 0.5 * Me.Width * .DxDy.Dx, .DxDy)
    End With

    Return _Projected
  End Function


  'Public Sub CentreFromLeftFront(ByVal X_LeftFront As Double, ByVal Y_LeftFront As Double, ByRef X_Centre As Double, Y_Centre As Double)

  'End Sub
  'Public Sub CentreFromRightFront(ByVal X_LeftFront As Double, ByVal Y_LeftFront As Double, ByRef X_Centre As Double, Y_Centre As Double)

  'End Sub
  'Public Sub CentreFromRightBack(ByVal X_LeftFront As Double, ByVal Y_LeftFront As Double, ByRef X_Centre As Double, Y_Centre As Double)

  'End Sub
  'Public Sub CentreFromLeftBack(ByVal X_LeftFront As Double, ByVal Y_LeftFront As Double, ByRef X_Centre As Double, Y_Centre As Double)

  'End Sub
End Class
