﻿Public Class clsXPoint
  Inherits clsDPoint

  Public V As Double
  Public A As Double
  Public J As Double
 


  Public Sub New(ByVal X As Double,
                 ByVal Y As Double,
                 ByVal DxDy As clsDxDy,
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(X, Y, DxDy)
    Me.V = V
    Me.A = A
    Me.J = J
  End Sub

  Public Sub New(ByVal DPoint As clsDPoint,
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(DPoint.X, DPoint.Y, DPoint.DxDy)
    Me.V = V
    Me.A = A
    Me.J = J
  End Sub






  Public ReadOnly Property Dx As Double
    Get
      Return Me.DxDy.Dx
    End Get
  End Property

  Public ReadOnly Property Dy As Double
    Get
      Return Me.DxDy.Dy
    End Get
  End Property

  Public Shadows Function CreateCopy(Optional ByVal OffsetX As Double = 0,
                                     Optional ByVal OffsetY As Double = 0)As clsXPoint

    Return New clsXPoint(Me.X + OffsetX, Me.Y + OffsetY, Me.DxDy, Me.V, Me.A, Me.J)
  End Function

 End Class
