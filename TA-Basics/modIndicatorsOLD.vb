﻿Public Module modIndicatorsOLD

  'Public Sub CrossingCourseStraight(ByVal RoadUser1 As clsRoadUserModel, ByVal Position1 As clsXPoint, ByVal RoadUser2 As clsRoadUserModel, ByVal Position2 As clsXPoint, _
  '                                   ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double, _
  '                                   ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double,
  '                                   ByRef TG1 As Double, ByRef TG2 As Double, ByRef X_TG As Double, ByRef Y_TG As Double)

  '  Dim _Corner11, _Corner12, _Corner13, _Corner14 As clsXPoint
  '  Dim _Corner21, _Corner22, _Corner23, _Corner24 As clsXPoint
  '  Dim _Side11, _Side12, _Side13, _Side14 As clsXLine
  '  Dim _Side21, _Side22, _Side23, _Side24 As clsXLine
  '  Dim _Xpoint As clsXPoint, _XLine As clsXLine
  '  Dim _Footprint1, _Footprint2 As clsRoadUserModel.RoadUserFootprintStructure


  '  TTC = NoValue
  '  TA1 = NoValue
  '  TA2 = NoValue
  '  TG1 = NoValue
  '  TG2 = NoValue

  '  _Footprint1 = RoadUser1.RoadUserFootprint(Position1)
  '  _Footprint2 = RoadUser2.RoadUserFootprint(Position2)

  '  'define road users corners and sides 
  '  _Corner11 = New clsXPoint(_Footprint1.LeftFront, Position1.V, Position1.A, Position1.J)
  '  _Corner12 = New clsXPoint(_Footprint1.RightFront, Position1.V, Position1.A, Position1.J)
  '  _Corner13 = New clsXPoint(_Footprint1.RightBack, Position1.V, Position1.A, Position1.J)
  '  _Corner14 = New clsXPoint(_Footprint1.LeftBack, Position1.V, Position1.A, Position1.J)

  '  _Corner21 = New clsXPoint(_Footprint2.LeftFront, Position2.V, Position2.A, Position2.J)
  '  _Corner22 = New clsXPoint(_Footprint2.RightFront, Position2.V, Position2.A, Position2.J)
  '  _Corner23 = New clsXPoint(_Footprint2.RightBack, Position2.V, Position2.A, Position2.J)
  '  _Corner24 = New clsXPoint(_Footprint2.LeftBack, Position2.V, Position2.A, Position2.J)

  '  _Side11 = New clsXLine(_Corner11, _Corner12, Position1.DxDy, Position1.V, Position1.A, Position1.J)
  '  _Side12 = New clsXLine(_Corner12, _Corner13, Position1.DxDy, Position1.V, Position1.A, Position1.J)
  '  _Side13 = New clsXLine(_Corner13, _Corner14, Position1.DxDy, Position1.V, Position1.A, Position1.J)
  '  _Side14 = New clsXLine(_Corner14, _Corner11, Position1.DxDy, Position1.V, Position1.A, Position1.J)

  '  _Side21 = New clsXLine(_Corner21, _Corner22, Position2.DxDy, Position2.V, Position2.A, Position2.J)
  '  _Side22 = New clsXLine(_Corner22, _Corner23, Position2.DxDy, Position2.V, Position2.A, Position2.J)
  '  _Side23 = New clsXLine(_Corner23, _Corner24, Position2.DxDy, Position2.V, Position2.A, Position2.J)
  '  _Side24 = New clsXLine(_Corner24, _Corner21, Position2.DxDy, Position2.V, Position2.A, Position2.J)


  '  If _Side11.IntersectsAt(_Side21) IsNot Nothing Or _
  '       _Side11.IntersectsAt(_Side22) IsNot Nothing Or _
  '         _Side11.IntersectsAt(_Side23) IsNot Nothing Or _
  '           _Side11.IntersectsAt(_Side24) IsNot Nothing Or _
  '     _Side12.IntersectsAt(_Side21) IsNot Nothing Or _
  '       _Side12.IntersectsAt(_Side22) IsNot Nothing Or _
  '         _Side12.IntersectsAt(_Side23) IsNot Nothing Or _
  '           _Side12.IntersectsAt(_Side24) IsNot Nothing Or _
  '     _Side13.IntersectsAt(_Side21) IsNot Nothing Or _
  '       _Side13.IntersectsAt(_Side22) IsNot Nothing Or _
  '         _Side13.IntersectsAt(_Side23) IsNot Nothing Or _
  '           _Side13.IntersectsAt(_Side24) IsNot Nothing Or _
  '     _Side14.IntersectsAt(_Side21) IsNot Nothing Or _
  '       _Side14.IntersectsAt(_Side22) IsNot Nothing Or _
  '         _Side14.IntersectsAt(_Side23) IsNot Nothing Or _
  '           _Side14.IntersectsAt(_Side24) IsNot Nothing _
  '  Then
  '    TTC = 0
  '  Else
  '    _Xpoint = _Corner11
  '    _XLine = _Side21
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side22
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side23
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side24
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)

  '    _Xpoint = _Corner12
  '    _XLine = _Side21
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side22
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side23
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side24
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)

  '    _Xpoint = _Corner13
  '    _XLine = _Side21
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side22
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side23
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side24
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)

  '    _Xpoint = _Corner14
  '    _XLine = _Side21
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side22
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side23
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)
  '    _XLine = _Side24
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA1, TA2, X_TA, Y_TA, TG1, TG2, X_TG, Y_TG)


  '    _Xpoint = _Corner21
  '    _XLine = _Side11
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side12
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side13
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side14
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)

  '    _Xpoint = _Corner22
  '    _XLine = _Side11
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side12
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side13
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side14
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)

  '    _Xpoint = _Corner23
  '    _XLine = _Side11
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side12
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side13
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side14
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)

  '    _Xpoint = _Corner24
  '    _XLine = _Side11
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side12
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side13
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '    _XLine = _Side14
  '    Call TTC_TA_TG(_Xpoint, _XLine, TTC, X_TTC, Y_TTC, TA2, TA1, X_TA, Y_TA, TG2, TG1, X_TG, Y_TG)
  '  End If ' some of RU borders cross


  'End Sub

  'Public Sub TTC_TA_TG(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine, _
  '                      ByRef TTC As Double, ByRef X_TTC As Double, ByRef Y_TTC As Double, _
  '                      ByRef TA1 As Double, ByRef TA2 As Double, ByRef X_TA As Double, ByRef Y_TA As Double, _
  '                      ByRef TG1 As Double, ByRef TG2 As Double, ByRef X_TG As Double, ByRef Y_TG As Double)

  '  Dim _TA As Double, _T2 As Double
  '  Dim _TG As Double, _T1 As Double

  '  Dim _TTCtemp As Double
  '  Dim _TAtemp As Double
  '  Dim _TGtemp As Double, _T1temp As Double
  '  Dim _Tp_temp As Double, _Tln_temp As Double
  '  Dim _Xtemp As Double, _Ytemp As Double




  '  'TTC
  '  If TTC_Point_To_Line(XPoint, XLine, _TTCtemp, _Xtemp, _Ytemp) Then
  '    If IsNotValue(TTC) Or IsLess(_TTCtemp, TTC) Then
  '      TTC = _TTCtemp
  '      X_TTC = _Xtemp
  '      Y_TTC = _Ytemp
  '    End If
  '  End If



  '  'TA
  '  If IsNotValue(TTC) Then
  '    If IsValue(TA1) And IsValue(TA2) Then
  '      _TA = Math.Abs(TA1 - TA2)
  '      _T2 = Math.Max(TA1, TA2)
  '    Else
  '      _TA = NoValue
  '      _T2 = NoValue
  '    End If

  '    If TA_Point_To_Line(XPoint, XLine, _Tp_temp, _Tln_temp, _Xtemp, _Ytemp) Then
  '      _TAtemp = Math.Abs(_Tp_temp - _Tln_temp)
  '      If IsNotValue(_TA) Or IsLess(_TAtemp, _TA) Then
  '        TA1 = _Tp_temp
  '        TA2 = _Tln_temp
  '        X_TA = _Xtemp : Y_TA = _Ytemp
  '      End If
  '    End If
  '  End If



  '  'TG
  '  If IsValue(TG1) And IsValue(TG2) Then
  '    _TG = Math.Max(TG1, TG2)
  '    _T1 = Math.Min(TG1, TG2)
  '  Else
  '    _TG = NoValue
  '    _T1 = NoValue
  '  End If

  '  If TG_Point_To_Line(XPoint, XLine, _Tp_temp, _Tln_temp, _Xtemp, _Ytemp) Then
  '    _TGtemp = Math.Max(_Tp_temp, _Tln_temp)
  '    _T1temp = Math.Min(_Tp_temp, _Tln_temp)
  '    If IsNotValue(_TG) Or _
  '       IsLess(_TGtemp, _TG) Or _
  '       (IsEqual(_TGtemp, _TG) And IsLess(_T1temp, _T1)) _
  '    Then
  '      TG1 = _Tp_temp
  '      TG2 = _Tln_temp
  '      X_TG = _Xtemp : Y_TG = _Ytemp
  '    End If
  '  End If

  'End Sub

  'Public Function TTC_Point_To_Line(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine, ByRef TTC As Double, Optional ByRef x As Double = NoValue, Optional ByRef y As Double = NoValue) As Boolean
  '  Dim _Vpx, _Vpy As Double
  '  Dim _Vlnx, _Vlny As Double
  '  Dim _k, _b, _c As Double
  '  Dim _t As Double
  '  Dim _XpNew, _YpNew As Double
  '  Dim _Xln1New, _Yln1New As Double
  '  Dim _Xln2New, _Yln2New As Double


  '  TTC_Point_To_Line = False
  '  TTC = NoValue
  '  x = NoValue
  '  y = NoValue

  '  If IsZero(XPoint.V) And IsZero(XLine.V) Then Exit Function

  '  _Vpx = XPoint.V * XPoint.DxDy.Dx
  '  _Vpy = XPoint.V * XPoint.DxDy.Dy
  '  _Vlnx = XLine.V * XLine.DxDy.Dx
  '  _Vlny = XLine.V * XLine.DxDy.Dy


  '  'check if k is infinite
  '  If IsZero(XLine.X1 - XLine.X2) Then
  '    _b = _Vpx - _Vlnx
  '    _c = XPoint.X - XLine.X1
  '  Else
  '    _k = (XLine.Y1 - XLine.Y2) / (XLine.X1 - XLine.X2)
  '    _b = _k * (_Vpx - _Vlnx) - (_Vpy - _Vlny)
  '    _c = _k * (XPoint.X - XLine.X1) - (XPoint.Y - XLine.Y1)
  '  End If

  '  If Not IsZero(_b) Then
  '    _t = -_c / _b
  '    If IsMoreOrEqual(_t, 0) Then
  '      _XpNew = XPoint.X + _Vpx * _t
  '      _YpNew = XPoint.Y + _Vpy * _t
  '      _Xln1New = XLine.X1 + _Vlnx * _t
  '      _Yln1New = XLine.Y1 + _Vlny * _t
  '      _Xln2New = XLine.X2 + _Vlnx * _t
  '      _Yln2New = XLine.Y2 + _Vlny * _t

  '      If IsBetweenNoOrder(_XpNew, _Xln1New, _Xln2New) And IsBetweenNoOrder(_YpNew, _Yln1New, _Yln2New) Then
  '        TTC_Point_To_Line = True
  '        x = _XpNew
  '        y = _YpNew
  '        TTC = _t
  '      End If 'IsBetween
  '    End If 't>=0
  '  End If 'Not IsZero(b)

  'End Function

  'Public Function TA_Point_To_Line(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine, ByRef Tp As Double, ByRef Tln As Double, Optional ByRef x As Double = NoValue, Optional ByRef y As Double = NoValue) As Boolean
  '  Dim _Vpx, _Vpy As Double
  '  Dim _Vlnx, _Vlny As Double
  '  Dim _FrozenPoint As clsXPoint
  '  Dim _FrozenLine As clsXLine

  '  Dim _Sp1 As Double, _Sln1 As Double
  '  Dim _Sp2 As Double, _Sln2 As Double

  '  Dim _Tp1 As Double, _Tln1 As Double
  '  Dim _Tp2 As Double, _Tln2 As Double
  '  Dim _Tp3 As Double, _Tln3 As Double 'Vp=0
  '  Dim _Tp4 As Double, _Tln4 As Double 'Vln=0

  '  Dim _TA As Double
  '  Dim _CrossPoint As clsGeomPoint
  '  Dim _dt1 As Double, _dt2 As Double, _dt3 As Double, _dt4 As Double



  '  'Check first if they are on a collision course
  '  If TTC_Point_To_Line(XPoint, XLine, Tp, x, y) Then
  '    Tln = Tp
  '    TA_Point_To_Line = True
  '    Exit Function
  '  End If




  '  TA_Point_To_Line = False
  '  Tp = NoValue
  '  Tln = NoValue
  '  x = NoValue
  '  y = NoValue
  '  _TA = NoValue



  '  If IsZero(XPoint.V) Or IsZero(XLine.V) Then Exit Function


  '  _Vpx = XPoint.V * XPoint.DxDy.Dx
  '  _Vpy = XPoint.V * XPoint.DxDy.Dy
  '  _Vlnx = XLine.V * XLine.DxDy.Dx
  '  _Vlny = XLine.V * XLine.DxDy.Dy





  '  _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine1)
  '  If _CrossPoint IsNot Nothing Then
  '    _Sp1 = XPoint.DistanceTo(_CrossPoint)
  '    _Sln1 = XLine.Point1.DistanceTo(_CrossPoint)
  '  Else
  '    _Sp1 = NoValue
  '    _Sln1 = NoValue
  '  End If

  '  _Tp1 = NoValue
  '  _Tln1 = NoValue
  '  _dt1 = NoValue
  '  If IsValue(_Sp1) And IsValue(_Sln1) Then
  '    _Tp1 = _Sp1 / XPoint.V
  '    _Tln1 = _Sln1 / XLine.V
  '    _dt1 = _Tp1 - _Tln1
  '  End If



  '  _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine2)
  '  If _CrossPoint IsNot Nothing Then
  '    _Sp2 = XPoint.DistanceTo(_CrossPoint)
  '    _Sln2 = XLine.Point2.DistanceTo(_CrossPoint)
  '  Else
  '    _Sp2 = NoValue
  '    _Sln2 = NoValue
  '  End If

  '  _Tp2 = NoValue
  '  _Tln2 = NoValue
  '  _dt2 = NoValue
  '  If IsValue(_Sp2) And IsValue(_Sln2) Then
  '    _Tp2 = _Sp2 / XPoint.V
  '    _Tln2 = _Sln2 / XLine.V
  '    _dt2 = _Tp2 - _Tln2
  '  End If


  '  'Vp=0 - freeze the point
  '  _FrozenPoint = XPoint.CreateCopy
  '  _FrozenPoint.V = 0
  '  If TTC_Point_To_Line(_FrozenPoint, XLine, _Tln3) Then
  '    _Tp3 = 0
  '    _dt3 = _Tp3 - _Tln3
  '  Else
  '    _Tp3 = NoValue : _Tln3 = NoValue : _dt3 = NoValue
  '  End If


  '  'Vln=0 - freeze the line
  '  _FrozenLine = XLine.CreateCopy
  '  _FrozenLine.V = 0

  '  If TTC_Point_To_Line(XPoint, _FrozenLine, _Tp4) Then
  '    _Tln4 = 0
  '    _dt4 = _Tp4 - _Tln4
  '  Else
  '    _Tp4 = NoValue : _Tln4 = NoValue : _dt4 = NoValue
  '  End If



  '  If IsValue(_dt1) Then
  '    If IsNotValue(_TA) Or IsLess(Math.Abs(_dt1), _TA) Then
  '      Tp = _Tp1
  '      Tln = _Tln1
  '      _TA = Math.Abs(_Tp1 - _Tln1)
  '      x = XPoint.X + _Vpx * _Tp1
  '      y = XPoint.Y + _Vpy * _Tp1
  '      TA_Point_To_Line = True
  '    End If
  '  End If

  '  If IsValue(_dt2) Then
  '    If IsNotValue(_TA) Or IsLess(Math.Abs(_dt2), _TA) Then
  '      Tp = _Tp2
  '      Tln = _Tln2
  '      _TA = Math.Abs(_Tp2 - _Tln2)
  '      x = XPoint.X + _Vpx * _Tp2
  '      y = XPoint.Y + _Vpy * _Tp2
  '      TA_Point_To_Line = True
  '    End If
  '  End If

  '  If IsValue(_dt3) Then
  '    If IsNotValue(_TA) Or IsLess(Math.Abs(_dt3), _TA) Then
  '      Tp = _Tp3
  '      Tln = _Tln3
  '      _TA = Math.Abs(_Tp3 - _Tln3)
  '      x = XPoint.X + _Vpx * _Tp3
  '      y = XPoint.Y + _Vpy * _Tp3
  '      TA_Point_To_Line = True
  '    End If

  '  End If

  '  If IsValue(_dt4) Then
  '    If IsNotValue(_TA) Or IsLess(Math.Abs(_dt4), _TA) Then
  '      Tp = _Tp4
  '      Tln = _Tln4
  '      _TA = Math.Abs(_Tp4 - _Tln4)
  '      x = XPoint.X + _Vpx * _Tp4
  '      y = XPoint.Y + _Vpy * _Tp4
  '      TA_Point_To_Line = True
  '    End If
  '  End If

  'End Function

  'Private Function TG_Point_To_Line(ByVal XPoint As clsXPoint, ByVal XLine As clsXLine, ByRef Tp As Double, ByRef Tln As Double, Optional ByRef x As Double = NoValue, Optional ByRef y As Double = NoValue) As Boolean
  '  Dim _Vpx As Double, _Vpy As Double
  '  Dim _Vlnx As Double, _Vlny As Double

  '  Dim _Sp1 As Double, _Sln1 As Double
  '  Dim _Sp2 As Double, _Sln2 As Double

  '  Dim _Tp1 As Double, _Tln1 As Double
  '  Dim _Tp2 As Double, _Tln2 As Double
  '  Dim _Tp3 As Double, _Tln3 As Double 'Vp=0
  '  Dim _Tp4 As Double, _Tln4 As Double 'Vln=0
  '  Dim _Tp5 As Double, _Tln5 As Double 'TTC?

  '  Dim _TG As Double, _T1_TG As Double
  '  Dim _CrossPoint As clsGeomPoint
  '  Dim _FrozenPoint As clsXPoint
  '  Dim _FrozenLine As clsXLine

  '  Dim _TGtemp As Double, _T1_TGtemp As Double



  '  TG_Point_To_Line = False
  '  _TG = NoValue
  '  _T1_TG = NoValue
  '  Tp = NoValue
  '  Tln = NoValue
  '  x = NoValue
  '  y = NoValue

  '  If IsZero(XPoint.V) Or IsZero(XLine.V) Then Exit Function

  '  _Vpx = XPoint.V * XPoint.DxDy.Dx
  '  _Vpy = XPoint.V * XPoint.DxDy.Dy
  '  _Vlnx = XLine.V * XLine.DxDy.Dx
  '  _Vlny = XLine.V * XLine.DxDy.Dy



  '  _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine1)
  '  If _CrossPoint IsNot Nothing Then
  '    _Sp1 = XPoint.DistanceTo(_CrossPoint)
  '    _Sln1 = XLine.Point1.DistanceTo(_CrossPoint)
  '  Else
  '    _Sp1 = NoValue
  '    _Sln1 = NoValue
  '  End If

  '  _Tp1 = NoValue
  '  _Tln1 = NoValue
  '  If IsValue(_Sp1) And IsValue(_Sln1) Then
  '    _Tp1 = _Sp1 / XPoint.V
  '    _Tln1 = _Sln1 / XLine.V
  '  End If





  '  _CrossPoint = XPoint.RayLine.IntersectsAt(XLine.RayLine2)
  '  If _CrossPoint IsNot Nothing Then
  '    _Sp2 = XPoint.DistanceTo(_CrossPoint)
  '    _Sln2 = XLine.Point2.DistanceTo(_CrossPoint)
  '  Else
  '    _Sp2 = NoValue
  '    _Sln2 = NoValue
  '  End If

  '  _Tp2 = NoValue
  '  _Tln2 = NoValue
  '  If IsValue(_Sp2) And IsValue(_Sln2) Then
  '    _Tp2 = _Sp2 / XPoint.V
  '    _Tln2 = _Sln2 / XLine.V
  '  End If


  '  'Vp=0 - freeze the point
  '  _FrozenPoint = XPoint.CreateCopy
  '  _FrozenPoint.V = 0
  '  If TTC_Point_To_Line(_FrozenPoint, XLine, _Tln3) Then
  '    _Tp3 = 0
  '  Else
  '    _Tp3 = NoValue : _Tln3 = NoValue
  '  End If


  '  'Vln=0 - freeze the line
  '  _FrozenLine = XLine.CreateCopy
  '  _FrozenLine.V = 0
  '  If TTC_Point_To_Line(XPoint, _FrozenLine, _Tp4) Then
  '    _Tln4 = 0
  '  Else
  '    _Tp4 = NoValue : _Tln4 = NoValue
  '  End If

  '  'TTC?
  '  If TTC_Point_To_Line(XPoint, XLine, _Tp5) Then
  '    _Tln5 = _Tp5 '!!!! NOT ALWAYS TRUE. But it does not affect the TG value, only the T1_TG
  '  Else
  '    _Tp5 = NoValue : _Tln5 = NoValue
  '  End If





  '  If IsValue(_Tp5) And IsValue(_Tln5) Then
  '    _T1_TG = _Tp5
  '    _TG = _Tp5
  '    Tp = _Tp5
  '    Tln = _Tln5
  '    x = XPoint.X + _Vpx * _Tp5
  '    y = XPoint.Y + _Vpy * _Tp5
  '    TG_Point_To_Line = True
  '  Else
  '    If IsValue(_Tp1) And IsValue(_Tln1) Then
  '      _T1_TGtemp = Math.Min(_Tp1, _Tln1)
  '      _TGtemp = Math.Max(_Tp1, _Tln1)
  '      If (Not IsValue(_TG)) Or _
  '          IsLess(_TGtemp, _TG) Or _
  '         (IsEqual(_TGtemp, _TG) And IsLess(_T1_TGtemp, _T1_TG)) _
  '      Then
  '        _T1_TG = _T1_TGtemp
  '        _TG = _TGtemp
  '        Tp = _Tp1
  '        Tln = _Tln1
  '        x = XPoint.X + _Vpx * _Tp1
  '        y = XPoint.Y + _Vpy * _Tp1
  '        TG_Point_To_Line = True
  '      End If
  '    End If
  '    If IsValue(_Tp2) And IsValue(_Tln2) Then
  '      _T1_TGtemp = Math.Min(_Tp2, _Tln2)
  '      _TGtemp = Math.Max(_Tp2, _Tln2)
  '      If (Not IsValue(_TG)) Or _
  '          IsLess(_TGtemp, _TG) Or _
  '         (IsEqual(_TGtemp, _TG) And IsLess(_T1_TGtemp, _T1_TG)) _
  '      Then
  '        _T1_TG = _T1_TGtemp
  '        _TG = _TGtemp
  '        Tp = _Tp2
  '        Tln = _Tln2
  '        x = XPoint.X + _Vpx * _Tp2
  '        y = XPoint.Y + _Vpy * _Tp2
  '        TG_Point_To_Line = True
  '      End If
  '    End If
  '  End If 'TTC exist

  '  If IsValue(_Tp3) And IsValue(_Tln3) Then
  '    _T1_TGtemp = Math.Min(_Tp3, _Tln3)
  '    _TGtemp = Math.Max(_Tp3, _Tln3)
  '    If (Not IsValue(_TG)) Or _
  '        IsLess(_TGtemp, _TG) Or _
  '       (IsEqual(_TGtemp, _TG) And IsLess(_T1_TGtemp, _T1_TG)) _
  '    Then
  '      _T1_TG = _T1_TGtemp
  '      _TG = _TGtemp
  '      Tp = _Tp3
  '      Tln = _Tln3
  '      x = XPoint.X + _Vpx * _Tp3
  '      y = XPoint.Y + _Vpy * _Tp3
  '      TG_Point_To_Line = True
  '    End If
  '  End If

  '  If IsValue(_Tp4) And IsValue(_Tln4) Then
  '    _T1_TGtemp = Math.Min(_Tp4, _Tln4)
  '    _TGtemp = Math.Max(_Tp4, _Tln4)
  '    If (Not IsValue(_TG)) Or _
  '        IsLess(_TGtemp, _TG) Or _
  '       (IsEqual(_TGtemp, _TG) And IsLess(_T1_TGtemp, _T1_TG)) _
  '    Then
  '      _T1_TG = _T1_TGtemp
  '      _TG = _TGtemp
  '      Tp = _Tp4
  '      Tln = _Tln4
  '      x = XPoint.X + _Vpx * _Tp4
  '      y = XPoint.Y + _Vpy * _Tp4
  '      TG_Point_To_Line = True
  '    End If
  '  End If

  'End Function

  'Public Function SpeedDifference(ByVal V1 As Double,
  '                                ByVal dxdy1 As clsDxDy,
  '                                ByVal v2 As Double,
  '                                ByVal dxdy2 As clsDxDy) As Double

  '  Dim _Vx, _Vy As Double

  '  _Vx = V1 * dxdy1.Dx - v2 * dxdy2.Dx
  '  _Vy = V1 * dxdy1.Dy - v2 * dxdy2.Dy

  '  Return Math.Sqrt(_Vx ^ 2 + _Vy ^ 2)
  'End Function

  'Public Sub SpeedAfterCollision(ByVal v1 As Double,
  '                               ByVal m1 As Integer,
  '                               ByVal dxdy1 As clsDxDy,
  '                               ByVal v2 As Double,
  '                               ByVal m2 As Integer,
  '                               ByVal dxdy2 As clsDxDy,
  '                               ByRef vAfter As Double,
  '                               ByRef dxdyAfter As clsDxDy)
  '  Dim _J1x, _J1y As Double
  '  Dim _J2x, _J2y As Double
  '  Dim _Jafterx, _Jaftery, _Jafter As Double


  '  If IsMore(m1 + m2, 0) Then
  '    _J1x = m1 * v1 * dxdy1.Dx
  '    _J1y = m1 * v1 * dxdy1.Dy
  '    _J2x = m2 * v2 * dxdy2.Dx
  '    _J2y = m2 * v2 * dxdy2.Dy
  '    _Jafterx = _J1x + _J2x
  '    _Jaftery = _J1y + _J2y

  '    _Jafter = Math.Sqrt(_Jafterx ^ 2 + _Jaftery ^ 2)
  '    dxdyAfter = New clsDxDy(_Jafterx / _Jafter, _Jaftery / _Jafter)
  '    vAfter = _Jafter / (m1 + m2)
  '  Else
  '    vAfter = NoValue
  '    dxdyAfter = New clsDxDy
  '  End If

  'End Sub

  'Public Sub SpeedAfterCollisionElastic(ByVal v1 As Double,
  '                                      ByVal m1 As Integer,
  '                                      ByVal dxdy1 As clsDxDy,
  '                                      ByVal v2 As Double,
  '                                      ByVal m2 As Integer,
  '                                      ByVal dxdy2 As clsDxDy,
  '                                      ByRef vAfter As Double,
  '                                      ByRef dxdyAfter As clsDxDy)
  '  Dim _J1x, _J1y As Double
  '  Dim _J2x, _J2y As Double
  '  Dim _Jafter1x, _Jafter1y, _Jafter1 As Double
  '  Dim _Jafter2x, _Jafter2y, _Jafter2 As Double


  '  'If IsMore(m1 + m2, 0) Then
  '  '  _J1x = m1 * v1 * dxdy1.Dx
  '  '  _J1y = m1 * v1 * dxdy1.Dy
  '  '  _J2x = m2 * v2 * dxdy2.Dx
  '  '  _J2y = m2 * v2 * dxdy2.Dy
  '  '  _Jafterx = _J1x + _J2x
  '  '  _Jaftery = _J1y + _J2y

  '  '  _Jafter = Math.Sqrt(_Jafterx ^ 2 + _Jaftery ^ 2)
  '  '  dxdyAfter = New clsDxDy(_Jafterx / _Jafter, _Jaftery / _Jafter)
  '  '  vAfter = _Jafter / (m1 + m2)
  '  'Else
  '  '  vAfter = NoValue
  '  '  dxdyAfter = New clsDxDy
  '  '   End If

  'End Sub

End Module
