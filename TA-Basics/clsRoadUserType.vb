﻿Public Class clsRoadUserType
  Public Name As String
  Public Length As Double
  Public Width As Double
  Public Height As Double
  Public Weight As Integer
  Public L2 As Double
  Public H2 As Double
  Public L3 As Double
  Public H3 As Double
  Public L4 As Double
  Public H4 As Double
  Public L5 As Double
  Public H5 As Double
  Public L6 As Double
  Public H6 As Double
  Public L7 As Double
  Public H7 As Double



  Public Sub New(ByVal Name As String,
                 ByVal Length As Double, ByVal Width As Double, ByVal Height As Double,
                 ByVal Weight As Integer,
                 ByVal L2 As Double, ByVal H2 As Double,
                 ByVal L3 As Double, ByVal H3 As Double,
                 ByVal L4 As Double, ByVal H4 As Double,
                 ByVal L5 As Double, ByVal H5 As Double,
                 ByVal L6 As Double, ByVal H6 As Double,
                 ByVal L7 As Double, ByVal H7 As Double)
    Me.Name = Name
    Me.Length = Length
    Me.Width = Width
    Me.Height = Height
    Me.Weight = Weight
    Me.L2 = L2
    Me.H2 = H2
    Me.L3 = L3
    Me.H3 = H3
    Me.L4 = L4
    Me.H4 = H4
    Me.L5 = L5
    Me.H5 = H5
    Me.L6 = L6
    Me.H6 = H6
    Me.L7 = L7
    Me.H7 = H7
  End Sub

End Class
