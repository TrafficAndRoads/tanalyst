﻿Public Class clsXLine
  Inherits clsDLine

  Public V As Double
  Public A As Double
  Public J As Double
 


  Public Sub New(ByVal X1 As Double, ByVal Y1 As Double,
                 ByVal X2 As Double, ByVal Y2 As Double,
                 ByVal DxDy As clsDxDy,
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(X1, Y1, X2, Y2, DxDy)
    Me.V = V
    Me.A = A
    Me.J = J
  End Sub

  Public Sub New(ByVal Point1 As clsGeomPoint,
                 ByVal Point2 As clsGeomPoint,
                 ByVal DxDy As clsDxDy, _
                 ByVal V As Double,
                 ByVal A As Double,
                 ByVal J As Double)

    MyBase.New(Point1, Point2, DxDy)
    Me.V = V
    Me.A = A
    Me.J = J
  End Sub

  Public Overloads Function CreateCopy(Optional ByVal OffsetX As Double = 0,
                                       Optional ByVal OffsetY As Double = 0) As clsXLine

    Return New clsXLine(Me.X1 + OffsetX, Me.Y1 + OffsetY, Me.X2 + OffsetX, Me.Y2 + OffsetY, Me.DxDy, V, A, J)
  End Function

End Class
