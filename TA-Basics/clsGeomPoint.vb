﻿Public Class clsGeomPoint
  Public X As Double
  Public Y As Double



  Public Sub New(ByVal X As Double, ByVal Y As Double)
    Me.X = X
    Me.Y = Y
  End Sub

  Public Function CreateClone(Optional ByVal OffsetX As Double = 0, Optional ByVal OffsetY As Double = 0) As clsGeomPoint
    Return New clsGeomPoint(Me.X + OffsetX, Me.Y + OffsetY)
  End Function

  Public Function DistanceTo(ByVal GeomPoint As clsGeomPoint) As Double
    Return Math.Sqrt((Me.X - GeomPoint.X) ^ 2 + (Me.Y - GeomPoint.Y) ^ 2)
  End Function

End Class
