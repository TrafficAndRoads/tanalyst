﻿Public Class clsTPoint
  Inherits clsGeomPoint

  Public Time As Double



  Public Sub New(ByVal X As Double, ByVal Y As Double, ByVal Time As Double)
    MyBase.New(X, Y)
    Me.Time = Time
  End Sub

  Public Shadows Function CreateCopy(Optional ByVal OffsetX As Double = 0, Optional ByVal OffsetY As Double = 0, Optional ByVal OffsetTime As Double = 0) As clsTPoint
    Return New clsTPoint(X + OffsetX, Y + OffsetY, Time + OffsetTime)
  End Function

End Class
