﻿Public Class clsValue
  Public ID As Integer
  Public Name As String
  Public Position As Integer
  Public Comment As String
  Public Status As RecordStatus



  Public Enum RecordStatus As Integer
    NoChanges = 0
    Updated = 1
    Added = 2
    Deleted = 3
  End Enum

End Class
